package core.util.TestAspectator;

import core.util.TestAspectator.beans.AspectBean;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Test methods for the Aspectator used by beans.
 *
 * @version 2024-05-10
 * @author lp
 */

@SuppressWarnings("UnnecessaryBoxing")
public class TestAspectatorForBeans {

 public static List<String> stamps;


 @BeforeTest(groups = {setup.Common.TEST_GROUP_NORMAL})
 void testSetup() {

  stamps = Collections.synchronizedList(new ArrayList<>());

 }



 /**
  * Test for after a property getter.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorAfter() {

  AspectBean aspb = new AspectBean();

  aspb.set("boolean", Boolean.TRUE);

  Assert.assertTrue((Boolean)aspb.get("boolean")); // Setting worked.

  Assert.assertTrue(stamps.contains("afterGetProperty for property boolean"));  // We were in apspect method.

 }



 /**
  * Test for before a property getter.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorBefore() {

  AspectBean aspb = new AspectBean();

  aspb.set("integer", 1);

  Assert.assertEquals(aspb.get("integer"), Integer.valueOf(1)); // Setting worked.

  Assert.assertTrue(stamps.contains("beforeGetProperty for property integer"));  // We were in apspect method.

 }



 /**
  * Test for around a property getter.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorAround() {

  AspectBean aspb = new AspectBean();

  aspb.set("long", Long.valueOf(2));

  Assert.assertEquals(aspb.get("long"), Long.valueOf(2)); // Setting worked.

  Assert.assertTrue(stamps.contains("aroundGetProperty for property long"));  // We were in apspect method.

 }



 /**
  * Test for around a property getter. Aspect modifies return value!
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorAroundMod() {

  AspectBean aspb = new AspectBean();

  aspb.set("long1", Long.valueOf(3));

  // The aspect modified the return value!
  // @see core.util.TestAspectator.aspects.TestBeanAspect.aroundGetProperty
  Assert.assertEquals(aspb.get("long1"), Long.valueOf(15));

  Assert.assertTrue(stamps.contains("aroundGetProperty for property long1"));  // We were in apspect method.

 }



 /**
  * Test for after a property setter.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorAfterSet() {

  AspectBean aspb = new AspectBean();

  aspb.set("booleanS", Boolean.TRUE);

  Assert.assertTrue((Boolean)aspb.get("booleanS")); // Setting worked.

  Assert.assertTrue(stamps.contains("afterSetProperty for property booleanS"));  // We were in apspect method.

 }



 /**
  * Test for before a property setter.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorBeforeSet() {

  AspectBean aspb = new AspectBean();

  aspb.set("integerS", Integer.valueOf(1));

  Assert.assertEquals(aspb.get("integerS"), Integer.valueOf(1)); // Setting worked.

  Assert.assertTrue(stamps.contains("afterSetProperty for property integerS"));  // We were in apspect method.

 }



 /**
  * Test for around a property setter.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorAroundSet() {

  AspectBean aspb = new AspectBean();

  aspb.set("longS", Long.valueOf(2));

  Assert.assertEquals(aspb.get("longS"), Long.valueOf(2)); // Setting worked.

  Assert.assertTrue(stamps.contains("aroundSetProperty for property longS"));  // We were in apspect method.

 }



 /**
  * Test for around a property setter.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorAroundSetMod() {

  AspectBean aspb = new AspectBean();

  aspb.set("long1S", Long.valueOf(2));

  // The aspect modified the value to set!
  // @see core.util.TestAspectator.aspects.TestBeanAspect.aroundSetProperty
  Assert.assertEquals(aspb.get("long1S"), Long.valueOf(3));

  Assert.assertTrue(stamps.contains("aroundSetProperty for property long1S"));  // We were in apspect method.

 }

}
