package core.util.TestAspectator.aspects;

import core.util.Aspectator.*;
import core.util.TestAspectator.TestAspectator;



/**
 * The third aspect class.
 *
 * @version 2018-08-12
 * @author pilgrim.lutz@imail.de
 *
 */

public class TestAspect3 implements Aspect{



 /**
  * Before method call.
  *
  * <p>Matches each method decorated with "TestAnnotation".</p>
  *
  * @see TestAspectator.TestAnnotation
  *
  * @param jointPoint The join point
  */

 @Before(matchedBy = MethodSelect.METHOD_HAS_ANNOTATION, value = ".*TestAnnotation.*")
 public void beforeMethodCall(JoinPoint jointPoint) {

  TestAspectator.stamps.add("TestAspect3.beforeMethodCall");

 }



 /**
  * After method call.
  *
  * <p>Matches each method decorated with "TestAnnotation".</p>
  *
  * @see TestAspectator.TestAnnotation
  *
  * @param jointPoint The join point
  */

 @After(matchedBy = MethodSelect.METHOD_HAS_ANNOTATION, value = ".*TestAnnotation.*")
 public void afterMethodCall(JoinPoint jointPoint) {

  TestAspectator.stamps.add("TestAspect3.afterMethodCall");

 }

}
