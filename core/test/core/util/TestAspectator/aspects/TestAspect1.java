package core.util.TestAspectator.aspects;

import core.util.Aspectator.*;
import core.util.TestAspectator.TestAspectator;
import core.util.Utilities;



/**
 * This first test aspect class.
 *
 * @version 2019-01-29
 * @author pilgrim.lutz@imail.de
 *
 */

public class TestAspect1 implements Aspect{



 /**
  * Before method call.
  *
  * <p>Matches the method with name "testMethod1".</p>
  *
  * @param jointPoint The join point
  */

 @Before(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*testMethod1.*", order = 2)
 public void beforeMethodCall(JoinPoint jointPoint) {

  TestAspectator.stamps.add("TestAspect1.beforeMethodCall");

 }



 /**
  * After method call.
  *
  * <p>Matches the method with name "testMethod1".</p>
  *
  * @param jointPoint The join point
  */

 @After(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*testMethod1.*", order = 2)
 public void afterMethodCall(JoinPoint jointPoint) {

  TestAspectator.stamps.add("TestAspect1.afterMethodCall");

 }



 /**
  * After return method call.
  *
  * <p>Matches the method with name "testMethod1".</p>
  *
  * @param jointPoint The join point
  */

 @AfterReturn(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*testMethod1.*")
 public void afterReturnMethodCall(JoinPoint jointPoint) {

  TestAspectator.stamps.add("TestAspect1.afterReturnMethodCall with tag " + jointPoint.getArgs()[1] + " and return value " + jointPoint.getRetval());

 }



 /**
  * After thrown method call.
  *
  * <p>Matches the method with name "testMethod1".</p>
  *
  * @param jointPoint The join point
  */

 @AfterThrown(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*testMethod1.*",
  thrown = core.util.TestAspectator.TestException1.class)
 public void afterThrownMethodCall(JoinPoint jointPoint) {

  TestAspectator.stamps.add("TestAspect1.afterThrownMethodCall with tag " + jointPoint.getArgs()[1] + " and Throwable " + Utilities.getRealProblem(jointPoint.getThrown()));

 }

}
