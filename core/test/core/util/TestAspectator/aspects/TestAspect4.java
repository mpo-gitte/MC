package core.util.TestAspectator.aspects;

import core.util.Aspectator.*;
import core.util.TestAspectator.TestAspectator;

import java.lang.reflect.InvocationTargetException;



/**
 * The third aspect class.
 *
 * @version 2019-02-01
 * @author pilgrim.lutz@imail.de
 *
 */

public class TestAspect4 implements Aspect{



 /**
  * Around method call.
  *
  * @param jointPoint The join point
  */

 @Around(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*testMethod1.*")
 public Object aroundMethodCall1(JoinPoint jointPoint) throws InvocationTargetException, IllegalAccessException {

  TestAspectator.stamps.add("TestAspect4.aroundMethodCall1");

  return jointPoint.proceed();

 }



 /**
  * Another Around method call.
  *
  * @param jointPoint The join point
  */

 @Around(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*testMethod1.*")
 public Object aroundMethodCall2(JoinPoint jointPoint) throws InvocationTargetException, IllegalAccessException {

  TestAspectator.stamps.add("TestAspect4.aroundMethodCall2");

  return jointPoint.proceed();

 }



 /**
  * One more Around method call.
  *
  * @param jointPoint The join point
  */

 @Around(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*testMethod1.*")
 public Object aroundMethodCall3(JoinPoint jointPoint) throws InvocationTargetException, IllegalAccessException {

  TestAspectator.stamps.add("TestAspect4.aroundMethodCall3");

  return jointPoint.proceed();

 }

}
