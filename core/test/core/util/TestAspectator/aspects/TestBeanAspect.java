package core.util.TestAspectator.aspects;

import core.bakedBeans.PropertyInfo;
import core.util.TestAspectator.TestAspectatorForBeans;

import java.lang.reflect.InvocationTargetException;

import static core.util.Aspectator.*;



/**
 * This test aspect class for beans.
 *
 * @version 2024-05-10
 * @author lp
 *
 */

@SuppressWarnings("UnnecessaryBoxing")
public class TestBeanAspect implements Aspect {



 /**
  * After method call.
  *
  * @param jp The join point
  */

 @After(
  matchedBy = MethodSelect.METHOD_HAS_ANNOTATION,
  value = ".*TestAnnotationBeanAfterGet.*"
 )
 public void afterGetProperty(JoinPoint jp) {

  // We're after a call to a getter. So first argument is always a PropertyInfo
  PropertyInfo pi = (PropertyInfo)jp.getArgs()[0];

  TestAspectatorForBeans.stamps.add("afterGetProperty for property " + pi.getName());

 }



 /**
  * Before method call.
  *
  * @param jp The join point
  */

 @Before(
  matchedBy = MethodSelect.METHOD_HAS_ANNOTATION,
  value = ".*TestAnnotationBeanBeforeGet.*"
 )
 public void beforeGetProperty(JoinPoint jp) {

  // We're before a call to a getter. So first argument is always a PropertyInfo
  PropertyInfo pi = (PropertyInfo)jp.getArgs()[0];

  TestAspectatorForBeans.stamps.add("beforeGetProperty for property " + pi.getName());

 }



 /**
  * Around method call.
  *
  * @param jp The join point
  */

 @Around(
  matchedBy = MethodSelect.METHOD_HAS_ANNOTATION,
  value = ".*TestAnnotationBeanAroundGet.*"
 )
 public Object aroundGetProperty(JoinPoint jp) throws InvocationTargetException, IllegalAccessException {

  // We're before a call to a getter. So first argument is always a PropertyInfo
  PropertyInfo pi = (PropertyInfo)jp.getArgs()[0];

  TestAspectatorForBeans.stamps.add("aroundGetProperty for property " + pi.getName());

  if ("long1".equals(pi.getName())) // Let us modify the return value for this property.
   return Long.valueOf(15);  // In real life we should look if we were the last...

  return
   jp.proceed();

 }



 /**
  * After method call.
  *
  * @param jp The join point
  */

 @After(
  matchedBy = MethodSelect.METHOD_HAS_ANNOTATION,
  value = ".*TestAnnotationBeanAfterSet.*"
 )
 public void afterSetProperty(JoinPoint jp) {

  // We're after a call to a getter. So first argument is always a PropertyInfo
  PropertyInfo pi = (PropertyInfo)jp.getArgs()[0];

  TestAspectatorForBeans.stamps.add("afterSetProperty for property " + pi.getName());

 }



 /**
  * Before method call.
  *
  * @param jp The join point
  */

 @Before(
  matchedBy = MethodSelect.METHOD_HAS_ANNOTATION,
  value = ".*TestAnnotationBeanBeforeSet.*"
 )
 public void beforeSetProperty(JoinPoint jp) {

  // We're before a call to a setter. So first argument is always a PropertyInfo
  PropertyInfo pi = (PropertyInfo)jp.getArgs()[0];

  TestAspectatorForBeans.stamps.add("beforeSetProperty for property " + pi.getName());

 }



 /**
  * Around method call.
  *
  * @param jp The join point
  */

 @Around(
  matchedBy = MethodSelect.METHOD_HAS_ANNOTATION,
  value = ".*TestAnnotationBeanAroundSet.*"
 )
 public Object aroundSetProperty(JoinPoint jp) throws InvocationTargetException, IllegalAccessException {

  // We're before a call to a setter. So first argument is always a PropertyInfo
  PropertyInfo pi = (PropertyInfo)jp.getArgs()[0];

  TestAspectatorForBeans.stamps.add("aroundSetProperty for property " + pi.getName());

  if ("long1S".equals(pi.getName())) // Let us modify the value for this property.
   jp.getArgs()[1] = Long.valueOf(3);

  return
   jp.proceed();

 }

}
