package core.util.TestAspectator.aspects;

import core.util.Aspectator.*;
import core.util.TestAspectator.TestAspectator;



/**
 * This second test aspect class.
 *
 * @version 2018-08-12
 * @author pilgrim.lutz@imail.de
 *
 */

public class TestAspect2 implements Aspect{



 /**
  * Before method call.
  *
  * <p>Matches the method with name "testMethod1".</p>
  *
  * @param jointPoint The join point
  */

 @Before(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*testMethod1.*", order = 1)
 public void beforeMethodCall(JoinPoint jointPoint) {

  TestAspectator.stamps.add("TestAspect2.beforeMethodCall");

 }



 /**
  * After method call.
  *
  * <p>Matches the method with name "testMethod1".</p>
  *
  * @param jointPoint The join point
  */

 @After(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*testMethod1.*", order = 1)
 public void afterMethodCall(JoinPoint jointPoint) {

  TestAspectator.stamps.add("TestAspect2.afterMethodCall");

 }

}
