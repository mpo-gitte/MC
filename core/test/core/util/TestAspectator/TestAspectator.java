package core.util.TestAspectator;

import core.util.Utilities;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Test methods for the Aspectator
 *
 * @version 2023-09-07
 * @author pilgrim.lutz@imail.de
 */

public class TestAspectator {

 public static List<String> stamps;


 @BeforeTest(groups = {setup.Common.TEST_GROUP_NORMAL})
 void testSetup() {

  stamps = Collections.synchronizedList(new ArrayList<>());

 }



 /**
  * Test for the Aspectator.
  *
  * <p>We use a derivate of the Aspectator. Only for more flexible loading of the aspect classes-</p>
  *
  * @see TAspectator
  *
  * @throws NoSuchMethodException If problems occur.
  * @throws InvocationTargetException If problems occur.
  * @throws IllegalAccessException If problems occur.
  * @throws InstantiationException If problems occur.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectator() throws Exception {

  testAspectator(0, "testAspectator", TestException1.class);

  Assert.assertTrue(stamps.contains("TestAspect1.afterReturnMethodCall with tag testAspectator and return value 1"), "Was not in TestAspect1.afterReturnMethodCall");

  // Aspect method for exception must not be called!
  Assert.assertFalse(stamps.contains("TestAspect1.afterThrownMethodCall with tag testAspectator and Throwable TestException1"), "Was not in TestAspect1.afterThrownMethodCall");

 }



 /**
  * Test for the Aspectator.
  *
  * <p>We use a derivate of the Aspectator. Only for more flexible loading of the aspect classes-</p>
  *
  * <p>In this test the test method will throw a TestException1.</p>
  *
  * @see TAspectator
  *
  * @throws NoSuchMethodException If problems occur.
  * @throws InvocationTargetException If problems occur.
  * @throws IllegalAccessException If problems occur.
  * @throws InstantiationException If problems occur.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorWithException1() throws Exception {

  testAspectator(1, "testAspectatorWithException1", TestException1.class);  // With exception 1

  // Aspect method for exception has to be called!
  Assert.assertTrue(stamps.contains("TestAspect1.afterThrownMethodCall with tag testAspectatorWithException1 and Throwable core.util.TestAspectator.TestException1"), "Was not in TestAspect1.afterThrownMethodCall");

  Assert.assertFalse(stamps.contains("TestAspect1.afterReturnMethodCall with tag testAspectatorWithException1 and return value 1"), "Was not in TestAspect1.afterReturnMethodCall");

 }



 /**
  * Test for the Aspectator.
  *
  * <p>We use a derivate of the Aspectator. Only for more flexible loading of the aspect classes-</p>
  *
  * <p>In this test the test method will throw a TestException2. This exception is derived
  * from TestException1. So aspect method must be called.</p>
  *
  * @see TAspectator
  *
  * @throws NoSuchMethodException If problems occur.
  * @throws InvocationTargetException If problems occur.
  * @throws IllegalAccessException If problems occur.
  * @throws InstantiationException If problems occur.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorWithException2() throws Exception {

  testAspectator(2, "testAspectatorWithException2", TestException2.class);  // With exception 2

  // Aspect method for exception has to be called!
  Assert.assertTrue(stamps.contains("TestAspect1.afterThrownMethodCall with tag testAspectatorWithException2 and Throwable core.util.TestAspectator.TestException2"), "Was not in TestAspect1.afterThrownMethodCall");

  Assert.assertFalse(stamps.contains("TestAspect1.afterReturnMethodCall with tag testAspectatorWithException2 and return value 1"), "Was not in TestAspect1.afterReturnMethodCall");

 }



 /**
  * Test for the Aspectator.
  *
  * <p>We use a derivate of the Aspectator. Only for more flexible loading of the aspect classes-</p>
  *
  * <p>In this test the test method will throw a TestException3. This exception is not derived
  * from TestException1. So aspect method must not be called.</p>
  *
  * @see TAspectator
  *
  * @throws NoSuchMethodException If problems occur.
  * @throws InvocationTargetException If problems occur.
  * @throws IllegalAccessException If problems occur.
  * @throws InstantiationException If problems occur.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorWithException3() throws Exception {

  testAspectator(3, "testAspectatorWithException3", TestException3.class);  // With exception 3

  // Aspect method for exception must not be called!
  Assert.assertFalse(stamps.contains("TestAspect1.afterThrownMethodCall with tag testAspectatorWithException3 and Throwable core.util.TestAspectator.TestException3"), "Was not in TestAspect1.afterThrownMethodCall");

  Assert.assertFalse(stamps.contains("TestAspect1.afterReturnMethodCall with tag testAspectatorWithException3 and return value 1"), "Was not in TestAspect1.afterReturnMethodCall");

 }



 /**
  * Testing a nested list of Around aspects.
  *
  * @throws NoSuchMethodException If problems occur.
  * @throws InvocationTargetException If problems occur.
  * @throws IllegalAccessException If problems occur.
  * @throws InstantiationException If problems occur.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorAround() throws Exception {

  // ... called in a way it does'nt throw an exception.
  Object o = testAspectator(0, "", null);  // With exception 3

  // It is surrounded by three advice methods:
  Assert.assertTrue(stamps.contains("TestAspect4.aroundMethodCall1"), "Was not in TestAspect4.aroundMethodCall1");
  Assert.assertTrue(stamps.contains("TestAspect4.aroundMethodCall2"), "Was not in TestAspect4.aroundMethodCall2");
  Assert.assertTrue(stamps.contains("TestAspect4.aroundMethodCall3"), "Was not in TestAspect4.aroundMethodCall3");

  // The return value of testMethod1() must be returned by all advice methods in chain.
  Assert.assertEquals(((Integer)o).intValue(), 1, "Return value of advised method not there!");

 }



 private Object testAspectator(int throwException, String tag, Class expectedException) throws Exception {


  // Get a method to weave the aspects to.

  Method m = TestAspectator.class.getDeclaredMethod("testMethod1", Integer.class, String.class);

  Object res = null;

  try {
   res = TAspectator.get().run(this, m, m, new Object[] {throwException, new String(tag)});
  } catch (Exception e) {
   Assert.assertEquals(Utilities.getRealProblem(e).getClass(), expectedException, "Wrong exception!");
  }

  // Each advice method in the aspect classes writes a unique string to the stamps ArrayList.
  // So we can check here if we were in each method.

  Assert.assertTrue(stamps.contains("TestAspect1.beforeMethodCall"), "Was not in TestAspect1.beforeMethodCall");
  Assert.assertTrue(stamps.contains("TestAspect1.afterMethodCall"), "Was not in TestAspect1.afterMethodCall");
  Assert.assertTrue(stamps.contains("TestAspect2.beforeMethodCall"), "Was not in TestAspect2.beforeMethodCall");
  Assert.assertTrue(stamps.contains("TestAspect2.afterMethodCall"), "Was not in TestAspect2.afterMethodCall");

  return res;

 }



 /**
  * This test checks if weaving aspects to methods marked with an annotation.
  *
  * @throws NoSuchMethodException If problems occur.
  * @throws InvocationTargetException If problems occur.
  * @throws IllegalAccessException If problems occur.
  * @throws InstantiationException If problems occur.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testAspectatorAnnotation() throws Exception {

  // Get the other test method which is marked with an annotation.

  Method m = TestAspectator.class.getDeclaredMethod("testMethod2");


  try {
   TAspectator.get().run(this, m, m, new Object[] {});
  }
  catch (Exception e) {
   Assert.fail("Exception unexpected");
  }


  // Again: check the stamps.

  Assert.assertTrue(stamps.contains("TestAspect3.beforeMethodCall"), "Was not in TestAspect3.beforeMethodCall");
  Assert.assertTrue(stamps.contains("TestAspect3.afterMethodCall"), "Was not in TestAspect3.afterMethodCall");

 }



 /**
  * A nearly empty test method.
  */

 public Object testMethod1(Integer throwException, String tag) throws Exception{

  if (throwException == 1)
   throw new TestException1();

  if (throwException == 2)
   throw new TestException2();

  if (throwException == 3)
   throw new TestException3();

  return 1;
 }



 /**
  * An empty test method. But this one carries an annotation used for triggering weaving.
  */

 @TestAnnotation
 public void testMethod2() {

 }



 /**
  * The annotation to mark a method for weaving.
  *
  */

 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface TestAnnotation {}

}
