package core.util.TestAspectator;



import core.util.Aspectator;

/**
 * Special Aspectator derivative for loading the aspect classes directly-
 *
 * @version 2019-04-14
 * @author pilgrim.lutz@imail.de
 *
 * @see core.util.Aspectator
 *
 */

public class TAspectator extends Aspectator {

 private static TAspectator me = null;



 public static TAspectator get() {

  synchronized(TAspectator.class) {
   if (me == null)
    me = new TAspectator();
  }

  return me;

 }



 /**
  * Loading the aspect classes directly.
  *
  * <p>Not via property file as in super class.</p>
  *
  * <p>For now we're loading the classes:</p>
  *
  * @see core.util.TestAspectator.aspects.TestAspect1
  * @see core.util.TestAspectator.aspects.TestAspect2
  * @see core.util.TestAspectator.aspects.TestAspect3
  *
  * @throws ClassNotFoundException May classes aren't existing.
  *
  */

 @Override
 protected void loadAspects() throws ClassNotFoundException {

  // Loading test aspects. That's the reason why loadAspects() is protected in Aspectator.
  aspectClasses.add(core.util.TestAspectator.aspects.TestAspect1.class);
  aspectClasses.add(core.util.TestAspectator.aspects.TestAspect2.class);
  aspectClasses.add(core.util.TestAspectator.aspects.TestAspect3.class);
  aspectClasses.add(core.util.TestAspectator.aspects.TestAspect4.class);

 }

}
