package core.util.TestAspectator.beans;


import core.bakedBeans.*;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;



/**
 * A test bean with aspects on getters.
 *
 * @version 2024-05-10
 * @author lp
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   name = "boolean",
   pclass = Boolean.class,
   getter = "aspectedBooleanGetter"
  ),
  @PropertyDescription(
   name = "integer",
   pclass = Integer.class,
   getter = "aspectedIntegerGetter"
  ),
  @PropertyDescription(
   name = "long",
   pclass = Long.class,
   getter = "aspectedLongGetter"
  ),
  @PropertyDescription(
   name = "long1",
   pclass = Long.class,
   getter = "aspectedLongGetter"
  ),
  @PropertyDescription(
   name = "booleanS",
   pclass = Boolean.class,
   setter = "aspectedBooleanSetter"
  ),
  @PropertyDescription(
   name = "integerS",
   pclass = Integer.class,
   setter = "aspectedIntegerSetter"
  ),
  @PropertyDescription(
   name = "longS",
   pclass = Long.class,
   setter = "aspectedLongSetter"
  ),
  @PropertyDescription(
   name = "long1S",
   pclass = Long.class,
   setter = "aspectedLongSetter"
  ),
 }
)



public class AspectBean extends TypedBean {

 @TestAnnotationBeanAfterGet
 @TypedBeanGetter
 protected Object aspectedBooleanGetter(PropertyInfo pi) {

  return stdGetter(pi);

 }



 @TestAnnotationBeanBeforeGet
 @TypedBeanGetter
 protected Object aspectedIntegerGetter(PropertyInfo pi) {

  return stdGetter(pi);

 }



 @TestAnnotationBeanAroundGet
 @TypedBeanGetter
 protected Object aspectedLongGetter(PropertyInfo pi) {

  return stdGetter(pi);

 }



 @TestAnnotationBeanAfterSet
 @TypedBeanSetter
 protected void aspectedBooleanSetter(PropertyInfo pi, Object val) {

  stdSetter(pi, val);

 }



 @TestAnnotationBeanAfterSet
 @TypedBeanSetter
 protected void aspectedIntegerSetter(PropertyInfo pi, Object val) {

  stdSetter(pi, val);

 }



 @TestAnnotationBeanAroundSet
 @TypedBeanSetter
 protected void aspectedLongSetter(PropertyInfo pi, Object val) {

  stdSetter(pi, val);

 }



 /**
  * The annotations to getter / setter a method for weaving aspects.
  */

 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface TestAnnotationBeanAfterGet {}

 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface TestAnnotationBeanBeforeGet {}

 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface TestAnnotationBeanAroundGet {}

 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface TestAnnotationBeanAfterSet {}

 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface TestAnnotationBeanBeforeSet {}

 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface TestAnnotationBeanAroundSet {}

}
