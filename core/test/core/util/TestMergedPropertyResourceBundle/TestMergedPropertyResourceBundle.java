package core.util.TestMergedPropertyResourceBundle;

import core.util.MergedPropertyResourceBundle;
import core.util.Utilities;
import org.testng.Assert;
import org.testng.annotations.Test;
import setup.Common;

import java.util.ResourceBundle;



/**
 * Checking the MergedPropertyResourceBundle.
 *
 * @version 2019-02-28
 * @author pilgrim.lutz@imail.de
 *
 */

public class TestMergedPropertyResourceBundle {



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testFetchMergedPropertyResourceBundle() {

  ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle("Test");

  String sd = Utilities.getParam(rb, "prop1", "");

  Assert.assertEquals(sd, "Property1 from test1");

  sd = Utilities.getParam(rb, "prop2", "");

  Assert.assertEquals(sd, "Property2 from test2");

  sd = Utilities.getParam(rb, "prop3", "");

  Assert.assertEquals(sd, "Property3 from test3");

 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testFetchMergedPropertyResourceBundleFromEnvironment() {

  ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle("Test");


  // Property comes from environment.

  String sd = Utilities.getParam(rb, "prop4", "");

  Assert.assertEquals(sd, "Property4 from environment");

 }

}
