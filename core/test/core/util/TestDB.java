package core.util;

import core.bakedBeans.PersistentBean;
import core.bakedBeans.beans.DataTypeMappingBean;
import core.bakedBeans.beans.MasterBean;
import core.bakedBeans.beans.SubBean;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;



/**
 * Core tests for db access
 *
 * @version 2023-09-07
 * @author pilgrim.lutz@imail.de
 */

public class TestDB {



 /**
  * Test type mapping
  *
  * <p>Assuming that data is already in database.</p>
  */

 @Test(groups = {setup.Common.TEST_GROUP_WITH_DATABASE})
 public void testDatatypeMapping() {

  DataTypeMappingBean dtm = new DataTypeMappingBean();


  // Standard types.

  dtm.load(1);
  Assert.assertEquals((LocalDateTime)dtm.get("created"), LocalDateTime.of(2020, 1, 19, 14, 30, 0, 0));
  Assert.assertEquals((String)dtm.get("teststring"), "Test1");
  Assert.assertEquals((LocalDate)dtm.get("testdate"), LocalDate.of(2020, 1, 19));
  Assert.assertEquals((Integer)dtm.get("testint"), Integer.valueOf(1));


  // String with umlauts.

  dtm.load(2);
  Assert.assertEquals((String)dtm.get("teststring"), "Test äöüÄÖÜß");

 }



 @Test(groups = {setup.Common.TEST_GROUP_WITH_DATABASE})
 public void testWriting() {

  DataTypeMappingBean dtm = new DataTypeMappingBean();

  dtm.set("created", LocalDateTime.now());
  dtm.set("teststring", "TestString äöüÄÖÜß");
  dtm.set("testdate", LocalDate.of(2020, 1, 12));
  dtm.set("testint", 42);

  dtm.save();


  // There two rows in the table. Sequence starts with 3.

  Assert.assertEquals((Integer)dtm.get("id"), Integer.valueOf(3));


  // Read again.

  dtm = new DataTypeMappingBean();

  dtm.load(3);
  Assert.assertEquals((String)dtm.get("teststring"), "TestString äöüÄÖÜß");
  Assert.assertEquals((Integer)dtm.get("testint"), Integer.valueOf(42));

 }



 /**
  * Test loading a sub bean.
  */

 @Test(groups = {setup.Common.TEST_GROUP_WITH_DATABASE})
 public void testCascadeRead() {

  MasterBean mb = new MasterBean();

  mb.load(1);

  // SubBean should be loaded too.
  SubBean sb = (SubBean)mb.get("subBean");

  Assert.assertEquals((String)sb.get("teststring"), "Sub1");

  Assert.assertEquals(mb.getStatus(), PersistentBean.Status.clean);
  Assert.assertEquals(sb.getStatus(), PersistentBean.Status.clean);

 }



 /**
  * Test writing a bean and its sub bean.
  */

 @Test(groups = {setup.Common.TEST_GROUP_WITH_DATABASE})
 public void testCascadeWrite() {


  // Write the master bean and its sub bean.

  MasterBean mb = new MasterBean();
  mb.set("created", LocalDateTime.now());
  mb.set("teststring", "testtest");

  SubBean sb = (SubBean)mb.get("subBean");
  sb.set("created", LocalDateTime.now());
  sb.set("teststring", "testSUBtest");

  mb.save();

  Integer id = (Integer)mb.get("id");


  // Relaod

  mb = new MasterBean();
  mb.load(id);

  sb = (SubBean)mb.get("subBean");

  Assert.assertEquals((String)sb.get("teststring"), "testSUBtest");

 }

}
