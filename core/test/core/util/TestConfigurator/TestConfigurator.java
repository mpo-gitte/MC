package core.util.TestConfigurator;

import core.util.Configurator;
import org.testng.Assert;
import org.testng.annotations.Test;
import setup.Common;

import java.lang.annotation.*;
import java.util.List;
import java.util.Optional;


public class TestConfigurator {


 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testConfigurator() {

  Configurator conni = Configurator.get();

  conni.configure(TestClass.class);  // Configure static members.

  TestClass tc = new TestClass();

  conni.configure(tc);  // Configure other members.

  Assert.assertEquals(TestClass.staticIntOVal, Integer.valueOf(42), "Value not set!");
  Assert.assertEquals(TestClass.staticIntVal, 41, "Value not set!");
  Assert.assertEquals(TestClass.staticDoubleOVal, 42.42, "Value not set!");
  Assert.assertEquals(TestClass.staticSpecialDoubleOVal, 21.21, "Value not set!");
  Assert.assertEquals(TestClass.staticSpecialDoubleOVal2, -21.21, "Value not set!");

  Assert.assertEquals(tc.intOVal, Integer.valueOf(42), "Value not set!");
  Assert.assertEquals(tc.intVal, 43, "Value not set!");
  Assert.assertEquals(tc.finalIntVal, -1234, "Value not set!");

  // Super classes statics configured?
  Assert.assertEquals(TestClass.staticSuperIntVal, 4711, "Value not set!");
  Assert.assertEquals(TestClass.staticSuperSuperIntVal, 4712, "Value not set!");

  // Super classes configured?
  Assert.assertEquals(tc.superIntVal, 4713, "Value not set!");
  Assert.assertEquals(tc.superSuperIntVal, 4714, "Value not set!");

  // The value is set by the ConfigValue TestConfigConfigIntegers which is itself configured by
  // the ConfigValue TestConfigIntegers.0
  Assert.assertEquals(TestClass.configIntVal, 41, "Value not set!");

 }



 /**
  * The annotation to mark a field for configuring.
  *
  */

 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.FIELD})
 public @interface TestAnnotation {

  double value();

 }



 /**
  * Checks method parameter configuration.
  *
  * For the method "testMethod1" with two parameters and no configuration.
  *
  * @throws NoSuchMethodException If problems occur.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testConfiguratorParameter1() throws NoSuchMethodException {

  Configurator conni = Configurator.get();

  List<Optional<Configurator.ConfigValueSupplier>> configValueSuppliers
   = conni.getConfigValueSuppliers(this.getClass(), this.getClass().getMethod("testMethod1", String.class, Integer.class));

  Assert.assertEquals(configValueSuppliers.size(), 2, "Not enough parameters");
  Assert.assertTrue(configValueSuppliers.get(0).isEmpty());
  Assert.assertTrue(configValueSuppliers.get(1).isEmpty());

 }



 /**
  * Checks method parameter configuration.
  *
  * For the method "testMethod2" with three parameters and configuration for the
  * last two parameters.
  *
  * @throws NoSuchMethodException If problems occur.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testConfiguratorParameter2() throws NoSuchMethodException {

  Configurator conni = Configurator.get();

  List<Optional<Configurator.ConfigValueSupplier>> configValueSuppliers
   = conni.getConfigValueSuppliers(this.getClass(), this.getClass().getMethod("testMethod2", String.class, Integer.class, Double.class));

  Assert.assertEquals(configValueSuppliers.size(), 3, "Not enough parameters");
  // First parameter has no config.
  Assert.assertTrue(configValueSuppliers.get(0).isEmpty());
  // Second parameter has config with annotation.
  Assert.assertEquals(configValueSuppliers.get(1).get().getFieldSelect(), Configurator.FieldSelect.FIELD_HAS_ANNOTATION);
  // Third parameter has config per name.
  Assert.assertEquals(configValueSuppliers.get(2).get().getFieldSelect(), Configurator.FieldSelect.FIELD_HAS_NAME);

 }



 /**
  * Checks method parameter configuration.
  *
  * For the method "testMethod2" with three parameters and configuration for the
  * last two parameters.
  *
  * @throws NoSuchMethodException If problems occur.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testConfiguratorParameter3() throws NoSuchMethodException {

  Configurator conni = Configurator.get();

  List<Optional<Configurator.ConfigValueSupplier>> configValueSuppliers
   = conni.getConfigValueSuppliers(this.getClass(), this.getClass().getMethod("testMethod3", String.class, Integer.class, Boolean.class));

  Assert.assertEquals(configValueSuppliers.size(), 3, "Not enough parameters");
  // First parameter has no config.
  Assert.assertTrue(configValueSuppliers.get(0).isEmpty());
  // Second parameter has config with annotation.
  Assert.assertEquals(configValueSuppliers.get(1).get().getFieldSelect(), Configurator.FieldSelect.FIELD_HAS_ANNOTATION);
  // Third parameter has config per data type.
  Assert.assertEquals(configValueSuppliers.get(2).get().getFieldSelect(), Configurator.FieldSelect.FIELD_HAS_NAME);

 }



 // This annotation marks config.
 // See TestConfigParam.
 @Documented
 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.PARAMETER})
 private @interface ParamConfig {

 }



 // Test method with no config.
 public void testMethod1(String p1, Integer p2) {

 }



 // Test method with config.
 public void testMethod2(String p1, @ParamConfig Integer p2, Double p3) {

 }



 // Test method with config.
 public void testMethod3(String p1, @ParamConfig Integer p2, Boolean p3) {

 }

}
