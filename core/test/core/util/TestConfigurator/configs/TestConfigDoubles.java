package core.util.TestConfigurator.configs;

import core.util.Configurator;
import core.util.TestConfigurator.TestConfigurator;



public class TestConfigDoubles implements Configurator.Config {

 Double dblVal = null;


 @Override
 public void onLoad() {

  dblVal = 42.42;  // Indirect "test" if onLoad() gets called.

 }



 /**
  * Config value for Double named "staticDoubleOval".
  *
  * @param Configurator.ExtField The field.
  * @return The value 42.42.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= ".*staticDoubleOVal.*")
 public Object getDoubleValue(Configurator.ConfigurableField field) {

  return dblVal;

 }



 /**
  * Config value for all Double with annotation "TestAnnotation".
  *
  * @param Configurator.ExtField The field.
  * @return The value 21.21. Eventually negative.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_ANNOTATION, value= ".*TestAnnotation.*")
 public Object getSpecialDoubleValue(Configurator.ConfigurableField field) {

  TestConfigurator.TestAnnotation anno =
   field.getField().getAnnotation(TestConfigurator.TestAnnotation.class);

  return dblVal / anno.value();

 }

}
