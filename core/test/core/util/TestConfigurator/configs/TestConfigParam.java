package core.util.TestConfigurator.configs;

import core.util.Configurator;


public class TestConfigParam implements Configurator.Config {



 /**
  * Config value for all Integer params with annotation ParamConfig.
  *
  * @param Configurator.ExtField The field.
  * @return The value 42.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_ANNOTATION, value= ".*ParamConfig.*")
 public Object getIntegerValue(Configurator.ConfigurableField field) {

  return 42;

 }



 /**
  * Config value for all third params.
  *
  * @param Configurator.ExtField The field.
  * @return The value 4711.2.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= ".*param\\[2\\].*")
 public Object getDoubleValue(Configurator.ConfigurableField field) {

  return 4711.2;

 }



 /**
  * Config value for all String params.
  *
  * @param Configurator.ExtField The field.
  * @return The value 4711.2.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= ".*Boolean param\\[.*")
 public Object getBooleanValue(Configurator.ConfigurableField field) {

  return Boolean.TRUE;

 }

}
