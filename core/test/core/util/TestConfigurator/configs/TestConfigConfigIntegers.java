package core.util.TestConfigurator.configs;

import core.util.Configurator;


public class TestConfigConfigIntegers implements Configurator.Config {

 static int staticIntVal;


 /**
  * Config value for all Integer.
  *
  * @param Configurator.ExtField The field.
  * @return The value 42.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value = "int.*\\.configIntVal")
 public Object getIntegerValue(Configurator.ConfigurableField field) {

  return staticIntVal;

 }



 /**
  * Config value for a static Integer.
  *
  * @param Configurator.ExtField The field.
  * @return The value 41.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= "int.*\\.staticIntVal")
 public Object getStaticIntValue(Configurator.ConfigurableField field) {

  return 41;

 }



 /**
  * Config value for a static Integer.
  *
  * @param Configurator.ExtField The field.
  * @return The value 4711.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= "int.*\\.staticSuperIntVal")
 public Object getStaticSuperIntValue(Configurator.ConfigurableField field) {

  return 4711;

 }



 /**
  * Config value for a static Integer.
  *
  * @param Configurator.ExtField The field.
  * @return The value 4712.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= "int.*\\.staticSuperSuperIntVal")
 public Object getStaticSuperSuperIntValue(Configurator.ConfigurableField field) {

  return 4712;

 }



 /**
  * Config value for a static Integer.
  *
  * @param Configurator.ExtField The field.
  * @return The value 4711.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= "int.*\\.superIntVal")
 public Object getSuperIntValue(Configurator.ConfigurableField field) {

  return 4713;

 }



 /**
  * Config value for a static Integer.
  *
  * @param Configurator.ExtField The field.
  * @return The value 4712.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= "int.*\\.superSuperIntVal")
 public Object getSuperSuperIntValue(Configurator.ConfigurableField field) {

  return 4714;

 }



 /**
  * Config value for a non static Integer.
  *
  * @param Configurator.ExtField The field.
  * @return The value 43.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= "int.*\\.intVal")
 public Object getIntValue(Configurator.ConfigurableField field) {

  return 43;

 }



 /**
  * Config value for another non static Integer.
  *
  * <p>This method should never be called because the matching value in TestClass is final!</p>
  *
  * @param Configurator.ExtField The field.
  * @return The value 333.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= "int.*\\.finalIntVal")
 public Object getIntValue1(Configurator.ConfigurableField field) {

  return 333;

 }

}
