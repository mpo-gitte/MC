package core.util.TestConfigurator;

import static core.util.TestConfigurator.TestConfigurator.*;

public class TestClass extends TestSuperClass {

  static int staticIntVal;
  static Integer staticIntOVal;
  static Double staticDoubleOVal;
  @TestAnnotation(2)
  static Double staticSpecialDoubleOVal;
  @TestAnnotation(-2)
  static Double staticSpecialDoubleOVal2;

  int intVal;
  final int finalIntVal = -1234;
  Integer intOVal;
  public double doubleVal;

  static int configIntVal;

}
