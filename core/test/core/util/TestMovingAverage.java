package core.util;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


/**
 * Test the moving average.
 *
 * @version 2023-11-25
 * @author lp
 */

public class TestMovingAverage {

 @DataProvider(name = "lotOfInts")
 public static Object[][] getLotOfInts() {

  return new Object[][]{
   {
    new int[]{1, 1, 1, 1, 1},
    new int[]{0, 0, 0, 0, 1}, 5
   },
   {
    new int[]{1, 5, 1, 5, 5},
    new int[]{0, 1, 1, 2, 3}, 5
   },
   {
    new int[]{5, 5, 5, 5, 5},
    new int[]{1, 2, 3, 4, 5}, 5
   },
   {
    new int[]{5, 5, 5, 5, 5, 5, 5, 4, 2, 2, 2, 1},
    new int[]{1, 2, 3, 4, 5, 5, 5, 4, 4, 3, 3, 2}, 5
   },
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "lotOfInts")
 public void testMovingAverage(int[] ints, int[] ave, int size) {

  MovingIntAverage mave = new MovingIntAverage(size, 0);

  for (int ix = 0; ix < ints.length; ix ++) {
   mave.add(ints[ix]);
   Assert.assertEquals(mave.getAverage(), ave[ix]);
  }

 }

}
