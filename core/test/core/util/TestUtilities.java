package core.util;

import org.apache.velocity.exception.MethodInvocationException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;



/**
 * Test for utilities.
 *
 * @version 2024-09-24
 * @author lp
 *
 */

public class TestUtilities {



 /**
  * Test isNull().
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testIsNull() {

  String sd = null;

  Assert.assertTrue(Utilities.isNull(sd), "Huh! Not null!");

  sd = "";

  Assert.assertFalse(Utilities.isNull(sd), "Huh! Null!");

 }



 /**
  * Test isEmpty().
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testIsEmpty() {

  String sd = null;

  Assert.assertTrue(Utilities.isEmpty(sd), "Huh! Not empty!");

  sd = "";

  Assert.assertTrue(Utilities.isEmpty(sd), "Huh! Empty!");

 }



 /**
  * Test isEmpty().
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testIsEmptyList() {

  List<String> l = null;

  Assert.assertTrue(Utilities.isEmptyList(l), "Huh! Not empty!");

  l = new ArrayList<>();

  Assert.assertTrue(Utilities.isEmptyList(l), "Huh! Not empty!");

  l.add("Test");

  Assert.assertFalse(Utilities.isEmptyList(l), "Huh! Empty!");

 }



 /**
  * Tests Postgres UTF8 quoting.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testPostgresQuote() {

  Assert.assertNotEquals("\\005c\\0027\\0008\\000c\\000a\\000d\\0009abc", Utilities.postgresQuote("\\\'\b\f\n\tabc"), "Quote doesn't work!");

 }



 /**
  * Test diffTimestamp().
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testDiffTimestamps() {

  LocalDateTime ts1 = LocalDateTime.now();
  LocalDateTime ts2 = ts1;  // LocalDateTime is immutable!

  Assert.assertEquals(Utilities.diffTimestamps(ts1, ts2), 0, "Oh, not equal!");  // Should never happen!

  int offset = 5 * 60;

  ts2 = ts2.plusSeconds(offset);

  // Early - later = positive! Utilities.diffTimestamps() return milliseconds.
  Assert.assertEquals(Utilities.diffTimestamps(ts1, ts2), offset * 1000, "Oh, not different!");

  Assert.assertEquals(Utilities.diffTimestamps(ts2, ts1), -offset * 1000, "Oh, not different!");

 }



 /**
  * Test the method getRealProblem().
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testGetRealProblem() {

  // It's a velocity exception.
  MethodInvocationException ex =
   new MethodInvocationException("Test", new NullPointerException(), "test", "test.vm", 4711, 0);

  Assert.assertEquals(
   Utilities.getRealProblem(ex).getClass(), java.lang.NullPointerException.class, "Oh, wrong exception!"
  );

  InvocationTargetException ex1 =
   new InvocationTargetException(new NullPointerException());

  Assert.assertEquals(
   Utilities.getRealProblem(ex1).getClass(), java.lang.NullPointerException.class, "Oh, wrong exception!"
  );

  // Sometimes InvocationTargetExceptions are nested!
  InvocationTargetException ex2 =
   new InvocationTargetException(new InvocationTargetException(new NullPointerException()));

  Assert.assertEquals(
   Utilities.getRealProblem(ex2).getClass(), java.lang.NullPointerException.class, "Oh, wrong exception!"
  );

 }



 /**
  * Tests the method isClassDerivedFrom().
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testIsClassDerivedFrom() {

  Assert.assertTrue(Utilities.isClassDerivedFrom(TestClass2.class, TestClass1.class));

  Assert.assertTrue(Utilities.isClassDerivedFrom(TestClass2.class, Object.class));

  Assert.assertTrue(Utilities.isClassDerivedFrom(Integer.class, Object.class));

  Assert.assertFalse(Utilities.isClassDerivedFrom(TestClass1.class, Integer.class));

 }



 private class TestClass1 {

 }



 private class TestClass2 extends TestClass1 {

 }



 /**
  * Checking the method for accessing system properties.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testgetSystemProperty() {

  final String defValIfNotSet = "defValIfNotSet";
  final String propName = "test.system.property";

  // Forcing default value.

  String sd = Utilities.getSystemProperty(propName, defValIfNotSet);

  Assert.assertEquals(sd, defValIfNotSet);  // Value not set. defVal must be returned.

  // "Real" value.

  System.setProperty(propName, "NEWVAL");

  sd = Utilities.getSystemProperty(propName, defValIfNotSet);

  Assert.assertEquals(sd, "NEWVAL");  // Property is set! The new value must be returned.

 }



 /**
  * Test method getDateString()
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testgetDateString() {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");
  LocalDate d = LocalDate.parse("23.11.2019", df);

  String sd = Utilities.getDateString(d);

  Assert.assertEquals(sd, "20191123");

 }



 /**
  * Test method getDateAndTimeString()
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testgetDateAndTimeString() {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
  LocalDateTime d = LocalDateTime.parse("23.11.2019 10:01:02", df);

  String sd = Utilities.getDateAndTimeString(d);

  Assert.assertEquals(sd, "20191123T100102Z");

 }



 /**
  * Test method getYearString()
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testgetYearString() {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");
  LocalDate d = LocalDate.parse("23.11.2019", df);

  String sd = Utilities.getYearString(d);

  Assert.assertEquals(sd, "2019");

 }



 /**
  * Test method getMonthString()
  */

 @DataProvider(name = "timestamps")
 public static Object[][] getTimeStamps() {

  return new Object[][]{
   {"04.01.2019", "01", "04"},
   {"23.11.2019", "11", "23"},
  };
 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "timestamps")
 public void testgetMonthString(String ts, String fmonth, String fday) {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");
  LocalDate d = LocalDate.parse(ts, df);

  String sd = Utilities.getMonthString(d);

  Assert.assertEquals(sd, fmonth);

 }



 /**
  * Test method getDayString()
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "timestamps")
 public void testgetDayString(String ts, String fmonth, String fday) {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");
  LocalDate d = LocalDate.parse(ts, df);

  String sd = Utilities.getDayString(d);

  Assert.assertEquals(sd, fday);

 }



 /**
  * Test method addOffset()
  */

 @DataProvider(name = "offsets")
 public static Object[][] getOffsets() {

  return new Object[][]{
   {"04.01.2019", 1, 0, 0, 5, 1, 2019},
   {"28.02.2019", 1, 0, 0, 1, 3, 2019},  // No leap year!
   {"30.12.2019", 2, 0, 0, 1, 1, 2020},
   {"28.02.2020", 1, 0, 0, 29, 2, 2020},  // Leap year!

   {"04.01.2019", 0, 1, 0, 4, 2, 2019},
   {"04.12.2019", 0, 1, 0, 4, 1, 2020},

   {"04.01.2019", 0, 0, 1, 4, 1, 2020}

  };
 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "offsets")
 public void testgetaddOffset(String ts, int dayOffset, int monthOffset, int yearOffset,
                              int dayExp, int monthExp, int yearExp) {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");

  LocalDate d = LocalDate.parse(ts, df);

  LocalDate dr = Utilities.addOffset(d, yearOffset, monthOffset, dayOffset);

  Assert.assertEquals(dr.getDayOfMonth(), dayExp);
  Assert.assertEquals(dr.getMonth(), Month.of(monthExp));
  Assert.assertEquals(dr.getYear(), yearExp);

 }



 /**
  * Tests the method makeNameReadable().
  */

 @DataProvider(name = "names")
 public static Object[][] getNames() {

  return new Object[][]{
   {"Wesley, Fred", "Fred Wesley"},
   {"von Schlippenbach, Alexander", "Alexander von Schlippenbach"},
   {"v. Schlippenbach, Alexander", "Alexander v. Schlippenbach"},
   {"Meier-Hauser, Kurt", "Kurt Meier-Hauser"},

   {"Test", "Test"},
   {"Test, Test1, Test2", "Test, Test1, Test2"},
   {"B,A", "A B"},

  };
 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "names")
 public void testmakeNameReadable(String name, String nameExp) {

  String sd = Utilities.makeNameReadable(name);

  Assert.assertEquals(sd, nameExp);

 }



 @DataProvider(name = "dates")
 public static Object[][] getDates() {

  return new Object[][] {
   {"02.12.2019", "de", "DE", "02.12.2019"},  // The monday of the week: Only time changes.
   {"04.12.2019", "de", "DE", "02.12.2019"},
   {"08.12.2019", "de", "DE", "02.12.2019"},  // The sunday of the week: Shift to monday.
   {"01.03.2020", "de", "DE", "24.02.2020"},  // Changes month in a leap year

   {"02.12.2019", "en", "GB", "02.12.2019"},  // The monday of the week: Only time changes.
   {"04.12.2019", "en", "GB", "02.12.2019"},
   {"08.12.2019", "en", "GB", "02.12.2019"},  // The sunday of the week: Shift to monday.

   {"02.12.2019", "en", "US", "01.12.2019"},  // The monday of the week: Shift to sunday.
   {"04.12.2019", "en", "US", "01.12.2019"},
   {"08.12.2019", "en", "US", "08.12.2019"},  // In this locale weeks will begin with sunday!
  };
 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "dates")
 public void beginOfThisWeek(String ts, String locLang, String locCountry, String rests) {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");

  LocalDate d = LocalDate.parse(ts, df);

  Locale loc = new Locale(locLang, locCountry);

  LocalDate dr = Utilities.beginOfThisWeek(d, loc);

  Assert.assertEquals(df.format(dr), rests);

 }



 /**
  * Tests the method todaysBeginning()
  */

 @DataProvider(name = "datesWithTime")
 public static Object[][] getDatesWithTime() {

  return new Object[][]{
   {"08.12.2019 00:00:00", "08.12.2019 00:00:00", "08.12.2019 23:59:59"},
   {"08.12.2019 23:59:59", "08.12.2019 00:00:00", "08.12.2019 23:59:59"},
  };
 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "datesWithTime")
 public void todaysBeginning(String ts, String resb, String rese) {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
  LocalDateTime d1 = LocalDateTime.parse(ts, df);

  LocalDateTime dr = Utilities.todaysBeginning(d1);

  Assert.assertEquals(df.format(dr), resb);

 }



 /**
  * Tests the method todaysEnd()
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "datesWithTime")
 public void todaysEnd(String ts, String resb, String rese) {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
  LocalDateTime d1 = LocalDateTime.parse(ts, df);

  LocalDateTime dr = Utilities.todaysEnd(d1);

  Assert.assertEquals(df.format(dr), rese);

 }



 /**
  * Tests the method yearDiff()
  */

 @DataProvider(name = "datesDiffs")
 public static Object[][] getDatesDiffs() {

  return new Object[][]{
   {"08.12.2019", "08.12.2019", 0},
   {"08.12.2019", "08.12.2020", 1},
   {"08.12.2050", "08.12.2010", -40},
   {"04.05.1956", "08.12.2010", 54},
  };
 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "datesDiffs")
 public void yearDiff(String date1, String date2, Integer res) {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");

  LocalDate d1 = LocalDate.parse(date1, df);
  LocalDate d2 = LocalDate.parse(date2, df);

  Integer id = Utilities.yearDiff(d1, d2);

  Assert.assertEquals(id, res);

 }



 /**
  * Tests divide.
  *
  * @return The result.
  */

 @DataProvider(name = "divDoubles")
 public static Object[][] getDivDoubles() {

  return new Object[][]{
   {10., 5., 2.},
   {2., 4., 0.5},
   {0., 4., 0.},
   {1., 0., null},
  };
 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "divDoubles")
 public void divide(Double d1, Double d2, Double res) {

  Double dd = Utilities.divide(d1, d2);

  if (res == null)
   Assert.assertTrue(dd.isInfinite());
  else
   Assert.assertEquals(dd, res);

 }



 /**
  * Tests substract
  *
  * return the result.
  */

 @DataProvider(name = "substractDoubles")
 public static Object[][] getSubstractDoubles() {

  return new Object[][]{
   {10., 5., 5.},
   {2., 4., -2.},
   {0., 4., -4.},
   {5.3, 2.4, 2.9},
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "substractDoubles")
 public void substract(Double d1, Double d2, Double res) {


  Double dd = Utilities.substract(d1, d2);

  Assert.assertEquals(dd, res);

 }



 /**
  * Tests multiply.
  *
  * @return The result.
  */

 @DataProvider(name = "multDoubles")
 public static Object[][] getMultDoubles() {

  return new Object[][]{
   {10., 5., 50.},
   {0., 4., 0.},
   {1., 0., 0.},
   {3, 3, 9.},
   {"5", 2, 10.},
   {"6", "2", 12.}
  };
 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "multDoubles")
 public void multiply(Object d1, Object d2, Double res) {

  Double dd = Utilities.multiply(d1, d2);

  Assert.assertEquals(dd, res);

 }



 @DataProvider(name = "someData")
 public static Object[][] getSomeData() {

  return new Object[][]{
   {4711, "4711"},
   {3.14, "3.14"},
   {"test", "\"test\""},
   {LocalDate.parse("11.03.1960", DateTimeFormatter.ofPattern("dd.MM.yyyy")), "\"1960-03-11\""},
   {LocalDateTime.parse("11.03.1960 12:12:12.123456", DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss.SSSSSS")), "\"1960-03-11T12:12:12.123456\""},
   {LocalDateTime.parse("11.03.1960 12:12:12.000", DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss.SSS")), "\"1960-03-11T12:12:12.000000\""}
  };
 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "someData")
 public void objectToString(Object testObject, String result) {

  Assert.assertEquals(Utilities.objectToString(testObject), result);

 }



 /**
  * Test decodeURIComponent().
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testDecodeURIComponent() throws UnsupportedEncodingException {

  String uri = "?X++%5B%5D";

  String sd = Utilities.decodeURIComponent(uri);

  Assert.assertEquals(sd, "?X++[]");

 }



 /**
  * Test objectToString
  *
  * <p>Test all supported data types.</p>
  *
  */
 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public static void testObjectToString() {

  String res;


  // Integer

  res = core.base.Utilities.objectToString(4711);
  Assert.assertEquals(res, "4711");


  // Long

  res = core.base.Utilities.objectToString(4711L);
  Assert.assertEquals(res, "4711");


  // Float

  res = core.base.Utilities.objectToString(-32.44);
  Assert.assertEquals(res, "-32.44");


  // Double

  res = core.base.Utilities.objectToString(100.11D);
  Assert.assertEquals(res, "100.11");


  // Boolean

  res = core.base.Utilities.objectToString(Boolean.TRUE);
  Assert.assertEquals(res, "true");
  res = core.base.Utilities.objectToString(Boolean.FALSE);
  Assert.assertEquals(res, "false");


  // LocalDate

  LocalDate d = LocalDate.parse("1960-03-11", DateTimeFormatter.ISO_DATE);
  res = core.base.Utilities.objectToString(d);
  Assert.assertEquals(res, "\"1960-03-11\"");  // Mind the quotes!


  // LocalDateTime

  LocalDateTime dt = LocalDateTime.parse("1960-03-11T10:15:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME);
  res = core.base.Utilities.objectToString(dt);
  Assert.assertEquals(res, "\"1960-03-11T10:15:00.000000\"");  // Always with nanos and quotes.

 }



 /**
  * Tests URLEncode.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public static void testURLEncode() throws UnsupportedEncodingException {

  String sd = Utilities.urlEncode("abcABCaöüßÄÖÜ");
  Assert.assertEquals(sd, "abcABCa%C3%B6%C3%BC%C3%9F%C3%84%C3%96%C3%9C");

 }



 /**
  * Tests testEncodeURIComponent.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public static void testEncodeURIComponent() throws UnsupportedEncodingException {

  String sd = Utilities.encodeURIComponent(" !'()~ÄÖÜäöüß");
  Assert.assertEquals(sd, "%20!'()~%C3%84%C3%96%C3%9C%C3%A4%C3%B6%C3%BC%C3%9F");

 }



 /**
  * Tests testEncodeURIComponentAlt.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public static void testEncodeURIComponentAlt() throws UnsupportedEncodingException {

  String sd = Utilities.encodeURIComponentAlt(".-_");
  Assert.assertEquals(sd, "%2E%2D%5F");

 }



 @DataProvider(name = "someDurations")
 public static Object[][] getSomeDurations() {

  return new Object[][]{
   {"10",     10},
   {":10",    10},
   {"5:10",   310},
   {"2:5:10", 7510},
   {"::10",   10},
   {"2::10",  7210},
   {"2::",    7200},
   {":5:",    300},
   {"::",     0},
  };
 }



 /**
  * Tests converting a duration string to a number seconds.
  *
  * <p>See test data for allowed patterns.</p>

  * @param duration A duration string.
  * @param seconds The resulting seconds.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "someDurations")
 public static void testGetDurationFromString(String duration, int seconds) {

  Assert.assertEquals(Utilities.getDurationFromString(duration), seconds);

 }



 /**
  * Test prefixListContains().
  */

 @DataProvider(name = "somePrefixesAndString")
 public static Object[][] getSomePrefixesAndStrings() {

  return new Object[][]{
   {new String[] {"test1", "test2"},     "test1test", true,  null},
   {new String[] {"test1", "test2"},     "test1",     true,  null},
   {new String[] {"test1", "test2"},     "test2test", true,  null},
   {new String[] {"test1", "test2"},     "test2",     true,  null},
   {new String[] {"test1", "test2"},     "test3test", false, null},
   {new String[] {"test1", "test2"},     "test3",     false, null},
   {new String[] {"test1", "test2"},     "",          false, null},
   {new String[] {"test1", "test2"},     null,        false, NullPointerException.class},
   {null,                                "test3",     false, NullPointerException.class},
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "somePrefixesAndString")
 public static void testPrefixListContains(String[] prefixes, String str, boolean res, Class<? extends Throwable> exc) {

  if (exc == null)
   Assert.assertEquals(Utilities.prefixListContains(List.of(prefixes), str), res);
  else
   Assert.assertThrows(exc, () -> Utilities.prefixListContains(List.of(prefixes), str));

 }



 /**
  * Test splitIssuers().
  */

 @DataProvider(name = "someIssuerList")
 public static Object[][] getSomeIssuerList() {

  return new Object[][]{
   {"test1&test2",   new String[] {"test1", "test2"}},
   {"test1 &test2",  new String[] {"test1", "test2"}},
   {"test1 & test2", new String[] {"test1", "test2"}},
   {"test1", new String[] {"test1"}},
   {"test1&", new String[] {"test1"}},
   {"&test1", new String[] {"test1"}},
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "someIssuerList")
 public static void testSplitIssuers(String issuers, String[] issuersList) {

  List<String> is = Utilities.splitIssuers(issuers);

  Assert.assertEquals(is.size(), issuersList.length);

  for (int id = 0; id < issuersList.length; id ++)
   Assert.assertEquals(is.get(id), issuersList[id]);

 }



 /**
  * Tests testremoveAccents.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public static void testRemoveAccents() {

  String sd = Utilities.removeAccents("ÄÖÜäöüāăąēîïĩíĝġńñšŝśûůŷ");

  Assert.assertEquals(sd, "AOUaouaaaeiiiiggnnsssuuy");

 }



 /**
  * Tests splitSearchString.
  */

 @DataProvider(name = "getSomeSearchStrings")
 public static Object[][] getSomeSearchStrings() {

  return new Object[][]{
   {"Roland Kaiser",   new String[] {"roland", "kaiser"}},
   {"Paquito D'Rivera",   new String[] {"paquito", "d'rivera"}},
   {"James \"Fat Head\" Jones", new String[] {"james", "fat", "head", "jones"}},
   {"James 'Fat Head' Jones", new String[] {"james", "fat", "head", "jones"}},
   {"James \"Fat Head' Jones", new String[] {"james", "fat", "head", "jones"}},
   {"James 'Fat Head\" Jones", new String[] {"james", "fat", "head", "jones"}},
   {"Danielak, Stefan (Wildschwein) ", new String[] {"danielak", "stefan", "wildschwein"}},
   {"Maier-Müller, Karl", new String[] {"maier", "müller", "karl"}},
   {"Groß, Bertram", new String[] {"groß", "bertram"}},
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "getSomeSearchStrings")
 public static void splitSearchString(String searchString, String[] result){

  List<String> whatWeGot = Utilities.splitSearchString(searchString);

  for (int id = 0; id < result.length; id ++)
   Assert.assertEquals(whatWeGot.get(id), result[id]);

 }




 @DataProvider(name = "getSomeStringArrays")
 public static Object[][] getSomeStringArrays() {

  return new Object[][]{
   {"IST",    new String[] {"ROLAND", "KAISER", "IST", "HEISER"}, true},
   {"KAI.*",  new String[] {"ROLAND", "KAISER", "IST", "HEISER"}, true},
   {".*SER$", new String[] {"ROLAND", "KAISER", "IST", "HEISER"}, true},
   {".*SER$", new String[] {"REGEX", "IS", "NOT", "INCLUDED"},    false},
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "getSomeStringArrays")
 public static void testContainsMatch(String searchString, String[] searchStrings, boolean expectedResult){

  List<String> sstrs = Arrays.asList(searchStrings);

  Assert.assertEquals(Utilities.containsMatch(sstrs, searchString), expectedResult);

 }

}
