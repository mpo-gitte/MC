package core.util;

import org.testng.Assert;
import org.testng.annotations.Test;



/**
 * Tests the FillingArrayList.
 *
 * @version 2021-12-08
 * @author pilgrim.lutz@imail.de
 */

public class TestFillingArrayList {



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testFillingArrayList() {

  FillingArrayList<String> fal = new FillingArrayList<>();

  fal.setFilled(9, "Test");

  // Because the FillingArrayList fills the list the size should be 10 now.

  Assert.assertEquals(fal.size(), 10);

  // The items 0 to 8 should be null.

  for (int id = 0; id < 9; id ++)
   Assert.assertNull(fal.get(id));

  // Item 9 is the set item --- of course!

  Assert.assertEquals(fal.get(9), "Test");

 }

}
