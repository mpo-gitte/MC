package core.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Enumeration;
import java.util.Random;



/**
 * Test the SecretService and his agents.
 *
 * @version 2025-02-08
 * @author pilgrim.lutz@imail.de
 *
 */

public class TestSecretService {

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testEncryptionDecryption() {

  SecretService mi6 = SecretService.get();

  Enumeration<String> purposes = mi6.getPurposes();


  // Check all agents for all purposes.

  while (purposes.hasMoreElements()) {

   String purpose = purposes.nextElement();

   String text =
    RandomStringUtils.secure().nextAlphanumeric((new Random()).nextInt(100) + 100);

   SecretService.IAgent bond = mi6.getAgent(purpose);

   String secret = bond.encrypt(text);  // Encrypt text with agent.

   if (purpose.startsWith("bean.")) {
    // CryptKey for Beans.
    Assert.assertEquals(text, bond.decrypt(secret), "Encryption / Decryption failed!");
   }
   else if (purpose.startsWith("password.")) {
    // Password hashing.
    Assert.assertTrue(bond.check(text, secret), "Hashing failed failed!");
   }

  }

 }

}
