--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de



-- Create table for test sub bean

create table subbean (
 id integer,
 created timestamp,
 teststring varchar(200),
 primary key (id)
);



-- And a sequence

create sequence subbean_id start with 2;



-- Create table for test master bean

create table masterbean (
 id integer,
 created timestamp,
 teststring varchar(200),
 sub_id integer not null,
 primary key (id)
-- foreign key (sub_id) references subbean (id)
);



-- And a sequence

create sequence masterbean_id start with 2;



-- Insert some test data

insert into subbean (id, created, teststring)
 values (1, '2020-01-19 14:30:00.00000',  'Sub1');
insert into masterbean (id, created, teststring, sub_id)
 values (1, '2020-01-19 14:30:00.00000',  'Test1', 1);
