--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de



-- Create table for test bean

create table dataTypeMapping (
 id integer,
 created timestamp,
 teststring varchar(200),
 testdate date,
 testint integer,
 primary key (id)
);



-- And a sequence

create sequence dataTypeMapping_id start with 3;



-- Insert some test data

insert into dataTypeMapping (id, created, teststring, testdate, testint)
 values (1, '2020-01-19 14:30:00.00000',  'Test1', '2020-01-19', 1);

insert into dataTypeMapping (id, created, teststring, testdate, testint)
 values (2, '2020-01-19 14:30:00.00000',  'Test äöüÄÖÜß', '2020-01-19', 2);
