--liquibase formatted sql
--changeset author:lp



-- Create table for bean

create table testjdbcdatastorebean (
 id integer,
 prop1 varchar(200),
 primary key (id)
);

insert into testjdbcdatastorebean (id, prop1)
 values (1, 'Prop1-1');
insert into testjdbcdatastorebean (id, prop1)
 values (2, 'Prop1-2');
insert into testjdbcdatastorebean (id, prop1)
 values (3, 'Prop1-3');
insert into testjdbcdatastorebean (id, prop1)
 values (4, 'Prop1-4');
insert into testjdbcdatastorebean (id, prop1)
 values (5, 'Prop1-5');
insert into testjdbcdatastorebean (id, prop1)
 values (6, 'Prop1-6');
insert into testjdbcdatastorebean (id, prop1)
 values (7, 'Prop1-7');
insert into testjdbcdatastorebean (id, prop1)
 values (8, 'Prop1-8');
insert into testjdbcdatastorebean (id, prop1)
 values (9, 'Prop1-9');
insert into testjdbcdatastorebean (id, prop1)
 values (10, 'Prop1-10');
