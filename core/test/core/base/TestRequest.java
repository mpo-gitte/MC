package core.base;

import core.tools.mock.HttpServletRequest;
import inet.ipaddr.HostName;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import setup.Common;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;



/**
 * Tests for the Request tool.
 *
 * @version 2023-09-24
 * @author lp
 */

public class TestRequest {

 @DataProvider(name = "browsers")
 public static Object[][] getBrowsers() {
  return new Object[][] {
   // Firefox < V.63 does not work
   {"Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0", Boolean.FALSE},
   {"Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/63.0", Boolean.TRUE},
   {"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.96 Safari/537.36", Boolean.TRUE},
   {"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134", Boolean.FALSE},
   // Chrome > 115 has some issues.
   {"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36", Boolean.FALSE}
  };
 }



 /**
  * Tests isFriendlyBrowser().
  *
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "browsers")
 public void testIsFriendlyBrowser(Object browser, Object isFriendly) {

  jakarta.servlet.http.HttpServletRequest httpreq =
   new HttpServletRequest()
    .withHeader(
     "User-Agent",
     browser.toString()
    );

  Request req = new Request().withHttpServletRequest(httpreq);

  Assert.assertEquals(req.isFriendlyBrowser(), ((Boolean)isFriendly).booleanValue());

 }



 /**
  * Tests getHostFromHeader()
  */

 @DataProvider(name = "hostHeaders")
 public static Object[][] getHostHeaders() throws SocketException {

  List<String> ourNames = new ArrayList<>();

  Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();

  while (interfaces.hasMoreElements()) {
   NetworkInterface ni = interfaces.nextElement();
   Enumeration<InetAddress> ias = ni.getInetAddresses();
   while (ias.hasMoreElements()) {
    InetAddress ia = ias.nextElement();
    if (!ia.isLinkLocalAddress()) {
     String sd = new HostName(ia.getHostAddress()).getHost();  // Normalize.
     if (!ourNames.contains(sd))
      ourNames.add(sd);
     sd = new HostName(ia.getHostName()).getHost();  // Normalize
     if (!ourNames.contains(sd))
      ourNames.add(sd);
    }
   }
  }

  Object[][] res = new Object[ourNames.size() * 2][2];

  int id = 0;
  for (String n : ourNames) {
   res[id ++] = new Object[]{"Host", n};
   res[id ++] = new Object[]{"X-Forwarded-Host", n};
  }

  return res;

 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "hostHeaders")
 public void testGetHostFromHeader(String hostHeader, String calledAs) {

  jakarta.servlet.http.HttpServletRequest httpreq =
   new HttpServletRequest()
    .withHeader(
     hostHeader,
     calledAs
    );

  Request req = new Request().withHttpServletRequest(httpreq);

  Assert.assertEquals(req.getHostFromHeader(), calledAs);

 }



 /**
  * Tests getParameter()
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testGetParameter() {

  jakarta.servlet.http.HttpServletRequest httpreq =
   new HttpServletRequest()
    .withParameter(
     "parameter1",
     "value1"
    );

  Request req = new Request().withHttpServletRequest(httpreq);

  Assert.assertEquals(req.getParameter("parameter1"), "value1");

  Assert.assertNull(req.getParameter("parameter2"));  // Was never set.

  Assert.assertEquals(req.getParameter("parameter3", "value3"), "value3");  // Was never set, too. But default value should be returned.

 }



 /**
  * Tests getUriPlain().
  */

 @DataProvider(name = "urlsToTest")
 public static Object[][] getDatesToFormat() {
  return new Object[][] {
   {"http://www.somewhere.com/check", "http://www.somewhere.com/check"},
   {"http://www.somewhere.com:8080/check", "http://www.somewhere.com:8080/check"},
   {"http://www.somewhere.com/check;param=test;jsessionid=4711", "http://www.somewhere.com/check"},
   {"http://www.somewhere.com:8080/check;param=test;jsessionid=4711", "http://www.somewhere.com:8080/check"},
   {"http://www.somewhere.com:8080/check/test.vm;param=test;jsessionid=4711", "http://www.somewhere.com:8080/check/test.vm"},
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "urlsToTest")
 public void testgGetUriPlain(String uri, String plainUri) {

  jakarta.servlet.http.HttpServletRequest httpreq =
   new HttpServletRequest()
    .withUri(uri);

  Request req = new Request().withHttpServletRequest(httpreq);

  Assert.assertEquals(req.getUriPlain(), plainUri);

 }



 /**
  * Tests getTemplatePath().
  */

 @DataProvider(name = "urlsToTestWithTemplate")
 public static Object[][] getUrlsToTestWithTemplate() {
  return new Object[][] {
   {"http://www.somewhere.com:8080/check/test.vm;param=test;jsessionid=4711", "/test.vm"},
   {"http://www.somewhere.com:8080/check/;param=test;jsessionid=4711", "/"},
   {"http://www.somewhere.com:8080/check;param=test;jsessionid=4711", ""},
   {"http://www.somewhere.com:8080/check/test/test.vm;param=test;jsessionid=4711", "/test/test.vm"},
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "urlsToTestWithTemplate")
 public void testGetTemplatePath(String uri, String templatePath) {

  jakarta.servlet.http.HttpServletRequest httpreq =
   new HttpServletRequest()
    .withUri(uri);

  Request req = new Request().withHttpServletRequest(httpreq);

  Assert.assertEquals(req.getTemplatePath(), templatePath);

 }



 /**
  * Tests getUriParameters().
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testgGetUriParmeters() {

  jakarta.servlet.http.HttpServletRequest httpreq =
   new HttpServletRequest()
    .withUri("http://www.somewhere.com/check;param=test;jsessionid=4711");

  Request req = new Request().withHttpServletRequest(httpreq);

  Assert.assertEquals(req.getUriParameters(), "param=test;jsessionid=4711");

  httpreq =
   new HttpServletRequest()
    .withUri("http://www.somewhere.com/check;");  // No parameters only semicolon.

  req = new Request().withHttpServletRequest(httpreq);

  Assert.assertNull(req.getUriParameters());

  httpreq =
   new HttpServletRequest()
    .withUri("http://www.somewhere.com/check");  // Absolutely nothing.

  req = new Request().withHttpServletRequest(httpreq);

  Assert.assertNull(req.getUriParameters());

 }



 /**
  * Tests getUriParameter().
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testgGetUriParmeter() {

  jakarta.servlet.http.HttpServletRequest httpreq =
   new HttpServletRequest()
    .withUri("http://www.somewhere.com/check;param=test;jsessionid=4711");

  Request req = new Request().withHttpServletRequest(httpreq);

  String p = req.getUriParameter("param", "blah");
  Assert.assertEquals(p, "test");

  p = req.getUriParameter("jsessionid", "blah");
  Assert.assertEquals(p, "4711");

  p = req.getUriParameter("isntthere", "blah");
  Assert.assertEquals(p, "blah");

 }



 /**
  * Tests stripJvmRouteFromSessionId()
  */

 @DataProvider(name = "sessionIdsToTest")
 public static Object[][] getSessionIds() {
  return new Object[][] {
   {"123456ssdvcvef44t", "123456ssdvcvef44t"},
   {"123456ssdvcvef44t.jvmroute", "123456ssdvcvef44t"},
   {".jvmroute", ""},
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "sessionIdsToTest")
 public void testStripJvmRouteFromSessionId(String sessionIdJR, String resultingSessionId) {

  Assert.assertEquals(Request.stripJvmRouteFromSessionId(sessionIdJR), resultingSessionId);

 }



 /**
  * Tests getIpAddress().
  */

 @DataProvider(name = "remoteCalls")
 public static Object[][] getRemoteCallers() {
  return new Object[][] {
   // Some host. Address will be used.
   {"42.42.42.42", null,                      null,                    "42.42.42.42"},
   // Known reverse proxy (see Request.properties). Hasn't set X-Forwarded-For. Exception will be thrown.
   {"172.17.0.42", null,                      SecurityException.class, "1.1.1.1"},
   // Known reverse proxy. X-Forwarded-For header set. Address in header will be used
   {"172.17.0.42", "2.2.2.2",                 null,                    "2.2.2.2"},
   // Know reverse proxy. But header contains a list of IPs. Scan list from the end. First unknown address is returned.
   {"172.17.0.42", "2.2.2.2, 172.17.0.42",     null,                    "2.2.2.2"},
   // Know reverse proxy. Header contains a list of IPs. No unknown address included.
   {"172.17.0.42", "172.17.0.42,172.17.0.42", SecurityException.class,                    "2.2.2.2"}
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "remoteCalls")
 public void testGetIpAddress(String remoteAddress, String xForwardedFor, Class<? extends Throwable> throwException, String remoteAddressToTest) {

  Request req = new Request().withHttpServletRequest(
   new HttpServletRequest()
    .withRemoteAddress(
     remoteAddress
    )
    .withHeader(
     "X-Forwarded-For",
     xForwardedFor
    )
  );

  if (!Utilities.isEmpty(throwException))
   Assert.assertThrows(throwException, req::getIpAddress);
  else
   Assert.assertEquals(req.getIpAddress(), remoteAddressToTest);

 }



 /**
  * Tests getAppPath().
  */

 @DataProvider(name = "getAppPaths")
 public static Object[][] getAppPathes() {
  return new Object[][] {
   {"http://www.somewhere.com/contextpath",             null,          null,                    "contextpath"},
   {"www.somewhere.com/contextpath",                    null,          null,                    "contextpath"},
   {"http://www.somewhere.com/contextpath/test.vm&t=1", null,          null,                    "contextpath"},
   {"http://www.somewhere.com/contextpath/test.vm&t=1", "/allowed1",   null,                    "allowed1"},
   {"http://www.somewhere.com/contextpath/test.vm&t=1", "/allowed2",   null,                    "allowed2"},
   {"http://www.somewhere.com/contextpath/test.vm&t=1", "/notallowed", SecurityException.class, "ehwurscht"},
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "getAppPaths")
 public void testGetAppPath(String url, String xForwardedContextPath, Class<? extends Throwable> throwException, String result) {

  Request req = new Request().withHttpServletRequest(
   new HttpServletRequest()
    .withUri(
     url
    )
    .withHeader(
     Request.MC_FORWARDED_CONTEXT_PATH,
     xForwardedContextPath
    )
  );

  if (!Utilities.isEmpty(throwException))
   Assert.assertThrows(throwException, req::getAppPath);
  else
   Assert.assertEquals(req.getAppPath(), result);

 }

}
