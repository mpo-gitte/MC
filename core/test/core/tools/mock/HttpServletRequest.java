package core.tools.mock;

import jakarta.servlet.*;
import jakarta.servlet.http.*;

import java.io.BufferedReader;
import java.security.Principal;
import java.util.*;



/**
 * A Mock for the HttpServletRequest.
 *
 * @version 2025-02-15
 * @author lp
 */

public class HttpServletRequest implements jakarta.servlet.http.HttpServletRequest {

 private final Map<String, String> headerMap = new HashMap<>();
 private String uri = null;
 private final Map<String, String> parameterMap = new HashMap<>();
 private String remoteAddress = null;



 @Override
 public String getAuthType() {

  return null;
 }



 @Override
 public Cookie[] getCookies() {

  return new Cookie[0];

 }



 @Override
 public long getDateHeader(String s) {

  return 0;

 }



 /**
  * Returns a header which is set with withHeader().
  *
  * @param name The name of the header.
  * @return The value for the header.
  *
  * @see HttpServletRequest#withHeader(String, String)
  */

 @Override
 public String getHeader(String name) {

  return headerMap.get(name);

 }



 public HttpServletRequest withHeader(String name, String value) {

  headerMap.put(name, value);

  return this;

 }



 @Override
 public Enumeration<String> getHeaders(String s) {

  return null;
 }



 @Override
 public Enumeration<String> getHeaderNames() {

  return null;

 }



 @Override
 public int getIntHeader(String s) {

  return 0;

 }



 @Override
 public String getMethod() {

  return null;

 }



 @Override
 public String getPathInfo() {

  return null;

 }



 @Override
 public String getPathTranslated() {

  return null;

 }



 /**
  * Returns the context path of the URL.
  *
  * <p>This implementation can only handle very simple URLs.</p>
  *
  * @return The part of the URL between hostname and app data.
  */

 @Override
 public String getContextPath() {

  int ic;

  String sd = uri.replaceAll(";.*$", "");  // Strip URL params.

  if (sd.startsWith("http://"))
   ic = 3;  // Has a scheme.
  else
   ic = 1;

  String[] parts = sd.split("/");

  return "/" + parts[ic];  // Context paths always start with a slash.

 }



 @Override
 public String getQueryString() {

  return null;

 }



 @Override
 public String getRemoteUser() {

  return null;

 }



 @Override
 public boolean isUserInRole(String s) {

  return false;

 }



 @Override
 public Principal getUserPrincipal() {

  return null;

 }



 @Override
 public String getRequestedSessionId() {

  return null;

 }



 /**
  * Returns the URI set with withUri().
  *
  * @return The URI.
  */

 @Override
 public String getRequestURI() {

  return uri;

 }



 public HttpServletRequest withUri(String uri) {

  this.uri = uri;

  return this;

 }



 @Override
 public StringBuffer getRequestURL() {

  return null;

 }



 @Override
 public String getServletPath() {

  return null;

 }



 @Override
 public HttpSession getSession(boolean b) {

  return null;

 }



 @Override
 public HttpSession getSession() {

  return null;

 }



 @Override
 public boolean isRequestedSessionIdValid() {

  return false;

 }



 @Override
 public boolean isRequestedSessionIdFromCookie() {

  return false;

 }



 @Override
 public boolean isRequestedSessionIdFromURL() {

  return false;

 }



 @Override
 public Object getAttribute(String s) {

  return null;

 }



 @Override
 public Enumeration<String> getAttributeNames() {

  return null;

 }



 @Override
 public String getCharacterEncoding() {

  return null;

 }



 @Override
 public void setCharacterEncoding(String s) {

 }



 @Override
 public int getContentLength() {

  return 0;

 }



 @Override
 public String getContentType() {

  return null;

 }



 @Override
 public ServletInputStream getInputStream() {

  return null;

 }



 @Override
 public String getParameter(String parameter) {

  return parameterMap.get(parameter);

 }



 public HttpServletRequest withParameter(String parameter, String value) {

  parameterMap.put(parameter, value);

  return this;

 }



 @Override
 public Enumeration<String> getParameterNames() {

  return null;

 }



 @Override
 public String[] getParameterValues(String s) {

  return new String[0];

 }



 @Override
 public Map<String, String[]> getParameterMap() {

  return null;

 }



 @Override
 public String getProtocol() {

  return null;

 }



 @Override
 public String getScheme() {

  return null;

 }



 @Override
 public String getServerName() {

  return null;

 }



 @Override
 public int getServerPort() {

  return 0;

 }



 @Override
 public BufferedReader getReader() {

  return null;

 }



 /**
  * Return the remote address set with withRemoteAddress().

  * @return The remote address.
  */

 @Override
 public String getRemoteAddr() {

  return remoteAddress;

 }



 /**
  * Set the remote address.
  *
  * @param remoteAddress The remote address to be returned by getRemoteAddress()-
  *
  * @return This HttpServletRequest.
  */

 public HttpServletRequest withRemoteAddress(String remoteAddress) {

  this.remoteAddress = remoteAddress;

  return this;

 }




 @Override
 public String getRemoteHost() {

  return null;

 }



 @Override
 public void setAttribute(String s, Object o) {

 }



 @Override
 public void removeAttribute(String s) {

 }



 @Override
 public Locale getLocale() {

  return null;

 }



 @Override
 public Enumeration<Locale> getLocales() {

  return null;

 }



 @Override
 public boolean isSecure() {

  return false;

 }



 @Override
 public RequestDispatcher getRequestDispatcher(String s) {

  return null;

 }



 @Override
 public int getRemotePort() {

  return 0;

 }



 @Override
 public String getLocalName() {

  return null;

 }



 @Override
 public String getLocalAddr() {

  return null;

 }



 @Override
 public int getLocalPort() {

  return 0;

 }



 @Override
 public <T extends HttpUpgradeHandler> T upgrade(Class<T> httpUpgradeHandlerClass) {

  return null;

 }



 @Override
 public String changeSessionId() {

  return null;

 }



 @Override
 public boolean authenticate(HttpServletResponse response) {

  return false;

 }



 @Override
 public void login(String username, String password) {

 }



 @Override
 public void logout() {

 }



 @Override
 public Collection<Part> getParts() {

  return null;

 }



 @Override
 public Part getPart(String name) {

  return null;

 }



 @Override
 public long getContentLengthLong() {

  return 0;

 }



 @Override
 public ServletContext getServletContext() {

  return null;

 }



 @Override
 public AsyncContext startAsync() throws IllegalStateException {

  return null;

 }



 @Override
 public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) throws IllegalStateException {

  return null;

 }



 @Override
 public boolean isAsyncStarted() {

  return false;

 }



 @Override
 public boolean isAsyncSupported() {

  return false;

 }



 @Override
 public AsyncContext getAsyncContext() {

  return null;

 }



 @Override
 public DispatcherType getDispatcherType() {

  return null;

 }



 /**
  * Obtain a unique (within the lifetime of the Servlet container) identifier
  * string for this request.
  * <p>
  * There is no defined format for this string. The format is implementation
  * dependent.
  *
  * @return A unique identifier for the request
  * @since Servlet 6.0
  */
 @Override
 public String getRequestId() {

  return null;
 }



 /**
  * Obtain the request identifier for this request as defined by the protocol
  * in use. Note that some protocols do not define such an identifier.
  * <p>
  * Examples of protocol provided request identifiers include:
  * <dl>
  * <dt>HTTP 1.x</dt>
  * <dd>None, so the empty string should be returned</dd>
  * <dt>HTTP 2</dt>
  * <dd>The stream identifier</dd>
  * <dt>HTTP 3</dt>
  * <dd>The stream identifier</dd>
  * <dt>AJP</dt>
  * <dd>None, so the empty string should be returned</dd>
  * </dl>
  *
  * @return The request identifier if one is defined, otherwise an empty
  * string
  * @since Servlet 6.0
  */
 @Override
 public String getProtocolRequestId() {

  return null;
 }



 /**
  * Obtain details of the network connection to the Servlet container that is
  * being used by this request. The information presented may differ from
  * information presented elsewhere in the Servlet API as raw information is
  * presented without adjustments for, example, use of reverse proxies that
  * may be applied elsewhere in the Servlet API.
  *
  * @return The network connection details.
  * @since Servlet 6.0
  */
 @Override
 public ServletConnection getServletConnection() {

  return null;
 }



 /**
  * Obtain the mapping information for this request.
  *
  * @return the mapping information for this request
  */
 @Override
 public HttpServletMapping getHttpServletMapping() {

  return jakarta.servlet.http.HttpServletRequest.super.getHttpServletMapping();
 }



 /**
  * Obtain a Map of the trailer fields that is not backed by the request
  * object.
  *
  * @return A Map of the received trailer fields with all keys lower case
  * or an empty Map if no trailers are present
  * @since Servlet 4.0
  */
 @Override
 public Map<String, String> getTrailerFields() {

  return jakarta.servlet.http.HttpServletRequest.super.getTrailerFields();
 }



 /**
  * Are trailer fields ready to be read (there may still be no trailers to
  * read). This method always returns {@code true} if the underlying protocol
  * does not support trailer fields. Otherwise, {@code true} is returned once
  * all the following are true:
  * <ul>
  * <li>The application has ready all the request data and an EOF has been
  *     received or the content-length is zero</li>
  * <li>All trailer fields, if any, have been received</li>
  * </ul>
  *
  * @return {@code true} if trailers are ready to be read
  * @since Servlet 4.0
  */
 @Override
 public boolean isTrailerFieldsReady() {

  return jakarta.servlet.http.HttpServletRequest.super.isTrailerFieldsReady();
 }

}
