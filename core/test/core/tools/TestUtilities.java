package core.tools;

import core.base.Utilities;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import setup.Common;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;


/**
 * Test methods for utilities tool.
 *
 * @version 2021-01-04
 * @author pilgrim.lutz@imail.de
 */

public class TestUtilities {



 /**
  * Test formatDate.
  */

 @DataProvider(name = "datesToFormat")
 public static Object[][] getDatesToFormat() {
  return new Object[][] {
   {"2019-12-08T01:23:10", "DE", "08.12.2019, 01:23:10", "08.12.2019"},
   {"2019-12-08T13:23:10", "DE", "08.12.2019, 13:23:10", "08.12.2019"},
   {"2019-12-08T01:23:10", "EN", "Dec 8, 2019, 1:23:10 AM", "Dec 8, 2019"},
   {"2019-12-08T13:23:10", "EN", "Dec 8, 2019, 1:23:10 PM", "Dec 8, 2019"},
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "datesToFormat")
 public void formatDate(String ts, String locConst, String res, String resWt) throws ParseException {

  LocalDateTime d = LocalDateTime.parse(ts, DateTimeFormatter.ISO_DATE_TIME);

  Locale loc = new Locale(locConst);

  String sd = Utilities.formatDate(d, loc);

  Assert.assertEquals(sd, res);

 }



 /**
  * Test formatDateWithoutTime.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "datesToFormat")
 public void formatDateWithoutTime(String ts, String locConst, String res, String resWt) throws ParseException {

  LocalDateTime d = LocalDateTime.parse(ts, DateTimeFormatter.ISO_DATE_TIME);

  Locale loc = new Locale(locConst);

  String sd = Utilities.formatDateWithoutTime(d, loc);

  Assert.assertEquals(sd, resWt);

 }



 /**
  * Test formatDate for LocalDate.
  */

 @DataProvider(name = "localDatesToFormat")
 public static Object[][] getLocalDatesToFormat() {
  return new Object[][] {
   {"2019-12-08", "DE", "08.12.2019"},
   {"2019-12-08", "EN", "Dec 8, 2019"}
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "localDatesToFormat")
 public void formatLocalDate(String ts, String locConst, String res) {

  LocalDate d = LocalDate.parse(ts, DateTimeFormatter.ISO_DATE);

  Locale loc = new Locale(locConst);

  String sd = Utilities.formatDate(d, loc);

  Assert.assertEquals(sd, res);

 }



 /**
  * Test formatDouble
  */

 @DataProvider(name = "doublesToFormat")
 public static Object[][] getDoublesToFormat() {
  return new Object[][] {
   {1.    , "DE", "%2.2f", "1,00"},
   {10000., "DE", "%5.2f", "10000,00"},
   {1.    , "EN", "%2.2f", "1.00"},
   {10000., "EN", "%5.2f", "10000.00"},
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "doublesToFormat")
 public void formatDouble(Double d, String locConst, String formatString, String res) throws ParseException {

  Locale loc = new Locale(locConst);

  String sd = Utilities.formatDouble(d, loc, formatString);

  Assert.assertEquals(sd, res);

 }



 /**
  * Test formatBoolean
  */

 @DataProvider(name = "booleansToFormat")
 public static Object[][] getBooleansToFormat() {
  return new Object[][] {
   {Boolean.TRUE, "true"},
   {Boolean.FALSE, "false"},
   {null, "false"},
  };
 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "booleansToFormat")
 public void formatBoolean(Boolean b, String res) {

  String sd = Utilities.formatBoolean(b);

  Assert.assertEquals(sd, res);

 }



 /**
  * Test formatDuration
  */

 @DataProvider(name = "durationsToFormat")
 public static Object[][] getDurationsToFormat() {
  return new Object[][] {
   {0L,    "0:00:00"},
   {10L,   "0:00:10"},
   {310L,  "0:05:10"},
   {7200L, "2:00:00"},
   {7510L, "2:05:10"},
  };
 }
 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, dataProvider = "durationsToFormat")
 public void formatDurations(Long duration, String res) {

  String sd = Utilities.formatDuration(duration);

  Assert.assertEquals(sd, res);

 }

}
