package core.bakedBeans;



@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "prop1",
   name = "prop1",
   pclass = Integer.class
  ),
 }
)
@PersistentBeanDescription(
 source = "MemoryTestBean1",
 keyName = "id",
 keyMakerClass = SillySequenceKeyMaker.class
)



public class MemoryTestBean1 extends MemoryBean {

}
