package core.bakedBeans;

import core.bakedBeans.beans.ADifferentJDBCBean;
import core.bakedBeans.beans.AJDBCBean;
import core.bakedBeans.beans.AJDBCBeanWithACollection;
import core.bakedBeans.beans.AnotherJDBCBean;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;



/**
 * A Test for the JDBCBean.
 *
 * @version 2024-08-15
 * @author lp
 *
 * @see PersistentBean
 */

public class TestJDBCBean {


 /**
  * Tests JDBCBean.getFindSelect().
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testGetFindSelect() {

  // Set up a bean.

  AJDBCBean b = new AJDBCBean();

  b.set("prop1", 1);
  b.set("prop2", "p2");
  b.set("prop3", "p3");

  // Build the query statement.

  List<Object> values = new ArrayList<>();

  String sd = b.getFindSelect(new String[]{"prop1", "prop2", "prop3"}, values);

  Assert.assertEquals(
   sd,
   "select AJDBCSource.prop2,AJDBCSource.prop1,AJDBCSource.prop3 from AJDBCSource where prop1=? AND prop2=? AND prop3=?",
   "Query statement is false!"
  );

  Assert.assertEquals(values.get(0), 1, "Argument 1 is wrong!");
  Assert.assertEquals(values.get(1), "p2", "Argument 2 is wrong!");
  Assert.assertEquals(values.get(2), "p3", "Argument 3 is wrong!");

 }



 /**
  * Tests JDBCBean.getFindSelect().
  *
  * <p>Has one parameter whith value of null.</p>
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testGetFindSelectWithNull() {

  // Set up a bean.

  AJDBCBean b = new AJDBCBean();

  b.set("prop1", 1);
  b.set("prop2", null);
  b.set("prop3", "p3");

  // Build the query statement.

  List<Object> values = new ArrayList<>();

  String sd = b.getFindSelect(new String[]{"prop1", "prop2", "prop3"}, values);

  Assert.assertEquals(
   sd,
   "select AJDBCSource.prop2,AJDBCSource.prop1,AJDBCSource.prop3 from AJDBCSource where prop1=? AND prop2 is null AND prop3=?",
   "Query statement is false!"
  );

  Assert.assertEquals(values.get(0), 1, "Argument 1 is wrong!");
  Assert.assertEquals(values.get(1), "p3", "Argument 2 is wrong!");

 }



 /**
  * Tests getPropertyInfosToLoad()
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testGetPropertiesToLoad() {

  Map<String, PropertyInfo> props = JDBCSupport.getPropertyInfosToLoad(AnotherJDBCBean.class, false);

  Assert.assertEquals(props.size(), 5);

  Assert.assertTrue(props.containsKey("prop1"));
  Assert.assertTrue(props.containsKey("prop2"));
  Assert.assertTrue(props.containsKey("prop3"));
  Assert.assertTrue(props.containsKey("prop4"));
  Assert.assertTrue(props.containsKey("prop5"));

 }



 /**
  * Tests getPropertyInfosToLoad() without aggregate functions.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testGetPropertiesToLoadWithoutAggregateFunctions() {

  Map<String, PropertyInfo> props = JDBCSupport.getPropertyInfosToLoad(AnotherJDBCBean.class, true);

  Assert.assertEquals(props.size(), 4);

  Assert.assertTrue(props.containsKey("prop1"));
  Assert.assertTrue(props.containsKey("prop2"));
  Assert.assertTrue(props.containsKey("prop3"));
  Assert.assertTrue(props.containsKey("prop4"));  // prop4 has upper -> allowed
  Assert.assertFalse(props.containsKey("prop5")); // prop5 has max -> not allowed

 }



 /**
  * Tests building a query string for a JDBCBean.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testGetLoadQueryString() {

  AnotherJDBCBean ab = new AnotherJDBCBean();

  String sd = ab.getLoadQueryString();

  // prop5 is not included because it has an aggregate function which is suppressed for normal bean loading.
  Assert.assertEquals(sd, "select sourcetable.prop2,sourcetable.prop1,upper(sourcetable.prop4) prop4,sourcetable.prop3,subbean.teststring ar$teststring,subbean.created ar$created,subbean.id ar$id from sourcetable,subbean where subbean.id=sourcetable.prop1 and sourcetable.prop1=?");

 }



 /**
  * Tests building a query string for a JDBCBean.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testGetLoadQueryStringAlt() {

  ADifferentJDBCBean ab = new ADifferentJDBCBean();

  String sd = ab.getLoadQueryString();

  // Note: The "teststring" property isn't included because it is not specified in ADifferentJDBCBean!

  Assert.assertEquals(sd, "select sourcetable.prop2,sourcetable.prop1,sourcetable.prop3,subbean.created ar$created,subbean.id ar$id from sourcetable,subbean where subbean.id=sourcetable.prop1 and sourcetable.prop1=?");

 }



 /**
  * Tests building a query string for a JDBCBean with a collection.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testGetLoadQueryStringWithOrder() {

  AJDBCBeanWithACollection ab = new AJDBCBeanWithACollection();

  // Get the query string for the collection.
  String query = ab.getQuery(ab.getPropertyInfo("collection"));

  Assert.assertEquals(query, "select AJDBCSource.prop2,AJDBCSource.prop1 from AJDBCSource where AJDBCSource.prop1=? order by AJDBCSource.prop1, AJDBCSource.prop2 desc");

 }

}
