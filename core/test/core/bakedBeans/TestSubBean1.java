package core.bakedBeans;



/**
 * A test bean for the hierarchical save test.
 *
 * @version 2018-02-25
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "topid",
   name = "topid",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "prop1",
   name = "prop1",
   pclass = Integer.class
  ),
 }
)
@PersistentBeanDescription(
 source = "TestSubBean1",
 keyName = "id",
 keyMakerClass = SillySequenceKeyMaker.class
)



public class TestSubBean1 extends MemoryBean {

}
