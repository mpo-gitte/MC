package core.bakedBeans;

import core.bakedBeans.beans.MasterBeanWithoutStore;
import core.bakedBeans.beans.SubBeanWithoutStore;
import org.testng.Assert;
import org.testng.annotations.Test;
import setup.Common;

import java.util.Arrays;


/**
 * Tests BeanList
 *
 * @version 2023-09-07
 * @author pilgrim.lutz@imail.de
 */

public class TestBeanList {


 /**
  * Tests creating a BeanList from a BeanMap.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testsCreatingABeanListFromABeanMap() {

  // Prepare a BeanMap.
  BeanMap<MasterBeanWithoutStore> bm = new BeanMap<>(MasterBeanWithoutStore.class, "id");
  MasterBeanWithoutStore mb = new MasterBeanWithoutStore();
  mb.set("id", Integer.valueOf(1));
  mb.set("teststring", "Test1");
  bm.put(mb);
  mb = new MasterBeanWithoutStore();
  mb.set("id", Integer.valueOf(2));
  mb.set("teststring", "Test2");
  bm.put(mb);
  mb = new MasterBeanWithoutStore();
  mb.set("id", Integer.valueOf(3));
  mb.set("teststring", "Test3");
  bm.put(mb);

  // Build the list to test.
  BeanList<MasterBeanWithoutStore> bl = new BeanList<>(MasterBeanWithoutStore.class, bm);

  // Length ok?
  Assert.assertEquals(bl.size(), 3, "Length wrong!");

  // Then check the content.
  int[] ints = {1, 2, 3};

  for (MasterBeanWithoutStore b : bl) {
   int id = (Integer) b.get("id");
   Assert.assertTrue(Arrays.stream(ints).anyMatch(i -> i == id));
  }

 }

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testsContainsAsSubBean() {

  // Build the list to test.
  BeanList<MasterBeanWithoutStore> bl = new BeanList<>(MasterBeanWithoutStore.class);
  MasterBeanWithoutStore mb = new MasterBeanWithoutStore();
  mb.set("id", Integer.valueOf(1));
  mb.set("teststring", "Test1");
  bl.add(mb);
  mb = new MasterBeanWithoutStore();
  mb.set("id", Integer.valueOf(2));
  mb.set("teststring", "Test2");
  bl.add(mb);
  mb = new MasterBeanWithoutStore();
  mb.set("id", Integer.valueOf(3));
  mb.set("teststring", "Test3");
  mb.set("subId", Integer.valueOf(0));
  SubBeanWithoutStore sb = new SubBeanWithoutStore();  // The MasterBean gets a SubBean.
  sb.set("id", Integer.valueOf(0));
  sb.set("teststring", "test_sb");
  mb.set("subBean", sb);
  bl.add(mb);

  // Prepare a SubBean to search for in the BeanList.
  SubBeanWithoutStore searchBean = new SubBeanWithoutStore();
  searchBean.set("id", 0);

  // Search for it.
  Assert.assertTrue(bl.containsAsSubBean("subBean", searchBean));

 }

}
