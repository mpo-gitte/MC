package core.bakedBeans;



/**
 * A DataStore for test purposes with a special signal method.
 *
 * @version 2022-12-16
 * @author pilgrim.lutz@imail.de
 *
 * @param <T> Type of bean this DataStore holds.
 */

public class TestSignalResetDataStore<T extends MemoryBean> extends TestMemoryBeanDataStore<T> {



 @Override
 public void reset() {

  TestDataStoreManager.stamps.add("TestSignalDataStore.signal() for " + maker.getCategory());

  // Reset the DataStore.
  super.reset();

 }

}
