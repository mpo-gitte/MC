package core.bakedBeans;



@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "prop1",
   name = "prop1",
   pclass = String.class
  ),
 }
)
@PersistentBeanDescription(
 source = "MemoryTestBean2",
 keyName = "id",
 keyMakerClass = SillySequenceKeyMaker.class
)



public class MemoryTestBean2 extends MemoryBean {

}
