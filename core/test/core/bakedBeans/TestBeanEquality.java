package core.bakedBeans;

import core.bakedBeans.beans.TestEqualityBean;
import org.testng.Assert;
import org.testng.annotations.Test;


/**
 * Testing bean equality and bean hash code.
 *
 * @version 2024-10-27
 * @author lp
 */

public class TestBeanEquality {


 /**
  * Test if two bean are equal.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testEquality() {

  Bean b1 = new TestEqualityBean();
  Bean b2 = new TestEqualityBean();

  Assert.assertEquals(b2, b1, "Empty beans are not equal!");

  b1.set("prop1", "value1");
  b1.set("prop2", 2);
  b2.set("prop1", "value1");
  b2.set("prop2", 2);

  Assert.assertEquals(b2, b1, "Identically filled beans are not equal!");

  Bean b3 = new TestEqualityBean();
  b3.set("prop2", 2);  // Bean filled intentionally in n different order.
  b3.set("prop1", "value1");

  Assert.assertEquals(b3, b1, "Identically (but in a different order) filled beans are not equal!");

  Bean b4 = new TestEqualityBean();
  b4.set("prop1", "value1");
  b4.set("prop2", 3);  // A distinct value!

  Assert.assertNotEquals(b4, b1, "Distinct beans are equal!");

 }



 /**
  * Test building the hash code of beans.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testHashCode() {

  Bean b1 = new TestEqualityBean();

  Assert.assertEquals(b1.hashCode(), 0, "Empty beans hash code isn't 0!");

  Bean b2 = new TestEqualityBean();

  b1.set("prop1", "value1");
  b1.set("prop2", 2);
  b2.set("prop1", "value1");
  b2.set("prop2", 2);

  Assert.assertEquals(b1.hashCode(), b2.hashCode(), "Identically filled beans don't have the same hash code!");

  Bean b3 = new TestEqualityBean();
  b3.set("prop2", 2);  // Bean filled intentionally in a different order.
  b3.set("prop1", "value1");

  Assert.assertEquals(b1.hashCode(), b3.hashCode(), "Identically (but in a different order) filled beans don't have the same hash code!");

  Bean b4 = new TestEqualityBean();
  b4.set("prop1", "value1");
  b4.set("prop2", 3);  // A distinct value!

  Assert.assertNotEquals(b1.hashCode(), b4.hashCode(), "Distinct beans have the same hash code!");

 }



 /**
  * Test building the hash code of beans containing BeanList s a property.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testHashCodeWithBeanList() {

  Bean b1 = new TestEqualityBean();

  BeanList<TestEqualityBean> bl1 = new BeanList<>(TestEqualityBean.class);

  for (int id = 0; id < 6; id++) {
   TestEqualityBean blb = new TestEqualityBean();
   blb.set("prop1", "sub value " + id);
   blb.set("prop2", id);
   bl1.add(blb);
  }

  b1.set("prop1", "value1");
  b1.set("prop2", 2);
  b1.set("prop3", bl1);

  Bean b2 = new TestEqualityBean();

  BeanList<TestEqualityBean> bl2 = new BeanList<>(TestEqualityBean.class);

  for (int id = 0; id < 6; id++) {
   TestEqualityBean blb = new TestEqualityBean();
   blb.set("prop1", "sub value " + id);
   blb.set("prop2", id);
   bl2.add(blb);
  }

  b2.set("prop1", "value1");
  b2.set("prop2", 2);
  b2.set("prop3", bl2);

  Assert.assertEquals(b1.hashCode(), b2.hashCode(), "Identically filled beans don't have the same hash code!");

 }

}
