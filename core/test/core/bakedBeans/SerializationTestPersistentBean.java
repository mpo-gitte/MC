package core.bakedBeans;

/**
 * This bean implements a PersistentBean for testing serialization.
 *
 * @version 2021-09-08
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   name = "prop1",
   pclass = String.class
  ),
  @PropertyDescription(
   name = "prop2",
   pclass = String.class
  ),
  @PropertyDescription(
   name = "prop3",
   alias = "Aprop3",
   pclass = String.class
  ),
 }
)
@PersistentBeanDescription(
 source = "SerializationPersistentBean",
 keyName = "id",
 keyMakerClass = SillySequenceKeyMaker.class
)



public class SerializationTestPersistentBean extends MemoryBean {

}
