package core.bakedBeans;



import core.bakedBeans.beans.TestJdbcDataStoreBean;
import core.util.FillingArrayList;
import org.testng.Assert;
import org.testng.annotations.Test;
import setup.Common;


/**
 * Tests a JDBCDataStore.
 *
 * @version 2023-10-26
 * @author lp
 */

public class TestJdbcDataStore {



 @Test(groups = {setup.Common.TEST_GROUP_WITH_DATABASE})
 void testSlicing() {

  DataStore<TestJdbcDataStoreBean> ds = DataStoreManager.get().dataStore(JDBCDataStore.class, "cat1")
   .query("select * from testjdbcdatastorebean order by id")
   .make(TestJdbcDataStoreBean.class);

  // Read some beans at the beginning.

  BeanList<TestJdbcDataStoreBean> res = ds.read(0, 3);
  String ident = ds.ident();
  long stamp = ds.getStamp();

  Assert.assertEquals(res.size(), 3, "Size wrong!");

  // Read some beans on the end.

  res = ds.read(7, 4);

  Assert.assertEquals(res.size(), 10, "Size wrong!");

  // Ident has to be the same!
  Assert.assertEquals(ident, ds.ident(), "Ident changed!");
  // But stamp should differ!
  long newStamp = ds.getStamp();
  Assert.assertNotEquals(stamp, newStamp, "Stamp remained the same!");

  // We produced a gap. Check it.

  Assert.assertTrue(
   checkContent(
    res,
    1, 2, 3, -1, -1, -1, -1, 8, 9, 10
   ), "Content wrong!");

  // Closing the gap.

  res = ds.read(3, 4);

  // Ident has to be the same!
  Assert.assertEquals(ident, ds.ident(), "Ident changed!");
  // But stamp should differ!
  Assert.assertNotEquals(newStamp, ds.getStamp(), "Stamp remained the same!");

  Assert.assertTrue(
   checkContent(
    res,
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10
   ), "Content wrong!");

 }



 private boolean checkContent(FillingArrayList<TestJdbcDataStoreBean> l, int... test) {

  for (int i = 0 ; i < l.size()  &&  i < test.length; i ++) {
   if (test[i] > 0  &&  (Integer)(l.get(i).get("id")) == test[i])
    continue;
   if (test[i] < 0  &&  l.get(i) == null)
    continue;
   return false;
  }

  return true;

 }

}
