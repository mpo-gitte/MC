package core.bakedBeans;



import core.exceptions.MCException;

import java.lang.reflect.Constructor;
import java.util.Map.Entry;
import java.util.Set;



/**
 * A neat DataStore for collecting MemoryBeans for a parent bean.
 *
 * @param <T>
 *
 * @version 2023-09-13
 * @author pilgrim.lutz@imail.de
 */
// ToDo: Make it more generic for usage outside tests.

public class TestMemoryBeanDataStore<T extends MemoryBean> extends DataStore {



 public BeanList<T> read() {

  Bean bean = null;

  try {
   Constructor<? extends Bean> constructor =
    this.maker.getResultClass().getConstructor();

   bean = constructor.newInstance();
  }
  catch (Exception e) {
   throw new MCException(e);
  }

  @SuppressWarnings("unchecked")
  T mb = (T) bean;

  Set<Entry<Object, MemoryBean>> amb = mb.getAll();  // Get all MemoryBeans of the required type as a Set.

  @SuppressWarnings("unchecked")
  BeanList<T> res = new BeanList(maker.getResultClass());

  Object k = maker.getValues()[0];  // The key value of the parent bean.

  if (k != null)
   for (Entry<Object, MemoryBean> e : amb) {
    @SuppressWarnings("unchecked")
    T b = (T)e.getValue();
    if (k.equals(b.get(maker.getQueryString())))  // queryString contains the name of property for matching ("foreign key").
     res.add(b);
   }

  return res;
 }

}
