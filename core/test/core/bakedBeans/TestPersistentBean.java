package core.bakedBeans;


import core.exceptions.BeanAccessException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * A Test for the PersistentBean.
 *
 * @version 2023-09-21
 * @author lp
 *
 * @see PersistentBean
 */

public class TestPersistentBean {

 private Integer id3 = null;



 /**
  * Set up and store a simple MemoryBean.
  */

 @BeforeClass(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void init() {

  MemoryTestBean3 mtb3 = new MemoryTestBean3();

  mtb3.set("prop1", "Prop1");
  mtb3.set("prop2", "Prop2");

  mtb3.save();

  id3 = (Integer)mtb3.getKeyValue();

 }



 /**
  * Tests PersistentBean.setFromString().
  *
  * <p>A very simple process:</p>
  *
  * <ol>
  *  <li>Create a bean.</li>
  *  <li>Check if has status "empty".</li>
  *  <li>Call setFromString() on this bean to set the key value. Bean should be loading now.</li>
  *  <li>Ceck if bean has status "clean" now.</li>
  *  <li>Check if properties are loaded.</li>
  * </ol>
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testPersistentBeanSetFromString_normal() {

  MemoryTestBean3 mtb = new MemoryTestBean3();

  Assert.assertTrue(mtb.status.equals(PersistentBean.Status.empty), "Bean isn't empty after instantiation!");

  mtb.setFromString("id", id3.toString());  // Assume bean gets loaded because we assign a value to the key value.

  Assert.assertTrue(mtb.status.equals(PersistentBean.Status.clean), "Bean isn't clean after loading!");

  Assert.assertEquals(mtb.get("prop1"), "Prop1", "Bean property missing!");
  Assert.assertEquals(mtb.get("prop2"), "Prop2", "Bean property missing!");

 }



 /**
  * Tests PersistentBean.setFromString().
  *
  * <p>A little more complex process:</p>
  *
  * <ol>
  *  <li>Create a bean.</li>
  *  <li>Check if has status "empty".</li>
  *  <li>Set a property to a specific value.</li>
  *  <li>Check if bean gets "dirty".</li>
  *  <li>Call setFromString() on this bean to set the key value. Bean should be loading now.</li>
  *  <li>Check if bean remains in status "dirty" now!</li>
  *  <li>Check if properties are loaded.</li>
  *  <li>Check if setted property has its value.</li>
  * </ol>
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testPersistentBeanSetFromString_altered() {

  MemoryTestBean3 mtb = new MemoryTestBean3();

  Assert.assertTrue(mtb.status.equals(PersistentBean.Status.empty), "Bean isn't empty after instantiation!");

  mtb.set("prop1", "PROP1");

  Assert.assertTrue(mtb.status.equals(PersistentBean.Status.dirty), "Bean isn't dirty after setting a property!");

  mtb.setFromString("id", id3.toString());  // Assume bean gets loaded because we assign a value to the key value.

  Assert.assertTrue(mtb.status.equals(PersistentBean.Status.dirty), "Bean isn't dirty after loading!");

  Assert.assertEquals(mtb.get("prop1"), "PROP1", "Bean property missing!");  // Setted value remains intact after loading!
  Assert.assertEquals(mtb.get("prop2"), "Prop2", "Bean property missing!");

 }



 /**
  * Tests PersistentBean.setFromString().
  *
  * <p>Nearly as above with a few changes:</p>
  *
  * <ol>
  *  <li>Create a bean.</li>
  *  <li>Check if has status "empty".</li>
  *  <li>Set a property to a <em>value which is already in the persistence system!</em></li>
  *  <li>Check if bean gets "dirty".</li>
  *  <li>Call setFromString() on this bean to set the key value. Bean should be loading now.</li>
  *  <li>Check if bean remains <em>changes status to "clean" now!</em></li>
  *  <li>Check if properties are loaded.</li>
  *  <li>Check if setted property has its value.</li>
  * </ol>
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testPersistentBeanSetFromString_alteredButNoChanges() {

  MemoryTestBean3 mtb = new MemoryTestBean3();

  Assert.assertTrue(mtb.status.equals(PersistentBean.Status.empty), "Bean isn't empty after instantiation!");

  mtb.set("prop1", "Prop1");  // Set property to value already persisted.

  Assert.assertTrue(mtb.status.equals(PersistentBean.Status.dirty), "Bean isn't dirty after setting a property!");

  mtb.setFromString("id", id3.toString());  // Assume bean gets loaded because we assign a value to the key value.

  Assert.assertTrue(mtb.status.equals(PersistentBean.Status.clean), "Bean isn't clean after loading!");  // Bean gets clean. Because setting of property above isn't a change.

  Assert.assertEquals(mtb.get("prop1"), "Prop1", "Bean property missing!");
  Assert.assertEquals(mtb.get("prop2"), "Prop2", "Bean property missing!");

 }



 /**
  * Tests PersistentBean.setFromString().
  *
  * <p>Try to load a bean which is not in the persistence system:</p>
  *
  * <ol>
  *  <li>Create a bean.</li>
  *  <li>Check if has status "empty".</li>
  *  <li>Set a property to a specific value</li>
  *  <li>Check if bean gets "dirty".</li>
  *  <li>Call setFromString() on this bean to set the key value. Use a "wrong" key value. Bean can't be loaded.</li>
  *  <li>Check if bean remains "dirty".</em></li>
  *  <li>Check if setted property remains the same.</li>
  *  <li>Check if setted key value property has its setted value.</li>
  * </ol>
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testPersistentBeanSetFromString_alteredButNotExisting() {

  MemoryTestBean3 mtb = new MemoryTestBean3();

  Assert.assertTrue(mtb.status.equals(PersistentBean.Status.empty), "Bean isn't empty after instantiation!");

  mtb.set("prop1", "PROP1");  // Set property.

  Assert.assertTrue(mtb.status.equals(PersistentBean.Status.dirty), "Bean isn't dirty after setting a property!");

  mtb.setFromString("id", "-1");  // Assume bean gets not loaded because we assign a value to the key value which is not existing.

  Assert.assertTrue(mtb.status.equals(PersistentBean.Status.dirty), "Bean isn't dirty after failed loading!");  // Bean gets clean. Because setting of property above isn't a change.

  Assert.assertEquals(mtb.get("prop1"), "PROP1", "Bean property missing!");
  Assert.assertEquals(mtb.get("id"), -1, "Bean property missing!");

 }



 /**
  * Tests denying setting a property.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testPersistentBeanSetFromString_propertyDenied() {

  MemoryTestBean4 mtb = new MemoryTestBean4();

  // "prop1" may be set.

  mtb.setFromString("prop1", "Prop1");

  Assert.assertEquals(mtb.get("prop1"), "Prop1", "Bean property missing!");

  // "prop2" may NOT be set. Bean denies.

  Assert.assertThrows(BeanAccessException.class, () -> mtb.setFromString("prop2", "Prop2"));

 }

}
