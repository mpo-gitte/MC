package core.bakedBeans;

import org.testng.Assert;
import org.testng.annotations.Test;
import setup.Common;

import java.util.List;



/**
 * A Test for saving PersistentBeans hierarchically.
 *
 * @version 2023-09-07
 * @author pilgrim.lutz@imail.de
 */

public class TestPersistentBeanHierarchicalSave {



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testStringBean() {

  TestBeanTop top = new TestBeanTop();
  top.set("prop1", 4711);


  // Prepare check relation to a StringBean.

  TestStringBean tstb = new TestStringBean();

  tstb.set("prop1", 42);
  tstb.set("prop2", "Zaphod Beeblebrox");

  top.set("relStringBeanProp2", tstb);


  // Save top and sub bean.

  top.save();


  // Test the Relationship.

  // We're using the SillySequenceKeyMaker in our Beans! It just increments a global integer!
  Assert.assertNotEquals(top.get("id"), null,"Unexpected id value!");

  // Relation to the StringBean.
  Assert.assertTrue(((String)top.get("prop2")).contains("Zaphod"),"StringBean not properly persisted!");

 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testCollection() {

  TestBeanTop top = new TestBeanTop();
  top.set("prop1", 4711);


  // Prepare check relation to a collection.

  @SuppressWarnings("unchecked")
  List<TestSubBean1> subs = (List<TestSubBean1>)top.get("relTestSubBean1Collection");

  TestSubBean1 tsb2 = new TestSubBean1();
  tsb2.set("prop1", Integer.valueOf(4713));
  subs.add(tsb2);
  tsb2 = new TestSubBean1();
  tsb2.set("prop1", Integer.valueOf(4714));
  subs.add(tsb2);


  // Save top and all its sub beans.

  top.save();


  // Test the Relationship.

  // We're using the SillySequenceKeyMaker in our Beans! It just increments a global integer!
  Assert.assertNotEquals(top.get("id"), null,"Unexpected id value!");

  @SuppressWarnings("unchecked")
  List<TestSubBean1> bs = (List<TestSubBean1>)top.get("relTestSubBean1Collection");

  Assert.assertEquals(bs.size(), 2);  // Correct size of collection.

  Assert.assertEquals(bs.get(0).get("topid"), top.get("id"));  // Correct foreign keys?
  Assert.assertEquals(bs.get(1).get("topid"), top.get("id"));

 }



 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testPersistenBean() {

  TestBeanTop top = new TestBeanTop();
  top.set("prop1", 4711);


  // Prepare check relation to another PersistentBean.

  TestSubBean1 tsb1 = new TestSubBean1();

  tsb1.set("prop1", Integer.valueOf(4712));

  top.set("relTestSubBean1", tsb1);


  // Save top and sub bean.

  top.save();


  // Test the Relationship.

  // We're using the SillySequenceKeyMaker in our Beans! It just increments a global integer!
  Assert.assertNotEquals(top.get("id"), null,"Unexpected id value!");

  // Relation to the other MemoryBean.
  Assert.assertEquals(top.get("fk1"), tsb1.get("id"),"Unexpected foreign key value!");

 }

}
