package core.bakedBeans;

import org.testng.Assert;
import org.testng.annotations.Test;
import setup.Common;


/**
 * Tests the PropertyList.
 *
 * @version 2023-10-22
 * @author lp
 */

public class TestPropertyList {



 /**
  * Tests adding properties.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testPropertyListNormal() {

  PropertyList<String> pl = new PropertyList<>(String.class);

  pl.add("4711");
  pl.add("4712");

  Assert.assertEquals(pl.size(), 2, "Size wrong!");  // Normal list behaviour.

 }



 /**
  * Tests getting properties.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testPropertyList() {

  PropertyList<String> pl = new PropertyList<>(String.class);

  // But now PropertyList-action!

  String sd = pl.getProperty(9);

  Assert.assertNotNull(sd);  // Element given.
  Assert.assertEquals(pl.size(), 10, "Size wrong!");  // New length.

  String sd1 = pl.getProperty(5);

  Assert.assertNotNull(sd1);  // Element given.

 }

}
