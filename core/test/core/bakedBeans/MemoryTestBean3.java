package core.bakedBeans;



@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "prop1",
   name = "prop1",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "prop2",
   name = "prop2",
   pclass = String.class
  ),
 }
)
@PersistentBeanDescription(
 source = "MemoryTestBean3",
 keyName = "id",
 keyMakerClass = SillySequenceKeyMaker.class
)



public class MemoryTestBean3 extends MemoryBean {

}
