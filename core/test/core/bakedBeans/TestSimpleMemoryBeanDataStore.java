package core.bakedBeans;



import core.exceptions.MCException;

import java.lang.reflect.Constructor;
import java.util.Map;
import java.util.Set;



/**
 * A very simple DataStore for MemoryBeans.
 *
 * @version 2023-09-13
 * @author pilgrim.lutz@imail.de
 *
 * @param <T>
 */

public class TestSimpleMemoryBeanDataStore<T extends MemoryBean> extends DataStore {



 /**
  * Read the list of beans.
  *
  * @return The list of beans or null if there are no beans.
  */

 @Override
 public BeanList<T> read() {

  @SuppressWarnings("unchecked")
  Class<T> resultClass = (Class<T>)this.maker.getResultClass();

  T t = null;
  try {
   Constructor<T> constructor = resultClass.getConstructor();
   t = constructor.newInstance();
  }
  catch (Exception e) {
   throw new MCException(e);
  }

  @SuppressWarnings("unchecked")
  T mb = (T) t;

  Set<Map.Entry<Object, MemoryBean>> amb = mb.getAll();  // Get all MemoryBeans of the required type as a Set.

  @SuppressWarnings("unchecked")
  BeanList<T> res = new BeanList<T>(resultClass);

  for (Map.Entry<Object, MemoryBean> e : amb) {
   @SuppressWarnings("unchecked")
   T b = (T)e.getValue();
   res.add(b);
  }

  return res;
 }



 /**
  * Called by the DataStoreManager to signal the DataStore.
  *
  * @param sig The signal.
  * @param subjectBean A Bean which has something to do with the signal. May be null.
  *
  * @see DataStoreManager
  */

 @Override
 public void signal(DataStoreSignal sig, PersistentBean subjectBean) {

  super.signal(sig, subjectBean);

  reset();

 }

}
