package core.bakedBeans;



@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "prop1",
   name = "prop1",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "prop2",
   name = "prop2",
   pclass = String.class
  ),
 }
)
@PersistentBeanDescription(
 source = "MemoryTestBean3",
 keyName = "id",
 keyMakerClass = SillySequenceKeyMaker.class
)



public class MemoryTestBean4 extends MemoryBean {



 @Override
 protected boolean denyPropertySetFromString(PropertyInfo propertyInfo) {

  // Deny to set property "prop2" during setFromString().

  if ("prop2".equals(propertyInfo.getAlias()))
   return true;

  return super.denyPropertySetFromString(propertyInfo);

 }

}
