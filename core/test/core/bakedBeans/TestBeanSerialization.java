package core.bakedBeans;

import core.bakedBeans.beans.SerializationTestTypedBean;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;



/**
 * A Test for serializing beans.
 *
 * @version 2024-12-07
 * @author lp
 *
 * @see PersistentBean
 */

public class TestBeanSerialization {



 /**
  * Tests serialization of a Bean.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testTypedBeanSerialization() {


  // Prepare a normal bean

  SerializationTestTypedBean b = new SerializationTestTypedBean();

  b.setByName("prop1", "prop1");
  b.setByName("prop2", "prop2");
  b.setByName("prop3", "prop3");
  b.setByName("prop4", null);  // Should be ignored.


  // Get properties for serialization.

  Map<String, Object> props = b.getPropertiesForSerialization();

  Assert.assertEquals(props.size(), 2);  // Should return the two properties.

  // Map should contain the properties of the bean. But the key should be the alias of the property.

  Assert.assertFalse(props.containsKey("prop1"));  // Should not be there because it is tagged as not to be serailized.
  Assert.assertEquals(props.get("prop2"), "prop2");
  Assert.assertEquals(props.get("Aprop3"), "prop3");  // Alias!

 }



 /**
  * Tests serialization of a PersistentBean.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testPersistentBeanSerializationClean() {


  // Prepare a persistent bean

  SerializationTestPersistentBean stb = new SerializationTestPersistentBean();

  stb.setByName("prop1", "Prop1");
  stb.setByName("prop2", "Prop2");
  stb.setByName("prop3", "Prop3");
  stb.save();  // Bean is clean now.
  Integer id = (Integer)stb.get("id");  // Store id for test.

  Map<String, Object> props = stb.getPropertiesForSerialization();

  Assert.assertEquals(props.size(), 1);  // Should return only the id because bean is saved and clean.
  Assert.assertEquals(props.get("id"), id);

 }



 /**
  * Tests serialization of a modified PersistentBean.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testPersistentBeanSerializationDirty() {


  // Prepare a persistent bean

  SerializationTestPersistentBean stb = new SerializationTestPersistentBean();

  stb.setByName("prop1", "Prop1");
  stb.setByName("prop2", "Prop2");
  stb.setByName("prop3", "Prop3");
  stb.save();  // Bean is clean now.
  Integer id = (Integer)stb.get("id");  // Store id for test.

  SerializationTestPersistentBean stbReloaded = new SerializationTestPersistentBean();
  stbReloaded.load(id);

  stbReloaded.setByName("prop3", "PropNew");  // Modify bean after saving.

  Map<String, Object> props = stbReloaded.getPropertiesForSerialization();

  Assert.assertEquals(props.size(), 2);  // Should return only the id and the only set property.
  Assert.assertEquals(props.get("id"), id);
  Assert.assertEquals(props.get("Aprop3"), "PropNew");  // Alias!

 }

}
