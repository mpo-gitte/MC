package core.bakedBeans.beans;



import core.bakedBeans.*;


/**
 * This bean implements a TypedBean for testing serialization.
 *
 * @see TestTypedBean#testTypedBeanSetFromString()
 *
 * @version 2024-12-07
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   name = "prop1",
   pclass = String.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   name = "prop2",
   pclass = String.class
  ),
  @PropertyDescription(
   name = "prop3",
   alias = "Aprop3",
   pclass = String.class
  ),
  @PropertyDescription(
   name = "prop4",
   alias = "Aprop4",
   pclass = Integer.class
  ),
 }
)



public class SerializationTestTypedBean extends TypedBean {

}
