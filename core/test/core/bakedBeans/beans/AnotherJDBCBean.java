package core.bakedBeans.beans;

import core.bakedBeans.*;


/**
 * Another JDBC test bean.
 *
 * @version 2024-08-15
 * @author lp
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "prop1",
   name = "prop1",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "prop2",
   name = "prop2",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "prop3",
   name = "prop3",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "prop4",
   name = "prop4",
   pclass = String.class,
   propertyFunction = PropertyFunction.upper
  ),
  @PropertyDescription(
   alias = "prop5",
   name = "prop5",
   pclass = String.class,
   propertyFunction = PropertyFunction.max
  ),
  @PropertyDescription (
   alias = "ARelation",
   name = "ar",
   base = "prop1",
   relationType = RelationType.dontSave,
   pclass = SubBean.class,
   setter = "relN2OSetter",
   getter = "relN2OGetterNL"
  ),
 }
)
@PersistentBeanDescription(
 source = "sourcetable",
 keyName = "prop1",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@JDBCBeanDescription(
 loadQueryString = ""
)

public class AnotherJDBCBean extends JDBCBean {

}
