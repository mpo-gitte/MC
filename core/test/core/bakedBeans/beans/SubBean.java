package core.bakedBeans.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;



/**
 * A bean for testing cascading beans.
 *
 * @version 2020-02-08
 * @author pilgrim.lutz@omail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "teststring",
   name = "teststring",
   pclass = String.class
  )
 }
)
@PersistentBeanDescription(
 source = "subbean",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@JDBCBeanDescription(
 loadQueryString =  "select * from subbean where id=?"
)

public class SubBean extends JDBCBean {

}
