package core.bakedBeans.beans;

import core.bakedBeans.*;

import java.util.ArrayList;


/**
 * A JDBC test bean.
 *
 * @version 2021-12-17
 * @author pilgrim.lutz@omail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "prop1",
   name = "prop1",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "prop2",
   name = "prop2",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "collection",
   pclass = ArrayList.class,
   iclass = core.bakedBeans.beans.AJDBCBean.class,
   getter = "simpleDataStoreGetter",
   dataStoreName = "application.beans.JDBCDataStore",
   loadWith = {"prop1", "prop2"},
   orderBy = {"core.bakedBeans.beans.AJDBCBean:prop1", "-core.bakedBeans.beans.AJDBCBean:prop2"},
   base = "prop1",
   category = "cat1"
  )
 }
)
@PersistentBeanDescription(
 source = "AJDBCBeanWAC",
 keyName = "prop1",
 keyMakerClass = JDBCSequenceKeyMaker.class
)

public class AJDBCBeanWithACollection extends JDBCBean {

}
