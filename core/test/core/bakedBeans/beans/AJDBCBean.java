package core.bakedBeans.beans;

import core.bakedBeans.*;


/**
 * A JDBC test bean.
 *
 * @version 2021-05-01
 * @author pilgrim.lutz@omail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "prop1",
   name = "prop1",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "prop2",
   name = "prop2",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "prop3",
   name = "prop3",
   pclass = String.class
  )
 }
)
@PersistentBeanDescription(
 source = "AJDBCSource",
 keyName = "prop1",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@JDBCBeanDescription(
 loadQueryString =  ""
)

public class AJDBCBean extends JDBCBean {

}
