package core.bakedBeans.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;



/**
 * A bean for testing casding beans.
 *
 * @version 2020-02-08
 * @author pilgrim.lutz@omail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "teststring",
   name = "teststring",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "subId",
   name = "sub_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "subBean",
   name = "sb",
   base = "subId",
   relationType = RelationType.beforeSave,
   pclass = SubBean.class,
   setter = "relN2OSetter",
   getter = "relN2OGetterNL"
  )
 }
)
@PersistentBeanDescription(
 source = "masterbean",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@JDBCBeanDescription(
 loadQueryString =  "select masterbean.*, subbean.id sb$id, subbean.created sb$created, subbean.teststring sb$teststring from masterbean, subbean where masterbean.id=? and subbean.id=masterbean.sub_id"
)

public class MasterBean extends JDBCBean {

}
