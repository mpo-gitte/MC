package core.bakedBeans.beans;

import core.bakedBeans.Bean;

/**
 * Dummy bean for testing bean equality.
 *
 * @version 2020-02-19
 * @author pilgrim.lutz@imail.de
 */

public class TestEqualityBean extends Bean {

}
