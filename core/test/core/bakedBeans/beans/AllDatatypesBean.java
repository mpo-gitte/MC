package core.bakedBeans.beans;



import core.bakedBeans.*;

import java.time.LocalDate;
import java.time.LocalDateTime;



/**
 * A test bean which has all valid datatypes.
 *
 * @see TestTypedBean#testTypedBeanSetFromString()
 *
 * @version 2020-02-08
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   name = "boolean",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "integer",
   pclass = Integer.class
  ),
  @PropertyDescription(
   name = "long",
   pclass = Long.class
  ),
  @PropertyDescription(
   name = "float",
   pclass = Float.class
  ),
  @PropertyDescription(
   name = "double",
   pclass = Double.class
  ),
  @PropertyDescription(
   name = "string",
   pclass = String.class
  ),
  @PropertyDescription(
   name = "date",
   pclass = LocalDate.class
  ),
  @PropertyDescription(
   name = "timestamp",
   pclass = LocalDateTime.class
  ),
 }
)



public class AllDatatypesBean extends TypedBean {

}
