package core.bakedBeans.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;


/**
 * A bean for testing cascading beans.
 *
 * @version 2021-12-27
 * @author pilgrim.lutz@omail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "teststring",
   name = "teststring",
   pclass = String.class
  )
 }
)
@PersistentBeanDescription(
 source = "subbean",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@JDBCBeanDescription(
 loadQueryString =  "select * from subbean where id=?"
)

public class SubBeanWithoutStore extends JDBCBean {



 /**
  * Saves this bean as a new bean.
  *
  * <p>Saves all the relations too.</p>
  */

 @Override
 public void save() {

  // Avoid any saving for test purposes.

 }

}
