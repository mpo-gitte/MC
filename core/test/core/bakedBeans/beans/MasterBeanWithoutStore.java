package core.bakedBeans.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;


/**
 * A bean for testing cascading beans.
 *
 * @version 2021-12-27
 * @author pilgrim.lutz@omail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "teststring",
   name = "teststring",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "subId",
   name = "sub_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "subBean",
   name = "sb",
   base = "subId",
   relationType = RelationType.beforeSave,
   pclass = SubBeanWithoutStore.class,
   setter = "relN2OSetter",
   getter = "relN2OGetterNL"
  )
 }
)
@PersistentBeanDescription(
 source = "masterbean",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@JDBCBeanDescription(
 loadQueryString = "select masterbean.*, subbean.id sb$id, subbean.created sb$created, subbean.teststring sb$teststring from masterbean, subbean where masterbean.id=? and subbean.id=masterbean.sub_id"
)

public class MasterBeanWithoutStore extends JDBCBean {

}
