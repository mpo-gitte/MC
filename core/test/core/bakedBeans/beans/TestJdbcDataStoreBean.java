package core.bakedBeans.beans;



import core.bakedBeans.*;



/**
 * A test bean for testing the JdbcDataStore.
 *
 * @version 2023-10-25
 * @author lp
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "prop1",
   name = "prop1",
   pclass = String.class
  ),
 }
)
@PersistentBeanDescription(
 source = "testjdbcdatastorebean",
 keyName = "id",
 keyMakerClass = SillySequenceKeyMaker.class
)



public class TestJdbcDataStoreBean extends JDBCBean {

}
