package core.bakedBeans.beans;

import core.bakedBeans.*;

import java.time.LocalDate;
import java.time.LocalDateTime;



/**
 * A bean for testing datatype mapping.
 *
 * @version 2020-01-28
 * @author pilgrim.lutz@omail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "teststring",
   name = "teststring",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "testdate",
   name = "testdate",
   pclass = LocalDate.class
  ),
  @PropertyDescription(
   alias = "testint",
   name = "testint",
   pclass = Integer.class
  )
 }
)
@PersistentBeanDescription(
 source = "datatypemapping",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@JDBCBeanDescription(
 loadQueryString =  "select * from datatypemapping where id=?"
)

public class DataTypeMappingBean extends JDBCBean {

}
