package core.bakedBeans.beans;

import core.bakedBeans.*;


/**
 * Some other JDBC test bean.
 *
 * @version 2021-12-06
 * @author pilgrim.lutz@omail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "prop1",
   name = "prop1",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "prop2",
   name = "prop2",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "prop3",
   name = "prop3",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "ARelation",
   name = "ar",
   base = "prop1",
   loadWith = {"id", "created"},
   relationType = RelationType.dontSave,
   pclass = SubBean.class,
   setter = "relN2OSetter",
   getter = "relN2OGetterNL"
  ),
 }
)
@PersistentBeanDescription(
 source = "sourcetable",
 keyName = "prop1",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@JDBCBeanDescription(
 loadQueryString = ""
)

public class ADifferentJDBCBean extends JDBCBean {

}
