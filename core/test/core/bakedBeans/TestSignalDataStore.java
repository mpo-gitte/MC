package core.bakedBeans;



/**
 * A DataStore for test purposes with a special signal method.
 *
 * @version 2022-02-09
 * @author pilgrim.lutz@imail.de
 *
 * @param <T> Type of bean this DataStore holds.
 */

public class TestSignalDataStore<T extends MemoryBean> extends TestMemoryBeanDataStore<T> {



 @Override
 public void signal(DataStoreSignal sig, PersistentBean subjectBean) {

  super.signal(sig, subjectBean);

  TestDataStoreManager.stamps.add("TestSignalDataStore.signal() for " + maker.getCategory());

  // Reset the DataStore.
  reset();

 }

}
