package core.bakedBeans;

import core.bakedBeans.beans.AllDatatypesBean;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import setup.Common;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;


/**
 * Some tests for the TypedBean.
 *
 * @version 2023-09-07
 * @author pilgrim.lutz@imail.de
 *
 * @see TypedBean
 */

public class TestTypedBean {



 /**
  * Checks the structure of a PersistentBean,
  *
  * <p>Uses getPropertyInfos()</p>
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testTypedBeanGetProperties() {

  Map<String, PropertyInfo> props =
   AllDatatypesBean.getPropertyInfos(AllDatatypesBean.class);

  Assert.assertEquals(props.size(), 8);

  Assert.assertTrue(props.containsKey("boolean"));
  Assert.assertTrue(props.containsKey("integer"));
  Assert.assertTrue(props.containsKey("long"));
  Assert.assertTrue(props.containsKey("float"));
  Assert.assertTrue(props.containsKey("double"));
  Assert.assertTrue(props.containsKey("string"));
  Assert.assertTrue(props.containsKey("date"));
  Assert.assertTrue(props.containsKey("timestamp"));

 }



 /**
  * Checks the structure of a PersistentBean,
  *
  * <p>Uses getPropertyInfosByName()</p>
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testTypedBeanGetPropertiesByName() {

  Map<String, PropertyInfo> props =
   AllDatatypesBean.getPropertyInfosByName(AllDatatypesBean.class);

  Assert.assertEquals(props.size(), 8);

  Assert.assertTrue(props.containsKey("boolean"));
  Assert.assertTrue(props.containsKey("integer"));
  Assert.assertTrue(props.containsKey("long"));
  Assert.assertTrue(props.containsKey("float"));
  Assert.assertTrue(props.containsKey("double"));
  Assert.assertTrue(props.containsKey("string"));
  Assert.assertTrue(props.containsKey("date"));
  Assert.assertTrue(props.containsKey("timestamp"));

 }



 /**
  * Tests TypedBean.setFromString() for all valid datatypes.
  *
  * <p>A datatype is valid if it can processed by TypedBean.setFromString().</p>
  *
  * @see TypedBean#setFromString(String, String)
  */

 @DataProvider(name = "testTypedBeanSetFromString")
 public static Object[][] getTypedBeanSetFromString() {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss.SSSSSS");

  return new Object[][] {
   {"string", "string", "string"},
   {"boolean", "true", Boolean.TRUE},
   {"boolean", "false", Boolean.FALSE},
   {"integer", "4711", 4711},
   {"long", "4712", 4712L},
   {"float", "1.234", 1.234F},
   {"double", "1.234", 1.234D},
   {"date", "1960-03-11", LocalDate.parse("11.03.1960", DateTimeFormatter.ofPattern("dd.MM.yyyy"))},
   {"timestamp", "1960-03-11T10:01:02.003002", LocalDateTime.parse("11.03.1960 10:01:02.003002", df)},
   {"timestamp", "1960-03-11T10:01:02", LocalDateTime.parse("11.03.1960 10:01:02.000000", df)},
   {"string", "NULL", "NULL"},
   {"string", "~NULL", "~NULL"},
   {"string", "NULL~", "NULL~"},
   {"string", "~NULL~", null}  // Special "textual" null value.
  };
 }

 @Test(dataProvider = "testTypedBeanSetFromString")
 public void testTypedBeanSetFromString(String propertyName, String value, Object expected) {

  AllDatatypesBean adb = new AllDatatypesBean();

  adb.setFromString(propertyName, value);
  Assert.assertEquals(
   expected,
   adb.get(propertyName),
   "Setting " + propertyName + " doesn't work!");

 }



/**
 * Test the BeanFactory which uses TypedBean.setFromString() to load a bean from a string.
 *
 * @see BeanFactory#loadBeanFromString(String, TypedBean)
 */

 @Test
 public void testBeanFactoryLoadFromString() {

  AllDatatypesBean adb = new AllDatatypesBean();

  // All together now.

  BeanFactory.loadBeanFromString(
   "string=\"string\";boolean=true;integer=4711;long=4712;float=1.234;double=1.234;date=\"1960-03-11\";timestamp=\"1960-03-11T10:01:02.003002\"",
   adb
  );

  Assert.assertEquals("string", adb.get("string"), "Setting String doesn't work!");
  Assert.assertEquals(Boolean.TRUE, adb.get("boolean"), "Setting Boolean doesn't work!");
  Assert.assertEquals(4711, adb.get("integer"), "Setting Integer doesn't work!");
  Assert.assertEquals(4712L, adb.get("long"), "Setting Long doesn't work!");
  Assert.assertEquals(1.234F, adb.get("float"), "Setting Float doesn't work!");
  Assert.assertEquals(1.234D, adb.get("double"), "Setting Double doesn't work!");
  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");
  Assert.assertEquals(LocalDate.parse("11.03.1960", df), adb.get("date"), "Setting Date doesn't work!");
  df = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss.SSSSSS");
  Assert.assertEquals(LocalDateTime.parse("11.03.1960 10:01:02.003002", df), adb.get("timestamp"), "Setting Timestamp doesn't work!");

 }



 /**
  * Test the BeanFactory which uses TypedBean.setFromString() to load a bean from a string
  * produced by BeanFactory.dumpBeanToString().
  *
  * @see BeanFactory#loadBeanFromString(String, TypedBean)
  * @see BeanFactory#dumpBeanToString(TypedBean)
  */

 @Test
 public void testBeanFactoryLoadFromStringFromdumpToString() {

  AllDatatypesBean adb0 = new AllDatatypesBean();
  adb0.set("string", "string");
  adb0.set("boolean", Boolean.TRUE);
  adb0.set("integer", 4711);
  adb0.set("long", 4712L);
  adb0.set("float", 1.234F);
  adb0.set("double", 1.234D);
  adb0.set("date",LocalDate.parse("11.03.1960", DateTimeFormatter.ofPattern("dd.MM.yyyy")));
  adb0.set("timestamp", LocalDateTime.parse("11.03.1960 10:01:02.003002", DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss.SSSSSS")));

  AllDatatypesBean adb = new AllDatatypesBean();

  // All together now.

  BeanFactory.loadBeanFromString(
   BeanFactory.dumpBeanToString(adb0),
   adb
  );

  Assert.assertEquals("string", adb.get("string"), "Setting String doesn't work!");
  Assert.assertEquals(Boolean.TRUE, adb.get("boolean"), "Setting Boolean doesn't work!");
  Assert.assertEquals(4711, adb.get("integer"), "Setting Integer doesn't work!");
  Assert.assertEquals(4712L, adb.get("long"), "Setting Long doesn't work!");
  Assert.assertEquals(1.234F, adb.get("float"), "Setting Float doesn't work!");
  Assert.assertEquals(1.234D, adb.get("double"), "Setting Double doesn't work!");
  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");
  Assert.assertEquals(LocalDate.parse("11.03.1960", df), adb.get("date"), "Setting Date doesn't work!");
  df = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss.SSSSSS");
  Assert.assertEquals(LocalDateTime.parse("11.03.1960 10:01:02.003002", df), adb.get("timestamp"), "Setting Timestamp doesn't work!");

 }

}
