package core.bakedBeans;



import core.bakedBeans.beans.AllDatatypesBean;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;



/**
 * Test bean utilities.
 *
 * @version 2024-04-10
 * @author lp
 */

public class TestUtilities {



 /**
  * Tests if a bean is empty.
  */

 @DataProvider(name = "testIsEmpty")
 public static Object[][] getIsEmpty() {

  return new Object[][] {
   {null, null, null, Boolean.TRUE},
   {new AllDatatypesBean(), null,     null,       Boolean.TRUE},
   {new AllDatatypesBean(), "string", "a string", Boolean.FALSE},
   {new AllDatatypesBean(), "string", "",         Boolean.FALSE},
   {new AllDatatypesBean(), "string", null,       Boolean.FALSE},
  };
 }

 @Test(dataProvider = "testIsEmpty")
 public void testIsEmpty(Bean bean, String propertyAlias, Object value, Boolean expectedResult) {

  if (bean != null  &&  propertyAlias != null)
   bean.set(propertyAlias, value);

  Assert.assertEquals(Utilities.isEmpty(bean), expectedResult);

 }



 /**
  * Tests getting a property.
  */

 @DataProvider(name = "testGetPropertyNotEmpty")
 public static Object[][] getGetPropertyNotEmpty() {

  return new Object[][] {
   {null, null, null, null},
   {new AllDatatypesBean(), null,     null,       null},
   {new AllDatatypesBean(), "string", "a string", "a string"},
   {new AllDatatypesBean(), "string", "",         ""},
   {new AllDatatypesBean(), "string", null,       null},
  };
 }

 @Test(dataProvider = "testGetPropertyNotEmpty")
 public void testGetPropertyNotEmpty(TypedBean bean, String propertyAlias, Object value, Object expectedResult) {

  if (bean != null  &&  propertyAlias != null)
   bean.set(propertyAlias, value);

  Assert.assertEquals(Utilities.getPropertyNotEmpty(bean, propertyAlias), expectedResult);

 }

}
