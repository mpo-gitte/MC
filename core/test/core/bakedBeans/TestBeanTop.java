package core.bakedBeans;

/**
 * The "top" bean for the test of hierarchical save.
 *
 * @version 2020-04-27
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "prop1",
   name = "prop1",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "fk1",
   name = "fk1",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "prop2",
   name = "prop2",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "relStringBeanProp2",
   base = "prop2",
   relationType = RelationType.beforeSave,
   pclass = TestStringBean.class,
   setter = "stringBeanSetter",
   getter = "stringBeanGetter"
  ),
  @PropertyDescription(
   alias = "relTestSubBean1",
   base = "fk1",
   relationType = RelationType.beforeSave,
   pclass = TestSubBean1.class,
   setter = "relN2OSetter",
   getter = "relN2OGetter"
  ),
  @PropertyDescription(
   alias = "relTestSubBean1Collection",
   base = "topid",  // in related Bean!
   relationType = RelationType.afterSave,
   pclass = BeanList.class,
   iclass = TestSubBean1.class,
   dataStoreName = "core.bakedBeans.TestMemoryBeanDataStore",
   query = "topid",
   category = "SaveTest",
   setter = "relO2NSetter",
   getter = "relO2NGetter"
  )
 }
)
@PersistentBeanDescription(
 source = "TestBeanTop",
 keyName = "id",
 keyMakerClass = SillySequenceKeyMaker.class
)



public class TestBeanTop extends MemoryBean {

}
