package core.bakedBeans;

import core.base.MeterMaid;
import core.servlet.BaseConfig;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import jakarta.servlet.http.HttpServletRequest;
import setup.Common;

import java.util.ArrayList;
import java.util.List;



/**
 * Test DataStoreManager.
 *
 * @version 2023-09-07
 * @author pilgrim.lutz@imail.de
 */

public class TestDataStoreManager {

 public static List<String> stamps;



 @BeforeSuite()
 public void init() {

  // No monitoring in tests.

  (new MeterMaid()).init(new BaseConfig((HttpServletRequest)null, null, null, null) {
   @Override
   public String getInitParameter(String parameter) {
    return null;
   }
   @Override
   public String getInitParameter(String parameter, String defValue) {
    return null;
   }
   @Override
   public String getVelocityProperty(String parameter, String defValue) {
    return null;
   }
  });

 }



 /**
  * Test the caching of the DataStoreManager.
  *
  * <p>Despite of the number of threads running the number of created DataStores has to be the same.</p>
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, threadPoolSize = 5, invocationCount = 50, timeOut = 10000)
 public void testDataStoreManager() {

  DataStoreManager.
   get().makeDataStore(
   "core.bakedBeans.TestMemoryBeanDataStore",
   "cat1",
   "",
   new Object[]{},
   MemoryTestBean1.class
  );

  DataStoreManager.get().makeDataStore(
   "core.bakedBeans.TestMemoryBeanDataStore",
   "cat1",
   "",
   new Object[]{},
   MemoryTestBean2.class
  );

  DataStoreManager.get().makeDataStore(
   "core.bakedBeans.TestMemoryBeanDataStore",
   "cat2",
   "",
   new Object[]{},
   MemoryTestBean1.class
  );

  DataStoreManager.get().makeDataStore(
   "core.bakedBeans.TestMemoryBeanDataStore",
   "cat2",
   "",
   new Object[]{},
   MemoryTestBean2.class
  );

  DataStoreManager.get().makeDataStore(
   "core.bakedBeans.TestMemoryBeanDataStore",
   "cat3",
   "",
   new Object[]{},
   MemoryTestBean1.class
  );

  DataStoreManager.get().makeDataStore(
   "core.bakedBeans.TestMemoryBeanDataStore",
   "cat3",
   "",
   new Object[]{},
   MemoryTestBean2.class
  );

  // The test created 6 DataStores in 3 categories.
  // Let us have a look...

  int cat1 = 0;
  int cat2 = 0;
  int cat3 = 0;

  for (DataStore ds : DataStoreManager.get().getDataStores().values()) {
   String c = ds.maker.getCategory();
   if ("cat1".equals(c))
    ++ cat1;
   else if ("cat2".equals(c))
    ++ cat2;
   else if ("cat3".equals(c))
    ++ cat3;
  }

  // We can not evaluate the number of DataStores returned by getDataStores() directly
  // because other threads are creating DataStores too.

  Assert.assertEquals(cat1, 2);
  Assert.assertEquals(cat2, 2);
  Assert.assertEquals(cat3, 2);

 }



 /**
  * Checks the signalling of DataStores.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testSignal() {

  stamps = new ArrayList<>();


  // Create some DataStores.

  DataStoreManager.get().makeDataStore(
   "core.bakedBeans.TestSignalDataStore",
   "catSig1",
   "",
   new Object[]{},
   MemoryTestBean1.class
  );

  DataStoreManager.get().makeDataStore(
   "core.bakedBeans.TestSignalDataStore",
   "catSig1",
   "",
   new Object[]{},
   MemoryTestBean2.class
  );

  DataStoreManager.get().makeDataStore(
   "core.bakedBeans.TestSignalDataStore",
   "catSig2",
   "",
   new Object[]{},
   MemoryTestBean1.class
  );

  DataStoreManager.get().makeDataStore(
   "core.bakedBeans.TestSignalDataStore",
   "catSig2",
   "",
   new Object[]{},
   MemoryTestBean2.class
  );


  // Signal for both categories.

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, "catSig1", "catSig2");


  // Check if we were in the signal-method of the DataStore.

  Assert.assertTrue(stamps.contains("TestSignalDataStore.signal() for catSig1"), "Was not in TestSignalDataStore.signal()!");
  Assert.assertTrue(stamps.contains("TestSignalDataStore.signal() for catSig2"), "Was not in TestSignalDataStore.signal()!");

 }



 /**
  * Checks the signalling of DataStores.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testGetStamp() {

  stamps = new ArrayList<>();

  // Prepare some beans.

  MemoryTestBean1 b1 = new MemoryTestBean1();
  b1.set("prop1", 1);
  b1.save();
  MemoryTestBean1 b2 = new MemoryTestBean1();
  b2.set("prop1", 2);
  b2.save();

  // Create a DataStore.

  DataStore<MemoryTestBean1> ds = DataStoreManager.get().makeDataStore(
   "core.bakedBeans.TestSimpleMemoryBeanDataStore",
   "catSig1",
   "",
   new Object[]{},
   MemoryTestBean1.class
  );

  // Get its stamp.

  Long dsStamp = ds.getStamp();

  // Signal the DataStore.

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, "catSig1");

  // Content of the DataStore changed?

  Assert.assertEquals(dsStamp, ds.getStamp(), "Stamp changed!");

  // Fake some business code: Add a bean.

  MemoryTestBean1 b3 = new MemoryTestBean1();
  b3.set("prop1", 3);
  b3.save();

  // Signal DataStore.

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, "catSig1");

  // Get a DataStore.

  DataStore<MemoryTestBean1> ds1 = DataStoreManager.get().makeDataStore(
   "core.bakedBeans.TestSimpleMemoryBeanDataStore",
   "catSig1",
   "",
   new Object[]{},
   MemoryTestBean1.class
  );

  Assert.assertEquals(ds, ds1, "Not the same DataStore!");  // Make sure it's the same DataStore!

  // Stamp has to be changed.

  Assert.assertNotEquals(dsStamp, ds1.getStamp(), "Stamp not changed!");

 }



 /**
  * Checks the semaphore mechanism of the DataStoreManager.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testgetSemaphore() throws InterruptedException {

  stamps = new ArrayList<>();

  DataStoreManager.get().makeDataStore(
   "core.bakedBeans.TestSignalDataStore",
   "catSig1",
   "",
   new Object[]{},
   MemoryTestBean1.class
  );

  TestThread testThread = new TestThread(stamps);  // Create a thread for testing.
  testThread.start();

  Thread.sleep(1000); // Waste some time to get thread up and running.

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, "catSig1");

  Thread.sleep(1000);  // Waste some time to get thread done.

  synchronized (stamps) {
   Assert.assertTrue(stamps.contains("TestSignalDataStore.signal() for catSig1"), "Was not in TestSignalDataStore.signal()!");
   Assert.assertTrue(stamps.contains("TestThread.wait() for catSig1"), "Was not in TestThread.wait()!");
  }

 }



 /**
  * Checks tagging of DataStores.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testTaggedBy() {

  // Fetch a DataStore.

  MemoryTestBean2 mtb1 = new MemoryTestBean2();
  mtb1.set("id", 1);
  mtb1.set("prop1", "PROP1");

  DataStore ds1 = DataStoreManager.get().dataStore(DataStore.class, "favourites")
   .taggedWith(   // Tagged with two Strings.
    "test1",
    mtb1
   )
   .make(MemoryTestBean1.class);

  // Fetch a DataStore with the same configuration.

  DataStore ds2 = DataStoreManager.get().dataStore(DataStore.class, "favourites")
   .taggedWith(   // Tagged with two Strings.
    "test1",
    mtb1
   )
   .make(MemoryTestBean1.class);

  // It should only one DataStore!
  Assert.assertEquals(ds1, ds2, "DataStores not the same");


  // Another text: Fetch a DataStore with exchanged tags.

  DataStore ds3 = DataStoreManager.get().dataStore(JDBCDataStore.class, "favourites")
   .taggedWith(   // Tagged are exchanged!
    mtb1,
    "test1"
   )
   .make(MemoryTestBean1.class);

  // It has to be a different DataStore!

  Assert.assertNotEquals(ds1, ds3, "DataStores are the same");


  // Now a bit more complicated test: Use a different beans instance with same content.

  MemoryTestBean2 mtb2 = new MemoryTestBean2();
  mtb2.set("id", 1);
  mtb2.set("prop1", "PROP1");

  DataStore ds4 = DataStoreManager.get().dataStore(JDBCDataStore.class, "favourites")
   .taggedWith(   // Tagged are exchanged!
    mtb2,
    "test1"
   )
   .make(MemoryTestBean1.class);

  // It has to be the same DataStore because bean content rules!

  Assert.assertEquals(ds3, ds4, "DataStores are different!");


  // Now another bit more complicated test:
  // Use a different bean instance again but with slightly different content.

  MemoryTestBean2 mtb3 = new MemoryTestBean2();
  mtb3.set("id", 2); // Look here for the difference!
  mtb3.set("prop1", "PROP1");

  DataStore ds5 = DataStoreManager.get().dataStore(JDBCDataStore.class, "favourites")
   .taggedWith(
    mtb3,
    "test1"
   )
   .make(MemoryTestBean1.class);

  // It has to be a different DataStore because bean content rules!

  Assert.assertNotEquals(ds3, ds5, "DataStores aren't different!");

 }



 /**
  * Checks resetting DataStores.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 void testResetableDataStore() {

  stamps = new ArrayList<>();

  DataStoreManager.get().dataStore(core.bakedBeans.TestSignalResetDataStore.class, "favourites")
   .resetable()  // Mark resetable.
   .taggedWith(
    "test1"
   )
   .make(MemoryTestBean1.class);

  // Signal.

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, "favourites");

  // We were in reset-method of the DataStore?

  Assert.assertTrue(stamps.contains("TestSignalDataStore.signal() for favourites"), "Was not in TestSignalResetDataStore.signal()!");

 }



 /**
  * Checks resetting DataStores with condition true.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 void testResetConditionTrue() {

  stamps = new ArrayList<>();

  DataStoreManager.get().dataStore(core.bakedBeans.TestSignalResetDataStore.class, "favourites")
   .resetable((DataStoreManager.IDataStoreMaker dsm, Bean b) -> {
    return ((String)dsm.getTaggedWith()[0]).equals("test1");
   })
   .taggedWith(
    "test1"
   )
   .make(MemoryTestBean1.class);

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, "favourites");

  Assert.assertTrue(stamps.contains("TestSignalDataStore.signal() for favourites"), "Was not in TestSignalResetDataStore.signal()!");

 }



 /**
  * Checks resetting DataStores with condition false.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 void testResetConditionFalse() {

  stamps = new ArrayList<>();

  DataStoreManager.get().dataStore(core.bakedBeans.TestSignalResetDataStore.class, "favourites")
   .resetable((DataStoreManager.IDataStoreMaker dsm, Bean b) -> {
    return ((String)dsm.getTaggedWith()[0]).equals("test1");  // Condition should be false.
   })
   .taggedWith(
    "test2"
   )
   .make(MemoryTestBean1.class);

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, "favourites");

  Assert.assertFalse(stamps.contains("TestSignalDataStore.signal() for favourites"), "Was in TestSignalResetDataStore.signal()!");

 }



 private class TestThread extends Thread {

  private final List<String> stamps;

  TestThread(List<String> stamps) {

   this.stamps = stamps;

  }

  public void run() {

   DataStoreManager.Semaphore sem = DataStoreManager.get().getSemaphore("catSig1");  // Get a semaphore...

   try {
    sem.waitFor();  // ... and wait for signal.
   } catch (InterruptedException e) {
    e.printStackTrace();
   }

   synchronized (this.stamps) {
    this.stamps.add("TestThread.wait() for catSig1");  // Note waitFor() worked!
   }

  }

 }

}
