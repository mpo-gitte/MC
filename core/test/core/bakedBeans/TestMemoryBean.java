package core.bakedBeans;

import org.testng.Assert;
import org.testng.annotations.Test;
import setup.Common;

import java.util.ArrayList;



/**
 * A Test for the memory management of MemoryBeans.
 *
 * @version 2023-07-09
 * @author pilgrim.lutz@imail.de
 */

public class TestMemoryBean {


 /**
  * Create 100 MemoryTestBean1 and check for the property values.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, threadPoolSize = 5, invocationCount = 60, timeOut = 10000)
 public void testMemoryBean1() {

  int id = 0;
  ArrayList<KeyValue> keys = new ArrayList<>();

  for (int id1 = 0; id1 < 100; id1 ++) {
   MemoryTestBean1 mtb = new MemoryTestBean1();

   Assert.assertEquals(mtb.getStatus(), PersistentBean.Status.empty, "Status for bean isn't empty!");

   mtb.set("prop1", id++);

   Assert.assertEquals(mtb.getStatus(), PersistentBean.Status.dirty, "Status for bean isn't dirty!");

   mtb.save();

   Assert.assertEquals(mtb.getStatus(), PersistentBean.Status.clean, "Status for bean " + mtb.getKeyValue() + " isn't clean!");

   keys.add(new KeyValue((Integer)mtb.getKeyValue(), mtb.get("prop1")));

  }

  for (KeyValue key : keys) {
   MemoryTestBean1 b = new MemoryTestBean1();

   b.load(key.key);

   Assert.assertEquals(b.getStatus(), PersistentBean.Status.clean, "Status for bean " + key.key + " isn't clean!");
   Assert.assertEquals(b.get("prop1"), key.value, "Unexpected property value " + key.value + " for bean " + key.key + "!");

   b.delete();
  }

 }



 /**
  * Create 100 MemoryTestBean2 and check for the property values.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL}, threadPoolSize = 3, invocationCount = 50, timeOut = 10000)
 public void testMemoryBean2() {

  int id = 0;
  ArrayList<KeyValue> keys = new ArrayList<>();

  for (int id1 = 0; id1 < 100; id1 ++) {
   MemoryTestBean2 mtb = new MemoryTestBean2();

   Assert.assertEquals(mtb.getStatus(), PersistentBean.Status.empty, "Status for bean isn't empty!");

   mtb.set("prop1", "String" + id++);

   Assert.assertEquals(mtb.getStatus(), PersistentBean.Status.dirty, "Status for bean isn't dirty!");

   mtb.save();

   Assert.assertEquals(mtb.getStatus(), PersistentBean.Status.clean, "Status for bean " + mtb.getKeyValue() + " isn't clean!");

   keys.add(new KeyValue((Integer)mtb.getKeyValue(), mtb.get("prop1")));
  }

  for (KeyValue key : keys) {
   MemoryTestBean2 b = new MemoryTestBean2();

   b.load(key.key);

   Assert.assertEquals(b.getStatus(), PersistentBean.Status.clean, "Status for bean " + key.key + " isn't clean!");
   Assert.assertEquals(b.get("prop1"), key.value, "Unexpected property value " + key.value + " for bean " + key.key + "!");

   b.delete();
  }

 }



 /**
  * Test dropping a source for MemoryBeans.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testMemoryBeanDrop() {

  // Prepare some beans.

  MemoryTestBean2 mtb = new MemoryTestBean2();
  mtb.set("prop1", "String1");
  mtb.save();

  mtb = new MemoryTestBean2();
  mtb.set("prop1", "String2");
  mtb.save();

  // Check if saved.

  Assert.assertEquals(mtb.getAll().size(), 2, "Not all beans saved!");

  // Drop the source.

  MemoryBean.drop("MemoryTestBean2");

  // All beans have to be gone away.

  Assert.assertEquals(mtb.getAll().size(), 0, "Huh, there are beans!");

 }



 private static class KeyValue {

  Integer key;
  Object value;

  KeyValue(Integer key, Object value) {
   this.key = key;
   this.value = value;
  }
 }

}
