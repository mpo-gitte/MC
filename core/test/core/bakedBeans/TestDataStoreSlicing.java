package core.bakedBeans;



import core.base.MeterMaid;
import core.servlet.BaseConfig;
import jakarta.servlet.http.HttpServletRequest;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;



/**
 * Test DataStore slicing.
 *
 * @version 2023-10-21
 * @author lp
 */

public class TestDataStoreSlicing {



 @BeforeSuite()
 public void init() {

  System.setProperty(
   "MergedPropertyResourceBundle.bundleName",
   "TestMergedPropertyResourceBundle"
  );

  // No monitoring in tests.

  (new MeterMaid()).init(new BaseConfig((HttpServletRequest)null, null, null, null) {
   @Override
   public String getInitParameter(String parameter) {
    return null;
   }
   @Override
   public String getInitParameter(String parameter, String defValue) {
    return null;
   }
   @Override
   public String getVelocityProperty(String parameter, String defValue) {
    return null;
   }
  });

 }



 /**
  * Test slicing of DataStores.
  */

 @Test()
 public void testDataStoreSlicing() {

  DataStore<MemoryTestBean1> ds =
   DataStoreManager.get().dataStore(ListDataStore.class, "cat1")
    .sourceList((o, c) -> {
     BeanList<MemoryTestBean1> result = new BeanList<>(MemoryTestBean1.class);

     for (int i = 0; i < c; i++) {
      MemoryTestBean1 m = BeanFactory.make(MemoryTestBean1.class);
      m.set("id", i);
      result.add(m);
     }

     return
      result;
    })
    .make(MemoryTestBean1.class);

  // Read 2 beans to the beginning of the list.

  BeanList<MemoryTestBean1> l =
   ds.read(0, 2);

  // Size of list has to be two!

  Assert.assertEquals(l.size(), 2, "Not enough beans!");

  // Next read 4 beans to offset 5 of the list.

  l =
   ds.read(5, 4);

  // Size of the list now has to be 9!

  Assert.assertEquals(l.size(), 9, "Not enough beans!");

  // We produced a gap in the list. Positions 2, 3 and 4 have to be empty!

  for (int i = 2; i <= 4; i++)
   Assert.assertNull(l.get(i), "Unexpected Bean!");

  // Now we're going to close the gap.

  l =
   ds.read(1, 5);  // Overlapping.

  // Gap has to be filled!

  for (int i = 2; i <= 4; i++)
   Assert.assertNotNull(l.get(i), "Unexpected Bean!");

 }

}
