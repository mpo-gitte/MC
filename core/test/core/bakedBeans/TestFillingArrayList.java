package core.bakedBeans;

import core.util.FillingArrayList;
import org.testng.Assert;
import org.testng.annotations.Test;
import setup.Common;


/**
 * Tests the FillingArrayList.
 *
 * @version 2023-10-22
 * @author lp
 */

public class TestFillingArrayList {



 /**
  * Tests adding properties.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testFillingArrayListNormal() {

  FillingArrayList<String> fl = new FillingArrayList<>();

  fl.add("4711");
  fl.add("4712");

  Assert.assertEquals(fl.size(), 2, "Size wrong!");  // Normal list behaviour.

 }



 /**
  * Test setting behind the end.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testFillingArrayListBehind() {

  FillingArrayList<String> fl = new FillingArrayList<>();

  fl.setFilled(9, "Test");

  Assert.assertEquals(fl.size(), 10);  // Element given.

  for (int i = 0; i < fl.size() - 1; i ++)
   Assert.assertNull(fl.get(i));

 }



 /**
  * Test setting behind the end.
  */

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testFillingArrayListSeekAndPut() {

  FillingArrayList<String> fl = new FillingArrayList<>();


  // Seek to the start of the list.

  fl.seek(0);

  Assert.assertEquals(fl.size(), 0);

  fl.put("Test 1");

  Assert.assertEquals(fl.size(), 1);

  Assert.assertTrue(checkContent(fl, true), "Content wrong!");


  // Seek somewhere behind end.

  fl.seek(10);

  Assert.assertEquals(fl.size(), 10);

  fl.put("Test 2");

  Assert.assertEquals(fl.size(), 11);

  Assert.assertTrue(
   checkContent(
    fl,
    true, false, false, false, false, false, false, false, false, false, true
   ), "Content wrong!");

  fl.put("Test 3");

  Assert.assertEquals(fl.size(), 12);

  fl.put("Test 4");

  Assert.assertEquals(fl.size(), 13);


  // Seek somewhere in the middle.

  fl.seek(5);

  fl.put("Test 5");

  Assert.assertEquals(fl.size(), 13);

  fl.put("Test 6");

  Assert.assertEquals(fl.size(), 13);

  Assert.assertTrue(
   checkContent(
    fl,
    true, false, false, false, false, true, true, false, false, false, true
   ), "Content wrong!");

 }



 private boolean checkContent(FillingArrayList<String> l, boolean... test) {

  for (int i = 0 ; i < l.size()  &&  i < test.length; i ++) {
   if (test[i]  &&  l.get(i) != null)
    continue;
   if (!test[i]  &&  l.get(i) == null)
    continue;
   return false;
  }

  return true;

 }

}
