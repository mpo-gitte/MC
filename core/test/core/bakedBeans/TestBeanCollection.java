package core.bakedBeans;



import core.exceptions.MCException;
import org.testng.Assert;
import org.testng.annotations.Test;
import setup.Common;


/**
 * Tests the BeanCollection.
 *
 * @version 2023-09-13
 * @author lp
 *
 */

public class TestBeanCollection {

 private final int SIZE = 10;

 @Test(groups = {setup.Common.TEST_GROUP_NORMAL})
 public void testsCreatingBeanCollection() throws InterruptedException {

  BeanCollection<MemoryTestBean1> col = new BeanCollection<>(MemoryTestBean1.class);

  // Fill collection with some beans.

  for (int id = 0; id < SIZE; id ++) {
   MemoryTestBean1 bean = new MemoryTestBean1();
   bean.set("prop1", id);
   bean.save();
   col.addInitial(bean);
  }


  // Test size.

  Assert.assertEquals(col.size(), SIZE);


  // Create a Threads which add some more beans to the collection.

  TestThread tt1 = new TestThread(col, 5);
  tt1.start();

  TestThread tt2 = new TestThread(col, 8);
  tt2.start();


  // Wait for threads to get done.

  tt1.join();
  tt2.join();


  // Check sizes of collection for each thread.

  Assert.assertEquals(tt1.getSize(), SIZE + 5);
  Assert.assertEquals(tt2.getSize(), SIZE + 8);

 }



 private class TestThread extends Thread {

  private final BeanCollection<MemoryTestBean1> col;
  private final int add;
  private int size;


  TestThread(BeanCollection<MemoryTestBean1> col, int add) {

   this.col = col;
   this.add = add;

  }

  public void run() {

   // Add some more beans.

   for (int id = 0; id < add; id ++) {
    MemoryTestBean1 bean = new MemoryTestBean1();
    try {
     bean.set("prop1", id);
     bean.save();
    }
    catch (Exception e) {
     throw new MCException(e);
    }
    col.add(bean);  // Use add() to ensure to get a thread local clone.
   }

   size = col.size();  // Store size for later test.

  }



  public int getSize() {

   return size;

  }

 }

}
