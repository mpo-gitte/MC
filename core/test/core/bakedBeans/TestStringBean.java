package core.bakedBeans;



/**
 * A StringBean for the hierarchical save test.
 *
 * @version 2018-02-25
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   name = "prop1",
   pclass = Integer.class
  ),
  @PropertyDescription(
   name = "prop2",
   pclass = String.class
  )
 }
)

public class TestStringBean extends StringBean {

}
