package setup;

import core.base.MeterMaid;
import core.servlet.BaseConfig;
import core.util.DB;
import core.util.LiquibaseSupport;
import jakarta.servlet.http.HttpServletRequest;
import org.testng.annotations.BeforeGroups;

import java.util.Arrays;



/**
 * Some initializations for tests
 *
 * @version 2024-05-08
 * @author lp
 */

public class Common {

 public static final String TEST_GROUP_NORMAL = "TestNormal";
 public static final String TEST_GROUP_WITH_DATABASE = "TestWithDatabase";



 /**
  * Initializes some common things.
  */

 @BeforeGroups(groups = {Common.TEST_GROUP_NORMAL, Common.TEST_GROUP_WITH_DATABASE})
 public void initBase() {

  // Set system property for name of MergedPropertyResourceBundle properties file.

  System.setProperty(
   "MergedPropertyResourceBundle.bundleName",
   "TestMergedPropertyResourceBundle"
  );


  // No monitoring in tests.

  (new MeterMaid()).init(new BaseConfig((HttpServletRequest)null, null, null, null) {
   @Override
   public String getInitParameter(String parameter) {
    return null;
   }
   @Override
   public String getInitParameter(String parameter, String defValue) {
    return null;
   }
   @Override
   public String getVelocityProperty(String parameter, String defValue) {
    return null;
   }
  });

 }



 /**
  * Initializes some db stuff.
  */

 @BeforeGroups(groups = TEST_GROUP_WITH_DATABASE)
 public void initDB() {

  // Drop all tables.

  try {
   DB.get().dropObjects(false, Arrays.asList(
    "dataTypeMapping",
    "subbean",
    "masterbean",
    "databasechangelog",
    "databasechangeloglock",
    "testjdbcdatastorebean"
   ));
   DB.get().dropObjects(true, Arrays.asList(
    "dataTypeMapping_id",
    "masterbean_id",
    "subbean_id"
   ));
  }
  catch (Exception e) {
   // Doesn't bother us. Problems occur later.
  }

  LiquibaseSupport.init(DB.get(), "core/db/");

 }

}
