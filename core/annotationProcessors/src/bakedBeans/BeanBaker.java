package bakedBeans;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;



/**
 * Annotation processor for baking beans.
 *
 * @version 2024-12-20
 * @author lp
 */

@SupportedAnnotationTypes("core.bakedBeans.TypedBeanDescription")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
public class BeanBaker extends bakedBeans.BaseAnnotationProcessor {

 private ProcessingEnvironment processingEnv;



 @Override
 public void init(ProcessingEnvironment processingEnv) {

  super.init(processingEnv);

  this.processingEnv = processingEnv;

 }



 @Override
 public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

  for (TypeElement annotation : annotations) {
   for (Element annotatedElement : roundEnv.getElementsAnnotatedWith(annotation)) {

    if (annotatedElement.getKind() == ElementKind.CLASS) {
     TypeElement ce = (TypeElement)annotatedElement;
     PackageElement pe = (PackageElement)ce.getEnclosingElement();

     BeanDescription bd = new BeanDescription();
     bd.setBaseName(ce.getQualifiedName().toString());
     bd.setPack(pe.getQualifiedName().toString());
     bd.setGenerator("bakedBeans.BeanBaker");
     bd.setDate(new Date().toString());
     bd.setComment("Built by V.1.10 of the marvelous BeanBaker!");

     try {
      for (AnnotationMirror am : ce.getAnnotationMirrors()) {
       if ("core.bakedBeans.TypedBeanDescription".equals(am.getAnnotationType().toString())) {

        boolean generate = false;

        for (ExecutableElement el1 : am.getElementValues().keySet()) {
          String sd = el1.getSimpleName().toString();

          if ("bakeBean".equals(sd)) {
           //  Switch for baking beans.
           mess.printMessage(Diagnostic.Kind.NOTE, "BeanBaker baking class \"" + bd.getName() + "\"");
           generate = true;
          }
          else if ("properties".equals(sd)) {
           AnnotationValue props = am.getElementValues().get(el1);
           List l = (List)props.getValue();

           // Scan bean properties.
           for (Object o : l) {
            AnnotationMirror am1 = (AnnotationMirror)o;
            if ("core.bakedBeans.PropertyDescription".equals(am1.getAnnotationType().toString())) {
             PropertyDescription pd = new PropertyDescription();

             for (ExecutableElement el2 : am1.getElementValues().keySet()) {
              String propArg = el2.getSimpleName().toString();
              AnnotationValue p = am1.getElementValues().get(el2);
              String val = clean(p.toString());

              if ("name".equals(propArg))
               pd.setName(val);
              else if ("alias".equals(propArg))
               pd.setAlias(val);
              else if ("pclass".equals(propArg))
               pd.setPclass(val);
              else if ("pclassPrefix".equals(propArg))
               pd.setPclassPrefix(val + ".");
              else if ("iclass".equals(propArg))
               pd.setIclass(val);
              else if ("iclassPrefix".equals(propArg))
               pd.setIclassPrefix(val + ".");
              else if ("dontBakeAliasSetter".equals(propArg))
               pd.setDontBakeAliasSetter(true);
              else if ("dontBakeAliasGetter".equals(propArg))
               pd.setDontBakeAliasGetter(true);
              else if ("bakeNameSetter".equals(propArg))
               pd.setBakeNameSetter(true);
              else if ("bakeNameGetter".equals(propArg))
               pd.setBakeNameGetter(true);

             }
             bd.addPropertyDescription(pd);
            }
           }
          }
         }

        if (generate)
         bakeBean(bd);

       }
      }

     }
     catch (Exception e) {
      mess.printMessage(Diagnostic.Kind.ERROR, "Cannot bake Bean \"" + bd.getName() + "\"!");
      return true;
     }
    }
    else {
     mess.printMessage(Diagnostic.Kind.ERROR, "Annotation \"TypedBeanDescription\" only allowed for classes!");
     return true;
    }
   }
  }

  return false;  // ok

 }


 private String clean(String val) {

  if (val.startsWith("\""))
   return val.substring(1, val.length() - (val.endsWith("\"") ? 1 : 0));

  return val;

 }


 private void bakeBean(BeanDescription bd) {

  int errpos = 0;

  try {
   errpos = 1;
   VelocityEngine velo = makeVelo();

   errpos = 2;
   VelocityContext vc = new VelocityContext();

   vc.put("beanDescription", bd);

   errpos = 3;
   InputStreamReader reader = new InputStreamReader(this.getClass().getResourceAsStream("BeanBaker/GenerateBean.vm"));

   errpos = 4;
   JavaFileObject jfo = processingEnv.getFiler().createSourceFile(bd.getBakedPack() + "." + bd.pack + "." + bd.getName());

   errpos = 5;
   Writer writer = jfo.openWriter();

   errpos = 6;
   velo.evaluate(vc, writer, "test", reader);

   errpos = 7;
   writer.close ();
  }
  catch (Exception e) {
   mess.printMessage(Diagnostic.Kind.ERROR, "BeanBaker has problems with Velocity: " + e.toString() + " on position " + errpos);
  }

 }



 private VelocityEngine makeVelo() {

  VelocityEngine velo = new VelocityEngine();

  velo.setProperty("runtime.log.logsystem.class","org.apache.velocity.runtime.log.SystemLogChute");

  velo.init();

  return velo;

 }



 public static class BeanDescription {

  private String baseName;
  private String pack;
  private List<PropertyDescription> propertyDescriptions = null;
  private String comment;
  private String date;
  private String generator;


  public String getPack() {
   return pack;
  }



  public void setPack(String pack) {
   this.pack = pack;
  }



  public String getName() {
   return baseName;
  }


  public String getBakedPack() {
   return "baked";
  }



  public String getBaseName() {
   return baseName;
  }



  public void setBaseName(String baseName) {
   String[] p = baseName.split("\\.");
   this.baseName = p[p.length - 1];
  }



  public void addPropertyDescription(PropertyDescription propDescr) {

   if (propertyDescriptions == null)
    propertyDescriptions = new ArrayList<PropertyDescription>();

   propertyDescriptions.add (propDescr);
  }


  public List<PropertyDescription> getPropertyDescriptions() {

   return propertyDescriptions;
  }



  public String getComment() {

   return comment;
  }



  public void setComment(String comment) {

   this.comment = comment;
  }



  public String getDate() {

   return date;
  }



  public void setDate(String date) {

   this.date = date;
  }



  public String getGenerator() {

   return generator;
  }



  public void setGenerator(String generator) {

   this.generator = generator;
  }
 }



 public static class PropertyDescription {

  private String alias;
  private String name;
  private String pclass;
  private String pclassPrefix;
  private String iclass;
  private String iclassPrefix;
  private boolean dontBakeAliasSetter;
  private boolean dontBakeAliasGetter;
  private boolean bakeNameSetter;
  private boolean bakeNameGetter;



  public boolean getDontBakeAliasSetter() {
   return dontBakeAliasSetter;
  }



  public void setDontBakeAliasSetter(boolean dontBakeAliasSetter) {
   this.dontBakeAliasSetter = dontBakeAliasSetter;
  }



  public boolean getDontBakeAliasGetter() {
   return dontBakeAliasGetter;
  }



  public void setDontBakeAliasGetter(boolean dontBakeAliasGetter) {
   this.dontBakeAliasGetter = dontBakeAliasGetter;
  }



  public boolean getBakeNameSetter() {
   return bakeNameSetter;
  }



  public void setBakeNameSetter(boolean bakeNameSetter) {
   this.bakeNameSetter = bakeNameSetter;
  }



  public boolean getBakeNameGetter() {
   return bakeNameGetter;
  }



  public void setBakeNameGetter(boolean bakeNameGetter) {
   this.bakeNameGetter = bakeNameGetter;
  }



  public String getAlias() {
   return alias;
  }



  public void setAlias(String alias) {
   this.alias = alias;
  }



  public String getName() {
   return name;
  }



  public void setName(String name) {
   this.name = name;
  }



  public String getPclass() {
   return pclass;
  }



  public void setPclass(String pclass) {
   this.pclass = stripClassSuffix(pclass);
  }



  public String getIclass() {
   return iclass;
  }



  public void setIclass(String iclass) {
   this.iclass = stripClassSuffix(iclass);
  }



  public String getPclassPrefix() {

   return pclassPrefix;

  }



  public void setPclassPrefix(String pclassPrefix) {

   this.pclassPrefix = pclassPrefix;

  }



  public String getIclassPrefix() {

   return iclassPrefix;

  }



  public void setIclassPrefix(String iclassPrefix) {

   this.iclassPrefix = iclassPrefix;

  }



  private String stripClassSuffix(String val) {

   if (val.endsWith(".class"))
    return val.substring(0, val.length() - 6);

   return val;

  }

 }

}
