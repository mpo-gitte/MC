package bakedBeans;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;



/**
 * Annotation processor for checking setters.
 *
 * @version 2023-08-18
 * @author lp
 */

@SupportedAnnotationTypes("bakedBeans.TypedBeanSetter")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
public class TypedBeanSetterProcessor extends bakedBeans.MethodCheckerProcessor {



 /**
  * A TypedBeanSetter must be void and accept a PropertyInfo, a String and an Object.
  *
  * @param processingEnv processing environment
  */

 @Override
 public void init(ProcessingEnvironment processingEnv) {

  super.init(processingEnv);

  whatToCheck = "TypedBeanSetter";

  parameterTypes = new String[] {
   "bakedBeans.PropertyInfo",
   "java.lang.Object"
  };

  returnType = "void";

 }

}
