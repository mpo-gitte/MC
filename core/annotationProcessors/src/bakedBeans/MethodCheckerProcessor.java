package bakedBeans;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.util.Set;



/**
 * Provides functionality to check annotated methods in a class.
 *
 * @version 2016-03-29
 * @author pilgrim.lutz@imail.de
 */

public abstract class MethodCheckerProcessor extends BaseAnnotationProcessor {

 String [] parameterTypes = null;
 String returnType = null;
 String whatToCheck = null;



 /**
  * Process annotations.
  *
  * @param annotations The annotations
  * @param roundEnv The environment for this round.
  *
  * @return false if method is ok. True otherwise.
  */

 @Override
 public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

  for (TypeElement annotation : annotations) {
   for (Element annotatedElement : roundEnv.getElementsAnnotatedWith(annotation)) {
    if (annotatedElement.getKind() == ElementKind.METHOD) {
     ExecutableElement ex = (ExecutableElement)annotatedElement;
     if (processMethod(ex))
       return true;  // Something is seriously wrong.
    }
    else {
     mess.printMessage(Diagnostic.Kind.ERROR, "Annotation \"" + whatToCheck + "\" only allowed for methods!");
     return true;
    }
   }
  }
  return false;  // Ok.
 }



 /**
  * Checks a method.
  *
  * @param ex The method.
  * @return false if method is ok. True otherwise.
  */

 protected boolean processMethod(ExecutableElement ex) {

  // Checking the return type.
  if (processMethodReturnType(ex)) return true;

  // Checking the parameters.
  if (processMethodParameters(ex)) return true;

  return false; // Ok, method is nice.
 }



 /**
  * Checks the methods parameters.
  *
  * <p>Checks the method parameters against the array parameterTypes.</p>
  *
  * @param ex The method.
  * @return false if parameter are ok. True otherwise.
  */

 protected boolean processMethodParameters(ExecutableElement ex) {

  if (parameterTypes.length != ex.getParameters().size()) {
   mess.printMessage(
    Diagnostic.Kind.ERROR,
    "Method \"" + ex.toString() + "\" has wrong signature for a " + whatToCheck + "! Size of parameter list has to be " + parameterTypes.length + "."
   );
   return true;
  }

  int id = 0;
  for (VariableElement ve : ex.getParameters()) {
   TypeMirror tm = ve.asType();
   if (!parameterTypes[id].equals(tm.toString())) {
    mess.printMessage(
     Diagnostic.Kind.ERROR,
     "Method \"" + ex.toString() + "\" has wrong signature for a " + whatToCheck + "! Parameter " + id + " has to be \"" + parameterTypes[id] + "\" instead of \"" + tm.toString() + "\"."
    );
    return true;
   }
   id ++;
  }
  return false;

 }



 /**
  * Checks the methods return type.
  *
  * The return type for the method is given in returnType.
  *
  * @param ex The method.
  * @return false if return type is ok. True otherwise.
  */

 protected boolean processMethodReturnType(ExecutableElement ex) {

  String rt = ex.getReturnType().toString();

  if (!returnType.equals(rt)) {
   mess.printMessage(
    Diagnostic.Kind.ERROR,
    "Method \"" + ex.toString() + "\" has wrong return type for a " + whatToCheck + "! Should be \"" + returnType + "\" instead of \"" + rt + "\"."
   );
   return true;
  }
  return false;

 }

}
