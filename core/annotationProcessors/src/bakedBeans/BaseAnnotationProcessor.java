package bakedBeans;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;



/**
 * Base class for all annotation processors
 *
 * @version 2023-05-17
 * @author pilgrim.lutz@imail.de
 */

public abstract class BaseAnnotationProcessor extends AbstractProcessor {

 Messager mess = null;



 /**
  * Initializer.
  * 
  * <p>Gets a messager currently.</p>
  *
  * @param processingEnv processing environment
  */

 @Override
 public void init(ProcessingEnvironment processingEnv) {

  super.init(processingEnv);

  mess = processingEnv.getMessager();  // Get a messager for us.

 }

}
