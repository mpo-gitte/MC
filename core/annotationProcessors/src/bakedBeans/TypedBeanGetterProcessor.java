package bakedBeans;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;



/**
 * Annotation processor for checking getters.
 *
 * @version 2023-08-18
 * @author lp
 */

@SupportedAnnotationTypes("bakedBeans.TypedBeanGetter")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
public class TypedBeanGetterProcessor extends bakedBeans.MethodCheckerProcessor {



 /**
  * A TypedBeanGetter must return an Object and accept a PropertyInfo and a String.
  *
  * @param processingEnv processing environment
  */

 @Override
 public void init(ProcessingEnvironment processingEnv) {

  super.init(processingEnv);

  whatToCheck = "TypedBeanGetter";

  parameterTypes = new String[] {
   "bakedBeans.PropertyInfo"
  };

  returnType = "java.lang.Object";

 }

}
