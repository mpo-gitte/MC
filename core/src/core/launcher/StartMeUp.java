package core.launcher;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.ha.session.ClusterSessionListener;
import org.apache.catalina.ha.session.JvmRouteBinderValve;
import org.apache.catalina.ha.tcp.ReplicationValve;
import org.apache.catalina.ha.tcp.SimpleTcpCluster;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.tribes.group.GroupChannel;
import org.apache.catalina.tribes.group.interceptors.MessageDispatchInterceptor;
import org.apache.catalina.tribes.group.interceptors.TcpFailureDetector;
import org.apache.catalina.tribes.group.interceptors.TcpPingInterceptor;
import org.apache.catalina.tribes.membership.McastService;
import org.apache.catalina.tribes.membership.MembershipServiceBase;
import org.apache.catalina.tribes.transport.ReplicationTransmitter;
import org.apache.catalina.tribes.transport.nio.NioReceiver;
import org.apache.catalina.tribes.transport.nio.PooledParallelSender;
import org.apache.catalina.tribes.membership.cloud.CloudMembershipService;

import java.io.IOException;
import java.nio.file.Files;



/**
 * Launcher for starting MC standalone with an embedded Tomcat.
 *
 * @version 2023-01-14
 * @author pilgrim.lutz@imail.de
 */

public class StartMeUp {



 /**
  * Main Method.
  *
  * @param args The command line.
  *
  * @throws LifecycleException Live isn't always easy!
  */

 public static void main(String[] args) throws LifecycleException, IOException {


  // Create Tomcat.

  Tomcat tommy = new Tomcat();  // ... can you hear me?


  // Port is 8080 if not given on command line.

  tommy.setPort(Integer.parseInt(getParameter(args, "httpPort", "8080")));


  // Create connectors.

  // This creates the default HTTP-Connector.
  tommy.getConnector();  // Can you feel me near you?

  // Create the ajp connector.
  int ajpPort = Integer.parseInt(getParameter(args, "ajpPort", "0"));
  if (ajpPort > 0) {
   Connector ajpConnector = new Connector("AJP/1.3");
   ajpConnector.setPort(ajpPort);
   ajpConnector.setRedirectPort(8443);
   ajpConnector.setSecure(false);
   ajpConnector.setAllowTrace(false);
   ajpConnector.setProperty("secretRequired", "false");
   ajpConnector.setProperty("address", "0.0.0.0");  // Enable IPV4 and IPV6.

   tommy.getService().addConnector(ajpConnector);
  }


  // Get path of War.

  String warFilePath =
   StartMeUp.class.getProtectionDomain().getCodeSource().getLocation().getPath();


  // Context path is base name of War. If not given on command line.

  String contextPath = getParameter(args, "contextPath", null);
  if (contextPath == null) {
   String[] p = warFilePath.split("/");
   String s = p[p.length - 1];
   contextPath = s.substring(0, s.length() - 4);
  }

  Context context = tommy.addWebapp("/" + contextPath, warFilePath);  // ... can you see me?


  // Clustering?

  if (Boolean.parseBoolean(getParameter(args, "clustering", "false")))
   enableClustering(args, tommy, context);


  // Start tomcat.

  tommy.setBaseDir(Files.createTempDirectory(contextPath + "-").toString());

  tommy.getHost().setAppBase(".");

  tommy.start();  // Can I help to cheer you?

  tommy.getServer().await();

 }



 private static void enableClustering(String[] args, Tomcat tommy, Context context) {

  // Don't forget:
  // firewall-cmd --direct --add-rule ipv4 filter INPUT 0 -m udp -p udp -m pkttype --pkt-type multicast -j ACCEPT
  // firewall-cmd --reload

  SimpleTcpCluster cluster = new SimpleTcpCluster();

  // Enter / create a group.

  GroupChannel channel = new GroupChannel();
  channel.setMembershipService(getMembershipService(args));

  // Settings for receiving.

  NioReceiver nioReceiver = new NioReceiver();
  nioReceiver.setAddress("auto");
  nioReceiver.setMaxThreads(6);
  nioReceiver.setPort(4000);
  channel.setChannelReceiver(nioReceiver);

  // Enable session replication

  ReplicationTransmitter sender = new ReplicationTransmitter();
  sender.setTransport(new PooledParallelSender());
  channel.setChannelSender(sender);
  cluster.addValve(new ReplicationValve());
  cluster.addValve(new JvmRouteBinderValve());


  channel.addInterceptor(new TcpPingInterceptor());
  channel.addInterceptor(new TcpFailureDetector());
  channel.addInterceptor(new MessageDispatchInterceptor());

  cluster.addClusterListener(new ClusterSessionListener());
  cluster.setChannel(channel);

  tommy.getEngine().setCluster(cluster);

  context.setDistributable(true);
  context.setManager(context.getCluster().createManager(""));
  tommy.getEngine().setJvmRoute(getParameter(args, "clusterSetJvmRoute", context.getPath()));

 }



 private static MembershipServiceBase getMembershipService(String[] args) {

  String mode = getParameter(args, "clusterBy", "multicast");

  if ("multicast".equals(mode)) {
   McastService mcastService = new McastService();
   mcastService.setAddress(getParameter(args, "clusterMulticastAddress", "228.0.0.4"));  // Address and port define membership
   mcastService.setPort(Integer.parseInt(getParameter(args, "clusterMulticastPort", "45564")));  // in a cluster.
   mcastService.setFrequency(500);
   mcastService.setDropTime(3000);

   return mcastService;
  }
  else if ("kubernetes".equals(mode))
   return new CloudMembershipService();
  else
   throw new IllegalArgumentException("Ill. membership mode!");

 }



 private static String getParameter(String[] args, String param, String defaultValue) {

  for (String arg : args) {

   String[] sd = arg.split("=");

   if (("--" + param).equals(sd[0]))
    return sd[1];
  }

  String env = System.getenv().get("MC_" + param);

  if (env != null  && !env.isEmpty())
   return env;

  return defaultValue;

 }

}
