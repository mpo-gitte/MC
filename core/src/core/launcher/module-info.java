/**
 * Module definition for launcher.
 *
 * @version 2023-01-14
 * @author lp
 */

module launcher {
 requires java.management;  // Tomcat needs it.
}
