package core.configs;

import core.base.Request;
import core.util.Configurator;


public class ConfigLocale implements Configurator.Config {



 /**
  * Config value for all method parameters marked with RequestConfig annotation.
  *
  * @param field The field.
  * @return The Request.
  *
  * @see core.util.RequestConfig
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_ANNOTATION, value= ".*LocaleConfig.*")
 public Object getLocale(Configurator.ConfigurableField field) {

  return Request.get().getLocale();

 }

}
