package core.configs;

import core.util.Configurator;
import org.apache.logging.log4j.LogManager;



public class ConfigLogger implements Configurator.Config {



 /**
  * Config value for all Loggers.
  *
  * @param field The field.
  * @return A logger for the fields containing class or for the class given by the WorkerContext annotation.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= ".*Logger.*logger.*")
 public Object getLogger(Configurator.ConfigurableField field) {

  @SuppressWarnings("unchecked")
  Class<? extends Object> clazz = field.getContainingClass();

  Configurator.Context wc = clazz.getAnnotation(Configurator.Context.class);

  if (wc != null)
   clazz = wc.value();

  return LogManager.getLogger(clazz);

 }

}
