package core.configs;

import core.bakedBeans.DataStoreManager;
import core.util.Configurator;


public class ConfigDataStoreManager implements Configurator.Config {



 /**
  * Config value for all Loggers.
  *
  * @param field The field.
  * @return A logger for the fields containing class.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= ".*DataStoreManager.*dataStoreManager.*")
 public Object getDataStoreManager(Configurator.ConfigurableField field) {

  return DataStoreManager.get();

 }

}
