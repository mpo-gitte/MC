package core.configs;

import core.util.Configurator;
import core.util.ObjectManager;

import java.lang.reflect.Field;


public class ConfigWorkerHelper implements Configurator.Config {



 /**
  * Config value for all worker helpers.
  *
  * @param field The field.
  * @return A fully initialized worker helper.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_ANNOTATION, value= ".*WorkerHelper.*")
 public ObjectManager.IObject getWorkerHelper(Configurator.ConfigurableField field) {

  Field fr = field.getField();

  @SuppressWarnings("unchecked")
  Class<? extends ObjectManager.IObject> helperClass =
   (Class<? extends ObjectManager.IObject>)fr.getType();

  return
   ObjectManager.get().getObject(helperClass);

 }

}
