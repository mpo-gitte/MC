package core.configs;

import core.base.MeterMaid;
import core.util.Configurator;



public class ConfigMeterMaid implements Configurator.Config {



 /**
  * Config value for all MeterMaid.
  *
  * @param field The field.
  * @return A MeterMaid.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= ".*MeterMaid.*lovelyRita.*")
 public Object getMeterMaid(Configurator.ConfigurableField field) {

  return MeterMaid.get();

 }

}
