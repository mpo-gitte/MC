package core.configs;

import core.util.Configurator;
import core.util.DB;


public class ConfigDB implements Configurator.Config {



 /**
  * Config value for all Loggers.
  *
  * @param field The field.
  * @return A logger for the fields containing class.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_NAME, value= ".*DB.*db.*")
 public Object getDB(Configurator.ConfigurableField field) {

  return DB.get();

 }

}
