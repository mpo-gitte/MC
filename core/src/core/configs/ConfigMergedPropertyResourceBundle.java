package core.configs;

import core.util.Configurator;
import core.util.MergedPropertyResourceBundle;
import core.util.Property;
import core.util.Utilities;

import java.lang.reflect.Field;
import java.util.*;



public class ConfigMergedPropertyResourceBundle implements Configurator.Config {



 /**
  * Config value for all Properties.
  *
  * @param field The field.
  * @return A MergedPropertyResourceBundle.
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_ANNOTATION, value= ".*Property.*")
 public Object getProperty(Configurator.ConfigurableField field) {

  Field fr = field.getField();

  String rbn = fr.getAnnotation(Property.class).value();

  String[] rpath = rbn.split("\\.");
  ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle(rpath[0]);

  if (rpath.length == 1)
   return rb;

  String sd = rbn.substring(rbn.indexOf('.') + 1);

  return makeGeneralValue(Utilities.getParam(rb, sd), fr.getType(), String.class);

 }



 private static Object makeGeneralValue(String val, Class returnClass, Class innerClass) {

  switch (returnClass.getName()) {

   case "int":
    returnClass = Integer.class;
    break;

   case "boolean":
    returnClass = Boolean.class;
    break;

   case "java.util.List": {
    List<Object> res = new ArrayList<>();
    for (String v : Utilities.split(val))
     res.add(Utilities.makeGeneralValue(v, innerClass));

    return res;
   }

   case "java.util.Map": {
    Map<Object, Object> res = new HashMap<>();
    for (String v : Utilities.split(val)) {
     List<String> vs = Utilities.split(v, '=');
     res.put(vs.get(0), Utilities.makeGeneralValue(vs.get(1), innerClass));
    }

    return res;
   }

   case "java.util.Locale": {
    return new Locale(val);
   }

  }

  return Utilities.makeGeneralValue(val, returnClass);

 }

}
