package core.exceptions;


import core.base.WorkerManager;

/**
 * This exception is thrown by the WorkerManager if it can't load a Worker.
 *
 * @version 2021-04-06
 * @author pilgrim.lutz@imail.de
 *
 * @see WorkerManager
 */

public class WorkerManagerException extends IllegalStateException {



 /**
  * Standard constructor.
  *
  * @param mess error message.
  */

 public WorkerManagerException(String mess) {

  super(mess);

 }

}
