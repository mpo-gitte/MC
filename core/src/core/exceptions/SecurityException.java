package core.exceptions;



/**
 * Exception thrown for security issues.
 *
 * @version 2013-09-11
 * @author pilgrim.lutz@imail.de
 *
 */

public class SecurityException extends MCException {



 /**
  * Standard constructor.
  *
  * @param message A method for the exception.
  */

 public SecurityException(String message) {

  super(message);

 }



 /**
  * Standard constructor.
  *
  * @param cause A throwable to wrap.
  */

 public SecurityException(Throwable cause) {

  super(cause);

 }

}
