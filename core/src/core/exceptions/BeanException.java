package core.exceptions;



/**
 * Thrown if beans got problems.
 *
 * @version 2023-09-13
 * @author lp
 */

public class BeanException extends MCException {

 private static final long serialVersionUID = 1L;



 /**
  * Create exception.
  *
  * @param mess A message.
  */

 public BeanException(String mess) {

  super(mess);

 }



 /**
  * Creates the exception.
  *
  * @param exception An exception.
  */

 public BeanException(Exception exception) {

  super(exception);

 }

}
