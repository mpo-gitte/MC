package core.exceptions;



/**
 * An exception to signal problems with encryption / decryption.
 *
 * @version 2018-11-17
 * @author pilgrim.lutz@imail.de
 */

public class SecretServiceException extends SecurityException {



 /**
  * Build the exception.
  *
  * @param mess the message to display.
  */

 public SecretServiceException(String mess) {

  super(mess);

 }



 /**
  * Build the exception.
  *
  * @param e A throwable to be wrapped.
  */

 public SecretServiceException(Throwable e) {

  super(e);

 }

}
