package core.exceptions;



/**
 * Exception is thrown if any database problems occur.
 *
 * @version 2023-08-18
 * @author lp
 */

public class DBException extends RuntimeException {

 static final long serialVersionUID = 0;



 /**
  * Creates a new DBException.
  *
  * @param mess The message.
  */

 public DBException(String mess) {

  super(mess);

 }



 /**
  * Creates a new DBException.
  *
  * @param ex An Exception to clo
  */

 public DBException(Exception ex) {

  super(ex);

 }

}
