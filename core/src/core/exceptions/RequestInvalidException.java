package core.exceptions;



import java.util.function.Predicate;



/**
 * An exception to signal illegal access to the application.
 *
 * @version 2013-09-12
 * @author pilgrim.lutz@imail.de
 */

public class RequestInvalidException extends SecurityException {

 private static final long serialVersionUID = 1L;



 /**
  * Build the exception.
  *
  * @param mess the message to display.
  */

 public RequestInvalidException(String mess) {

  super(mess);

 }



 /**
  * Factory method for creating a new RequestException.
  *
  * @param condition Exception is only thrown if condition returns true.
  * @param args Arguments for the constructor of the exception.
  */

 public static void when(Predicate<Void> condition, Object ... args) {

  MCException.when(RequestInvalidException.class, condition, args);

 }

}
