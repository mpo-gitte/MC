package core.exceptions;



/**
 * Thrown if illegal access to a bean is made.
 *
 * @version 2023-09-21
 * @author lp
 */

public class BeanAccessException extends SecurityException {

 private static final long serialVersionUID = 1L;



 /**
  * Create exception.
  *
  * @param mess A message.
  */

 public BeanAccessException(String mess) {

  super(mess);

 }



 /**
  * Creates the exception.
  *
  * @param exception An exception.
  */

 public BeanAccessException(Exception exception) {

  super(exception);

 }

}
