package core.exceptions;



/**
 * Thrown whenever a URL isn't correct.
 *
 * @version 2018-11-10
 * @author pilgrim.lutz@imail.de
 *
 */

public class IllUrlException extends SecurityException {



 public IllUrlException(String mess) {

  super(mess);

 }

}
