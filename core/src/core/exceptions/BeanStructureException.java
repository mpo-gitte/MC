package core.exceptions;



/**
 * Thrown if problems with the structure of a bean occured.
 *
 * @version 2023-08-19
 *
 * @author pilgrim.lutz@imail.de
 */

public class BeanStructureException extends BeanException {

 private static final long serialVersionUID = 1L;



 /**
  * Standard constructor with an error message.
  *
  * @param mess Error message.
  */

 public BeanStructureException(String mess) {

  super(mess);

 }

}
