package core.exceptions;



import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;



/**
 * Base exception for MC.
 *
 * @version 2023-09-11
 * @author pilgrim.lutz@imail.de
 */

public class MCException extends RuntimeException {



 /**
  * Standard constructor.
  *
  * @param mess error message.
  */

 public MCException(String mess) {

  super(mess);

 }



 /**
  * Standard constructor.
  *
  * @param exception an exception to wrap.
  */

 public MCException(Exception exception) {

  super(exception);

 }



 /**
  * Standard constructor.
  *
  * @param throwable a throwable to wrap.
  */

 public MCException(Throwable throwable) {

  super(throwable);

 }



 /**
  * Standard constructor.
  *
  * @param mess error message.
  * @param exception an exception to wrap.
  */

 public MCException(String mess, Exception exception) {

  super(mess, exception);

 }



 /**
  * Factory method for creating a new exception.
  *
  * @param exceptionClass The exception to throw.
  * @param condition Exception is only thrown if condition returns true.
  * @param args Arguments for the constructor of the exception.
  */

 public static void when(Class<? extends MCException> exceptionClass, Predicate<Void> condition, Object ... args) {

  if (condition.test(null)) {
   List<Class<?>> argTypes =
    Arrays.stream(args).map(Object::getClass).collect(Collectors.toList());

   try {
    throw
     (Throwable)exceptionClass.getConstructor(argTypes.toArray(new Class[0])).newInstance(args);
   }
   catch (Throwable e) {
    throw new MCException((Exception) e);
   }
  }
 }



 /**
  * Factory method for creating a new MCException.
  *
  * @param condition Exception is only thrown if condition returns true.
  * @param args Arguments for the constructor of the exception.
  */

 public static void when(Predicate<Void> condition, Object ... args) {

  MCException.when(MCException.class, condition, args);

 }

}
