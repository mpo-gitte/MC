package core.base;



import com.sun.net.httpserver.BasicAuthenticator;
import com.sun.net.httpserver.HttpServer;
import core.exceptions.MCException;
import core.servlet.BaseConfig;
import core.servlet.BaseModuleDescription;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics;
import io.micrometer.core.instrument.binder.system.ProcessorMetrics;
import io.micrometer.prometheusmetrics.PrometheusConfig;
import io.micrometer.prometheusmetrics.PrometheusMeterRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;



/**
 * Helper class for Micrometer and Prometheus.
 *
 * <p>Use a prometheus configuration similar like this:</p>
 *
 * <code>
 *  - job_name: local_puc
 *    metrics_path: /prometheus
 *    static_configs:
 *      - targets: ['localhost:8085']
 *    basic_auth:
 *        username: prometheus
 *        password: password
 * </code>
 *
 * <p>Switch on monitoring with by setting the port.</p>
 *
 * <p>See servlet.properties for some values.</p>
 *
 * @version 2025-02-08
 * @author lp
 */

@BaseModuleDescription(order = 0)
public class MeterMaid implements BaseModule {

 private static final Logger logger = LogManager.getLogger(MeterMaid.class);

 private PrometheusMeterRegistry prometheusRegistry = null;

 private String urlPath;
 private String metricsPrefix;
 private HttpServer server;

 private static MeterMaid me;



 /**
  * Initialize the MeterMaid.
  *
  * @param baseConfig Configuration
  *
  * <p>Takes from the config:</p>
  *
  * <p>
  * urlPath: Path in URL for prometheus endpoint.
  * port: The portnumber (Don't forget to open it in your firewall).
  * user: username for basic auth.
  * password: passwotd for basic auth.
  * metricsPrefix: Prefix for all metrics.
  * </p>
  *
  */

 public void init(BaseConfig baseConfig) {

  this.urlPath = baseConfig.getVelocityProperty("prometheus.path", "/prometheus");
  this.metricsPrefix = baseConfig.getVelocityProperty("prometheus.prefix", "mc");

  String port = baseConfig.getInitParameter("prometheus.port");

  if (port == null)
   logger.info("MeterMaid is switched off!");
  else {
   String user = baseConfig.getInitParameter("prometheus.user");
   String password = baseConfig.getInitParameter("prometheus.password");

   logger.info("MeterMaid initializing on port: \"" + port + "\" and path: \"" + urlPath + "\" for \"" + user + "\"");

   try {

    prometheusRegistry =
     new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);

    new JvmMemoryMetrics().bindTo(prometheusRegistry);
    new ProcessorMetrics().bindTo(prometheusRegistry);
    new JvmThreadMetrics().bindTo(prometheusRegistry);

    prometheusEndpoint(Integer.parseInt(port), user, password);

   } catch (IOException e) {
    throw new MCException(e);
   }
  }


 }



 public MeterMaid() {

  me = this;

 }



 public static MeterMaid get() {

  MCException.when((Void) -> me == null, "MeterMaid not initialized!");

  return me;

 }



 private void prometheusEndpoint(int port, String user, String password) throws IOException {

  if (prometheusRegistry != null) {
   server = HttpServer.create(new InetSocketAddress(port), 0);

   server.createContext(urlPath, httpExchange -> {
    String response = prometheusRegistry.scrape();
    httpExchange.sendResponseHeaders(200, response.getBytes().length);
    try (OutputStream os = httpExchange.getResponseBody()) {
     os.write(response.getBytes());
    }
   }).setAuthenticator(new MeterMaidBasicAuthentication("user", user, password));

   new Thread(server::start).start();

  }

 }



 /**
  * Closes the MeterMaid.
  */

 public void close() {

  me.server.stop(0);

 }



 /**
  * Record an elapsed time.
  *
  * @param name The name of the Micrometer timer.
  * @param tag A tag.
  * @param tagValue A value for the tag.
  * @param start the start time.
  */

 // ToDo: Check calls and eventually eliminate them.
 public void recordElapsedTime(String name, String tag, String tagValue, long start) {

  if (prometheusRegistry !=  null)
   Timer.builder(makeName(name))
    .tag(tag, tagValue)
    .register(prometheusRegistry)
    .record(System.nanoTime() - start, TimeUnit.NANOSECONDS);

 }



 /**
  * Increment a counter with a tag.
  *
  * @param name The name of the counter.
  */

 public void incrementCounter(String name, String tag, String tagValue) {

  if (prometheusRegistry !=  null)
   Counter.builder(makeName(name))
    .tag(tag, tagValue)
    .register(prometheusRegistry)
    .increment();

 }



 /**
  * Increment a counter with a tag.
  *
  * @param name The name of the counter.
  */

 public void incrementCounter(String name, String tag1, String tagValue1, String tag2, String tagValue2) {

  if (prometheusRegistry !=  null)
   Counter.builder(makeName(name))
    .tag(tag1, tagValue1)
    .tag(tag2, tagValue2)
    .register(prometheusRegistry)
    .increment();

 }



 /**
  * Set a gauge.
  *
  * @param name The name of the counter.
  * @param fn A Supplier for the value.
  */

 public void setGauge(String name, Supplier<Number> fn) {

  if (prometheusRegistry !=  null)
   Gauge.builder(makeName(name), fn)
    .register(prometheusRegistry);

 }



 private String makeName(String name) {

  return metricsPrefix + "_" + name;

 }



 private static class MeterMaidBasicAuthentication extends BasicAuthenticator {

  private String user;
  private String password;



  MeterMaidBasicAuthentication(String realm, String user, String password) {

   super(realm);

   this.user = user;
   this.password = password;

  }



  @Override
  public boolean checkCredentials(String user, String password) {

   return user.equals(this.user)  &&  password.equals(this.password);

  }

 }

}
