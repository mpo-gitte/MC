package core.base;

import core.exceptions.WorkerManagerException;
import core.servlet.BaseConfig;
import core.servlet.BaseModuleDescription;
import core.util.Timer;
import core.util.Utilities;
import core.util.*;
import jakarta.el.MethodNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;


/**
 * Class for managing workers.
 *
 * <p>It should live in the application scope of the VelocityView servlet.</p>
 *
 * @version 2024-08-09
 * @author lp
 */

@BaseModuleDescription(name = "workman", order = 2)
public class WorkerManager implements BaseModule {

 private static final String ImplementationSuffix = "Impl";

 private static final Logger logger = LogManager.getLogger(WorkerManager.class);

 private final ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle("WorkerManager");
 private final String workerPacks = rb.getString("WorkerPackages");

 private static HashMap<String, IWorker> workers = null;
 private static HashMap<String, Object> namedWorkers = null;

 private static ThreadLocal<Class<? extends Exception>[]> exceptionsToCatch =
  new ThreadLocal<Class<? extends Exception>[]>() {
   protected synchronized Class<? extends Exception>[] initialValue() {
    return null;
   }
 };

 private static ThreadLocal<Throwable> lastException = new ThreadLocal<Throwable>() {
  protected synchronized Exception initialValue() {
   return null;
  }
 };

 private final Configurator configurator = Configurator.get();

 private static MeterMaid lovelyRita;



 /**
  * Initialize the Messenger.
  *
  * @param baseConfig Configuration.
  *
  */

 public void init(BaseConfig baseConfig) {

  lovelyRita = MeterMaid.get();
  lovelyRita.setGauge("WorkerManager_numberOfWorkers", new WorkerCounter());  // Monitor the number of loaded bean workers.

 }



 public WorkerManager() {

 }



 /**
  * Called by the servlet for initialisation.
  *
  * <p>Loads the workers.</p>
  *
  * @param baseConfig Uses the ServletContext
  *
  * @return Named workers.
  */

 public Map<String, Object> initApplicationScope(BaseConfig baseConfig) {

  loadWorkers();

  return namedWorkers;

 }



 private synchronized void loadWorkers() {

  if (workers == null) {

   workers = new HashMap<>();

   logger.info("loading workers...");

   Utilities.split(rb.getString("LoadOnStart")).forEach(workerDef -> {
    String workerClassName = null;
    String workerName = null;

    for (String np : Utilities.split(workerDef, '=')) {
     workerName = workerClassName;
     workerClassName = np;
    }

    IWorker worker = get(workerClassName);

    if (workerName != null) {
      if (namedWorkers == null)
       namedWorkers = new HashMap<>();
     namedWorkers.put(workerName, worker);
    }

   });

   logger.info("... loading workers ready!");
  }

 }



 /**
  * Fetches a worker.
  *
  * <p>A worker is an object which implements the interface IWorker.</p>
  *
  * <p>A worker has to be thread-safe because it runs in application scope.</p>
  *
  * @param workerName The class name of the worker.
  *
  * @return The requested worker
  */

 public synchronized IWorker get(String workerName) {

  IWorker worker;

  String workerClassName = Utilities.capitalize(workerName);  // A class name should start with a capital.

  if (workers.containsKey(workerClassName))
   worker = workers.get(workerClassName);  // Already known worker.
  else {
   worker = buildWorker(workerClassName);

   if (workers.containsKey(workerClassName))
    worker = workers.get(workerClassName);
   else
    workers.put(workerClassName, worker);
  }

  return worker;

 }



 /**
  * Enables catching the given exceptions during next call of a business method.
  *
  * @param exceptions An array with exceptions to catch-
  */

 public void catchException(Class<? extends Exception>[] exceptions) {

  logger.debug(() -> {
   StringBuilder sb = new StringBuilder();

   sb.append("Preparing to catch exceptions ");

   for (Class ec : exceptions)
    sb.append(ec.getName()).append(' ');

   return sb.toString();
  });

  exceptionsToCatch.set(exceptions);

  lastException.set(null);  // Clear last stored exception.

 }



 /**
  * Enables catching the given exception during next call of a business method.
  *
  * @param exception The class name of the exception to catch.
  */

 @SuppressWarnings("unchecked")
 public void catchException(String exception) throws ClassNotFoundException {

  Class<?>[] ecs = new Class[1];

  ecs[0] = Class.forName(exception);

  catchException((Class<? extends Exception>[])ecs);

 }



 /**
  * Enables catching the given exceptions during next call of a business method.
  *
  * @param exceptions The class names of the exceptions to catch.
  */

 @SuppressWarnings("unchecked")
 public void catchExceptions(List<String> exceptions) throws ClassNotFoundException {

  Class<?>[] ecs = new Class[exceptions.size()];

  int id = 0;
  for (String sd : exceptions)
    ecs[id ++] = Class.forName(sd);

  catchException((Class<? extends Exception>[])ecs);

 }



 /**
  * Returns the last Exception thrown by a Worker method if it was catched with catchException().
  *
  * @return The last thrown exception
  *
  * @see #catchException(Class[])
  * @see #catchException(String)
  */

 public Throwable getLastException() {

  Throwable ex = lastException.get();

  logger.debug(() -> "getLastException(): \"" + ((ex == null) ? "none" : ex.getClass().getName()) + "\"");

  return ex;

 }



 public static Class<? extends Exception>[] getExceptionsToCatch() {

  return exceptionsToCatch.get();

 }



 /**
  * Sets the last raised exception.
  *
  * @param ex The exception.
  */

 public static void setLastException(Throwable ex) {

  lastException.set(ex);

 }



 /**
  * Check the health of the loaded workers.
  *
  * @return true if all workers are healthy. False if only one worker isn't healthy.
  */

 public Boolean checkHealth() {

  return
   workers.entrySet().stream().noneMatch((w) -> w.getValue().healthCheck().equals(IWorker.Health.ill));

 }



 private IWorker buildWorker(final String workerName) throws WorkerManagerException {

  logger.debug(() -> "WorkerManager: Building worker: \"" + workerName + "\"");

  Class<? extends IWorker> workerClass = getWorkerClass(workerName, ImplementationSuffix);

  logger.debug(() -> "WorkerManager: Class loaded: \"" + workerClass.getName() + "\"");

  try {
   IWorker workerInstance = (IWorker)ObjectManager.get().getObject(workerClass);

   handleSpecialMethods(workerClass, workerInstance);

   Class<? extends IWorker> workerInterfaceClass = getWorkerClass(workerName, "");

   logger.debug(() -> "WorkerManager: Interface loaded: \"" + workerInterfaceClass.getName() + "\"");

   // Finally return a proxy with a WorkerHandler.
   return
    (IWorker)Proxy.newProxyInstance(
     workerInterfaceClass.getClassLoader(),
     new Class[]{workerInterfaceClass},
     new WorkerHandler(workerInstance, exceptionsToCatch)
    );

  }
  catch (Exception ex) {
   logger.error("WorkerManager: Error for worker: \"" + workerName + "\"");
   throw
    new WorkerManagerException(ex.toString());
  }

 }



 private Class<? extends IWorker> getWorkerClass(String workerName, String implementationSuffix) {

  for (String workerPack : Utilities.split(workerPacks)) {
   String sd = workerPack + "." + workerName + implementationSuffix;

   try {
    @SuppressWarnings("unchecked")
    Class<? extends IWorker> workerClass = (Class<? extends IWorker>)Class.forName(sd);
    return workerClass;
   }
   catch (ClassNotFoundException ex) {
    if (logger.isDebugEnabled())
     logger.debug("building worker: not possible for: " + sd);
    // Just continue with next package.
   }
  }

  throw
   new WorkerManagerException("Can't find worker \"" + workerName + "\"!");

 }



 static class WorkerHandler implements InvocationHandler {

  private final IWorker workerInstance;
  private final ThreadLocal<Class<? extends Exception>[]> exceptionsToCatch;
  private final HashMap<Method, Link> links = new HashMap<>();
  private final Configurator configurator = Configurator.get();



  WorkerHandler(IWorker workerInstance, ThreadLocal<Class<? extends Exception>[]> exceptionsToCatch) {

   this.workerInstance = workerInstance;
   this.exceptionsToCatch = exceptionsToCatch;

  }



  public Object invoke(Object proxy, Method methodInInterface, Object[] args) throws Exception {

   logger.debug(() -> "Invoking " + workerInstance + "." + methodInInterface.getName());

   Object res = null;

   Link toMethodInWorker = link(methodInInterface);
   Method method = toMethodInWorker.getMethod();

   Object[] methodArgs = getMethodArgs(args, toMethodInWorker, method);

   try {
    res = Aspectator.get().run(workerInstance, methodInInterface, method, methodArgs);
   }
   catch (InvocationTargetException ex) {
    exceptionsToCatch.set(null);

    Throwable realProblem = Utilities.getRealProblem(ex);
    StringWriter sw = new StringWriter();
    realProblem.printStackTrace(new PrintWriter(sw));

    logger.error(
     workerInstance.getClass() + " " +
     realProblem.toString() + " " +
     sw.toString()
    );

    throw (Exception)realProblem;
   }
   finally {
    exceptionsToCatch.set(null);
   }

   logger.debug(() -> "Returning result for " + workerInstance + "." + method.getName());

   return res;

  }



  private Object[] getMethodArgs(Object[] args, Link toMethodInWorker, Method method) {

   AtomicInteger instanceParameterCount = new AtomicInteger();
   AtomicInteger interfaceParameterCount = new AtomicInteger();

   return toMethodInWorker.getConfigValueSuppliers().stream()
    .map(csv ->
     csv.map(
      cs -> cs.getValue(new Configurator.ConfigurableParam(workerInstance.getClass(), method, instanceParameterCount.getAndIncrement()))
     )
     .orElseGet(() ->
      args[interfaceParameterCount.getAndIncrement()]
     )
    ).toArray();

  }



  private synchronized Link link(Method methodInInterface) {

   Link ret = links.get(methodInInterface);

   if (ret == null)
    for (Method instanceMethod : workerInstance.getClass().getMethods()) {
     if (instanceMethod.getName().equals(methodInInterface.getName())) {

      List<Optional<Configurator.ConfigValueSupplier>> configValueSuppliers = configurator.getConfigValueSuppliers(workerInstance.getClass(), instanceMethod);

      int instanceParameterCount = 0;
      int interfaceParameterCount = 0;

      for (Optional<Configurator.ConfigValueSupplier> cs : configValueSuppliers) {
       if (!cs.isPresent()) {
        // No ConfigValueSupplier: Interface parameter must match!
        if (instanceMethod.getParameterTypes()[instanceParameterCount] != methodInInterface.getParameterTypes()[interfaceParameterCount])
         throw new IllegalArgumentException(
          "Parameter doesn't match: " +
           workerInstance.getClass().getName() + "." + instanceMethod.getName() + ": " + instanceParameterCount + " " +
           methodInInterface.getName() + ": " + interfaceParameterCount
         );
        interfaceParameterCount++;
       }
       instanceParameterCount++;
      }

      ret = new Link(instanceMethod, configValueSuppliers);
      links.put(methodInInterface, ret);
     }
    }

   if (ret == null)
    throw new MethodNotFoundException("Method \"" + methodInInterface.getName() + "\" not found!");

   return ret;

  }
 }



 private static class Link {

  private final Method method;
  private final List<Optional<Configurator.ConfigValueSupplier>> configValueSuppliers;

  Link(Method method, List<Optional<Configurator.ConfigValueSupplier>> configValueSuppliers) {

   this.method = method;
   this.configValueSuppliers = configValueSuppliers;

  }



  public Method getMethod() {

   return method;

  }



  public List<Optional<Configurator.ConfigValueSupplier>> getConfigValueSuppliers() {

   return configValueSuppliers;

  }

 }



 /**
  * This interface identifies a worker
  *
  * @version 2024-08-03
  * @author lp
  *
  */

 public interface IWorker extends ObjectManager.IObject {

  /**
   *  Indicates the health status of the worker.
   */

  public enum Health {

   /**
    * Health status is unknown.
    */
   unknown,
   /**
    * Worker is healthy.
    */
   healthy,
   /**
    * Worker is ill.
    */
   ill

  }



  /**
   * Returns the health status of a worker.
   *
   * @return The health status.
   */

  default public Health healthCheck() {

   return Health.unknown;

  }

 }



 private static class WorkerCounter implements Supplier<Number> {

  @Override
  public Number get() {

   return workers.size();

  }
 }



 @Documented
 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface Metric {

  public enum MetricTypes {
   GAUGE
  }


  /**
   * The name for the metric.
   *
   * @return The name.
   */

  String name();



  /**
   * The type for the metric.
   *
   * @return The type.
   */

  MetricTypes type();

 }



 @Documented
 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface TimerJob {



  /**
   * The name for the TimerJob.
   *
   * @return The name.
   */

  String name();



  /**
   * The schedule property.
   *
   * @return The schedule property.
   */

  String schedule();



  /**
   * ClusterJob flag.
   *
   * @return The clusterJob flag
   */

  boolean clusterJob() default true;

 }



 private void handleSpecialMethods(Class<?> workerClass, IWorker worker) {

  Arrays.stream(workerClass.getDeclaredMethods())
   .forEach(m -> {

     // Check for metric.

     Metric manno = m.getAnnotation(Metric.class);
     if (manno != null) {
      switch (manno.type()) {
       case GAUGE:
        lovelyRita.setGauge(manno.name(), new MetricsSupplier<Number>(m, worker));
      }
     }

     // Check for TimerJob.

     TimerJob tjanno = m.getAnnotation(TimerJob.class);
     if (tjanno != null) {
      String sd = tjanno.schedule();

      int id = sd.indexOf('.');

      String schedule = null;

      if (id > -1) {
       ResourceBundle rbl =
        MergedPropertyResourceBundle.getMergedBundle(sd.substring(0, id));

       schedule =
        Utilities.getParam(rbl, sd.substring(id + 1));
      }
      else
       schedule = tjanno.schedule();

      Timer.get().run(tjanno.name(), schedule, tjanno.clusterJob(), new TimerJobRunnable(m, worker));
     }
    }
   );

 }



 /**
  * Glue for Metrics.
  *
  * @param <T>
  */

 public static class MetricsSupplier<T> implements Supplier<T> {

  Method metricsMethod;
  IWorker worker;

  MetricsSupplier(Method metricsMethod, IWorker worker) {

   this.metricsMethod = metricsMethod;
   this.worker = worker;

  }

  @Override
  public T get() {

   try {
    @SuppressWarnings("unchecked")
    T res = (T)metricsMethod.invoke(worker, new Object[]{});
    return
     res;
   }
   catch (Exception e) {
    throw new WorkerManagerException(e.toString());
   }
  }
 }



 /**
  * Glue for TimerJobs.
  */

 public static class TimerJobRunnable implements Runnable {

  Method timerJobMethod;
  IWorker worker;

  TimerJobRunnable(Method timerJobMethod, IWorker worker) {

   this.timerJobMethod = timerJobMethod;
   this.worker = worker;

  }

  @Override
  public void run() {

   try {
    timerJobMethod.invoke(worker, new Object[]{});
   }
   catch (Exception e) {
    throw new WorkerManagerException(e.toString());
   }
  }
 }

}
