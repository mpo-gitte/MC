package core.base;



import core.bakedBeans.DataStoreManager;
import core.servlet.BaseModuleDescription;
import core.util.MergedPropertyResourceBundle;
import org.apache.commons.lang3.time.DurationFormatUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Formatter;
import java.util.Locale;
import java.util.ResourceBundle;



/**
 * A bundle of utility methods.
 *
 * <p>Used in application scope of the servlet.</p>
 *
 * @version 2025-02-08
 * @author lp
 */

@BaseModuleDescription(name = "utilities", order = 2)
public class Utilities extends core.bakedBeans.Utilities implements BaseModule {



 /**
  * Formats a timestamp.
  *
  * @param date The timestamp
  * @param loc The locale
  * @return The date formatted according to the locale
  */

 public static String formatDate(LocalDateTime date, Locale loc) {

  DateTimeFormatter df = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).withLocale(loc);

  return df.format(date);

 }



 /**
  * Formats a timestamp without time.
  *
  * @param date The timestamp
  * @param loc The locale
  * @return The date formatted according to the locale
  */

 public static String formatDateWithoutTime(LocalDateTime date, Locale loc) {

  DateTimeFormatter df = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(loc);

  return df.format(date);

 }



 /**
  * Formats a date.
  *
  * @param date The Date
  * @param loc The locale
  * @return The date formatted according to the locale
  */

 public static String formatDate(LocalDate date, Locale loc) {

  DateTimeFormatter df = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(loc);

  return df.format(date);

 }



 /**
  * Formats a Double.
  *
  * @param d The value.
  * @param loc The locale for the format conversion
  * @param format The format due to Formatter.format().

  * @return A String containing the formatted Double value.

  * @see Formatter#format(Locale, String, Object...)
  */

 public static String formatDouble(Double d, Locale loc, String format) {

  return (new Formatter()).format(loc, format, d).toString();

 }



 /**
  * Formats a boolean.
  *
  * @param b The boolean value.
  *
  * @return "true" or "false". If boolean is null "false".
  */

 public static String formatBoolean(Boolean b) {

  return (b == null) ? "false" : b.toString();

 }



 public static String formatDuration(Long duration) {

  return
   DurationFormatUtils.formatDuration(duration * 1000, "H:mm:ss", true);

 }



 /**
  * Gets the DataStoreManager used by this application.
  *
  * @return The DataStoreManger
  */

 public DataStoreManager getDataStoreManager() {

  return DataStoreManager.get();

 }



 /**
  * Returns the current time.
  *
  * @return The current time as a LocalDateTime.
  */

 public LocalDateTime nowAsLocalDateTime() {

  return LocalDateTime.now();

 }



 /**
  * Gets the requested property value or the given default value.
  *
  * @param rbName Name of resource bundle
  * @param val Name of the property
  * @param defval Default value if property value not found

  * @return The property value
  *
  * @see MergedPropertyResourceBundle
  */

 public static String getParam(String rbName, String val, String defval) {

  ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle(rbName);

  return getParam(rb, val, defval);

 }

}
