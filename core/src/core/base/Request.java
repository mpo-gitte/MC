package core.base;



import core.bakedBeans.BeanFactory;
import core.bakedBeans.CookieBean;
import core.bakedBeans.PersistentBean;
import core.exceptions.IllUrlException;
import core.exceptions.MCException;
import core.exceptions.RequestInvalidException;
import core.servlet.BaseConfig;
import core.servlet.BaseModuleDescription;
import core.servlet.Scope;
import core.util.MergedPropertyResourceBundle;
import core.util.Utilities;
import inet.ipaddr.HostName;
import jakarta.servlet.http.*;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.app.event.EventCartridge;
import org.apache.velocity.app.event.EventHandler;
import org.apache.velocity.app.event.implement.EscapeReference;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 * This class provides methods for querying request information.
 *
 * <p>It's a servlet tool for the request scope.</p>
 *
 * @version 2025-02-08
 * @author lp
 */

@BaseModuleDescription(name = "req", scope = Scope.request, order = 3)
public class Request implements BaseModule {

 private static final Logger logger = LogManager.getLogger(Request.class);

 private VelocityContext velocityContext;
 private VelocityEngine velocityEngine;
 private HttpServletRequest req;
 private HttpServletResponse res;
 private HashMap<Class<? extends PersistentBean>, PersistentBean> sessionBeans = new HashMap<>();

 private static final ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle("Request");
 private static final List<String> localIpAddressPrefixes = Utilities.getParamAsList(rb, "LocalIpAddressPrefixes", null);
 private static final List<String> friendlyBrowsersStrings = Utilities.getParamAsList(rb, "FriendlyBrowsersStrings", null);
 private static final List<String> evalBrowsersStrings = Utilities.getParamAsList(rb, "EvalBrowsersStrings", new String[] {});
 private static final List<String> validUrls = Utilities.getParamAsList(rb, "ValidUrls", new String[] {""});
 private static final String requestValidatorImpl = Utilities.getParam(rb, "RequestValidator", StandardRequestValidator.class.getName());
 private static final List<String> proxyIpAddressPrefixes = Utilities.getParamAsList(rb, "ProxyIpAddressPrefixes", null);
 private static final List<String> allowedContextPaths = Utilities.getParamAsList(rb, "AllowedForwardedContextPaths", new String[] {});
 private static final boolean shouldIdentify = Utilities.getParam(rb, "Identify", false);
 private static IRequestValidator reqval;
 private static List<String> homeUrls;

 static String MC_FORWARDED_CONTEXT_PATH = "MC-FORWARDED-CONTEXT-PATH";
 static final String MC_INSTANCE_RESPONSE_HEADER = "MC-Instance";

 static {
  try {
   reqval = (IRequestValidator) Class.forName(requestValidatorImpl).getConstructor().newInstance();
  } catch (Exception e) {
   logger.error("Request(): Can't load RequestValidator!");
  }

  try {
   homeUrls = new ArrayList<>();

   Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();

   while (interfaces.hasMoreElements()) {
    NetworkInterface ni = interfaces.nextElement();
    Enumeration<InetAddress> ias = ni.getInetAddresses();
    while (ias.hasMoreElements()) {
     InetAddress ia = ias.nextElement();
     if (!ia.isLinkLocalAddress()) {  // No one should call us with a link local address!
      String sd = new HostName(ia.getHostAddress()).getHost();  // Normalize.
      if (!homeUrls.contains(sd))
       homeUrls.add(sd);
      sd = new HostName(ia.getHostName()).getHost();  // Normalize
      if (!homeUrls.contains(sd))
       homeUrls.add(sd);
     }
    }
   }
  } catch (SocketException e) {
   logger.error("Request(): Can't get ip address or hostname of this host!");
  }

 }

 private static final ThreadLocal<Request> mes = new ThreadLocal<Request>() {
  protected synchronized Request initialValue() {
   return null;
  }
 };

 private Boolean isFriendlyBrowser = null;



 /**
  * The only constructor.
  */

 public Request() {
 }



 /**
  * Initialises the Request with a HttpServletRequest.
  *
  * <p>For test purposes.</p>
  *
  * @param req A HttpServletRequest
  * @return this request.
  */

 public Request withHttpServletRequest(HttpServletRequest req) {

  this.req = req;

  return this;

 }



 /**
  * Return the Request instance for the current thread.
  *
  * @return The Request.
  */

 static public Request get() {

  if (logger.isDebugEnabled())
   logger.debug("get(): Thread: " + Thread.currentThread().getId());

  return mes.get();

 }



 /**
  * Used for initialization by the servlet.
  *
  * @param baseConfig The current HttpServletRequest.
  */

 public void init(BaseConfig baseConfig) {

  req = baseConfig.getHttpServletRequest();
  res = baseConfig.getHttpServletResponse();

  if (shouldIdentify) {
   try {
    res.setHeader(MC_INSTANCE_RESPONSE_HEADER, InetAddress.getLocalHost().toString());
   } catch (UnknownHostException e) {
    logger.error("init(): Can't get local host: " + e);
   }
  }

  velocityContext = baseConfig.getVelocityContext();
  velocityEngine = baseConfig.getVelocityEngine();

  if (logger.isDebugEnabled())
   logger.debug("init(): initialized from ViewContext for thread: " +  + Thread.currentThread().getId());

  mes.set(this);  // Store this instance for use by workers and beans in the same thread.

 }



 private List<String> loadList(String paramName, Map<?, ?> params) {

  StringTokenizer st = new StringTokenizer((String)params.get(paramName), ";");

  List<String> list = new ArrayList<>();

  while (st.hasMoreTokens())
   list.add(st.nextToken());

  return list;

 }



 /**
  * Checks the validity of this request.
  *
  * <p>Uses the request validator.</p>
  *
  * @see IRequestValidator
  *
  */

 public void validateRequest() {

  // Check validity of the current request.

  RequestInvalidException.when(
   (Void) -> !reqval.isValid(this),
   "Invalid Request!"
  );

 }



 /**
  * Returns the name of the browser.
  *
  * @return The browsers name.
  */

 public String getBrowserName() {

  return req.getHeader("User-Agent");

 }



 public String getIfNoneMatch() {

  String inm = req.getHeader("If-None-Match");

  if (Utilities.isEmpty(inm))
   return null;

  return Utilities.naked(inm);

 }



 public void setETag(String value) {

  if (!Utilities.isEmpty(value))
   res.setHeader("ETag", Utilities.clothed(value));

 }



 /**
  * Returns whether the request comes from a compatible browser.
  *
  * <p>The definition of a prefixes for compatible browsers is made in toolbox.xml.</p>
  *
  * <p>The entry "friendlyBrowsersPrefixes" contains a colon separated list of browser name prefixes.
  *
  * <p>There is also a kind of negative list  in toolbox.xml.</p>
  *
  * <p>The entry "evalBrowsersPrefixes" contains a colon separated list of browser name prefixes.
  * Requests having a header field "User-Agent" beginning with one of the prefixes
  * in evalBrowserPrefixes are considered coming from a
  * incompatible browser. If the content of the header field is found in friendlyBrowsers the
  * Browser is compatible.</p>
  *
  * @return True if the user uses a compatible browser. False otherwise.
  */

 public boolean isFriendlyBrowser() {

  if (isFriendlyBrowser == null)
   isFriendlyBrowser = getIsFriendlyBrowser();

  return isFriendlyBrowser;

 }



 private boolean getIsFriendlyBrowser() {

  String browsername = getBrowserName();

  if (browsername != null) {

   for (String addr : evalBrowsersStrings)
    if (browsername.matches(addr)) return false;

   for (String sd : friendlyBrowsersStrings) {
    List<String> p = Utilities.split(sd, '=');

    Matcher m = Pattern.compile(p.get(0)).matcher(browsername);

    if (m.find()) {
     int currver = Integer.parseInt(m.group(2));

     List<String> versions = Utilities.split(p.get(1), '-');

     int maxver = Integer.MAX_VALUE;

     if (versions.size() > 1)
      maxver = Integer.parseInt(versions.get(1));

     int minver = Integer.parseInt(versions.get(0));

     return
      currver >= minver  &&  currver <= maxver;
    }
   }
  }

  return false;

 }



 /**
  * Returns the Ip address of the client.
  *
  * @return The ip address.
  */

 public String getIpAddress() {

  String remoteAddr = req.getRemoteAddr();

  if (logger.isDebugEnabled())
   logger.debug(
    "getIpAddress(): checking remote address from header \"" +
    remoteAddr + "\" against proxyAddressPrefixes: " + proxyIpAddressPrefixes);

  if (Utilities.prefixListContains(proxyIpAddressPrefixes, remoteAddr)) {
   // We're behind a known reverse proxy. So we trust X-Forwarded-For!
   String xFwdForAddrs = req.getHeader("X-Forwarded-For");

   if (logger.isDebugEnabled())
    logger.debug("getIpAddress(): Looking in \"X-Forwarded-For\" header: " + xFwdForAddrs);

   if (Utilities.isEmpty(xFwdForAddrs))
    throw new SecurityException("Reverse proxy didn't set \"X-Forwarded-For\" header!");

   String[] ras = xFwdForAddrs.split(",");

   for (int id = ras.length - 1; id >= 0; id --) {
    String adr = ras[id].trim();
    if (!Utilities.prefixListContains(proxyIpAddressPrefixes, adr))
     return adr;
   }

   throw
    new SecurityException(
     "\"X-Forwarded-For\" header with suspicious value: \"" + xFwdForAddrs + "\"!"
    );
  }

  if (logger.isDebugEnabled())
   logger.debug("getIpAddress(): returning \"" + remoteAddr + "\".");

  return remoteAddr;

 }



 /**
  * Returns the port of the client.
  *
  * @return The ip address.
  */

 public int getPort() {

  int remotePort = req.getRemotePort();
  String remoteAddr = req.getRemoteAddr();

  for (String addr : proxyIpAddressPrefixes)
   if (remoteAddr.startsWith(addr)) {
    String fp = req.getHeader("X-Forwarded-Port");
    if (!Utilities.isEmpty(fp))
     remotePort = Integer.parseInt(fp);
    break;
   }

  return remotePort;

 }



 /**
  * Returns whether the request comes from the local network.
  *
  * <p>The definition of a prefix for local address is made in toolbox.xml.</p>
  *
  * <p>The entry "localIpAddressPrefixes" contains a colon separated list of address prefixes. Client addresses
  * beginning with one of theses prefixes are considered local.</p>
  *
  * @return True if request comes from the local network. False otherwise.
  */

 public boolean isLocal() {

  return isLocal(this);

 }



 /**
  * Returns whether or not the given request comes from the local network.
  *
  * <p>The definition of a prefix for local address is made in toolbox.xml.</p>
  *
  * <p>The entry "localIpAddressPrefixes" contains a colon separated list of address prefixes. Client addresses
  * beginning with one of theses prefixes are considered local.</p>
  *
  * @param req The request
  *
  * @return True if request comes from the local network. False otherwise.
  */

 public static boolean isLocal(Request req) {

  String raddr = req.getIpAddress();

  logger.debug(() -> "isLocal(): checking \"" + raddr + "\" against: " + localIpAddressPrefixes);

  for (String addr : localIpAddressPrefixes)
   if (raddr.startsWith(addr)) return true;

  return false;

 }



 /**
  * Returns the client's locale.
  *
  * @return The client's locale.
  */

 public Locale getLocale() {

  String lang = req.getHeader("Accept-Language");


  if (lang == null)
   return Locale.US;
  if (lang.length() < 2)
   return Locale.US;

  String la = lang.trim().substring(0, 2);

  return new Locale(la, la.toUpperCase());

 }



 /**
  * Returns the URL from the request.
  *
  * @return The URL.
  */

 public String getUrl() {

  return req.getRequestURL().toString();

 }



 /**
  * Return the URI.
  *
  * @return The URI.
  */

 public String getUri() {

  return req.getRequestURI();

 }



 /**
  * Returns the URI without any colon separated characters.
  *
  * <p>Mainly used to strip a session id from the URI.</p>
  *
  * @return The plain URI.
  */

 public String getUriPlain() {

  String sd = req.getRequestURI();

  int id = sd.indexOf(";");

  if (id > 0)
   return sd.substring(0, id);

  return sd;

 }



 /**
  * Returns the path of the template in the URI.
  *
  * @return The path. Starts with a slash. Empty string if no template found.
  */

 public String getTemplatePath() {

  String sd = getUriPlain();  // Stripped URI.

  String ctp = req.getContextPath();

  return
   sd.replaceFirst("^.*" + ctp, "");  // Strip from beginning to context path.

 }



 /**
  * Returns the URI parameters.
  *
  * @return The URI paramters. That is the complete part after the semicolon (Evtl. session id).
  * Null if there is nothing.
  */

 public String getUriParameters() {

  String sd = req.getRequestURI();

  int id = sd.indexOf(";") + 1;

  if (id > 0  &&  id < sd.length())
   return sd.substring(id);

  return null;

 }



 /**
  * Returns a single parameter from the URI of the HTTP request.
  *
  * @param name The name of the parameter.
  * @param defaultValue Default value will be returned if parameter isn't present.
  * @return The value of the parameter or the default value.
  */

 public String getUriParameter(String name, String defaultValue) {

  String ps = getUriParameters();
  if (ps == null)
   return defaultValue;

  Map<String, String> pm = Utilities.getMapFromString(ps);

  String p = pm.get(name);

  if (p == null)
   p = defaultValue;

  return p;

 }



 /**
  * Returns the client's language.
  *
  * @return The client's language.
  */

 public String getContentLanguage() {

  Locale loc = getLocale();
  String sd = loc.getVariant();
  return loc.getCountry() + (("".equals(sd)) ? "" : "-" + sd);

 }



 /**
  * Returns the map of parameters from the HTTP request.
  *
  * @return The parameter map.
  */


 @SuppressWarnings("unchecked")
 public Map<String, String[]> getParameterMap() {

  return req.getParameterMap();

 }



 /**
  * Returns a single parameter from the HTTP request.
  *
  * @param name The name of the parameter.
  * @param defaultValue Default value will be returned if parameter isn't present.
  * @return The value of the parameter or the default value.
  */

 public String getParameter(String name, String defaultValue) {

  String p = req.getParameter(name);

  if (p == null)
   p = defaultValue;

  return p;

 }



 /**
  * Returns a single parameter from the HTTP request.
  *
  * @param name The name of the parameter.
  * @return The value of the parameter.
  */

 public String getParameter(String name) {

  return req.getParameter(name);

 }



 /**
  * Returns an ArrayList of parameters from the HTTP request.
  *
  * @param name The name of the parameter.
  * @return The values of the parameters.
  */

 public ArrayList<String> getParameterAsArrayList(String name) {

  ArrayList<String> al = null;

  for (Enumeration<?> ps = req.getParameterNames(); ps.hasMoreElements();) {
   String sd = (String)ps.nextElement();
   if (sd.startsWith(name + '[')) {
    String[] nps = sd.split("\\[|\\]");
    int ix = 0;
    if (nps.length > 0)
     ix = Integer.parseInt(nps[1]);

    if (al == null) al = new ArrayList<>();

    if (ix > al.size() - 1) {
     for (int id = al.size(); id <= ix; id ++)  // fill the list.
      al.add(null);
    }
    al.set(ix, getParameter(sd));

   }
  }

  return al;

 }



 /**
  * Returns a CookieBean from the Request.
  *
  * @param className The name of the result class. Has to be a derived from CookieBean.
  * @param cookieName The name of the Cookie.
  * @return The bean.
  *
  * @see CookieBean
  */

 public CookieBean getCookieAsBean(String className, String cookieName) {

  return BeanFactory.getCookieAsBean(req, cookieName, className);

 }



 /**
  * Returns the Cookie with the given name.
  *
  * @param name Name of the Cookie.
  * @return The value of the Cookie or null if the cookie is not existing.
  *
  */

 public String getCookie(String name) {

  Cookie[] cookies = req.getCookies();

  if (cookies != null)
   for (Cookie co : cookies)
    if (name.equals(co.getName()))
     return co.getValue();

  return null;

 }



 /**
  * Returns the id of the current session.
  *
  * @return The session id.
  */

 public String getSessionId() {

  return
   stripJvmRouteFromSessionId(getSession().getId());

 }



 /**
  * Returns true if this session is new.
  *
  * @return true if session is new. False otherwise.
  */

 public boolean getIsNew() {

  return
   getSession().isNew();

 }



 public static String stripJvmRouteFromSessionId(String sessionId) {

  return
   sessionId.split("\\.")[0];

 }


 /**
  * Returns the current session.
  *
  * @return The session.
  */

 public HttpSession getSession() {

  return req.getSession();

 }




 /**
  * Returns the referrer of the current session.
  *
  * @return The referer for this session.
  */

 public String getReferer() {

  return req.getHeader("Referer");

 }


 /**
  * Set the given object as the session binding listener.
  *
  * @param obj An object which implements HttpSessionBindingListener.
  */

 public void setSessionBinding(HttpSessionBindingListener obj) {

  req.getSession().setAttribute("BindingListener" + getSessionId(), obj);

 }



 /**
  * Builds a string containing the URL for this request
  *
  * <p>Adds session id if necessary.</p>
  *
  * @param path If this parameter isn't null it will be used as the servlet path.
  * @return The URL.
  */

 public String getServerUrl(String path) {

  if (getCookie("testCookie") == null)
   return res.encodeURL(getServerUrlBare(path));
  else
   return getServerUrlBare(path);

 }



 public String getServerUrlBare(String path) {

  return makeUrl(path, new StringBuilder()).toString();

 }



 private StringBuilder makeUrl(String path, StringBuilder sb) {

  // Check request for an URL

  if (sb.length() == 0)
   fromRequest(sb);

  if (path != null)
   sb.append(path);
  else
   sb.append(req.getServletPath());

  String sd = req.getPathInfo();
  if (sd != null)
   sb.append(sd);

  return sb;

 }



 private StringBuilder fromRequest(StringBuilder sb) {

  sb.append(getProtocol());
  sb.append("://");

  String sd = getHostFromHeader();
  if (Utilities.isEmpty(sd)) {
   // Only if host header isn't present. Should not occur for HTTP 1.1 .
   sb.append(req.getServerName());
   int p = req.getServerPort();
   if (p != 80) {
    sb.append(':');
    sb.append(p);
   }
  }
  else
   sb.append(sd);

  return sb;

 }



 /**
  * Gets the protocol.
  *
  * @return Either the value of "X-Forwarded-Proto" or the scheme in the request.
  */

 String getProtocol() {

  String sd = req.getHeader("X-Forwarded-Proto");

  if (Utilities.isEmpty(sd))
   sd = req.getScheme();

  return sd;

 }


 String getHostFromHeader() {

  String sd = req.getHeader("X-Forwarded-Host");

  if (Utilities.isEmpty(sd)) {
   sd = req.getHeader("Host");
   logger.info(
    "getHostFromHeader(): Header \"X-Forwarded-Host\" not set! " +
    "Using Header \"Host\" with value " +
     sd + "\" instead!"
   );
  }
  else {
   String[] hl = sd.split (",");
   final String sd1 = hl[0];  // Using the first name given.
   logger.debug(() ->
    "getHostFromHeader(): Using Header \"X-Forwarded-Host\" with first value \"" +
     sd1 + "\"."
   );
  }

  // ToConsider: Omitting port! --- Security issue?
  String host = new HostName(sd).getHost();

  if (validUrls.contains (host))
   return sd;

  if (homeUrls.contains (host))
   return sd;

  throw new IllUrlException("Called with illegal URL: \"" + sd + "\"");

 }


 /**
  * Returns the HTTP method used for calling this application.
  *
  * @return Something like "GET' or 'POST'
  */

 public String getMethod() {

  return req.getMethod();

 }


 /**
  * Sets the return code in the response.
  *
  * @param returnCode HTTPStatusCode.
  */

 public void setReturnCode(int returnCode) {

  res.setStatus(returnCode);

 }


 /**
  * Sets the error in the response.
  *
  * @param returnCode HTTPStatusCode
  * @param errorMessage Message text.
  */

 public void sendError(int returnCode, String errorMessage) {

  try {
   res.sendError(returnCode, errorMessage);
  }
  catch (IOException e) {
   throw new MCException(e);
  }

 }


 /**
  * Sets the content type of the response.
  *
  * @param contentType content type.
  */

 public void setContentType(String contentType) {

  res.setContentType(contentType);

 }



 /**
  * Returns the authorization header.
  *
  * @return The authorization header.
  */

 public String getAuthorization() {

  return req.getHeader("authorization");

 }



 /**
  * Returns the authorization header.
  *
  * @return The authorization header with the given name.
  *
  * @param which The name of the authorization header
  */

 public String getAuthorization(String which) {

  String sd = req.getHeader("authorization");

  if (!Utilities.isEmpty(sd)  &&  sd.startsWith(which + " ")) {
   String[] p = sd.split(" ");
   return p[1];
  }

  return null;
 }



 /**
  * Returns the application path.
  *
  * <p>The application path is the context path from the request without the leading slash.</p>
  *
  * <p>If custom http header HTTP_X_FORWARDED_CONTEXT_PATH is set it is used and checked
  * if it contained in the list in property "AllowedContextPaths" it is used. Otherwise
  * a SecurityException is thrown.</p>
  *
  * <p>Used in Request.vm to render all links to MC resources.</p>
  */

 public String getAppPath() {

  String hctp = req.getHeader(MC_FORWARDED_CONTEXT_PATH);

  logger.debug(() -> "getAppPath(): Looking for context path in \"" + MC_FORWARDED_CONTEXT_PATH + "\" = \"" + hctp + "\"");

  if (!Utilities.isEmpty(hctp)) {
   logger.debug(() -> "getAppPath(): Checking context path \"" + hctp + "\" against " + allowedContextPaths);

   if (allowedContextPaths.contains(hctp)) {
    int len = hctp.length();

    if (len > 0)
     return hctp.substring(1, len);  // AppPath is without leading slash!
   }
   else
    throw new SecurityException("\"" + hctp + "\" not in allowed context paths!");
  }
  else {
   String sd = req.getContextPath();

   int len = sd.length();

   if (len > 0)
    return sd.substring(1, len);  // AppPath is without leading slash!
  }

  throw new SecurityException("Can't get context path!");

 }



 /**
  * Creates a new session.
  *
  * <p>Copies all attributes from the old to the new session.</p>
  */

 public void makeNewSession() {

  HttpSession sess = req.getSession(false);

  //  Store attributes.

  HashMap<String, Object> attributes = new HashMap<>();
  @SuppressWarnings("unchecked")
  Enumeration<String> enames = sess.getAttributeNames();

  while (enames.hasMoreElements()) {
   String name = enames.nextElement();
   attributes.put(name, sess.getAttribute(name));
  }

  //  Invalidate old session.

  String osid = sess.getId();
  sess.invalidate();

  // Create new session

  sess = req.getSession(true);

  // Restore attributes to the new session.

  for (Map.Entry<String, Object> et : attributes.entrySet())
   sess.setAttribute(et.getKey(), et.getValue());

  // Better throw away all session beans.

  sessionBeans = new HashMap<>();

  logger.info("makeNewSession(): session " + osid + " renewed to " + sess.getId());

 }



 /**
  * Returns a session related bean.
  *
  * <p>If the bean does not exist it will be created and cached for use in the requests threat.</p>
  *
  * <p>The bean must have a key value which can receive the session id.</p>
  *
  * @param beanClass The class of the bean which will be instantiated.

  * @return The instance
  *
  */

 public PersistentBean getSessionBeanViaSessionId(Class<? extends PersistentBean> beanClass) {

  if (sessionBeans.containsKey(beanClass))
   return sessionBeans.get(beanClass);

  if (logger.isDebugEnabled())
   logger.debug("getSessionBeanViaSessionId(): creating bean: \"" + beanClass.getName() + "\"");

  PersistentBean b;

  try {
   b = BeanFactory.make(beanClass);
   b.setFromStringByName(b.getKeyName(), getSessionId());  // key name is name of a property!
  }
  catch (Exception e) {
   throw new SecurityException(
    "Can't get session bean \"" + beanClass.getName() +
    "\" for sessionid: " + getSessionId() +
     (getIsNew() ? " (new session) " : " ") +
    ". Reason: " + e.toString()
   );
  }

  sessionBeans.put(beanClass, b);

  return b;

 }



 /**
  * Returns the underlying ServletRequest.
  *
  * @return The ServletRequest.
  */

 public HttpServletRequest getHttpServletRequest() {

  return req;

 }


 public void setHeader(String name, String value) {

  res.setHeader(name, value);

 }



 /**
  * Returns the ViewContext.
  *
  * @return The current ViewContext.
  */

 public VelocityContext getVelocityContext() {

  return velocityContext;

 }



 /**
  * Returns the ViewEngine.
  *
  * @return The current ViewEngine.
  */

 public VelocityEngine getVelocityEngine() {

  return velocityEngine;

 }



 /**
  * This class defines an object which validates each request.
  *
  * <p>Write down the class name in Request.properties as RequestValidator=myClass</p>
  *
  * <p>The class will be instantiated by the Request tool and the method will be called for
  * each request.</p>
  */

 public interface IRequestValidator {

  /**
   * Called to check the request.
   *
   * @param req The request.
   * @return true if request is valid. False otherwise.
   */

  boolean isValid(Request req);

 }



 static class StandardRequestValidator implements IRequestValidator {



  public StandardRequestValidator() {}



  @Override
  public boolean isValid(Request req) {

   if (logger.isDebugEnabled())
    logger.debug("isValid(): All request are always valid!");

   return true;  // All requests are valid!
  }

 }



 private static final String evcname = "__EVENTCARTRIGDE";
 private static final String evhname = "__EVENTHANDLER";
 private static final String evswitch = "__EVENTACTIVE";



 /**
  * Switches HTML output on.
  */

 public void setHtmlOutputOn() {

  if (!"true".equals((String)velocityContext.get(evswitch))) {

   EventCartridge ec = (EventCartridge) velocityContext.get(evcname);
   if (ec == null) {
    logger.debug("setHtmlOutputOn(): creating new EventCartridge.");

    ec = new EventCartridge();
    velocityContext.put(evcname, ec);
   }

   EventHandler ev = (EventHandler) velocityContext.get(evhname);
   if (ev == null) {
    if (logger.isDebugEnabled())
     logger.debug("setFilter(): creating new EventHandler.");

    ev = new EscapeHtmlReference();

    velocityContext.put(evhname, ev);
   }

   ec.addEventHandler(ev);
   velocityContext.put(evswitch, "true");

   if (logger.isDebugEnabled())
    logger.debug("setHtmlOutputOn(): activating EventHandler \"" + ev.getClass().getName() + "\"");

   ec.attachToContext(velocityContext);
  }

 }



 // Because with Velocity 2.3 this is deprecated we use an own implementation.

 private static class EscapeHtmlReference extends EscapeReference {

  @Override
  protected String escape(Object text) {
   return StringEscapeUtils.escapeHtml3(text.toString());
  }

  @Override
  protected String getMatchAttribute() {
   return "eventhandler.escape.html.match";
  }

 }



 /**
  * Switches HTML output off.
  *
  */

 public void setHtmlOutputOff() {

  EventCartridge ec =	(EventCartridge)velocityContext.get(evcname);
  EventHandler ev =	(EventHandler)velocityContext.get(evhname);

  logger.debug(() -> "setHtmlOutputOff(): deactivating EventHandler \"" + ev.getClass().getName() + "\"");

  ec.removeEventHandler(ev);
  velocityContext.put(evswitch, "false");

 }



 /**
  * Flush the reponse writer.
  */

 public void flushResponseWriter() {

  try {
   res.getWriter().flush();
  } catch (IOException e) {
   logger.error("flushResponseWriter(): Flush didn't work!");
  }

 }

}
