package core.base;

import core.exceptions.MCException;
import core.servlet.BaseConfig;
import core.servlet.BaseModuleDescription;
import org.apache.catalina.ha.session.ClusterSessionListener;
import org.apache.catalina.ha.tcp.SimpleTcpCluster;
import org.apache.catalina.tribes.*;
import org.apache.catalina.tribes.group.GroupChannel;
import org.apache.catalina.tribes.membership.McastService;
import org.apache.catalina.tribes.membership.MembershipServiceBase;
import org.apache.catalina.tribes.transport.nio.NioReceiver;
import org.apache.catalina.tribes.membership.cloud.CloudMembershipService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;


/**
 * Messenger.
 *
 * @version 2024-01-17
 * @author lp
 *
 */

@BaseModuleDescription(order = 1)
public class Messenger implements BaseModule {

 private final Logger logger = LogManager.getLogger(Messenger.class);

 private static Messenger me = null;

 private GroupChannel channel;

 private static Map<String, List<Consumer<String>>> receivers = null;

 private static MeterMaid lovelyRita;



 public Messenger() {

  me = this;

 }



 /**
  * Initialize the Messenger.
  *
  * @param baseConfig Configuration.
  *
  * Takes from the configuration:
  *
  * clusterMode
  * multicastAddress
  * multicastPort
  *
  */

 public void init(BaseConfig baseConfig) {

  if (Boolean.parseBoolean(baseConfig.getInitParameter("messenger"))) {

   logger.info("Initializing Messenger:");

   lovelyRita = MeterMaid.get();

   String clusterMode = baseConfig.getInitParameter("messenger.clusterBy", "multicast");
   String multicastAddress = baseConfig.getInitParameter("messenger.multicastAddress", "228.0.0.4");
   String multicastPort = baseConfig.getInitParameter("messenger.multicastPort", "45564");


  // Create a channel.

  channel = new GroupChannel();

  channel.setMembershipService(getMembershipService(clusterMode, multicastAddress, multicastPort));

  NioReceiver nioReceiver = new NioReceiver();
  nioReceiver.setAddress("auto");
  nioReceiver.setMaxThreads(6);
  nioReceiver.setPort(4000);
  channel.setChannelReceiver(nioReceiver);

  SimpleTcpCluster cluster = new SimpleTcpCluster();
  cluster.addClusterListener(new ClusterSessionListener());
  cluster.setChannel(channel);


  // Create listeners.

  ChannelListener msgListener = new MessageListener(logger);
  MembershipListener mbrListener = new MemberListener(logger, channel);


  // Attach the listeners to the channel.

  channel.addMembershipListener(mbrListener);
  channel.addChannelListener(msgListener);


  // Start the channel.

  try {
   channel.start(Channel.DEFAULT);
  } catch (ChannelException e) {
   logger.error("Can't create Messenger! " + e);
  }


  // Prepare receivers map

  receivers = new HashMap<>();


  // Metric for number of cluster members.

  lovelyRita.setGauge("Messenger_numberOfClusterMembers", new Supplier<Number>() {
   @Override
   public Integer get() {
    return channel.getMembers().length;
   }
  });

  }
  else
   logger.info("Messaging is off!");

 }



 private MembershipServiceBase getMembershipService(String clusterMode, String multicastAddress, String multicastPort) {

  if ("multicast".equals(clusterMode)) {
   logger.info("Getting buddies using multicast address: \"" + multicastAddress + "\" and port: \"" + multicastPort + "\"");

   McastService mcastService = new McastService();

   mcastService.setAddress(multicastAddress);  // Address and port define membership
   mcastService.setPort(Integer.parseInt(multicastPort));  // in a cluster.
   mcastService.setFrequency(500);
   mcastService.setDropTime(3000);

   return mcastService;
  }
  else if ("kubernetes".equals(clusterMode)) {
   logger.info("Getting buddies using Kubernetes API.");

   return new CloudMembershipService();
  }
  else
   throw new MCException("Ill. membership mode!");

 }



 /**
  * The Messenger is a singleton. This method gets the one and only.
  *
  * @return The instance of the Messenger.
  *
  */

 public static Messenger get() {

  MCException.when((Void) -> me == null, "Messenger not initialized!");

  return me;

 }



 /**
  * Send a message to the cluster.
  *
  * @param messageType The type of the message. Used for selection of the receiver.
  * @param payload The payload of the message.
  */

 public void send(String messageType, String payload) {

  String mess = messageType + " " + payload;

  logger.debug(() -> "send(): " + mess);

  // Send the message

  try {
   if (channel.hasMembers()) {
    channel
     .send(
       channel.getMembers(),
       mess,
       Channel.SEND_OPTIONS_DEFAULT,
       new ErrorHandler() {

        @Override
        public void handleError(ChannelException x, UniqueId id) {

         logger.error("send(): Can't send message: \"" + mess + "\"");
         throw new MCException("Can't send message: \"" + mess + "\"");

        }

        @Override
        public void handleCompletion(UniqueId id) {

         logger.info("send(): successfully sent: \"" + mess + "\"");

        }

       }
      );

    lovelyRita.incrementCounter("Messenger_sentMessages", "messageType", messageType);
   }
  } catch (ChannelException e) {
   logger.error("send(): Can't send message! " + e);
  }

 }



 private static class MessageListener implements ChannelListener {

  private final Logger logger;



  public MessageListener(Logger logger) {

   this.logger = logger;

  }



  @Override
  public void messageReceived(Serializable msg, Member sender) {

   logger.debug(() -> "messageReceived(): " + msg.toString() + " " + sender.toString());

   // Dispatch message to receivers depending on message type.

   String[] sd = msg.toString().split(" ");

   MCException.when(
    (Void) -> sd.length <= 1,
    "messageReceived(): Illegal message format!"
   );

   String messageType = sd[0];

   List<Consumer<String>> rs = receivers.get(messageType);

   if (rs == null)
    logger.error("messageReceived(): No receiver for messageType \"" + messageType + "\"!");
   else {
    lovelyRita.incrementCounter("Messenger_receivedMessages", "messageType", messageType);
    rs.forEach(r -> r.accept(sd[1]));
   }
  }



  @Override
  public boolean accept(Serializable msg, Member sender) {

   logger.debug(() -> "accept(): " + msg.toString() + " " + sender.toString());

//   return (!msg.toString().startsWith("SESSION-DELTA"));

   return true;

  }

 }



 private static class MemberListener implements MembershipListener {

  private final Logger logger;
  private final Channel channel;



  public MemberListener(Logger logger, Channel channel) {

   this.logger = logger;
   this.channel = channel;

  }



  @Override
  public void memberAdded(Member member) {

   logger.debug(() -> "memberAdded(): " + channel.getMembers().length + " members now.");

  }



  @Override
  public void memberDisappeared(Member member) {

   logger.debug(() -> "memberAdded(): " + channel.getMembers() .length+ " members now.");

  }

 }



 /**
  * The general message for communication between MC instances.
  */

 public static class Message implements Serializable {

  public String messageType;
  public String payloadClass;
  public String payload;



  public Message(String messageType, String payloadClass, String payload) {

   this.messageType = messageType;
   this.payloadClass = payloadClass;
   this.payload = payload;

  }



  public String toString() {

   return messageType + ";" +
    ((payloadClass == null) ? "" : payloadClass) + ";" +
    ((payload == null) ? "" : payload);

  }

 }



 /**
  * Adds a receiver for a message type.
  *
  * @param messageType The message type.
  * @param receiver The receiver.
  */

 public void addReceiver(String messageType, Consumer<String> receiver) {

  List<Consumer<String>> rs = receivers.get(messageType);

  if (rs == null) {
   rs = new ArrayList<>();
   receivers.put(messageType, rs);
  }

  rs.add(receiver);

 }

}
