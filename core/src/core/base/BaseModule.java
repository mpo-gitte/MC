package core.base;

import core.servlet.BaseConfig;

import java.util.Map;


/**
 * Interface for BaseModules.
 *
 * @version 2022-11-25
 * @author lp
 */

public interface BaseModule {



 /**
  * This method will be called by the servlet during initialization.
  *
  * @param baseConfig The configuration,
  */

 default void init(BaseConfig baseConfig) {}



 /**
  * This method will be called by the servlet during initialization.
  *
  * @param baseConfig The configuration,
  *
  * @return A map of objects which will be put in the application scope.
  */

 default Map<String, Object> initApplicationScope(BaseConfig baseConfig) {

  return null;

 }

}
