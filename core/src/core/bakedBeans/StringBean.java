package core.bakedBeans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



/**
 * This is a bean which can be dumped into a String and also loaded from a String.
 *
 * @version 2016-03-10
 * @author pilgim.lutz@imail.de
 *
 */

public class StringBean extends TypedBean {

 private static Logger logger = LogManager.getLogger(StringBean.class);


 /**
  * Dumps all properties of this bean to a String in a format which can be parsed by loadFromString().
  *
  * @return The String with the textual representation of the beans properties. Will be null for empty beans.
  *
  * @see StringBean#fromString(String)
  */

 public String toString() {

  try {
   return BeanFactory.dumpBeanToString(this);
  }
  catch (Exception e) {
   logger.error("Can't dump bean to String! Bean: \"" + this.getClass().getName() + "\" Exception: " + e);
   return null;
  }

 }

}
