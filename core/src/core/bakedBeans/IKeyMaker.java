package core.bakedBeans;



/**
 * Interface for Objects which generates keys for beans.
 *
 * @version very very old.
 * @author pilgrim.lutz@imail.de
 *
 */

public interface IKeyMaker {



 /**
  * Called when key generating begins.
  *
  * @param forBean The bean for which the key is generated.
  * @throws BakedBeansKeyMakerException If problems occur.
  */

 public void startMakeKey(PersistentBean forBean) throws BakedBeansKeyMakerException;



 /**
  * Called when key generating ends.
  *
  * @param forBean The bean for which the key is generated.
  * @throws BakedBeansKeyMakerException If problems occur.
  */

 public void endMakeKey(PersistentBean forBean) throws BakedBeansKeyMakerException;

}
