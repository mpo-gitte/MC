package core.bakedBeans;



import core.base.Messenger;
import core.base.MeterMaid;
import core.exceptions.MCException;
import core.util.Configurator;
import core.util.MergedPropertyResourceBundle;
import core.util.Timer;
import core.util.Utilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;



/**
 * A manager for DataStores.
 *
 * @version 2024-10-27
 * @author lp
 *
 */

public class DataStoreManager {

 private static Logger logger;
 private static DataStoreManager me = null;
 private final HashMap<String, DataStoreCategory> categories;
 private final ResourceBundle rb;
 private static final MeterMaid lovelyRita = MeterMaid.get();
 private static long sempahoreTimeout = 5000L;



 private DataStoreManager() {

  logger = LogManager.getLogger(DataStoreManager.class);

  categories = new HashMap<>();

  me = this;

  rb = MergedPropertyResourceBundle.getMergedBundle("DataStoreManager");

  sempahoreTimeout =
   Utilities.getParam(rb, "Semaphore.waitFor.Timeout", 5000);

  prepareSignalDistribution();

  if (lovelyRita != null)
   lovelyRita.setGauge("number_current_datastores", new DataStoreCounter());  // Monitor the number of DataStores.

  Timer.get().run("DatastoreManager.DataStoreWatcher", Utilities.getParam(rb, "Watcher.schedule", "0 0/1 * * * ?"), false, () -> {

   synchronized(categories) {
    for (DataStoreCategory cat : categories.values()) {
     ArrayList<DataStore> dst = new ArrayList<>();

     Map<String, DataStore<? extends Bean>> stores = cat.getStores();

     for (DataStore ds : stores.values())
      if (ds.getLifeCounter() == 0) {
       logger.debug(() -> "DataStoreWatcher: Removing DataStore: " + ds.ident());
       dst.add(ds);
      } else if (!ds.isBusy())
       ds.decrementLifeCounter();

     for (DataStore ds : dst)
      stores.remove(ds.ident());
    }
   }

  });

}



 /**
  * Gets the one and only DataStoreManager.
  *
  * @return The instance of the DataStoreManager
  *
  */

 public static DataStoreManager get() {

  synchronized(DataStoreManager.class) {
   if (me == null)
    me = new DataStoreManager();
  }

  return me;

 }



 /**
  * Creates a DataStore or returns a DataStore from cache.
  * <p>Each newly created DataStore will be put in a cache and will be shared by the threads of
  * the application. Therefore DataStores should be thread-safe!</p>
  *
  * @param className class name for the DataStore
  * @param category The category of the DataStore
  * @param queryString The query string
  * @param resultClass The type of BakedBean for the result.
  *
  * @return A DataStore
  */

 @SuppressWarnings("unchecked")
 public <T extends Bean> DataStore<T> makeDataStore(
  String className, String category, String queryString, Class<T> resultClass) {

  return
   dataStore(className, category)
    .query(queryString)
    .make(resultClass);

 }



 /**
  * Creates a DataStore or returns a DataStore from cache.
  * <p>Each newly created DataStore will be put in a cache and will be shared by the threads of
  * the application. Therefore DataStores should be thread-safe!</p>
  *
  * @param className class name for the DataStore
  * @param category The category of the DataStore
  * @param queryString The query string
  * @param values A list with values (eventually needed for the query string)
  * @param resultClass The type of BakedBean for the result.
  *
  * @return A DataStore
  */

 @SuppressWarnings("unchecked")
 public <T extends Bean> DataStore<T> makeDataStore(
  String className, String category, String queryString, List<Object> values, Class<T> resultClass) {

  return
   dataStore(className, category)
    .query(queryString)
    .values(values)
    .make(resultClass);

 }



 /**
  * Creates a DataStore or returns a DataStore from cache.
  * <p>Each newly created DataStore will be put in a cache and will be shared by the threads of
  * the application. Therefore DataStores should be thread-safe!</p>
  *
  * @param className class name for the DataStore
  * @param category The category of the DataStore
  * @param queryString The query string
  * @param values An array with values (eventually needed for the query string)
  * @param resultClass The type of BakedBean for the result.
  *
  * @return A DataStore
  */

 @SuppressWarnings("unchecked")
 public <T extends Bean> DataStore<T> makeDataStore(
  String className, String category, String queryString, Object[] values, Class<T> resultClass) {

  return
   dataStore(className, category)
    .query(queryString)
    .values(values == null ? null : Arrays.asList(values))
    .make(resultClass);

 }



 private DataStoreCategory getDataStoreCategory(String category) {

  DataStoreCategory cat;

  if (categories.containsKey(category)) {
   cat = categories.get(category);
  }
  else {
   cat = new DataStoreCategory(category);
   categories.put(category, cat);
  }

  return cat;

 }



 /**
  * Creates a DataStore or returns a DataStore from cache.
  *
  * <p>The name specifies a DataStore definition from DataStoreManager.properties. All parameters for
  * a DataStore a specified there.</p>
  *
  * @param name The name of the DataStore in DataStoreManager.properties.
  * @param values An array with values (eventually need for the query string)
  * @return The DataStore.
  *
  * @see DataStoreManager#makeDataStore(String, String, String, Object[], Class)
  */

 public <T extends Bean> DataStore<T> makeDataStore(String name, Object[] values) {

  String sd = rb.getString("DataStore." + name);

  logger.debug(() -> "make DataStore with name: " + name);

  Map<String, String> def = Utilities.getMapFromString(sd);

  @SuppressWarnings("unchecked")
  Class<T> resultClass =
   (Class<T>)BeanFactory.loadBeanClass(def.get("resultClass"));

  return
   dataStore(def.get("className"), def.get("category"))
    .query(def.get("queryString"))
    .values(values)
    .make(resultClass);

 }



 /**
  * Returns a DataStoreMaker for making a DataStore.
  *
  * @param className Class name for the DataStore.
  * @param category Cotegory for the DataStore.
  * @return A DataStoreMaker.
  */

 public DataStoreMaker dataStore(String className, String category) {

  return
   new DataStoreMaker(className, category);

 }



 /**
  * Returns a DataStoreMaker for making a DataStore.
  *
  * @param dataStoreClass Class name for the DataStore.
  * @param category Cotegory for the DataStore.
  * @return A DataStoreMaker.
  */

 public synchronized DataStoreMaker dataStore(Class<? extends DataStore> dataStoreClass, String category) {

  return
   new DataStoreMaker(dataStoreClass, category);

 }



 /**
  * Deletes a DataStore.
  *
  * @param dsdel The DataStore to delete.
  */

 public void delete(ArrayList<String> dsdel) {

  for (String sd : dsdel)
   if (sd != null)
    for (DataStoreCategory cat : categories.values())
     if (cat.getStores().containsKey(sd)) {
      logger.debug(() -> "deleting DataStore: " + sd);
      cat.getStores().remove(sd);
     }

 }



 /**
  * Returns a map containing all DataStores currently created.
  * @return The map
  */

 public Map<String, DataStore<? extends Bean>>getDataStores() {

  Map<String, DataStore<? extends Bean>> res = new HashMap<>();

  for (DataStoreCategory cat : categories.values())
   res.putAll(cat.getStores());

  return res;

 }



 /**
  * Returns all categories from DataStoreManager.properties.
  * @return An ArrayList filled with the categories.
  */

 public ArrayList<String> getCategories() {

  ArrayList<String> res = new ArrayList<>();

  Enumeration<String> ps = rb.getKeys();

  while (ps.hasMoreElements()) {
   String k = ps.nextElement();
   if (k.startsWith("Category")) {
    String[] sd = k.split("\\.");
    res.add(sd[1]);
   }
  }

  return res;

 }



 /**
  * Signals all DataStores of the given category.
  *
  * @param sig The signal for the DataStore.
  * @param subjectBean The PersistentBean which causes the signal to be send.
  * @param categorynames The signal will be send to all DataStores with this categories.
  *
  * @see DataStore#signal(DataStore.DataStoreSignal, PersistentBean)
  */

 public DataStoreManager signal(DataStore.DataStoreSignal sig, PersistentBean subjectBean, String ... categorynames) {

  for (String categoryname : categorynames) {
   logger.debug(() -> "signal(): \"" + sig + "\" \"" + categoryname + "\"");

   synchronized (categories) {
    if (categories.containsKey(categoryname)) {
     DataStoreCategory cat = categories.get(categoryname);

     for (DataStore store : cat.getStores().values())
      store.signal(sig, subjectBean);

     externalSignal(sig, subjectBean, cat);

     distributeSignal(sig, categoryname, subjectBean);
    }
   }
  }

  return this;

 }



 private void signal(DataStore.DataStoreSignal sig, PersistentBean subjectBean, String categoryname) {

  synchronized(categories) {
   logger.debug(() -> "signal(): \"" + sig + "\" \"" + categoryname + "\"");

   if (categories.containsKey(categoryname)) {
    DataStoreCategory cat = categories.get(categoryname);

    for (DataStore store : cat.getStores().values())
     store.signal(sig, subjectBean);

    externalSignal(sig, subjectBean, cat);
   }
  }

 }



 private void distributeSignal(DataStore.DataStoreSignal sig, String categoryname, PersistentBean subjectBean) {

  if (Utilities.getParam(rb, "distribute", "false").equals("true")) {
   logger.debug(() -> "distributeSignal(): " + sig.name());

   String name = null;
   String beanData = null;

   if (subjectBean != null) {
    name = subjectBean.getClass().getName();
    beanData =
     BeanFactory.dumpPropertiesToString(subjectBean.getPropertiesForSerialization());
   }

   DsmMessage mess = new DsmMessage(sig, categoryname, name, beanData);

   Messenger.get().send("DataStoreSignal", mess.toString());

  }

 }



 private static class DsmMessage {

  private final DataStore.DataStoreSignal signal;
  private final String categoryname;
  private final String beanClass;
  private final String beanData;



  public DsmMessage(DataStore.DataStoreSignal signal, String categoryname, String beanClass, String beanData) {

   this.signal = signal;
   this.categoryname = categoryname;
   this.beanClass = beanClass;
   this.beanData = beanData;

  }



  public String toString() {

   return
    signal + ":" +
    categoryname + ":" +
    (beanClass == null ? "" : beanClass) + ":" +
    (beanData == null ? "" : beanData);

  }



  public DataStore.DataStoreSignal getSignal() {

   return signal;

  }



  public String getCategoryname() {

   return categoryname;

  }



  public String getBeanClass() {

   return beanClass;

  }



  public String getBeanData() {

   return beanData;

  }



  public static DsmMessage fromString(String mess) {

   String[] sd = mess.split(":", 4);

   return
    new DsmMessage(DataStore.DataStoreSignal.valueOf(sd[0]), sd[1], sd[2], sd[3]);

  }

 }



 private void prepareSignalDistribution() {

  if (Utilities.getParam(rb, "distribute", "false").equals("true")) {
   logger.info("prepareSignalDistribution()");

   Messenger.get().addReceiver("DataStoreSignal", m -> receiver(DsmMessage.fromString(m)));

  }

 }



 private void receiver(DsmMessage mess) {

  logger.debug(() -> "receiver(): " + mess.toString());

  PersistentBean bean = null;
  String beanClass = mess.getBeanClass();

  if (!Utilities.isEmpty(beanClass)) {
   // Got a bean in the message.
   bean = (PersistentBean)BeanFactory.createBeanFromString(mess.beanData, beanClass);
  }

  // Signal DataStores.

  signal(mess.getSignal(), bean, mess.getCategoryname());

 }



 /**
  * Signals all DataStores of the given category.
  *
  * @param sig The signal for the DataStore.
  * @param categorynames The signal will be sent to all DataStores with these categories.
  *
  * @see DataStore#signal(DataStore.DataStoreSignal, PersistentBean)
  */

 public DataStoreManager signal(DataStore.DataStoreSignal sig, String ... categorynames) {

  return
   signal(sig, null, categorynames);

 }



 private void externalSignal(DataStore.DataStoreSignal sig, PersistentBean subjectBean, DataStoreCategory cat) {

  Semaphore sem = cat.getSemaphore();  // Get the semaphore...
  sem.setSignal(sig);  // ... and put some information about the signal in it.
  sem.setBean(subjectBean);

  logger.debug(() -> "externalSignal(): for semaphore with category: \"" + cat.name + "\"");

  synchronized (sem) {
   sem.notifyAll();  // Wake up all waiting requests.
  }
 }



 /**
  * Getting a semaphore for a signal.
  *
  * @param category DataStore category.
  *
  * @return The semaphore.
  */

 public synchronized DataStoreManager.Semaphore getSemaphore(String category) {

  DataStoreCategory cat = getDataStoreCategory(category);

  logger.debug(() -> "getSemaphore(): for category: \"" + category + "\"");

  return cat.getSemaphore();

 }



 private static class DataStoreCategory {

  private final String name;
  private final HashMap<String, DataStore<? extends Bean>> stores = new HashMap<>();
  private Semaphore sem = null;



  DataStoreCategory(String name) {

   this.name = name;

  }



  void addStore(String id, DataStore<? extends Bean> store) {

   stores.put(id, store);

  }



  HashMap<String, DataStore<? extends Bean>> getStores() {

   return stores;

  }



  boolean hasDataStore(String id) {

   return stores.containsKey(id);

  }



  DataStore<? extends Bean> getDataStore(String id) {

   return stores.get(id);

  }


  Semaphore getSemaphore() {

   if (sem == null)
    sem = new Semaphore();

   return sem;
  }

 }



 public static class Semaphore {

  private DataStore.DataStoreSignal signal;
  private PersistentBean bean;



  Semaphore() {}



  void setSignal(DataStore.DataStoreSignal sig) {

   this.signal = sig;

  }



  void setBean(PersistentBean bean) {

   this.bean = bean;

  }



  public DataStore.DataStoreSignal getSignal() {

   return signal;

  }



  public PersistentBean getBean() {

   return bean;

  }



  public void waitFor() throws InterruptedException {

   synchronized (this) {
    try {
     this.wait(sempahoreTimeout);
    }
    catch (InterruptedException e) {
     logger.error("waitFor(): Timeout!");
     throw e;
    }
   }

  }

 }



 private class DataStoreCounter implements Supplier<Number> {

  @Override
  public Number get() {

   return
     categories.keySet().stream()
     .map(key -> categories.get(key).getStores().size())
     .reduce(0, Integer::sum);

  }
 }



 /**
  * The DataStoreMaker to make a DataStore.
  */

 // Limit access to the DataStoreMaker.

 public interface IDataStoreMaker {

  Object[] getTaggedWith();

 }



 public class DataStoreMaker implements IDataStoreMaker {

  private Class<? extends DataStore> dataStoreClass = null;
  private String category;
  private String queryString;
  private Class<? extends Bean> resultClass;
  private List<Object> values;
  private String[] loadWith;
  private BiFunction<IDataStoreMaker, Bean, Boolean> resetCriteria;
  private Operator where;
  private String[] orderBy;
  private BiFunction<Integer, Integer, BeanList<? extends Bean>> sourceList;
  private BeanComparator[] sortBy;
  private String[] sortByPropertyAlias;
  private BeanComparator[] sortByAlias;
  private Supplier<Stream<? extends Bean>> sourceStream;
  private boolean oneTransaction;
  private Consumer<BeanList<? extends Bean>> onReady = null;
  private Object[] taggedWith;
  private String[] groupBy;
  private Operator having;
  private Function<DataStore, Integer> makeHashCode;
  private boolean distinct;



  /**
   * Standard constructor.
   *
   * @param className Class for the DataStore.
   * @param category Category for the DataStore.
   */

  public DataStoreMaker(String className, String category) {

   Class<?> aClass = null;

   try {
    aClass = Class.forName(className);
   }
   catch (ClassNotFoundException e) {
    throw new MCException(e);
   }

   @SuppressWarnings("unchecked")
   Class<? extends DataStore> clazz = (Class<? extends DataStore>) aClass;

   init(clazz, category);

  }



  /**
   * Standard constructor.
   *
   * @param dataStoreClass Class for the DataStore.
   * @param category Category for the DataStore.
   */

  public DataStoreMaker(Class<? extends DataStore> dataStoreClass, String category) {

   init(dataStoreClass, category);

  }



  private void init(Class<? extends DataStore> dataStoreClass, String category) {

   this.dataStoreClass = dataStoreClass;
   this.category = category;

   this.queryString = null;
   this.values = null;
   this.loadWith = null;
   this.orderBy = null;
   this.sourceList = null;
   this.sourceStream = null;
   this.oneTransaction = false;
   this.sortBy = null;
   this.onReady = null;
   this.taggedWith = null;
   this.makeHashCode =  (ds -> {
    return ds.read().hashCode();
   });
   this.distinct = false;

  }



  /**
   * Sets the queryString for the DataStore.
   *
   * @param queryString The query string.
   *
   * @return This DataStoreMaker.
   */

  public DataStoreMaker query(String queryString) {

   this.queryString = queryString;

   return this;

  }



  /**
   * Set the values for the DataStore.
   *
   * @param values The values.
   *
   * @return This DataStoreMaker.
   */

  public DataStoreMaker values(List<Object> values) {

   this.values = values;

   return this;

  }



  /**
   * Set the values for the DataStore.
   *
   * @param values The values.
   *
   * @return This DataStoreMaker.
   */

  public DataStoreMaker values(Object[] values) {

   if (values != null)
    this.values = Arrays.asList(values);

   return this;

  }



  /**
   * Sets the list of properties to load.
   *
   * @param loadWith An array of property aliases.
   *
   * @return This DataStoreMaker
   */

  public DataStoreMaker loadWith(String ... loadWith) {

   this.loadWith = loadWith;

   return this;

  }



  /**
   * Select distinct
   *
   * @return This DataStoreMaker.
   */

  public DataStoreMaker distinct() {

   distinct = true;

   return this;

  }



  /**
   * The DataStore will be resetable.
   *
   * @return This DataStoreMaker.
   */

  public DataStoreMaker resetable() {

   resetable((ds, b) -> {return Boolean.TRUE;});

   return this;

  }



  public DataStoreMaker resetable(BiFunction<IDataStoreMaker, Bean, Boolean> resetCriteria) {

   this.resetCriteria = resetCriteria;

   return this;

  }



  /**
   * All work within foreach will be done in one transaction.
   *
   * <p>Works only if transactions are supported by the underlying persistence system.</p>
   *
   * @return This DataStoreMaker.
   */

  public DataStoreMaker oneTransaction() {

   oneTransaction = true;

   return this;

  }



  public DataStoreMaker where(Operator operator) {

   this.where = operator;

   return this;

  }



  public DataStoreMaker orderBy(String... aliases) {

   this.orderBy = aliases;

   return this;

  }



  public DataStoreMaker having(Operator operator) {

   this.having = operator;

   return this;

  }



  public DataStoreMaker groupBy(String... aliases) {

   this.groupBy = aliases;

   return this;

  }




  public DataStoreMaker sourceList(BiFunction<Integer, Integer, BeanList<? extends Bean>> sourceList) {

   this.sourceList = sourceList;

   return this;

  }




  public DataStoreMaker sourceStream(Supplier<Stream<? extends Bean>> sourceStream) {

   this.sourceStream = sourceStream;

   return this;

  }



  public DataStoreMaker sortBy(BeanComparator ... sortBy) {

   this.sortBy = sortBy;

   return this;

  }




  public DataStoreMaker sortBy(String ... sortByPropertyAlias) {

   this.sortByPropertyAlias = sortByPropertyAlias;

   return this;

  }



  public DataStoreMaker taggedWith(Object ... taggedWith) {

   this.taggedWith = taggedWith;

   return this;

  }



  public DataStoreMaker onReady(Consumer<BeanList<? extends Bean>> onReady) {

   this.onReady = onReady;

   return this;

  }



  public DataStoreMaker makeHashCode(Function<DataStore, Integer> makeHashCode) {

   this.makeHashCode = makeHashCode;

   return this;

  }



  /**
   * Make the DataStore.
   *
   * @param resultClass The class for the beans in the DataStore.
   *
   * @return A new or an used DataStore.
   */

  public <T extends Bean> DataStore<T> make(Class<T> resultClass) {

   this.resultClass = resultClass;

   return
    create();

  }



  @SuppressWarnings("unchecked")
  private <T extends Bean> DataStore<T> create() {

   String lc = null;

   try {
    lc = rb.getString("Category." + category + ".LifeCounter");
   }
   catch (Exception e) {
    logger.warn("No LifeCounter for category '" + category + "' in 'DataStoreManager.properties'! Setting to 1.");
   }

   String identString = ident();
   DataStore<T> ds = null;

   synchronized(categories) {
    DataStoreCategory cat;

    cat = getDataStoreCategory(category);

    if (cat.hasDataStore(identString)) {
     // DataStore is already existing.
     logger.debug(() -> "getting DataStore from cache: " + identString);
     ds = (DataStore<T>) cat.getDataStore(identString);  // ToDo: Make it nice even without "unchecked" above.
    }
    else {
     // DataStore doesn't exist: Create it.
     logger.debug(() -> "creating a new DataStore: " + identString);

     @SuppressWarnings("unchecked")
     Class<? extends DataStore<T>> cl =
      (Class<? extends DataStore<T>>)dataStoreClass;

     Configurator.get().configure(cl);

     try {
      ds = cl.getConstructor().newInstance();
     }
     catch (Exception e) {
      throw new MCException(e);
     }

     Configurator.get().configure(ds);

     ds.init(identString, this);

     cat.addStore(ds.ident(), ds);
    }
   }

   return ds.use(lc);

  }



  private String ident() {

   StringBuilder sb = new StringBuilder();

   // Build the ident string from all information.

   sb.append(category)
    .append(";")
    .append(dataStoreClass.getName())
    .append(";")
    .append(queryString)
    .append(";")
    .append(resultClass.getName());

   if (values != null)
    for (Object v : values)
     if (v != null)
      sb.append(v.toString());
   sb.append(";");

   if (loadWith != null)
    for (Object p : loadWith)
     if (p != null)
      sb.append(p);
   sb.append(";");

   if (where != null)
    sb.append(where);
   sb.append(";");

   if (sourceList != null)
    sb.append(sourceList.getClass());
   sb.append(";");

   if (sourceStream != null)
    sb.append(sourceStream.getClass());
   sb.append(";");

   if (sortBy != null)
    sb.append(Arrays.toString(sortBy));
   sb.append(";");

   if (sortByPropertyAlias != null)
    sb.append(Arrays.toString(sortByPropertyAlias));
   sb.append(";");

   if (orderBy != null)
    sb.append(Arrays.toString(orderBy));
   sb.append(";");

   if (oneTransaction)
    sb.append(oneTransaction);
   sb.append(";");

   if (onReady != null)
    sb.append(onReady);
   sb.append(";");

   if (taggedWith != null)
    for (Object p : taggedWith)
     if (p != null)
      sb.append(p);
   sb.append(";");

   if (resetCriteria != null)
    sb.append(resetCriteria.getClass());
   sb.append(";");

   if (groupBy != null)
    sb.append(Arrays.toString(groupBy));
   sb.append(";");

   if (having != null)
    sb.append(having);
   sb.append(";");

   if (makeHashCode != null)
    sb.append(makeHashCode.getClass());
   sb.append(";");

   if (distinct)
    sb.append(distinct);
   sb.append(";");

   return sb.toString();

  }



  public String getCategory() {

   return category;

  }



  public String getQueryString() {

   return queryString;

  }



  public Class<? extends Bean> getResultClass() {

   return resultClass;

  }



  public Object[] getValues() {

   return
    values == null ? null : values.toArray();

  }



  public void setValue(Object val)  {

   if (values == null)
    values = new ArrayList<>();

   values.add(val);

  }



  public void clearValues() {

   values = null;

  }



  public String[] getLoadWith() {

   return loadWith;

  }



  public BiFunction<IDataStoreMaker, Bean, Boolean> getResetCriteria() {

   return resetCriteria;

  }



  public Operator getWhere() {

   return where;

  }



  public String[] getOrderBy() {

   return orderBy;

  }



  public String[] getGroupBy() {

   return groupBy;

  }



  public Operator getHaving() {

   return having;

  }



  public BiFunction<Integer, Integer, BeanList<? extends Bean>> getSourceList() {

   return sourceList;

  }



  public Supplier<Stream<? extends Bean>> getSourceStream() {

   return sourceStream;

  }



  public boolean getOneTransaction() {

   return oneTransaction;

  }



  public boolean getDistinct() {

   return distinct;

  }



  public BeanComparator[] getSortBy() {

   if (sortBy != null)
    return sortBy;

   if (sortByAlias != null)
    return sortByAlias;

   if (sortByPropertyAlias != null) {
    sortByAlias =
     Arrays.stream(sortByPropertyAlias)
      .map(s ->
       {
        BeanComparator.Direction dir = BeanComparator.Direction.ascending;

        if (s.startsWith("+")) {
         s = s.substring(1);
        } else if (s.startsWith("-")) {
         s = s.substring(1);
         dir = BeanComparator.Direction.descending;
        }
        return new BeanComparator<TypedBean>(s, dir);
       }
      )
      .collect(Collectors.toList())
      .toArray(new BeanComparator[sortByPropertyAlias.length]);

    return sortByAlias;
   }

   return null;

  }



  public Consumer<BeanList<? extends Bean>> getOnReady() {

   return onReady;

  }



  public Object[] getTaggedWith() {

   return taggedWith;

  }



  public Function<DataStore, Integer> getMakeHashCode() {

   return makeHashCode;

  }

 }

}
