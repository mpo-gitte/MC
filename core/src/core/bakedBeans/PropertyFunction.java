/**
 * Describes the function for a property.

 * @version 2023-09-22
 * @author lp
 *
 */

package core.bakedBeans;

/**
 * A function for a property.
 *
 * @version 2024-08-23
 * @author lp
 */

public enum PropertyFunction {

 /**
  *  Property has no function.
  */
 none {
  @Override
  public boolean isAggregate() {
   return false;
  }
 },
 /**
  * Add max.
  */
 min,
 /**
  * Add min.
  */
 max,
 /**
  * Add count.
  */
 count,
 /**
  * Add avg.
  */
 avg,
 /**
  * Add upper.
  */
 upper {
  @Override
  public boolean isAggregate() {
   return false;
  }
 },
 /**
  * Add lower.
  */
 lower {
  @Override
  public boolean isAggregate() {
   return false;
  }
 },
 /**
  * Add year.
  */
 year {
  @Override
  public boolean isAggregate() {
   return false;
  }
 },
 /**
  * Add sum.
  */
 sum,
 /**
  * Add string concat.
  */
 concat;



 public boolean isAggregate() {

  return true;

 }

}
