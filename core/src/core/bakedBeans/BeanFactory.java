package core.bakedBeans;



import core.base.MeterMaid;
import core.exceptions.BeanException;
import core.exceptions.MCException;
import core.util.Configurator;
import core.util.Utilities;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Supplier;


/**
 * Factory class for creating and manipulating beans.
 *
 * @version 2024-12-27
 * @author lp
 */

public class BeanFactory {

 private static final Logger logger = LogManager.getLogger(BeanFactory.class);

 private static final HashSet<Class<? extends Bean>> beanClasses = new HashSet<>();

 private static final MeterMaid lovelyRita = MeterMaid.get();
 static {
  lovelyRita.setGauge("number_current_beanclasses", new BeanclassCounter());  // Monitor the number of loaded bean classes.
 }



 /**
  * Creates a bean.
  *
  * @param className The name of the bean class. Any subclass of Bean can be generated.
  *
  * @return The brand-new Bean.
  */

 public static <T extends Bean> T make(String className) {

  @SuppressWarnings("unchecked")
  Class<T> cl = (Class<T>)loadBeanClass(className);

  return make(cl);

 }



 /**
  * Loads the class of a Bean.
  *
  * @param className The class name for the Bean.
  *
  * @return the bean class
  */

 public static Class<? extends Bean> loadBeanClass(String className) {

  Class<?> aClass;
  try {
   aClass = Class.forName(className);
  }
  catch (ClassNotFoundException e) {
   throw new BeanException(e);
  }

  @SuppressWarnings("unchecked")
  Class<? extends Bean> cl = (Class<? extends Bean>)aClass;

  configureBeanClass(cl);

  return cl;

 }



 private static void configureBeanClass(Class<? extends Bean> beanClass) {

  if (!beanClasses.contains(beanClass)) {

   Configurator.get().configure(beanClass);

   beanClasses.add(beanClass);
  }

 }



 /**
  * Makes a bean instance of the given class.
  *
  * @param beanClass the bean class.
  * @param <T> The type.
  *
  * @return A new bean of the given class.
  */

 public static <T extends Bean> T make(Class<T> beanClass) {

  logger.debug(() -> "making Bean: \"" + beanClass.getName() + "\"");

  configureBeanClass(beanClass);

  T bean;
  try {
   bean = beanClass.getConstructor().newInstance();
  }
  catch (Exception e) {
   throw new MCException(
    "make(): Can't get a new instance for bean class: \"" + beanClass + "\"",
    e);
  }

  Configurator.get().configure(bean);

  return bean;

 }



 /**
  * Creates a PersistentBean and loads it.
  *
  * @param className The name for the Bean. Any subclass of PersistentBean can be generated.
  * @param keyvalue The value for the key for loading.
  *
  * @return The brand new PersistentBean.
  */

 public static PersistentBean make(String className, Object keyvalue) {

  PersistentBean pb = make(className);  // This bean has to be a persistent bean. If not...

  pb.load(keyvalue);

  return pb;

 }



 protected static Prop getProp(String pname) {

  int id = pname.indexOf("[");
  if (id == -1)
   id = pname.indexOf("(");

  Prop p = new Prop();

  if (id > -1) {
   p.name = pname.substring(0, id).trim();
   p.index = Integer.parseInt(pname.substring(id + 1, pname.length() - 1));
  }
  else {
   p.name = pname.trim();
   p.index = 0;
  }

  return p;

 }



 protected static class Prop {
  public String name;
  public int index;
 }



 protected static Bean fetchPropertyBean(Bean b, Prop p) {

  Method m =
   getGetMethod(
     b.getClass(),
     "get" + p.name.substring(0, 1).toUpperCase() + p.name.substring(1)
   );

  Object o;

  if (m != null)
   try {
    o = m.invoke(b, (Object[])null);
   }
   catch (Exception e) {
    throw new BeanException(e);
   }
  else
   o = b.get(p.name);

  if (o.getClass() == BeanList.class)
   return (Bean)((BeanList)o).getProperty(p.index);

  if (o.getClass() == PropertyList.class)
   return (Bean)((PropertyList)o).getProperty(p.index);

  return (Bean)o;

 }



 private static Method getGetMethod(Class<?> forClass, String name) {

  Class<?> cl = forClass;

  while (cl != null) {
   for (Method m : cl.getDeclaredMethods())
    if (name.equals(m.getName())  &&  m.getParameterTypes().length == 0)
     return m;
   cl = cl.getSuperclass();
  }

  return null;

 }



 /**
  * Extracts a persistent sub bean from the given bean.
  * <p>The resulting bean will have the status empty or if at least
  * one property could setted the status clean.</p>
  *
  * @param srcBean The source bean.
  * @param pclass The class for the sub bean. Must be a derivate of PersistentBean.
  * @param prefix Prefix to the names of the properties of the srcBean which should make up the sub bean.
  * @return The sub bean.
  *
  * @see PersistentBean
  * @see PersistentBean.Status
  */

 public static Bean makePersistentSubBean(Bean srcBean, Class<? extends Bean> pclass, String prefix) {

  PersistentBean b = (PersistentBean)make(pclass);
  String bp = prefix + "_";
  int id = bp.length();
  Iterator<?> it = srcBean.getProperties().keySet().iterator();

  while (it.hasNext()) {
   String sd = (String)it.next();
   if (sd.startsWith(bp)) {
    b.setRawByName(sd.substring(id), srcBean.get(sd));
    b.setStatus(PersistentBean.Status.clean);
   }
  }

  return b;

 }



 /**
  * Dumps a TypedBean to String in a special format which can be read by loadBeanFromString().
  *
  * @param srcBean The bean to dump.
  *
  * @return The string representation of the bean. If the bean is empty this method returns null.
  *
  * @see BeanFactory#loadBeanFromString(String, TypedBean)
  * @see StringBean
  *
  */

 public static String dumpBeanToString(TypedBean srcBean) {

  StringBuilder sb = new StringBuilder();
  Iterator<?> it = srcBean.getCurrentProperties().keySet().iterator();  // Getting the currently contained values.

  while (it.hasNext()) {
   String sd = (String)it.next();
   if (sb.length() > 0) sb.append(';');
   Class<? extends Object> cl;
   try {
    cl = srcBean.getPropertyClassByName(sd);
   }
   catch (Exception e) {
    cl = Object.class;
   }
   sb.append(sd).append("=").append(core.util.Utilities.objectToString(cl, srcBean.getByName(sd)));
  }

  return sb.length() == 0 ? null : sb.toString();

 }



 /**
  * Dumps the map to a String.
  *
  * @param properties The map with properties.
  *
  * @return The result String.
  */

 public static String dumpPropertiesToString(Map<String, Object> properties) {

  StringBuilder sb = new StringBuilder();

  for (String sd : properties.keySet()) {
   if (sb.length() > 0)
    sb.append(';');
   sb.append(sd).append("=")
    .append(Utilities.objectToString(properties.get(sd).getClass(), properties.get(sd)));
  }

  return sb.length() == 0 ? null : sb.toString();

 }



 /**
  * Loads a bean from a String generated by dumpBeanToString().
  *
  * @param srcString The string with the bean's data.
  * @param destBean The (empty) Bean for the data.

  * @see BeanFactory#dumpBeanToString(TypedBean)
  * @see StringBean
  */

 public static void loadBeanFromString(String srcString, TypedBean destBean) {

  logger.debug(() -> "loadBeanFromString() : " + destBean + " " + srcString);

  ArrayList<String> props = core.util.Utilities.split(srcString);

  for (String p : props) {
   ArrayList<String> sp = core.util.Utilities.split(p, '=');
   if (sp.size() == 2) {
    Class<?> cl = destBean.getPropertyClassByName(sp.get(0));
    if (cl == Integer.class || cl == Long.class || cl == Float.class || cl == Double.class || cl == Boolean.class )
     destBean.setFromStringByName(sp.get(0), sp.get(1));
    else
     destBean.setFromStringByName(sp.get(0), Utilities.naked(sp.get(1)));
   }
  }
 }



 /**
  * Loads a bean from a string.
  *
  * <p>Properties will be accessed by alias!</p>
  *
  * @param srcString The String with the bean properties.
  * @param beanClass The class of the bean.
  */

 public static TypedBean createBeanFromString(String srcString, String beanClass) {

  // Create Bean.

  TypedBean bean =
   make(beanClass);

  // Got properties: fill bean.

  if (!Utilities.isEmpty(srcString)) {
   for (String p : Utilities.split(srcString)) {
    ArrayList<String> sp = Utilities.split(p, '=');
    if (sp.size() == 2) {
     Class<?> cl = bean.getPropertyClass(sp.get(0));
     if (cl == Integer.class || cl == Long.class || cl == Float.class || cl == Double.class || cl == Boolean.class)
      bean.setFromString(sp.get(0), sp.get(1));
     else
      bean.setFromString(sp.get(0), Utilities.naked(sp.get(1)));
    }
   }
  }

  return bean;

 }



 /**
  * Builds a BeanList from a DataStore.
  *
  * @param dataStoreName The name of the DataStore.
  * @param contentClass The class of the Beans in the BeanList.
  * @param query The query string for the DataStore.
  * @param keyValue The only parameter for the DataStore.
  * @param category The category for the DataStore.
  *
  * @return A new BeanList filled with the result of the DataStore.
  *
  * @see DataStore
  */

 public static BeanList<? extends Bean> makeBeanList(String dataStoreName, Class<? extends Bean> contentClass, String query, Object keyValue, String category) {

  List<? extends Bean> al = DataStoreManager.get().makeDataStore(
   dataStoreName, category,
   query,
   new Object[] {keyValue},
   contentClass
  ).read();

  BeanList<? extends Bean> pl = new BeanList<>(contentClass);
  pl.fill(al);

  return pl;

 }



 /**
  * Builds a BeanList from a DataStore.
  *
  * @param dataStoreName The name of the DataStore.
  * @param contentClass The class of the Beans in the BeanList.
  * @param operator Query for the DataStore.
  * @param parent The bean for the value.
  * @param category The category for the DataStore.
  *
  * @return A new BeanList filled with the result of the DataStore.
  *
  * @see DataStore
  */

 public static BeanList<? extends Bean> makeBeanList(String dataStoreName, Class<? extends Bean> contentClass, Operator operator, Bean parent, String category) {

  return
   DataStoreManager.get().dataStore(dataStoreName, category)
    .resetable(BeanFactory::concerned)
    .taggedWith(parent)
    .where(operator)
    .make(contentClass)
    .read();

 }



 private static Boolean concerned(DataStoreManager.IDataStoreMaker dsm, Bean b) {

  if (b ==  null)
   return Boolean.TRUE;  // signal() supplied no bean. Better to "feel" concerned.

  try {
   return
    dsm.getTaggedWith()[0].equals(b);  // Yes, it's our "parent"!
  }
  catch (Exception e) {
   return Boolean.TRUE;  // In doubt: We're better concerned.
  }

 }



 /**
  * Creates a bean from a Cookie.
  *
  * @param req The request.
  * @param cookieName The name of the Cookie.
  * @param className The name of the result class. Has to be a derivate of CookieBean.
  *
  * @return The bean.
  */

 public static CookieBean getCookieAsBean(ServletRequest req, String cookieName, String className) {

  Cookie[] cs = ((HttpServletRequest)req).getCookies();

  if (cs != null)
   for (Cookie c : cs) {
    if (c.getName().equals(cookieName)) {
     CookieBean b = make(className);
     b.fromString(c.getValue());
     return b;
    }
   }

  return null;

 }



 /**
  * Returns a list of TypedBeans from the HTTP request.
  *
  * <p>A bean is identified by its name an index and a property:</p>
  *
  * <code>bean[0].property</code>
  *
  * <p>If the property is the key of the bean this bean will be loaded. All other properties will be set.</p>
  *
  * <p>If no index is given an index of zero is presumed.</p>
  *
  * @param req The request.
  * @param bclass The class of the Bean.
  * @param name The name of the bean within the request parameters.
  *
  * @return The value of the parameter.
  */

 public static BeanList<? extends Bean> getParameterAsBeanArrayList(ServletRequest req, Class<? extends Bean> bclass, String name) {

  BeanList<? extends Bean> al = null;

  for (Enumeration<?> ps = req.getParameterNames(); ps.hasMoreElements();) {
   String sd = (String)ps.nextElement();
   String[] path = sd.split("\\.");

   if (path.length > 0) {
    Prop p = getProp(path[0]);

    if (p.name.equals(name)) {
     if (al == null) al = new BeanList<>(bclass);
     Bean b = al.getProperty(p.index);

     for (int id = 1; id < path.length - 1; id ++)
      b = fetchPropertyBean(b, getProp(path[id]));

     p = getProp(path[path.length - 1]);

     String pstr = req.getParameter(sd);

     String value;
     try {
      value = Utilities.decodeURIComponent(pstr);
     }
     catch (Exception e) {
      throw new BeanException(e);
     }

     ((TypedBean)b).setFromString(p.name, value);
    }
   }
  }

  return al;

 }



 private static class BeanclassCounter implements Supplier<Number> {

  @Override
  public Number get() {

    return beanClasses.size();

  }
 }

}
