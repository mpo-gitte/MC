package core.bakedBeans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import core.util.DBConnection;

import java.sql.ResultSet;



/**
 * This key maker uses DB sequences for generating keys.
 *
 * @version 2018-10-14
 * @author pilgrim.lutz@imail.de
 *
 */

public class JDBCSequenceKeyMaker implements IKeyMaker {

 private static Logger logger;



 public JDBCSequenceKeyMaker() {

  super();

  logger = LogManager.getLogger(JDBCSequenceKeyMaker.class.getName());

 }



 /**
  * Starts making the key and asks the DB sequence.
  *
  * <p>The sequence must have a name made up from the source name of the bean a "_" and the key name of the bean.</p>
  *
  * @param forBean The bean for which the key is generated.
  * @throws BakedBeansKeyMakerException If something wents wrong.
  *
  */

 public void startMakeKey(PersistentBean forBean) throws BakedBeansKeyMakerException {

  DBConnection con;
  try {
   con = ((JDBCBean)forBean).getConnection();
  } catch (Exception e1) {
   throw new BakedBeansKeyMakerException("can't get connection!");
  }

  ResultSet rs;
  try {
   String sd =
    "select nextval('" + forBean.getSource() + "_" + forBean.getKeyName() + "')";

   logger.debug(sd);

   rs = con.prepareStatement(sd).executeQuery();
  } catch (Exception e1) {
   throw new BakedBeansKeyMakerException("can't get execute sequence query!");
  }

  Integer newKeyValue = null;

  try {
   rs.next();
   newKeyValue = rs.getInt(1);
  }
  catch (Exception e) {
   throw new BakedBeansKeyMakerException("can't get value from result set!");
  }

  try {
   forBean.set(forBean.getKeyName(), newKeyValue);
  }
  catch (Exception e) {
   throw new BakedBeansKeyMakerException("can't set key value");
  }

  try {
   ((JDBCBean)forBean).releaseConnection();
  } catch (Exception e) {
   throw new BakedBeansKeyMakerException("can't release connection.");
  }
  
 }



 /**
  * End of key making is empty for this key maker.
  *
  * @param forBean The bean for which the key is generated.
  * @throws BakedBeansKeyMakerException If something went wrong.
  *
  */

 public void endMakeKey(PersistentBean forBean) throws BakedBeansKeyMakerException {

 }

}
