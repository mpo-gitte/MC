package core.bakedBeans;

import java.lang.annotation.*;



/**
 * Annotation for JDBC bean description.
 *
 * @version 2018-10-14
 * @author pilgrim.lutz@imail.de
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface JDBCBeanDescription {



 String loadQueryString();

}
