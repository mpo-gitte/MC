package core.bakedBeans;



import core.exceptions.MCException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Predicate;


/**
 * DataStore which loads from a BeanList.
 *
 * @version 2023-09-13
 * @author pilgrim.lutz@imail.de
 */

public class ListDataStore<T extends Bean> extends DataStore<T> {

 private final static Logger logger = LogManager.getLogger(ListDataStore.class);
 BeanList<T> beanList;
 AtomicInteger position = new AtomicInteger(0);



 /**
  * Standard constructor
  */

 public ListDataStore() {

  super();

 }



 /**
  * Iterate over the beans.
  *
  * <p>This method does not cache!</p>
  *
  * @param consumer A consumer called for each bean.
  * @param enough   A predicate which stops scanning if it returns true
  * @param offset
  * @param count
  */

 @Override
 public synchronized void forEach(Consumer<T> consumer, Predicate<T> enough, int offset, int count) {

  logger.debug(() -> "foreach(): executing " + ident());

  forEach(consumer, enough, (Integer o, Integer c) -> {
   try {
    int index = position.getAndIncrement();

    if (index < beanList.size())
     return
      beanList.get(index);

    return null;  // No more data.
   }
   catch (Exception ex) {
    throw new MCException(ex);
   }

  }, offset, count);

 }



 /**
  * Prepares access to the source list.
  */

 @Override
 protected void open(int offset, int count) {

  @SuppressWarnings("unchecked")
  BeanList<T> beanList =
   (BeanList<T>) maker.getSourceList().apply(offset, count);

  this.beanList = beanList;

  position.getAndSet(0);

 }



 /**
  * Resets the DataStore.
  *
  * <p>Deletes the internal result. This forces a new read on next access.</p>
  */

 @Override
 public synchronized void reset() {

  // Call super class for having care of the stamp.
  super.reset();

  logger.debug(() -> "reset(): " + ident());

 }

}
