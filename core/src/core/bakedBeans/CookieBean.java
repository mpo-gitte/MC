package core.bakedBeans;



import core.exceptions.BeanException;
import core.util.Utilities;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;



/**
 * A CookieBean is StringBean which can be stored in a HTTP-Cookie.
 *
 * @version 2023-08-20
 * @author pilgrim.lutz@imail.de
 *
 */

public class CookieBean extends StringBean {


 
 /**
  * Loads the Bean from a String.
  *
  * <p>Because the beans comes from a Cookie this method expects that the
  * string is surrounded by double quotes.</p>
  *
  * @param str The string with the bean content surrounded with double quotes.
  *
  */

 @Override
 public void fromString(String str) {

  String decode = null;
  try {
   decode = URLDecoder.decode(str, "UTF-8");
  }
  catch (UnsupportedEncodingException e) {
   throw new BeanException(e);
  }

  super.fromString(Utilities.naked(decode));

 }

 
 
 /** 
  * Dumps the bean to a String.
  *
  * <p>The beans content will be quoted an surrounded by double quotes.</p>
  *  
  * @return The formatted String ready to be stored in a Cookie.
  * 
  */

 @Override
 public String toString() {

  try {
   return URLEncoder.encode(Utilities.clothed(super.toString()), "UTF-8");
  }
  catch (Exception e) {
   return e.getMessage();  // Hope this works as an error message.
  }

 }

}
