/**
 * Describes the type of relation for PersistentBeans.

 * @version 2014-05-23
 * @author lp
 *
 */

package core.bakedBeans;

public enum RelationType {

 /**
  *  Property is no relation.
  */
 none,
 /**
  * Related bean has to be saved BEFORE father bean is saved.
  */
 beforeSave,
 /**
  * Related bean has to be saved AFTER father bean is saved.
  */
 afterSave,
 /**
  * Related bean should not be saved.
  * <p>Sometimes used for back references.</p>
  */
 dontSave,
 /**
  * Related bean is never loaded when father bean is loaded.
  */
 loose,
}
