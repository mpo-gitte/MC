package core.bakedBeans;



import core.bakedBeans.PersistentBean.Status;
import core.exceptions.MCException;
import core.util.DB;
import core.util.DBConnection;
import core.util.DBConnectionStore;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;



/**
 * DataStore which loads from a JDBC layer.
 *
 * @version 2024-09-18
 * @author lp
 */

public class JDBCDataStore<T extends JDBCBean> extends DataStore<T> {

 private static Logger logger;
 private ResultSet rs;
 ResultSetMetaData rsm;
 DBConnection con;
 String queryString = null;
 String countString = null;
 Long count = null;
 Object[] values = null;
 Object[] countValues = null;



 /**
  * Standard constructor
  */

 public JDBCDataStore() {

  super();

 }



 /**
  * Iterate over the beans.
  *
  * <p>This method does not cache!</p>
  *
  * @param consumer A consumer called for each bean.
  * @param enough   A predicate which stops scanning if it returns true
  * @param offset
  * @param count
  */

 @Override
 public synchronized void forEach(Consumer<T> consumer, Predicate<T> enough, int offset, int count) {

  logger.debug(() -> "foreach(): executing " + ident());

  forEach(consumer, enough, (Integer o, Integer c) -> {
   @SuppressWarnings("unchecked")
   Class<T> resultClass = (Class<T>)maker.getResultClass();
   try {
    if (rs.next()) {
     T b = BeanFactory.make(resultClass);
     JDBCSupport.fillBeanFromResultset(b, rs, rsm);

     b.setStatus(Status.clean);

     return b;
    }
    return null;  // No more data.
   }
   catch (Exception ex) {
    throw new MCException(ex);
   }

  }, offset, count);

 }



 /**
  * Closes the JDBC connection.
  */

 @Override
 protected void close() {

  try {
   if (rs != null)
    rs.close();
  }
  catch (SQLException e) {
   throw new MCException(e);
  }

  rs = null;  // Help the garbage collector.

  releaseConnection();

  super.close();  // Call base class.

 }



 /**
  * Prepares the JDBC connection and does the query.
  */

 @Override
 protected void open(int offset, int count) {

  super.open(offset, count);  // Call base class.

  getConnection();

  if (queryString == null) {
   queryString = makeQueryString();
   values = maker.getValues();
  }

  String limitClause = "";
  Object[] usevalues = values;


  // Slicing

  if (count < Integer.MAX_VALUE) {
   limitClause = " offset ? limit ?";
   int newsize = 0;
   if (values != null)
    newsize = values.length;

   Object[] vs = new Object[newsize + 2];
   for (int i = 0; i < newsize; i++)
    vs[i] = values[i];
   int id = newsize;
   vs[id] = offset;
   vs[++ id] = count;
   usevalues = vs;
  }

  rs = JDBCSupport.queryResultSet(con, queryString + limitClause, usevalues);
  try {
   rsm = rs.getMetaData();
  }
  catch (SQLException e) {
   throw new MCException(e);
  }

 }



 private void getConnection() {

  if (maker.getOneTransaction()) {
   // Ensure all action with in foreach is in the same transaction.
   con = DBConnectionStore.getConnection();
  }
  else
   con = DB.get().getConnection();  // Fetching an own connection to avoid problems with the consumer.

 }



 private void releaseConnection() {

  if (maker.getOneTransaction())
   DBConnectionStore.releaseConnection();
  else
   DB.get().releaseConnection(con);

 }



 /**
  * Resets the DataStore.
  *
  * <p>Deletes the internal result. This forces a new read on next access.</p>
  */

 @Override
 public synchronized void reset() {

  // Call super class for having care of the stamp.
  super.reset();

  count = null;

  logger.debug(() -> "reset(): " + ident());

 }



 private String makeQueryString() {

  String sd = DB.get().getDirtyReadSelectPostfix();

  String queryString = maker.getQueryString();

  if (Utilities.isEmpty(queryString)) {
   maker.clearValues();
   @SuppressWarnings("unchecked")
   Class<? extends JDBCBean> resultClass = (Class<? extends JDBCBean>) maker.getResultClass();
   String source = PersistentBean.getSource(resultClass);

   try {
    queryString = JDBCSupport.getLoadQueryStringFromBeanProperties(
     resultClass,
     source,
     maker.getLoadWith(),
     null,
     null,
     maker.getWhere() == null ?  null : makeWhereClause(maker.getWhere(), new StringBuilder(), source),
     false,
     maker.getDistinct()
    );

   }
   catch (Exception e) {
    throw new MCException(
     "makeQueryString(): Can't get query string from bean \"" +
      resultClass.getName() + "\"! Because: ",
     e
    );
   }

   if (!Utilities.isEmpty(maker.getGroupBy())) {
    queryString =
     queryString.concat(JDBCSupport.makeGroupClause(resultClass, maker.getGroupBy()));
   }

   if (!Utilities.isEmpty(maker.getHaving())) {
    queryString =
     queryString.concat(" having " + makeWhereClause(maker.getHaving(), new StringBuilder(), source));
   }

   if (!Utilities.isEmpty(maker.getOrderBy())) {
    queryString =
     queryString.concat(JDBCSupport.makeOrderClause(resultClass, maker.getOrderBy()));
   }
  }

  return queryString + sd;

 }



 private String makeWhereClause(Operator operator, StringBuilder sb, String source) {

  PropertyInfo prop;

  switch(operator.getType()) {

   case AND:
    makeWhereBraces(operator, " and ", sb, source);
   break;

   case OR:
    makeWhereBraces(operator, " or ", sb, source);
   break;

   case GIVEN:
    getPropertyInfo(operator, sb, source);
    sb.append(" is not null");
   break;

   case NOTGIVEN:
    getPropertyInfo(operator, sb, source);
    sb.append(" is null");
   break;

   case ISTRUE:
    prop = getPropertyInfo(operator);
    String base = prop.getBase();
    if (Utilities.isEmpty(base))
     sb.append(prop.getName()).append(" is true");
    else {
     @SuppressWarnings("unchecked")
     Class<? extends TypedBean> resultClass = (Class<? extends TypedBean>) maker.getResultClass();
     PropertyInfo propb = TypedBean.getPropertyInfo(resultClass, base);
     sb.append(propb.getName()).append("&").append(prop.getBitMask()).append(" != 0");
    }
   break;

   case NOTEQUAL:
    getTerm(operator, "!=?", "not in (", sb, source);
   break;

   case EQUAL:
    getTerm(operator, "=?", "in (", sb, source);
   break;

   case LIKE:
    getPropertyInfo(operator, sb, source);
    sb.append(" like ?");
    maker.setValue(operator.getArguments()[1]);
    break;

   case NOTLIKE:
    sb.append(" (not ");
    getPropertyInfo(operator, sb, source);
    sb.append(" like ?");
    maker.setValue(operator.getArguments()[1]);
    sb.append(" or ");
    getPropertyInfo(operator, sb, source);
    sb.append(" is null)");
    break;

   case UPPER:
    sb.append(" upper(");
    getPropertyInfo(operator, sb, source);
    sb.append(")");
    break;

   case GT:
    getTerm(operator, ">?", "", sb, source);
    break;

   case LT:
    getTerm(operator, "<?", "", sb, source);
    break;

   case GE:
    getTerm(operator, ">=?", "", sb, source);
    break;

   case LE:
    getTerm(operator, "<=?", "", sb, source);
    break;

  }

  return sb.toString();

 }



 private void getTerm(Operator operator, String op, String opA, StringBuilder sb, String source) {

  PropertyInfo pi = getPropertyInfo(operator, sb, source);

  if (operator.getArguments().length == 2) {
   sb.append(op);
   setValue(operator, pi, 1);
  }
  else {
   Object[] arguments = operator.getArguments();
   sb.append(
    Arrays.stream(arguments, 1, arguments.length)
     .map(arg -> {
      setValue(pi, arg);
      return "?";
     })
     .collect(Collectors.joining(", ", " " + opA, ")"))
   );
  }

 }



 private void setValue(Operator operator, PropertyInfo pi, int argIx) {

  if (pi != null  &&  pi.getPclass().isEnum())
   maker.setValue(((Enum) operator.getArguments()[argIx]).ordinal());  // Special handling for enums.
  else
   maker.setValue(operator.getArguments()[argIx]);

 }



 private void setValue(PropertyInfo pi, Object arg) {

  if (pi != null  &&  pi.getPclass().isEnum())
   maker.setValue(((Enum) arg).ordinal());  // Special handling for enums.
  else
   maker.setValue(arg);

 }



 private void makeWhereBraces(Operator operator, String sqlOp, StringBuilder sb, String source) {

  String delim = "(";

  for (Object op : operator.getArguments()) {
   sb.append(delim);
   makeWhereClause((Operator)op, sb, source);
   delim = sqlOp;
  }

  sb.append(")");

 }



 private PropertyInfo getPropertyInfo(Operator operator) {

  String alias;

  alias = (String)operator.getArguments()[0];
  @SuppressWarnings("unchecked")
  Class<? extends TypedBean> resultClass = (Class<? extends TypedBean>)maker.getResultClass();

  return TypedBean.getPropertyInfoByPath(resultClass, alias);

 }



 private PropertyInfo getPropertyInfo(Operator operator, StringBuilder sb, String source) {

  Object operator1 = operator.getArguments()[0];
  PropertyInfo prop = null;

  if (operator1 instanceof String) {
   String alias = (String) operator1;
   @SuppressWarnings("unchecked")
   Class<? extends TypedBean> resultClass = (Class<? extends TypedBean>) maker.getResultClass();

   prop = TypedBean.getPropertyInfoByPath(resultClass, alias);
   String val = null;

   if (prop.getPclass().isEnum()) {
//    sb.append(source).append('.');  // Todo: Check sub-beans.
//    sb.append(TypedBean.getPropertyInfo(resultClass, prop.getBase()).getName());
    val = source + "." + TypedBean.getPropertyInfo(resultClass, prop.getBase()).getName();
   }
   else {
    val = source + "." + prop.getName();
//    sb.append(source).append('.');  // Todo: Check sub-beans.
//    sb.append(prop.getName());
   }

   sb.append(JDBCSupport.mapFunction(prop.getPropertyFuntion(), val));
  }
  else
   makeWhereClause((Operator)operator1, sb, source);

  return prop;

 }



 /**
  * Counts the number of Beans.
  *
  * @return The number of Beans or records.
  */

 @Override
 public Long count() {

  if (count == null) {

   if (countString == null) {
    @SuppressWarnings("unchecked")
    Class<T> resultClass = (Class<T>) maker.getResultClass();
    maker.clearValues();
    String source = PersistentBean.getSource(resultClass);
    StringBuilder sb = new StringBuilder("select count(1) from ")
     .append(source);
    Operator where = maker.getWhere();
    if (where != null) {
     sb.append(" where ");
     makeWhereClause(where, sb, source);
    }
    countString = sb.toString();
    countValues = maker.getValues();
   }

   getConnection();
   rs = JDBCSupport.queryResultSet(con, countString, countValues);

   try {
    rs.next();
    count = rs.getLong(1);

    rs.close();
   } catch (SQLException e) {
    throw new MCException(e);
   }
   releaseConnection();
  }

  return count;

 }

}
