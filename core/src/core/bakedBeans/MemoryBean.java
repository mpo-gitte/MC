package core.bakedBeans;



import core.exceptions.BeanException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;



/**
 * This class implements a PersistentBean which can be loaded and saved in Memory.
 *
 * @version 2023-08-20
 * @author pilgrim.lutz@imail.de
 */

public abstract class MemoryBean extends PersistentBean {

 private static final HashMap<String, HashMap<Object, MemoryBean>> dataMaps = new HashMap<>();



 /**
  * Stores the bean in the underlying map.
  *
  * @param asNew If true the bean is assumed to be a new bean.
  *
  * @throws BeanException If problems occur.
  */

 protected void store(boolean asNew) throws BeanException {

  Object keyValue = null;

  try {
   keyValue = getKeyValue();
  }
  catch (Exception e) {
   throw new BeanException(e);
  }

  if (asNew) {
   synchronized (this.getClass()) {
    getProperties().putAll(getSettedProperties());
    getMap().put(keyValue, this);
   }
  }
  else {
   PersistentBean b = getMap().get(keyValue);
   b.getProperties().putAll(getSettedProperties());
  }

 }



 /**
  * Loads the bean from the map system for internal purposes.
  *
  * @param key The key.
  *
  * @return The loaded bean.
  */

 @Override
 public MemoryBean loadInternal(Object key) {

  synchronized (this.getClass()) {
   if (getMap().containsKey(key)) {
    MemoryBean memoryBean = getMap().get(key);

    for (String propName : memoryBean.getPropertyInfosByName().keySet()) {
     Object val = memoryBean.getByName(propName);
     this.setRawByName(propName, val);
    }

    // If bean was empty it is now clean.
    // If bean was dirty status is managed by setRawByName().

    if (status == Status.empty)
     status = Status.clean;
   }
  }

  return this;

 }



 private synchronized HashMap<Object, MemoryBean> getMap() {

  HashMap<Object, MemoryBean> m = dataMaps.get(getSource());

  if (m == null) {
   m = new HashMap<>();
   dataMaps.put(getSource(), m);
  }

  return m;

 }



 /**
  * Drop a "table".
  *
  * @param source The name of the source map.
  */

 public static void drop(String source) {

  HashMap<Object, MemoryBean> m = dataMaps.remove(source);

 }



 /**
  * Return all MemoryBeans for the class of this instance.
  *
  * @return A Set with Beans.
  */

 public Set<Map.Entry<Object, MemoryBean>> getAll() {

  return getMap().entrySet();

 }



 /**
  * Removes (deletes) the bean in the map.
  *
  * @throws BeanException If problems occurred.
  */

 @Override
 public void remove() throws BeanException {

  synchronized (this.getClass()) {
   Object keyValue = null;
   try {
    keyValue = getKeyValue();
   }
   catch (Exception e) {
    throw new BeanException(e);
   }

   getMap().remove(keyValue);
  }

 }



 /**
  * Rollbacks changes in the map.
  *
  * <p>Not necessary for MemoryBeans.</p>
  */

 protected void rollback() throws BeanException {

 }

}
