package core.bakedBeans;


import core.exceptions.BeanException;
import core.exceptions.MCException;
import core.util.DBConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;



/**
 * Class with support methods for JDBC access.
 *
 * @version 2024-09-18
 * @author lp
 */

class JDBCSupport {

 protected static final Logger logger = LogManager.getLogger(JDBCSupport.class);


 /**
  * Fill bean properties from ResultSet data.
  *
  * @param bean The bean to fill.
  * @param rs The ResultSet.
  * @param rsm The ResultSetMetaData. Used for column identification.
  */

 static void fillBeanFromResultset(PersistentBean bean, ResultSet rs, ResultSetMetaData rsm) {

  int id;
  String sd;

  try {
   for (id = 1; id <= rsm.getColumnCount(); id ++) {
    sd = rsm.getColumnName(id);


    // Preparing the bean path.

    String[] beanPath = sd.split("\\$");
    int beanPathLength = beanPath.length - 1;


    // Find the bean to fill.

    PersistentBean beanToSet = bean;

    for (int id1 = 0; id1 < beanPathLength; id1 ++)
     beanToSet = (PersistentBean)beanToSet.getByName(beanPath[id1].toLowerCase());


    // Set property.

    String propName = beanPath[beanPathLength].toLowerCase();
    Class<?> pclass = beanToSet.getPropertyClassByName(propName);

    Object o = rs.getObject(sd, pclass);
    if (rs.wasNull())  // Don't optimize! --- wasNull() does only work AFTER getObject()!
     o = null;
    beanToSet.setRawByName(propName, o);
   }
  }
  catch (SQLException e) {
   throw new BeanException(e);
  }

 }



 /**
  * Create a ResultSet.
  *
  * @param b This bean has the DB connection.
  * @param queryString The SQL statement.
  * @param values The values for the "?" in the statement.
  *
  * @return A ResultSet.
  */

 static ResultSet queryResultSet(PersistentBean b, String queryString, Object[] values) {

  return queryResultSet(((JDBCBean)b).getConnection(), queryString, values);

 }



 /**
  * Create a ResultSet.
  *
  * @param con DB connection.
  * @param queryString The SQL statement.
  * @param values The values for the "?" in the statement.
  *
  * @return A ResultSet.
  */

 static ResultSet queryResultSet(DBConnection con, String queryString, Object[] values) {


  // Prepare statement.

  PreparedStatement stmt = con.prepareStatement(queryString);


  // Fill values into the query.

  try {
   if (values != null) {
    int id = 1;
    for (Object value : values) {
     if (value == null)
      stmt.setNull(id++, Types.OTHER);  // null values require special handling for some DBs.
     else
      stmt.setObject(id++, value);
    }
   }


   // Result.

   return stmt.executeQuery();
  }
  catch (SQLException e) {
   logger.error(e);
   logger.error("queryResultSet(): Query string: \"" + queryString + "\"");
   throw new MCException(e);
  }

 }



 /**
  * Closes the result set and releases the DB connection for the bean.
  *
  * @param rs ResultSet.
  * @param b Bean.
  */

 static void close(ResultSet rs, JDBCBean b) {

  try {
   rs.close();  // Close the ResultSet.
  }
  catch (SQLException e) {
   throw new MCException(e);
  }
  finally {
   b.releaseConnection();  // Release the db connection associated with the bean.
  }

 }



 /**
  * Builds a query statement from bean properties.
  *
  * @param beanClass                  The class of the bean
  * @param source                     The table name
  * @param propertiesToLoad           Alias of properties to load.
  * @param keyName                    Name of the key
  * @param lkey                       Last key (used by recursion).
  * @param additionalWhereClause      A string with addtions to the where clause.
  * @param suppressAggregateFunctions Don't include properties with aggregate functions.
  * @param distinct
  * @return The SQL-statement.
  */

 static String getLoadQueryStringFromBeanProperties(
  Class<? extends JDBCBean> beanClass,
  String source,
  String[] propertiesToLoad,
  String keyName,
  String lkey,
  String additionalWhereClause,
  boolean suppressAggregateFunctions,
  boolean distinct) {

  // Build the select statement from beans properties.

  List<String> columns = new ArrayList<>();
  List<String> tables = new ArrayList<>();
  List<String> whereClauses = new ArrayList<>();

  getSelectParts(
   columns,
   tables,
   whereClauses,
   beanClass,
   source,
   propertiesToLoad,
   0,
   keyName,
   lkey,
   suppressAggregateFunctions
  );

  StringBuilder fb = new StringBuilder();

  fb.append("select");
  if (distinct)
   fb.append(" distinct");
  String delim = " ";

  for (String s : columns) {
   fb.append(delim).append(s);
   delim = ",";
  }

  fb.append(" from");
  delim = " ";

  for (String s : tables) {
   fb.append(delim).append(s);
   delim = ",";
  }

  delim = " where ";

  for (String s : whereClauses) {
   fb.append(delim).append(s);
   delim = " and ";
  }

  if (!Utilities.isEmpty(additionalWhereClause))
   fb.append(delim).append(additionalWhereClause);

  return
   fb.toString();

 }



 private static void getSelectParts(List<String> columns, List<String> tables, List<String> whereClauses,
                                    Class<? extends JDBCBean> beanClass, String name, String[] propertiesToLoad,
                                    int level, String keyName, String lkey, boolean suppressAggregateFunctions) {

  PersistentBeanDescription a =
   PersistentBean.getAnnotation(beanClass, PersistentBeanDescription.class);

  // The table name.
  String source = a.source();
  if (!tables.contains(source))  // May be table is already in because of cyclic relations.
   tables.add(source);

  // Get the "normal" properties...
  Collection<PropertyInfo> props = getProps(beanClass, propertiesToLoad, suppressAggregateFunctions);

  final int flevel = level;

  // ... and ddd their names to the list of columns.
  props.stream()
   .map((prop) -> getColumnDef(beanClass, name, source, flevel, prop, true))
   .forEach(columns::add);

  // Get the "sub-bean" relations.
  props = getPropsRelations(beanClass, propertiesToLoad);

  // Those properties have to processed by getSelectParts() to get their properties.
  for (PropertyInfo p : props) {
   @SuppressWarnings("unchecked")
   Class<? extends JDBCBean> pclass = (Class<? extends JDBCBean>) p.getPclass();
   getSelectParts(columns, tables, whereClauses,
    pclass,
    p.getName(),
    p.getLoadWith(),
    ++level,
    "",
    source + "." + TypedBean.getPropertyInfo(beanClass, p.getBase()).getName(),
    false);
  }

  // The where-clause.
  if (!Utilities.isEmpty(lkey)) {
   String kn = keyName;
   if (Utilities.isEmpty(kn))
    kn = a.keyName();
   whereClauses.add(source + "." + kn + "=" + lkey);
  }

 }



 private static String getColumnDef(Class<? extends JDBCBean> beanClass, String name, String source, int flevel, PropertyInfo prop, boolean addName) {

  String colDef;

  if (prop.getPropertyFuntion() != PropertyFunction.none) {
   String sd = prop.getName();
   if (!Utilities.isEmpty(prop.getBase())) {
    try {
     sd = TypedBean.getPropertyInfo(beanClass, prop.getBase()).getName();
    } catch (Exception e) {
     throw new MCException(e);
    }
   }
   colDef = mapFunction(prop.getPropertyFuntion(), source + "." + sd);
  } else
   colDef = source + "." + prop.getName();

  if (flevel > 0)
   return
    colDef + " " + name + "$" + prop.getName();

  if (prop.getPropertyFuntion() != PropertyFunction.none  &&  addName)
   return
    colDef + " " + prop.getName();

  return colDef;

 }



 public static String mapFunction(PropertyFunction propertyFunction, String value) {

  switch (propertyFunction) {

   case min:
    return "min(" + value + ")";
   case max:
    return "max(" + value + ")";
   case count:
    return "count(" + value + ")";
   case avg:
    return "avg(" + value + ")";
   case upper:
    return "upper(" + value + ")";
   case lower:
    return "lower(" + value + ")";
   case year:
    return "cast(extract(year from " + value + ") as integer)";
   case sum:
    return "sum(" + value + ")";
   case concat:
    return "string_agg(" + value + ",';')";
  }

  return value;

 }



 private static Collection<PropertyInfo> getPropsRelations(Class<? extends JDBCBean> beanClass, String[] propertiesToLoad) {

  return
   getPropertyInfosLoadRelations(beanClass).values().stream()
    .filter(pi ->
     Utilities.isEmpty(propertiesToLoad) || propertiesToLoad.length == 0  ||   // Property is valid if no properties to load or...
      Arrays.stream(propertiesToLoad).anyMatch(a -> a.equals(pi.getAlias())))  // ... the property alias is in the list
    .collect(Collectors.toCollection(ArrayList<PropertyInfo>::new));

 }



 private static Collection<PropertyInfo> getProps(Class<? extends JDBCBean> beanClass, String[] propertiesToLoad, boolean suppressAggregateFunctions) {

  return
   getPropertyInfosToLoad(beanClass, suppressAggregateFunctions).values().stream()
    .filter(pi ->
      Utilities.isEmpty(propertiesToLoad) || propertiesToLoad.length == 0  ||   // Property is valid if no properties to load or...
      Arrays.stream(propertiesToLoad).anyMatch(a -> a.equals(pi.getAlias())))  // ... the property alias is in the list
    .collect(Collectors.toCollection(ArrayList<PropertyInfo>::new));

 }



 /**
  * Returns a map with properties which can be loaded from DB.
  *
  * @param beanClass The class of the bean.
  * @param suppressAggregateFunctions Exclude properties with aggregate function
  * @return The map containing the properties.
  */

 static Map<String, PropertyInfo> getPropertyInfosToLoad(Class<? extends JDBCBean> beanClass, boolean suppressAggregateFunctions) {

  return
   TypedBean.getPropertyInfos(beanClass).values().stream()
    .filter(pi -> !suppressAggregateFunctions  ||  !pi.getPropertyFuntion().isAggregate())
    .filter(pi -> pi.getRelationType() == RelationType.none)
    .filter(PropertyInfo::getLoad)
    .filter(pi -> !Utilities.isEmpty(pi.getName()))
    .filter(pi -> Utilities.isEmpty(pi.getDataStoreName()))
    .collect(Collectors.toMap(PropertyInfo::getAlias, pi -> pi));

 }



 /**
  * Builds an order-by-clause.
  *
  * @param beanClass The bean class
  * @param orderBy An array with alias. Eventually prefixed with a property alias of a sub bean
  *                and a "+" for ascending of a "-" for descending.
  * @return A String with complete order clause.
  */

 static String makeOrderClause(Class<? extends JDBCBean> beanClass, String[] orderBy) {

  if (!Utilities.isEmpty(orderBy)  &&  orderBy.length > 0)
   return
    Arrays.stream(orderBy)
     .map(order -> {
      try {
       // Prefix "+" or "-" for direction "asc" or "desc".
       String dir = "";
       if (order.startsWith("+")) {
        order = order.substring(1);
        dir = " asc";
       }
       else if (order.startsWith("-")) {
        order = order.substring(1);
        dir = " desc";
       }
       // Path consists of BeanClass:property:property
       PropertyInfo piSort = TypedBean.getPropertyInfoByPath(beanClass, order);
       @SuppressWarnings("unchecked")
       Class<? extends PersistentBean> clazz = (Class<? extends PersistentBean>) piSort.getForClass();
       return
        getColumnDef(
         beanClass,
         order,
         PersistentBean.getSource(clazz),
         0,
         piSort,
         false
        ) + dir;
      }
      catch (Exception e) {
       throw new MCException(e);
      }

     })
     .collect(Collectors.joining(", ", " order by ", ""));

  return "";

 }



 /**
  * Builds an group-by-clause.
  *
  * @param beanClass The bean class
  * @param groupBy An array with alias.
  * @return A String with complete group clause.
  */

 static String makeGroupClause(Class<? extends JDBCBean> beanClass, String[] groupBy) {

  if (!Utilities.isEmpty(groupBy)  &&  groupBy.length > 0)
   return
    Arrays.stream(groupBy)
     .map(group -> {
      try {
       // Path consists of BeanClass:property:property
       PropertyInfo piSort = TypedBean.getPropertyInfoByPath(beanClass, group);
       @SuppressWarnings("unchecked")
       Class<? extends PersistentBean> clazz = (Class<? extends PersistentBean>) piSort.getForClass();
       return
        getColumnDef(
         beanClass,
         group,
         PersistentBean.getSource(clazz),
         0,
         piSort,
         false
        );
      }
      catch (Exception e) {
       throw new MCException(e);
      }

     })
     .collect(Collectors.joining(", ", " group by ", ""));

  return "";

 }



 /**
  * Returns a map with properties which can be loaded from DB.
  *
  * @param beanClass The class of the bean.
  *
  * @return The map containing the properties.
  */

 static Map<String, PropertyInfo> getPropertyInfosLoadRelations(Class<? extends JDBCBean> beanClass) {

//  HashMap<String, PropertyInfo> props = new HashMap<>();
//
//  TypedBean.getPropertyInfos(beanClass).forEach((key, val) -> {
//   if (val.getRelationType() != RelationType.none  &&
//       Utilities.isClassDerivedFrom(val.getPclass(), JDBCBean.class)  &&
//       Utilities.isEmpty(val.getDataStoreName())) {  // Properties without any relation are treated as DB columns.
//    props.put(key, val);
//   }
//  });
//
//  return props;

  return
   TypedBean.getPropertyInfos(beanClass).values().stream()
    .filter(pi -> pi.getRelationType() != RelationType.none)
    .filter(pi -> pi.getRelationType() != RelationType.loose)
    .filter(pi -> Utilities.isClassDerivedFrom(pi.getPclass(), JDBCBean.class))
    .filter(pi -> Utilities.isEmpty(pi.getDataStoreName()))
    .collect(Collectors.toMap(PropertyInfo::getAlias, pi -> pi));

 }

}
