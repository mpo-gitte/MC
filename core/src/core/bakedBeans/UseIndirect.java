package core.bakedBeans;

import java.lang.annotation.*;



/**
 * This annotation is used by the BeanBaker annotation processor to trigger the java compiler.
 *
 * @version 2018-12-19
 * @author pilgrim.lutz@imail.de
 */

@Documented
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.METHOD, ElementType.PACKAGE})
public @interface UseIndirect {

 Class<? extends TypedBean> value();

}
