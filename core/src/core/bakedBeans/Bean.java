package core.bakedBeans;

import java.util.*;


/**
 * This class is the base class for all beans.
 * <p>It is only a wrapper around a Map (currently a HashMap).</p>
 *
 * @version 2024-12-20
 * @author lp
 */

public abstract class Bean {

 private final HashMap<String, Object> properties;



 /**
  * Creates the bean and instantiates the internal Map.
  */

 public Bean() {

  properties = new HashMap<>();

 }



 /**
  * Set the property "key".
  *
  * @param key The key for the Map.
  * @param value The value for the Map.
  */

 public Bean set(String key, Object value) {

  if (has(key))
   properties.remove(key);

  properties.put(key, value);

  return this;

 }



 /**
  * Gets the value with the key "key" from the Map.
  *
  * @param key Tke key for the Map.
  * @return The value from the Map.
  */

 public Object get(String key) {

  return properties.get(key);

 }



 /**
  * Checks whether a property "key" is in the internal Map.
  *
  * @param key The key for the Map.
  * @return true if key is in the Map. False otherwise.
  */

 public boolean has(String key) {

  return properties.containsKey(key);

 }



 /**
  * Return the internal Map.
  *
  * @return The internal map.
  */

 public Map<String, Object> getProperties() {

  return properties;

 }



 /**
  * Return the internal Map for serialization. Derived beans may return strange things...
  *
  * @return The internal map.
  */

 public Map<String, Object> getPropertiesForSerialization() {

  return properties;

 }



 /**
  * Return the internal Map or whatever a derived bean may decide.
  *
  * @return The internal map.
  */

 public Map<String, Object> getCurrentProperties() {

  return properties;

 }


 
 /**
  * Checks if the bean is empty.
  *
  * <p>"Empty" means the bean has no properties. Derivates of this bean may have other conditions for emptiness.</p>
  *
  * @return true in case the bean has no properties. False otherwise.
  */

 public boolean isEmpty() {
 
  return properties.isEmpty();

 }



 /**
  * Checks if this bean is equal to the other bean.
  *
  * <p>Beans are equal if all their properties are equal.</p>
  *
  * <p>Empty beans are equal.</p>
  *
  * @param other The other bean to compare.
  * @return true if the beans have equal content. False otherwise.
  */

 @Override
 public boolean equals(Object other) {

  if (other.getClass() != getClass())  // Different types aren't equal!
   return false;

  return properties.equals(((Bean)other).getProperties());

 }



 /**
  * Returns the hash code of the content of the bean.
  *
  * @return The hash code.
  */

 @Override
 public int hashCode() {

  return properties.hashCode();

 }

}
