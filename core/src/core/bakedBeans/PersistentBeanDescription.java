package core.bakedBeans;

import java.lang.annotation.*;



/**
 * This annotation provides information about persistent beans.
 *
 * @version 2016-03-10
 * @author pilgrim.lutz@imail.de
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface PersistentBeanDescription {

 String source();
 String keyName() default "";
 Class<? extends Object> keyClass() default java.lang.Object.class;
 Class<? extends IKeyMaker> keyMakerClass() default DoesReallyNothingKeyMaker.class;

}
