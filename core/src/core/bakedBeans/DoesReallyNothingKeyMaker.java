package core.bakedBeans;

/**
 * This KeyMaker does'nt make a key.
 *
 * <p>It ist used as a kind of placeholder for beans which make keys by itself or use foreign
 * references as keys.</p>
 * 
 * @version 2012-04-21
 * @author lp
 */
public class DoesReallyNothingKeyMaker implements IKeyMaker {



 public void startMakeKey(PersistentBean forBean) throws BakedBeansKeyMakerException {

 }



 public void endMakeKey(PersistentBean forBean) throws BakedBeansKeyMakerException {

 }
}
