package core.bakedBeans;



/**
 * Operators for the where-method of DataStores.
 *
 * @version 2024-09-16
 * @author lp
 */

public class Operator {

 /**
  * The type of the operator.
  */

 public enum Type {
  NOTEQUAL,
  EQUAL,
  NOTLIKE,
  LIKE,
  NOTGIVEN,
  GIVEN,
  AND,
  OR,
  ISTRUE,
  UPPER,
  GT,
  LT,
  GE,
  LE
 };

 private final Type type;
 private final Object[] arguments;



 private Operator(Type type, Object... arguments) {

  this.type = type;
  this.arguments = arguments;

 }



 /**
  * Creates a new Operator.
  *
  * @param type The type for the new Operator.
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator create(Type type, Object... arguments) {

  return
   new Operator(type, arguments);

 }



 /**
  * Creates a new Operator of type EQUAL.
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator EQUAL(Object... arguments) {

  return
   create(Type.EQUAL, arguments);

 }



 /**
  * Creates a new Operator of type NOTEQUAL.
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator NOTEQUAL(Object... arguments) {

  return
   create(Type.NOTEQUAL, arguments);

 }



 /**
  * Creates a new Operator of type NOTLIKE.
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator NOTLIKE(Object... arguments) {

  return
   create(Type.NOTLIKE, arguments);

 }



 /**
  * Creates a new Operator of type LIKE.
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator LIKE(Object... arguments) {

  return
   create(Type.LIKE, arguments);

 }



 /**
  * Creates a new Operator of type NOTGIVEN.
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator NOTGIVEN(Object... arguments) {

  return
   create(Type.NOTGIVEN, arguments);

 }



 /**
  * Creates a new Operator of type GIVEN.
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator GIVEN(Object... arguments) {

  return
   create(Type.GIVEN, arguments);

 }



 /**
  * Creates a new Operator of type AND.
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator AND(Object... arguments) {

  return
   create(Type.AND, arguments);

 }



 /**
  * Creates a new Operator of type OR.
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator OR(Object... arguments) {

  return
   create(Type.OR, arguments);

 }



 /**
  * Creates a new Operator of type ISTRUE.
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator ISTRUE(Object... arguments) {

  return
   create(Type.ISTRUE, arguments);

 }



 /**
  * Creates a new Operator of type UPPER.
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator UPPER(Object... arguments) {

  return
   create(Type.UPPER, arguments);

 }



 /**
  * Creates a new Operator of type GT (greater than).
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator GT(Object... arguments) {

  return
   create(Type.GT, arguments);

 }



 /**
  * Creates a new Operator of type LT (less than).
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator LT(Object... arguments) {

  return
   create(Type.LT, arguments);

 }



 /**
  * Creates a new Operator of type GE (greater or equal than).
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator GE(Object... arguments) {

  return
   create(Type.GE, arguments);

 }



 /**
  * Creates a new Operator of type LE (less or equal than).
  *
  * @param arguments Its arguments.
  *
  * @return A new Operator.
  */

 public static Operator LE(Object... arguments) {

  return
   create(Type.LE, arguments);

 }



 /**
  * Gets the type of this operator.
  *
  * @return The type.
  */

 Type getType() {

  return type;

 }



 /**
  * Gets the arguments of this operator.
  *
  * @return The arguments.
  */

 public Object[] getArguments() {

  return arguments;

 }



 /**
  * Return a string representation of this operator and all its arguments.
  *
  * <p>If arguments are Operators this method will be called recursively.</p>
  *
  * @return A String.
  */

 public String toString() {

  StringBuilder sb = new StringBuilder();

  sb.append(this.type).append("(");

  for (Object op : getArguments())
   sb.append(op.toString());

  sb.append(")");

  return sb.toString();

 }

}
