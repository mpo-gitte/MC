package core.bakedBeans;



import core.exceptions.BeanException;



/**
 * Thrown if a key maker gets a problem.
 *
 * @version 2023-08-19
 * @author lp
 */

public class BakedBeansKeyMakerException extends BeanException {


 /**
  * Creates an exception.
  *
  * @param mess A message.
  */

 public BakedBeansKeyMakerException(String mess) {

  super(mess);

 }

}
