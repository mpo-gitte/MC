package core.bakedBeans;



import core.exceptions.BeanAccessException;
import core.exceptions.BeanException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.util.*;
import java.util.stream.Collectors;

import static core.bakedBeans.Operator.EQUAL;



/**
 * The PersistentBean is a bean which can be stored somewhere and can be loaded from there.
 *
 * <p>The implementations of this class are responsible for managing the storage of data.</p>
 *
 * <p>The PersistentBean maintains a second internal list of properties. The standard setter puts all set properties in this list.</p>
 *
 * @version 2024-12-27
 * @author lp
 *
 */

public abstract class PersistentBean extends TypedBean {

 private static Logger logger = LogManager.getLogger(PersistentBean.class.getName());
 private HashMap<String, Object> settedProperties;
 private IKeyMaker keyMaker = null;
 private HashMap<String, RelationInfo> relations = null;

 /**
  * The status of the bean.
  */
 protected Status status;



 /**
  * The PersistentBean dumps only its key.
  *
  * @return The String representation of the key.
  */

 @Override
 public String toString() {

  try {
   return getKeyName() + "=" + Utilities.objectToString(getKeyValue());
  }
  catch (Exception e) {
   logger.error("Can't dump bean to String! Bean: \"" + this.getClass().getName() + "\" Exception: " + e);
   return null;
  }

 }



 /**
  * The standard constructor.
  */

 public PersistentBean() {

  super();

  settedProperties = new HashMap<String, Object>();

  status = Status.empty;

 }



 /**
  * Sets a property in the bean. The property is identified by name.
  *
  * <p>Mainly used by a persistence system to fill a bean.</p>
  *
  * <p>This method checks whether or not the property is already set by a prior call to set(). If so and the values are equal
  * the property will be removed from the list of set properties. If this list becomes empty the status of the bean will be set
  * to clean.</p>
  *
  * @param name The name of the property.
  * @param value The value for the property. Currently, this method does no type checking!
  */

 protected void setRawByName(String name, Object value) {

  PropertyInfo pi = getStrictPropertyInfoByName(name);

  // ToConsider: Check the datatype of the value.
  //  checkDatatype(pi, value);

  super.stdSetter(pi, value);

  alignSetPropertiesAndStatus(name);

 }



 private void alignSetPropertiesAndStatus(String name) {

  if (settedProperties.containsKey(name)) {
   if (Utilities.isEqual(getProperties().get(name), settedProperties.get(name))) {
    settedProperties.remove(name);
    if (settedProperties.size() == 0)
     status = Status.clean;
   }
  }

 }



 /**
  * Compares this PersistentBean to another PersistentBean.
  *
  * @param otherBean The other PersistentBean.
  *
  * @return true if the key values are equal. False otherwise.
  */

 public boolean equals(Object otherBean) {

  PersistentBean b = (PersistentBean)otherBean;  // Should crash if anotherBean isn't a PersistentBean.

  try {
   Object bk = b.getKeyValueInternal();
   Object tk = getKeyValueInternal();
   return tk.equals(bk);
  }
  catch (Exception e) {/* Beans aren't equal if an exception is raised. */}

  return false;

 }



 /**
  * Own method to handle set properties.
  *
  * @return The hash value.
  */

 @Override
 public int hashCode() {

  return
   getCurrentProperties().hashCode();

 }



 /**
  * The standard getter.
  *
  * <p>Try to get the property from the list of set properties first.</p>
  *
  * @param pi The PropertyInfo
  *
  * @return The value of the property.
  */

 @Override
 @TypedBeanGetter
 protected Object stdGetter(PropertyInfo pi) {

  String key = pi.getName();  // Normal bean values are managed by name.

  if (settedProperties.containsKey(key))
   return settedProperties.get(key);

  return super.stdGetter(pi);

 }



 /**
  * Standard setter.
  *
  * <p>Sets the property in the list of set properties.</p>
  *
  * @param pi The PropertyInfo
  * @param value The value of the property.
  */

 @Override
 @TypedBeanSetter
 protected void stdSetter(PropertyInfo pi, Object value) {

  String key = pi.getName();  // Normal bean values are managed by name.

  logger.debug(() -> "stdSetter(): " + key + " value: " + value);

  Object oldval = getByName(key);
  if (oldval != null  &&  oldval.equals(value))
   return;  // Value already set

  if (has(key))
   settedProperties.remove(key);

  settedProperties.put(key, value);

  status = Status.dirty;

 }



 /**
  * A getter for relations n to one.
  *
  * @param pi The PropertyInfo
  *
  * @return The related data with type defined in PropertyInfo.pclass.
  */

 @TypedBeanGetter
 protected Object relN2OGetter(PropertyInfo pi) {

  String key = pi.getAlias();  // Relations are managed by aliases...

  RelationInfo ri = getRelation(key);

  if (ri == null) {
   PersistentBean b = (PersistentBean)makeBean(pi);

   Object relval = get(pi.getBase());  // ... but rely on a real property!
   if (relval != null)
    b.loadInternal(relval);

   ri = new RelationInfo(pi.getRelationType(), b);
   relations.put(key, ri);
  }

  return ri.getObject();

 }



 private Bean makeBean(PropertyInfo pi) {

  return BeanFactory.make(pi.getPclassAsBean());

 }



 /**
  * A getter for relations n to one.
  *
  * <p>The resulting data will not be loaded.</p>
  *
  * @param pi The PropertyInfo
  *
  * @return The related data with type defined in PropertyInfo.pclass.
  */

 @TypedBeanGetter
 protected Object relN2OGetterNL(PropertyInfo pi) {

  String key = pi.getAlias();

  RelationInfo ri = getRelation(key);

  if (ri == null) {
   Bean b = makeBean(pi);

   ri = new RelationInfo(pi.getRelationType(), b);
   relations.put(key, ri);
  }

  return ri.getObject();

 }



 /**
  * A getter for relations n-to-one.
  *
  * <p>This one uses the DataStore specified in PropertyInfo.dataStore..</p>
  *
  * @param pi The PropertyInfo
  *
  * @return The related data with type defined in PropertyInfo.pclass.
  */

 @TypedBeanGetter
 protected Object relN2OGetterDS(PropertyInfo pi) {

  String key = pi.getAlias();  // Relations are always managed by alias.

  Object o = get(pi.getBase());

  Bean returnBean = null;

  if (o != null) {

   @SuppressWarnings("unchecked")
   Class<? extends PersistentBean> pclassAsBean = (Class<? extends PersistentBean>) pi.getPclassAsBean();

   BeanList<? extends Bean> pl =
    BeanFactory.makeBeanList(
     pi.getDataStoreName(),
     pclassAsBean,
     EQUAL(getKeyName(pclassAsBean), o),
     this,
     pi.getCategory()
    );

   returnBean = pl.get(0);

  }
  else
   returnBean = makeBean(pi);

  RelationInfo ri = getRelation(key);

  if (ri == null) {
   ri = new RelationInfo(pi.getRelationType(), null);
   relations.put(key, ri);
  }

  ri.setObject(returnBean);

  return
   returnBean;

 }



 /**
  * Setter for n-to-one-relations.
  *
  * @param pi The PropertyInfo
  */

 @TypedBeanSetter
 protected void relN2OSetter(PropertyInfo pi, Object value) {

  String alias = pi.getAlias();

  PersistentBean b = (PersistentBean)value;
  b.save();
  this.set(pi.getBase(), b.getByName(b.getKeyName()));  // Set key.

  if (relations == null)
   relations = new HashMap<>();

  RelationInfo ri = new RelationInfo(pi.getRelationType(), b);
  relations.put(alias, ri);

 }



 /**
  * Getter for the primary key value.
  *
  * @param pi The PropertyInfo (not used here).
  *
  * @return The related data with type defined in PropertyInfo.pclass.
  */

 @TypedBeanGetter
 protected Object keyGetter(PropertyInfo pi) {

  return get(getKeyName());

 }



 /**
  * Setter for the primary key value.
  *
  * @param pi The PropertyInfo (not used here).
  */

 @TypedBeanSetter
 protected void keySetter(PropertyInfo pi, Object value) {

  loadInternal(value);

 }



 /**
  * Getter for StringBeans.
  *
  * @param pi The PropertyInfo.
  *
  * @return The related data with type defined in PropertyInfo.pclass.
  */

 @TypedBeanGetter
 protected Object stringBeanGetter(PropertyInfo pi) {

  String key = pi.getAlias();  // Relations are always managed by alias.

  RelationInfo ri = getRelation(key);

  if (ri == null) {
   StringBean b = (StringBean)makeBean(pi);
   Object relval = get(pi.getBase());
   if (relval != null)
    b.fromString((String)relval);

   ri = new RelationInfo(pi.getRelationType(), b);
   relations.put(key, ri);
  }

  return ri.getObject();

 }



 /**
  * Setter for StringBeans.
  *
  * @param pi The PropertyInfo.
  */

 @TypedBeanSetter
 protected void stringBeanSetter(PropertyInfo pi, Object value) {

  TypedBean b = (TypedBean)value;
  this.set(pi.getBase(), b.toString());

 }



 /**
  * Getter for to get a BeanList from a String.
  *
  * @param pi The PropertyInfo.
  *
  * @return The BeanList with included type defined in PropertyInfo.iclass.
  */

 @TypedBeanGetter
 protected Object beanListFromStringGetter(PropertyInfo pi) {

  String key = pi.getAlias();  // Relations are always managed by alias.

  RelationInfo ri = getRelation(key);

  if (ri == null) {
   BeanList<? extends Bean> bl = new BeanList<>(pi.getIclassAsBean());

   Object relval = get(pi.getBase());
   if (relval != null)
    bl.fromString((String)relval);

   ri = new RelationInfo(pi.getRelationType(), bl);
   relations.put(key, ri);
  }

  return ri.getObject();

 }



 /**
  * Setter for to get a BeanList from a String.
  *
  * @param pi The PropertyInfo.
  */

 @TypedBeanSetter
 protected void beanListFromStringSetter(PropertyInfo pi, Object value) {

  BeanList b = (BeanList)value;
  this.set(pi.getBase(), b.toString());

 }



 /**
  * Getter for sub beans.
  *
  * @param pi The PropertyInfo.
  *
  * @return The sub bean with included type defined in PropertyInfo.iclass.
  */

 @TypedBeanGetter
 protected Object subBeanGetter(PropertyInfo pi) {

  String key = pi.getAlias();  // Relations are always managed by alias.

  RelationInfo ri = getRelation(key);

  if (ri == null) {
   PersistentBean b =
    (PersistentBean)BeanFactory.makePersistentSubBean(this, pi.getPclassAsBean(), pi.getPrefix());

   ri = new RelationInfo(pi.getRelationType(), b);
   relations.put(key, ri);
  }

  return ri.getObject();

 }



 /**
  * Getter for one to n relations.
  *
  * <p>Warning: If the underlying DataStore is reset this relation would not be reset!
  * The DataStore is only a source and only used once.</p>
  *
  * @param pi The PropertyInfo.
  *
  * @return The BeanList with included type defined in PropertyInfo.iclass.
  */

 @TypedBeanGetter
 protected Object relO2NGetter(PropertyInfo pi) {

  String key = pi.getAlias();  // Relations are always managed by alias.

  RelationInfo ri = getRelation(key);

  if (ri == null) {
   BeanList pl =
    BeanFactory.makeBeanList(pi.getDataStoreName(), pi.getIclassAsBean(), getQuery(pi), this.getKeyValue(), pi.getCategory());
   ri = new RelationInfo(pi.getRelationType(), pl);
   relations.put(key, ri);
  }

  return ri.getObject();

 }



 /**
  * Getter for collections.
  *
  * <p>A collection "knows" to which bean it belongs and resets the only if this bean is given
  * to signal().</p>
  *
  * @param pi The PropertyInfo.
  *
  * @return The BeanList with included type defined in PropertyInfo.
  */

 @TypedBeanGetter
 protected Object collectionGetter(PropertyInfo pi) {

  String key = pi.getAlias();  // Relations are always managed by alias.

  RelationInfo ri = getRelation(key);

  if (ri == null) {
   DataStore ds = DataStoreManager.get().dataStore(pi.getDataStoreName(), pi.getCategory())
    .resetable((dsm, b) -> {
     if (!core.bakedBeans.Utilities.isEmpty(b)) {  // Got a signal bean?

      PersistentBean pbs = (PersistentBean) b;
      PersistentBean pb = (PersistentBean) dsm.getTaggedWith()[0];

      try {
       if (logger.isDebugEnabled())
        logger.debug("collectionGetter(): checking " + pbs.getKeyValue() + " against " + pb.getKeyValue());

       if (!pb.getKeyValue().equals(pbs.getKeyValue()))   // "Our" signal bean?
        return Boolean.FALSE;  // No! Don't reset!
      } catch (Exception e) {
       // Does not bother us so far.
       logger.error("collectionGetter(): Key comparison failed!");
      }
     }

     return Boolean.TRUE;  // To be "careful": Reset!
     })
    .taggedWith(this)
    .loadWith(pi.getLoadWith().length == 0 ? null : pi.getLoadWith())
    .where(EQUAL(pi.getBase(), this.getKeyValue()))
    .orderBy(getOrderBy(pi))
    .make(pi.getIclassAsBean());

   ri = new RelationInfo(pi.getRelationType(), ds);
   relations.put(key, ri);
  }

  if (logger.isDebugEnabled())
   logger.debug("collectionGetter(): DataStore: " + ((DataStore)ri.getObject()).ident());

  return ri.getObject();

 }



 /**
  * A hook to supply special sort criteria.
  *
  * @param pi The PropertyInfo
  *
  * @return sort criteria.
  */

 public String[] getOrderBy(PropertyInfo pi) {

  return pi.getOrderBy();

 }



 /**
  * Getter for simple DataStores.
  *
  * @param pi The PropertyInfo.
  *
  * @return A list with included type defined in PropertyInfo.iclass.
  */

 @TypedBeanGetter
 protected Object simpleDataStoreGetter(PropertyInfo pi) {

  return DataStoreManager.get().makeDataStore(
   pi.getDataStoreName(), pi.getCategory(),
   getQuery(pi),
   new Object[] {this.getKeyValue()},
   pi.getIclassAsBean()
  ).read();

 }



 /**
  * Getter for simple DataStores.
  *
  * <p>Adds a relation</p>
  *
  * @param pi The PropertyInfo.
  *
  * @return A list with included type defined in PropertyInfo.iclass.
  */

 @TypedBeanGetter
 protected Object simpleBeanListGetter(PropertyInfo pi) {

  BeanList<? extends Bean> l  = DataStoreManager.get().makeDataStore(
   pi.getDataStoreName(), pi.getCategory(),
   getQuery(pi),
   new Object[] {this.getKeyValue()},
   pi.getIclassAsBean()
  ).read();

  String key = pi.getAlias();  // Relations are managed by aliases...
  getRelation(key);

  RelationInfo ri = new RelationInfo(pi.getRelationType(), l);
  relations.put(key, ri);

  return
   l;

 }



 /**
  * A setter for a BeanList.
  *
  * @param pi The property to set.
  * @param value The value. A BeanList.
  */

 @TypedBeanSetter
 protected void simpleBeanListSetter(PropertyInfo pi, Object value) {

  // Does only work with BeanLists!
  @SuppressWarnings("unchecked")
  BeanList<PersistentBean> ls = (BeanList<PersistentBean>)value;

  ls
   .stream()
   .filter(PersistentBean::isDirty)
   .forEach(b -> processBean(pi, b));

 }



 /**
  * Get or build the query string.
  *
  * @param pi PropertyInfo
  *
  * @return The query string
  */

 protected String getQuery(PropertyInfo pi) {

  return pi.getQuery();

 }



 /**
  * Setter for one to n relations.
  *
  * @param pi The PropertyInfo.
  */

 @TypedBeanSetter
 protected void relO2NSetter(PropertyInfo pi, Object value) {

  @SuppressWarnings("unchecked")
  List<PersistentBean> beans = (List<PersistentBean>)value;

  for (PersistentBean b: beans) {
   if (b.getStatus() == Status.dirty) {
    b.set(pi.getBase(), getKeyValue());
    b.save();
   }
  }

 }



 /**
  * Setter for collections.
  *
  * @param pi The PropertyInfo.
  */

 @TypedBeanSetter
 protected void collectionSetter(PropertyInfo pi, Object value) {

  // Does only work with DataStores!
  @SuppressWarnings("unchecked")
  DataStore<PersistentBean> ds = (DataStore<PersistentBean>)value;

  if (ds.isRead()) {

   if (ds.isReadBean()) {
    PersistentBean bean = ds.readBean();
    if (bean.getStatus() == Status.dirty)
     processBean(pi, bean);
   }
   else if (ds.isCollected())
    ds.collect()
     .stream()
     .filter(PersistentBean::isDirty)
     .forEach(b -> processBean(pi, b));
   else
    ds.read()
     .stream()
     .filter(PersistentBean::isDirty)
     .forEach(b -> processBean(pi, b));
  }

 }



 private void processBean(PropertyInfo pi, PersistentBean b) {

   b.set(pi.getBase(), getKeyValue());
   b.save();

 }



 /**
  * Checks if the bean has the given property set.
  *
  * @param key The name of the property.
  *
  * @return true if property is set. False otherwise.
  */

 public boolean has(String key) {

  if (settedProperties.containsKey(key)) return true;

  return super.has(key);

 }



 /**
  * Sets a property from a String.
  *
  * <p>Because a TypedBean should know about the data types of its properties it should be able to perform
  * the data conversion.</p>
  *
  * <p>If the given property is the name of the primary alias the bean will be loaded.
  *
  * @param alias The alias of the property.
  * @param value The value for the property.
  */

 public void setFromString(String alias, String value) {

  PropertyInfo propertyInfo = getPropertyInfos().get(alias);

  setFromString(propertyInfo, value);

 }



 private void setFromString(PropertyInfo propertyInfo, String value) {

  if (denyPropertySetFromString(propertyInfo))
   throw
    new BeanAccessException("Property \"" + propertyInfo.getAlias() + "\" may not be set!");

  Object keyVal = this.getKeyValue();

  Object i = Utilities.makeGeneralValue(value, propertyInfo.getPclass());

  set(propertyInfo.getAlias(), i);

  if (!Utilities.isEqual(keyVal, this.getKeyValue()))
   load(this.getKeyValue());

 }



 protected boolean denyPropertySetFromString(PropertyInfo propertyInfo) {

  return false;  // Always assignable. But see derived beans.

 }



 /**
  * Sets a property from a String.
  *
  * <p>Because a TypedBean should know about the data types of its properties it should be able to perform
  * the data conversion.</p>
  *
  * <p>If the given property is the name of the primary name the bean will be loaded.
  *
  * @param name The name of the property.
  * @param value The value for the property.
  */

 public void setFromStringByName(String name, String value) {

  PropertyInfo propertyInfo = getPropertyInfosByName().get(name);

  setFromString(propertyInfo, value);

 }



 /**
  * Fetches an Annotation. Searches over the whole class hierarchy.
  *
  * @param annotationClass Annotation
  * @return The Annotation
  */

 protected <A extends Annotation> A getAnnotation(java.lang.Class<A> annotationClass) {

  return getAnnotation(this.getClass(), annotationClass);

 }



 /**
  * Fetches an Annotation. Searches over the whole class hierarchy.
  *
  * @param beanClass Bean class to start.
  * @param annotationClass Annotation
  * @return The Annotation
  */

 protected static <A extends Annotation> A getAnnotation(Class<? extends TypedBean> beanClass, Class<A> annotationClass) {

  Class cl = beanClass;

  while (cl != null) {
   @SuppressWarnings("unchecked")
   A a = (A)cl.getAnnotation(annotationClass);
   if (a != null)
    return a;

   cl = cl.getSuperclass();
  }

  return null;

 }



 /**
  * Returns the name of the data source.

  * <p>That is the value of PersistentBeanDescription.source</p>

  * @return The name of the data source.
  */

 public String getSource() {

  return getAnnotation(PersistentBeanDescription.class).source();

 }



 /**
  * Returns the name of the data source.
  *
  * <p>That is the value of PersistentBeanDescription.source</p>
  *
  * @param beanClass The class of the bean.
  *
  * @return The name of the data source.
  */

 public static String getSource(Class<? extends PersistentBean> beanClass) {

  return PersistentBean.getAnnotation(beanClass, PersistentBeanDescription.class).source();

 }



 /**
  * Returns the name of the primary key.
  *
  * @return The name.
  */

 public String getKeyName() {

  return getAnnotation(PersistentBeanDescription.class).keyName();

 }



 /**
  * Returns the name of the primary key.
  *
  * @param beanClass The class of the bean.
  *
  * @return The name.
  */

 public static String getKeyName(Class<? extends PersistentBean> beanClass) {

  return PersistentBean.getAnnotation(beanClass, PersistentBeanDescription.class).keyName();

 }



 /**
  * Returns the class of the primary key.
  *
  * @return The class.
  */

 public Class<? extends Object> getKeyClass() {

  return getAnnotation(PersistentBeanDescription.class).keyClass();

 }



 /**
  * Returns the class of for the key maker. Comes from the PersistentBeanDescription.
  *
  * @return The class.
  *
  * @see PersistentBeanDescription
  */

 protected Class<? extends IKeyMaker> getKeyMakerClass() {

  return getAnnotation(PersistentBeanDescription.class).keyMakerClass();

 }



 /**
  * Returns the value of the primary key.
  *
  * @return The value.
  */

 public Object getKeyValue() {

  return getByName(getKeyName());

 }



 /**
  * Returns the value of the key for internal purposes.
  *
  * @return The value.
  */

 public Object getKeyValueInternal() {

  return getByName(getKeyName());

 }



 /**
  * Loads the bean from the persistence system.
  *
  * <p>Normally this method calls loadInternal(). But see derivates for specialties.</p>
  *
  * @param key The key.
  *
  * @return The loaded bean.
  */

 public PersistentBean load(Object key) {

  return loadInternal(key);

 }



 /**
  * Loads the bean from the persistence system for internal purposes.
  *
  * @param key The key.
  *
  * @return The loaded bean.
  */

 public PersistentBean loadInternal(Object key) {

  return null;

 }



 /**
  * Saves the bean.
  */

 public void save() {

  if (core.util.Utilities.isEmpty(getKeyValue())) {
   if (maySaveNew())
    saveNew();
   else
    throw
     new BeanAccessException("Save bean rejected");
  }
  else {
   if (!maySave())
    throw
     new BeanAccessException("Save bean rejected");

   try {
    openStoreBracket();

    saveRelations(RelationType.beforeSave);

    if (status == Status.dirty)
     store(false);

    status = Status.clean;

    saveRelations(RelationType.afterSave);

    closeStoreBracket();

    clearRelations();
   }
   catch (Exception e) {
    rollback();
    throw e;
   }
  }

 }



 protected void clearRelations() {

  // ToDo: This could be much "finer". Consider about to delete only those relations relying on set properties.
  relations = null;

 }



 /**
  * Stores the bean in the persistence system.
  *
  * @param asNew If true the bean is assumed to be a new bean.
  */

 protected abstract void store(boolean asNew);



 /**
  * Removes (deletes) the bean from the persistence system.
  */

 protected abstract void remove();



 /**
  * Perform a rollback within the persistence system.
  */

 protected abstract void rollback();



 /**
  * Overwrite this method to check access rights.
  *
  * @return true and the bean will be saved.
  */

 protected boolean maySave() {

  return true;

 }



 /**
  * Overwrite this method to check access rights.
  *
  * @return true and the bean will be saved.
  */

 protected boolean maySaveNew() {

  return true;

 }



 /**
  * Overwrite this method to check access rights.
  *
  * @return true and the bean will be deleted.
  */

 protected boolean mayDelete() {

  return true;

 }



 /**
  * Saves this bean as a new bean.
  *
  * <p>Saves all the relations too.</p>
  */

 public void saveNew() {

  openStoreBracket();

  saveRelations(RelationType.beforeSave);

  synchronized(this.getClass()) {  // Making key generation and saving atomic.
   makeKeyStart();

   store(true);

   status = Status.clean;

   makeKeyEnd();
  }

  saveRelations(RelationType.afterSave);

  closeStoreBracket();

  clearRelations();

 }



 /**
  * Gets called before storing of bean and sub beans (relations) start.
  */

 protected void openStoreBracket() {

 }



 /**
  * Gets called after the bean and its sub beans are stored.
  */

 protected void closeStoreBracket() {

 }



 /**
  * Saves all the relations of this bean.
  *
  * @param relType the type of the relation.
  */

 protected void saveRelations(RelationType relType) {

  if (relations != null) {
   for (String key: relations.keySet()) {
    RelationInfo ri = relations.get(key);
    if (ri.type == relType)
     set(key, ri.object);
   }
  }

 }



 protected void makeKeyStart() {

  if (keyMaker == null)
   keyMaker = getKeyMaker();

  keyMaker.startMakeKey(this);

 }



 protected void makeKeyEnd() {

  keyMaker.endMakeKey(this);

 }



 protected IKeyMaker getKeyMaker() {

  try {
   return
    getKeyMakerClass().getConstructor().newInstance();
  }
  catch (Exception e) {
   throw new BeanException(e);
  }

 }



 /**
  * Return the internal Map of set properties.
  *
  * @return The internal map.
  */

 public Map<String, Object> getSettedProperties() {

  return settedProperties;

 }



 /**
  * Finds a persistence bean in the storage.
  *
  * <p>This implementation does nothing. Because how to find the bean depends
  * on the persistence system and therefor has to be implemented by derived beans.</p>
  *
  * @param props Properties for the search operation.
  *
  * @return Always null. Because a concrete bean has to define how to find a special bean.
  */

 public List<? extends PersistentBean> find(String ... props) {

  return null;

 }



 /**
  * Deletes the bean from the persistence system.
  */

 public void delete() {

  if (mayDelete())
   remove();
  else
   throw new BeanException("Delete bean rejected");

 }



 /**
  * Returns the status of the bean.
  *
  * @param status The status.
  */

 public void setStatus(Status status) {

  this.status = status;

 }



 /**
  * Sets the status of the bean.
  *
  * @return Its status.
  */

 public Status getStatus() {

  return status;

 }



 private static class RelationInfo {

  private final RelationType type;
  private Object object;



  public RelationInfo(RelationType type, Object object) {

   this.type = type;
   this.object = object;

  }



  public RelationType getType() {

   return type;

  }



  public Object getObject() {

   return object;

  }



  public void setObject(Object object) {

   this.object = object;

  }



  @Override
  public int hashCode() {

   return
    object.hashCode();

  }

 }



 /**
  * Checks if the bean is empty.
  *
  * <p>"Empty" means the bean has the status "empty".</p>
  *
  * @return true in case the bean has the status "empty". False otherwise.
  */

 public boolean isEmpty() {

  return status == Status.empty;

 }



 /**
  * Checks if the bean is dirty.
  *
  * <p>"Dirty" means the bean is altered but not saved.
  *
  * @return true in case the bean has the status "dirty". False otherwise.
  */

 public boolean isDirty() {

  return status == Status.dirty;

 }



 /**
  * Returns a TypedBean containing overwritten properties of this bean
  * with their old values.
  *
  * @return The TypedBean or null if bean isn't dirty.
  */

 public TypedBean oldValues() {

  if (!settedProperties.isEmpty()) {
   TypedBean r = BeanFactory.make(this.getClass());
   Set<java.util.Map.Entry<String, Object>> entries = settedProperties.entrySet();

   for (java.util.Map.Entry e: entries) {
    String k = (String)e.getKey();
    r.setByName(k, getProperties().get(k));
   }

   if (!r.isEmpty())
    return r;
  }

  return null;
 }



 /**
  * Calculates a hash code for the beans properties and all its relations.
  *
  * <p>This method isn't used normally. Because it has to load all the
  * data it might be very slow.</p>
  *
  * <p>To use the method for special needs override hashCode() and call
  * this method.</p>
  *
  * @see Bean#hashCode()
  *
  * @return The hash code.
  */

 public int deepHashCode() {

  final int prime = 31;
  int res = 1;

  // Make sure all bean properties are read.

  for (String palias : getPropertyInfos().keySet())
   get(palias);

  // Calculate the hash code for the properties.

  Map.Entry<String, Object> propentry;

  for (Iterator<Map.Entry<String, Object>> it = getProperties().entrySet().iterator(); it.hasNext(); res = res * prime + propentry.hashCode())
   propentry = it.next();

  // Extend the hash code with the relations.

  Map.Entry<String, RelationInfo> relentry;

  for (Iterator<Map.Entry<String, RelationInfo>> it = relations.entrySet().iterator(); it.hasNext(); res = res * prime + relentry.hashCode())
   relentry = it.next();

  return res;

 }



 private RelationInfo getRelation(String key) {

  if (relations == null)
   relations = new HashMap<String, RelationInfo>();

  return relations.get(key);

 }



 /**
  * Returns the current content of the bean.
  *
  * <p>This is a mix of the loaded content overridden by the set content.</p>
  *
  * @return The properties.
  */

 @Override
 public Map<String, Object> getCurrentProperties() {

  Map<String, Object> currentProperties = new HashMap<String, Object>(super.getCurrentProperties());

  currentProperties.putAll(settedProperties);

  return currentProperties;

 }



 /**
  * Return the internal Map for serialization.
  *
  * <p>Because the values are for external purposes. This bean returns a map with the alias of the
  * bean as the key./p>
  *
  * <p>For a clean bean only the id will be put in the map. For a dirty bean the set properties will be added.</p>
  *
  * @return The map.
  */

 @Override
 public Map<String, Object> getPropertiesForSerialization() {

  return
   getPropertyInfos().values().stream()
    .filter(pi -> pi.getRelationType() == RelationType.none  &&
                  (pi.getSerialize() == SerializationMode.always ||
                   pi.getSerialize() == SerializationMode.ifSet &&
                   getStatus() == Status.dirty &&
                   settedProperties.containsKey(pi.getName()) &&
                   !Utilities.isEmpty(getSafe(pi.getAlias()))
                  )
           )
    .collect(Collectors.toMap(PropertyInfo::getAlias, pi -> getSafe(pi.getAlias())));

 }



 /**
  * Status of the bean.
  */

 public enum Status {

  /**
   * Bean is empty. No properties are set. Bean not loaded from source.
   */
  empty,
  /**
   * Bean is loaded from source. No properties changed.
   */
  clean,
  /**
   * Properties changed.
   */
  dirty
 }
}
