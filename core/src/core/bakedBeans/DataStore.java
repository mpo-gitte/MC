package core.bakedBeans;


import core.exceptions.MCException;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;


/**
 * A DataStore is a wrapper around a List of Beans and a source which delivers the beans.
 *
 * @version 2024-12-23
 * @author lp
 *
 * @param <T> The type of the bean the datastore handles.
 */

public class DataStore<T extends Bean> {

 protected static Logger logger;

 protected DataStoreManager.DataStoreMaker maker;
 protected String ident;
 protected BeanList<T> result = null;
 protected BeanCollection<T> resultCollection = null;
 protected T resultBean = null;
 protected boolean busy = false;

 private int useCount;
 private LocalDateTime lastUse;
 private LocalDateTime creation;
 private int lifeCounter = 1;

 private int oldOffset = 0;
 private int oldCount = Integer.MAX_VALUE;



 public DataStore() {}


 /**
  * Signals for the method signal.
  */

 public enum DataStoreSignal {
  /**
   * During writing something.
   */
  write,
  /**
   * During deleting something.
   */
  delete
 }

 private long stamp = 0;



 /**
  * Initialize a DataStore from a DataStoreMaker.
  *
  * @param ident The ident string for this DataStore.
  * @param maker The DataStoreMaker with information for this DataStore.
  */

 public void init(String ident, DataStoreManager.DataStoreMaker maker) {

  this.ident = ident;
  this.maker = maker;

  this.creation = LocalDateTime.now();

 }



 /**
  * Read the list of beans.
  *
  * @return The list of beans or null if there are no beans.
  */

 public synchronized BeanList<T> read() {

  if (result == null) {
   logger.debug(() -> "read(): building new result for \"" + ident() + "\"");
   @SuppressWarnings("unchecked")
   Class<T> resultClass = (Class<T>)maker.getResultClass();
   result = new BeanList<T>(resultClass);
   forEach(result::add);
  }
  else
   logger.debug(() -> "read(): returning cached result for \"" + ident() + "\"");

  return result;

 }



 /**
  * Read the list of beans.
  *
  * @return The list of beans or null if there are no beans.
  */

 public synchronized BeanList<T> read(int offset, int count) {

  if (result == null) {
   logger.debug(() -> "read(): building new result for \"" + ident() + "\"");
   @SuppressWarnings("unchecked")
   Class<T> resultClass = (Class<T>)maker.getResultClass();
   result = new BeanList<T>(resultClass);
  }
  else
   logger.debug(() -> "read(): returning cached result for \"" + ident() + "\"");


  // Narrow the slice.

  int i = offset;

  while (i < (offset + count)  &&  i < (result.size() - 1)) {
   if (!Utilities.isEmpty(result.get(i))) {
    offset ++;
    count --;
   }
   else
    break;
   i++;
  }

  if (count > 0) {
   i = offset + count - 1;
   while (i > offset  &&  i < (result.size() - 1)) {
    if (!Utilities.isEmpty(result.get(i)))
     count --;
    else
     break;
    i--;
   }
  }

  // Read

  if (count > 0) {
   result.seek(offset);

   forEach(result::put, offset, count);
  }

  return result;

 }



 /**
  * Read the slice list of beans.
  *
  * @return Only the slice of the list of beans or null if there are no beans.
  */

 public synchronized List<T> readSlice(int offset, int count) {

  BeanList<T> res = read(offset, count);

  int toIndex = offset + count;

  toIndex = (toIndex > res.size()) ? res.size() : toIndex;

  return
   res.subList(offset, toIndex);

 }



 /**
  * Read the collection of beans.
  *
  * @return The collection of beans or null if there are no beans.
  */

 public synchronized BeanCollection<T> collect() {

  if (resultCollection == null) {
   logger.debug(() -> "collect(): building new collection for \"" + ident() + "\"");
   @SuppressWarnings("unchecked")
   Class<T> resultClass = (Class<T>)maker.getResultClass();
   resultCollection = new BeanCollection<T>(resultClass);
   forEach(resultCollection::addInitial);
  }
  else
   logger.debug(() -> "collect(): returning cached collection for \"" + ident() + "\"");

  return resultCollection;

 }



 /**
  * Read a single bean.
  *
  * @return The bean.
  */

 public synchronized T readBean() {

  if (resultBean == null) {
   logger.debug(() -> "readBean(): building new result for \"" + ident() + "\"");
   forEach((b) -> resultBean = b, (b) -> resultBean != null, 0, Integer.MAX_VALUE);
  }
  else
   logger.debug(() -> "readBean(): returning cached result for \"" + ident() +"\"");

  @SuppressWarnings("unchecked")
  Class<T> resultClass = (Class<T>)maker.getResultClass();
  return resultBean == null ? BeanFactory.make(resultClass) : resultBean;

 }



 /**
  * Iterate over the beans.
  *
  * <p>This method does not cache!</p>
  *
  * @param consumer A consumer called for each bean.
  */

 public synchronized void forEach(Consumer<T> consumer)  {

  forEach(consumer, 0, Integer.MAX_VALUE);

 }



 /**
  * Iterate over the beans.
  *
  * <p>This method does not cache!</p>
  *
  * @param consumer A consumer called for each bean.
  * @param offset Start.
  * @param count Number to read.
  */

 public synchronized void forEach(Consumer<T> consumer, int offset, int count)  {

  forEach(consumer, (T) -> false, offset, count);

 }



 /**
  * Iterate over the beans.
  *
  * <p>This method does not cache!</p>
  *
  * @param consumer A consumer called for each bean.
  * @param enough   A predicate to indicate the destination has got enough data.
  * @param offset Currently not supported!
  * @param count Currently not supported!
  */

 public synchronized void forEach(Consumer<T> consumer, Predicate<T> enough, int offset, int count) {

  try {

   // Open source.

   open(0, Integer.MAX_VALUE);

   // Scan source.

   @SuppressWarnings("unchecked")
   Stream<T> str = (Stream<T>)maker.getSourceStream().get();

   if (str != null) {
    str
     .takeWhile(b -> !enough.test(b))
     .forEach(consumer);

    sort();
    onReady();
   }
  }
  finally {
   close();
  }


 }



 /**
  * Main iteration loop.
  *
  * @param consumer For getting the beans.
  * @param enough   A predicate to indicate the destination has got enough data.
  * @param source   For supplying the beans.
  * @param offset The offset.
  * @param count The number of items to read.
  */

 public synchronized void forEach(Consumer<T> consumer, Predicate<T> enough,
                                  BiFunction<Integer, Integer, T> source,
                                  int offset, int count) {

  try {

   // Open source.

   open(offset, count);

   // Scan source.

   T b;

   while ((b = source.apply(offset, count)) != null) {  // Get bean from source.
    consumer.accept(b);  // Put bean to the consumer.

    if (enough.test(b))
     break;  // Consumer has got enough data.
   }

   sort();
   onReady();

  }
  catch (MCException ex) {
   throw ex;
  }
  catch (Exception ex) {
   throw new MCException(ex);
  }
  finally {
   close();
  }

 }



 /**
  * Opens the source.
  */

 protected void open(int offset, int count) {

  if (offset != oldOffset  ||  count != oldCount) {
   // If slice changes reset stamp!

   stamp = 0;

   oldOffset = offset;
   oldCount = count;

  }

  busy = true;

 }



 /**
  * Closes the source.
  *
  * <p>It's empty here. But some implementations need some action.</p>
  */

 protected void close() {

  busy = false;

 }



 private void sort() {

  @SuppressWarnings("unchecked")
  BeanComparator<T>[] sortBy = (BeanComparator<T>[])maker.getSortBy();

  if (sortBy != null)
   for (BeanComparator<T> bc : sortBy) {
    result.sort(bc);
   }

 }



 private void onReady() {

  Consumer<BeanList<? extends Bean>> onReady = maker.getOnReady();

  if (onReady != null)
   onReady.accept(result);

 }



 /**
  * Returns an ident string for this DataStore.
  *
  * @return the ident string.
  */

 public final String ident() {

  return ident;

 }



 /**
  * Increments the life counter of this DataStore. Called by the DataStoreManager.
  *
  * @param lifeCounter Initial value for life counter.
  * @return This DataStore.
  *
  * @see DataStoreManager
  */

 public DataStore<T> use(String lifeCounter) {

  ++ useCount;
  lastUse = LocalDateTime.now();

  if (lifeCounter != null)
   this.lifeCounter = Integer.parseInt(lifeCounter);

  return this;

 }



 /**
  * Returns the current life counter. Called by the DataStoreManager.
  *
  * @return The current value of the life counter of this DataStore.
  *
  * @see DataStoreManager
  */

 public int getLifeCounter() {

  return lifeCounter;

 }



 /**
  * Returns the use count. Called by the DataStoreManager.
  *
  * @return The current value of the use count of this DataStore.
  *
  * @see DataStoreManager
  */

 public int getUseCount() {

  return useCount;

 }



 /**
  * Returns the creation date of the DataStore.
  *
  * @return the creation date.
  */

 public LocalDateTime getCreation() {

  return creation;

 }



 /**
  * Get the data of the last use of this DataStore.
  *
  * @return The date of the last use.
  */

 public LocalDateTime getLastUse() {

  return lastUse;

 }



 /**
  * Returns the category of this DataStore.
  *
  * @return The category-
  */

 public String getCategory() {

  return maker.getCategory();

 }



 /**
  * Returns the query string.
  *
  * @return the query string.
  */

 public String getQueryString() {

  return maker.getQueryString();

 }



 /**
  * Decrements the life counter. Called by the DataStoreManager.
  *
  * @see DataStoreManager
  */

 public void decrementLifeCounter() {

  lifeCounter --;

 }



 /**
  * Returns the values of this DataStore.
  *
  * @return The values.
  */

 public Object[] getValues() {

  return maker.getValues();

 }



 /**
  * Called by the DataStoreManager to signal the DataStore.
  *
  * @param sig The signal.
  * @param subjectBean A Bean which has something to do with the signal. May be null.
  *
  * @see DataStoreManager
  */

 public void signal(DataStoreSignal sig, PersistentBean subjectBean) {

  BiFunction<DataStoreManager.IDataStoreMaker, Bean, Boolean> resetCriteria =
   maker.getResetCriteria();

  if (resetCriteria != null  &&  resetCriteria.apply(maker, subjectBean))
   reset();

 }



 /**
  * Resets the DataStore.
  */

 public synchronized void reset() {

  stamp = 0;

  result = null;
  resultBean = null;
  resultCollection = null;

 }



 /**
  * Tell the DataStoreManager this DataStore is busy.
  *
  * <p>The DataStore busy during data access.</p>
  *
  * @return true if an operation fetching data from a source is running. False otherwise.
  */

 public boolean isBusy() {

  return busy;

 }



 /**
  * Returns the hash code of the DataStore.
  *
  * <p>Normally a DataStores hash code is the hash code of its content.
  * If this behaviour isn't suitable it can be altered by using makeHashCode
  * function of the DataStoreMaker.</p>
  *
  * @see DataStoreManager.DataStoreMaker#makeHashCode(Function)
  *
  * @return The hash code.
  */

 public int hashCode() {

  return
   maker.getMakeHashCode().apply(this);

 }



 /**
  * Return the stamp of the DataStore.
  *
  * <p>The stamp has to change each time the DataStore is reset. So callers can detect changes to the data
  * inside the DataStore.</p>
  *
  * <p>Because it depends on the implementation of the DataStore and if signal() calls reset(). Use this
  * functionality with care!</p>
  *
  * @return The current value of the stamp.
  */

 public Long getStamp() {

  if (stamp == 0)
   stamp = hashCode();

  return stamp;

 }



 /**
  * Counts the number of Beans.
  *
  * @return The number of Beans or records.
  */

 public Long count() {

  AtomicLong count = new AtomicLong();

  forEach(b -> {count.incrementAndGet();});

  return count.longValue();

 }



 /**
  * Returns if either result, resultBean or resultCollection aren't null.
  *
  * @return DataStore has a result.
  */

 public boolean isRead() {

  return result != null  ||  resultBean != null  ||  resultCollection != null;

 }



 /**
  * Returns if the DataStores collect()-method was used.
  *
  * @return DataStore has a resultCollection.
  */

 public boolean isCollected() {

  return resultCollection != null;

 }



 /**
  * Returns if the DataStores readBean()-method was used.
  *
  * @return DataStore has a resultBean.
  */

 public boolean isReadBean() {

  return resultBean != null;

 }

}
