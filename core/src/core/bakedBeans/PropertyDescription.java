package core.bakedBeans;

import java.lang.annotation.*;



/**
 * This annotation describes a property of a bean.
 *
 * <p>During the first access to the structure of a bean all PropertyDescription will be "converted" to PropertyInfo.</p>
 *
 * @version 2023-04-18
 * @author pilgrim.lutz@imail.de
 *
 * @see PropertyInfo
 * @see TypedBean
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface PropertyDescription {

 String name() default "";
 String prefix() default "";
 String base() default "";
 String alias() default "";
 Class<? extends Object> pclass();
 Class<? extends Object> iclass() default Object.class;
 String setter() default "stdSetter";
 String getter() default "stdGetter";
 RelationType relationType() default RelationType.none;
 String dataStoreName() default "";
 String query() default "";
 String category() default "";
 int maxLength() default 0;
 boolean dontBakeAliasSetter() default false;  // Only for annotation processor "BeanBaker".
 boolean dontBakeAliasGetter() default false;  // Only for annotation processor "BeanBaker".
 boolean bakeNameSetter() default false;  // Only for annotation processor "BeanBaker".
 boolean bakeNameGetter() default false;  // Only for annotation processor "BeanBaker".
 String iclassPrefix() default "";  // Used during class loading in PropertyInfo.
 String pclassPrefix() default "";  // Used during class loading in PropertyInfo.
 int bitMask() default 0; // Used for bit operations.
 SerializationMode serialize() default SerializationMode.ifSet; // Used for serialization.
 String[] loadWith() default {};
 String[] orderBy() default {};
 boolean load() default true;  // Include in load query string.
 PropertyFunction propertyFunction() default PropertyFunction.none;
}
