package core.bakedBeans;



import core.exceptions.BeanException;
import core.exceptions.BeanStructureException;
import core.util.Utilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;



/**
 * Instances of this class provide information about a property in a bean.
 *
 * @version 2023-08-20
 * @author pilgrim.lutz@imail.de
 */

public class PropertyInfo {

 private String name;
 private String prefix;
 private String base;
 private String alias;
 private Class<? extends Object> pclass;

 private Class<? extends Object> iclass;
 private Method setterMethod;
 private Method getterMethod;
 private RelationType relationType;
 private String dataStoreName;
 private String query;
 private String category;
 private int maxLength;
 private int bitMask;
 private SerializationMode serialize;
 private String[] loadWith;
 private String[] orderBy;
 private Class<? extends TypedBean> forClass;
 private Boolean load;
 private PropertyFunction propertyFunction;

 private static Logger logger = LogManager.getLogger(PropertyInfo.class);



 /**
  * Constructor for building a PropertyInfo.
  *
  * @param forClass The bean this PropertyInfo is valid for.
  * @param name The name of the property.
  * @param prefix The prefix.
  * @param base The base.
  * @param alias The alias.
  * @param pclass The class of the property (data type).
  * @param iclass The inner class for collection type properties.
  * @param getter The name of the level 0 getter.
  * @param setter The name of the level 0 setter.
  * @param relationType The relation type.
  * @param dataStoreName The name of the DataStore.
  * @param query The query string for the above DataStore.
  * @param category The category for the above DataStore.
  * @param maxLength The maximum length for the value of the property.
  * @param bitMask A bitmask for bit operations.
  * @param serialize The serialization mode
  * @param loadWith Properties to load.
  * @param load Include in load query string.
  * @param propertyFunction Surrounding function for the property.
  *
  * @see DataStore
  */

 public PropertyInfo(Class<? extends TypedBean> forClass, String name, String prefix, String base, String alias, Class<? extends Object> pclass, Class<? extends Object> iclass, String getter, String setter, RelationType relationType, String dataStoreName, String query, String category, int maxLength, int bitMask, SerializationMode serialize,
                     String[] loadWith, String[] orderBy, Boolean load, PropertyFunction propertyFunction) {

  this.forClass = forClass;

  this.name = name;
  this.prefix = prefix;
  this.base = base;
  this.alias = alias;
  if (this.alias == null  ||  "".equals(this.alias))
   this.alias = name;
  this.pclass = pclass;
  this.iclass = iclass;

  getterMethod = getMethod(forClass, getter, TypedBeanGetter.class);
  if (!Utilities.isEmpty(setter))  // setter is optional
   setterMethod = getMethod(forClass, setter,  TypedBeanSetter.class);

  this.relationType = relationType;
  this.dataStoreName = dataStoreName;
  this.query = query;
  this.category = category;
  this.maxLength = maxLength;
  this.bitMask = bitMask;
  this.serialize = serialize;

  this.loadWith = loadWith;
  this.orderBy = orderBy;

  this.load = load;

  this.propertyFunction = propertyFunction;

 }



 /**
  * Standard constructor.
  */

 public PropertyInfo() {

 }



 private Method getMethod(Class<? extends TypedBean> forClass, String name, Class<? extends Annotation> methodType) {

  Class<?> cl = forClass;

  while (cl != null) {
   for (Method m : cl.getDeclaredMethods())
    if (name.equals(m.getName()))
     if (m.getAnnotation(methodType) != null)
      return m;
     else
      logger.debug ("Method \"" + m.getName() + "\" found in \"" + cl.getName() + "\". But has no annotation \"" + methodType.toString() + "\"!");

   cl = cl.getSuperclass();
  }

  throw
   new BeanStructureException("Can't find " + name + " for class " + forClass.getName());

 }



 /**
  * A convenient constructor.
  *
  * @param forClass The bean this PropertyInfo is valid for.
  * @param name The name of the property.
  * @param prefix The prefix.
  * @param alias The alias.
  * @param pclass The class of the property (data type).
  * @param maxLength The maximum length for the value of the property.
  */

 public PropertyInfo(Class<?extends TypedBean> forClass, String name, String prefix, String alias, Class<? extends Object> pclass, int maxLength) {

  this(
   forClass,
   name,
   prefix,
   null,
   alias,
   pclass,
   Object.class,
   "stdGetter",
   "stdSetter",
   RelationType.none,
   null,
   null,
   null,
   maxLength,
   0,
   SerializationMode.ifSet,
   null,
   null,
   Boolean.TRUE,
   PropertyFunction.none
  );

 }



 /**
  * A constructor to build a PropertyInfo from a ProperyDescription.
  *
  * @param forClass The bean this PropertyInfo is valid for.
  * @param descr The PropertyDescription.
  */

 public PropertyInfo(Class<?extends TypedBean> forClass, PropertyDescription descr) {

  this(
   forClass,
   descr.name(),
   descr.prefix(),
   descr.base(),
   descr.alias(),
   getPrefixedClass(descr.pclass(), descr.pclassPrefix()),
   getPrefixedClass(descr.iclass(), descr.iclassPrefix()),
   descr.getter(),
   descr.setter(),
   descr.relationType(),
   descr.dataStoreName(),
   descr.query(),
   descr.category(),
   descr.maxLength(),
   descr.bitMask(),
   descr.serialize(),
   descr.loadWith(),
   descr.orderBy(),
   descr.load(),
   descr.propertyFunction()
  );

 }



 /**
  * A copy constructor.
  *
  * @param pi A PropertyInfo as source.
  */

 public PropertyInfo(PropertyInfo pi) {

  this();

  this.forClass = pi.getForClass();
  this.name = pi.getName();
  this.prefix = pi.getPrefix();
  this.base =  pi.getBase();
  this.alias = pi.getAlias();
  this.pclass = pi.getPclass();
  this.iclass = pi.getIclass();
  this.getterMethod = pi.getGetterMethod();
  this.setterMethod = pi.getSetterMethod();
  this.relationType = pi.getRelationType();
  this.dataStoreName = pi.getDataStoreName();
  this.query = pi.getQuery();
  this.category = pi.getCategory();
  this.maxLength = pi.getMaxLength();
  this.bitMask = pi.getBitMask();
  this.serialize = pi.getSerialize();
  this.loadWith = pi.getLoadWith();
  this.orderBy = pi.getOrderBy();
  this.load = pi.getLoad();
  this.propertyFunction = pi.getPropertyFuntion();

 }



 private static Class<? extends Object> getPrefixedClass(final Class<? extends Object> cl, final String prefix) {

  Class<? extends Object> retClass = cl;

  if (!Utilities.isEmpty(prefix))
   try {
    retClass = Class.forName(prefix + "." + cl.getName());
   }
   catch (ClassNotFoundException e) {
    throw new BeanException(e);
   }

  return retClass;

 }



 /**
  * Returns the bean class this property is valid for.
  *
  * @return The name.
  */

 public Class<?extends TypedBean> getForClass() {

  return forClass;

 }



 /**
  * Returns the name of the property.
  *
  * @return The name.
  */

 public String getName() {

  return name;

 }



 /**
  * Returns the prefix of the property.
  *
  * @return The prefix.
  */

 public String getPrefix() {

  return prefix;

 }



 /**
  * Returns the base of the property.
  *
  * @return The base.
  */

 public String getBase() {

  return base;

 }



 /**
  * Returns the alias of the property.
  *
  * <p>If no alias is specified this method returns the name of the property.</p>
  *
  * @return The alias (or the name).
  */

 public String getAlias() {

  return Utilities.isEmpty(alias) ? getName() : alias;

 }



 /**
  * Returns the class of the property.
  *
  * @return The class.
  */

 public Class<? extends Object> getPclass() {

  return pclass;

 }



 /**
  * Returns the class of the properties as a Bean class.
  *
  * <p>This is a convenience method. Make sure you defined the inner class as a Bean!</p>
  *
  * @return The Bean class.
  */

 public Class<? extends Bean> getPclassAsBean() {

  @SuppressWarnings("unchecked")
  Class<? extends Bean> pclass = (Class<? extends Bean>) this.pclass;

  return pclass;

 }



 /**
  * Sets the class of the propery.
  *
  * @param pclass The class.
  */

 public void setPclass(Class<? extends Object> pclass) {

  this.pclass = pclass;

 }



 /**
  * Sets the iclass.
  *
  * @param iclass The inner class.
  */

 public void setIclass(Class<? extends Object> iclass) {

  this.iclass = iclass;

 }



 /**
  * Set the name.
  *
  * @param name The name of the property.
  */

 public void setName(String name) {

  this.name = name;

 }



 /**
  * Set the base.
  *
  * @param base The base.
  */

 public void setBase(String base) {

  this.base = base;

 }



 /**
  * Returns the level 0 getter of the property.
  *
  * @return The getter.
  */

 public Method getGetterMethod() {

  return getterMethod;

 }



 /**
  * Returns the level 0 setter of the property.
  *
  * @return The setter.
  */

 public Method getSetterMethod() {

  return setterMethod;

 }



 /**
  * Returns the relation type of the property.
  *
  * @return The relation type.
  */

 public RelationType getRelationType() {

  return relationType;

 }



 /**
  * Returns the name of the DataStore.
  *
  * @return The name.
  *
  * @see DataStore
  */

 public String getDataStoreName() {

  return dataStoreName;

 }



 /**
  * Returns the query string for the DataStore.
  *
  * @return The query string.
  *
  * @see DataStore
  */

 public String getQuery() {

  return query;

 }



 /**
  * Returns the query string for the DataStore.
  */

 public void setQuery(String query) {

  this.query = query;

 }



 /**
  * Returns the category of the DataStore.
  *
  * @return The category.
  *
  * @see DataStore
  */

 public String getCategory() {

  return category;

 }



 /**
  * Returns the maximum length for the value of the property.
  *
  * @return The maximum length.
  */

 public int getMaxLength() {

  return maxLength;

 }



 /**
  * Returns the inner class for collection type properties.
  *
  * @return The class.
  */

 public Class<? extends Object> getIclass() {

  return iclass;

 }



 /**
  * Returns the inner class for collection type properties as a Bean class.
  *
  * <p>This is a convenience method. Make sure you defined the inner class as a Bean!</p>
  *
  * @return The Bean class.
  */

 public Class<? extends Bean> getIclassAsBean() {

  @SuppressWarnings("unchecked")
  Class<? extends Bean> iclass = (Class<? extends Bean>) this.iclass;

  return iclass;

 }



 /**
  * Return the bitmask.
  *
  * @return The bitmask.
  */

 public int getBitMask() {

  return bitMask;

 }



 /**
  * Returns the serialization mode.
  *
  * @return Teh serialization mode.
  */

 public SerializationMode getSerialize() {

  return serialize;

 }



 /**
  * Returns the properties to load.
  *
  * @return An array with property aliases.
  */

 public String[] getLoadWith() {

  return loadWith;

 }



 /**
  * Returns the order for a collection.
  *
  * @return An array with property aliases.
  */

 public String[] getOrderBy() {

  return orderBy;

 }



 /**
  * Returns the load flag.
  *
  * @return true if this property should be included in the load query string.
  */

 public Boolean getLoad() {

  return load;

 }



 /**
  * Returns the property function
  *
  * @return true if this property should be included in the load query string.
  */

 public PropertyFunction getPropertyFuntion() {

  return propertyFunction;

 }

}
