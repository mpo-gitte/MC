package core.bakedBeans;


import core.exceptions.BeanStructureException;

import java.util.Map;



/**
 * A special PropertyList for Beans.
 *
 * @param <T> Type.
 *
 * @version 2023-08-20
 * @author pilgrim.lutz@imail.de
 */

public class BeanList<T extends Bean> extends PropertyList<T> {



 /**
  * The Constructor.
  *
  * @param propertyClass A bean class.
  */

 public BeanList(Class<T> propertyClass) {

  super(propertyClass);

 }



 /**
  * The Constructor.
  *
  * @param propertyClass A bean class.
  * @param beanMap A bean map to fill the list.
  *
  * @throws BeanStructureException If problems with the structure of the bean occur.
  */

 public BeanList(Class<T> propertyClass, Map<Object, ? extends Bean> beanMap) throws BeanStructureException {

  super(propertyClass);
  this.fill(beanMap.values());

 }



 @Override
 protected T makeItem() {

  return BeanFactory.make(propertyClass);

 }



 /**
  * Checks if one of the beans in this list has the given bean as a sub bean.
  *
  * @param alias The alias for the sub bean.
  * @param bean The bean to search for.
  *
  * @return true if bean is found. False otherwise.
  */

 public boolean containsAsSubBean(String alias, Bean bean) {

  for (Bean b : this)
   if (b.get(alias).equals(bean))
    return true;

  return false;

 }



 /**
  * Puts an item to the position set by seek()
  *
  * @param item
  */

 @Override
 public void put(T item) {

  if (seekPos >= this.size()  ||  Utilities.isEmpty(this.get(seekPos)))
   super.put(item);
  else
   ++ seekPos;

 }

}
