package core.bakedBeans;

import java.lang.annotation.*;



@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface TypedBeanDescription {

 PropertyDescription[] properties();

 boolean bakeBean() default false;  // Only for annotation procesor "BeanBaker".

}
