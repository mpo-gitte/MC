package core.bakedBeans;

import java.lang.annotation.*;



/**
 * Marks a getter for TypedBean.
 *
 * <p>A so called "Level-0-setter-method" has to be tagged with this annotation.</p>
 *
 * <p>Otherwise a TypedBean does not accept the method as a setter.</p>
 *
 * <p>There is an annotation processor which checks the method signature during compilation!</p>
 *
 * @version 2016-03-14
 * @author pilgrim.lutz@imail.de
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface TypedBeanSetter {

}
