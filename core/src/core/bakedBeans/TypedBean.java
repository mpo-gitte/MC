package core.bakedBeans;



import core.exceptions.BeanException;
import core.exceptions.BeanStructureException;
import core.util.Aspectator;
import core.util.Utilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;



/**
 * This class extends Bean with a method for setting attributes from a String.
 *
 * <p>The concrete class has to know the conversion from String to the type of the attribute.</p>
 *
 * <p>This class is used by the BeanFactory.</p>
 *
 * @see BeanFactory
 *
 * @version 2024-12-20
 * @author lp
 *
 */

public abstract class TypedBean extends Bean {

 private static final HashMap<Class<?>, HashMap<String, PropertyInfo>> beanDescriptionsByAlias
  = new HashMap<>();
 private static final HashMap<Class<?>, HashMap<String, PropertyInfo>> beanDescriptionsByName
  = new HashMap<>();
 private static final Logger logger = LogManager.getLogger(TypedBean.class.getName());



 /**
  * Getter for enumerated values.
  *
  * @param pi The PropertyInfo.
  *
  * @return The related data with type defined in PropertyInfo.pclass.
  */

 @TypedBeanGetter
 protected Object enumGetter(PropertyInfo pi) {

  Integer id = (Integer)get(pi.getBase());

  if (id == null)
   return null;

  return
   pi.getPclass().getEnumConstants()[id];

 }



 /**
  * Setter for enumerated values.
  *
  * @param pi The PropertyInfo.
  */

 @TypedBeanSetter
 protected void enumSetter(PropertyInfo pi, Object value) {

  int id = ((Enum<?>)value).ordinal();

  set(pi.getBase(), id);

 }



 /**
  * Sets a property from a String.
  *
  * <p>Because a TypedBean should know about the data types of its properties it should be able to perform
  * the data conversion.</p>
  *
  * @param key The alias of the property.
  * @param value The value for the property.
  */

 public void setFromString(String key, String value)  {

  set(key, Utilities.makeGeneralValue(value, getPropertyClass(key)));

 }



 /**
  * Sets a property from a String.
  *
  * <p>Because a TypedBean should know about the data types of its properties it should be able to perform
  * the data conversion.</p>
  *
  * @param key The name of the property.
  * @param value The value for the property.
  */

 public void setFromStringByName(String key, String value) {

  Class<?> pcn = getPropertyClassByName(key);

  if (pcn == null)
   return;  // If property not found be a tolerant reader.

  setByName(key, Utilities.makeGeneralValue(value, pcn));

 }



 /**
  * Returns the "data type" of the given property.
  *
  * <p>Simple and silly beans (almost all :-) don't override this method.</p>
  *
  * @param key The name of the property.
  *
  * @return The class of the property. null means: "I don't know anything about my structure!"
  */

 public Class<?> getPropertyClass(String key) {

  PropertyInfo pdi = getPropertyInfo(key);

  if (pdi != null)
   return pdi.getPclass();

  return null;

 }



 /**
  * Returns the "data type" of the given property.
  *
  *  <p>Simple and silly beans (almost all :-) don't override this method.</p>
  *
  * @param key The name of the property.
  *
  * @return The class of the property. null means: "I don't know anything about my structure!"
  */

 public Class<?> getPropertyClassByName(Object key) {

  PropertyInfo pdi = getPropertyInfoByName(key);

  if (pdi != null)
   return pdi.getPclass();

  return null;

 }



 /**
  * Returns information about the given property.
  *
  * @param key The name or alias of the property.
  *
  * @return PropertyInfo
  */

 public PropertyInfo getPropertyInfo(String key) {

  return getPropertyInfo(this.getClass(), key);

 }



 /**
  * Returns information about the given property.
  *
  * @param beanClass The class of the bean.
  * @param propertyAlias The name or alias of the property.
  *
  * @return PropertyInfo
  */

 public static PropertyInfo getPropertyInfo(Class<? extends TypedBean> beanClass, String propertyAlias) {

  return getPropertyInfos(beanClass).get(propertyAlias);

 }



 /**
  * Returns information about the given property.
  *
  * <p>The property is identified by a path: BeanClassName:property[:property]</p>
  *
  * @param beanClass The class of the bean.
  * @param propertyAliasPath The path of alias to the property.
  *
  * @return PropertyInfo
  *
  */

 public static PropertyInfo getPropertyInfoByPath(Class<? extends TypedBean> beanClass, String propertyAliasPath) {

  String[] path = propertyAliasPath.split(":");

  Class<? extends TypedBean> beanClassCurrent = beanClass;
  PropertyInfo pi;

  for (int id = 0; id < path.length; id++) {
   String alias = path[id];

   if (id == (path.length - 1)) {
    // End of the path: Must be a normal property!
    pi = getPropertyInfo(beanClassCurrent, alias);
    if (!Utilities.isEmpty(pi)  &&
     pi.getRelationType() == RelationType.none  &&
     !Utilities.isClassDerivedFrom(pi.getPclass(), PersistentBean.class))
     return pi;
   }
   else if (id == 0) {
    // Start of the path: Has to be a bean class.
    @SuppressWarnings("unchecked")
    Class<? extends TypedBean> clazz = (Class<? extends TypedBean>)BeanFactory.loadBeanClass(alias);
    beanClassCurrent = clazz;
   }
   else {
    // On the path: A sub bean property which provides a bean class.
    pi = getPropertyInfo(beanClassCurrent, alias);
    if (!Utilities.isEmpty(pi)  &&
     pi.getRelationType() != RelationType.none  &&
     Utilities.isClassDerivedFrom(pi.getPclass(), PersistentBean.class) &&
     Utilities.isEmpty(pi.getDataStoreName())) {
     @SuppressWarnings("unchecked")
     Class<? extends TypedBean> clazz = (Class<? extends TypedBean>) pi.getPclass();
     beanClassCurrent = clazz;
    }
   }

  }

  throw
   new BeanStructureException("Property with path \"" + propertyAliasPath + "\" not found!");

 }



 /**
  * Returns information about the given property.
  *
  * @param propertyName The name of the property.
  *
  * @return PropertyInfo
  */

 public PropertyInfo getPropertyInfoByName(Object propertyName) {

  return
   getPropertyInfosByName().get((String)propertyName);

 }



 /**
  * Dumps this bean to a String in a format which can be parsed by fromString().
  *
  * <p>This method returns null. See implementation for details.</p>
  *
  * @return The String with the textual representation of the beans properties.
  */

 public String toString() {

  return super.toString();

 }



 /**
  * Populates the bean with data from the given String.
  *
  * <p>This String has to be in the format produced by toString().</p>
  *
  * <p>This method does nothing. See implementation for details.
  *
  * @param str The String with the beans properties.
  */

 public void fromString(String str) {

  BeanFactory.loadBeanFromString(str, this);

 }



 /**
  * Returns the map with PropertyInfos mapped by alias.
  *
  * @return The map of PropertyInfos
  *
  * @see PropertyInfo
  */

 public HashMap<String, PropertyInfo> getPropertyInfos() {

  return getPropertyInfos(this.getClass());

 }



 /**
  * Returns the map with PropertyInfos mapped by alias.
  *
  * @param beanClass The "real" class to inspect.
  * @return The map of PropertyInfos.
  *
  * @see PropertyInfo
  */

 static public HashMap<String, PropertyInfo> getPropertyInfos(Class<? extends TypedBean> beanClass) {

  synchronized(beanClass) {
   HashMap<String, PropertyInfo> propsByAlias = beanDescriptionsByAlias.get(beanClass);

   if (propsByAlias == null) {
    propsByAlias = new HashMap<>();

    Class<?> cl = beanClass;

    while (cl != null) {
     TypedBeanDescription descr = cl.getAnnotation(TypedBeanDescription.class);

     if (descr != null) {
      for (PropertyDescription pd : descr.properties()) {
       PropertyInfo pi = new PropertyInfo(beanClass, pd);
       logger.debug(() -> "Bean '" + beanClass.getName() + "' has property '" + pi.getName() + "' alias '" + pi.getAlias() + "'");
       if (propsByAlias.containsKey(pi.getAlias()))  // Do not overwrite already existing properties. This is for overwriting properties.
        logger.debug(() -> "Property '" + pi.getName() + "' alias '" + pi.getAlias() + "' already defined.");
       else
        propsByAlias.put(pi.getAlias(), pi);
      }
     }

     cl = cl.getSuperclass();  // Search in the whole class hierarchy.
    }

    beanDescriptionsByAlias.put(beanClass, propsByAlias);
   }

   return propsByAlias;

  }

 }



 /**
  * Returns the map with PropertyInfos mapped by name.
  *
  * @return The map of PropertyInfos
  *
  * @see PropertyInfo
  */

 public HashMap<String, PropertyInfo> getPropertyInfosByName() {

  return getPropertyInfosByName(this.getClass());

 }



 /**
  * Returns the map with PropertyInfos mapped by name.
  *
  * @param beanClass The class of the bean to inspect.
  * @return The map of PropertyInfos.
  *
  * @see PropertyInfo
  */

 public static HashMap<String, PropertyInfo> getPropertyInfosByName(Class<? extends TypedBean> beanClass) {

  synchronized(beanClass) {
   HashMap<String, PropertyInfo> propsByName = beanDescriptionsByName.get(beanClass);

   if (propsByName == null) {
    propsByName = new HashMap<>();

    Class<?> cl = beanClass;

    while (cl != null) {
     TypedBeanDescription descr = cl.getAnnotation(TypedBeanDescription.class);

     if (descr != null) {
      for (PropertyDescription pd : descr.properties()) {
       PropertyInfo pi = new PropertyInfo(beanClass, pd);
       String name = pi.getName();
       if (!Utilities.isEmpty(name))
        if (!propsByName.containsKey(pi.getAlias()))  // Do not overwrite already existing properties. This is for overwriting properties.
         propsByName.put(name, pi);
      }
     }

     cl = cl.getSuperclass();  // Search in the whole class hierarchy.
    }

    beanDescriptionsByName.put(beanClass, propsByName);
   }

   return propsByName;

  }

 }



 /**
  * A hook to add PropertyInfos "manually".
  *
  * <p>Not used often. Because the structure of a bean should be declared by PropertyDescriptions. Empty here.</p>
  *
  * @param propsByName The map for the PropertyInfos.
  */

 protected void addPropertyInfosByName(HashMap<String, PropertyInfo> propsByName) {

 }



 /**
  * Sets a property by alias.
  *
  * @param alias The name of the property.
  * @param value The value for the property.
  */

 public TypedBean set(String alias, Object value) {

  return
   set(getStrictPropertyInfoByAlias(alias), alias, value);

 }



 /**
  * Sets a property by name.
  *
  * <p>Mainly used by persistance layers.</p>
  *
  * @param name The name of the property.
  * @param value The value for the property.
  */

 public void setByName(String name, Object value) {

  set(getStrictPropertyInfoByName(name), name, value);

 }



 private TypedBean set(PropertyInfo pi, String key, Object value) {

  try {
   checkDatatype(pi, value);

//   pi.getSetterMethod().invoke(this, pi, value);
   Method method = pi.getSetterMethod();
   method.setAccessible(true);

   Aspectator.get().run(this, method, method, new Object[] {pi, value});
  }
  catch (Exception e) {
   logger
    .error(() ->
     this.getClass().getName() +
     ".set(): Can't invoke setter for property: \"" +
     key +
     "\" value: \"" + value.toString() + "\"" +
     " setter: \"" +  pi.getSetterMethod() + "\"" +
     " error: " + Utilities.getRealProblem(e)
    );
   throw
    new BeanException(e);
  }

  return
   this;

 }



 /**
  * Checks whether the given value has the correct data type for the property
  * of the bean.
  *
  * @param pi The PropertyInfo meant.
  * @param value The value to check
  */

 protected void checkDatatype(PropertyInfo pi, Object value) {

  if (value != null)
   if (value.getClass() != pi.getPclass()) {
    // ToDo: Find a way to check containing data type for PropertyLists, too.

    String errmess = "Property with name \"" + pi.getName() + "\" in bean \"" + this.getClass().getName() +  "\" given value has incorrect data type: " + value.getClass().getName() + ". Bean needs: " + pi.getPclass().getName();

    logger.error (errmess);

    throw new BeanStructureException(errmess);
   }

 }



 /**
  * Gets the property with the given alias.
  *
  * @param alias The name of the property.
  * @return The value of the property.
  */

 public Object get(String alias) {

  PropertyInfo pi = getStrictPropertyInfoByAlias(alias);

  try {
   Method method = pi.getGetterMethod();
   method.setAccessible(true);

   return
    Aspectator.get().run(this, method, method, new Object[] {pi});
  }
  catch (Exception e) {
   logger.error(() ->
    "TypedBean.get(): Bean: \"" + this.getClass().getName() +
     "\" alias: \"" + alias +
     "\" getter method: \"" + pi.getGetterMethod() + "\""
   );
   logger.error(core.bakedBeans.Utilities.getRealProblem(e));
   throw new BeanException(e);
  }

 }



 /**
  * Gets the property with the given alias.
  *
  * @param key The name of the property.
  * @return The value of the property or null if the property doesn't exist.
  */

 protected Object getSafe(String key) {

  try {
   return get(key);
  }
  catch (Exception e) {
   logger.error(() ->
    "getSave(): Can't get property \"" + key + "\" for bean \"" + this.getClass().getName() + "\"!"
   );
  }

  return null;

 }



 /**
  * Gets the property with the given name.
  *
  * @param name The name of the property.
  *
  * @return The value of the property.
  */

 public Object getByName(String name) {

  PropertyInfo pi = getStrictPropertyInfoByName(name);

  try {
   Method method = pi.getGetterMethod();
   method.setAccessible(true);

   return
    Aspectator.get().run(this, method, method, new Object[] {pi});
  }
  catch (Exception e) {
   logger.error(() ->
    "TypedBean.getByName(): Bean: \"" + this.getClass().getName() +
    "\" name: \"" + name +
    "\" getter method: \"" + pi.getGetterMethod() + "\""
   );
   logger.error(core.bakedBeans.Utilities.getRealProblem(e));
   throw new BeanException(e);
  }

 }



 private PropertyInfo getStrictPropertyInfoByAlias(String key) {

  PropertyInfo pi = getPropertyInfo(key);

  if (pi == null) {
   String errmess = "Property with alias \"" + key + "\" in bean \"" + this.getClass().getName() + "\" not found!";

   logger.error (errmess);

   throw new BeanStructureException(errmess);
  }
  return pi;
 }



 /**
  * Gets a PropertyInfo by name.
  *
  * @param key The name of the property.
  *
  * @return The PropertyInfo for the property.
  *
  */

 protected PropertyInfo getStrictPropertyInfoByName(String key) {

  PropertyInfo pi = getPropertyInfoByName(key);

  if (pi == null) {
   String errmess = "Property with name \"" + key + "\" in bean \"" + this.getClass().getName() + "\" not found!";

   logger.error (errmess);

   throw
    new BeanStructureException(errmess);
  }
  return pi;
 }



 /**
  * The standard setter.
  *
  * @param pi The PropertyInfo
  * @param value The value of the propery.
  */

 @TypedBeanSetter
 protected void stdSetter(PropertyInfo pi, Object value) {

  super.set(pi.getName(), value);  // Normal bean values are always managed by name.

 }



 /**
  * The standard getter.
  *
  * @param pi The PropertyInfo
  *
  * @return The value of the property.
  */

 @TypedBeanGetter
 protected Object stdGetter(PropertyInfo pi) {

  return super.get(pi.getName());  // Normal bean values are always managed by name.

 }



 /**
  * BitMask getter. Gets a bit from an Integer.
  *
  * <p>The bit is given by "bitMask" in the PropertyInfo.</p>
  *
  * @param pi The PropertyInfo
  *
  * @return The value of the property.
  */

 @TypedBeanGetter
 protected Object bitMaskGetter(PropertyInfo pi) {

  Integer bv = (Integer)get(pi.getBase());  // Base is managed by alias!
  int baseVal = 0;

  if (bv != null)
   baseVal = bv;

  int bitMask = pi.getBitMask();

  return (baseVal & bitMask) > 0;

 }



 /**
  * Sets a bit in an Integer.
  *
  * <p>The bit is given by "bitMask" in the PropertyInfo.</p>
  *
  * @param pi The PropertyInfo
  */

 @TypedBeanSetter
 protected void bitMaskSetter(PropertyInfo pi, Object val) {

  Integer bv = (Integer)get(pi.getBase());  // Base is managed by alias!
  int baseVal = 0;

  if (bv != null)
   baseVal = bv;

  int bitMask = pi.getBitMask();

  if ((Boolean)val)
   baseVal = baseVal | bitMask;
  else
   baseVal = baseVal & ~bitMask;

  set(pi.getBase(), baseVal);

 }



 /**
  * Get the class.
  *
  * <p>Used for signature. Can be overridden by baked Beans to return the same class as the defined Bean.</p>
  *
  * @return The class of the bean.
  *
  */

 public Class<? extends TypedBean> getClassAlias() {

  return getClass();

 }



 /**
  * Fill the bean from the given bean.
  *
  * <p>Only "normal" propterties will be copied!</p>
  *
  * <p>Normal properties are properties with a name and no relation.</p>
  *
  * @param src The source bean
  */

 public void setFrom(TypedBean src) {

  Set<String> pnames = src.getPropertyInfos().keySet();

  for (String pname: pnames) {
   PropertyInfo pi = src.getPropertyInfo(pname);
   if (!Utilities.isEmpty(pi.getName())  &&  pi.getRelationType() == RelationType.none)  // Copy only "real" values.
    this.set(pname, src.get(pname));
  }

 }



 /**
  * Return the internal Map for serialization.
  *
  * <p>Because the values are for external purposes. This bean returns a map with the alias of the
  * bean as the key./p>
  *
  * @return The map.
  */

 @Override
 public Map<String, Object> getPropertiesForSerialization() {

  return
   getPropertyInfos().values().stream()
    .filter(pi -> pi.getSerialize() != SerializationMode.never &&
                  !core.bakedBeans.Utilities.isEmpty(getSafe(pi.getAlias()))
    )
    .collect(Collectors.toMap(PropertyInfo::getAlias, pi -> getSafe(pi.getAlias())));

 }

}
