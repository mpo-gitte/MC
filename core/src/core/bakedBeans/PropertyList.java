package core.bakedBeans;



import core.exceptions.BeanStructureException;
import core.exceptions.MCException;
import core.util.FillingArrayList;
import core.util.Utilities;

import java.util.ArrayList;
import java.util.Collection;



/**
 * The PropertyList holds the data for list type properties.
 *
 * <p>All list items have the same type which is known to the list.</p>
 *
 * <p>Access to a non existing item leads to instantiation of this item.</p>
 * 
 * @version 2023-09-13
 * @author pilgrim.lutz@imail.de
 *
 */

public class PropertyList<T> extends FillingArrayList<T> {

 Class<T> propertyClass;


 
 /**
  * Standard constructor.
  * 
  * @param propertyClass The class of the list items. Those items can be "normal" Java objects or beans.
  */

 public PropertyList(Class<T> propertyClass) {

  super();
  
  this.propertyClass = propertyClass;

 }
 


 /**
  * Gets the property with the given index.
  * <p>If the property is not existing it will be created. Note: The item is empty!</p>
  * 
  * @param ix The index
  * @return The Item
  *
  */
 
 public T getProperty(int ix) {

  T o;

  if (ix >= size()) {
   o = makeItem();

   setFilled(ix, o);
  }
  else {
   o = super.get(ix);
   if (o == null) {
    o = makeItem();
    set(ix, o);
   }
  }

  return o;

 }



 /**
  * Makes a new item for the list.
  *
  * @return A new instance from the class for the list.
  */

 protected T makeItem() {

  T o;
  try {
   o = propertyClass.getConstructor().newInstance();
  }
  catch (Exception e) {
   throw new MCException(e);
  }

  return o;

 }



 /**
  * Dumps the list to a String.
  *
  * <p>Opposite to fromString()</p>
  *
  * @return The String.
  *
  * @see PropertyList#fromString(String)
  */

 public String toString() {

  StringBuilder sb = new StringBuilder();
  
  for (int id = 0; id < size(); id ++) {
   T o = get(id);
   if (o != null) {
    String sd = o.toString();
    if (sd.length() > 0)
     sb.append(id).append("=\"").append(Utilities.normalQuote(sd)).append("\";");
   }
  }

  return sb.length() ==  0 ? null : sb.toString();

 }


 /**
  * Fills the list from a String.
  *
  * <p>Opposite to toString()</p>
  *
  * @param srcString The String.
  *
  * @see PropertyList#toString()
  */

 public void fromString(String srcString) {

  ArrayList<String> beanStrings = Utilities.split(srcString);

  for (String bs : beanStrings) {
   ArrayList<String>sb = Utilities.split(bs, '=');
   if (sb.size() == 2) {
    TypedBean b = (TypedBean)this.getProperty(Integer.parseInt(sb.get(0)));
    b.fromString(Utilities.naked(sb.get(1)));
   }
  }
  
 }



 /**
  * Adds all Beans from the given list to this PropertyList.
  *
  * @param beanCollection A collection of Beans.
  */

 public void fill(Collection<? extends Bean> beanCollection) throws BeanStructureException {

  for (Bean b : beanCollection)
   if (propertyClass.isAssignableFrom(b.getClass())) {
    @SuppressWarnings("unchecked")
    T b1 = (T)b;
    this.add(b1);
   }
   else
    throw new BeanStructureException("Can't add Bean of class \"" + b.getClass() + "\" to PropertyList with content \"" + propertyClass + "\"");
 }
 
}
