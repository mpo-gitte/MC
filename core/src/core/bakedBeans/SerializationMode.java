/**
 * Serialization mode for properties.

 * @version 2021-09-08
 * @author pilgrim.lutz@imail.de
 *
 */

package core.bakedBeans;

public enum SerializationMode {

 /**
  *  Property will never be serialized.
  */
 never,
 /**
  * Property will be serialized if modified.
  */
 ifSet,
 /**
  * Property will alays be serialized..
  */
 always,
}
