package core.bakedBeans;



/**
 * A very simple KeyMaker which increments a long value starting by one.
 *
 * @version 2018-09-07
 * @author pilgrim.lutz@imail.de
 *
 */

public class SillySequenceKeyMaker implements IKeyMaker {

 static private int id = 0;



 /**
  * Increments its internal value and sets the key of the bean to it.
  *
  * <p>Note: The key of the bean has to have the data type Long!</p>

  * @param forBean The bean
  * @throws BakedBeansKeyMakerException If problems occur.
  */

 @Override
 public void startMakeKey(PersistentBean forBean) throws BakedBeansKeyMakerException {

  synchronized (SillySequenceKeyMaker.class) {  // Making incrementing and storing atomic.
   ++id;

   try {
    forBean.set(forBean.getKeyName(), id);
   } catch (Exception e) {
    throw new BakedBeansKeyMakerException("SillySequenceKeyMaker got problems!");
   }
  }

 }



 /**
  * Does nothing for this KeyMaker.
  *
  * @param forBean the bean.
  * @throws BakedBeansKeyMakerException Should never be thrown.
  */

 @Override
 public void endMakeKey(PersistentBean forBean) throws BakedBeansKeyMakerException {

 }

}
