package core.bakedBeans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;


/**
 * A comparator for Beans.
 *
 * @version 2022-01-26
 * @author pilgrim.lutz@imail.de
 *
 * @param <T> The Bean.
 */

public class BeanComparator<T extends Bean> implements Comparator<T> {

 Logger logger = LogManager.getLogger(this.getClass());



 /**
  * Sort direction.
  */

 public enum Direction {
  ascending,
  descending
 }



 private Direction direction;
 private String propertyAlias;



 /**
  * Constructor.
  *
  * @param propertyAlias The alias of the property used for comparison.
  * @param direction The sort direction.
  */

 public BeanComparator(final String propertyAlias, final Direction direction) {

  this.propertyAlias = propertyAlias;
  this.direction = direction;

 }


 /**
  * Constructor.
  *
  * @param propertyAlias The alias of the property used for comparison.
  */

 public BeanComparator(final String propertyAlias) {

  this(propertyAlias, Direction.ascending);

 }



 /**
  * The compare method.
  *
  * <p>The comparison is done with the value of the given property. The value must implement Comparable!</p>
  *
  * @param b1 First Bean.
  * @param b2 Second Bean.
  * @return 0 for equality. -1 if b1 "less" than b2. 1 otherwise,
  *
  * @see Comparable
  */

 @Override
 public int compare(T b1, T b2) {

  try {
   @SuppressWarnings("unchecked")
   Comparable<Object> v1 = (Comparable)((T)b1).get(propertyAlias);
   @SuppressWarnings("unchecked")
   Comparable<Object> v2 = (Comparable)((T)b2).get(propertyAlias);

   return v1.compareTo(v2) * ((direction == Direction.descending) ? -1 : 1);

  } catch (Exception e) {
   logger.error("compare(): " + e);
  }

  return 0;
 }

}
