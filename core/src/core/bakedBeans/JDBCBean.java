package core.bakedBeans;



import core.exceptions.BeanStructureException;
import core.exceptions.BeanException;
import core.util.DBConnection;
import core.util.DBConnectionStore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;



/**
 * This class implements a PersistentBean which can be loaded and saved via JDBC.
 *
 * @version 2024-09-18
 * @author lp
 */

public abstract class JDBCBean extends PersistentBean {

 protected static final Logger logger = LogManager.getLogger(JDBCBean.class);
 private static HashMap<Class<? extends JDBCBean>, String> queryStatements = new HashMap<>();



 /**
  * Gets a connection for the DB.
  *
  * <p>This method makes sure that all subsequent beans get the same connection during save.</p>
  *
  * @return The connection.
  */

 public DBConnection getConnection() {

  return DBConnectionStore.getConnection();

 }



 /**
  * Releases a connection given by getConnection().
  *
  * @see JDBCBean#getConnection()
  */

 public void releaseConnection() {

  DBConnectionStore.releaseConnection();

 }



 /**
  * Overwriting this method is a chance for a derived application bean to modify the type map.
  *
  * @param types The type map.
  * @return The eventually modified type map. This implementation returns the unmodified map.
  */

 public HashMap<String, Class<?>> verifyTypes(HashMap<String, Class<?>> types){

  return types;

 }



 /**
  * Loads the bean with a DataStore using the loadQueryString from JDBCBeanDescription.
  *
  * @param keyvalue the value for the primary key.
  *
  * @return The bean.
  *
  * @see JDBCBeanDescription
  */

 public PersistentBean loadInternal(Object keyvalue) {

  String queryString = getLoadQueryString();

  logger.debug(() -> "load(): executing query: " + queryString);

  ResultSet rs = JDBCSupport.queryResultSet(this, queryString, new Object[] {keyvalue});

  try {
   if (rs.next()) {
    JDBCSupport.fillBeanFromResultset(this, rs, rs.getMetaData());

    // If the bean was empty it becomes clean after successful loading.
    // If the bean was dirty it remains dirty if all setted properties are
    // different from the value in the persistence system. Otherwise it becomes clean.

    if (isEmpty())
     setStatus(Status.clean);

    // Handle status of eventually loaded subbeans too.
    handleStatusSubBeans(this);

   }
//   else
//    JDBCSupport.close(rs, this);
  }
  catch (Exception e) {
   throw
    new BeanException(e);
  }
  finally {
   JDBCSupport.close(rs, this);
  }

   return this;

  }



 private void handleStatusSubBeans(PersistentBean bean) {

  bean.getPropertyInfos().values().stream()
   .filter(pi -> pi.getRelationType() != RelationType.none) // Only relations...
   .filter(pi -> pi.getRelationType() != RelationType.loose) // Only "real" relations...
   .filter(pi -> Utilities.isClassDerivedFrom(pi.getPclass(), PersistentBean.class))  // ... to another bean.
   .forEach(pi -> {
    try {
     PersistentBean b = (PersistentBean)bean.get(pi.getAlias());
     if (b.isEmpty()  &&  !b.getProperties().isEmpty())  // Something set during load of top bean.
      b.setStatus(Status.clean);
     handleStatusSubBeans(b);
    } catch (Exception e) {
     logger.error("handleStatusSubBeans(): Cannot handle sub bean: " + pi.getPclass() + "!");
    }
   });
 }



 /**
  * Gets the loadQueryString from the JDBCDescription.
  *
  * @return The query string.
  *
  * @see JDBCBeanDescription
  */

 protected synchronized String getLoadQueryString() {

  String loadQueryStatement = queryStatements.get(this.getClass());

  if (loadQueryStatement == null) {

   // Get it from annotation.
   JDBCBeanDescription anno = getAnnotation(JDBCBeanDescription.class);
   if (anno != null)
    loadQueryStatement = anno.loadQueryString();

   // Build it from bean infos.
   if (Utilities.isEmpty(loadQueryStatement)) {
    loadQueryStatement = JDBCSupport.getLoadQueryStringFromBeanProperties(
     this.getClass(),
     this.getSource(),
     null,
     null,
     "?",
     null,
     true,
     false);

    if (logger.isDebugEnabled())
     logger.debug("getLoadQueryString(): " + loadQueryStatement);

   }

   queryStatements.put(this.getClass(), loadQueryStatement);
  }

  return loadQueryStatement;

 }



 /**
  * Stores the bean in the underlying table.
  *
  * @param asNew If true the bean is assumed to be a new bean.
  */

 protected void store(boolean asNew) throws BeanException {

  String sd;

  if (asNew)
   sd = prepareInsertStatement();
  else
   sd = prepareUpdateStatement();

  DBConnection con = getConnection();

  PreparedStatement stmt = con.prepareStatement(sd);
  attachProperties(stmt, asNew ? null : getKeyValueInternal());

  try {
   stmt.executeUpdate();
  }
  catch (SQLException e) {
   throw new BeanException(e);
  }

  releaseConnection();

 }



 /**
  * Prepare connection and transaction.
  */

 @Override
 protected void openStoreBracket() {

  getConnection();

 }



 /**
  * Ends connection and transaction.
  */

 @Override
 protected void closeStoreBracket() {

  releaseConnection();

 }



 private void attachProperties(PreparedStatement stmt, Object keyValue) {

  //  TODO: We store only set properties. May be that this becomes a problem later!
  Iterator<?> it = getSettedProperties().keySet().iterator();
  int id = 1;

  try {
   while (it.hasNext()) {
    Object o = this.getByName((String)it.next());
    if (o == null)
     stmt.setNull(id ++, Types.OTHER);  // null values require special handling for Adabas.
    else
     stmt.setObject(id ++, o);
   }

   if (keyValue != null)
    stmt.setObject(id, keyValue);
  }
  catch (SQLException e) {
   throw new BeanException(e);
  }

 }



 /**
  * Prepares the SQL statement for inserting the bean into the DataBase table.
  *
  * @return The statement.
  */

 private String prepareInsertStatement() {

  int id;

  StringBuilder ps = new StringBuilder();

  ps.append("INSERT INTO ").append(getSource()).append(" (");

  Iterator<?> it = getSettedProperties().keySet().iterator();

  String sep = "";
  int count = 0;
  while (it.hasNext()) {
   String sd = (String)it.next();
   ps.append(sep).append(sd);
   sep = ",";
   count ++;
  }

  ps.append(" ) VALUES (");

  for (id = 1; id < count; id ++)
   ps.append("?,");
  ps.append("?)");

  return ps.toString();

 }



 /**
  * Prepares the SQL statement for updating the bean in the DataBase table.
  *
  * @return The statement.
  */

 private String prepareUpdateStatement() {

  StringBuilder ps = new StringBuilder();

  ps.append("UPDATE ").append(getSource()).append(" SET ");

  Iterator<?> it = getSettedProperties().keySet().iterator();

  String sep = "";
  while (it.hasNext()) {
   String sd = (String)it.next();
   ps.append(sep).append(sd).append("=?");
   sep = ",";
  }

  ps.append(" WHERE ").append(getKeyName()).append("=?");

  return ps.toString();

 }



 /**
  * Trys to find the bean.
  *
  * @param props Properties for the search operation.
  *
  * @return A bean or null if no bean can't be found.
  */

 public List<? extends PersistentBean> find(String ... props) {

  return find(props, null);

 }



 /**
  * Tries to find the bean.
  *
  * @param props Properties for the search operation.
  * @param extra extra parameter for searching.
  *
  * @return A bean or null if no bean can't be found.
  */

 public List<? extends PersistentBean> find(String[] props, String extra) {

  return find(props, extra, null);

 }



 /**
  * Tries to find the bean.
  *
  * @param propAliases Properties for the search operation.
  * @param extra extra parameter for searching.
  * @param orderBy order by clause.
  *
  * @return A bean or null if no bean can't be found.
  */

 public List<? extends PersistentBean> find(String[] propAliases, String extra, String orderBy) {

  List<Object> values = new ArrayList<>();

  StringBuilder sb = new StringBuilder(getFindSelect(propAliases, values));

  if (extra != null)
   sb.append(extra);

  if (orderBy != null)
   sb.append(" ORDER BY ").append(orderBy);

  logger.debug(() -> "find(): " + sb.toString());

  @SuppressWarnings("unchecked")
  Class<JDBCBean> bClass = (Class<JDBCBean>) this.getClass();

  @SuppressWarnings("unchecked")
  BeanList<JDBCBean> res = new BeanList(bClass);

  ResultSet rs = JDBCSupport.queryResultSet(this.getConnection(), sb.toString(), values.toArray());

  ResultSetMetaData rsm = null;
  try {
   rsm = rs.getMetaData();
  }
  catch (SQLException e) {
   throw new BeanException(e);
  }

  // Scan ResultSet, create bean and put it to the result list.

  try {
   while (rs.next()) {

    JDBCBean b = BeanFactory.make(bClass);
    JDBCSupport.fillBeanFromResultset(b, rs, rsm);
    b.setStatus(Status.clean);

    res.add(b);

   }
  }
  catch (Exception ex) {
   logger.error("forEach(): " + ex.getMessage());
  }
  finally {
   releaseConnection();
  }

  return res;

 }



 protected String getFindSelect(String[] propAliases, List<Object> values) {

  StringBuilder sb = new StringBuilder();

  String sep = "";

  for (String propAlias : propAliases) {
   sb.append(sep);
   sb.append(getPropertyInfo(propAlias).getName());
   Object o = get(propAlias);
   if (o == null)
    sb.append(" is null");
   else {
    sb.append("=?");
    values.add(o);
   }

   sep = " AND ";
  }

  String loadQueryStatement = JDBCSupport.getLoadQueryStringFromBeanProperties(
   this.getClass(),
   this.getSource(),
   null,
   null,
   null,
   sb.toString(),
   true,
   false);

  return
   loadQueryStatement;

 }



 /**
  * Removes (deletes) the bean in the database table.
  *
  * @throws BeanException If problems occurred.
  */

 @Override
 public void remove() {

  String sd = "DELETE FROM " + getSource() + " WHERE " + getKeyName() + "=?";

  logger.debug(() -> "remove(): \"" + sd + "\" for \"" + getKeyValueInternal() + "\"");

  DBConnection con = getConnection();

  PreparedStatement stmt = con.prepareStatement(sd);

  try {
   stmt.setObject(1, getKeyValueInternal());
   stmt.execute();
  }
  catch (SQLException e) {
   throw new BeanException(e);
  }

  releaseConnection();

 }



 /**
  * Rollbacks changes in the database table.
  */

 protected void rollback() {

  DBConnectionStore.rollback();

 }



 @Override
 protected String getQuery(PropertyInfo pi) {

  String query = super.getQuery(pi);

  if (Utilities.isEmpty(query)) {
   Class<?> clazz = pi.getIclass();

   @SuppressWarnings("unchecked")
   Class<? extends JDBCBean> beanClass = (Class<? extends JDBCBean>)clazz;

   String base = pi.getBase();

   if (Utilities.isEmpty(base))
    throw new BeanStructureException("getQuery(): base not set!");

   PropertyInfo bpi = TypedBean.getPropertyInfo(beanClass, base);

   if (Utilities.isEmpty(bpi))
    throw new BeanStructureException("getQuery(): Property for base \"" + base + "\" for bean \"" + beanClass.getName() + "\" not found!");

   query = JDBCSupport.getLoadQueryStringFromBeanProperties(
    beanClass,
    pi.getName(),
    pi.getLoadWith(),
    bpi.getName(),
    "?",
    null, false, false) + makeOrderClause(pi);

   // Store the query for later use.
   pi.setQuery(query);
  }

  if (logger.isDebugEnabled())
   logger.debug("getQuery(): " + query);

  return query;

 }




 private String makeOrderClause(PropertyInfo pi) {

  StringBuilder sb = new StringBuilder();

  String[] orderBy = pi.getOrderBy();

  return JDBCSupport.makeOrderClause(this.getClass(), orderBy);

 }

}
