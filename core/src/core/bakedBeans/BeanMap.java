package core.bakedBeans;

import core.exceptions.BeanStructureException;

import java.util.HashMap;
import java.util.List;



/**
 * A map for Beans.
 *
 * @version 2023-09-13
 * @author pilgrim.lutz@imail.de
 *
 * @param <T> Bean class.
 */

public class BeanMap<T extends Bean> extends HashMap<Object, T> {

 private String propertyAlias;
 private Class<T> beanClass;



 /**
  * Constructs a BeanMap.
  *
  * @param propertyAlias The value of the property with this alias will be used as the key for the map.
  */

 public BeanMap(Class<T> beanClass, final String propertyAlias) {

  super();

  this.beanClass = beanClass;

  this.propertyAlias = propertyAlias;

 }



 /**
  * Put the given bean in the map.
  *
  * @param bean This bean will be putted in the map.
  *             <p>The key is the value of the property with 'propertyAlias' given during construction of the map.</p>
  */

 public void put(T bean) {

  super.put(getKey(bean), bean);

 }



 /**
  * Put all the beans from the list in the map.
  *
  * @param beans This beans will be putted in the map.
  *              <p>The key is the value of the property with 'propertyAlias' given during construction of the map.</p>
  */

 public void put(List<T> beans) {

  beans.forEach(bean -> {
   super.put(getKey(bean), bean);
  });

 }



 private Object getKey(T bean) {

  return bean.get(propertyAlias);

 }



 /**
  * Get the bean from the map.
  *
  * @param bean Only need as bearer for the property used as key.
  *
  * @return The bean or null if bean was not included in the map.
  */

 public T get(T bean) {

  return super.get(getKey(bean));

 }



 /**
  * Get the content of the map as a list.
  *
  * @return A list with all beans from the map.
  */

 public BeanList<T> toList() throws BeanStructureException {

  return new BeanList<T>(this.beanClass , this);

 }

}
