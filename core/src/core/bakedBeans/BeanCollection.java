package core.bakedBeans;

import core.exceptions.BeanStructureException;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;



/**
 * A list of Beans per thread.
 *
 * @version 2023-08-19
 * @author lp
 *
 * @param <T>
 */

public class BeanCollection<T extends Bean> extends AbstractList<T> {

 private BeanList<T> originalBeanList = null;
 private Class<T> propertyClass = null;

 private ThreadLocal<BeanList> modifiedBeanLists = new ThreadLocal<BeanList>() {
  protected synchronized BeanList initialValue() {
   return null;
  }
 };



 public BeanCollection(Class<T> propertyClass) {

  this.propertyClass = propertyClass;

  originalBeanList = new BeanList<>(propertyClass);

 }



 private synchronized BeanList<T> getBeanListForThread() {

  @SuppressWarnings("unchecked")
  BeanList<T> ours = modifiedBeanLists.get();

  return
   (ours != null) ? ours : originalBeanList;

 }



 private synchronized BeanList<T> getBeanListForThreadModified() {

  @SuppressWarnings("unchecked")
  BeanList<T> ours = modifiedBeanLists.get();

  if (ours == null) {
   BeanList<T> bl = new BeanList<T>(propertyClass);
   try {
    bl.fill(originalBeanList);
   }
   catch (BeanStructureException e) {
    e.printStackTrace();
   }

   modifiedBeanLists.set(bl);
   ours = bl;
  }

  return
   ours;

 }



 public boolean add(T bean) {

  return
   getBeanListForThreadModified().add(bean);

 }



 public synchronized boolean addInitial(T bean) {

  return
   originalBeanList.add(bean);

 }



 public T get(int index) {

  return
   (T)getBeanListForThread().get(index);

 }



 @Override
 public int size() {

  return getBeanListForThread().size();

 }



 /**
  * Replaces each element of this list with the result of applying the
  * operator to that element.  Errors or runtime exceptions thrown by
  * the operator are relayed to the caller.
  *
  * @param operator the operator to apply to each element
  * @throws UnsupportedOperationException if this list is unmodifiable.
  *                                       Implementations may throw this exception if an element
  *                                       cannot be replaced or if, in general, modification is not
  *                                       supported
  * @throws NullPointerException          if the specified operator is null or
  *                                       if the operator result is a null value and this list does
  *                                       not permit null elements
  *                                       (<a href="Collection.html#optional-restrictions">optional</a>)
  * @implSpec The default implementation is equivalent to, for this {@code list}:
  * <pre>{@code
  *     final ListIterator<E> li = list.listIterator();
  *     while (li.hasNext()) {
  *         li.set(operator.apply(li.next()));
  *     }
  * }</pre>
  * <p>
  * If the list's list-iterator does not support the {@code set} operation
  * then an {@code UnsupportedOperationException} will be thrown when
  * replacing the first element.
  * @since 1.8
  */
 @Override
 public void replaceAll(UnaryOperator<T> operator) {

  getBeanListForThreadModified().replaceAll(operator);

 }



 /**
  * Sorts this list according to the order induced by the specified
  * {@link Comparator}.  The sort is <i>stable</i>: this method must not
  * reorder equal elements.
  *
  * <p>All elements in this list must be <i>mutually comparable</i> using the
  * specified comparator (that is, {@code c.compare(e1, e2)} must not throw
  * a {@code ClassCastException} for any elements {@code e1} and {@code e2}
  * in the list).
  *
  * <p>If the specified comparator is {@code null} then all elements in this
  * list must implement the {@link Comparable} interface and the elements'
  * {@linkplain Comparable natural ordering} should be used.
  *
  * <p>This list must be modifiable, but need not be resizable.
  *
  * @param c the {@code Comparator} used to compare list elements.
  *          A {@code null} value indicates that the elements'
  *          {@linkplain Comparable natural ordering} should be used
  * @throws ClassCastException            if the list contains elements that are not
  *                                       <i>mutually comparable</i> using the specified comparator
  * @throws UnsupportedOperationException if the list's list-iterator does
  *                                       not support the {@code set} operation
  * @throws IllegalArgumentException      (<a href="Collection.html#optional-restrictions">optional</a>)
  *                                       if the comparator is found to violate the {@link Comparator}
  *                                       contract
  * @implSpec The default implementation obtains an array containing all elements in
  * this list, sorts the array, and iterates over this list resetting each
  * element from the corresponding position in the array. (This avoids the
  * n<sup>2</sup> log(n) performance that would result from attempting
  * to sort a linked list in place.)
  * @implNote This implementation is a stable, adaptive, iterative mergesort that
  * requires far fewer than n lg(n) comparisons when the input array is
  * partially sorted, while offering the performance of a traditional
  * mergesort when the input array is randomly ordered.  If the input array
  * is nearly sorted, the implementation requires approximately n
  * comparisons.  Temporary storage requirements vary from a small constant
  * for nearly sorted input arrays to n/2 object references for randomly
  * ordered input arrays.
  *
  * <p>The implementation takes equal advantage of ascending and
  * descending order in its input array, and can take advantage of
  * ascending and descending order in different parts of the same
  * input array.  It is well-suited to merging two or more sorted arrays:
  * simply concatenate the arrays and sort the resulting array.
  *
  * <p>The implementation was adapted from Tim Peters's list sort for Python
  * (<a href="http://svn.python.org/projects/python/trunk/Objects/listsort.txt">
  * TimSort</a>).  It uses techniques from Peter McIlroy's "Optimistic
  * Sorting and Information Theoretic Complexity", in Proceedings of the
  * Fourth Annual ACM-SIAM Symposium on Discrete Algorithms, pp 467-474,
  * January 1993.
  * @since 1.8
  */
 @Override
 public void sort(Comparator<? super T> c) {

  getBeanListForThreadModified().sort(c);

 }



 /**
  * Creates a {@link Spliterator} over the elements in this list.
  *
  * <p>The {@code Spliterator} reports {@link Spliterator#SIZED} and
  * {@link Spliterator#ORDERED}.  Implementations should document the
  * reporting of additional characteristic values.
  *
  * @return a {@code Spliterator} over the elements in this list
  * @implSpec The default implementation creates a
  * <em><a href="Spliterator.html#binding">late-binding</a></em>
  * spliterator as follows:
  * <ul>
  * <li>If the list is an instance of {@link RandomAccess} then the default
  *     implementation creates a spliterator that traverses elements by
  *     invoking the method {@link List#get}.  If such invocation results or
  *     would result in an {@code IndexOutOfBoundsException} then the
  *     spliterator will <em>fail-fast</em> and throw a
  *     {@code ConcurrentModificationException}.
  *     If the list is also an instance of {@link AbstractList} then the
  *     spliterator will use the list's {@link AbstractList#modCount modCount}
  *     field to provide additional <em>fail-fast</em> behavior.
  * <li>Otherwise, the default implementation creates a spliterator from the
  *     list's {@code Iterator}.  The spliterator inherits the
  *     <em>fail-fast</em> of the list's iterator.
  * </ul>
  * @implNote The created {@code Spliterator} additionally reports
  * {@link Spliterator#SUBSIZED}.
  * @since 1.8
  */
 @Override
 public Spliterator<T> spliterator() {

  return getBeanListForThread().spliterator();

 }



 /**
  * Returns an array containing all of the elements in this collection,
  * using the provided {@code generator} function to allocate the returned array.
  *
  * <p>If this collection makes any guarantees as to what order its elements
  * are returned by its iterator, this method must return the elements in
  * the same order.
  *
  * @param generator a function which produces a new array of the desired
  *                  type and the provided length
  * @return an array containing all of the elements in this collection
  * @throws ArrayStoreException  if the runtime type of any element in this
  *                              collection is not assignable to the {@linkplain Class#getComponentType
  *                              runtime component type} of the generated array
  * @throws NullPointerException if the generator function is null
  * @apiNote This method acts as a bridge between array-based and collection-based APIs.
  * It allows creation of an array of a particular runtime type. Use
  * {@link #toArray()} to create an array whose runtime type is {@code Object[]},
  * or use {@link #toArray(Object[]) toArray(T[])} to reuse an existing array.
  *
  * <p>Suppose {@code x} is a collection known to contain only strings.
  * The following code can be used to dump the collection into a newly
  * allocated array of {@code String}:
  *
  * <pre>
  *     String[] y = x.toArray(String[]::new);</pre>
  * @implSpec The default implementation calls the generator function with zero
  * and then passes the resulting array to {@link #toArray(Object[]) toArray(T[])}.
  * @since 11
  */
 @Override
 public <T> T[] toArray(IntFunction<T[]> generator) {

  return
   getBeanListForThread().toArray(generator);

 }



 /**
  * Removes all of the elements of this collection that satisfy the given
  * predicate.  Errors or runtime exceptions thrown during iteration or by
  * the predicate are relayed to the caller.
  *
  * @param filter a predicate which returns {@code true} for elements to be
  *               removed
  * @return {@code true} if any elements were removed
  * @throws NullPointerException          if the specified filter is null
  * @throws UnsupportedOperationException if elements cannot be removed
  *                                       from this collection.  Implementations may throw this exception if a
  *                                       matching element cannot be removed or if, in general, removal is not
  *                                       supported.
  * @implSpec The default implementation traverses all elements of the collection using
  * its {@link #iterator}.  Each matching element is removed using
  * {@link Iterator#remove()}.  If the collection's iterator does not
  * support removal then an {@code UnsupportedOperationException} will be
  * thrown on the first matching element.
  * @since 1.8
  */
 @Override
 public boolean removeIf(Predicate<? super T> filter) {

  return
   getBeanListForThreadModified().removeIf(filter);

 }



 /**
  * Returns a sequential {@code Stream} with this collection as its source.
  *
  * <p>This method should be overridden when the {@link #spliterator()}
  * method cannot return a spliterator that is {@code IMMUTABLE},
  * {@code CONCURRENT}, or <em>late-binding</em>. (See {@link #spliterator()}
  * for details.)
  *
  * @return a sequential {@code Stream} over the elements in this collection
  * @implSpec The default implementation creates a sequential {@code Stream} from the
  * collection's {@code Spliterator}.
  * @since 1.8
  */
 @Override
 public Stream<T> stream() {

  return
   getBeanListForThread().stream();

 }



 /**
  * Returns a possibly parallel {@code Stream} with this collection as its
  * source.  It is allowable for this method to return a sequential stream.
  *
  * <p>This method should be overridden when the {@link #spliterator()}
  * method cannot return a spliterator that is {@code IMMUTABLE},
  * {@code CONCURRENT}, or <em>late-binding</em>. (See {@link #spliterator()}
  * for details.)
  *
  * @return a possibly parallel {@code Stream} over the elements in this
  * collection
  * @implSpec The default implementation creates a parallel {@code Stream} from the
  * collection's {@code Spliterator}.
  * @since 1.8
  */
 @Override
 public Stream<T> parallelStream() {

  return
   getBeanListForThread().parallelStream();

 }



 /**
  * Performs the given action for each element of the {@code Iterable}
  * until all elements have been processed or the action throws an
  * exception.  Actions are performed in the order of iteration, if that
  * order is specified.  Exceptions thrown by the action are relayed to the
  * caller.
  * <p>
  * The behavior of this method is unspecified if the action performs
  * side-effects that modify the underlying source of elements, unless an
  * overriding class has specified a concurrent modification policy.
  *
  * @param action The action to be performed for each element
  * @throws NullPointerException if the specified action is null
  * @implSpec <p>The default implementation behaves as if:
  * <pre>{@code
  *     for (T t : this)
  *         action.accept(t);
  * }</pre>
  * @since 1.8
  */
 @Override
 public void forEach(Consumer<? super T> action) {

  getBeanListForThread().forEach(action);

 }

}
