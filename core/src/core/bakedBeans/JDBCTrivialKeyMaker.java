package core.bakedBeans;

import java.sql.ResultSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.util.DB;
import core.util.DBConnection;




public class JDBCTrivialKeyMaker implements IKeyMaker {

 private static Logger logger;

 

 public JDBCTrivialKeyMaker() {

  super();

  logger = LogManager.getLogger(JDBCTrivialKeyMaker.class.getName());

 }



 public void startMakeKey(PersistentBean forBean) throws BakedBeansKeyMakerException {

  DBConnection con;
  try {
   con = ((JDBCBean)forBean).getConnection();
  } catch (Exception e1) {
   throw new BakedBeansKeyMakerException("can't get connection!");
  }

  ResultSet rs;
  try {
   String sd =
    "SELECT MAX(" + forBean.getKeyName() + ") FROM " +
     forBean.getSource() + DB.get().getDirtyReadSelectPostfix();

   logger.debug(sd);

   rs = con.prepareStatement(sd).executeQuery();
  } catch (Exception e1) {
   throw new BakedBeansKeyMakerException("can't get execute select max query!");
  }

  Integer newKeyValue = null;

  try {
   if (!rs.next())
    newKeyValue = 1;  // Table seems to be empty. Using the value 1 as the first key.
   else
    newKeyValue = rs.getInt(1) + 1;
  }
  catch (Exception e) {
//    newKeyValue = new Integer(1);  // Table seems to be empty. Using the value 1 as the first key.
   throw new BakedBeansKeyMakerException("can't get value from result set!");
  }

  try {
   forBean.set(forBean.getKeyName(), newKeyValue);
  }
  catch (Exception e) {
   throw new BakedBeansKeyMakerException("can't set key value");
  }

  try {
   ((JDBCBean)forBean).releaseConnection();
  } catch (Exception e) {
   throw new BakedBeansKeyMakerException("can't release connection.");
  }
  
 }

 

 public void endMakeKey(PersistentBean forBean) throws BakedBeansKeyMakerException {

 }

}
