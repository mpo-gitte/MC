package core.bakedBeans;


/**
 * Utilities for bakedBeans.
 *
 * @version 2024-04-10
 * @author lp
 *
 */

public class Utilities extends core.util.Utilities {



 /**
  * Checks if the given Object is empty.
  *
  * <p>If o is a Bean the return value is the return value of the bean isEmpty method.</p>
  *
  * @param o An object to check.
  *
  * @return True if o is empty false otherwise.
  *
  * @see core.util.Utilities#isEmpty
  */

 public static boolean isEmpty(Object o) {

  if (core.util.Utilities.isEmpty(o)) return true;

  if (isClassDerivedFrom(o.getClass(), Bean.class))
   return ((Bean)o).isEmpty();

  return false;

 }



 /**
  * Returns the value of the given property if the bean is not empty.
  *
  * @param bean The bean.
  * @param propertyAlias The alias of the property
  *
  * @return The value of the property.
  */

 public static Object getPropertyNotEmpty(TypedBean bean, String propertyAlias) {

  return
   isEmpty(bean) ? null : bean.get(propertyAlias);

 }

}
