package core.util;

import core.exceptions.SecretServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * This class is a utility to encrypt and decrypt Strings.
 *
 * @version 2018-12-01
 * @author pilgrim.lutz@imail.de
 *
 */

public class SecretService {

 private Logger logger = LogManager.getLogger(SecretService.class);
 private ResourceBundle rb;
 private HashMap<String, IAgent> agents = new HashMap<>();

 private static SecretService me = null;



 private SecretService() {

  rb = MergedPropertyResourceBundle.getMergedBundle("SecretService");

  me = this;

 }



 /**
  * The SecretService is a singleton. This method gets the one and only.
  *
  * @return The instance of the SecretService.
  *
  */

 public static SecretService get() {

  synchronized(SecretService.class) {
   if (me == null)
    me = new SecretService();
  }

  return me;

 }



 /**
  * Get an agent for encryption and decryption.
  *
  * @param purpose A string with a purpose. Purpose are defined in SecretService.properties.
  *
  * @return An agent.
  *
  */

 public synchronized IAgent getAgent(String purpose) {

  if (!agents.containsKey(purpose)) {

   logger.info("getAgent(): creating agent for purpose: \"" + purpose + "\"");

   String sd = rb.getString(purpose);

   Map<String, String> def = Utilities.getMapFromString(sd);

   try {
    @SuppressWarnings("unchecked")
    Class<? extends IAgent> clazz = (Class<? extends IAgent>)Class.forName(def.get("agent"));

    IAgent agent = null;

    agent = clazz.getConstructor().newInstance();

    agent.init(def.get("password"));

    agents.put(purpose, agent);

   }
   catch (Exception e) {
    throw new SecretServiceException(e);
   }

  }

  return agents.get(purpose);

 }



 /**
  * Returns all purposes from configuration.
  *
  * <p>Used only in unit test.</p>
  *
  * @return The purposes.
  */

 protected Enumeration<String> getPurposes() {

  return rb.getKeys();

 }



 /**
  * This class is the interface for all SecurityService agents.
  *
  * @version 2018-11-13
  * @author pilgrim.lutz@imail.de
  *
  */

 public interface IAgent {



  /**
   * Initializes the instance of the class.
   *
   * @param password The password for encryption
   */

  public void init(String password);



  /**
   * Encrypts the given String.
   *
   * @param what The String to encrypt.
   *
   * @return The encrypted String.
   */

  public String encrypt(String what);



  /**
   * Decrypts the given String.
   *
   * <p>Thr String has to be encoded with encrypt() of course.</p>
   *
   * @param what The String to decrypt.
   *
   * @return The decrypted String.
   */

  public String decrypt(String what);


  /**
   * Check if the given string leads to the given hash.
   *
   * @param what The string to check
   * @param hashed The hash value to check against
   *
   * @return true for match. False otherwise.
   */

  public boolean check(String what, String hashed);

 }
}
