package core.util;

import core.exceptions.DBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;



/**
 * Some base methods for Database access.
 *
 * @version 2023-08-18
 * @author pilgrim.lutz@imail.de
 */

public class DBBase {

 private static Logger logger = LogManager.getLogger(DBBase.class);

 static ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle("DB");

 static String Url = rb.getString("URL");
 static Properties props = new Properties();
 static {
  props.put("user", rb.getString("User"));
  props.put("password", rb.getString("Password"));
 }
 static String Driver = rb.getString("Driver");



 /**
  * Loads the Database driver.
  */

 public static void loadDriver() {

  try {
   // Try to load driver.
   Class.forName(Driver);
  }
  catch (Throwable e) {
   String mess = "loadDriver(): Can't load JDBC driver: \"" + Driver + "\"\n\n" + e.getMessage();
   logger.error(mess);
   throw new DBException(mess);
  }

 }



 /**
  * Gets a "raw" connection.
  *
  * <p>This is a "real" connection from the DriverManager. It is not a pool member and will
  * never be shared!</p>
  *
  * @return A connection to the DB.
  */

 public static Connection getRawConnection() {

  try {
   return
    DriverManager.getConnection(Url, props);
  } catch (SQLException e) {
   throw new DBException(e);
  }

 }



 /**
  * Returns the Url of the Database.
  *
  * @return The Url.
  */

 public static String getUrl () {

  return Url;

 }



 /**
  * Return the Database driver.
  *
  * @return The driver.
  */

 public static Driver getDriver() {

  try {
   return
    DriverManager.getDriver(Url);
  }
  catch (SQLException e) {
   throw new DBException(e);
  }

 }



 /**
  * Returns the Database user.
  *
  * @return The user.
  */

 public static String getUser() {

  return
   props.get("user").toString();

 }

}
