package core.util;


import core.base.MeterMaid;
import core.exceptions.DBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Supplier;



/**
 * A class for database access.
 *
 * <p>Provides a simple connection pool.</p>
 *
 * @version 2025-01-01
 * @author lp
 *
 */

public class DB {

 private static Logger logger = LogManager.getLogger(DB.class.getName());
 private static boolean connected;
 private static String catalog;

 private static DB me = null;
 private static final ArrayList<DBConnection> connections = new ArrayList<DBConnection>();
 private static int MaxConnections = 5;
 private static int MinConnections = 0;
 private static int Wait = 5000;
 private static int Retries = 5;
 private static int LifeTime = 60000;
 private static String ReadDirtySelectPostfix = "";
 private static boolean Dropable = false;
 private final Monitor connectionMonitor = new Monitor();
 private static int nrWaitingThreads = 0;

 private static MeterMaid lovelyRita = MeterMaid.get();



 /**
  * Standard constructor.
  * <p>An application should have only one instance of this class.</p>
  * <p>The constructor reads some parameters from a file called DB.properties:<p>
  * <table>
  * <tr><th>Name</th><th>Purpose</th><th>Default</th></tr>
  * <tr><td>URL</td><td>The URL for the database to connect to.</td><td>-</td></tr>
  * <tr><td>Driver</td><td>The name of the driver class.</td><td>-</td></tr>
  * <tr><td>User</td><td>The name of the database user.</td><td>-</td></tr>
  * <tr><td>Password</td><td>His password.</td><td>-</td></tr>
  * <tr><td>MaxConnections</td><td>The maximum number of database connection the application will get.</td><td>5</td></tr>
  * <tr><td>MinConnections</td><td>The minimum number of database connection held open.</td><td>0</td></tr>
  * <tr><td>Retries</td><td>The number of retries to get a connection.</td><td>5</td></tr>
  * <tr><td>Wait</td><td>The time to wait between the retries.</td><td>2000</td></tr>
  * <tr><td>LifeTime</td><td>The lifetime for connections (msec).</td><td>60000</td></tr>
  * <tr><td>TypeMap</td><td>The name of a resource bundle with valid data types for this database.</td><td>-</td></tr>
  * </table>
  */

 public DB() {

  ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle("DB");

  MaxConnections = Utilities.getParam(rb, "MaxConnections", 5);
  MinConnections = Utilities.getParam(rb, "MinConnections", 0);
  Retries = Utilities.getParam(rb, "Retries", 5);
  Wait = Utilities.getParam(rb, "Wait", 2000);
  LifeTime = Utilities.getParam(rb, "LifeTime", 60000);
  ReadDirtySelectPostfix = rb.getString("ReadDirtySelectPostfix");
  Dropable = Utilities.getParam(rb, "Dropable", false);

  lovelyRita.setGauge("number_max_db_connections", new Supplier<Number>() {
   @Override
   public Integer get() {
    return MaxConnections;
   }
  });

  lovelyRita.setGauge("number_min_db_connections", new Supplier<Number>() {
   @Override
   public Integer get() {
    return MinConnections;
   }
  });

  lovelyRita.setGauge("number_current_db_connections", new Supplier<Number>() {
   @Override
   public Integer get() {
    return getNrConnections();
   }
  });

  lovelyRita.setGauge("number_current_used_db_connections", new Supplier<Number>() {
   @Override
   public Integer get() {
    return getNrUsedConnections();
   }
  });

  lovelyRita.setGauge("number_current_waiting_threads", new Supplier<Number>() {
   @Override
   public Integer get() {
    return nrWaitingThreads;
   }
  });

  DBBase.loadDriver();

  Timer.get().run("DB.ConnectionWatcher", Utilities.getParam(rb, "ConnectionWatcher.schedule", "0 0/1 * * * ?"), false, () -> {

   logger.debug("ConnectionWatcher: Checking connections.");

   synchronized(connections) {
    ArrayList<DBConnection> constc = new ArrayList<DBConnection>();

    for (DBConnection c: connections)
     if (c.getCloseTag()) {
      if (connections.size() > MinConnections) {
       logger.debug("ConnectionWatcher: Closing connection.");
       c.close();
       constc.add(c);
      }
     }
     else
      c.setCloseTag();

    for (DBConnection c : constc)
     connections.remove(c);
   }

  });

  connected = false;

  me = this;

 }



 /**
  * Returns the one and only instance of this class.
  * @return the singleton
  */

 public static DB get() {

  synchronized(DB.class) {
   if (me == null)
    new DB();
  }

  return me;

 }



 private DBConnection getConnectionFromPool() {

  synchronized(connections) {

   // Try to get an unused connection from the pool.

   for (DBConnection c: connections)
    if (!c.isInUse()) {
     logger.debug(() -> "getConnectionFromPool(): Got a connection from the pool. Created: " + c.getCreation() + " Used: " + c.getUsageCount() + " Last used: " + c.getLastUse());
     return c.use();
    }

   // Got none? Let's make a new connection to the database.

   if (connections.size() < MaxConnections) {

    // Make a new connection.

    Connection con = DBBase.getRawConnection();

    if (!connected) {
     if (logger.isInfoEnabled()) {
      logger.info(DBBase.getDriver());
      logger.info(DBBase.getUrl());
      logger.info("user: " + DBBase.getUser());
     }

     // Get some meta data from the database.
     try {
      catalog = con.getCatalog();
     }
     catch (SQLException e) {
      throw new DBException(e);
     }
     connected = true;
    }

    DBConnection c = new DBConnection(con);
    connections.add(c);
    logger.debug(() -> "Created a new connection: " + connections.size() + "#" + MaxConnections);

    return c.use();  // A brand-new connection.
   }

  }

  return null;  // Got no connection!

 }



 /**
  * Get a connection to the database.
  * <p>This connection has to be released after usage.</p>
  * @return a connection to the database.
  */

 public DBConnection getConnection() {

  int retry = Retries;
  DBConnection con;

  while (retry > 0) {
   con = getConnectionFromPool();
   if (con != null) return con;

   synchronized(connectionMonitor) {
    ++ nrWaitingThreads;

    try {
     if (logger.isDebugEnabled())
      logger.debug("Waiting for a new connection. " + retry + " Times remaining.");
     connectionMonitor.signalled = false;
     connectionMonitor.wait(Wait);
     logger.debug(() -> "End waiting for a new connection. Signal: " + connectionMonitor.signalled);
     if (!connectionMonitor.signalled)
      -- retry;
    }
    catch (InterruptedException e) {
     logger.error("Waiting for connection was interupted!");
    }

    -- nrWaitingThreads;
   }
  }

  logger.error("Can't get a database connection!");

  throw new DBException("Can't get a database connection!");

 }



 /**
  * Releases a connection.
  * <p>The connection will be reused later.</p>
  * <p>The connection must not be used after calling this method.</p>
  * @param con the database connection to be released.
  */

 public void releaseConnection(DBConnection con) {

  synchronized(connections) {
   for (DBConnection c: connections)
    if (c == con) {
     c.unUse();
     logger.debug("relaseConnection(): Release a connection");

     synchronized(connectionMonitor) {
      connectionMonitor.signalled = true;
      connectionMonitor.notify();
     }

     break;
    }
  }
 }



 /**
  * Returns the maximum number of database connections.
  * @return The maximum number of connections.
  */

 public Integer getMaxConnections() {

  return MaxConnections;

 }



 /**
  * Returns the current number of database connections.
  * @return The current number of connections.
  */

 public Integer getNrConnections() {

  return connections.size();

 }



 /**
  * Returns the current number of database connections in use.
  * @return The current number of connections.
  */

 public Integer getNrUsedConnections() {

  return
   (int)connections
    .stream()
    .filter(DBConnection::isInUse)
    .count();

 }



 /**
  * Returns the postfix for the select statement for selecting "dirty records".
  * @return The postfix or an empty string.
  */

 public String getDirtyReadSelectPostfix() {

  if (ReadDirtySelectPostfix == null)
   return "";

  return " " + ReadDirtySelectPostfix;

 }



 /**
  * Dropping the given tables form the database.
  *
  * <p>Database must be marked as "droppable" in DB.properties. Otherwise an exception is thrown.</p>
  *
  * @param sequences If true objectsToDrop contains sequence names. If false tables are meant.
  * @param objectsToDrop A list with table names.
  */

 public void dropObjects(boolean sequences, List<String> objectsToDrop) {

  if (Dropable) {
   DBConnection con = getConnection();

   for (String tn : objectsToDrop) {
    logger.debug(() -> "dropObjects(): dropping \"" + tn + "\"");

    Statement stmt = null;
    try {
     stmt = con.getConnection().createStatement();
    }
    catch (SQLException e) {
     throw new DBException(e);
    }
    try {
     stmt.execute("drop " + (sequences ? "sequence " : "table ") + " if exists " + tn + " cascade");
    }
    catch (SQLException e) {
     logger.error("Cannot drop \"" + tn + "\"\n" + e);
     // Is wurscht.
    }
   }

   releaseConnection(con);
  }
  else {
   String mess = "Database is not dropable!";

   logger.error(mess);
   throw new DBException(mess);
  }

 }



 private class Monitor {

  public boolean signalled;

 }

}
