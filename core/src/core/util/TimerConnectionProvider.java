package core.util;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;



/**
 * Retrieves DB connections for the Timer
 *
 * @version 2023-08-18
 * @author pilgrim.lutz@imail.de
 */

public class TimerConnectionProvider implements org.quartz.utils.ConnectionProvider {

 private static final Logger logger = LogManager.getLogger(TimerConnectionProvider.class);



 /**
  * Fetches a connection.
  *
  * @return connection managed by this provider
  */

 @Override
 public Connection getConnection() {

  logger.debug(() -> "getConnection(): ");

  return
   DBBase.getRawConnection();

 }



 @Override
 public void shutdown() throws SQLException {

  logger.debug(() -> "shutdown(): ");

 }



 @Override
 public void initialize() throws SQLException {

 }

}
