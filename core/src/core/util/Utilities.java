package core.util;


import core.exceptions.MCException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.exception.MethodInvocationException;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.CharacterIterator;
import java.text.Normalizer;
import java.text.StringCharacterIterator;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * A bundle of utility methods.
 *
 * @version 2024-09-24
 * @author lp
 */

public class Utilities {

 private static final Logger logger = LogManager.getLogger(Utilities.class);



 /**
  * Checks if argument is null.
  *
  * @param o Argument to check.
  * @return true if o is null. False otherwise.
  *
  */

 public static boolean isNull(Object o) {

  return (o == null);

 }



 /**
  * Returns if the given object is null or an empty string.
  *
  * @param o The object.
  *
  * @return True if empty false otherwise.
  */

 public static boolean isEmpty(Object o) {

  if (isNull(o)) return true;

  if (o.getClass() == String.class)
   if (((String)o).length() == 0) return true;

  return false;

 }


 /**
  * Checks if list is null or empty.
  *
  * @param l The list.
  *
  * @return true if list is null or empty.
  */

 public static boolean isEmptyList(List<? extends Object> l) {

  if (l == null)
   return true;

  return
   l.isEmpty();

 }



 public static boolean isTrue(Object o) {

  if (o == null) return false;

  Class<?> cl = o.getClass();

  if (cl == Boolean.class) {
   return ((Boolean)o).booleanValue();
  }

  return false;

 }



 public static boolean isZero(Object o) {

  if (o == null) return true;

  Class<?> cl = o.getClass();

  if (cl == Integer.class) {
   if (((Integer)o).intValue() == 0)
    return true;
  }

  return true;

 }



 public static String getParam(ResourceBundle rb, String val, String defval) {

  String ret = null;

  try {
   ret = rb.getString(val);
  }
  catch (Exception e) { /* Doesn't matter. */ }

  if (isEmpty(ret)) return defval;

  return ret;

 }



 public static String getParam(ResourceBundle rb, String val) {

  String ret = null;

  try {
   ret = rb.getString(val);
  }
  catch (Exception e) { /* Doesn't matter. */ }

  if (isEmpty(ret)) return null;

  return ret;

 }



 /**
  * Gets a parameter from a ResourceBundle as a List of Strings.
  *
  * @param rb the ResourceBundle
  * @param val The name of the entry.
  * @param defval The default value.
  * @param separator The separator within the ResourceBundle entry.
  *
  * @return The List of Strings or a List made up from the defval array.
  */

 public static List<String> getParamAsList(ResourceBundle rb, String val, String[] defval, String separator) {

  String sd = null;

  try {
   sd = rb.getString(val);
  }
  catch (Exception e) { /* Doesn't matter. */ }

  if (isEmpty(sd))
   return Arrays.asList(defval);

  return Arrays.asList(sd.split(separator));

 }



 /**
  * Gets a parameter from a ResourceBundle as a List of Strings.
  *
  * @param rb the ResourceBundle
  * @param val The name of the entry. Items have to be separated by a colon.
  * @param defval The default value.
  *
  * @return The List of Strings or a List made up from the defval array.
  */

 public static List<String> getParamAsList(ResourceBundle rb, String val, String[] defval) {

  return getParamAsList(rb, val, defval, ";");

 }



 /**
  * Gets parameter from a ResourceBundle.
  *
  * @param rb The ResourceBundle.
  * @param val The name of the parameter.
  * @param defval A default value.
  *
  * @return The value of the parameter or the default value if the parameter doesn't exist as a boolean.
  */

 public static boolean getParam(ResourceBundle rb, String val, boolean defval) {

  String ret = null;

  try {
   ret = rb.getString(val);
  }
  catch (Exception e) { /* Doesn't matter. */ }

  if (isEmpty(ret)) return defval;

  return Boolean.parseBoolean(ret);

 }



 /**
  * Gets parameter from a ResourceBundle.
  *
  * @param rb The ResourceBundle.
  * @param val The name of the parameter.
  * @param defval A default value.

  * @return The value of the parameter or the default value if the parameter doesn't exist as a int.
  */

 public static int getParam(ResourceBundle rb, String val, int defval) {

  String ret = null;

  try {
   ret = rb.getString(val);
  }
  catch (Exception e) { /* Doesn't matter. */ }

  if (isEmpty(ret)) return defval;

  return Integer.parseInt(ret);

 }



 /**
  * Shortens the given string to the given length.
  *
  * @param str The string.
  * @param len The length.
  *
  * @return The shortend string.
  */

 public static String shorten(String str, int len) {

  if (str.length() > len)
   return str.substring(0, len);

  return str;

 }



 /**
  * Capitalizes a string. That is the returned string starts with an upper case letter.
  *
  * @param str The input string.
  * @return The capitalized string.
  */

 public static String capitalize(String str) {

  if (!isEmpty(str))
   return str.substring(0, 1).toUpperCase() + str.substring(1);

  return null;

 }



 /**
  * Quotes a String for JavaScript.
  * @param str The String.
  * @return The quoted String.
  */

 public static String jsQuote(String str) {

  CharacterIterator it = new StringCharacterIterator(str);
  StringBuffer sb = new StringBuffer();

  char c = it.first();

  while (c != CharacterIterator.DONE) {
   switch(c) {
    case '\\':
     sb.append("\\\\");
     break;
    case '\'':
     sb.append("\\x27");
     break;
    case '"':
     sb.append("\\x22");
     break;
    default:
     sb.append(c);
     break;
   }
   c = it.next();
  }

  return sb.toString();

 }



 /**
  * Quotes a String.
  * @param str The String.
  * @return The quoted String.
  */

 public static String normalQuote(String str) {

  CharacterIterator it = new StringCharacterIterator(str);
  StringBuffer sb = new StringBuffer();

  char c = it.first();

  while (c != CharacterIterator.DONE) {
   switch(c) {
    case '\\':
     sb.append("\\\\");
     break;
    case '"':
     sb.append("\\\"");
     break;
    default:
     sb.append(c);
     break;
   }
   c = it.next();
  }

  return sb.toString();

 }



 /**
  * Quotes a String.
  * @param str The String.
  * @return The quoted String.
  */

 public static String postgresQuote(String str) {

  CharacterIterator it = new StringCharacterIterator(str);
  StringBuffer sb = new StringBuffer();

  char c = it.first();

  while (c != CharacterIterator.DONE) {
   switch(c) {
    case '\\':
     sb.append("\\005c");
     break;
    case '\'':
     sb.append("\\0027");
     break;
    case '\b':
     sb.append("\\0008");
     break;
    case '\f':
     sb.append("\\000c");
     break;
    case '\n':
     sb.append("\\000a");
     break;
    case '\r':
     sb.append("\\000d");
     break;
    case '\t':
     sb.append("\\0009");
     break;
    default:
     sb.append(c);
     break;
   }
   c = it.next();
  }

  return sb.toString();

 }



 /**
  * "urlencodes" a String.
  *
  * @param str The String.
  * @return The quoted String.
  *
  * @throws UnsupportedEncodingException If encoding isn't present.
  */

 public static String urlEncode(String str) throws UnsupportedEncodingException {

  return URLEncoder.encode(str, "UTF-8");

 }



 /**
  * Encodes an URI component.
  *
  * @param str The string to encode
  * @return The encoded string
  *
  * @throws UnsupportedEncodingException If problems occurred.
  */

 public static String encodeURIComponent(String str) throws UnsupportedEncodingException {

  return urlEncode(str)
   .replaceAll("\\+", "%20")
   .replaceAll("\\%21", "!")
   .replaceAll("\\%27", "'")
   .replaceAll("\\%28", "(")
   .replaceAll("\\%29", ")")
   .replaceAll("\\%7E", "~");

 }



 /**
  * Encodes an URI component.
  *
  * <p>With less replacement.</p>
  *
  * @param str The string to encode
  * @return The encoded string
  *
  * @throws UnsupportedEncodingException If problems occurred.
  */

 public static String encodeURIComponentAlt(String str) throws UnsupportedEncodingException {

  return urlEncode(str)
   .replaceAll("\\.", "%2E")
   .replaceAll("-", "%2D")
   .replaceAll("_", "%5F");

 }



 /**
  * Decodes an URI component.
  *
  * <p>This includes the "+"!</p>
  *
  * @param str The string to decode.
  * @return The decoded string..
  *
  * @throws UnsupportedEncodingException If errors occur.
  */

 public static String decodeURIComponent(String str) throws UnsupportedEncodingException {

  return URLDecoder.decode(str.replace("+", "%2B"), "UTF-8");

 }



 public static String normalUnQuote(String str) {

  CharacterIterator it = new StringCharacterIterator(str);
  StringBuffer sb = new StringBuffer();

  char c = it.first();
  boolean tn = false;
  while (c != CharacterIterator.DONE) {
   if (c == '\\' && !tn)
    tn = true;
   else {
    sb.append(c);
    tn = false;
   }
   c = it.next();
  }

  return sb.toString();

 }



 public static ArrayList<String> split(String str) {

  return split(str, ';', '"', '\\');

 }



 public static ArrayList<String> split(String str, char splitChar) {

  return split(str, splitChar, '"', '\\');

 }



 public static ArrayList<String> split(String str, char splitChar, char stringDelimiter, char stringQuoteChar) {

  ArrayList<String> res = new ArrayList<String>();
  CharacterIterator it = new StringCharacterIterator(str);
  StringBuffer sb = new StringBuffer();

  boolean inString = false;
  boolean asItIs = false;
  char c = it.first();

  while (c != CharacterIterator.DONE) {
   if (c == splitChar  &&  !inString) {
    res.add(sb.toString().trim());
    sb = new StringBuffer();
   }
   else if (c == stringDelimiter) {
    if (asItIs)
     asItIs = false;
    else if (inString)
     inString = false;
    else
     inString = true;
    sb.append(c);
   }
   else if (c == stringQuoteChar) {
    asItIs = !asItIs;
    sb.append(c);
   }
   else
    sb.append(c);

   c = it.next();
  }

  if (sb.length() > 0)
   res.add(sb.toString().trim());

  return res;

 }



 /**
  * Converts an Object into its string representation
  *
  * <p>Some more complex data types will be quoted.</p>
  *
  * @param cl Objects class
  * @param o The object
  * @return A String containing the objects string representation
  */

 public static final String STD_DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS";

 public static String objectToString(Class<? extends Object> cl, Object o) {

  if (cl == Integer.class  ||  cl == Long.class  ||  cl == Float.class  ||  cl == Double.class  ||  cl == Boolean.class) {
   if (o != null)
    return o.toString();
  }
  else if (cl == LocalDateTime.class) {
   if (o != null) {
    DateTimeFormatter df = DateTimeFormatter.ofPattern(STD_DATE_TIME_PATTERN);
    return "\"" + df.format((LocalDateTime) o) + "\"";  // Always with quotes.
   }
  }
  else {
   if (o != null)
    return "\"" + Utilities.normalQuote(o.toString()) + "\"";  // all other types (including String, dates and "unknown") will be quoted.
   else
    return "\"\"";
  }

  return "";

 }



 public static String objectToString(Object o) {

  return objectToString(o.getClass(), o);

 }



 /**
  * Checks whether the first given class is derived from the second.
  *
  * @param cl Class to test
  * @param testClass Class to test against.

  * @return true if first class is derived from second. False otherwise.
  */

 public static boolean isClassDerivedFrom(Class<?> cl, Class<?> testClass) {

  return testClass.isAssignableFrom(cl);

 }



 public static boolean isEqual(Object o1, Object o2) {

  if (o1 == null  &&  o2 == null)
   return true;
  if (o1 == null  ||  o2 == null)
   return false;

  return o1.equals(o2);

 }



 public static void logDebug (String mess) {

  logger.debug(mess);

 }



 /**
  * Returns true if str is null or an empty string.
  *
  * @param str The string.
  * @return true if null oder zero length. False otherwise.
  */

 public static boolean stringIsEmpty(String str) {

  if (isNull(str))
   return true;

  if (str.length() == 0)
   return true;

  return false;
 }



 /**
  * Returns the current time.
  *
  * @return now
  */

 public static Date now() {

  return new Date();

 }



 /**
  * Returns the beginning of the current week.
  *
  * @param date The current date
  * @param loc The locale
  *
  * @return a date with the first day of the week at 0 o'clock.
  */

 public static LocalDate beginOfThisWeek(LocalDate date, Locale loc) {

  DayOfWeek firstDayOfWeek = WeekFields.of(loc).getFirstDayOfWeek();

  DayOfWeek weekDay = date.getDayOfWeek();

  int id = 0;
  if (firstDayOfWeek == DayOfWeek.MONDAY) {  // Week starts on monday.
   id = weekDay.getValue() - firstDayOfWeek.getValue();
  }
  else if (firstDayOfWeek == DayOfWeek.SUNDAY) {  // Week starts on sunday.
   id = weekDay.getValue();
   if (weekDay == DayOfWeek.SUNDAY) id = 0;
  }

  return date.plusDays(-id);

 }



 /**
  * Returns the beginning of the current week.
  *
  * @param loc The locale
  *
  * @return a date with the first day of the week at 0 o'clock.
  */

 public static LocalDate beginOfThisWeek(Locale loc) {

  return beginOfThisWeek(LocalDate.now(), loc);

 }



 /**
  * Returns the beginning of the given day.
  *
  * @param date the day
  *
  * @return this very early morning.
  */

 public static LocalDateTime todaysBeginning(LocalDateTime date) {

  return
   date.withHour(0).withMinute(0).withSecond(0).withNano(0);

 }



 /**
  * Returns the beginning of the current day.
  *
  * @return this very early morning.
  */

 public static LocalDateTime todaysBeginning() {

  return todaysBeginning(LocalDateTime.now());

 }



 /**
  * Returns the end of current day.
  *
  * @param date The day
  *
  * @return the last moment of this day.
  */

 public static LocalDateTime todaysEnd(LocalDateTime date) {

  return
   date.withHour(23).withMinute(59).withSecond(59).withNano(999999999);

 }



 /**
  * Returns the end of current day.
  *
  * @return the last moment of this day.
  */

 public static LocalDateTime todaysEnd() {

  return todaysEnd(LocalDateTime.now());

 }



 /**
  * Calculates the difference between to timestamps.
  *
  * @param ts1 The first timestamp.
  * @param ts2 The second timestamp.
  *
  * @return the difference in milliseconds.
  */

 public static long diffTimestamps(LocalDateTime ts1, LocalDateTime ts2) {

  // Early - later = positive!

  return
   ts2.toInstant(ZoneOffset.UTC).toEpochMilli() - ts1.toInstant(ZoneOffset.UTC).toEpochMilli();

 }



 /**
  * Strips off surrounding double quotes and unquotes the String.
  * @param str The String.
  * @return The "naked" String.
  * @see Utilities#clothed(String)
  */

 public static String naked(String str) {

  int st = 0;
  int en = str.length();

  if (str.startsWith("\"")) {
   st = 1;
  }
  if (str.endsWith("\""))
   en --;

  if (en <= st)
   return "";

  return normalUnQuote(str.substring(st, en));

 }



 /**
  * Gets a Map from String.
  *
  * @param srcString The string with the map.
  * @return The "real" map.
  */

 public static Map<String, String> getMapFromString(String srcString) {

  Map<String, String> res = new HashMap<>();

  for (String p : split(srcString)) {
   ArrayList<String> sp = split(p, '=');
   if (sp.size() == 2)
    res.put(sp.get(0), naked(sp.get(1)));
  }

  return res;
 }



 /**
  * Surrounds a String with double quotes and quotes double quotes and backslashes.
  *
  * @param str The string to handle.
  *
  * @return The quoted and surrounded String.
  *
  * @see Utilities#naked(String)
  */

 public static String clothed(String str) {

  return (new StringBuilder()).append("\"").append(normalQuote(str)).append("\"").toString();

 }



 /**
  * Converts a string to the given data type.
  *
  * @param value The value.
  * @param cl The class of the data type.
  *
  * @return The converted value.
  */

 public static Object makeGeneralValue(String value, Class<?> cl) {

  Object i = null;

  final String special_vaulue_null = "~NULL~";

  if (!special_vaulue_null.equals(value)) {
   if (cl == String.class)
    i = value;
   else if (cl == LocalDate.class)
    i = (isEmpty(value)) ? null : LocalDate.parse(value, DateTimeFormatter.ISO_LOCAL_DATE);
   else if (cl == LocalDateTime.class)
    i = (isEmpty(value)) ? null : LocalDateTime.parse(value, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss[.SSSSSS]"));
   else if (!isEmpty(value)) {
    try {
     Method m = cl.getMethod("valueOf", new Class<?>[]{String.class});
     i = m.invoke(null, new Object[]{value});
    }
    catch (Exception e) {
     logger.error("makeGeneralValue(): valueOf not found for " + cl.getName());
     throw
      new MCException(e);
    }
   }
  }

  return i;

 }



 /**
  * Converts a string to the given data type.
  *
  * @param value The value.
  * @param clname The name of the class of the data type.
  *
  * @return The converted value.
  */

 public static Object makeGeneralValue(String value, String clname) {

  try {
   return makeGeneralValue(value, Class.forName(clname));
  }
  catch (ClassNotFoundException e) {
   throw new MCException(e);
  }

 }



 /**
  * Changes the given string to friendly foldername.
  *
  * <p>Some characters will be mapped. Some characters will be ignored.</p>
  *
  * @param str The string
  * @return foldername
  */

 public String makeNiceFoldername(String str) {

  StringBuilder sb = new StringBuilder();

  for (char c : str.toUpperCase().toCharArray())
   if ((c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'))
    sb.append(c);
   else if (c == '\u00c4')  // Ä
    sb.append("AE");
   else if (c == '\u00d6')  // Ö
    sb.append("OE");
   else if (c == '\u00dc')  // Ü
    sb.append("UE");
   else if (c == '\u00df')  // ß
    sb.append("ss");
   else if (c == ' ')
    sb.append("_");

  return sb.toString();
 }



 /**
  * Gets the underlying exception for MethodInvocationException and
  * InvocatinTargetException.
  *
  * @param ex The Exception.
  * @return The "inner" exception. Or the given exception if it is not one of the above.
  */

 public static Throwable getRealProblem(Throwable ex) {

  Class<?> ec = ex.getClass();

  if (ec == MethodInvocationException.class)
   return ((MethodInvocationException) ex).getCause();  // Get the real problem.

  if (ec == InvocationTargetException.class) {
   Throwable target = ((InvocationTargetException) ex).getTargetException();

   while (target.getClass() == InvocationTargetException.class)
    target = ((InvocationTargetException) target).getTargetException();

   return target;  // Get the real problem.
  }

  return ex;

 }



 /**
  * Gets a system property.
  *
  * @param propName property name
  * @param defVal default value.
  *
  * @return The value of the property or the default value if property can't be found.
  *
  */

 public static String getSystemProperty(String propName, String defVal) {

  String bn = System.getProperty(propName);

  if (!isEmpty(bn))
   return bn;

  return defVal;

 }



 /**
  * Returns the date as a String formatted like "yyyyMMdd"
  *
  * @param date The date to convert
  * @return The converted string
  */

 public static String getDateString(LocalDate date) {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyyMMdd");
  return df.format(date);

 }



 /**
  * Returns the date as a String formatted like "yyyyMMddThhmmssZ"
  *
  * @param date The date to convert
  * @return The converted string
  */

 public static String getDateAndTimeString(LocalDateTime date) {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyyMMdd'T'hhmmss'Z'");
  return df.format(date);

 }



 /**
  * Returns only the year of the date as a String formatted like "yyyy"
  *
  * @param date The date to convert
  * @return The converted string
  */

 public static String getYearString(LocalDate date) {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy");
  return df.format(date);

 }



 /**
  * Returns only the month of the date as a String formatted like "MM"
  *
  * @param date The date to convert
  * @return The converted string
  */

 public static String getMonthString(LocalDate date) {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("MM");
  return df.format(date);

 }



 /**
  * Returns only the day of the date as a String formatted like "dd"
  *
  * @param date The date to convert
  * @return The converted string
  */

 public static String getDayString(LocalDate date) {

  DateTimeFormatter df = DateTimeFormatter.ofPattern("dd");
  return df.format(date);

 }



 /**
  * Adds an offset to a date.
  *
  * @param date The date.
  * @param years offset for year
  * @param months offset for month
  * @param days offset for day
  *
  * @return The given date plus offsets
  */

 public static LocalDate addOffset(LocalDate date, Integer years, Integer months, Integer days) {

  return
   date
    .plusYears(years)
    .plusMonths(months)
    .plusDays(days);

 }



 /**
  * Swaps lastname and firstname.
  *
  * <p>Works only for names with exactly one colon. Otherwise the unchanged string is returned.</p>
  *
  * @param name A name
  * @return The name with eventually swapped firstname and lastname.
  */

 public static String makeNameReadable(String name) {

  String[] parts = name.split(",");

  if (parts.length == 1  ||  parts.length > 2)  // Does not work for names with more than one colon.
   return name;

  return parts[1].trim() + " " + parts[0].trim();

 }



 /**
  * Calculates the difference of the years
  *
  * @param date1 Will be substracted from the second date.
  * @param date2 The second date

  * @return The difference
  */

 public static int yearDiff(LocalDate date1, LocalDate date2) {

  return date2.getYear() - date1.getYear();

 }




 private static double getDouble(Object o) {

  if (o instanceof Double)
   return ((Double) o).doubleValue();
  if (o instanceof Integer)
   return ((Integer)o).doubleValue();
  if (o instanceof Long)
   return ((Long)o).doubleValue();
  if (o instanceof String)
   return (Double.parseDouble(o.toString()));

  return 0.0;

 }



 /**
  * As the name says...
  *
  * @param o1 Divide this value
  * @param o2 with this value.
  *
  * @return The result or null in case of error.
  */

 public static Double divide(Object o1, Object o2) {

  try {
   return getDouble(o1) / getDouble(o2);
  }
  catch (Exception e) {
   return null;  // Don't make trouble.
  }

 }



 /**
  * As the name says...
  *
  * @param o1 Take this value
  * @param o2 and substract this value.
  *
  * @return The difference of the values.
  */

 public static Double substract(Object o1, Object o2) {

  return getDouble(o1) - getDouble(o2);

 }



 /**
  * As the name says...
  *
  * @param o1 Multiply this value
  * @param o2 with this value.
  *
  * @return The result.
  */

 public static Double multiply(Object o1, Object o2) {

  return getDouble(o1) * getDouble(o2);

 }



 public static int getDurationFromString(String str) {

  String[] p = str.split(":", 3);
  int[] factors = {1, 60, 60 * 60};

  int res = 0;

  int ifc = 0;
  for (int id = Math.min(p.length, 3) - 1; id >= 0; id --) {
   String o = p[id];
   if (!Utilities.isEmpty(o))
    res = res + Integer.parseInt(o) * factors[ifc];
   ifc ++;
  }

  return res;

 }



 /**
  * Checks if the given string is matched by the list of prefixes.
  *
  * @param prefixes The list of prefixes.
  * @param str The string to check.
  *
  * @return true if one of the prefixes matches the string. False otherwise.
  */

 public static boolean prefixListContains(List<String> prefixes, String str) {

  return
   prefixes.stream().anyMatch(str::startsWith);

 }



 /**
  * Splits a string in parts delimited by "&".
  *
  * @param issuers The list of issuers.
  *
  * @return  The list of issuers trimmed.
  */

 public static List<String> splitIssuers(String issuers) {

  return
   Arrays.stream(issuers.split("&"))
    .filter(i -> !Utilities.isEmpty(i))
    .map(String::trim)
    .collect(Collectors.toList());

 }



 /**
  * Find all classes in a package.
  *
  * @param packageName THe name of the package.
  * @param type return type.
  *
  * @return A stream of classes.
  *
  * @throws IOException If problems occur.
  */

 public static <T> Stream<Class<T>> findClassesInPackage(String packageName, Class<T> type) throws IOException {

  InputStream stream = Utilities.class.getClassLoader()
   .getResource(packageName.replaceAll("[.]", "/")).openStream();

  BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

  return reader.lines()
   .filter(line -> line.endsWith(".class"))
   .map(line -> getClass(line, packageName, type));

 }



 private static <T> Class<T> getClass(String className, String packageName, Class<T> type) {

  try {
   @SuppressWarnings("unchecked")
   Class<T> tClass = (Class<T>) Class.forName(packageName + "." + className.substring(0, className.lastIndexOf('.')));
   return
    tClass;
  }
  catch (ClassNotFoundException e) {
   throw new MCException(e);
  }

 }



 /**
  * Removes all accents from a string.
  *
  * @param str The string.
  * @return The "clean" string.
  */

 public static String removeAccents(String str) {

  return
   str == null ? null : Normalizer.normalize(str, Normalizer.Form.NFKD).replaceAll("\\p{M}", "");

 }



 /**
  * Splits a search string into tokens.
  *
  * @param searchString the value.
  * @return A list with string tokens.
  */

 public static List<String> splitSearchString(String searchString) {

  ArrayList<String> res = new ArrayList<>();

  StringTokenizer sn =
   new StringTokenizer(searchString, " ;,.%?*\t!-+&", false);

  while (sn.hasMoreTokens()) {
   String sd = sn.nextToken().toLowerCase(Locale.ROOT);

   // Strip common decoration.

   res.add(
    sd.replaceAll("^[\\\"'\\(]", "")
    .replaceAll("[\\\"'\\)]$", "")
   );
  }

  return
   res;

 }



 /** Just give back a null value for Velcity templates.
  *
  * @return A "real" null.
  */

 public Object getNull() {

  return null;

 }



 /**
  * Find a regex in a list of Strings.
  *
  * @param lst The list of Strings.
  * @param regex A regular expression
  *
  * @return true if the regex is anywhere in the list. False otherwise.
  */

 public static boolean containsMatch(List<String> lst, String regex) {

  return
   lst.parallelStream()
    .anyMatch(s -> s.matches(regex));

 }

}
