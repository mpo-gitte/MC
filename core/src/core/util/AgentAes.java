package core.util;



import core.exceptions.MCException;
import core.exceptions.SecretServiceException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Security;



/**
 * This class does encryption / decryption with AES.
 *
 * @version 2025-02-08
 * @author lp
 *
 */

public class AgentAes implements SecretService.IAgent {

 Logger logger = LogManager.getLogger(AgentAes.class);

 private SecretKeySpec key = null;



 /**
  * Decrypts the given string.
  *
  * @param what The String to decrypt.
  *
  * @return The decrypted string. Ready for use Base64 encoded.
  */

 @Override
 public String decrypt(String what) {

  logger.debug(() -> "decrypt(): " + what);

  try {
   return new String(getCipher(Cipher.DECRYPT_MODE).doFinal(Base64Coder.decode(what)));
  }
  catch (Exception e) {
   throw new SecretServiceException(e);
  }

 }



 /**
  * Encrypt the given string.
  *
  * @param what The String to encrypt.
  *
  * @return The encrypted string.
  */

 @Override
 public String encrypt(String what) {

  byte[] input = what.getBytes(StandardCharsets.UTF_8);

  try {
   Cipher cipher = getCipher(Cipher.ENCRYPT_MODE);

   byte[] cipherText = new byte[cipher.getOutputSize(input.length)];

   int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
   cipher.doFinal(cipherText, ctLength);

   return new String(Base64Coder.encode(cipherText));

  } catch (Exception e) {
   throw new SecretServiceException(e);
  }

 }



 private Cipher getCipher(int mode) {

  Cipher cipher;
  try {
   cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
   cipher.init(mode, key);
  }
  catch (Exception e) {
   throw new MCException(e);
  }

  return cipher;

 }



 /**
  * Initialize AES.
  *
  * @param password The password for encryption
  */

 @Override
 public void init(String password) {

  final int PASSWORD_LENGTH = 32;

  logger.debug("Setting up AES...");

  if (password == null)
   password = RandomStringUtils.secure().nextAlphanumeric(PASSWORD_LENGTH);
  else if (password.length() < PASSWORD_LENGTH)
   password =
    password.repeat((PASSWORD_LENGTH / password.length()) + 1);

  password = password.substring(0, PASSWORD_LENGTH);

  Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

  key =
   new SecretKeySpec(password.getBytes(StandardCharsets.UTF_8), "AES");

  logger.debug("... done.");

 }



 /**
  * Check is not implemented for AES.
  *
  * @param what The string to check
  * @param hashed The hash value to check against
  *
  * @return nothing
  */

 @Override
 public boolean check(String what, String hashed) {

  throw new SecurityException("Not implemented!");

 }
}
