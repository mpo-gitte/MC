package core.util;

import java.lang.annotation.*;


/**
 * Marks a variable to be configured with a heler.
 *
 * @version 2024-07-29
 * @author lp
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface WorkerHelper {

}
