package core.util;



import core.exceptions.MCException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


/**
 * The Configurator injects values into classes and objects.
 *
 * @version 2024-08-02
 * @author lp
 */

public class Configurator {

 private static Configurator me = null;

 private static Logger logger = LogManager.getLogger(Configurator.class);

 private ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle("Configurator");

 private List<ConfigValueSupplier> configValueSuppliers = new ArrayList<>();

 private HashSet<Class> configuredClasses = new HashSet<>();

 private List<String> packages = Utilities.getParamAsList(rb, "packages", null);


 /**
  * Get the one and only Configurator.
  *
  * @return The Configurator.
  */

 public static Configurator get() {

  synchronized (Configurator.class) {
   if (me == null)
    me = new Configurator();
  }

  return me;

 }



 private Configurator() {

  try {
   loadConfigs();
  }
  catch (Exception e) {
   throw new MCException(e);
  }

 }



 private void loadConfigs() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

  Map<Class, Config> instances =  new HashMap<>();

  for (String cln : Utilities.getParamAsList(rb, "configClasses", null)) {
   @SuppressWarnings("unchecked")
   Class<? extends Config> clazz = (Class<? extends Config>)Class.forName(cln.trim());

   logger.debug(() -> "loading config class: \"" + clazz.getName() + "\"");

   for (Method method : clazz.getMethods()) {
    ConfigValue cv = method.getAnnotation(ConfigValue.class);
    if (cv != null) {
     logger.debug(() -> "using method: \"" + method.getName() + "\"");

     Config inst = instances.get(clazz);

     if (inst == null) {  // New Config?
      configure(clazz);
      inst = clazz.getConstructor().newInstance();
      configure(inst);
      inst.onLoad();
      instances.put(clazz, inst);
     }

     configValueSuppliers.add(new ConfigValueSupplier(inst, method, cv.matchedBy(), cv.value()));

    }

   }
  }

 }



 /**
  * Configure static (non final) fields in the given class.
  *
  * @param clazz The class to configure.
  */

 synchronized public void configure(Class clazz) {

  Class cl = clazz;

  while (continueWith(cl)) {
   if (configuredClasses.contains(cl))
    return;  // Ready!

   configureClass(cl);

   configuredClasses.add(cl);

   cl = cl.getSuperclass();
  }

 }



 /**
  * Returns a list of ConfigValueSuppliers.
  *
  * @param clazz The class in which the method is defined.
  * @param method The method.
  * @return A List with option ConfigValueSuppliers.
  */

 public List<Optional<ConfigValueSupplier>> getConfigValueSuppliers(Class clazz, Method method) {

  AtomicInteger id = new AtomicInteger();

  return Arrays.stream(method.getParameterTypes())
   .map(pt -> getConfigValueSupplier(new ConfigurableParam(clazz, method, id.getAndIncrement())))
   .collect(Collectors.toList());

 }



 private boolean continueWith(Class cl) {

  return
   packages.contains(cl.getPackageName());

 }



 private void configureClass(Class clazz) {

  Arrays.stream(clazz.getDeclaredFields())
   .map((Field f) -> new ConfigurableField(clazz, f))
   .filter(ConfigurableField::isNotFinal)  // Keeping finals untouched!
   .filter(ConfigurableField::isStatic)
   .forEach(this::configureField);

 }



 /**
  * Configure all (non final) fields in given object.
  *
  * @param obj The object to configure.
  */

 public void configure(Object obj) {

  Class cl = obj.getClass();

  while (continueWith(cl)) {

   configureObject(obj, cl);

   cl = cl.getSuperclass();
  }

 }



 private void configureObject(Object obj, Class clazz) {

  Arrays.stream(clazz.getDeclaredFields())
   .map((Field f) -> new ConfigurableField(obj, f))
   .filter(ConfigurableField::isNotFinal)  // Keeping finals untouched!
   .filter((ConfigurableField cf) -> !cf.isStatic())
   .forEach(this::configureField);

 }



 private void configureField(ConfigurableField field) {

  logger.debug(() -> "Configuring field: \"" + field.getFieldSignature() + "\"");

  configValueSuppliers.stream()
   .filter((ConfigValueSupplier cs) -> cs.matches(field))
   .map((ConfigValueSupplier cs) -> cs.getValue(field))
   .forEach(field::setFieldValue);

 }



 private Optional<ConfigValueSupplier> getConfigValueSupplier(ConfigurableField field) {

  logger.debug(() -> "getConfigValueSuppier(): \"" + field.getFieldSignature() + "\"");

  return Optional.ofNullable(configValueSuppliers.stream()
   .filter((ConfigValueSupplier cs) -> cs.matches(field))
   .findFirst().orElse(null));

 }



 /**
  * Holds information about a field which is configurable.
  */

 public static class ConfigurableField {

  private Field field;
  protected Class clazz;
  private Object object;



  private ConfigurableField(Class clazz, Field field) {

   this.field = field;
   this.clazz = clazz;
   this.object = null;

  }



  private ConfigurableField(Object object, Field field) {

   this.field = field;
   this.clazz = object.getClass();
   this.object = object;

  }



  /**
   * Gets the underlying "real" reflection field.
   *
   * <p>Used to fetch annotations. Don't set the value of the field!</p>
   *
   * @return The field.
   */

  public Field getField() {

   return field;

  }



  /**
   * Get the annotations of the field.
   *
   * @return An array filled with annotations. May be empty.
   */

  public Annotation[] getAnnotations() {

   return field.getDeclaredAnnotations();

  }



  /**
   * Gets the defining class for the field.
   *
   * @return The class.
   */

  public Class getContainingClass() {

   return clazz;

  }


  /**
   * Gets the fields signature.
   *
   * @return The signature string.
   */

  String getFieldSignature() {

   return
    field.getType() +
    " " +
    clazz.getName() +
    "." +
    field.getName();

  }



  /**
   * Checks if the field is static.
   *
   * @return true if static. false otherwise.
   */

  boolean isStatic() {

   return Modifier.isStatic(field.getModifiers());

  }



  /**
   * Checks if the field is not final.
   *
   * @return true if not final. false otherwise.
   */

  boolean isNotFinal() {

   return !Modifier.isFinal(field.getModifiers());

  }



  private void setFieldValue(Object obj) {

   boolean accessible = field.canAccess(object);  // object may be null for static fields in classes.

   field.setAccessible(true);

   try {
    if (obj != null) {
     logger.debug(() -> "Setting field \"" + getFieldSignature() + "\"");
     field.set(object, obj);
    }
   } catch (IllegalAccessException e) {
    logger.error("Can't set value of field \"" + getFieldSignature() + "\"!");
   }

   field.setAccessible(accessible);
  }

 }



 /**
  * Holds information about a parameter for a method.
  */

 public static class ConfigurableParam extends ConfigurableField {

  private final int fieldNo;
  private final Class<?> paramClass;
  private final Method method;



  public ConfigurableParam(Class clazz, Method method, int fieldNo) {

   super(clazz, null);

   this.fieldNo = fieldNo;
   this.paramClass = method.getParameterTypes()[fieldNo];
   this.method = method;

  }



  @Override
  String getFieldSignature() {

   return
    method.getReturnType().getName() +
    " " +
    clazz.getName() +
     "." +
    method.getName() +
    " " +
    paramClass.getName() +
    " " +
    "param[" + fieldNo + "]";

  }



  @Override
  public Annotation[] getAnnotations() {

   return method.getParameterAnnotations()[fieldNo];

  }

 }



 /**
  * Enum specifies the match type for ConfigValues to fields.
  */

 public enum FieldSelect {



  /**
   * Check against the field name.
   *
   * <p>value is a regex for the fields signature.</p>
   */

  FIELD_HAS_NAME {
   @Override
   public boolean matches(ConfigurableField field, String matchFor) {

    return field.getFieldSignature().matches(matchFor);

   }

  },



  /**
   * Check if field has an annotation.
   *
   * <p>value is a regex for the name of the annotation.</p>
   */

  FIELD_HAS_ANNOTATION {
   @Override
   public boolean matches(ConfigurableField field, String matchFor) {

    for (Annotation a : field.getAnnotations())
     if (a.annotationType().getName().matches(matchFor))
      return true;  // The field has an annotation matching the given name.

    return false;

   }

  };



  /**
   * Method checks if the config value matches the joint point.
   *
   * @param field    The field point.
   * @param matchFor The value form the aspect annotation.
   *
   * @return True if matches. False otherwise.
   */

  public abstract boolean matches(ConfigurableField field, String matchFor);

 }



 /**
  * This interface marks a class as config.
  */

 public interface Config {



  /**
   * When a config value class gets loaded the Configurator will call this method.
   *
   * <p>The default implementation is empty.</p>
   */

  default public void onLoad() {

   // Empty by default.

  };

 }



 /**
  * Annotation for defining a method as a ConfigValue.
  */

 @Documented
 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface ConfigValue {



  /**
   * Matching method.

   * @return The matching method.
   */

  FieldSelect matchedBy();



  /**
   * The value for matching.
   *
   * @return The value.
   */

  String value() default "";

 }



 /**
  * An Object supplying a Config value.
  */

 public static class ConfigValueSupplier {

  private Config inst;
  private Method method;
  private FieldSelect fieldSelect;
  private String value;



  ConfigValueSupplier(Config inst, Method method, FieldSelect fieldSelect, String value) {

   this.inst = inst;
   this.method = method;
   this.fieldSelect = fieldSelect;
   this.value = value;

  }



  boolean matches(ConfigurableField field) {

   return fieldSelect.matches(field, value);

  }



  public Object getValue(ConfigurableField field) {

   try {
    return method.invoke(inst, field);
   }
   catch (Exception e) {
    Throwable ex = Utilities.getRealProblem(e);
    logger.error("Can't get value for field \"" + field.getFieldSignature() + "\": " + ex);
    throw new MCException(ex);
   }

  }



  public FieldSelect getFieldSelect() {

   return this.fieldSelect;

  };

 }



 /**
  * Declares the context of  a worker helper.
  *
  * @version 2024-07-29
  * @author lp
  *
  */

 @Documented
 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.TYPE})
 public static @interface Context {

  Class<? extends Object> value();

 }

}
