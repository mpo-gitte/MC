package core.util;

import core.exceptions.MCException;

import java.util.HashMap;



/**
 * Manager for objects.
 *
 * @version 2024-08-03
 * @author lp
 */

public class ObjectManager {

 private static ObjectManager me = null;

 private static final HashMap<String, IObject> objects = new HashMap<>();



 /**
  * Get the one and only ObjectManager.
  *
  * @return The ObjectManager.
  */

 public static ObjectManager get() {

  synchronized (ObjectManager.class) {
   if (me == null)
    me = new ObjectManager();
  }

  return me;

 }



 /**
  * Creates, configures and initializes an object.
  *
  * @param objectClass The class for the object.
  *
  * @return The object.
  */

 public synchronized IObject getObject(Class<? extends IObject> objectClass) {

  String objectClassName = objectClass.getName();

  IObject obj;

  if (objects.containsKey(objectClassName))
   obj = objects.get(objectClassName);  // Helper already known.
  else {
   Configurator.get().configure(objectClass);

   try {
    obj = objectClass.getConstructor().newInstance();
   }
   catch (Exception e) {
    throw new MCException(e);
   }

   Configurator.get().configure(obj);

   obj.onLoad();  // Some IObjects need initialization.

   objects.put(objectClassName, obj);
  }

  return
   obj;

 }



 /**
  * Interface for objects.
  */

 public interface IObject {

  /**
   * When an object gets loaded the ObjectManager will call this method.
   *
   * <p>The default implementation is empty.</p>
   *
   * @see ObjectManager
   *
   */

  default public void onLoad() {

   // Nothing to do here!

  }

 }

}
