package core.util;

import java.lang.annotation.*;


/**
 * Marks a Method parameter to be filled with the Request
 *
 * @version 2021-04-18
 * @author pilgrim.lutz@imail.de
 *
 * @see core.configs.ConfigRequest
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface RequestConfig {

}
