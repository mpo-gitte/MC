package core.util;

import java.util.ArrayList;



/**
 * The FillingArrayList adds a method for setting a list item behind the lists end.
 *
 * @author lutz
 * @version 2018-02-18
 *
 */

public class FillingArrayList<T> extends ArrayList<T> {

 protected int seekPos = -1;



 /**
  * This method sets a list item.
  *
  * <p>If the index is larger then the size of the list it will be filled with null values.</p>
  *
  * @param ix The index
  * @param o The item
  */

 public void setFilled(int ix, T o) {

  if (ix >= size()) {
   for (int id = ix - size(); id > 0; id --)  // fill the list.
    add(null);
   add(o);
  }
  else
   set(ix, o);

 }



 /**
  * Set the position for the next put.
  *
  * @param seekPos The position.
  */

 public void seek(int seekPos) {

  this.seekPos = seekPos;

  if (seekPos > size())
   setFilled(seekPos - 1, null);

 }



 /**
  * Puts an item to the position set by seek()
  *
  * @param item
  */

 public void put(T item) {

  if (seekPos == size())
   add(item);
  else
   set(seekPos, item);

  ++ seekPos;

 }

}
