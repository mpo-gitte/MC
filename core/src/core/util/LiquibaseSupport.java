package core.util;


import core.exceptions.MCException;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.Resource;

import java.io.IOException;
import java.util.List;


/**
 * Support for Liquibase during start up.
 *
 * @version 2025-02-12
 * @author lp
 */

public class LiquibaseSupport {



 /**
  * Start Liquibase to update database.
  *
  * @param db MC's Database.
  * @param path Path to changelog.xml.
  */

 public static void init(DB db, String path) {

  DBConnection c = db.getConnection();

  try {
   Database database =
    DatabaseFactory.getInstance()
     .findCorrectDatabaseImplementation(new JdbcConnection(c.getConnection()));

   Liquibase liquibase = new Liquibase("changelog.xml", new ResourceAccessor(path), database);

   liquibase.update(new Contexts(), new LabelExpression());
  }
  catch (LiquibaseException e) {
   throw new MCException(e);
  }
  finally {
   db.releaseConnection(c);
  }

 }



 private static class ResourceAccessor extends ClassLoaderResourceAccessor {

  private String pathPrefix;



  public ResourceAccessor(String pathPrefix) {

   this.pathPrefix = pathPrefix;

  }



 /*
  * Prepare access to changelog.xml
  *
  * @param path Liquibases path to changelog.xml.
  *
  * @return MC's complete path to changlog.xml.
  */

  @Override
  public List<Resource> getAll(String path) throws IOException {

   return
    super.getAll(pathPrefix + path);

  }

 }

}
