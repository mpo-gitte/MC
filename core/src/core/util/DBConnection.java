package core.util;



import core.exceptions.DBException;
import core.exceptions.MCException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;



/**
 * This class is wrapper around java.sql.Connection.
 *
 * @version 2024-05-16
 * @author lp
 *
 */

public class DBConnection {

 private final Connection con;
 private boolean isInUse;
 private boolean closeTag;
 private int useCount;
 private LocalDateTime lastUse;
 private final LocalDateTime creation;
 private boolean transactionOn;
 private final HashMap<String, PreparedStatement> stmts;
 private static final Logger logger = LogManager.getLogger(DBConnection.class);



 /**
  * Standard constructor.
  *
  * @param con The JDBC connection.
  */

 public DBConnection(Connection con) {

  this.con = con;
  isInUse = false;
  creation = LocalDateTime.now();
  transactionOn = false;
  stmts = new HashMap<>();

 }



 /**
  * Creates a prepared Statement for this connection.
  * @param sql An SQL statement.
  * @return a java.sql.PreparedStatement
  */

 public synchronized PreparedStatement prepareStatement(String sql) {

  PreparedStatement stmt = stmts.get(sql);

  if (stmt == null) {
   try {
    stmt = con.prepareStatement(sql);
   }
   catch (SQLException e) {
    throw new DBException(e);
   }
   stmts.put(sql, stmt);
  }

  return stmt;

 }



 /**
  * Checks if this DBConnection is in use.
  * @return true if this DBConnection is in use. False otherwise.
  */

 public boolean isInUse() {

  return isInUse;

 }



 /**
  * Sets all internal data which makes up a DBConnection in use.
  *
  * <p>MC relies on AutoCommit! Connections fetched with this method have
  * setAutoCommit(true) unless a transaction will be started.</p>
  *
  * @return the DBConnection itself for convenience.
  */

 public DBConnection use() {

  ++ useCount;
  lastUse = LocalDateTime.now();
  isInUse = true;
  closeTag= false;

  try {
   con.setAutoCommit(true);
  }
  catch (SQLException e) {
   throw new MCException(e);
  }

  return this;

 }



 /**
  * Marks a connection as not in use.
  */

 public void unUse() {

  isInUse = false;

 }



 /**
  * Returns the creation date.
  *
  * @return the creation date
  */

 public LocalDateTime getCreation() {

  return creation;

 }



 /**
  * Returns the last usage.
  *
  * @return the date of the last usage
  */

 public LocalDateTime getLastUse() {

  return lastUse;

 }



 /**
  * Returns the usage count.
  *
  * @return the usage count
  */

 public Integer getUsageCount() {

  return useCount;

 }



 /**
  * Returns the close tag.
  *
  * @return the state of the close tag.
  */

 public boolean getCloseTag() {

  return closeTag;

 }



 /**
  * Marks the connection for closing.
  *
  */

 public void setCloseTag() {

  closeTag = true;

 }


 /**
  * Closes the connection.
  *
  */

 public void close() {

  try {
   con.close();
  }
  catch (Exception e) {
   logger.error("close(): Error on closing connection: " + e);
   // Intended to bypass all exception handling. So MCs connection pool lets
   // this connection "in use". This should be visible in monitoring and
   // should lead to some action in the health endpoint of the application.
   throw new Error(e);
  }

 }



 /**
  * Starts a transaction.
  */

 public void beginTransaction() {

  if (!transactionOn) {
   try {
    con.setAutoCommit(false);
   }
   catch (SQLException e) {
    throw new DBException(e);
   }
   transactionOn = true;
  }

 }



 /**
  * Commits a transaction.
  */

 public void commit() {

  if (transactionOn) {
   try {
    con.commit();
   }
   catch (SQLException e) {
    throw new DBException(e);
   }
  }

  transactionOn = false;

 }



 /**
  * Rolls back a transaction.
  */

 public void rollback() {

  if (transactionOn) {
   try {
    con.rollback();
   }
   catch (SQLException e) {
    throw new MCException(e);
   }
  }

  transactionOn = false;

 }



 /**
  * Returns the underlying "real" Connection.
  *
  * <p>MC relies on AutoCommit! Connections fetched with this method have
  * setAutoCommit(true) unless a transaction will be started.</p>
  *
  * @return The Connection
  */

 public Connection getConnection() {

  try {
   con.setAutoCommit(true);
  }
  catch (SQLException e) {
   throw new MCException(e);
  }

  return con;

 }

}
