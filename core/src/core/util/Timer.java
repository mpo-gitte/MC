package core.util;

import core.base.MeterMaid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;


/**
 * The Timer schedules jobs.
 *
 * @version 2024-05-07
 * @author pilgrim.lutz@imail.de
 */

public class Timer {

 private static Timer me = null;

 private static final Properties properties = new Properties();
 static {
  properties.put("org.quartz.jobStore.isClustered", "true");
  properties.put("org.quartz.scheduler.instanceId", "AUTO");
  properties.put("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
  properties.put("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.PostgreSQLDelegate");
  properties.put("org.quartz.jobStore.dataSource", "quartzDataSource");
  properties.put("org.quartz.jobStore.tablePrefix", "QRTZ_");
  properties.put("org.quartz.threadPool.threadCount", "5");
  properties.put("org.quartz.dataSource.quartzDataSource.connectionProvider.class", "core.util.TimerConnectionProvider");
 }

 private final SchedulerFactory schedFacCluster = new StdSchedulerFactory(properties);
 private final SchedulerFactory schedFac = new StdSchedulerFactory();

 private static final Logger logger = LogManager.getLogger(Timer.class);
 private static final Map<String, Runnable> actions = new HashMap<>();

 private static boolean clusteringEnabled = false;

 private static final MeterMaid lovelyRita = MeterMaid.get();



 /**
  * Get the one and only Timer.
  *
  * @return The Configurator.
  */

 public static Timer get() {

  synchronized (Timer.class) {
   if (me == null) {
    try {
     me = new Timer();
    } catch (SchedulerException e) {
     logger.error("Can't create Timer!");
    }
   }
  }

  return me;

 }



 private Timer() throws SchedulerException {

  ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle("Timer");

  clusteringEnabled = Utilities.getParam(rb, "clustering", false);

 }



 /**
  * Run a Timer.
  *
  * @param jobName The Name of timer job.
  * @param jobSchedule The schedule plan.
  * @param clusterJob True for clustering.
  * @param callback A callback with the action.
  */

 public void run(String jobName, String jobSchedule, boolean clusterJob, Runnable callback) {

  logger.info("run(): \"" + jobName + "\" at \"" + jobSchedule + "\"");

  actions.put(jobName, callback);

  try {
   Scheduler sched;
   if (clusterJob && clusteringEnabled) {
    sched = schedFacCluster.getScheduler();
    logger.info("run(): Using clustering scheduler.");
   }
   else {
    sched = schedFac.getScheduler();
    logger.info("run(): Not using clustering scheduler.");
   }

   JobDetail job = JobBuilder.newJob(ActionJob.class)
    .withIdentity(jobName, "group1")
    .build();

   Trigger trigger;

   if (jobSchedule.startsWith("!!")) {
    trigger = TriggerBuilder.newTrigger()
     .withIdentity("trigger_" + jobName, "group2")
     .withSchedule(SimpleScheduleBuilder.repeatSecondlyForTotalCount(1))
     .build();
   }
   else
    trigger = TriggerBuilder.newTrigger()
     .withIdentity("trigger_" + jobName, "group1")
     .withSchedule(CronScheduleBuilder.cronSchedule(jobSchedule))
     .build();

   sched.deleteJob(job.getKey());  // Maybe this job was formerly run.

   sched.scheduleJob(job, trigger);
   sched.start();
  }
  catch (SchedulerException e) {
   logger.error("run(): Can't start timer: \"" + jobName + "!\"\n" + e);
  }

 }



 /**
  * Quartz job.
  */

 @DisallowConcurrentExecution
 public static class ActionJob implements Job {

  public void execute(JobExecutionContext context) throws JobExecutionException {

   String jobName = context.getJobDetail().getKey().getName();

   logger.info("execute(): \"" + jobName + "\"");

   long start = System.nanoTime();

   actions.get(jobName).run();

   // Record elapsed time.
   lovelyRita.recordElapsedTime("timer", "job", jobName, start);

  }

 }

}
