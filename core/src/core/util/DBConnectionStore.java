package core.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



/**
 * Connections per Thread.
 *
 * @version 2025-01-03
 * @author lp
 *
 */

public class DBConnectionStore {

 private static final Logger logger = LogManager.getLogger(DBConnectionStore.class);

 private static final ThreadLocal<DBConnectionStore> connections = new ThreadLocal<>() {
  protected synchronized DBConnectionStore initialValue() {
   return null;
  }
 };

 public DBConnection con;
 public int useCount;



 private DBConnectionStore(DBConnection con, int useCount) {

  this.con = con;
  this.useCount = useCount;

 }



 /**
  * Gets a DB connection for the current threat.
  *
  * <p>Subsequent calls within the same threat will always return the same connection!</p>
  *
  * @return The DB connection.
  */

 public static DBConnection getConnection() {

  DBConnectionStore con = connections.get();

  if (con == null) {
   logger.debug(() -> "getConnection(): New connection!");

   con = new DBConnectionStore(DB.get().getConnection(), 0);
   con.con.beginTransaction();
   connections.set(con);
  }
  else
   logger.debug(() -> "getConnection(): Used connection!");

  ++ con.useCount;

  return con.con;

 }



 /**
  * Releases a DB connection for a threat.
  *
  * @param immediately if set to true the DB connection will be closed immediately.
  */

 public static void releaseConnection(boolean immediately) {

  DBConnectionStore con = connections.get();

  if (immediately)
   con.useCount = 0;
  else
   -- con.useCount;

  if (con.useCount == 0) {
   logger.debug(() -> "releaseConnection(): Really release connection!");

   con.con.commit();
   DB.get().releaseConnection(con.con);
   connections.remove();
  }
  else
   logger.debug(() -> "releaseConnection(): omit releasing connection!");

 }



 /**
  * Releases a DB connection for a threat.
  */

 public static void releaseConnection() {

  releaseConnection(false);

 }



 /**
  * Rollbacks the transaction for the current DB connection in the current threat.
  */

 public static void rollback() {

  DBConnectionStore con = connections.get();

  if (con != null) {
   logger.error("rollback(): rollback!");

   con.con.rollback();
   DB.get().releaseConnection(con.con);
   connections.remove();
  }

 }

}
