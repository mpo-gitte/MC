package core.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;



/**
 * This class merges a PropertyResourceBundle from several sources.
 *
 * <p>The path where to find the resource bundles is specified in MergedPropertyResourceBundle.properties.</p>
 *
 * @version 2020-09-06
 * @author pilgrim.lutz@imail.de
 *
 */

public class MergedPropertyResourceBundle extends ResourceBundle implements Serializable {

 private static final Logger logger = LogManager.getLogger(MergedPropertyResourceBundle.class);

 private static final String bn =
  Utilities.getSystemProperty("MergedPropertyResourceBundle.bundleName", "MergedPropertyResourceBundle");

 private static final ResourceBundle rb = PropertyResourceBundle.getBundle(bn);
 private static final List<String> packages = Utilities.split(rb.getString("BundlePath"));
 private final Map<String, String> bundle = new HashMap<>();
 private final static Map<String, ResourceBundle> propBundles = new HashMap<>();



 /**
  * Gets a ResourceBundle with the given name.
  *
  * @param baseName The name
  *
  * @return A resourceBundle
  */

 public static ResourceBundle getMergedBundle(String baseName) {

  return getMergedBundle(baseName, Locale.getDefault());

 }



 /**
  * Gets a ResourceBundle with the given name.
  *
  * @param baseName The name
  * @param locale A locale
  *
  * @return A resourceBundle
  */

 public static synchronized ResourceBundle getMergedBundle(String baseName, Locale locale) {

  String key = baseName + "-" + locale;
  ResourceBundle rb;

  if (propBundles.containsKey(key))
   rb = propBundles.get(key);
  else {
   rb = new MergedPropertyResourceBundle(baseName, locale);
   propBundles.put(key, rb);
  }

  return rb;

 }



 /**
  *
  * <p>Merging means: The constructor reads all property files along the path. Properties found in one
  * file won't be overwritten by an entry with the same name in file parsed later.</p>
  *
  * @param baseName The name for the resource bundle.
  * @param locale The locale.
  *
  */

 private MergedPropertyResourceBundle(String baseName, Locale locale) {

  logger.debug(() -> "MergedPropertyResourceBundle(): \"" + baseName + "\"");

  for (String pack : packages) {
   String bun = pack + "." + baseName;

   logger.debug(() -> "MergedPropertyResourceBundle(): trying \"" + bun + "\"");

   ResourceBundle rb = null;
   try {
    if (pack.startsWith("ENV:"))
     rb = readBundleFromEnvironment(pack, baseName, locale);
    else
     rb = PropertyResourceBundle.getBundle(bun, locale);
   } catch (Exception e) { /* Does not bother us. Just go ahead and try next. */ }

   if (rb != null) {

    for (String key : rb.keySet())
     if (!bundle.containsKey(key)) {
      logger.debug(() -> "MergedPropertyResourceBundle(): picking key \"" + key + "\"");
      bundle.put(key, rb.getString(key));
     }
   }
  }
 }



 private ResourceBundle readBundleFromEnvironment(String pack, String basename, Locale locale) {

  String en = pack.split(":")[1] + "_" + basename;

  Map<String, String> env = System.getenv();

  Map<String, String> rb = env.keySet().stream()
   .filter(key -> key.startsWith(en))
   .collect(Collectors.toMap(key -> fitPropertyName(en, key), key -> System.getenv().get(key)));

  if (rb.isEmpty())
   return null;

  return new ResourceBundle() {
   @Override
   protected Object handleGetObject(String s) {

    return rb.get(s);

   }



   @Override
   public Enumeration<String> getKeys() {

    return Collections.enumeration(rb.keySet());

   }

  };

 }



 private String fitPropertyName(String prefix, String rawName) {

  return
   rawName.substring(prefix.length() + 1).replace('_', '.');

 }



 @Override
 protected Object handleGetObject(String key) {

  return bundle.get(key);

  }



 @Override
 public Enumeration<String> getKeys() {

  return Collections.enumeration(bundle.keySet());

 }

}
