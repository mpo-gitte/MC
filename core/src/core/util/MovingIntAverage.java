package core.util;



import java.util.Arrays;



/**
 * Class for calculating a moving average.
 *
 * @version 2011-11-25
 * @author lp
 */

public class MovingIntAverage {

 private final int size;
 private int total;
 private int index = 0;
 private final int[] samples;



 /**
  * Standard constructor.
  *
  * @param size Number of samples for calculation.
  * @param initValue Initial value for samples.
  */

 public MovingIntAverage(int size, int initValue) {

  this.size = size;

  samples = new int[size];

  total = initValue * size;

  Arrays.fill(samples, initValue);

 }



 /**
  * Put a new value to the samples.
  *
  * @param newValue The new value.
  */

 public void add(int newValue) {

  total -= samples[index];

  samples[index] = newValue;

  total += newValue;

  if (++index == size) index = 0;

 }



 /**
  * Gets the average.
  *
  * @return The average.
  */

 public int getAverage() {

  return total / size;

 }

}
