package core.util;

import core.exceptions.SecretServiceException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.crypto.generators.OpenBSDBCrypt;

import java.nio.charset.StandardCharsets;



/**
 * This class calculate a Bcrypt hash.
 *
 * @version 2015-02-08
 * @author pilgrim.lutz@imail.de
 *
 */

public class AgentBCrypt implements SecretService.IAgent {

 private Logger logger = LogManager.getLogger(AgentBCrypt.class);

 private String version;
 private int costFactor;



 /**
  * Decrypts the given string.
  *
  * @param what The String to decrypt.
  *
  * @return The decrypted string. Ready for use Base64 encoded.
  */

 @Override
 public String decrypt(String what) {

   // Hashes can't be recalculated (Hope this is true! :-)

   throw new SecretServiceException("Not possible!");

 }



 /**
  * Encrypt the given string.
  *
  * @param what The String to encrypt.
  *
  * @return The encrypted string.
  */

 @Override
 public String encrypt(String what) {

  return
   OpenBSDBCrypt.generate(
    version,
    what.toCharArray(),
    RandomStringUtils.secure().nextAlphanumeric(16).getBytes(StandardCharsets.UTF_8),
    costFactor
   );

 }



 /**
  * Misuses the password parameter for version and cost factor.
  *
  * @param password version and cost factor as a string in the form "vv-cf". Example: "2b-4".
  */

 @Override
 public void init(String password) {

  logger.debug(() -> "Setting up BCrypt.");

  String[] ps = password.split("-");

  version = ps[0];
  costFactor = Integer.parseInt(ps[1]);

 }



 /**
  * Checks is the given string leads to given hash.
  *
  * @param what The string to check
  * @param hashed The hash value to check against
  *
  * @return True if so. False if not.
  */

 @Override
 public boolean check(String what, String hashed) {

  return OpenBSDBCrypt.checkPassword(hashed, what.toCharArray());

 }
}
