package core.util;

import java.lang.annotation.*;


/**
 * Marks a Method parameter to be filled with the Locale
 *
 * @version 2022-01-05
 * @author pilgrim.lutz@imail.de
 *
 * @see core.configs.ConfigRequest
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface LocaleConfig {

}
