package core.util;

import java.lang.annotation.*;


/**
 * Marks a Method parameter to be filled with the ViewContext
 *
 * @version 2022-01-00
 * @author pilgrim.lutz@imail.de
 *
 * @see core.configs.ConfigRequest
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface ViewContextConfig {

}
