package core.util;



import core.exceptions.MCException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;



/**
 * Aspectator manages aspects for workers.
 *
 * @version 2024-08-02
 * @author lp
 */

public class Aspectator {

 private static final Logger logger = LogManager.getLogger(Aspectator.class.getName());

 private static Aspectator me = null;

 private final ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle("Aspectator");

 protected final List<Class<? extends Aspect>> aspectClasses = new ArrayList<>();

 private final Map<Method, Link> mesh = new HashMap<>();



 protected Aspectator() {

  try {
   loadAspects();
  } catch (ClassNotFoundException e) {
   throw new MCException(e);
  }

 }



 protected void loadAspects() throws ClassNotFoundException {

  logger.debug(() -> "loadAspects():");

  for (String cln : Utilities.getParamAsList(rb, "aspectClasses", new String[] {})) {
   @SuppressWarnings("unchecked")
   Class<? extends Aspect> clazz = (Class<? extends Aspect>)Class.forName(cln.trim());

   logger.debug(() -> "loadAspects(): Loading aspect class: \"" + clazz.getName() + "\"");

   aspectClasses.add(clazz);

  }

 }



 /**
  * Gets the one and only Aspectator.
  *
  * @return The instance of the Aspectator
  */

 public static Aspectator get() {

  synchronized(Aspectator.class) {
   if (me == null)
    me = new Aspectator();
  }

  return me;

 }



 /**
  * Calls a method and handles all Aspects for that method.
  *
  * @param inst The instance of an object.
  * @param methodInInterface The definition of method to call in the interface.
  * @param method The method to call.
  * @param args The arguments for this method.
  */

 public Object run(Object inst, Method methodInInterface, Method method, Object[] args) throws Exception  {

  logger.debug(() -> "run(): \"" + inst + "." + method.getName() + "\"");

  Object res;

  try {
   checkBeforeAspects(inst, methodInInterface, method, args);

   res = checkAroundAspects(inst, methodInInterface, method, args);

   checkAfterReturnAspects(inst, methodInInterface, method, args, res);

  }
  catch (Exception ex) {
   checkAfterThrownAspects(inst, methodInInterface, method, args, ex);

   throw ex;
  }
  finally {
   checkAfterFinalAspects(inst, methodInInterface, method, args);
  }

  logger.debug(() -> "run(): Returning result for \"" + inst + "." + method.getName() + "\"");

  return res;

 }



 /**
  * Handles all the "Before" Aspects for a method to call.
  *
  * @param inst The instance of an object.
  * @param methodInInterface The method definition in the interface.
  * @param method The method which will be called later.
  * @param args The arguments for this method.
  *
  * @throws InvocationTargetException Something went seriously wrong.
  * @throws IllegalAccessException Something went seriously wrong.
  * @throws InstantiationException Something went seriously wrong.
  */

 public void checkBeforeAspects(Object inst, Method methodInInterface, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {

  logger.debug(() -> "checkBeforeAspects for \"" + inst.getClass().getName() + "." + method.getName() + "\"");

  JoinPoint joinPoint = new JoinPoint(inst, methodInInterface,  method, args);

  for (Advice a : getInvocation(joinPoint).beforeAdvices)
   a.method.invoke(a.obj, joinPoint);

 }



 /**
  * Handles all the "After" Aspects for a method which was called before.
  *
  * @param inst The instance of an object.
  * @param methodInInterface The method definition in the interface.
  * @param method The method which was called before.
  * @param args The arguments for this method.
  *
  * @throws InvocationTargetException Something went seriously wrong.
  * @throws IllegalAccessException Something went seriously wrong.
  * @throws InstantiationException Something went seriously wrong.
  */

 public void checkAfterFinalAspects(Object inst, Method methodInInterface, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {

  logger.debug(() -> "checkAfterFinalAspects for \"" + inst.getClass().getName() + "." + method.getName() + "\"");

  JoinPoint joinPoint = new JoinPoint(inst, methodInInterface, method, args);

  for (Advice a : getInvocation(joinPoint).afterFinalAdvices)
   a.method.invoke(a.obj, joinPoint);

 }



 /**
  * Handles all the "AfterReturn" Aspects for a method which was called before.
  *
  * @param inst The instance of an object.
  * @param methodInInterface The method definition in the interface.
  * @param method The method which was called before.
  * @param args The arguments for this method.
  * @param retVal The return value from this method.
  *
  * @throws InvocationTargetException Something went seriously wrong.
  * @throws IllegalAccessException Something went seriously wrong.
  * @throws InstantiationException Something went seriously wrong.
  */

 public void checkAfterReturnAspects(Object inst, Method methodInInterface, Method method, Object[] args, Object retVal) throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {

  logger.debug(() -> "checkAfterReturnAspects for \"" + inst.getClass().getName() + "." + method.getName() + "\"");

  JoinPoint joinPoint = new JoinPoint(inst, methodInInterface, method, args, retVal);

  for (Advice a : getInvocation(joinPoint).afterReturnAdvices)
   a.method.invoke(a.obj, joinPoint);

 }



 /**
  * Handles all the "AfterReturn" Aspects for a method which was called before.
  *
  * @param inst The instance of an object.
  * @param methodInInterface The method definition in the interface.
  * @param method The method which was called before.
  * @param args The arguments for this method.
  * @param thrown The Throwable raised by this method.
  *
  * @throws InvocationTargetException Something went seriously wrong.
  * @throws IllegalAccessException Something went seriously wrong.
  * @throws InstantiationException Something went seriously wrong.
  */

 public void checkAfterThrownAspects(Object inst, Method methodInInterface, Method method, Object[] args, Throwable thrown) throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {

  logger.debug(() -> "checkAfterThrownAspects for \"" + inst.getClass().getName() + "." + method.getName() + "\"");

  JoinPoint joinPoint = new JoinPoint(inst, methodInInterface, method, args, Utilities.getRealProblem(thrown));

  Class<? extends Throwable> tcl = Utilities.getRealProblem(thrown).getClass();

  for (Advice a : getInvocation(joinPoint).afterThrownAdvices)
   if (Utilities.isClassDerivedFrom(tcl, a.thrown))
    a.method.invoke(a.obj, joinPoint);

 }



 /**
  * Handles all the "Around" Aspects for a method to call.
  *
  * @param inst The instance of an object.
  * @param methodInInterface The method definition in the interface.
  * @param method The method which will be called later.
  * @param args The arguments for this method.
  *
  * @throws InvocationTargetException Something went seriously wrong.
  * @throws IllegalAccessException Something went seriously wrong.
  * @throws InstantiationException Something went seriously wrong.
  */

 public Object checkAroundAspects(Object inst, Method methodInInterface, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {

  logger.debug(() -> "checkAroundAspects for \"" + inst.getClass().getName() + "." + method.getName() + "\"");

  JoinPoint joinPoint = new JoinPoint(inst, methodInInterface, method, args);

  joinPoint.setAroundAdvices(getInvocation(joinPoint).aroundAdvices);

  return joinPoint.proceedStart();

 }




 private synchronized Link getInvocation(JoinPoint joinPoint) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {

  Method method = joinPoint.getMethod();

  Link i;

  if (mesh.containsKey(method))
   i = mesh.get(method);  // Already woven.
  else {
   i = weave(joinPoint);

   mesh.put(method, i);
  }

  return i;

 }



 private Link weave(JoinPoint joinPoint) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

  Link res = new Link();
  res.beforeAdvices = new ArrayList<>();
  res.afterFinalAdvices = new ArrayList<>();
  res.afterReturnAdvices = new ArrayList<>();
  res.afterThrownAdvices = new ArrayList<>();
  res.aroundAdvices = new ArrayList<>();

  for (Class<? extends Aspect> cl : aspectClasses) {  // For all registered aspect classes...
   for (Method aspectMethod : cl.getMethods()) {  // ... and all methods there in...

    Before banno = aspectMethod.getAnnotation(Before.class);

    if (banno != null)  // ... which have the annotation "Before".
     addAspect(
      "before",
      res.beforeAdvices,
      banno.matchedBy(),
      banno.value(),
      banno.order(),
      null,
      joinPoint,
      aspectMethod,
      cl
     );

    After afanno = aspectMethod.getAnnotation(After.class);

    if (afanno != null)  // ... which have the annotation "After".
     addAspect(
      "after final",
      res.afterFinalAdvices,
      afanno.matchedBy(),
      afanno.value(),
      afanno.order(),
      null,
      joinPoint,
      aspectMethod,
      cl
     );

    AfterReturn aranno = aspectMethod.getAnnotation(AfterReturn.class);

    if (aranno != null)  // ... which have the annotation "AfterReturn".
     addAspect(
      "after return",
      res.afterReturnAdvices,
      aranno.matchedBy(),
      aranno.value(),
      aranno.order(),
      null,
      joinPoint,
      aspectMethod,
      cl
     );

    AfterThrown atanno = aspectMethod.getAnnotation(AfterThrown.class);

    if (atanno != null)  // ... which have the annotation "AfterThrown".
     addAspect(
      "after thrown",
      res.afterThrownAdvices,
      atanno.matchedBy(),
      atanno.value(),
      atanno.order(),
      atanno.thrown(),
      joinPoint,
      aspectMethod,
      cl
     );

    Around artanno = aspectMethod.getAnnotation(Around.class);

    if (artanno != null)  // ... which have the annotation "Around".
     addAspect(
      "around",
      res.aroundAdvices,
      artanno.matchedBy(),
      artanno.value(),
      artanno.order(),
      null,
      joinPoint,
      aspectMethod,
      cl
     );

   }

  }

  Collections.sort(res.beforeAdvices);
  Collections.sort(res.afterFinalAdvices);
  Collections.sort(res.afterReturnAdvices);
  Collections.sort(res.afterThrownAdvices);
  Collections.sort(res.aroundAdvices);

  return res;

 }


 private void addAspect(String logMess, List<Advice> advices,
                        MethodSelect msel, String val, int order, Class<? extends Throwable> thrown,
                        JoinPoint joinPoint, Method aspectMethod, Class<? extends Aspect> aspectClass)
  throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

  if (msel.matches(joinPoint, val)) {
   // Matches!
   advices.add(new Advice(createAspect(aspectClass), aspectMethod, order, thrown));

   logger.debug(() -> "addAspect(): woven " + logMess +" method \"" + joinPoint.obj.getClass().getName() + "." + joinPoint.getMethod().getName() + "\" with aspect \"" + aspectClass.getName() + "." + aspectMethod.getName() + "\"");

  }

 }



 private Aspect createAspect(Class<? extends Aspect> aspectClass) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {

  return
   (Aspect)ObjectManager.get().getObject(aspectClass);

 }



 public interface Aspect extends ObjectManager.IObject {

 }



 private static class Link {

  List<Advice> beforeAdvices;
  List<Advice> afterFinalAdvices;
  List<Advice> afterReturnAdvices;
  List<Advice> afterThrownAdvices;
  List<Advice> aroundAdvices;



  public Link() {

   beforeAdvices = new ArrayList<>();
   afterFinalAdvices = new ArrayList<>();
   afterReturnAdvices = new ArrayList<>();
   afterThrownAdvices = new ArrayList<>();
   aroundAdvices = new ArrayList<>();

  }

 }



 private static class Advice implements Comparable<Advice> {

  final Object obj;
  final Method method;
  final int order;
  final Class<? extends Throwable> thrown;


  public Advice(Object obj, Method method, int order, Class<? extends Throwable> thrown) {

   this.obj = obj;
   this.method = method;
   this.order = order;
   this.thrown = thrown;

  }



  @Override
  public int compareTo(Advice ad) {

   if (order == ad.order)
    return 0;
   else if (order > ad.order)
    return 1;
   else
    return -1;

  }

 }



 /**
  * A JoinPoint is position in the program flow.
  *
  * <p>MC currently supports only method calls as JoinPoints.</p>
  *
  */

 public static class JoinPoint {

  private final Object obj;
  private final Method methodInInterface;
  private final Method method;
  private final Object[] args;
  private Object retVal;
  private Throwable thrown;
  private String sig = null;
  private List<Advice> aroundAdvices = null;
  private int aroundAdvicesIndex = 0;



  /**
   * Create a new JoinPoint.
   *
   * @param obj The object instance.
   * @param methodInInterface The method definition in the interface.
   * @param method The method with in the object.
   * @param args The arguments for the method.
   */

  public JoinPoint(Object obj, Method methodInInterface, Method method, Object[] args) {

   this.obj = obj;
   this.methodInInterface = methodInInterface;
   this.method = method;
   this.args = args;

  }



  /**
   * Create a new JoinPoint.
   *
   * @param obj The object instance.
   * @param methodInInterface The method definition in the interface.
   * @param method The method with in the object.
   * @param args The arguments for the method.
   * @param retVal The return value from the method.
   */

  public JoinPoint(Object obj, Method methodInInterface, Method method, Object[] args, Object retVal) {

   this(obj, methodInInterface, method, args);

   this.retVal = retVal;

  }



  /**
   * Create a new JoinPoint.
   *
   * @param obj The object instance.
   * @param methodInInterface The method definition in the interface.
   * @param method The method with in the object.
   * @param args The arguments for the method.
   * @param thrown The Throwable thrown by the  method.
   */

  public JoinPoint(Object obj, Method methodInInterface, Method method, Object[] args, Throwable thrown) {

   this(obj, methodInInterface, method, args);

   this.thrown = thrown;

  }


  /**
   * Gets the method.
   *
   * @return The method.
   *
   */

  public Method getMethod() {

   return method;

  }



  /**
   * Gets the methodInInterface.
   *
   * @return The method.
   *
   */

  public Method getMethodInInterface() {

   return methodInInterface;

  }



  /**
   * Gets the arguments.
   *
   * @return The arguments.
   */

  public Object[] getArgs() {

   return args;

  }



  /**
   * Gets the return value.
   *
   * @return The return value.
   */

  public Object getRetval() {

   return retVal;

  }



  /**
   * Gets the Throwable.
   *
   * @return The Throwable.
   */

  public Throwable getThrown() {

   return thrown;

  }



  /**
   * Return the methods signature.
   *
   * <p>The signature is build from:</p>
   *
   * <ul>
   *  <li>The class name of the methods return type (full path)</li>
   *  <li>A space</li>
   *  <li>The methods class name (full path)</li>
   *  <li>A dot</li>
   *  <li>The methods name</li>
   *  <li>An opening bracket</li>
   *  <li>All class names (full path) of all of the methods parameters separated by colon</li>
   *  <li>A closing bracket</li>
   * </ul>
   *
   * @return The signature string.
   */

  public String getMethodSignature() {

   if (sig == null) {
    StringBuilder sb = new StringBuilder();

    sb.append(method.getReturnType().getName())
     .append(' ')
     .append(obj.getClass().getName())
     .append('.')
     .append(method.getName())
     .append('(');

    String sep = "";
    for (Class<?> cl : method.getParameterTypes()) {
     sb.append(sep).append(cl.getName());
     sep = ",";
    }

    sb.append(')');

    // ToConsider: Append all exceptions as well?

    sig = sb.toString();
   }

   return sig;

  }



  void setAroundAdvices(List<Advice> aroundAdvices) {

   this.aroundAdvices = aroundAdvices;

  }



  /**
   * Called from an advice method to proceed the advice list.
   *
   * @return result
   *
   * @throws InvocationTargetException If problems occur.
   * @throws IllegalAccessException If problems occur.
   */

  public Object proceed() throws InvocationTargetException, IllegalAccessException {

   if (aroundAdvicesIndex < aroundAdvices.size()) {
    Advice a = aroundAdvices.get(aroundAdvicesIndex);

    aroundAdvicesIndex ++;

    return a.method.invoke(a.obj, this);  // Invoke advice method.

   }

   // On the end of the advice list call advised method.

   logger.debug(() -> {
    StringBuilder sb = new StringBuilder();

    sb.append("proceed(): calling method: ");

    sb.append(obj.toString());
    sb.append(":");

    sb.append(method.getName());
    sb.append("(");

    String sep = "";
    for (Object arg : args) {
     sb.append(sep);
     sb.append(arg.toString());
     sep = ", ";
    }
    sb.append(")");

    return sb;
   });

   return method.invoke(obj, args);

  }




  Object proceedStart() throws InvocationTargetException, IllegalAccessException {

   aroundAdvicesIndex = 0;

   return proceed();

  }

 }



 /**
  * Enum specifies the match type for weaving aspects for join points.*
  */

 public enum MethodSelect {



  /**
   * Check against the method name.
   *
   * <p>value is a regex for the method signature.</p>
   */

  METHOD_HAS_NAME {
   @Override
   public boolean matches(JoinPoint joinPoint, String matchFor) {

    return joinPoint.getMethodSignature().matches(matchFor);

   }

  },



  /**
   * Check if method has an annotation.
   *
   * <p>value is a regex for the name of the annotation.</p>
   */

  METHOD_HAS_ANNOTATION {
   @Override
   public boolean matches(JoinPoint joinPoint, String matchFor) {

    for (Annotation a : joinPoint.getMethod().getDeclaredAnnotations())
     if (a.annotationType().getName().matches(matchFor))
      return true;  // The method has an annotation matching the given name.

    for (Annotation a : joinPoint.getMethodInInterface().getDeclaredAnnotations())
     if (a.annotationType().getName().matches(matchFor))
      return true;  // The method definition in the interface has an annotation matching the given name.

    return false;

   }

  };



  /**
   * Method checks if the aspect matches the joint point.
   *
   * @param joinPoint The join point.
   * @param matchFor The value form the aspect annotation.
   * @return True if matches. False otherwise.
   */

  public abstract boolean matches(JoinPoint joinPoint, String matchFor);

 }



 /**
  * Annotation for marking advice methods running before the advised methods.
  */

 @Documented
 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface Before {



  /**
   * Matching method.

   * @return The matching method.
   */

  Aspectator.MethodSelect matchedBy();



  /**
   * The value for matching.
   *
   * @return The value.
   */

  String value() default "";



  /**
   * Execution order.
   *
   * <p>Advice methods will be sorted by this value before calling.</p>

   * @return The execution order.
   */

  int order() default 0;

 }



 /**
  * Annotation for marking advice methods running finally after the advised methods.
  */

 @Documented
 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface After {



  /**
   * Matching method.

   * @return The matching method.
   */

  Aspectator.MethodSelect matchedBy();



  /**
   * The value for matching.
   *
   * @return The value.
   */

  String value() default "";



  /**
   * Execution order.
   *
   * <p>Advice methods will be sorted by this value before calling.</p>
   *
   * @return The execution order.
   */

  int order() default 0;

 }



 /**
  * Annotation for marking advice methods running finally after the advised methods.
  */

 @Documented
 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface AfterReturn {



  /**
   * Matching method.

   * @return The matching method.
   */

  Aspectator.MethodSelect matchedBy();



  /**
   * The value for matching.
   *
   * @return The value.
   */

  String value() default "";



  /**
   * Execution order.
   *
   * <p>Advice methods will be sorted by this value before calling.</p>

   * @return The execution order.
   */

  int order() default 0;

 }



 /**
  * Annotation for marking advice methods running around (instead of) the advised methods.
  */

 @Documented
 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface Around {



  /**
   * Matching method.

   * @return The matching method.
   */

  Aspectator.MethodSelect matchedBy();



  /**
   * The value for matching.
   *
   * @return The value.
   */

  String value() default "";



  /**
   * Execution order.
   *
   * <p>Advice methods will be sorted by this value before calling.</p>

   * @return The execution order.
   */

  int order() default 0;

 }



 /**
  * Annotation for marking advice methods running finally after the advised methods.
  */

 @Documented
 @Retention(RetentionPolicy.RUNTIME)
 @Target({ElementType.METHOD})
 public @interface AfterThrown {



  /**
   * Matching method.

   * @return The matching method.
   */

  Aspectator.MethodSelect matchedBy();



  /**
   * The value for matching.
   *
   * @return The value.
   */

  String value() default "";



  /**
   * Execution order.
   *
   * <p>Advice methods will be sorted by this value before calling.</p>

   * @return The execution order.
   */

  int order() default 0;



  /**
   * Throwable for which the aspect is responsible.
   *
   * @return The Throwable.
   */

  Class<? extends Throwable> thrown() default Throwable.class;

 }

}
