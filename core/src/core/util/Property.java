package core.util;

import java.lang.annotation.*;


/**
 * Marks a Property
 *
 * @version 2020-04-19
 * @author pilgrim.lutz@imail.de
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Property {

 String value();

}
