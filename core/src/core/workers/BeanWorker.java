package core.workers;

import core.bakedBeans.BeanList;
import core.bakedBeans.TypedBean;
import core.base.WorkerManager;



public interface BeanWorker extends WorkerManager.IWorker {

 BeanList getParameterAsBeanList(String className, String name);

 TypedBean getParameterAsBean(String className, String name);

 TypedBean make(String className);

}
