package core.workers;

import core.bakedBeans.Bean;
import core.bakedBeans.BeanFactory;
import core.bakedBeans.BeanList;
import core.bakedBeans.TypedBean;
import core.base.Request;
import core.base.WorkerManager;
import core.util.RequestConfig;


/**
 * A worker for generating Beans from the servlet request.
 *
 * @version 2023-09-30
 * @author lp
 */

public class BeanWorkerImpl implements WorkerManager.IWorker {



 /**
  * Returns a list of PersistentBeans from the HTTP request.
  *
  * <p>A bean is identified by its name an index and a property:</p>
  *
  * <code>bean[0].property</code>
  *
  * <p>If the property is the key of the bean this bean will be loaded. All other properties will be set.</p>
  *
  * <p>If no index is given an index of zero is presumed.</p>
  *
  * @param className The name of the PersistentBean class.
  * @param name The name of the bean within the request parameters.
  * @return The value of the parameter.
  */

 public BeanList getParameterAsBeanList(@RequestConfig() Request request, String className, String name) {

  Class<? extends Bean> aClass = BeanFactory.loadBeanClass(className);

  return BeanFactory.getParameterAsBeanArrayList(request.getHttpServletRequest(), aClass, name);

 }



 /**
  * Returns a single TypedBean from the request.
  *
  * @param className The name of the PersistentBean class.
  * @param name The name of the bean within the request parameters.
  * @return The Bean.
  */

 public TypedBean getParameterAsBean(@RequestConfig() Request request, String className, String name) {

  BeanList bs = getParameterAsBeanList(request, className, name);

  if (bs != null)
   return (TypedBean)bs.get(0);

  return null;

 }



 /**
  * Creates a bean of the given class.
  *
  * @param className bean class.

  * @return An instance of the bean.
  */

 public TypedBean make(String className) {

  return BeanFactory.make(className);

 }

}
