package core.servlet.filters;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;


/**
 * Filter for caching disabling.
 *
 * @version 2025-02-25
 * @author lp
 */

public class NoCache implements Filter {



 public void doFilter(ServletRequest request, ServletResponse response,
                      FilterChain chain) throws IOException, ServletException {

  HttpServletResponse res = (HttpServletResponse)response;

  res.setHeader("Expires", "Tue, 11 Mar 1960 00:00:00 GMT");
  res.setDateHeader("Last-Modified", new Date().getTime());
  res.setHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
  res.setHeader("Pragma", "no-cache");

  chain.doFilter(request, response);

 }

}
