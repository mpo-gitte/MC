package core.servlet.filters;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter for caching enabling.
 *
 * @version 2025-02-25
 * @author lp
 */

public class CacheEternally implements Filter {



 public void doFilter(ServletRequest request, ServletResponse response,
                      FilterChain chain) throws IOException, ServletException {

  HttpServletResponse res = (HttpServletResponse)response;

  res.setHeader("Expires", "Tue, 11 Mar 2060 00:00:00 GMT");
  res.setHeader("Cache-Control", "max-age=31536000, public");  // Nearly a year.

  chain.doFilter(request, response);

 }

}
