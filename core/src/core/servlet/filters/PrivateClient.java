package core.servlet.filters;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter for caching in users client.
 *
 * @version 2025-02-25
 * @author lp
 */

public class PrivateClient implements Filter {



 public void doFilter(ServletRequest request, ServletResponse response,
                      FilterChain chain) throws IOException, ServletException {

  HttpServletResponse res = (HttpServletResponse)response;

  res.setHeader("Cache-Control", "max-age=1800, private");  // Half an hour.

  chain.doFilter(request, response);

 }

}
