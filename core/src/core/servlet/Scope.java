package core.servlet;


/**
 * The scope BaseModule will "live" in the Servlet.
 *
 * @version 2022-11-19
 * @author lp
 */

public enum Scope {


 /**
  * Application scope. BaseModule is a singleton and will be created only once.
  */

 application,



 /**
  * The BaseModule lives in the session.
  * That is the BaseModule will be created an initialized for each session.
  */

 session,


 /**
  * The BaseModule lives in the request.
  * That is the BaseModule will be created an initialized for each request.
  */

 request

}
