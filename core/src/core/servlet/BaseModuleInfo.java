package core.servlet;

import core.base.BaseModule;

/**
 * This class holds information about a loaded BaseModule.
 *
 * @version 2022-11-22
 * @author lp
 */

public class BaseModuleInfo {

 private String key;
 private Class<BaseModule> baseModuleClass;

 private String scope;
 private boolean exactPath;
 private String path;



 /**
  * Set the scope for the BaseModule.
  *
  * @param scope The scope.
  */

 public void setScope(String scope) {

  this.scope = scope;

 }



 /**
  * Gets the scope for the BaseModule.
  *
  * @return The scope.
  */

 public String getScope() {

  return scope;

 }



 /**
  * Set request path for BaseModule
  *
  * @param path The path.
  */

 public void setRequestPath(String path) {

  // Make sure all paths begin with slash.
  if (!path.startsWith("/"))
   path = "/" + path;

  if (path.equals("/*")) {
   // Match all paths.
   this.path = null;
  }
  else if (path.endsWith("*")) {
   // Match some paths.
   exactPath = false;
   this.path = path.substring(0, path.length() - 1);
  }
  else {
   // Match one path.
   exactPath = true;
   this.path = path;
  }

 }

 /**
  * Gets the request path.
  *
  * @return request path restriction for this tool
  */

 public String getRequestPath() {

  return this.path;

 }



 /**
  * Checks if the BaseModule is valid for the request path.
  *
  * @param requestedPath The request path.
  *
  * @return true if valid false otherwise.
  */

 public boolean allowsRequestPath(String requestedPath) {

  if (this.path == null)
   return true;

  if (exactPath)
   return this.path.equals(requestedPath);
  else if (requestedPath != null)
   return requestedPath.startsWith(this.path);

  return false;

 }



 /**
  * Sets the key (name in the VelocityContext) for the BaseModule.
  *
  * @param key The key.
  */

 public void setKey(String key) {

  this.key = key;

 }



 /**
  * Get the key for the BaseModule.
  *
  * @return The key.
  */

 public String getKey() {

  return key;

 }



 /**
  * Sets the class of the BaseModule.
  *
  * @param baseModuleClass The class of the BaseModule.
  */

 public void setBaseModuleClass(Class<BaseModule> baseModuleClass) {

  this.baseModuleClass = baseModuleClass;

 }



 /**
  * Gets the class of the BaseModule.
  *
  * @return The class.
  */

 public Class<BaseModule> getBaseModuleClass() {

  return
   this.baseModuleClass;

 }

}
