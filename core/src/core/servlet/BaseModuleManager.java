package core.servlet;

import core.base.BaseModule;
import core.util.Utilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;


/**
 * The BaseModuleManager loads and initialize BaseModules.
 *
 * @see BaseModule
 *
 * @version 2022-11-26
 * @author lp
 */

public class BaseModuleManager {

 private static final String SESSION_TOOLS_KEY =
  BaseModuleManager.class.getName() + ":session-tools";

 private static Logger logger = LogManager.getLogger(BaseModuleManager.class);

 private Map<String, Object> appTools;
 private ArrayList<BaseModuleInfo> sessionToolInfo;
 private ArrayList<BaseModuleInfo> requestToolInfo;
 private boolean createSession;

 private static HashMap<String, BaseModuleManager> managersMap = new HashMap<>();



 private BaseModuleManager() {

  appTools = new HashMap<>();
  sessionToolInfo = new ArrayList<>();
  requestToolInfo = new ArrayList<>();
  createSession = true;

 }



 /**
  * Gets the BaseModules.
  *
  * @param servletContext The servlet context.
  * @param baseConfig The configuration
  *
  * @return The BaseModules.
  */

 static synchronized BaseModuleManager getInstance(ServletContext servletContext, BaseConfig baseConfig) {

  // Get the unique key for this toolbox file in this servlet context.
  String uniqueKey = "" + servletContext.hashCode() + ':' + baseConfig.hashCode();

  // Check if an instance already exists.
  BaseModuleManager toolboxManager = managersMap.get(uniqueKey);

  if (toolboxManager == null) {
   toolboxManager = new BaseModuleManager();

   toolboxManager.addBaseModules(baseConfig);

   // Remember it.
   managersMap.put(uniqueKey, toolboxManager);

   logger.debug("Toolbox setup complete.");

  }

  return toolboxManager;

 }



 private void addBaseModules(BaseConfig baseConfig) {

  try {
   getBaseModuleClasses("core.base").forEach(cl -> {
    try {
     String name = classVelocityName(cl);
     logger.info("loading base module: " + name);

     switch(classScope(cl)) {

      case application:
       @SuppressWarnings("unchecked")
       BaseModule base = (BaseModule)cl.getConstructor().newInstance();
       base.init(baseConfig);

       // BaseModule may return objects for the application scope.
       Map<String, Object> namedWorkers = base.initApplicationScope(baseConfig);
       if (namedWorkers != null)
        appTools.putAll(namedWorkers);

       logger.info("adding base module: " + cl.getName() + " in scope application as \"" + name + "\"");

       if (!Utilities.isEmpty(name))
         appTools.put(name, base);

      break;

      case request:
       logger.info("adding base module: " + cl.getName() + " in scope request as \"" + name + "\"");

       BaseModuleInfo sti = new BaseModuleInfo();
       sti.setScope("request");
       sti.setKey(name);
       sti.setBaseModuleClass(cl);
       sti.setRequestPath(classPath(cl));
       requestToolInfo.add(sti);
      break;

      case session:
       logger.info("adding base module: " + cl.getName() + " in scope session as \"" + name + "\"");

       BaseModuleInfo sti1 = new BaseModuleInfo();
       sti1.setScope("session");
       sti1.setKey(name);
       sti1.setBaseModuleClass(cl);
       sessionToolInfo.add(sti1);
      break;

     }
    }
    catch (Exception e) {
     throw new RuntimeException(e);
    }
   });
  } catch (Exception e) {
   throw new RuntimeException(e);
  }

 }



 private static Stream<Class<BaseModule>> getBaseModuleClasses(String packageName) throws IOException {

  String markerInterfaceName = packageName + ".BaseModule";

  // Fetch the classes of interest. Honour sort order given by annotations.

  return Utilities.findClassesInPackage(packageName, BaseModule.class)
   .filter(cl -> Arrays.stream(cl.getInterfaces()).anyMatch(i -> markerInterfaceName.equals(i.getName())))
   .sorted((cl1, cl2) -> classOrder(cl1).compareTo(classOrder(cl2)));

 }



 private static Integer classOrder(Class cl1) {

  @SuppressWarnings("unchecked")
  BaseModuleDescription bmd = (BaseModuleDescription) cl1.getAnnotation(BaseModuleDescription.class);

  return (bmd == null) ? Integer.MAX_VALUE : bmd.order();

 }



 private static String classVelocityName(Class cl1) {

  @SuppressWarnings("unchecked")
  BaseModuleDescription bmd = (BaseModuleDescription) cl1.getAnnotation(BaseModuleDescription.class);

  return (bmd == null) ? null : bmd.name();

 }



 private static Scope classScope(Class cl1) {

  @SuppressWarnings("unchecked")
  BaseModuleDescription bmd = (BaseModuleDescription) cl1.getAnnotation(BaseModuleDescription.class);

  return (bmd == null) ? Scope.application : bmd.scope();

 }



 private static String classPath(Class cl1) {

  @SuppressWarnings("unchecked")
  BaseModuleDescription bmd = (BaseModuleDescription) cl1.getAnnotation(BaseModuleDescription.class);

  return (bmd == null) ? "*" : bmd.path();

 }



 /**
  * Collects the BaseModules.
  *
  * @param httpServletRequest The servlet request
  * @param httpServletResponse The servlet response
  * @param velocityContext The velocity context
  * @param velocityEngine The velocity context.
  *
  * @return All BaseModules for the current request.
  */

 public Map<String, Object> getToolbox(
  HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
  VelocityContext velocityContext, VelocityEngine velocityEngine) {

  BaseConfig baseConfig =
   new BaseConfig(httpServletRequest, httpServletResponse, velocityContext, velocityEngine);

  String requestPath = ServletUtils.getPath(httpServletRequest);

  // Create the toolbox map with the application tools in it.
  Map<String, Object> toolbox = new HashMap<>(appTools);

  if (!sessionToolInfo.isEmpty()) {
   HttpSession session = httpServletRequest.getSession(createSession);

   if (session != null) {
    // Allow only one thread per session at a time.
    synchronized (getMutex(session)) {

     // Get the session tools. Session tools map is stored in the session.
     @SuppressWarnings("unchecked")
     Map<String, BaseModule> stmap =
      (Map<String, BaseModule>) session.getAttribute(SESSION_TOOLS_KEY);

     if (stmap == null) {
      // Init and store session tools map.
      stmap = new HashMap<>(sessionToolInfo.size());

      Map<String, BaseModule> finalStmap = stmap;
      sessionToolInfo.forEach(sti -> finalStmap.put(sti.getKey(), getBaseModule(sti, baseConfig)));

      session.setAttribute(SESSION_TOOLS_KEY, stmap);
     }

     // Add them to the toolbox.
     toolbox.putAll(stmap);
    }
   }
  }

  // Add and initialize request tools.
  requestToolInfo.forEach(toolInfo -> {
   if (toolInfo.allowsRequestPath(requestPath)) {
    toolbox.put(toolInfo.getKey(), getBaseModule(toolInfo, baseConfig));
   }
  });

  return toolbox;

 }



 private BaseModule getBaseModule(BaseModuleInfo sti, BaseConfig baseConfig) {

  BaseModule baseModule = null;

  try {
   baseModule = sti.getBaseModuleClass().getConstructor().newInstance();

   baseModule.init(baseConfig);
  }
  catch (Exception e) {
   throw new RuntimeException(e);
  }

  return baseModule;

 }



 private Object getMutex(HttpSession session) {

  // yes, this uses double-checked locking, but it is safe here
  // since partial initialization of the lock is not an issue

  Object lock = session.getAttribute("session.mutex");

  if (lock == null) {
   // one thread per toolbox manager at a time
   synchronized(this) {
    // in case another thread already came thru
    lock = session.getAttribute("session.mutex");
    if (lock == null) {
     // use a Boolean because it is serializable and small
     lock = true;
     session.setAttribute("session.mutex", lock);
    }
   }
  }
  return lock;
 }

}
