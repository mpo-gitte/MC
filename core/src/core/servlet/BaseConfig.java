package core.servlet;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


/**
 * Configuration data for BaseModules.
 *
 * @version 2002-11-25
 * @author lp
 */

public class BaseConfig {

 private VelocityEngine velocityEngine = null;
 private ServletConfig servletConfig = null;
 private ServletContext servletContext = null;
 private HttpServletRequest httpServletRequest = null;
 private HttpServletResponse httpServletResponse = null;
 private VelocityContext velocityContext = null;



 /**
  * Constructor.
  * @param httpServletRequest The servlet request.
  * @param httpServletResponse The servlet response.
  * @param velocityContext The Velocity context.
  * @param velocityEngine The Velocity engine.
  */

 public BaseConfig(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, VelocityContext velocityContext, VelocityEngine velocityEngine) {

  this.httpServletRequest = httpServletRequest;
  this.httpServletResponse = httpServletResponse;
  this.velocityContext = velocityContext;
  this.velocityEngine = velocityEngine;

 }



 /**
  * Constructor.
  *
  * @param servletConfig The ServletConfig.
  * @param velocityEngine The Velocity engine.
  */

 public BaseConfig(ServletConfig servletConfig, ServletContext servletContext, VelocityEngine velocityEngine) {

  this.servletConfig = servletConfig;
  this.servletContext = servletContext;
  this.velocityEngine = velocityEngine;

 }



 /**
  * Gets an init parameter.
  *
  * @param parameter Name of the parameter.
  * @return The value of the parameter.
  */

 public String getInitParameter(String parameter) {

  return getInitParameter(parameter, null);

 }



 /**
  * Gets an init parameter.
  *
  * @param parameter Name of the parameter.
  * @param defValue The default value is used if the parameter can't be found.
  * @return The value of the parameter.
  */

 public String getInitParameter(String parameter, String defValue) {

  // System property first.
  String sd = System.getProperty(parameter);

  if (sd != null)
   return sd;

  // Then environment variable.
  String envkey = "MC_" + parameter.replace(".", "_");
  sd = System.getenv(envkey);

  if (sd != null)
   return sd;

  // Servlet configuration finally.
  sd = servletConfig.getInitParameter(parameter);

  if (sd != null)
   return sd;

  // Default value.
  return defValue;

 }



 /**
  * Gets a parameter from the Velocity configuration.
  *
  * @param parameter Name of the parameter.
  * @param defValue The default value is used if the parameter can't be found.
  * @return The value of the parameter.
  */

 public String getVelocityProperty(String parameter, String defValue) {

  String prop = (String)velocityEngine.getProperty(parameter);

  if (prop == null || prop.length() == 0)
   return defValue;

  return prop;

 }



 /**
  * Gets the ServletContext.
  *
  * @return The ServletContext from the ServletConfig.
  */

 public ServletContext getServletContext() {

  return servletConfig.getServletContext();

 }



 /**
  * Gets the servlet request.
  *
  * @return The servlet request.
  */

 public HttpServletRequest getHttpServletRequest() {

  return httpServletRequest;

 }



 /**
  * Get the servlet response.
  *
  * @return The servlet response.
  */

 public HttpServletResponse getHttpServletResponse() {

  return httpServletResponse;

 }



 /**
  * Gets the Velocity context.
  *
  * @return The Velocity context.
  */

 public VelocityContext getVelocityContext() {

  return velocityContext;

 }



 /**
  * Gets the Velocity engine.
  *
  * @return The Velocity engine.
  */

 public VelocityEngine getVelocityEngine() {

  return velocityEngine;

 }

}
