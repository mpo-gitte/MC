package core.servlet;


import core.base.MeterMaid;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ResourceNotFoundException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.Map;
import java.util.Properties;


/**
 * MCs servlet.
 *
 * <p>This servlet is based on the VelocityViewServlet</p>
 *
 * @version 2025-03-04
 *
 * @author Dave Bryson
 * @author <a href="mailto:jon@latchkey.com">Jon S. Stevens</a>
 * @author <a href="mailto:sidler@teamup.com">Gabe Sidler</a>
 * @author <a href="mailto:geirm@optonline.net">Geir Magnusson Jr.</a>
 * @author <a href="mailto:kjohnson@transparent.com">Kent Johnson</a>
 * @author <a href="mailto:dlr@finemaltcoding.com">Daniel Rall</a>
 * @author Nathan Bubna
 * @author lp
 */

public class MCServlet extends HttpServlet {

 private final Logger logger = LogManager.getLogger(MCServlet.class);

 private static final long serialVersionUID = -3329444102562079189L;
 private static final String CONTENT_TYPE = "default.contentType";
 private static final String DEFAULT_CONTENT_TYPE = "text/html";
 private static final String ERROR_MODE = "default.errorMode";
 private static final String DEFAULT_ERROR_MODE = "0";
 public static final String SERVLET_CONTEXT_KEY = ServletContext.class.getName();
 protected static final String INIT_PROPS_KEY =
  "org.apache.velocity.properties";
 protected static final String DEFAULT_PROPERTIES_PATH =
  "/WEB-INF/velocity.properties";
 protected BaseModuleManager baseModuleManager = null;
 private VelocityEngine velocity = null;
 private String defaultContentType;
 private String errorMode = DEFAULT_ERROR_MODE;
 private String[] utfPaths;


 /**
  * Called by the servlet engine to initialize the servlet.
  *
  * @param servletConfig The servlet configuration.
  *
  * @throws ServletException If something went wrong.
  */

 public void init(ServletConfig servletConfig) throws ServletException  {

  logger.info("MCServlet initializing...");

  super.init(servletConfig);

  // Init velocity.
  initVelocity(servletConfig);

  // Init BaseModules.
  initBaseModules(new BaseConfig(servletConfig, getServletContext(), velocity));

  errorMode = getVelocityProperty(ERROR_MODE, DEFAULT_ERROR_MODE);

  logger.info("MCServlet: Default error-mode is: \"{}\"", errorMode);

  defaultContentType = getVelocityProperty(CONTENT_TYPE, DEFAULT_CONTENT_TYPE);

  logger.info("MCServlet: Default content-type is: \"{}\"", defaultContentType);

  // Setting CharSets for templates.
  String utfPath = getVelocityProperty("responseCharset.utf8", "");
  if (!utfPath.isEmpty())
   utfPaths = utfPath.split(";");
  if (utfPaths != null)
   for (int id = 0; id < utfPaths.length; id ++) {
    logger.info("MCServlet: Using Charset UTF-8 for templates in path: \"{}\"", utfPaths[id]);
    utfPaths[id] = "glob:" + utfPaths[id];
   }

  logger.info("MCServlet initializing ready!");

 }



 private String findInitParameter(ServletConfig config) {

  // Check the servlet config first.
  String param = config.getInitParameter(INIT_PROPS_KEY);

  if (param == null || param.isEmpty()) {
   // Check the servlet context.
   ServletContext servletContext = config.getServletContext();
   param = servletContext.getInitParameter(INIT_PROPS_KEY);
  }

  return param;

 }



 private String getVelocityProperty(String key, String alternate) {

  String prop = (String)velocity.getProperty(key);

  if (prop == null || prop.isEmpty())
   return alternate;

  return prop;

 }



 private void initBaseModules(BaseConfig baseConfig) {

  ServletContext servletContext = getServletContext();

  baseModuleManager = BaseModuleManager.getInstance(servletContext, baseConfig);

  servletContext.setAttribute("lovelyRita", MeterMaid.get());  // Make sure MeterMaid is loaded and initialized!

 }



 private void initVelocity(ServletConfig config) throws ServletException {

  velocity = new VelocityEngine();

  velocity.setApplicationAttribute(SERVLET_CONTEXT_KEY, getServletContext());

  // Try reading an overriding user Velocity configuration.

  try {
   Properties p = loadConfiguration(config);
   velocity.setProperties(p);
  }
  catch(Exception e) {
   logger.warn("initVelocity(): Unable to read Velocity configuration file: ", e);
   logger.warn("initVelocity(): Using default Velocity configuration.");
  }

  // Initialize velocity instance.
  try {
   velocity.init();
  }
  catch(Exception e) {
   logger.error("initVelocity(): PANIC! unable to init()", e);
   throw new ServletException(e);
  }

 }



 private Properties loadConfiguration(ServletConfig config) throws IOException {

  // Get the path to the custom props file.
  String propsFile = findInitParameter(config);

  if (propsFile == null) {
   // If not found look for default.
   propsFile = DEFAULT_PROPERTIES_PATH;
   logger.debug(() -> "loadConfiguration(): Looking for custom properties at '" + DEFAULT_PROPERTIES_PATH + "'");
  }

  Properties p = new Properties();
  InputStream is = getServletContext().getResourceAsStream(propsFile);

  if (is != null) {
   p.load(is);
   logger.info("loadConfiguration(): Using custom properties at '{}'", propsFile);
  }
  else
   logger.debug("loadConfiguration(): No custom properties found. Using default Velocity configuration.");

  return p;

 }


 /**
  * Handles GET - calls doRequest()
  */

 public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

  doRequest(request, response);

 }



 /**
  * Handle a POST request - calls doRequest()
  */

 public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

  doRequest(request, response);

 }



 private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException {

  Template template = null;
  long start = System.nanoTime();

  try {

   // First, get a context.
   Context velocityContext = createContext(request, response, velocity);

   // Set the content type. Can be changed by template.
   setContentType(response);

   // Get the template with specific CharSet.
   template = getTemplateWithCharSet(request, response);

   // Bail if we can't find the template.
   if (template == null) {
    logger.error("doRequest(): Couldn't find template to match request.");
    throw new ResourceNotFoundException("Template not found!");
   }

   // Merge the template and context.
   mergeTemplate(template, velocityContext, response);

  }
  catch (Exception e) {

   // Count errors.
   MeterMaid.get().incrementCounter(
    "velocity_exceptions",
    "template", template != null ? template.getName() : "",
    "exception", e.toString()
   );

   error(request, response, e);
  }
  finally {
   String tn = template != null ? template.getName() : "";

   // Record elapsed time.
   MeterMaid.get().recordElapsedTime("request", "template", tn, start);

   // Record status code.
   MeterMaid.get().incrementCounter(
    "status",
    "template", tn,
    "status", Integer.toString(response.getStatus())
   );
  }

 }



 private Context createContext(HttpServletRequest request, HttpServletResponse response, VelocityEngine velocityEngine) {

  return
   new VelocityContext() {
    /**
     * Retrieves value from BaseModules.
     *
     * @param key name of value to get
     * @return value as object
     */
    @Override
    public Object internalGet(String key) {

    // Check BaseModules first.
    Map<String, Object> toolbox = baseModuleManager.getToolbox(request, response, this, velocityEngine);

    Object o = toolbox.get(key);

    if (o != null)
     return o;

    // If key is not in BaseModules call super class.
    return super.internalGet(key);
    }
   };

 }



 private void setContentType(HttpServletResponse response) {

  response.setContentType(defaultContentType);

 }



 private Template getTemplateWithCharSet(HttpServletRequest request, HttpServletResponse response) {

  String path = ServletUtils.getPath(request);

  return
   velocity.getTemplate(path, setCharSetForTemplate(path, response.getCharacterEncoding()));

 }



 private void mergeTemplate(Template template, Context velocityContext, HttpServletResponse response) throws IOException {

  response.setCharacterEncoding(template.getEncoding());

  Writer writer = response.getWriter();

  velocityContext.put("VeloWriter", writer);  // Sometimes we need the writer in templates.

  template.merge(velocityContext, writer);

 }



 private void error(HttpServletRequest request, HttpServletResponse response, Exception e) throws ServletException {

  // Log the exception.
  String path = ServletUtils.getPath(request);

  Throwable cause = e;

  if (cause instanceof MethodInvocationException) {
   // Get the real problem!
   Throwable realCause = cause.getCause();
   cause = realCause == null ? cause : realCause;
  }
  String why = cause.getMessage();

  StringWriter sw = new StringWriter();
  cause.printStackTrace(new PrintWriter(sw));

  logger.error(
   "error(): Exception processing the template for path: \"{}\" reason: \"{}\" stack: \"{}", path, cause, sw
  );

  try {
   StringBuilder html = new StringBuilder();
   html
    .append("<html>\n")
    .append("<head><title>Error</title></head>\n")
    .append("<body>\n")
    .append("<h2>MC: Application Error!</h2>");

   if ("1".equalsIgnoreCase(errorMode)) {
    // Be verbose.
    html
     .append("<br>in template: ")
     .append(path)
     .append("<br><br>");

    if (why != null && !why.trim().isEmpty())
     html
      .append(StringEscapeUtils.escapeHtml3(why))
      .append("\n<br>\n");

    html
     .append("<pre>\n")
     .append(StringEscapeUtils.escapeHtml3(sw.toString()))
     .append("</pre>\n");
   }

   html.append("</body>\n")
    .append("</html>");

   response.getWriter().write(html.toString());
  }
  catch (Exception e2) {
   // Exception while reporting the exception! --- Giving up!
   logger.error("error(): Exception while generating error screen: {}", e2.toString());

   throw new ServletException(e);
  }

 }



 /**
  * Destroys the servlet.
  *
  * <p>Does some de-initialization.</p>
  */

 public void destroy() {

  MeterMaid.get().close();

 }



 private String setCharSetForTemplate(String path, String defaultCharSet) {

  if (utfPaths != null) {
   for (String up : utfPaths) {
    PathMatcher pm = FileSystems.getDefault().getPathMatcher(up);
    if (pm.matches(Path.of(path)))
     return StandardCharsets.UTF_8.name();
   }
  }

  return defaultCharSet;

 }

}
