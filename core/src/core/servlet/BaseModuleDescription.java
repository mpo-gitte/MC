package core.servlet;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * This annotation describes a BaseModule.
 *
 * @version 2022-11-21
 * @author lp
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface BaseModuleDescription {



 /**
  * If the name is given the BaseModule will be put into the VelocityContext.
  *
  * @return The name of the BaseModule.
  */

 String name() default "";


 /**
  * Because BaseModules may depend on each other ordered loading can be done.
  *
  * <p>BaseModule with the lowest order are loaded first.</p>
  *
  * @return The order number.
  */

 int order() default Integer.MAX_VALUE;


 /**
  * The servlet scope in which the BaseModule will be "live".
  *
  * @return The scope.
  */

 Scope scope() default Scope.application;


 /**
  * The path for the request.
  *
  * @return the path.
*/

 String path() default "*";

}
