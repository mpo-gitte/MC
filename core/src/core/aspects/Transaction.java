package core.aspects;

import core.util.Aspectator;
import core.util.Aspectator.Around;
import core.util.Aspectator.JoinPoint;
import core.util.Aspectator.MethodSelect;
import core.util.DBConnectionStore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;


/**
 * Aspect for a transaction bracket.
 *
 * @version 2025-01-03
 * @author lp
 */

public class Transaction implements Aspectator.Aspect {

 private static Logger logger = LogManager.getLogger(Transaction.class);



 /**
  * Create a transaction bracket.
  *
  * <p>Triggered by Annotation CatchException.</p>
  *
  * @param jointPoint The join point
  *
  * @see core.aspects.annotations.Transaction
  * @see DBConnectionStore
  */

 @Around(matchedBy = MethodSelect.METHOD_HAS_ANNOTATION, value = ".*Transaction.*", order = 1)
 public Object transact(JoinPoint jointPoint) throws InvocationTargetException, IllegalAccessException {

  logger.debug(() -> "starting transaction for: \"" + jointPoint.getMethodSignature() + "\"");

  DBConnectionStore.getConnection();
  Object retVal;

  try {
   retVal =  jointPoint.proceed();
  }
  catch (Exception ex) {
   DBConnectionStore.rollback();
   throw ex;
  }

  DBConnectionStore.releaseConnection();

  return retVal;

 }

}
