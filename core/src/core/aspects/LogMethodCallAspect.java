package core.aspects;

import core.util.Aspectator;
import core.util.Aspectator.*;
import core.util.Utilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



/**
 * This aspect logs method calls.
 *
 * <p>Intended to be used for debug purposes.</p>
 *
 * @version 2019-01-27
 * @author pilgrim.lutz@imail.de
 *
 */

public class LogMethodCallAspect implements Aspectator.Aspect{

 private static Logger logger = LogManager.getLogger(LogMethodCallAspect.class);



 /**
  * Log before method call.
  *
  * @param jointPoint The join point
  */

 @Aspectator.Before(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*", order = 99)
 public void logBeforeMethodCall(JoinPoint jointPoint) {

  logger.debug(() -> "Before calling: \"" + jointPoint.getMethodSignature() + "\"");

 }



 /**
  * Log after method call.
  *
  * @param jointPoint The join point
  */

 @Aspectator.After(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*")
 public void logMethodCallAfter(JoinPoint jointPoint) {

  logger.debug(() -> "After calling: \"" + jointPoint.getMethodSignature() + "\"");

 }



 /**
  * Log after return method call.
  *
  * @param jointPoint The join point
  */

 @Aspectator.AfterReturn(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*")
 public void logMethodCallAfterReturn(JoinPoint jointPoint) {

  logger.debug(() -> {
   Object retval = jointPoint.getRetval();
   return
    "After return calling: \"" + jointPoint.getMethodSignature() + "\" returned: " +
     (Utilities.isEmpty(retval) ? "nothing" : "type=\"" + retval.getClass() + "\" value=\"" + retval + "\"");
  });

 }



 /**
  * Log after thrown method call.
  *
  * @param jointPoint The join point
  */

 @Aspectator.AfterThrown(matchedBy = MethodSelect.METHOD_HAS_NAME, value = ".*")
 public void logMethodCallAfterThrown(JoinPoint jointPoint) {

  logger.debug(() ->
   "After thrown calling: \"" + jointPoint.getMethodSignature() + "\" throwed: \"" +
   jointPoint.getThrown() + "\""
  );
 }

}
