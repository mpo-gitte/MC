package core.aspects;

import core.base.WorkerManager;
import core.util.Aspectator;
import core.util.Aspectator.Around;
import core.util.Aspectator.JoinPoint;
import core.util.Aspectator.MethodSelect;
import core.util.Utilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;



/**
 * Aspects for Exception handling
 *
 * @version 2020-02-14
 * @author pilgrim.lutz@imail.de
 */

public class CatchException implements Aspectator.Aspect {

 private static Logger logger = LogManager.getLogger(CatchException.class);



 /**
  * Watch exceptions thrown by methods.
  *
  * Triggered by Annotation CatchException.
  *
  * @param jointPoint The join point
  *
  * @see CatchException
  */

 @Around(matchedBy = MethodSelect.METHOD_HAS_ANNOTATION, value = ".*CatchException.*", order = 99)
 public Object catchException(JoinPoint jointPoint) throws InvocationTargetException, IllegalAccessException {

  logger.debug(() -> "watching exception for: \"" + jointPoint.getMethodSignature() + "\"");

  try {
   return jointPoint.proceed();
  }
  catch (Exception ex) {

   Throwable rex = Utilities.getRealProblem(ex);
   Class<?> ec = rex.getClass();

   while (ec != null) {
    if (isOneOfOurExceptions(ec)) {
     logger.debug(() -> "catching exception \"" + rex.getClass().getName() + "\" for \"" + jointPoint.getMethodSignature() + "\"");

     WorkerManager.setLastException(rex);

     return null;
    }

    ec = ec.getSuperclass();
   }
   logger.debug(() -> "throwing exception \"" + ex.getClass().getName() + "\" for \"" + jointPoint.getMethodSignature() + "\"");

   throw ex;
  }

 }



 private boolean isOneOfOurExceptions(Class<?> ex) {

  Class<?>[] excs = WorkerManager.getExceptionsToCatch();

  if (excs != null)
   for (Class ec : excs)
    if (ec == ex)
     return true;

  return false;

 }

}
