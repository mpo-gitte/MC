package core.aspects;

import core.base.Request;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import core.util.Aspectator.Aspect;
import core.util.Aspectator.Before;
import core.util.Aspectator.JoinPoint;
import core.util.Aspectator.MethodSelect;
import core.exceptions.SecurityException;

import core.aspects.annotations.OnlyLocal;
import core.util.Utilities;



/**
 * Aspect for checking the login request came from a local machine.
 *
 * @version 2024-01-02
 * @author lp
 */

public class CheckLocalAspect implements Aspect {

 private static Logger logger = LogManager.getLogger(CheckLocalAspect.class.getName());



 /**
  * Aspect checks if the login was issued from a local machine.
  *
  * <p>The method to check is marked with the OnlyLocal annotation.</p>
  *
  * @param joinPoint The join point (with the annotated method in it)
  *
  * @see OnlyLocal
  * @see Request#isLocal()
  */

 @Before(matchedBy = MethodSelect.METHOD_HAS_ANNOTATION, value = ".*OnlyLocal.*")
 public void checkLocal(JoinPoint joinPoint) {

  logger.debug(() -> "checking request is local for method: \"" + joinPoint.getMethodSignature() + "\"");

  try {
   if (!Utilities.isTrue(Request.get().isLocal()))
    throw new SecurityException(
     "Request to method \"" +
     joinPoint.getMethodSignature() + "\" " +
     "doesn't come from a local machine: \"" +
     Request.get().getIpAddress() + "!"
    );
  }
  catch (Exception e) {
   throw new SecurityException(e);
  }

 }

}
