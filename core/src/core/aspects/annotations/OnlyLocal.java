package core.aspects.annotations;

import java.lang.annotation.*;


/**
 * Triggers Aspect CheckLocal
 *
 * @version 2018-10-03
 * @author pilgrim.lutz@imail.de
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface OnlyLocal {

}
