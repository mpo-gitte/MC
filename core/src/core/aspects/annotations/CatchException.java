package core.aspects.annotations;

import java.lang.annotation.*;


/**
 * Triggers Aspect CatchException
 *
 * @see CatchException
 *
 * @version 2019-02-02
 * @author pilgrim.lutz@imail.de
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface CatchException {

}
