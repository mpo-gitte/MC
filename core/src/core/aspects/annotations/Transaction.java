package core.aspects.annotations;

import java.lang.annotation.*;


/**
 * Triggers Aspect Transaction
 *
 * @see core.aspects.Transaction
 *
 * @version 2025-01-03
 * @author lp
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Transaction {

}
