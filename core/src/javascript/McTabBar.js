//
// Component
//
// @version 2021-07-02
// @author pilgrim.lutz@imail.de
//

class McTabBar extends HTMLDivElement {



 constructor() {

  super();

 }



 connectedCallback() {

  this.onClickFunctionName = this.getAttribute("data-onClick");

  this.subElements = new Array();
  this.activeElement = null;

  var els = this.children;
  var i;

  for (i = 0; i < els.length; i++) {
   var el = els[i];
   this.subElements.push(el);
   var me = this;
   el.addEventListener("click", function(event) {me.onClick(event);})
  }

 }



 onClick(event) {

  var el = event.target;

  var i;
  for (i = 0; i < this.subElements.length; i++) {
   var b = this.subElements[i];
   if (el.id == b.id) {
    window[this.onClickFunctionName](i);
    break;
   }
  }

 }



 select(nr) {

  var i;
  for (i = 0; i < this.subElements.length; i++)
   if (i == nr)
    this.subElements[i].className = "TabBarSelected";
   else
    this.subElements[i].className = "TabBarUnselected";

 }

}



customElements.define("mc-tabbar", McTabBar, { extends: "div" });
