//
// Component for stacking DIVs.
//
// @version 2021-08-19
// @author pilgrim.lutz@imail.de
//

class McStack extends HTMLDivElement {



 constructor() {

  super();

 }



 connectedCallback() {

  this.showFunctionName = this.getAttribute("data-onShow");

  this.subElements = new Array();
  this.activeElement = null;

  var els = this.children;
  var i;

  for (i = 0; i < els.length; i++) {
   var el = els[i];
   this.subElements.push(el);

   el.style.visibility = "hidden";
   el.style.display = "none";
  }

 }



 show(nr) {

  var el = this.subElements[nr];

  if (this.activeElement != null) {
   this.activeElement.style.visibility = "hidden";
   this.activeElement.style.display = "none";
  }

  this.activeElement = el;
  el.style.visibility = "visible";
  el.style.display = "block";

  window.sessionStorage[this.makeParamName()] = nr;

  if (this.showFunctionName != null)
   window[this.showFunctionName](el);

 }



 init() {

  var nr = window.sessionStorage[this.makeParamName()];
  if (nr == undefined)
   nr = 0;

  this.show(nr);

 }



 makeParamName() {

  return "Stack_" + this.id;

 }

}



customElements.define("mc-stack", McStack, { extends: "div" });
