//
// Class for Marks.
//
// @version 2024-07-25
// @author lp
//

import Sprite from "./Sprite.js";



export default class McMark extends Sprite {

 static _marks = new Map();



 constructor(content, id) {

  super("mark", id);

  this.pmcontent = content;

 }



 show(element) {

  super.show(null, element, this.pmcontent)

  element.style.visibility = "hidden";

 }



 hide(element) {

  super.hide()

  if (element != undefined)
   element.style.visibility = "visible";

 }



 getOffset() {

  return 2;

 }



 getYOffset() {

  return -25;

 }



 static showMark(element, content) {

  var mark = this._marks.get(element.id);

  if (mark == null) {
   var mark = new McMark(content, element.id);

   this._marks.set(element.id, mark);
  }

  mark.show(element);

 }



 static hideAll() {

  for (let [id, mark] of this._marks) {
    let el = document.getElementById(id);
    mark.hide(el);
  }

 }

}
