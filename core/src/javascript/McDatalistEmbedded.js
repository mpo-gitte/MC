//
// Component to be filled with remote data.
//
// @version 2012-09-23
// @author lp
//

class McDatalistEmbedded extends HTMLDataListElement {



 constructor() {

  super();

  this.embedUrl = this.getAttribute("embedurl");
  this.readyFunctionName = this.getAttribute("onready");
  this.inputfield = this.getAttribute("inputfield");
  this.queryString = "";
  this.querystringprefix = "";
  this.busy = false;

  var sd = this.getAttribute("extrafields");
  this.extrafields = null;
  if (sd != null  &&  sd != "")
   this.extrafields = sd.split(";");

  this.multiple = false;

 }



 connectedCallback() {

  this.eli = document.getElementById(this.inputfield);

  var sd = this.eli.getAttribute("multiple");
  if (sd != null  &&  sd != "")
   this.multiple = true;

 }



 fill() {

  var str = this.buildParams();

  if (this.eli.value.length > 1) {
   this.fillWith(str);
   this.queryString = str;
  }
  else
   this.innerHTML = "";

 }



 buildParams() {

  var sd = this.eli.value;

  if (this.multiple) {
   var sda = this.eli.value.split(/[ ;,.%?*	!-+&]/);
   sd = sda[sda.length - 1];
   this.querystringprefix = this.eli.value.substring(0, this.eli.value.length - sd.length)
  }

  sd = this.eli.name + "=" + sd;

  if (this.multiple) {
   sd = sd + "&prefix=" + encodeURIComponent(this.querystringprefix);
  }

  if (this.extrafields != null)
   for (var id = 0; id < this.extrafields.length; id ++) {
    var f = this.extrafields[id];
    var v = document.getElementById(f).value;
    if (v != null  &&  v != "")
     sd = sd + "&" + f + "=" + v;
   }

  return sd;

 }



 fillWith(params) {

  if (this.busy == false) {
   this.busy = true;

//   this.innerHTML = null;

   this.req = new XMLHttpRequest();
   this.req.open('POST', this.embedUrl, true);
   this.req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=iso-8859-1");

   var me = this;
   this.req.onreadystatechange = function(){me._wereReady()};

   this.req.send(params);
  }

 }



 _wereReady() {

  if (this.req.readyState == 4) {

   var str = this.buildParams();

   if (str == this.queryString) {
    this.innerHTML = this.req.responseText;

    if (this.readyFunctionName != null)
     window[this.readyFunctionName](this);
   }
   else
    this.fillWith(str);

   this.busy = false;
  }

 }

}



customElements.define("mc-datalist-embedded", McDatalistEmbedded, { extends: "datalist" });
