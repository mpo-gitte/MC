//
// Support for horizontal scrolling DIVs.
//
// @version 2022-10-02
// @author pilgrim.lutz@imail.de
//

class HScroller {

 constructor(key) {

  this.key = key;

 }



 store() {

  var divs = document.getElementsByClassName("HScroller");
  var i;

  for (i = 0; i < divs.length; i++) {
   var el = divs[i];

   if (el.style.visibility != "hidden")
    window.sessionStorage[this.makeParamName(el)] = el.scrollLeft;
  }

 }



 resetAll() {

  var divs = document.getElementsByClassName("HScroller");
  var i;

  for (i = 0; i < divs.length; i++) {
   var el = divs[i];
   window.sessionStorage[this.makeParamName(el)] = null;
  }

 }



 scrollAll() {

  var divs = document.getElementsByClassName("HScroller");
  var i;

  for (i = 0; i < divs.length; i++) {
   var el = divs[i];
   this.scroll(el);
  }

 }



 scroll(el) {

  el.scrollLeft = window.sessionStorage[this.makeParamName(el)];

 }



 makeParamName(el) {

  return "HScroller_" + this.key + el.id;

 }

}
