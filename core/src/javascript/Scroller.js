//
// Support for scrolling DIVs.
//
// @version 2021-08-19
// @author pilgrim.lutz@imail.de
//

class Scroller {

 constructor(key) {

  this.key = key;

 }



 store() {

  var divs = document.getElementsByClassName("Scroller");
  var i;

  for (i = 0; i < divs.length; i++) {
   var el = divs[i];

   if (el.style.visibility != "hidden")
    window.sessionStorage[this.makeParamName(el)] = el.scrollTop;
  }

 }



 resetAll() {

  var divs = document.getElementsByClassName("Scroller");
  var i;

  for (i = 0; i < divs.length; i++) {
   var el = divs[i];
   window.sessionStorage[this.makeParamName(el)] = null;
  }

 }



 scrollAll() {

  var divs = document.getElementsByClassName("Scroller");
  var i;

  for (i = 0; i < divs.length; i++) {
   var el = divs[i];
   this.scroll(el);
  }

 }



 scroll(el) {

  el.scrollTop = window.sessionStorage[this.makeParamName(el)];

 }



 makeParamName(el) {

  return "Scroller_" + this.key + el.id;

 }

}
