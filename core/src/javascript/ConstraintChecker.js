var ConstraintCheckerCallback = function(elem, query, queryParam, callback) {

 this.elem = elem;
 this.query= query;
 this.queryParam = queryParam;
 this.callback = callback;
 var xreq;
 var queryVal;

 return {

  getXreq: function() {
   return xreq;
  },



  getElem: function() {
   return elem;
  },



  getQueryVal: function() {
   return queryVal;
  },



  getCallback: function() {
   return callback;
  },



  query: function() {
   queryVal = elem.value;

   xreq = new XMLHttpRequest();
   xreq.open('POST', query, true);
   xreq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=iso-8859-1');
   var o = this;
   xreq.onreadystatechange = function(){o.callback()};
   xreq.send(queryParam + queryVal);

  },



  callback: function() {
   var x = this.getXreq();

   if (x.readyState == 4) {
    if (this.getElem().value == this.getQueryVal()) {
     var val = (x.responseText == "true");
     var fn = window[callback];

     if (fn)
      fn(this.getElem(), val);
     else
      alert("query returned: " + val);
    }
    else
     this.query();
   }

  },



  check: function() {
   this.query();
  },

 }

};



var ConstraintChecker = function(elementName, query, queryParam, callback) {

 this.elementName = elementName;
 this.query = query;
 this.queryParam = queryParam;
 this.callback = callback;

 return {

  check: function() {

   var elem = document.getElementById(elementName);

   var o = new ConstraintCheckerCallback(elem, query, queryParam, callback);

   elem.addEventListener("keyup", function(){o.check()});
   elem.addEventListener("focus", function(){o.check()});

  },

 }

};
