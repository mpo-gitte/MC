//
// Base class for all "flying" objects.
//
// @version 2024-12-03
// @author lp
//

export default class Sprite {



 constructor(cssClass, id) {

  this._createSprite(cssClass, id);

 }



 _createSprite(cssClass, id) {

  this.div = document.createElement("DIV");
  this.div.innerHTML = "EMPTY!";
  this.div.classList.add("MCSPRITE");
  this.div.classList.add(cssClass);

  if (id != undefined)
   this.div.id = id;

  document.body.appendChild(this.div);

  this.hide();

 }



 show(event, element, content) {

  this.div.innerHTML = content;

  this.div.style.visibility = "visible";
  this.div.style.display = "block";
  this.div.style.position = "absolute";

  if (event != null) {
   var who = event.target;
   var xpos = event.clientX;
   var ypos = event.clientY;
   var keepLeft = true;
  }
  else {
   var who = element;
   var xpos = 0;
   var ypos = 0;
   var keepLeft = false;
  }

  var pos = this._getScreenPos(xpos, ypos, who, keepLeft);

  this.div.style.top = pos[1];
  this.div.style.left = pos[0];

 }



 _getScreenPos(posx, posy, who, keepLeft) {

  var offset = this.getOffset();
  var yoffset = this.getYOffset();
  var oleft = posx + offset;
  var otop = posy + yoffset;

  if (who) {
   otop = yoffset;

   var obj = who;
   do {
    if (obj.scrollTop)
     otop -= obj.scrollTop;
   }
   while (obj = obj.parentNode);

   obj = who;
   do {
    otop = otop + obj.offsetTop;
    if (keepLeft == false)
     oleft += obj.offsetLeft;
   }
   while (obj = obj.offsetParent);

   otop += who.offsetHeight;

   if ((otop + this.div.offsetHeight) > document.body.clientHeight)
    otop = otop - this.div.offsetHeight - who.offsetHeight - yoffset;
  }
  else
   if ((otop + this.div.offsetHeight) > document.body.clientHeight)
    otop = posy - this.div.offsetHeight - yoffset;

  if ((oleft + this.div.offsetWidth) > document.body.clientWidth)
   oleft = document.body.clientWidth - this.div.offsetWidth - 1;

  return [oleft, otop];

 }



 getOffset() {

  return 10;

 }



 getYOffset() {

  return this.getOffset();

 }



 hide() {

  this.div.style.visibility = "hidden";
  this.div.style.display = "none";

 }



 isShown() {

  if (this.div.style.display == "none")
   return false;

  return true;

 }



 hideSprites(cssClass) {

  if (cssClass == null)
   cssClass = "MCSPRITE";

  const sprites = document.getElementsByClassName(cssClass);

  for (let id = 0; id < sprites.length; id++) {
   sprites[id].style.visibility = "hidden";
   sprites[id].style.display = "none";
  }

 }

}
