//
// Component to be filled with remote data.
//
// @version 2024-12-03
// @author lp
//

class McDivEmbedded extends HTMLDivElement {



 constructor() {

  super();

 }



 connectedCallback() {

  this.embedUrl = this.getAttribute("embedurl");
  this.embedParams = this.getAttribute("embedparams");
  this.readyFunctionName = this.getAttribute("onready");
  this.innerElem = this.getAttribute("innerelem");
  this.dontFill = false;
  this.observer = null;
  var df = this.getAttribute("dontfill");
  if (df == "true")
   this.dontFill = true;

  if (this.dontFill == false)
   this.fill();

 }



 fill() {

  this.fillWith(this.embedParams);

 }



 fillWith(params) {

  this.req = new XMLHttpRequest();
  if (params != undefined  &&  params != "")
   params = "?" + params;
  else
   params = "";
  this.req.open('GET', this.embedUrl + params, true);
  this.req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=iso-8859-1");

  var me = this;
  this.req.onreadystatechange = function(){me._wereReady()};

  this.req.send();

 }



 _wereReady() {

  if (this.req.readyState == 4) {

   if (this.readyFunctionName != null) {
    var me = this;
    var dest = me;

    if (this.innerElem != null)
     dest = document.getElementById(this.innerElem);

    this.observer = new MutationObserver(function(mutations, observer) {
     mutations.forEach(function(mutation) {
      window[me.readyFunctionName](dest);
      observer.disconnect();
     });
    });

    var config = {
     attributes: false,
     childList: true,
     characterData: false
    };

    this.observer.observe(dest, config)
   }

   var me1 = this;

   setTimeout(
    function() {
     var dest = me1;

     if (me1.innerElem != null)
      dest = document.getElementById(me1.innerElem);

     dest.innerHTML = me1.req.responseText;
    },
    0
   );

  }

 }



 getEmbedParams() {

  return this.embedParams;

 }

}



customElements.define("mc-div-embedded", McDivEmbedded, { extends: "div" });
