//
// Class for ToolTips.
//
// @version 2024-12-03
// @author lp
//

import Sprite from "./Sprite.js";



var me;
var delay_time = 1500;
var display_time = 5000;



export default class ToolTip extends Sprite {



 constructor() {

  super("tooltip", undefined);
  super.hide();

  me = this;

  var _tooltiptimer = null;

  var ttevent = null;
  var ttcontent = null;

 }



 show(event, content) {

  this.ttevent = event;
  this.ttcontent = content;

  if (this._tooltiptimer != null) {
   this._noDelay();
   this._show();
  }
  else
   this._tooltiptimer = window.setTimeout("_showToolTip()", delay_time);

 }



 _show() {

  super.show(this.ttevent, null, this.ttcontent);

  this._tooltiptimer = window.setTimeout("_hideToolTip()", display_time);

 }



 hide() {

  if (this._tooltiptimer != null) {
   var bd = this.div.style.visibility != "hidden";
   super.hide();
   if (bd)
    this._tooltiptimer = window.setTimeout("_noDelayToolTip()", display_time);
  }

 }



 _hide() {

  this._noDelay();

  super.hide();

 }



 _noDelay() {

  window.clearTimeout(this._tooltiptimer);
  this._tooltiptimer = null;

 }

}



window._hideToolTip = function _hideToolTip() {

 me._hide();

}



window._showToolTip = function _showToolTip() {

 me._show();

}



window._noDelayToolTip = function _noDelayToolTip() {

 me._noDelay();

}
