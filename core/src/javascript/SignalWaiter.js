/**
* Waiting for signal.
*
* @version 2019-03-11
* @author pilgrim.lutz@imail.de
*/

class SignalWaiter {



 constructor(url, category, onMessageFunctionName) {

  this.url = url;
  this.category = category;
  this.onMessageFunctionName = onMessageFunctionName;

  this.evtSource = new EventSource(this.url + "?category=" + this.category);

  var me = this;
  this.evtSource.addEventListener("message", function(e){me.receiveEvent(e)});

  this.evtSource.addEventListener('open', function(e) {

   console.log("SignalWaiter.open()");

  }, false);

  this.evtSource.addEventListener('error', function(e) {

   if (e.readyState == EventSource.CLOSED) {
    alert("SignalWaiter.error()!");
   }

  }, false);

 }



 receiveEvent(ev) {

  if (this.url.indexOf(ev.origin) < 0) {
   alert("SignalWaiter: ill. origin!")
  }
  else {
   var data = JSON.parse(ev.data);

   this.signal = data.signal;
   this.beanclass = data.beanclass;
   this.beanid = data.beanid;

   window[this.onMessageFunctionName](this);
  }

 }

}
