//
// The lazy loader
//
// @version 2023-12-15
// @author pilgrim.lutz@imail.de



var LazyLoaderCaller = function(element, xreq, callback, observer) {

 this.element = element;
 this.xreq = xreq;
 this.observer = observer;
 this.callback = callback;



 function setValue(el, val) {

  if (callback != null) {
   var fn = window[callback];

   if (fn)
    fn(el, val);
   else
    console.log("LazyLoader: Callback \"" + callback + "\" not found!")
   }
  else
   el.innerHTML = val;

  el.setAttribute("data-lazyloaderstop", "true")

 };



 return {

  getElement: function() {
   return element;
  },



  getXreq: function() {
   return xreq;
  },



  getObserver: function() {
   return observer;
  },



  setValueCallback: function() {
   var x = this.getXreq();

   if (x.readyState == 4) {
    var val = x.responseText;
    var tel = this.getElement();

    setValue(tel, val);
   }
  },

 }

};



var LazyLoader = function(containerElement) {

 this.containerElement = containerElement;
 this.observer = null;

 return {

  handleSingleEntry: function(element, observer) {

   var u = element.getAttribute("data-lazyloaderurl");
   if (u != null) {
    var q = element.getAttribute("data-lazyloaderquery");
    var cb = element.getAttribute("data-lazyloadercallback");
    var sync = element.getAttribute("data-lazyloadersync");

    this.queryValue(element, u, q, cb, observer, sync);
   }

  },



  callback: function(entries, observer) {

   for (var id = 0; id < entries.length; id++) {
     var iel = entries[id];
     if (iel.isIntersecting) {
      var el = iel.target;
      this.observer.unobserve(el);
      this.handleSingleEntry(el, observer);
     }
    }
   },



  queryValue: function(element, url, query, callback, observer, sync) {

   var xreq = new XMLHttpRequest();

   var params = "";
   if (query != undefined  &&  query != "")
    params = "?" + query;

   xreq.open('GET', url + params, sync != "true");
   xreq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=iso-8859-1");

   var o = new LazyLoaderCaller(element, xreq, callback, observer);
   xreq.onreadystatechange = function(){o.setValueCallback()};

   xreq.send();

  },



  observe: function() {

   if (this.observer == null) {
    var options = {
     root: containerElement,
     rootMargin: '0px',
     threshold: 0.05
    };

    var that = this;
    this.observer =
     new IntersectionObserver(function(entries, observer){that.callback(entries, observer)}, options);
   }

   this.scanRecursive(containerElement);

  },



  scanRecursive: function(el) {

   var b = el.getAttribute("data-lazyloaderstop");
   if (b)
    return;

   var b = el.getAttribute("data-lazyloaderurl");
   if (b) {
    this.observer.observe(el);
    return;
   }

   var children = el.children;
   for (var i = 0; i < children.length; i++) {
    var el = children[i];
    this.scanRecursive(el)
   }
  },

 }
};
