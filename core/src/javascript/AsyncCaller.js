/**
* Making asynchronous requests.
*
* @version 2019-02-18
* @author pilgrim.lutz@imail.de
*/

class AsyncCaller {



 constructor() {

 }



 call(url, params, func) {

  this.req = new XMLHttpRequest();
  this.req.open('POST', url, true);
  this.req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=iso-8859-1");

  this.readyFunctionName = func;

  var me = this;
  this.req.onreadystatechange = function(){me._wereReady()};

  this.req.send(params);

 }



 _wereReady() {

  if (this.req.readyState == 4) {

   if (this.readyFunctionName != null)
    window[this.readyFunctionName](this);
  }

 }

}
