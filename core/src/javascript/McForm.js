//
// MC's form.
//
// @version 2024-07-24
// @author lp
//

import McMark from "./McMark.js";

class McForm extends HTMLFormElement {



 constructor() {

  super();

  this.url = this.getAttribute("data-mcform-url");
  this.readyFunctionNameFill = this.getAttribute("data-mcform-onreadyfill");
  this.readyFunctionNameSave = this.getAttribute("data-mcform-onreadysave");
  this.errorFunctionNameSave = this.getAttribute("data-mcform-onerrorsave");

  this.keyFields = new Array();
  this.dataFields = new Array();

 }



 connectedCallback() {

  this.collectFields();

  this.changed = false;

 }



 collectFields() {

  this._collectFields(this.children);

 }



 _collectFields(elements) {

  var i;

  for (i = 0; i < elements.length; i++) {
   var el = elements[i];
   if (el.tagName == "INPUT"  ||  el.tagName == "CHECKBOX"  ||  el.tagName == "SELECT") {
    var q = el.getAttribute("data-mcform-key");
    if (q)
     this.keyFields.push(el);
    this.dataFields.push(el);

    var me = this;
    el.addEventListener("change", function(event) {me._onChange(event);})
   }
   else if (el.children.length > 0)
    this._collectFields(el.children);
  }

  this.addEventListener("click", function(event) {this._onClickForm(event);})

 }



 _onChange(event) {

  var el = event.target;

  el.className = "formnormal";

  var mention = true;

  if (window.onChangeForm)
   mention = !window.onChangeForm(this.name, el)

  if (!this.changed)
   this.changed = mention;

 }



 _onClickForm(event) {

  McMark.hideAll();

 }



 checkMandatory(errmess) {

  var els = this.getElementsByTagName("INPUT");

  for (var i = 0; i < els.length; i++) {
   var el = els[i];
   var q = el.getAttribute("data-mcform-mandatory");
   if (q) {
    if (el.value == "") {
     el.className = "formerror";
     el.focus();

     this._showErrmess(errmess, el);

     return false;
    }
   }

  }

  return true;

 }



 checkPatterns(errmess) {

  var els = this.getElementsByTagName("INPUT");

  for (var i = 0; i < els.length; i++) {
   var el = els[i];
   var pattern = el.getAttribute("pattern");
   if (pattern) {
    var regex = new RegExp(pattern);

    if (el.value != ""  &&  !regex.test(el.value)) {
     el.className = "formerror";
     el.focus();

     this._showErrmess(errmess, el);

     return false;
    }
   }

  }

  return true;

 }



 _showErrmess(errmess, el) {

  var name = el.id;
  var label = document.getElementById(name + "_label");

  if (label) {
   name = label.innerHTML.trim();
  }

  var sd = errmess;
  var sd1 = name.replace(/ \*/g, "")

  alert(sd.replace(/\{0\}/g, sd1));

 }



 isChanged() {

  return this.changed;

 }



 clean() {

  this.changed = false;

 }



 fill() {

  var params = "";
  var sep = "?";

  for (var i = 0; i < this.keyFields.length; i++) {
   var el = this.keyFields[i];
   params = params + sep + encodeURIComponent(el.name) + "=" + encodeURIComponent(el.value);
   sep = "&";
  }

  this.freq = new XMLHttpRequest();
  this.freq.open('GET', this.url + params, true);
  this.freq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

  var me = this;
  this.freq.onreadystatechange = function(){me._wereReadyFill()};

  this.freq.send(null);

 }



 _wereReadyFill() {

  if (this.freq.readyState == 4) {

   var fd = JSON.parse(this.freq.responseText);

   this._doFill(fd);

   if (this.readyFunctionNameFill != null)
    window[this.readyFunctionNameFill](this);
  }

 }



 _doFill(params) {

   var els = this.dataFields;
   var el;
   var val;

   for (var ix = 0; ix < els.length; ix ++) {
    el = els[ix];
    val = params[el.id];

    if (el.type == "checkbox"  ||  el.tagName == "CHECKBOX")
     el.checked = (val != "false")  &&  (val != undefined)  &&  (val != "");
    else if (val != undefined)
     el.value = val;
   }
 }



 save() {

  this.sreq = new XMLHttpRequest();
  this.sreq.open('POST', this.url, true);
  this.sreq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

  var me = this;
  this.sreq.onreadystatechange = function(){me._wereReadyFillSave()};

  var els = this.dataFields;
  var params = "";
  var sep = "";

  for (var i = 0; i < els.length; i++) {
   var el = els[i];
   var val = el.value;
   if (val != undefined  &&  (el.type != "checkbox"  ||  el.checked == true)  &&  (el.tagName != "CHECKBOX"  ||  el.checked == true)) {
    params = params + sep + encodeURIComponent(el.name) + "=" + encodeURIComponent(val);
    sep = "&";
   }
  }

  this.sreq.send(params);

 }



 _wereReadyFillSave() {

  if (this.sreq.readyState == 4) {

   var status = this.sreq.status;

   if (status === 0  ||  (status >= 200 && status < 400)) {
    if (this.readyFunctionNameSave != null)
     window[this.readyFunctionNameSave](status);
   }
   else {
    console.log("MCForm: Error during save(): " + status);
    if (this.errorFunctionNameSave != null)
     window[this.errorFunctionNameSave](status);
   }
  }

 }



 getDataFields() {

  return this.dataFields;

 }



 store() {

  var els = this.dataFields;
  var el;

  for (var ix = 0; ix < els.length; ix ++) {
   el = els[ix];
   if (el.id != "") {
    if (el.type == "checkbox"  ||  el.tagName == "CHECKBOX")
     window.sessionStorage[this._makeParamName(el)] = el.checked;
    else if (el.value != undefined)
     window.sessionStorage[this._makeParamName(el)] = el.value;
   }
  }

 }



 restore() {

  var els = this.dataFields;
  var el;
  var val;

  for (var ix = 0; ix < els.length; ix ++) {
   el = els[ix];
   if (el.id != "") {
    val = window.sessionStorage[this._makeParamName(el)];
    if (el.type == "checkbox"  ||  el.tagName == "CHECKBOX")
     el.checked = (val == "true");
    else if (val != undefined)
     el.value = val;
   }
  }

 }



 clearstore() {

  var toDelete = new Array();

  for (var ix = 0; ix < window.sessionStorage.length; ix ++) {
   var key = window.sessionStorage.key(ix);
   if (key.startsWith("Form_" + this.id))
    toDelete.push(key);
  }

  for (var ix = 0; ix < toDelete.length; ix ++) {
   window.sessionStorage.removeItem(toDelete[ix]);
  }

 }



 _makeParamName(el) {

  return "Form_" + this.id + "_" + el.id;

 }



 toQueryString() {

  var params = "";
  var sep = "";

  for (var i = 0; i < this.dataFields.length; i++) {
   var el = this.dataFields[i];
   var val = el.value;
   if (val != undefined  &&  (el.type != "checkbox"  ||  el.checked == true)  &&  (el.tagName != "CHECKBOX"  ||  el.checked == true)) {
    params = params + sep + encodeURIComponent(el.name) + "=" + encodeURIComponent(el.value);
    sep = "&";
   }
  }

  return params;

 }



 fromQueryString(qs) {

  var ps = qs.split("&");

  for (var i = 0; i < ps.length; i++) {
   var p = ps[i].split("=");
   var el = document.getElementById(decodeURIComponent(p[0]));
   var val = decodeURIComponent(p[1])
   if (el.type == "checkbox"  ||  el.tagName == "CHECKBOX")
    el.checked = (val != "");
   else if (val != undefined)
    el.value = val;
  }

 }

}



customElements.define("mc-form", McForm, { extends: "form" });
