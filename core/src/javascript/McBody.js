//
// MC's body.
//
// @version 2024-12-04
// @author lp
//

import ToolTip from "./ToolTip.js";
import PopUpMenu from "./PopUpMenu.js";
import McMark from "./McMark.js";

var toolTip;
var popUpMenus;
var fieldmap;


export default class McBody extends HTMLBodyElement {



 constructor() {

  super();

  popUpMenus = new Map();

 }



 connectedCallback() {

  toolTip = new ToolTip();

  document.body.style.cursor = 'default';

 }

}



customElements.define("mc-body", McBody, { extends: "body" });



window.McBody_showToolTip = function MCBody_showToolTip(event, content) {

 toolTip.show(event, content);

}



window.McBody_hideToolTip = function McBody_hideToolTip() {

 toolTip.hide();

}



window.McBody_setBusy = function McBody_setBusy(flag) {

 if (flag)
  document.body.style.cursor = 'wait';
 else
  document.body.style.cursor = 'default';

}



window.McBody_createPopUpMenu = function McBody_createPopUpMenu(name, content) {

 var men = popUpMenus.get(name);

 if (men == null) {
  var men = new PopUpMenu(content);

  popUpMenus.set(name, men);

 }

}



window.McBody_showPopUpMenu = function McBody_showPopUpMenu(event, name) {

 var men = popUpMenus.get(name);

 if (men.isShown())
  men.hide();
 else
  men.show(event);

}



window.McBody_call = function McBody_call(link) {

 if (window.mayLeavePage)
  if (!mayLeavePage()) return;

 McBody_setBusy(true);

 location.href = link;

}



window.McBody_showMark = function McBody_showMark(element, content) {

 McMark.showMark(element, content);

}



 window.McBody_setMark = function setMark(property, pos, length) {

  if (fieldmap == null)
   fieldmap = new Map();

  var positions = fieldmap.get(property);

  if (positions == undefined) {
   const npos = [];
   npos.push({pos: pos, length: length});
   fieldmap.set(property, npos);
  }
  else
   positions.push({pos: pos, length: length});

 }



 window.McBody_doMark = function doMark() {

  fieldmap.forEach(_markField);

 }



 function _markField(value, fieldname, map) {

  value.sort((a, b) => {
   if (a.pos < b.pos)
    return -1;
   else if (b.pos < a.pos)
    return 1;
   else
    return 0;
  });

  var el = document.getElementById(fieldname);
  var sd = el.value;
  var newVal = "";
  var oldXPos = 0;

  value.forEach((x, i) =>  {

   newVal = newVal
    + sd.substring(oldXPos, x.pos)
    + "<mark>"
    + sd.substring(x.pos, x.pos + x.length)
    + "</mark>";

   oldXPos = x.pos + x.length;

  });

  newVal = newVal
   + sd.substring(oldXPos);

  McBody_showMark(el, newVal);

 }
