class RubberQuoteDiv {

 constructor (div, quote, max) {
  this.div = div;
  this.quote = quote;
  this.max = max;
 };
}



class RubberBuddyDiv {

 constructor (div, buddy) {
  this.div = div;
  this.buddy = buddy;
 }
}



class Rubber {

 constructor(parent) {

  this.rubberQuoteDivs = new Array();
  this.rubberBuddyDivs = new Array();
  this.rubberDeco = new Array();

  var els = parent.children;
  var i;

  for (i = 0; i < els.length; i++) {
   var el = els[i];
   if (el.tagName == "DIV"  ||  el.tagName == "FORM") {
    var q = el.getAttribute("data-rubberQuote");
    if (q) {
     var m = el.getAttribute("data-rubberMax");
     if (!m)
      m = Number.MAX_SAFE_INTEGER;
     var rqd = new RubberQuoteDiv(el, q, m);
     this.rubberQuoteDivs.push(rqd);
    }
    else {
     var b = el.getAttribute("data-rubberBuddy");
     if (b) {
      var rbd = new RubberBuddyDiv(el, b);
      this.rubberBuddyDivs.push(rbd);
     }
     else
      this.rubberDeco.push(el);
    }
   }
  }

 }



 stretch(offset) {

  var last = -1;
  var sum = 0;
  var h;
  var m;
  var id;
  var decoHeight = 0;

  for (id = 0; id < this.rubberDeco.length; id++) {
   var el = this.rubberDeco[id];
   var style = window.getComputedStyle(el);
   decoHeight += el.offsetHeight;
   decoHeight += parseInt(style.getPropertyValue("margin-top"), 10);
   decoHeight += parseInt(style.getPropertyValue("margin-bottom"), 10);
  }

  var available = document.body.offsetHeight - decoHeight - offset;

  for (id = 0; id < this.rubberQuoteDivs.length; id++) {
   h = parseInt(available * this.rubberQuoteDivs[id].quote);
   m = this.rubberQuoteDivs[id].max;
   if (m == -1)
    m = this.rubberQuoteDivs[id].div.scrollHeight;
   if (h > m)
    h = this.rubberQuoteDivs[id].div.scrollHeight;

   this.rubberQuoteDivs[id].div.style.height = h;
   sum += h;
   last = id;
  }

  if (last > -1)
   this.rubberQuoteDivs[last].div.style.height = h + (available - sum);

  for (id = 0; id < this.rubberBuddyDivs.length; id++) {
   var el = document.getElementById(this.rubberBuddyDivs[id].buddy);
   this.rubberBuddyDivs[id].div.style.height = el.offsetHeight;
  }
 }

}