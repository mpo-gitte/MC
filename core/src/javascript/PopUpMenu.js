//
// Class for PopUpMenus.
//
// @version 2024-12-04
// @author lp
//

import Sprite from "./Sprite.js";

var me;
var display_time = 10000;



export default class PopUpMenu extends Sprite {



 constructor(content) {

  super("PopupMenu", undefined);
  super.hide();

  me = this;

  var me1 = this;
  this.div.addEventListener("mouseleave",
   function() {
    var dest = me1;

    dest._hide();
   }
  )

  var _popupmenutimer = null;
  this.pmcontent = content;

 }



 show(element) {

  super.hideSprites(null);

  super.show(null, element, this.pmcontent)

  if (this._popupmenutimer != null) {
   window.clearTimeout(this._popupmenutimer);
   this._popupmenutimer = null;
  }

  var me1 = this;
  this._popupmenutimer = window.setTimeout(
   function() {
    var dest = me1;

    dest._hide();
   },
   display_time
  );

 }



 getOffset() {

  return 2;

 }



 _hide() {

  window.clearTimeout(this._popupmenutimer);
  this._popupmenutimer = null;

  super.hide();

 }

}
