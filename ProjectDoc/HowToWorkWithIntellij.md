# How to work with IntelliJ

## Compiling

MC will be built with gradle (gradle wrapper 7.3). So have a look at

File->Settings->Build, Execution, Deployment->Build Tools->Gradle

1. Make sure for both "Build and run using:" and "Run tests using:" "Gradle" is selected
2. For "Distribution:" select "Wrapper"
3. For "Gradle JVM:" select Java 11 or Java 17



## Running

### If there is no PostgreSQL DB

Currently, MC only runs with PostgreSQL. So we have to connect to a Postgres-DB on which MC is allowed to
create tables and alter data. MC uses embedded Liquibase for DB initialization during startup.

Start psql and issue the following commands:

    create database mc_local;
    create user mc_local with password 'mc_password';
    grant all privileges on database mc_local to mc_local;

The database, user and password are examples!

Make sure Postgres accepts connections from other hosts than localhost with password. See
Postgres configuration in pg_hba.conf and postgresql.conf.



### Starting MC

Back in IntelliJ select "Edit Configurations...". Add a JAR application.

Select the WAR-file from build/libs.

Set the following environment variables to the DB connection from above:

    PUC_DB_Password=mc_password;\
    PUC_DB_URL=jdbc:postgresql://localhost/mc_local;\
    PUC_DB_User=mc_local

This is a minimum configuration! See "HowToStart.md" for more startup options.
