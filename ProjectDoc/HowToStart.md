# How to start MC

MC can be started in one of two ways:

1. Existing Tomcat.\
   Use the Management-Console of an installed Tomcat to install the WAR-File located in build/libs.
   
2. Use the embedded Tomcat.\
The WAR includes an embedded Tomcat. The WAR can be started as a normal JAR:\
   java -jar build/libs/MC-local.war\
MC starts up and begins to listen on port 8080 for HTTP requests.
   
## Command line parameters

Starting MC with the embedded Tomcat gives much more flexibility. The following command line parameters are used:

--httpPort=nnnn\
To specify an alternative port for HTTP requests. Default is 8080.

--ajpPort=nnnn\
Create an AJP connector with the given port.

--contextPath=xxxxx\
Without this argument the context path is the base name of the WAR. "MC-local" for instance.
With this argument an alternative context path can be specified.

## More command line parameters

MC uses MicroMeter to be monitored by Prometheus. There are some Java system properties for access from Prometheus:

-Dprometheus.port=nnnn\
Specifies an alternative port for Prometheus. Default is 9900.

-Dprometheus.user=xxxxx\
The port is protected with BasicAuth. This option specifies the user. Default is "prometheus".

-Dprometheus.password=xxxxx\
Specifies an alternative password for Prometheus. Default is "LightMyFire".

Afraid of having a password in the command line? Specify the above parameters as environment variables:
MC_prometheus_port, MC_prometheus_user and MC_prometheus_password.

## Clustering

### Session replication

MC can work in a cluster with session replication. If MC runs in this way no session stickyness is needed
in the load balancer.

Use the following command line parameters:

--clustering=true\
Enables clustering. Default is false.

--clusterSetJvmRoute\
The appendix to the session id. Default is the same as the context path.

--clusterBy\
This option specifies the way an MC instance find its neighbours.

If set to "multicast" a multicast request is issued to the address and port described below.

If set to "kubernetes" the Kubernetes-API is queried for other pods in the same namespace. For this mode
a Kubernetes NameSpace and a ServiceAccount with appropriate role and role bindings is needed. --- Of
course MC has to run in Kubernetes.

In this case the following options aren't used.

--clusterMulticastAddress\
Sets the Multicast address for the "rendevouz" of MC instances. Default is 228.0.0.4.

--clusterMulticastPort\
The port for the multicast. Default is 45564.

Both address and port define the membership of an MC instance in a cluster. All MC instances which
should work together are required to have the same values for address and port.

All those parameters can be specified as environment variables. Use: MC_clustering, MC_clusterBy,
MC_clusterSetJvmRoute, MC_clusterMulticastAddress and MC_clusterMulticastPort.

Sometimes clustering will not work due to firewall and / or routing settings. So think about:

    firewall-cmd --direct --add-rule ipv4 filter INPUT 0 -m udp -p udp -m pkttype --pkt-type multicast -j ACCEPT
    firewall-cmd --reload

If ufw is in use for configuring the firewall think about the following commands:

    ufw allow in proto udp to ADDRESS/4
    ufw allow in proto udp from ADDRESS/4

Replace ADDRESS with the configured multicast address.



### Messaging

When MC is running in a cluster it is important to avoid "split brain" behaviour caused by
caching data read from the persistence system.

Some environment variable have to be set:

MC_messenger\
has to be set to true to enable messaging at all.

MC_messenger_clusterBy\
Same behaviour as option "--clusterBy".

MC_messenger_multicastAddress\
set to an address for multicast. Default is 228.0.0.4.

MC_messenger_multicastPort\
set to the port for multicast. Default is 45564.

Address and port may be the same as for session replication!

At least tell the DataStoreManager to distribute signals:

PUC_DataStoreManager_distribute\
set it true to enable distributing of signals.

If MC runs in a cluster messaging becomes important. All instances communicate
with each other. And they have to "understand" each other. So the keys for all
crypted beans for all instances of MC in a cluster have to be the same!

In the chapter "Security" below you can find examples for setting the key for a 
bean. This has to be done for each bean. And each MC instance in a cluster has
to get those settings!

You can find a list of all application beans in the folder "application/src/application/beans".


## Database

MC uses Postgres for data storage. The database connection has to be specified by some environment variables:

PUC_DB_URL\
The URL of the database. Something like
"jdbc:postgresql://localhost/puc_local"

PUC_DB_User\
The name of the database user. This user has to be able to create, read and write tables and sequences.
MC uses Liquibase to create the database structure and to insert some demo data.

PUC_DB_Password\
The password for the above user.

## Security

In a production environment the following environment variables should be set to configure the encryption of IDs in HTTP requests:

PUC_SecretService_bean_application_beans_Ticket\
Set it to "agent=core.util.AgentAes\;password=YOURTOPSECTRETPASSWORD"

PUC_SecretService_bean_application_beans_Record\
Set it to "agent=core.util.AgentAes\;password=ANOTHERTOPSECRETPASSWORD"
