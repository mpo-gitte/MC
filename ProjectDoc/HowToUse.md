# How to use MC

The demo data included in this repository provide the following users:

| login name | Password  | Description                                                               |
|------------|-----------|---------------------------------------------------------------------------|
| guest      |           | the default user who is logged in after calling MC. Has only a few rights |
| mcreader   | Mcreader1 | a user with more rights than guest                                        |
| mcwriter   | Mcwriter1 | may create, edit and delete items                                         |
| mcmaster   | Mcmaster1 | has some administration functions                                         |

New users can be added with the "User management" in the "System"-Menu. This menu item is only visible
to the user "mcmaster".
