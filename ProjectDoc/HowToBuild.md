# How to build MC

MC can be build with Java 11. Just call the gradle task "build":

     sh gradlew build

This compiles, tests and builds the WAR.

The WAR can be found in build/libs .

## Other gradle tasks

|Task| Description                                          | Comment                                            |
|----|------------------------------------------------------|----------------------------------------------------|
|assemble|compile only||
|clear|remove all build files, temps and the WAR||
|test|run the unit tests||
|testWithDatabase|runs the database tests|Set the database connection with the environment variables db_user_test, db_password_test, db_dropable_test. You can find this settings in gradle.properties.|
|jar|build WAR only||
|makeDockerImage|builds and publishes the docker image. Creates a manifest list. | Environment variable DOCKER_REGISTRY has to be set.|
|dependencyCheckAnalyze|calls the OWASP Dependency check|| 

## Command line options

|Option| Description|Comment|
|------|------------|-------|
|mctag|sets the message for MCs about page|set via gradle property expansion. Default value is "First try". For the stages "NEW" and "FIX" this message will be displayed on the about-page.|
|mcstage|sets the stage|will be used as part of the WAR name. Default value is "local".|
|mcerrormode|sets the error mode|sets MC's error mode via gradle property expansion. If errormode is set to 1 a error message will be more detailed. |

Specify the options as "-Poption=value".
