var Title_onNavigationMenuEvent;
var Title_onMainMenuEvent;



function Title_onTitleBarButton(event, name, targetUrl) {

 McBody_call(targetUrl)

}



function Title_onTitleBarButtonContext(event, name) {

 if (name == "Back") {
  if (!event) event = window.event;

  var mel = document.getElementById("Title_navigationMenu");
  if (mel != undefined) {
   mel.fill();
   Title_onNavigationMenuEvent = event;
   return false;
  }
 }

}



function Title_showNavigationMenue() {

 var mel = document.getElementById("Title_navigationMenu");

 McBody_hideToolTip();
 McBody_createPopUpMenu("navigation", mel.innerHTML);
 McBody_showPopUpMenu(Title_onNavigationMenuEvent.target, "navigation");

 Title_onNavigationMenuEvent.stopPropagation();
 Title_onNavigationMenuEvent.preventDefault();

}



function Title_onClickMainMenu(event) {

 event.stopPropagation();
 event.preventDefault();

 var mel = document.getElementById("Title_mainMenu");

 if (mel != undefined) {
  mel.fill();
  Title_onMainMenuEvent = event;
 }

 return false;

}



function Title_showMainMenue() {

 var mel = document.getElementById("Title_mainMenu");

 McBody_createPopUpMenu("mainMenu", mel.innerHTML);
 McBody_showPopUpMenu(Title_onMainMenuEvent.target, "mainMenu");

}
