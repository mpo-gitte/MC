package application.beans;

import core.bakedBeans.JDBCBean;
import core.bakedBeans.PersistentBeanDescription;
import core.bakedBeans.PropertyDescription;
import core.bakedBeans.PropertyInfo;
import core.bakedBeans.TypedBeanDescription;
import core.bakedBeans.TypedBeanGetter;
import core.bakedBeans.TypedBeanSetter;

import java.time.LocalDateTime;



/**
 * Beans of this class hold descriptions for musical instruments.
 * 
 * @version 2023-09-05
 * @author pilgrim.lutz@imail.de
 *
 * @see Musician
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "description",
   name = "description",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "longDescription",
   pclass = String.class,
   setter = "setLongDescription",
   getter = "getLongDescription",
   dontBakeAliasSetter = true
  ),
  @PropertyDescription(
   alias = "mainInstrumentId",
   name = "main_instrument_id",
   pclass = String.class
  )
 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "instruments",
 keyName = "id"
)

public class Instrument extends JDBCBean {



 /**
  * Getter returns  a textual description for this instrument.
  *
  * @param pi The PropertyInfo (not used)
  *
  * @return A string with the textual description of this instrument.
  */

 @TypedBeanGetter
 public Object getLongDescription(PropertyInfo pi) {

  return (String)get("id") + " - " + (String)get("description");

 }



 /**
  * Dummy-Setter for the textual description. Does nothing.
  *
  * @param pi The PropertyInfo
  *
  * @param value The unused value.
  */

 @TypedBeanSetter
 public void setLongDescription(PropertyInfo pi, Object value) {

 }

}
