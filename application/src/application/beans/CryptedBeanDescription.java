package application.beans;

import java.lang.annotation.*;



/**
 * The annotation provides information about a crypted bean.
 *
 * @version 2021-06-25
 * @author pilgrim.lutz@imail.de
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface CryptedBeanDescription {



 public enum TimestampMode {

  /**
   * A new ("fresh") timestamp for each instance of a bean will be created.
   */
  fresh,

  /**
   * A predefined timestamp for each instance of a bean will be used.
   */
  constant,

 };



 boolean paranoid() default false;
 TimestampMode timestampMode() default TimestampMode.constant;

}