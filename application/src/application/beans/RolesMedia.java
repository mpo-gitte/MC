package application.beans;

import core.bakedBeans.*;

/**
 * This class defines the relation between roles and media..
 *
 * @version 2021-12-07
 * @author pilgrim.lutz@imail.de
 *
 * @see Role
 * @see Media
 *
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "roleId",
   name = "role_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "mediaId",
   name = "medium_id",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "subMedia",
   name = "sm",
   base = "mediaId",
   relationType = RelationType.dontSave,
   pclass = Media.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterNL"
  ),
 },
 bakeBean = true
)
@PersistentBeanDescription(
 source = "rolesmedia",
 keyName = "id",
 keyClass = Integer.class
)



public class RolesMedia extends JDBCBean {

}