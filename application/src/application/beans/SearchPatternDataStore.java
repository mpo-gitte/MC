package application.beans;

import java.util.List;

import core.bakedBeans.JDBCDataStore;
import core.bakedBeans.PersistentBean;



/**
 * Implements a DataStore for SearchPatterns.
 *
 * @version 2018-03-10
 * @author pilgrim.lutz@imail.de
 *
 */

public class SearchPatternDataStore extends JDBCDataStore<SearchPattern> {



 /**
  * Recognizes DataStoreSignal.delete and reloads the list of SearchPatterns if the SearchPatterns
  * in the list are owned by the owner of the deleted SearchPattern given in subjectBean.
  */

 @Override
 public void signal(DataStoreSignal sig, PersistentBean subjectBean) {

  super.signal(sig, subjectBean);

  if (sig == DataStoreSignal.delete) {
   // if search patterns are deleted the list has to be reloaded.
   reset();
   return;
  }

  SearchPattern sp = (SearchPattern)subjectBean;

  try {
   List<SearchPattern> al = read();
   if (al.size() > 0) {
    SearchPattern sp1 = al.get(0);
    // if SearchPatterns in list belonging to the owner of the subjectBean the list has to be resetted.
    if (((Integer)sp1.get("userId")).intValue() == ((Integer)sp.get("userId")).intValue())
     reset();
   }
   else
    reset();
  }
  catch (Exception e) {
   // This should not bother us.
  }

 }

}