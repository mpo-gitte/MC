package application.beans;

import core.bakedBeans.BeanList;
import core.bakedBeans.PropertyDescription;
import core.bakedBeans.TypedBean;
import core.bakedBeans.TypedBeanDescription;



/**
 * This bean holds statistics for musicians.
 *
 * @version 2023-05-19
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "musiciansAverageAge",
   name = "maa",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "musiciansMinAge",
   name = "mia",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "musiciansYoungestMusicians",
   name = "mym",
   pclass = BeanList.class,
   iclass = Musician.class,
   iclassPrefix = "baked"
  ),
  @PropertyDescription(
   alias = "musiciansEldestMusicians",
   name = "mem",
   pclass = BeanList.class,
   iclass = Musician.class,
   iclassPrefix = "baked"
  ),
  @PropertyDescription(
   alias = "musiciansEldestLivingMusicians",
   name = "melm",
   pclass = BeanList.class,
   iclass = Musician.class,
   iclassPrefix = "baked"
  ),
  @PropertyDescription(
   alias = "musiciansWithAge",
   name = "mwa",
   pclass = Long.class
  ),
  @PropertyDescription(
   alias = "musiciansWithDeath",
   name = "mwd",
   pclass = Long.class
  )
 },
 bakeBean = true
)



public class MusicianStatisticBean extends TypedBean {
}