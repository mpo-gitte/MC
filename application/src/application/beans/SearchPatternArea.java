package application.beans;

import core.bakedBeans.PropertyDescription;
import core.bakedBeans.StringBean;
import core.bakedBeans.TypedBeanDescription;



/**
 * This class is a helper for the class SearchPattern.
 *
 * <p>It holds three Booleans which tell the SearchEngine where to search.</p>
 *
 * @version 2017-10-21
 * @author pilgrim.lutz@imail.de

 * @see SearchPattern
 *
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "records",
   name = "records",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "titles",
   name = "titles",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "musicians",
   name = "musicians",
   pclass = Boolean.class
  )
 },
 bakeBean = true
)

public class SearchPatternArea extends StringBean {
}