package application.beans;

import core.bakedBeans.PersistentBean;

/**
 * This interface enables beans to be logged.
 *
 * <p>Sometime the LogbookWorker accepts only such beans.</p>
 *
 * @version 2023-09-05
 * @author lp
 *
 * @see application.workers.LogbookWorker
 */

public interface ILogable {



 /**
  * This method has to return a string with a content which is significant for a bean instances.
  *
  * @return A nice string.
  */

 public String significant();



 /**
  * Get the key value
  *
  * @return The key value.
  *
  * @see PersistentBean#getKeyValue()
  */

 public Object getKeyValue();



 /**
  * Returns if the ILogbable is empty.
  *
  * @return True if empty. False otherwise.
  * @see PersistentBean#isEmpty()
  */

 public boolean isEmpty();



 /**
  * Returns the correction statements for logbook correction by the logbook worker.
  *
  * @return An arry with the statments.
  * @see application.workers.LogbookHelper#correctLogbook(ILogable)
  */

 public String[] getLogBookCorrectionStatements();
 
}
