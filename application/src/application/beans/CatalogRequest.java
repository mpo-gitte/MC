package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;


/**
 * This class implements a "catalog request".
 *
 * @version 2022-07-24
 * @author pilgrim.lutz@imail.de
 *
 * @see Record
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "recordId",
   name = "record_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "subRecord",
   name = "sr",
   base = "recordId",
   loadWith = {"id", "interpreter"},
   relationType = RelationType.afterSave,
   pclass = Record.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterNL",
   dontBakeAliasSetter = true
  )
 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "catalogrequests",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)

public class CatalogRequest extends JDBCBean {

}