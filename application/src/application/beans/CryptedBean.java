package application.beans;

import core.bakedBeans.*;
import core.util.Utilities;



/**
 * Base class for all beans which use an encrypted key.
 *
 * <p>Provides getter and setter for encrypted keys.</p>
 *
 * @version 2024-08-11
 * @author lp
 */

public abstract class CryptedBean extends JDBCBean {

 private boolean paranoid = false;
 private static final long defaultTimestamp = System.currentTimeMillis();
 private CryptedBeanDescription.TimestampMode timestampMode = CryptedBeanDescription.TimestampMode.fresh;



 /**
  * Standard constructor.
  */

 public CryptedBean() {

  CryptedBeanDescription annotation = getAnnotation(CryptedBeanDescription.class);
  if (annotation != null) {
   this.paranoid = annotation.paranoid();
   this.timestampMode = annotation.timestampMode();
  }
 }



 /**
  * The setter for an encrypted key.
  *
  * @param pi The PropertyInfo
  * @param value The value for the key. This has to be an encrypted String whith the string representation of  BeanKey bean.
  *
  * @see BeanKey
  */

 @TypedBeanSetter
 public void cryptKeySetter(PropertyInfo pi, Object value) {

  logger.debug(() -> "cryptKeySetter(): " + value);

  if (Utilities.isEmpty(value))
   return;

  BeanKey k = BeanKey.makeBeanKey(getClassAlias());

  k.fromString((String)value);

  k.check(this.getClassAlias());

  if (Login.isFromExtern()  &&  paranoid)
   k.check((Integer)Login.get().get("userId"));

  stdSetter(getPropertyInfoByName(getKeyName()), k.getKey());

 }



 /**
  * The getter for an encrypted key.
  *
  * @param pi The PropertyInfo
  *
  * @return An encrypted string representation of a BeanKey bean.
  *
  * @see BeanKey
  */

 @TypedBeanGetter
 public Object cryptKeyGetter(PropertyInfo pi) {

  BeanKey bk = getBeanKey(getKeyValue(), getClassAlias(), timestampMode);

  return bk.toString();

 }



 /**
  * Helper method to create a BeanKey.
  *
  * @param id The id of the bean.
  * @param forClass The class of the bean.
  * @param timestampMode The mode for the timestamp.
  *
  * @return A BeanKey.
  */

 public static BeanKey getBeanKey(Object id, Class<? extends TypedBean> forClass, CryptedBeanDescription.TimestampMode timestampMode) {

  return
   BeanKey.makeBeanKey(
    id,
    forClass,
    Login.isFromExtern() ? (Integer)Login.get().get("userId") : 0,
    timestampMode == CryptedBeanDescription.TimestampMode.constant ? defaultTimestamp : System.currentTimeMillis()
   );

 }



 @Override
 protected boolean denyPropertySetFromString(PropertyInfo propertyInfo) {

  // Deny setting of key!

  if (this.getKeyName().equals(propertyInfo.getName()))
   return true;

  return
   super.denyPropertySetFromString(propertyInfo);

 }

}
