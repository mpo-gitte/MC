package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;


/**
 * This class implements a mapping for catalog requests.
 *
 * @version 2022-09-08
 * @author pilgrim.lutz@imail.de
 *
 * @see CatalogRequest
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "fromString",
   name = "fromstring",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "toString",
   name = "tostring",
   pclass = String.class
  )
 },
 bakeBean = true
)
@PersistentBeanDescription(
 source = "catalogmappings",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription(
 timestampMode = CryptedBeanDescription.TimestampMode.constant
)

public class CatalogMapping extends CryptedBean {

}