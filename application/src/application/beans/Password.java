package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;


/**
 * Bean for storing passwords.
 *
 * @version 2021-12-07
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias= "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "userId",
   name = "user_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "password",
   name = "password",
   pclass = String.class,
   maxLength = 64
  ),
  @PropertyDescription(
   alias = "salt",
   name = "salt",
   pclass = String.class,
   maxLength = 64
  ),
  @PropertyDescription(
   alias = "secretPurpose",
   name = "secretpurpose",
   pclass = String.class,
   maxLength = 64
  ),
  @PropertyDescription(
   alias = "expires",
   name = "expires",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias= "expirationCount",
   name = "expcount",
   pclass = Integer.class
  )
 },
 bakeBean = true
)
@PersistentBeanDescription(
 source = "passwords",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)



public class Password extends JDBCBean {

}