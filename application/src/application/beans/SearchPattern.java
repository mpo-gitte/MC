package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;
import java.util.List;



/**
 * Holds information for search patterns.
 *
 * <p>A SearchPattern contains the search string and his position in the data,
 * the media and the areas to search, whether only new records should be searched
 * and a description.<p>
 *
 * @version 2022-09-05
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription (
  properties = {
   @PropertyDescription(
    alias = "id",
    name = "id",
    pclass = Integer.class,
    serialize = SerializationMode.never
   ),
   @PropertyDescription(
    alias = "created",
    name = "created",
    pclass = LocalDateTime.class
   ),
   @PropertyDescription(
    alias = "lastaccess",
    name = "lastaccess",
    pclass = LocalDateTime.class
   ),
   @PropertyDescription(
    alias = "name",
    name = "name",
    pclass = String.class,
    maxLength = 64
   ),
   @PropertyDescription(
    alias = "userId",
    name = "user_id",
    pclass = Integer.class
   ),
   @PropertyDescription(
    alias = "searchstring",
    name = "searchstring",
    pclass = String.class,
    maxLength = 128
   ),
   @PropertyDescription(
    alias = "position",
    name = "position",
    pclass = Integer.class,
    dontBakeAliasSetter = true,
    dontBakeAliasGetter = true
   ),
   @PropertyDescription (
    alias = "enumPosition",
    base = "position",
    pclass = SearchPattern.position.class,
    setter = "enumSetter",
    getter = "enumGetter"
   ),
   @PropertyDescription(
    alias = "cryptKeyId",
    pclass = String.class,
    setter = "cryptKeySetter",
    getter = "cryptKeyGetter",
    serialize = SerializationMode.always
   ),
   @PropertyDescription(
    alias = "areas",
    name = "areas",
    pclass = String.class,
    maxLength = 64,
    dontBakeAliasSetter = true,
    dontBakeAliasGetter = true
   ),
   @PropertyDescription(
    alias = "relAreas",
    base = "areas",
    relationType = RelationType.beforeSave,
    pclass = SearchPatternArea.class,
    pclassPrefix = "baked",
    setter = "stringBeanSetter",
    getter = "stringBeanGetter"
   ),
   @PropertyDescription(
    alias = "media",
    name = "media",
    pclass = String.class,
    maxLength = 4,
    dontBakeAliasSetter = true,
    dontBakeAliasGetter = true
   ),
   @PropertyDescription(
    alias = "relMedia",
    base = "media",
    relationType = RelationType.beforeSave,
    pclass = BeanList.class,
    iclass = Media.class,
    iclassPrefix = "baked",
    setter = "beanListFromStringSetter",
    getter = "beanListFromStringGetter"
   ),
   @PropertyDescription(
    alias = "flags",
    name = "flags",
    pclass = String.class,
    maxLength = 64,
    dontBakeAliasSetter = true,
    dontBakeAliasGetter = true
   ),
   @PropertyDescription(
    alias = "relFlags",
    base = "flags",
    relationType = RelationType.beforeSave,
    pclass = SearchPatternFlags.class,
    pclassPrefix = "baked",
    setter = "stringBeanSetter",
    getter = "stringBeanGetter"
   ),
  },
 bakeBean = true
 )
@PersistentBeanDescription (
  source = "searchpatterns",
  keyName = "id",
  keyMakerClass = JDBCSequenceKeyMaker.class
 )
@CryptedBeanDescription (
 paranoid = true
)



public class SearchPattern extends CryptedBean {

 // DON'T CHANGE ORDER! DON'T INSERT ITEMS!
 public enum position {
  /**
   * Beginning of string
   */
  prefix,
  /**
   * within the string
   */
  within,
  /**
   * at the end of the string
   */
  postfix,
  /*
   * exact match
   */
  exact
 }



 /**
  * Finds a SearchPattern with the given parameters.
  *
  * @param props Parameters for searching.
  * @return The resulting beans in a list or null if nothing found.
  */

  @Override
  public List<? extends SearchPattern> find(String ... props) {

   saveRelations(RelationType.beforeSave);  // Flatten the bean. Note: We've got no real relations --- only some string representations!

   @SuppressWarnings("unchecked")
   List<? extends SearchPattern> sps = (List<? extends SearchPattern>) super.find(props);

   return sps;

  }



 /**
  * Makes a describing text for the SearchPattern. Usually used in a template.
  *
  * @return The string with the description.
  */

  public String getDescription(String searchPageAreaRecords, String searchPageAreaTitles, String searchPageAreaMusicians, String searchPageLabelSearchNoChildren) {

   StringBuilder sb = new StringBuilder();

   if (Utilities.isTrue((Boolean)(((SearchPatternFlags)get("relFlags")).get("onlyNew"))))
    sb.append("* ");

   String sd = (String)get("searchstring");
   if (!core.util.Utilities.isEmpty(sd))
    sb.append(sd);

   sb.append(" (");

   String msep = "";
   for (Object o : (BeanList)get("relMedia")) {
    Media med = (Media)o;
    if (med != null) {
     sb.append(msep).append(med.get("id"));
     msep = ", ";
    }
   }

   sb.append(") (");

   SearchPatternArea areas = (SearchPatternArea)get("relAreas");
   msep = "";
   if (Utilities.isTrue(areas.get("records"))) {
    sb.append(searchPageAreaRecords);
    msep = ", ";
   }
   if (Utilities.isTrue(areas.get("titles"))) {
    sb.append(msep).append(searchPageAreaTitles);
    msep = ", ";
   }
   if (Utilities.isTrue(areas.get("musicians")))
    sb.append(msep).append(searchPageAreaMusicians);

   sb.append(")");

   if (Utilities.isTrue(((SearchPatternFlags)get("relFlags")).get("noChildren")))
    sb.append(msep).append(searchPageLabelSearchNoChildren);

   return sb.toString();

  }

}
