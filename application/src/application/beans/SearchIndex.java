package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;


/**
 * This class implements a "search index".
 *
 * @version 2024-06-19
 * @author lp
 *
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "beanName",
   name = "beanname",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "propertyAlias",
   name = "propertyalias",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "value",
   name = "value",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "recordId",
   name = "record_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "titleId",
   name = "title_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "musicianId",
   name = "musician_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "musicianaliasId",
   name = "musicianalias_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "instrumentId",
   name = "instrument_id",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "subRecord",
   name = "sr",
   base = "recordId",
   relationType = RelationType.loose,
   pclass = Record.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetter"
  ),
  @PropertyDescription(
   alias = "subTitle",
   name = "sr",
   base = "titleId",
   relationType = RelationType.loose,
   pclass = Title.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetter"
  ),
  @PropertyDescription(
   alias = "subMusician",
   name = "sr",
   base = "musicianId",
   relationType = RelationType.loose,
   pclass = Musician.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetter"
  ),
  @PropertyDescription(
   alias = "subAlias",
   name = "sr",
   base = "aliasId",
   relationType = RelationType.loose,
   pclass = Musician.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetter"
  ),
  @PropertyDescription(
   alias = "method",
   name = "method",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "position",
   name = "position",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "length",
   name = "length",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "andOther",
   name = "andot",
   relationType = RelationType.loose,
   pclass = BeanList.class,
   iclass = SearchIndex.class,
   iclassPrefix = "baked"
  ),

 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "searchindex",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription(
 timestampMode = CryptedBeanDescription.TimestampMode.constant
)

public class SearchIndex extends CryptedBean {

}
