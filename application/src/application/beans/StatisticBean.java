package application.beans;

import core.bakedBeans.*;


/**
 * This class is a simple container for results produced by the StatisticsWorker.
 * <p>It has to be derived from JDBCBean because it will be fetched by a JDBCDataStore.
 * And that companion doesn't like other beans than JDBCBeans. :-)/p>
 *
 * @version 2022-01-22
 * @author pilgrim.lutz@imail.de
 *
 * @see application.workers.StatisticsWorker
 * @see JDBCDataStore
 *
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription (
   alias = "subMedia",
   name = "med",
   relationType = RelationType.afterSave,
   pclass = Media.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterNL"
  ),
  @PropertyDescription (
   alias = "subInstrument",
   name = "instr",
   relationType = RelationType.afterSave,
   pclass = Instrument.class,
   setter = "relN2OSetter",
   getter = "relN2OGetterNL"
  ),
  @PropertyDescription (
   alias = "subMusician",
   name = "sm",
   relationType = RelationType.afterSave,
   pclass = Musician.class,
   setter = "relN2OSetter",
   getter = "relN2OGetterNL"
  ),
  @PropertyDescription(
   alias = "issuer",
   name = "issuer",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "number",
   name = "number",
   pclass = Long.class
  ),
  @PropertyDescription(
   alias = "numberWithTitles",
   name = "numberWithTitles",
   pclass = Long.class
  ),
  @PropertyDescription(
   alias = "numberWithMusicians",
   name = "numberWithMusicians",
   pclass = Long.class
  ),
  @PropertyDescription(
   alias = "ayear",
   name = "ayear",
   pclass = Integer.class
  ),
 },
 bakeBean = true
)

@PersistentBeanDescription (
 source = "-",
 keyName = "id"
)



public class StatisticBean extends JDBCBean {

}
