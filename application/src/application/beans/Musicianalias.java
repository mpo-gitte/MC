package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;



/**
 * This class holds an alias name for a musician.
 *
 * @version 2023-09-30
 * @author lp
 *
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription (
   alias = "musicianId",
   name = "musician_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "name",
   name = "name",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "reason",
   name = "reason",
   pclass = String.class
  ),
 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "musicianalias",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription(
 timestampMode = CryptedBeanDescription.TimestampMode.constant
)

public class Musicianalias extends CryptedBean {

}
