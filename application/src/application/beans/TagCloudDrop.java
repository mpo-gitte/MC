package application.beans;

import core.bakedBeans.*;



/**
 * This class is a simple container for results produced by the TagCloudWorker.
 *
 * @version 2023-09-07
 * @author pilgrim.lutz@imail.de
 *
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias = "label",
   name = "label",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "number",
   name = "number",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "weight",
   name = "weight",
   pclass = Double.class
  ),
  @PropertyDescription(
   alias = "type",
   name = "type",
   pclass = Integer.class,
   dontBakeAliasSetter = true,
   dontBakeAliasGetter = true
  ),
  @PropertyDescription(
   alias = "CatalogItemId",
   name = "catalogItemId",
   pclass = Integer.class
  ),
 },
 bakeBean = true
)



public class TagCloudDrop extends TypedBean {


 /**
  * A property method to get the CatalogItemId encrypted.
  *
  * @return The encrypted id.
  */

 public String getEncryptedCatalogItemId() {

  return
   CryptedBean.getBeanKey(
    get("CatalogItemId"), CatalogItem.class, CryptedBeanDescription.TimestampMode.constant
   ).toString();

 }

}
