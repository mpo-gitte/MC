package application.beans;

import core.bakedBeans.PersistentBean;
import org.apache.logging.log4j.Logger;



/**
 * DataStore for storing collection on records.
 *
 * @version 2022-01-09
 * @author pilgrim.lutz@imail.de
 */

public class RecordsCollectionDataStore extends ResetDataStore {

 private static Logger logger;



 /**
  * Assumes subjectBean is a Job. Gets the record id and checks if this instance is concerned.
  *
  * @param sig The signal.
  * @param subjectBean The bean which causes the signal. Has to be a Record!  If null this
  *                    DataStore is always concerned.
  */

 @Override
 public void signal(DataStoreSignal sig, PersistentBean subjectBean) {

  Record rec = (Record)subjectBean;

  try {
   if (rec == null  ||  maker.getValues()[0].equals(rec.get("id"))) {  // Parameter 0 holds record id.
    // If equal this datastore hangs on that record and has to be resseted. Is done by super class.
    if (logger.isDebugEnabled())
     logger.debug("signal(): for " + rec.get("id") + " my bean!");

    super.signal(sig, subjectBean);
   }
  }
  catch (Exception e) {
   /* Does not bother us. */
   logger.error("signal(): Got an exception: ", e);
  }

 }

}
