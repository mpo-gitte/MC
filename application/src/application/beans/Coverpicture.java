package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;



/**
 * This bean holds data for cover pictures.
 *
 * @version 2023-09-05
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "description",
   name = "description",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "origin",
   name = "origin",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "filename",
   name = "filename",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "nr",
   name = "nr",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "recordId",
   name = "record_id",
   pclass = Integer.class
  ),
 },
 bakeBean = true
)
@PersistentBeanDescription(
 source = "coverpictures",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)



public class Coverpicture extends CryptedBean {



 /**
  * Returns the field filename or the id if filename is empty.
  *
  * @return filename
  */

 public String getSmartFilename() {

  String sd = (String)get("filename");

  if (Utilities.isEmpty(sd))
   return ((Integer)get("id")).toString() + ".jpg";

  return sd;

 }



 @Override
 protected boolean maySave() {

  return Login.get().may("editCoverpictures");

 }



 @Override
 protected boolean maySaveNew() {

  return
   Login.get().may("editCoverpictures");

 }



 @Override
 protected boolean mayDelete() {

  return
   Login.get().may("editCoverpictures");

 }

}
