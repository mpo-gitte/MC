package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;



/**
 * This class implements a "favourite record".
 *
 * <p>A favourite has an own key and a record inside. It is an entry of a FavouritesList.</p>
 *
 * @version 2021-12-06
 * @author pilgrim.lutz@imail.de
 *
 * @see FavouritesList
 * @see Record
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "favouriteslistId",
   name = "favouriteslist_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "recordsId",
   name = "record_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "info",
   name = "info",
   pclass = String.class,
   maxLength = 256
  ),
  @PropertyDescription (
   alias = "cryptKeyId",
   relationType = RelationType.none,
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   alias = "orderNr",
   name = "ordernr",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "subRecord",
   name = "sr",
   base = "recordsId",
   loadWith = {"id", "created", "interpreter", "title", "releaseyear", "number", "mediumId", "label", "ordernumber"},
   relationType = RelationType.afterSave,
   pclass = Record.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterNL",
   dontBakeAliasSetter = true
  )
 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "favourites",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription (
 paranoid = true
)

public class Favourites extends CryptedBean {

}