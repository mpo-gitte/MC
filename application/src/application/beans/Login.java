package application.beans;

import baked.application.beans.Setting;
import core.bakedBeans.*;
import core.base.Request;
import core.util.Utilities;

import java.time.LocalDateTime;
import java.util.List;



/**
 * This class holds some information about the current user and his session.
 *
 * @version 2024-10-29
 * @author lp
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "sessionId",
   name = "session_id",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "userId",
   name = "user_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "address",
   name = "address",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "host",
   name = "host",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "effectiveRights",
   name = "effective_rights",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "autoLogin",
   name = "autologin",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "ticketId",
   name = "ticket_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "clientId",
   name = "client_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "lastseen",
   name = "lastseen",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "relEffectiveRights",
   base = "effectiveRights",
   relationType = RelationType.dontSave,
   pclass = Rights.class,
   setter = "stringBeanSetter",
   getter = "stringBeanGetter"
  ),
  @PropertyDescription (
   alias = "refUser",
   base = "userId",
   relationType = RelationType.dontSave,
   pclass = User.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   category = "users"
  ),
  @PropertyDescription (
   alias = "refClient",
   base = "clientId",
   relationType = RelationType.dontSave,
   pclass = Client.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   category = "users"
  ),
  @PropertyDescription (
   alias = "refTicket",
   base = "ticketId",
   relationType = RelationType.dontSave,
   pclass = Ticket.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   category = "users"
  ),
 },
 bakeBean = true
)

@PersistentBeanDescription(
 source = "logins",
 keyName = "session_id",
 keyClass = String.class
)



public class Login extends JDBCBean implements RestrictionSource {

 public static final String MC_GUEST_USERNAME = "guest";



 /**
  * This bean uses the session id as its key.
  */

 @Override
 protected void makeKeyStart() {

  this.setByName(this.getKeyName(), Request.get().getSessionId());

 }



 /**
  * This bean uses the session id as its key.
  *
  * <p>This implementation does nothing.</p>
  *
  * @see Login#makeKeyStart
  */

 @Override
 protected void makeKeyEnd() {}



 /**
  * Ask if a user has the given right.
  *
  * <p>Rights are stored in a Rights bean in the user Role bean.</p>

  * @param what The name of the right.
  *
  * @return Tru if the user has the right. False otherwise.
  *
  * @see Role
  * @see Rights
  */

 public Boolean may(String what) {

  return
   Utilities.isTrue(((Rights)get("relEffectiveRights")).get(what));

 }



 /**
  * Asks whether the current user is the guest user.
  *
  * @return True if the current user is the guest user. False otherwise.
  */

 public Boolean getIsGuest() {

  return MC_GUEST_USERNAME.equals(((User)get("refUser")).get("loginname"));

 }



 /**
  * Returns whether a user is logged in.
  *
  * @return true if a user is logged in. False otherwise.
  */

 public Boolean isLoggedIn() {

  return !Utilities.isEmpty(get("userId"));

 }



 /**
  * Restricts access to media specified in the current users role.
  *
  * <p>Implemented to use a Login bean as a restriction source in queries.</p>
  *
  * @param ref This is the bean which asks for a restriction.
  *
  * @param purpose These parameters tell the restriction source for which kind of restriction the bean asks.
  *
  * @return A list filled with the Login bean.
  */

 public List<? extends PersistentBean> getMediaRestriction(PersistentBean ref, RestrictionSource.Purpose purpose) {

  if (purpose == RestrictionSource.Purpose.SelectingRecords) {
   @SuppressWarnings("unchecked")
   List<? extends PersistentBean> res = (List) ((Role) ((User)get("refUser")).get("refRole")).get("colRolesMedia");
   return res;
  }

  return null;

 }



 /**
  * Gets the current login via request.
  *
  * @return The Login-bean.
  *
  * @see Request#getSessionBeanViaSessionId(Class)
  */

 public static Login get() {

  try {
   Login sb = (Login) Request.get().getSessionBeanViaSessionId(Login.class);

   sb.set("lastseen", LocalDateTime.now());
   sb.save();
   // ToConsider: Does this have any performance issues?
   DataStoreManager.get().signal(DataStore.DataStoreSignal.write, sb, "logins");

   return sb;
  } catch (Exception e) {
   throw new SecurityException(e);
  }

 }



 /**
  * Check whether we're in call from extern or from intern.
  *
  * @return true if we're called from a request.
  */

 public static boolean isFromExtern()  {

  return
   !Utilities.isEmpty(Request.get());

 }



 /**
  * Get a setting for the user.
  *
  * <p>Common settings will be used first!</p>
  *
  * @param key The name of the setting
  *
  * @return The value of the setting.
  */

 public String getSetting(String key) {

//  @SuppressWarnings("unchecked")
  int userId = (Integer)this.get("userId");

  String settingValue =
   Setting.getSetting(key, userId);

  if (settingValue == null) {
   settingValue =
    Setting.getSetting(key);
  }

  return
   settingValue;

 }

}
