package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;



/**
 * This class holds information about a black sheep.
 *
 * @version 2023-09-21
 * @author lp
 *
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "userId",
   name = "user_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "subUser",
   name = "subuser",
   base = "userId",
   relationType = RelationType.dontSave,
   pclass = User.class,
   setter = "relN2OSetter",
   getter = "relN2OGetterNL",
   dontBakeAliasSetter = true
  ),
  @PropertyDescription(
   alias = "reason",
   name = "reason",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "address",
   name = "address",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "port",
   name = "port",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "sessionId",
   name = "session_id",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "clientId",
   name = "client_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "refClient",
   base = "clientId",
   relationType = RelationType.dontSave,
   pclass = Client.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   category = "users"
  ),
  @PropertyDescription(
   alias = "lastseen",
   name = "lastseen",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "counter",
   name = "counter",
   pclass = Integer.class
  ),
 },
 bakeBean = true
)
@PersistentBeanDescription(
 source = "blacksheep",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription(
 timestampMode = CryptedBeanDescription.TimestampMode.constant
)



public class Blacksheep extends CryptedBean {

}
