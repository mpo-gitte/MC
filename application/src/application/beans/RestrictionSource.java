package application.beans;



import core.bakedBeans.PersistentBean;

import java.util.List;



/**
 * This interface specifies an object which restricts selections.
 *
 * @version 2008-02-09
 * @author lp
 */

public interface RestrictionSource {



 /**
  * Constants for modes.
  *
  * @see RestrictionSource#getMediaRestriction(PersistentBean ref, Purpose purpose)
  */

 public enum Purpose {
  SelectingRecords,
 }



 /**
  * This method retrieves a restriction which restricts access to media.
  *
  * @param ref This is the bean which asks for a restriction.
  * @param purpose This parameters tells the restriction source for which kind of restriction the bean asks.
  * @return A list filled with PersistentBeans which make up the restriction or null if no restriction is available.
  */

 public List<? extends PersistentBean> getMediaRestriction(PersistentBean ref, Purpose purpose);

}
