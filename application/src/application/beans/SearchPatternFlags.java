package application.beans;

import core.bakedBeans.PropertyDescription;
import core.bakedBeans.StringBean;
import core.bakedBeans.TypedBeanDescription;



/**
 * Bean for flags for SearchPatterns.
 *
 * @version 2024-09-15
 * @author lp
 *
 * @see SearchPattern
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "onlyNew",
   name = "on",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "noChildren",
   name = "noc",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "onlyIndexSearch",
   name = "ois",
   pclass = Boolean.class
  )
 },
 bakeBean = true
)

public class SearchPatternFlags extends StringBean {
}
