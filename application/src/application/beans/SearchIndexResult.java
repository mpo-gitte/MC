package application.beans;

import core.bakedBeans.*;
import core.exceptions.BeanStructureException;

import java.time.LocalDateTime;


/**
 * This class implements a "search index".
 *
 * @version 2024-06-19
 * @author lp
 *
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   alias = "number",
   name = "number",
   base = "id",
   propertyFunction = PropertyFunction.count,
   pclass = Long.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "beanName",
   name = "beanname",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "propertyAlias",
   name = "propertyalias",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "value",
   name = "value",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "recordId",
   name = "record_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "titleId",
   name = "title_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "musicianId",
   name = "musician_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "musicianaliasId",
   name = "musicianalias_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "instrumentId",
   name = "instrument_id",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "subRecord",
   name = "sr",
   base = "recordId",
   relationType = RelationType.loose,
   pclass = Record.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetter"
  ),
  @PropertyDescription(
   alias = "subTitle",
   name = "sr",
   base = "titleId",
   relationType = RelationType.loose,
   pclass = Title.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetter"
  ),
  @PropertyDescription(
   alias = "subMusician",
   name = "sr",
   base = "musicianId",
   relationType = RelationType.loose,
   pclass = Musician.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetter"
  ),
  @PropertyDescription(
   alias = "subAlias",
   name = "sr",
   base = "aliasId",
   relationType = RelationType.loose,
   pclass = Musician.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetter"
  ),
  @PropertyDescription(
   alias = "method",
   name = "method",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "position",
   name = "position",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "length",
   name = "length",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "allValues",
   name = "allvalues",
   base = "value",
   propertyFunction = PropertyFunction.concat,
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "andOther",
   name = "andot",
   relationType = RelationType.loose,
   pclass = BeanList.class,
   iclass = SearchIndexResult.class,
   iclassPrefix = "baked"
  ),

 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "searchindex",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription(
 timestampMode = CryptedBeanDescription.TimestampMode.constant
)

public class SearchIndexResult extends CryptedBean {



 /**
  * Returns the alias of the property for referencing.
  *
  * @return the property alias.
  */

  public String getRefIdAlias() {

    switch ((String)this.get("beanName")) {

     case "Record":
      return "recordId";

     case "Title":
      return "titleId";

     case "Musician":
      return "musicianId";

     case "Musicianalias":
      return "musicianaliasId";

     case "Instrument":
      return "instrumentId";

    }

    throw
     new BeanStructureException("No refIdAlias!");

  }

}
