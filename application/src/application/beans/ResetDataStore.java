package application.beans;

import core.bakedBeans.DataStore;
import core.bakedBeans.JDBCDataStore;
import core.bakedBeans.PersistentBean;


/**
 * Special DataStore which reacts to signals.
 *
 * <p>When it gets signaled it resets its content.</p>
 *
 * @version 2022-01-16
 * @author pilgrim.lutz@imail.de
 *
 */

public class ResetDataStore extends JDBCDataStore {



 /**
  * This method gets called by the DataStoreManager and resets the DataStore.
  *
  * @param sig The signal.
  * @param subjectBean The bean which causes the signal.
  * @see DataStore
  */

 @Override
 public void signal(DataStoreSignal sig, PersistentBean subjectBean) {

  super.signal(sig, subjectBean);

  // Always reset the DataStore.
  reset();

 }

}