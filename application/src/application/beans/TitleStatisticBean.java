package application.beans;

import core.bakedBeans.PropertyDescription;
import core.bakedBeans.TypedBean;
import core.bakedBeans.TypedBeanDescription;


/**
 * This bean holds statistics for titles.
 *
 * @version 2022-02-16
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "totalTitleCount",
   name = "ttc",
   pclass = Long.class
  ),
  @PropertyDescription(
   alias = "knownTitleCount",
   name = "ktc",
   pclass = Long.class
  ),
  @PropertyDescription(
   alias = "unknownTitleCount",
   name = "utc",
   pclass = Long.class
  ),
  @PropertyDescription(
   alias = "estimatedAverage",
   name = "eav",
   pclass = Long.class
  ),
  @PropertyDescription(
   alias = "estimatedUnknown",
   name = "eua",
   pclass = Long.class
  ),
  @PropertyDescription(
   alias = "estimatedTotal",
   name = "eto",
   pclass = Long.class
  ),
  @PropertyDescription(
   alias = "knownTotalDuration",
   name = "ktd",
   pclass = Long.class
  ),
 },
 bakeBean = true
)



public class TitleStatisticBean extends TypedBean {
}