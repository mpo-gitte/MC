package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;



/**
 * Beans of this class hold jobs for titles.
 *
 * <p>A job is combination of an instrument and a musician.  It "hangs" on a title.</p>
 * 
 * @version 2021-12-17
 * @author pilgrim.lutz@imail.de
 *
 * @see Musician
 * @see Title
 * @see Instrument
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "titleId",
   name = "title_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "musicianId",
   name = "musician_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "instrumentId",
   name = "instrument_id",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription (
   alias = "subTitle",
   name = "subtit",
   relationType = RelationType.dontSave,
   pclass = Title.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterNL",
   base = "titleId",
   loadWith = {"id", "interpreter", "title"}
  ),
  @PropertyDescription (
   alias = "subMusician",
   name = "sm",
   base = "musicianId",
   relationType = RelationType.beforeSave,
   pclass = Musician.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterNL",
   loadWith = {"id", "name"}
  ),
  @PropertyDescription (
   alias = "subInstrument",
   name = "si",
   relationType = RelationType.dontSave,
   pclass = Instrument.class,
   setter = "relN2OSetter",
   getter = "relN2OGetterNL",
   base = "instrumentId",
   loadWith = {"id"}
  )
 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "jobs",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)



public class Job extends CryptedBean {



 /**
  * Standard constructor.
  */

 public Job() {

  super();

 }



 /**
  * Checks whether this job equals another job.
  *
  * @param otherBean The other job.
  * @return true if they're equal. False otherwise.
  */

 @Override
 public boolean equals(Object otherBean) {

  Bean b = (Bean)otherBean;

  try {
   if (this.get("musicianId").equals(b.get("musicianId")) &&
       this.get("instrumentId").equals(b.get("instrumentId")))
    return true;
  }
  catch (Exception e) { /*  Doesn't bother us. Result ist simply false. */}

  return false;
 }
}