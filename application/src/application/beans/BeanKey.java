package application.beans;

import core.bakedBeans.*;
import core.exceptions.SecurityException;
import core.util.SecretService;
import org.apache.logging.log4j.Logger;



/**
 * This class represents keys in MC.
 *
 * <p>Keys are encrypted and tagged with a user id, class and a timestamp.</p>
 *
 * <p>Because application beans which use this class may have different kinds of primary keys this bean may have
 * a different structure per instance!</p>
 *
 * @version 2023-09-05
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "classHash",
   name = "c",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "userId",
   name = "u",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "t",
   pclass = Long.class
  ),
  @PropertyDescription(
   alias = "k",
   name = "k",
   pclass = Integer.class
  )
 }
)



public class BeanKey extends StringBean {

 private static Logger logger;
 private String className;



 /**
  * Factory method for making a BeanLKey for the given key class.
  *
  * @param forClass The class of the calling bean.
  *
  * @return A new BeanKey.
  */

 static BeanKey makeBeanKey(Class<? extends TypedBean> forClass) {

  BeanKey beanKey = BeanFactory.make(BeanKey.class);

  beanKey.className = "bean." + forClass.getName();

  return beanKey;

 }



 /**
  * Factory method for making a new BeanKey for the given bean class.
  * 
  * @param keyValue The name of the key value of the bean class.
  * @param forClass The bean class.
  * @param timestamp A timestamp for the BeanKey
  *
  * @return A new BeanKey.
  */

 static BeanKey makeBeanKey(Object keyValue, Class<? extends TypedBean> forClass, Integer userId, long timestamp) {

  BeanKey beanKey = BeanFactory.make(BeanKey.class);

  beanKey.className = "bean." + forClass.getName();

  beanKey.set("k", keyValue);
  beanKey.set("classHash", forClass.getName().hashCode());
  beanKey.set("created", timestamp);
  beanKey.set("userId", userId);

  return beanKey;

 }



 /**
  * Returns the content of the bean encrypted.
  *
  * @return The encrypted content of all bean properties.
  */

 @Override
 public String toString() {

  return SecretService.get().getAgent(className).encrypt(super.toString());

 }



 /**
  * Builds the bean from encypted data.
  *
  * @param str The encrypted String whith the beans properties.
  */

 @Override
 public void fromString(String str) {

  super.fromString(SecretService.get().getAgent(className).decrypt(str));

 }

 

 /**
  * Returns the key from this BeanKey.
  * 
  * @return The key.
  */
 
 public Object getKey() {

  return get("k");

 }


 
 /**
  * Checks whether this BeanKey is suitable for the given bean class.
  * 
  * @param theClass the class for the test.
  */

  void check(Class<? extends TypedBean> theClass) {

  if (((Integer)get("classHash")) != theClass.getName().hashCode())
   throw new SecurityException("Key corrupted!");
  
 }


 
 /**
  * Checks whether this BeanKey ist suitable for the given user id.
  * 
  * @param uid the user id for the test.
  */

 public void check(Integer uid) {

  if (((Integer)get("userId")).intValue() != uid.intValue())
   throw new SecurityException("Key corrupted!");

 }

}
