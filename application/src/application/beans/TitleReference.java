package application.beans;

import core.bakedBeans.*;



/**
 * Bean for TitleReferences.
 *
 * @version 2021-12-07
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "userId",
   name = "user_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "titleId",
   name = "title_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "purpose",
   name = "purpose",
   pclass = Integer.class,
   dontBakeAliasSetter = true,
   dontBakeAliasGetter = true
  ),
  @PropertyDescription (
   alias = "subTitle",
   name = "st",
   base = "titleId",
   relationType = RelationType.dontSave,
   pclass = Title.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterNL"
  ),
  @PropertyDescription (
   alias = "enumPurpose",
   base = "purpose",
   pclass = TitleReference.purposes.class,
   setter = "enumSetter",
   getter = "enumGetter"
  )
 },
 bakeBean = true
)
@PersistentBeanDescription(
 source = "titlereferences",
 keyName = "title_id"
)



public class TitleReference extends JDBCBean {



 /**
  * Enum with purposes for entries.
  */

 // DON'T CHANGE ORDER! DON'T INSERT ITEMS! --- NEVER EVER!
 public enum purposes {

  /**
   * No sensefull reason. Should not be used.
   */
  none,
  /**
   * TitleReference is part of a temporary result.
   */
  tempResult,
 }



 /**
  * The standard constructor.
  */

 public TitleReference() {

  super();

 }

}