package application.beans;



import core.bakedBeans.*;
import core.util.Utilities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;



/**
 * This bean holds data for a musician
 *
 * @version 2023-09-30
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "name",
   name = "name",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "comment",
   name = "comment",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "born",
   name = "born",
   pclass = LocalDate.class
  ),
  @PropertyDescription(
   alias = "bornIn",
   name = "bornin",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "died",
   name = "died",
   pclass = LocalDate.class
  ),
  @PropertyDescription(
   alias = "diedIn",
   name = "diedin",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "instrumentId",
   name = "instrument_id",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   alias = "flags",
   name = "flags",
   pclass = Integer.class,
   dontBakeAliasSetter = true,
   dontBakeAliasGetter = true
  ),
  @PropertyDescription(
   alias = "appearsInBirthdayCalendar",
   base = "flags",
   pclass = Boolean.class,
   getter = "bitMaskGetter",
   setter =  "bitMaskSetter",
   bitMask = Musician.BITMASK_APPEARS_IN_BIRTHDAY_CALENDAR
  ),
  @PropertyDescription(
   alias = "appearsInBirthdayCalendarMainMenu",
   base = "flags",
   pclass = Boolean.class,
   getter = "bitMaskGetter",
   setter =  "bitMaskSetter",
   bitMask = Musician.BITMASK_APPEARS_IN_BIRTHDAY_CALENDAR_MAINMENU
  ),
  @PropertyDescription(
   alias = "colMusicianAliases",
   base = "musicianId",
   relationType = RelationType.afterSave,
   pclass = JDBCDataStore.class,
   iclass = Musicianalias.class,
   iclassPrefix = "baked",
   setter = "collectionSetter",
   getter = "collectionGetter",
   dataStoreName = "core.bakedBeans.JDBCDataStore",
   loadWith = {"id", "name", "reason"},
   orderBy = {"application.beans.Musicianalias:name", "application.beans.Musicianalias:reason"},
   category = "musicians",
   dontBakeAliasSetter = true
  ),
 },
 bakeBean = true
)
@PersistentBeanDescription(
 source = "musicians",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription(
 timestampMode = CryptedBeanDescription.TimestampMode.constant
)



public class Musician extends CryptedBean implements ILogable {

 public static final int BITMASK_APPEARS_IN_BIRTHDAY_CALENDAR = 1;
 public static final int BITMASK_APPEARS_IN_BIRTHDAY_CALENDAR_MAINMENU = 2;



 /**
  * Checks whether a musician is in the DataBase.
  *
  * <p>Two musicians are distinct if the name and the main instrument differ.</p>
  *
  * @return True: He's not in the DataBase. False otherwise.
  */

 public boolean isUnique() {

  List<Musician> res =
   DataStoreManager.get().makeDataStore(
    "core.bakedBeans.JDBCDataStore",
    "musicians",
    "select 1 from musicians where name=? and instrumentId=?",
    new Object[] {get("name"), get("instrumentId")},
    Musician.class
   ).read();

  return Utilities.isEmptyList(res);

 }



 /**
  * Checks whether a musician is in the DataBase.
  *
  * <p>Two musicians are distinct if the name, the main instrument differ and the id differ..</p>
  *
  * @return True: He's not in the DataBase. False otherwise.
  */

 public boolean isUniqueOther() {

  Object keyValue = get(getKeyName());

  if (Utilities.isEmpty(keyValue))
   return isUnique();

  List<Musician> res =
   DataStoreManager.get().makeDataStore(
    "core.bakedBeans.JDBCDataStore",
    "musicians",
    "select * from musicians where name=? and instrument_id=? and id !=?",
    new Object[] {get("name"), get("instrumentId"), keyValue},
    Musician.class
   ).read();

  return Utilities.isEmptyList(res);

 }



 /**
  * Returns the concatanation of "name" and "instrument".
  *
  * @return That significant string.
  */

 public String significant() {

  return get("name") + " (" + get("instrumentId") + ")";

 }



 /**
  * Returns the correction string for the logbook writer.
  *
  * <p>Musicians need the columns musician_id and o_musication_id to be corrected corrected.</p
  *
  * @return The correction strings.
  */

 @Override
 public String[] getLogBookCorrectionStatements() {

  return new String[] {
   "update logbook set musician_id=null, musiciansig=? where musician_id=?",
   "update logbook set omusician_id=null, omusiciansig=? where omusician_id=?"
  };

 }



 /**
  * Gets the age of this musician.
  *
  * @return The age. Or null if musician has no birthday.
  */

 public Integer getAge() {

  LocalDate b = (LocalDate)get("born");

  if (!Utilities.isEmpty(b)) {
   return Utilities.yearDiff(b, LocalDate.now());
  }

  return null;

 }

}
