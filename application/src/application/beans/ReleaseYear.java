package application.beans;

import core.bakedBeans.*;


/**
 * This Bean holds information about release years.
 *
 * @version 2023-04-20
 * @author lp
 *
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias = "releaseyear",
   name = "releaseyear",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "number",
   name = "number",
   base = "releaseyear",
   propertyFunction = PropertyFunction.count,
   pclass = Long.class
  ),
 },
 bakeBean = true
)

@PersistentBeanDescription (
 source = "Records",
 keyName = "NO_KEY"
)



public class ReleaseYear extends JDBCBean {

}