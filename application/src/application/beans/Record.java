package application.beans;



import core.bakedBeans.*;
import core.util.Property;
import core.util.Utilities;

import java.time.LocalDateTime;
import java.util.HashMap;

import static core.bakedBeans.Operator.AND;
import static core.bakedBeans.Operator.EQUAL;


/**
 * This bean holds data for a record
 *
 * @version 2023-12-01
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "interpreter",
   name = "interpreter",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "title",
   name = "title",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "label",
   name = "label",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "ordernumber",
   name = "ordernumber",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "recording",
   name = "recording",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "mediumId",
   name = "medium_id",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "number",
   name = "number",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "releaseyear",
   name = "releaseyear",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "comment",
   name = "comment",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   alias = "colTitles",
   base = "recordId",
   relationType = RelationType.afterSave,
   pclass = JDBCDataStore.class,
   iclass = Title.class,
   iclassPrefix = "baked",
   setter = "collectionSetter",
   getter = "collectionGetter",
   dataStoreName = "core.bakedBeans.JDBCDataStore",
   loadWith = {"id", "side", "track", "interpreter", "title", "duration"},
   orderBy = {"application.beans.Title:side", "application.beans.Title:track"},
   category = "titles",
   dontBakeAliasSetter = true
  ),
  @PropertyDescription(
   alias = "relCoverpictures",
   base = "recordId",
   relationType = RelationType.afterSave,
   pclass = JDBCDataStore.class,
   iclass = Coverpicture.class,
   iclassPrefix = "baked",
   setter = "collectionSetter",
   getter = "collectionGetter",
   dataStoreName = "core.bakedBeans.JDBCDataStore",
   loadWith = {"id", "nr", "description", "origin", "filename"},
   orderBy = {"application.beans.Coverpicture:nr"},
   category = "coverpictures",
   dontBakeAliasSetter = true
  ),
 },
 bakeBean = true
)
@PersistentBeanDescription(
 source = "records",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription(
 timestampMode = CryptedBeanDescription.TimestampMode.constant
)



public class Record extends CryptedBean implements ILogable{

 @Property("Record.NewLimit")
 private static LocalDateTime newLimit;

 private static final HashMap<String, String[]> orders = new HashMap<>();
 static {
  orders.put("+side", new String[] {"+application.beans.Title:side", "application.beans.Title:track"});
  orders.put("+track", new String[] {"+application.beans.Title:track", "application.beans.Title:side"});
  orders.put("+interpreter", new String[] {"+application.beans.Title:interpreter", "application.beans.Title:side", "application.beans.Title:track", "application.beans.Title:title"});
  orders.put("+title", new String[] {"+application.beans.Title:title", "application.beans.Title:interpreter", "application.beans.Title:side", "application.beans.Title:track"});
  orders.put("-side", new String[] {"-application.beans.Title:side", "application.beans.Title:track"});
  orders.put("-track", new String[] {"-application.beans.Title:track", "application.beans.Title:side"});
  orders.put("-interpreter", new String[] {"-application.beans.Title:interpreter", "application.beans.Title:side", "application.beans.Title:track", "application.beans.Title:title"});
  orders.put("-title", new String[] {"-application.beans.Title:title", "application.beans.Title:interpreter", "application.beans.Title:side", "application.beans.Title:track"});
 }

 public static String SETTINGS_SORTKEY = "recordtitles.orderBy";



 /**
  * Returns the combination of "interpreter" and "title".
  *
  * @return That significant string.
  */

 public String significant() {

  String sd = (String)get("interpreter");
  String sd1 = (String)get("title");
  return (Utilities.isEmpty(sd)? "": sd) + " / " + (Utilities.isEmpty(sd1)? "": sd1);

 }



 /**
  * There is a "new limit" in "record.properties". All records with a creation date after this date are considered "new".
  *
  * @return The date.
  *
  * @see Record#isNew
  */

 public LocalDateTime getNewLimit() {

  return newLimit;

 }



 /**
  * Checks whether or not a bean "is new".
  *
  * <p>That means the creation date is newer than the newLimit.</p>
  *
  * @return True if so. False otherwise.
  *
  * @see Record#getNewLimit
  */

 public Boolean isNew() {

  return newLimit.isBefore((LocalDateTime) (get("created")));

 }



 /**
  * Compares this Record to another PersistenBean.
  *
  * @param otherBean The other PersistentBean may be a Favourite or a Record.
  *
  * <p>In case it is Favourite the comparison is done with the Record (Property "subRecord") of the Favourite.
  * In case it is a Record normal comparison ist done. </p>
  *
  * @return true if the key values are equal. False otherwise.
  *
  * @see Favourites
  */

 public boolean equals(Object otherBean) {

  try {
   if (Favourites.class.isAssignableFrom(otherBean.getClass()))  // Maybe otherBean is baked.
    otherBean = ((Favourites)otherBean).get("subRecord");

   return super.equals(otherBean);
  }
  catch (Exception e) {/* Any Exception means beans aren't equal. */}

  return false;
 }



 /**
  * Returns the correction string for the logbook writer.
  *
  *  <p>Records need the column title_id to be corrected.</p
  *
  * @return The correction string.
  */

 @Override
 public String[] getLogBookCorrectionStatements() {

  return new String[] {
   "update logbook set record_id=null, recordsig=? where record_id=?",
  };

 }



 @Override
 protected boolean maySave() {

  return Login.get().may("editrecords");

 }



 @Override
 protected boolean maySaveNew() {

  return
   Login.get().may("addrecords");

 }



 @Override
 protected boolean mayDelete() {

  return
   Login.get().may("addrecords");

 }



 /**
  * Gets the order from the user settings.
  *
  * @param pi The PropertyInfo
  * @return sort criteria.
  */

 @Override
 public String[] getOrderBy(PropertyInfo pi) {

  if (Login.isFromExtern()  &&  "colTitles".equals(pi.getAlias())) {
   Setting setting = DataStoreManager.get().dataStore(JDBCDataStore.class, "settings")
    .resetable()
    .where(
     AND(
      EQUAL("userId", Login.get().get("userId")),
      EQUAL("key", SETTINGS_SORTKEY),
      EQUAL("itemId", this.getKeyValue())
     )
    )
    .make(Setting.class)
    .readBean();

   if (!core.bakedBeans.Utilities.isEmpty(setting)) {
    SettingsOrderBy order = (SettingsOrderBy) setting.get("relOrderBy");
    return orders.get(order.getOrderForOrderByClause() + order.get("property"));
   }

   return super.getOrderBy(pi);
  }

  return null;

 }

}
