package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;


/**
 * This class implements a "catalog request".
 *
 * @version 2022-07-27
 * @author pilgrim.lutz@imail.de
 *
 * @see Record
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "recordId",
   name = "record_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "subRecord",
   name = "sr",
   base = "recordId",
   loadWith = {"id", "created", "interpreter", "title", "releaseyear", "number", "mediumId", "label", "ordernumber"},
   relationType = RelationType.afterSave,
   pclass = Record.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterNL",
   dontBakeAliasSetter = true
  ),
  @PropertyDescription(
   alias = "catalogItemId",
   name = "catalogitem_id",
   pclass = Integer.class
  ),
 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "catalogrecords",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)

public class CatalogRecord extends JDBCBean {

}