package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;



/**
 * This class holds information about a user.
 * <p>Direct usage of this class is seldom. Most information about the logged in user
 * can be retrieved from the login bean.</p>
 *
 * @see application.workers.LoginWorker
 *
 * @version 2025-01-02
 * @author lp
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "loginname",
   name = "loginname",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "firstname",
   name = "firstname",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "lastname",
   name = "lastname",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "email",
   name = "email",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "roleId",
   name = "role_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription (
   alias = "refRole",
   base = "roleId",
   relationType = RelationType.beforeSave,
   pclass = Role.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   category = "users"
  ),
  @PropertyDescription(
   alias = "flags",
   name = "flags",
   pclass = Integer.class,
   dontBakeAliasGetter = true,
   dontBakeAliasSetter = true
  ),
  @PropertyDescription(
   alias = "locked",
   base = "flags",
   pclass = Boolean.class,
   getter = "bitMaskGetter",
   setter =  "bitMaskSetter",
   bitMask = User.BITMASK_LOCKED
  ),
 },
 bakeBean = true
)
@PersistentBeanDescription(
  source = "users",
  keyName = "id",
  keyClass = Integer.class,
  keyMakerClass = JDBCSequenceKeyMaker.class
 )
@CryptedBeanDescription(
 timestampMode = CryptedBeanDescription.TimestampMode.constant
)



public class User extends CryptedBean implements ILogable {

 // Flags.

 public static final int BITMASK_LOCKED = 1;



 @Override
 public String significant() {

   return
    (String)get("loginname");

 }



 @Override
 public String[] getLogBookCorrectionStatements() {

  return new String[] {
   "update logbook set user_id=null, usersig=? where user_id=?",
   "update logbook set ouser_id=null, ousersig=? where ouser_id=?"
  };

 }

}
