package application.beans;

import core.bakedBeans.PropertyDescription;
import core.bakedBeans.TypedBeanDescription;
import core.bakedBeans.CookieBean;




/**
 * A Bean for storing a windows geometry information in a Cookie.
 * 
 * @version 2016-01-23
 * @author pilgrim.lutz@imail.de
 *
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   name = "w",
   pclass = Integer.class
  ),
  @PropertyDescription(
   name = "h",
   pclass = Integer.class
  ),
  @PropertyDescription(
   name = "x",
   pclass = Integer.class
  ),
  @PropertyDescription(
   name = "y",
   pclass = Integer.class
  )
 }
)

public class CookieGeometryBean extends CookieBean {

}