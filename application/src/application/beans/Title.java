package application.beans;

import core.bakedBeans.*;
import core.util.Utilities;

import java.time.LocalDateTime;
import java.util.HashMap;

import static core.bakedBeans.Operator.AND;
import static core.bakedBeans.Operator.EQUAL;


/**
 * This bean holds data for titles.
 *
 * @version 2023-05-13
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "interpreter",
   name = "interpreter",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "title",
   name = "title",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "recordId",
   name = "record_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "duration",
   name = "duration",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "composer",
   name = "composer",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "recordingdate",
   name = "recordingdate",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "location",
   name = "location",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "producer",
   name = "producer",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "side",
   name = "side",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "track",
   name = "track",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   alias = "subRecord",
   name = "sr",
   base = "recordId",
   loadWith = {"id", "interpreter", "title"},
   relationType = RelationType.dontSave,
   pclass = Record.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterNL"
  ),
  @PropertyDescription(
   alias = "colJobs",
   base = "titleId",
   relationType = RelationType.afterSave,
   pclass = JDBCDataStore.class,
   iclass = Job.class,
   iclassPrefix = "baked",
   setter = "collectionSetter",
   getter = "collectionGetter",
   dataStoreName = "core.bakedBeans.JDBCDataStore",
   loadWith = {"id", "musicianId", "titleId", "instrumentId", "subInstrument", "subTitle", "subMusician"},
   orderBy = {"application.beans.Job:instrumentId", "application.beans.Musician:name"},
   category = "jobs"
  )
 },
 bakeBean = true
)
@PersistentBeanDescription(
 source = "titles",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription(
 timestampMode = CryptedBeanDescription.TimestampMode.constant
)



public class Title extends CryptedBean implements ILogable{

 private static final HashMap<String, String[]> orders = new HashMap<>();
 static {
  orders.put("+instrument", new String[] {"+application.beans.Job:instrumentId", "application.beans.Musician:name"});
  orders.put("+musician", new String[] {"+application.beans.Musician:name", "application.beans.Job:instrumentId"});
  orders.put("-instrument", new String[] {"-application.beans.Job:instrumentId", "application.beans.Musician:name"});
  orders.put("-musician", new String[] {"-application.beans.Musician:name", "application.beans.Job:instrumentId"});
 }

 public static String SETTINGS_SORTKEY = "titlejobs.orderBy";



 /**
  * Returns the concatanation of "interpreter" and "title".
  *
  * @return That significant string.
  */

 public String significant() {

  String sd = (String)get("interpreter");
  String sd1 = (String)get("title");
  return (Utilities.isEmpty(sd)? "": sd) + " / " + (Utilities.isEmpty(sd1)? "": sd1);

 }



 /**
  * Returns the correction string for the logbook writer.

  * <p>Titles need the column title_id to be corrected corrected.</p
  *
  * @return The correction string.
  */

 @Override
 public String[] getLogBookCorrectionStatements() {

  return new String[] {
   "update logbook set title_id=null, titlesig=? where title_id=?",
  };

 }



 @Override
 protected boolean maySave() {

  return Login.get().may("edittitles");

 }



 @Override
 protected boolean maySaveNew() {

  return
   Login.get().may("addtitles");

 }



 @Override
 protected boolean mayDelete() {

  return
   Login.get().may("addtitles");

 }



 /**
  * Gets the order from the user settings.
  *
  * @param pi The PropertyInfo
  * @return sort criteria.
  */

 @Override
 public String[] getOrderBy(PropertyInfo pi) {

  if (Login.isFromExtern()  &&  "colJobs".equals(pi.getAlias())) {
   Setting setting = DataStoreManager.get().dataStore(JDBCDataStore.class, "settings")
    .resetable()
    .where(
     AND(
      EQUAL("userId", Login.get().get("userId")),
      EQUAL("key", SETTINGS_SORTKEY),
      EQUAL("itemId", this.getKeyValue())
     )
    )
    .make(Setting.class)
    .readBean();

   if (!core.bakedBeans.Utilities.isEmpty(setting)) {
    SettingsOrderBy order = (SettingsOrderBy) setting.get("relOrderBy");
    return orders.get(order.getOrderForOrderByClause() + order.get("property"));
   }

   return super.getOrderBy(pi);
  }

  return null;

 }

}
