package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;



/**
 * Class for favourites lists
 *
 * @version 2023-12-03
 * @author lp
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias= "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription (
   alias = "enumPurpose",
   base = "purpose",
   pclass = FavouritesList.purposes.class,
   setter = "enumSetter",
   getter = "enumGetter"
  ),
  @PropertyDescription(
   alias = "name",
   name = "name",
   pclass = String.class,
   maxLength = 64
  ),
  @PropertyDescription(
   alias = "purpose",
   name = "purpose",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   alias = "userId",
   name = "user_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "subUser",
   name = "subuser",
   base = "userId",
   relationType = RelationType.dontSave,
   loadWith = {"id", "loginname", "firstname", "lastname"},
   pclass = User.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterNL",
   dontBakeAliasSetter = true
  ),
  @PropertyDescription(
   alias = "colFavourites",
   base = "favouriteslistId",
   relationType = RelationType.afterSave,
   pclass = DataStore.class,
   iclass = Favourites.class,
   iclassPrefix = "baked",
   setter = "collectionSetter",
   getter = "collectionGetter",
   dataStoreName = "core.bakedBeans.JDBCDataStore",
   orderBy = {"orderNr"},
   category = "favourites",
   dontBakeAliasSetter = true
  ),
  @PropertyDescription(
   alias = "access",
   name = "access",
   pclass = Integer.class,
   dontBakeAliasSetter = true,
   dontBakeAliasGetter = true
  ),
  @PropertyDescription(
   alias = "userAccessRead",
   base = "access",
   pclass = Boolean.class,
   getter = "bitMaskGetter",
   setter =  "bitMaskSetter",
   bitMask = FavouritesList.BITMASK_ACCESS_USER_READ
  ),
  @PropertyDescription(
   alias = "userAccessWrite",
   base = "access",
   pclass = Boolean.class,
   getter = "bitMaskGetter",
   setter =  "bitMaskSetter",
   bitMask = FavouritesList.BITMASK_ACCESS_USER_WRITE
  ),
  @PropertyDescription(
   alias = "orderNr",
   name = "ordernr",
   pclass = Integer.class
  )
 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "favouriteslists",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription (
 paranoid = true,
 timestampMode = CryptedBeanDescription.TimestampMode.constant
)



public class FavouritesList extends CryptedBean {

 // DON'T CHANGE ORDER! DON'T INSERT ITEMS!
 public enum purposes {

  /**
   * A users favourites list.
   */
  user,
  /**
   * A favourites list for newly created records. Created by MC.
   */
  systemNewRecords,

 }



 /**
  * Access bit masks.
  */

 public static final int BITMASK_ACCESS_USER_READ = 1;
 public static final int BITMASK_ACCESS_USER_WRITE = 2;



 /**
  * Need a deep hash code here.
  *
  * @return The hash code.
  */

 @Override
 public int hashCode() {

  return
   deepHashCode();

 }

}
