package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;

import static core.bakedBeans.Operator.AND;
import static core.bakedBeans.Operator.EQUAL;



/**
 * Class for settings
 *
 * @version 2024-10-29
 * @author lp
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias= "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "key",
   name = "key",
   pclass = String.class,
   maxLength = 64
  ),
  @PropertyDescription(
   alias = "itemId",
   name = "item_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "value",
   name = "value",
   pclass = String.class,
   maxLength = 256
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   alias = "userId",
   name = "user_id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "subUser",
   name = "subuser",
   base = "userId",
   relationType = RelationType.dontSave,
   loadWith = {"id", "loginname", "firstname", "lastname"},
   pclass = User.class,
   setter = "relN2OSetter",
   getter = "relN2OGetterNL",
   dontBakeAliasSetter = true
  ),
  @PropertyDescription(
   alias = "relOrderBy",
   base = "value",
   relationType = RelationType.beforeSave,
   pclass = SettingsOrderBy.class,
   pclassPrefix = "baked",
   setter = "stringBeanSetter",
   getter = "stringBeanGetter"
  ),
 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "settings",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription (
 paranoid = true,
 timestampMode = CryptedBeanDescription.TimestampMode.constant
)



public class Setting extends CryptedBean {


 /**
  * Get a setting.
  *
  * @param key Name of the setting.
  *
  * @return The value of the setting.
  */

 public static String getSetting(String key) {

  return
   getSetting(key, 0);  // Means a common setting.

 }



 /**
  * Get a setting.
  *
  * @param key Name of the setting.
  * @param userId Retrieve setting for this user.
  *
  * @return The value of the setting.
  */

 public static String getSetting(String key, int userId) {

  logger.debug(() -> "Setting.getSetting(): \"" + key + "\"");

  Setting setting =
   DataStoreManager.get().dataStore(JDBCDataStore.class, "settings")
    .resetable()
    .where(
     AND(
      EQUAL("userId", userId),
      EQUAL("key", key)
     )
    )
    .make(Setting.class)
    .readBean();

  return
   Utilities.isEmpty(setting) ? null : (String)setting.get("value");

 }



 /**
  * Set a setting.
  *
  * @param key The name.
  * @param value the value.
  */

 public static void setSetting(String key, String value) {

  logger.debug(() -> "Setting.setSetting(): \"" + key + "\" \"" + value + "\"");

  DataStoreManager.get().dataStore(JDBCDataStore.class, "settings")
   .resetable()
   .where(
    AND(
     EQUAL("userId", 0),  // Means a common setting.
     EQUAL("key", key)
    )
   )
   .make(Setting.class)
   .forEach(PersistentBean::delete);

  Setting setting = BeanFactory.make(Setting.class);
  setting.set("userId", 0);
  setting.set("key", key);
  setting.set("value", value);
  setting.set("created", LocalDateTime.now());

  setting.saveNew();
  setting.load(setting.get("id"));  // Read bean to get latest values.

  DataStoreManager.get()
   .signal(DataStore.DataStoreSignal.write, setting,"settings");

 }

}
