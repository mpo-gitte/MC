package application.beans;

import application.aspects.annotations.SkillIndexSearch;
import application.aspects.annotations.SkillNewFavouritesList;
import core.bakedBeans.*;


/**
 * This class holds a set of rights.
 *
 * @version 2024-12-24
 * @author lp
 *
 * @see Role
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   name = "autologin",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "readrecords",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "addrecords",
   pclass = Boolean.class,
   getter = "indexReady"
  ),
  @PropertyDescription(
   name = "editrecords",
   pclass = Boolean.class,
   getter = "indexReady"
  ),
  @PropertyDescription(
   name = "deleterecords",
   pclass = Boolean.class,
   getter = "indexReady"
  ),
  @PropertyDescription(
   name = "readtitles",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "addtitles",
   pclass = Boolean.class,
   getter = "indexReady"
  ),
  @PropertyDescription(
   name = "edittitles",
   pclass = Boolean.class,
   getter = "indexReady"
  ),
  @PropertyDescription(
   name = "deletetitles",
   pclass = Boolean.class,
   getter = "indexReady"
  ),
  @PropertyDescription(
   name = "search",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "searchempty",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "readmusicians",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "editmusicians",
   pclass = Boolean.class,
   getter = "indexReady"
  ),
  @PropertyDescription(
   name = "savesearchpatterns",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "complexsearch",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "namesearchpatterns",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "readinstruments",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "addinstruments",
   pclass = Boolean.class,
   getter = "indexReady"
  ),
  @PropertyDescription(
   name = "getdiscography",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seeids",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seelogbook",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "deletelogbookentries",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seestatisticscount",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seestatisticstopmnusicians",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seestatisticsunemployedmusicians",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seestatisticstopissuers",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seestatisticstopmusicians",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seestatisticsaddition",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seefavouriteslists",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seefavouriteslistsothers",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seebrowserinfo",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seedbinfo",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seenewmark",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seelogbookothers",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "generateId3Tags",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "searchForNoChildren",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   name = "seeTagCloud",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "editInstruments",
   name = "edIstr",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "dumpData",
   name = "dDa",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "setForeignPassword",
   name = "sfP",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "seeBlacksheep",
   name = "sBs",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "seeCurrentLogins",
   name = "sLs",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "deleteCurrentLogins",
   name = "dLs",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "seeBirthdaysCurrent",
   name = "sBC",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "seeCoverpictures",
   name = "sCP",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "seeCoverpicturesUnlimited",
   name = "sCPu",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "editCoverpictures",
   name = "eCP",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "storeSearchPatternInCookie",
   name = "ssc",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "seeStatisticsTitles",
   name = "sST",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "seeStatisticsMusicians",
   name = "sSM",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "seeCatalog",
   name = "sca",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "rebuildCatalog",
   name = "rca",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "mappingCatalog",
   name = "mca",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "sortFavouritesLists",
   name = "sortfl",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "sort",
   name = "sort",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "sortReleaseYears",
   name = "sortry",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "catalogPrintView",
   name = "cprv",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "indexSearch",
   name = "idxs",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "changePassword",
   name = "cpw",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "userManagement",
   name = "umnt",
   pclass = Boolean.class
  ),


  // Skills: Volatile, not persisted "rights".

  @PropertyDescription(
   alias = "skillNewFavouritesList",
   getter = "skillNewFavouritesListGetter",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "skillIndexSearch",
   getter = "skillIndexSearchGetter",
   pclass = Boolean.class
  ),
 }
)

public class Rights extends StringBean {


  /**
   * A special getter for the skill to see New-FavouritesList.
   *
   * @param pi The PropertyInfo
   *
   * @return True if skill is given. False otherwise.
   */

  @SkillNewFavouritesList
  @TypedBeanGetter
  protected Object skillNewFavouritesListGetter(PropertyInfo pi) {

   return false;

  }



  /**
   * A special getter for the skill to do an index search.
   *
   * @param pi The PropertyInfo
   *
   * @return True if skill is given. False otherwise.
   */

  @SkillIndexSearch
  @TypedBeanGetter
  protected Object skillIndexSearchGetter(PropertyInfo pi) {

   return false;

  }



  /**
   * This getter will be used if a right depends on search index rebuild is done.
   *
   * @param pi Property info
   *
   * @return True if role has the right. False otherwise.
   */

  @TypedBeanGetter
  protected Object indexReady(PropertyInfo pi) {

   if (!"TRUE".equals(Setting.getSetting("INDEXREADY")))
    return false;

   return
    stdGetter(pi);

  }

}
