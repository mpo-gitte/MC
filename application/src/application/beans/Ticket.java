package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;



/**
 * This bean holds data for tickets.
 *
 * @version 2023-09-05
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "userId",
   name = "user_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "refUser",
   base = "userId",
   relationType = RelationType.beforeSave,
   pclass = User.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   category = "users"
  ),
  @PropertyDescription(
   alias = "lastaccess",
   name = "lastaccess",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "type",
   name = "type",
   pclass = Integer.class,
   dontBakeAliasGetter = true,
   dontBakeAliasSetter = true
  ),
  @PropertyDescription (
   alias = "enumType",
   base = "type",
   pclass = Ticket.Type.class,
   setter = "enumSetter",
   getter = "enumGetter"
  ),
  @PropertyDescription(
   alias = "cancelled",
   name = "cancelled",
   pclass = Boolean.class
  ),
  @PropertyDescription(
   alias = "until",
   name = "until",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "factor",
   name = "factor",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  )
 },
 bakeBean = true
)
@PersistentBeanDescription(
  source = "tickets",
  keyName = "id",
  keyClass = java.lang.Integer.class,
  keyMakerClass = JDBCSequenceKeyMaker.class
 )



public class Ticket extends CryptedBean {



 /**
  * Type of a ticket.
   */

 public enum Type {

  /**
   * A standard ticket.
   */
  standard,
  /**
   * A ticket with a time span for validity.
   */
  season,
  /*
   * A ticket with maximum number of usage till invalidity. Aka "Streifenkarte".
   */
  multi
 }

 
 /**
  * Checks wether or not a ticket is cancelled.
  *
  * @return true if ticket is canceld. False otherwise.
  */

 public Boolean isCancelled() {
  
  return core.util.Utilities.isTrue(get("cancelled"));

 }

}
