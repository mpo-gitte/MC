package application.beans;



import core.bakedBeans.*;
import core.exceptions.MCException;
import core.util.DBConnection;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;



/**
 * Class for Entries in the logbook.

 * @version 2025-01-07
 * @author lp
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription (
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription (
   alias = "keyId",
   pclass = Integer.class,
   setter = "keySetter",
   getter = "keyGetter"
  ),
  @PropertyDescription (
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription (
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription (
   alias = "userId",
   name = "user_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "clientId",
   name = "client_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "verb",
   name = "verb",
   pclass = Integer.class,
   dontBakeAliasSetter = true,
   dontBakeAliasGetter = true
  ),
  @PropertyDescription (
   alias = "recordId",
   name = "record_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "titleId",
   name = "title_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "musicianId",
   name = "musician_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "ouserId",
   name = "ouser_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "omusicianId",
   name = "omusician_id",
   pclass = Integer.class
  ),
  @PropertyDescription (
   alias = "clientip",
   name = "clientip",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "sessionid",
   name = "sessionid",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "referer",
   name = "referer",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "info",
   name = "info",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "data",
   name = "data",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "titlesig",
   name = "titlesig",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "musiciansig",
   name = "musiciansig",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "omusiciansig",
   name = "omusiciansig",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "ousersig",
   name = "ousersig",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "usersig",
   name = "usersig",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "recordsig",
   name = "recordsig",
   pclass = String.class
  ),
  @PropertyDescription (
   alias = "refClient",
   base = "clientId",
   relationType = RelationType.beforeSave,
   pclass = Client.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   category = "clients"
  ),
  @PropertyDescription (
   alias = "refUser",
   base = "userId",
   relationType = RelationType.beforeSave,
   pclass = User.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   loadWith = {"id", "loginname", "firstname", "lastname"},
   category = "users"
  ),
  @PropertyDescription (
   alias = "enumVerb",
   base = "verb",
   pclass = LogbookEntry.verbs.class,
   setter = "enumSetter",
   getter = "enumGetter"
  ),
  @PropertyDescription (
   alias = "refOUser",
   base = "ouserId",
   relationType = RelationType.beforeSave,
   pclass = User.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   loadWith = {"id", "loginname", "firstname", "lastname"},
   category = "users"
  ),
  @PropertyDescription (
   alias = "refMusician",
   base = "musicianId",
   relationType = RelationType.beforeSave,
   pclass = Musician.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   loadWith = {"id", "name", "instrumentId"},
   category = "musicians"
  ),
  @PropertyDescription (
   alias = "refOMusician",
   base = "omusicianId",
   relationType = RelationType.beforeSave,
   pclass = Musician.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   loadWith = {"id", "name", "instrumentId"},
   category = "musicians"
  ),
  @PropertyDescription (
   alias = "refRecord",
   base = "recordId",
   relationType = RelationType.beforeSave,
   pclass = Record.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   loadWith = {"id", "interpreter", "title"},
   category = "records"
  ),
  @PropertyDescription (
   alias = "refTitle",
   base = "titleId",
   relationType = RelationType.beforeSave,
   pclass = Title.class,
   pclassPrefix = "baked",
   setter = "relN2OSetter",
   getter = "relN2OGetterDS",
   dataStoreName = "application.beans.ResetDataStore",
   loadWith = {"id", "interpreter", "title"},
   category = "titles"
  )
 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "logbook",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription (
 paranoid = true
)

public class LogbookEntry extends CryptedBean {

 // DON'T CHANGE ORDER! DON'T INSERT ITEMS!
 public enum verbs {

  /**
   * Login of a user.
   */
  login,
  /**
   * Auto login (via ticket) of a user.
   */
  autologin,
  /**
   * Logoff of a user.
   */
  logoff,
  /**
   * Record read.
    */
  read,
  /**
   * Record written.
   */
  write,
  /**
   * Record deleted.
   */
  delete,
  /**
   * AutoLogin activated for user.
   */
  activateAutoLogin,
  /**
   * AutoLoigin deactivated for a user.
   */
  cancelAutoLogin,
  /**
   * Exchanged a musician.
   */
  exchangeMusician,
  /**
   * Created a musician,#.
   */
  createMusician,
  /**
   * Attached a musician to a title.
   */
  attachMusician,
  /**
   * Deattached a Musician from a title.
   */
  detachMusician,
  /**
   * Someone requested ID3 tags for a Record.
   */
  getId3Tags,
  /**
   * Someone sets a password for a user.
   */
  setPassword,
  /**
   * Someone sets a loginname for a user. (Not used, currently.)
   */
  setLoginname,
  /**
   * Backdoor opened.
   */
  backdoorOpened,
  /**
   * Item deleted by system.
   */
  deleteSystem,
  /**
   * User created.
   */
  userCreated,
  /**
   * The user was locked
   */
  userLocked,
  /**
   * The user was unlocked
   */
  userUnlocked,
 }

 private static Logger logger;


 
 public static verbs[] allVerbs() {

  return verbs.values();

 }



 /**
  * Deletes this an all older LogbookEntries.
  */

 public void deleteThisAndOlder() {

  StringBuffer sb = new StringBuffer();
  
  sb.append("DELETE FROM ");
  sb.append(getSource());
  sb.append(" WHERE CREATED <= ?");
  
  String sd = sb.toString();

  DBConnection con = getConnection();

  PreparedStatement stmt = con.prepareStatement(sd);

  try {
   stmt.setObject(1, get("created"));

   logger.debug(() -> "deleteThisAndOlder(): executing: " + sd);

   stmt.execute();
  }
  catch (SQLException e) {
   throw new MCException(e);
  }

  releaseConnection();

 }



 /**
  * Saves the LogbookEntry and signals DataStores.
  */

 public void saveReset() {

  save();

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, this, "logbook");

 }

}
