package application.beans;

import core.bakedBeans.*;
import core.util.Utilities;

import java.time.LocalDateTime;
import java.util.List;



/**
 * This beans holds data about the users web client.
 *
 * @version 2023-08-20
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "clientstring",
   name = "clientstring",
   pclass = String.class
  )
 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "clients",
 keyName = "id"
)



public class Client extends JDBCBean {

 /**
  * This bean generates the key by itself.
  *
  * <p>The value for the key is the client string.</p>
  */

 @Override
 protected void makeKeyStart() {

  Integer newKeyValue = this.get("clientstring").hashCode();
  this.set(this.getKeyName(), newKeyValue);

 }



 /**
  * This bean generates the key by itself.
  *
  * <p>This implementation does nothing.</p>
  *
  * @see Client#makeKeyStart
  */

 @Override
 protected void makeKeyEnd() {}



 /**
  * This method saves the bean.
  *
  * <p>If there is a bean with with the same clientstring in the Database this bean will not be saved
  * but gets some fields update and its state set to clean.</p>
  */

 @Override
 public void saveNew() {

  List<? extends PersistentBean> cls = this.find(new String[] {"clientstring"});
  if (Utilities.isEmptyList(cls)) {
   this.set("created", LocalDateTime.now());
   super.saveNew();
  }
  else {
   this.set("id", cls.get(0).get("id"));
   this.status = PersistentBean.Status.clean;
  }
 }

}
