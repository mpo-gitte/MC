package application.beans;

import core.bakedBeans.*;

import java.time.LocalDate;

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "flags",
   name = "flags",
   pclass = Integer.class,
   dontBakeAliasGetter = true,
   dontBakeAliasSetter = true
  ),
  @PropertyDescription(
   alias = "thisWeek",
   base = "flags",
   pclass = Boolean.class,
   getter = "bitMaskGetter",
   setter =  "bitMaskSetter",
   bitMask = BirthdayCalendarEvent.BITMASK_THIS_WEEK
  ),
  @PropertyDescription(
   alias = "eventDate",
   name = "ed",
   pclass = LocalDate.class
  ),
  @PropertyDescription(
   alias = "subMusician",
   pclassPrefix = "baked",
   name = "sm",
   pclass = Musician.class
  )
 },
 bakeBean = true
)



public class BirthdayCalendarEvent extends TypedBean {



 // Flags.

 public static final int BITMASK_THIS_WEEK = 1;

}