package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;



/**
 * This Bean holds information about titles added in a year.
 *
 * @version 2023-07-22
 * @author lp
 *
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class,
   dontBakeAliasGetter = true,
   dontBakeAliasSetter = true
  ),
  @PropertyDescription(
   alias = "ayear",
   name = "ayear",
   base = "created",
   propertyFunction = PropertyFunction.year,
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "number",
   name = "number",
   base = "created",
   propertyFunction = PropertyFunction.count,
   pclass = Long.class
  ),
 },
 bakeBean = true
)

@PersistentBeanDescription (
 source = "titles",
 keyName = "NO_KEY"
)



public class AdditionYearTitles extends JDBCBean {

}
