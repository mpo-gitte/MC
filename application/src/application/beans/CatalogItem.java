package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;


/**
 * This class implements a "catalog item".
 *
 * @version 2022-10-22
 * @author pilgrim.lutz@imail.de
 *
 */

@TypedBeanDescription (
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class,
   serialize = SerializationMode.never
  ),
  @PropertyDescription(
   alias = "cryptKeyId",
   pclass = String.class,
   setter = "cryptKeySetter",
   getter = "cryptKeyGetter",
   serialize = SerializationMode.always
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "text",
   name = "text",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "relCatalogRecords",
   relationType = RelationType.afterSave,
   pclass = BeanList.class,
   iclass = CatalogRecord.class,
   iclassPrefix = "baked",
   setter = "simpleBeanListSetter",
   getter = "simpleBeanListGetter",
   dataStoreName = "application.beans.ResetDataStore",
   orderBy = {"application.beans.Record:interpreter", "application.beans.Record:releaseyear"},
   loadWith = {"id", "recordId", "subRecord"},
   base = "catalogItemId",
   category = "catalog",
   dontBakeAliasSetter = true
  ),
 },
 bakeBean = true
)
@PersistentBeanDescription (
 source = "catalogitems",
 keyName = "id",
 keyMakerClass = JDBCSequenceKeyMaker.class
)
@CryptedBeanDescription(
 timestampMode = CryptedBeanDescription.TimestampMode.constant
)

public class CatalogItem extends CryptedBean {

}
