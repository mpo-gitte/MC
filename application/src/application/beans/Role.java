package application.beans;

import core.bakedBeans.*;

import java.time.LocalDateTime;

/**
 * This class defines a role for a user.
 *
 * @version 2024-12-30
 * @author lp
 * 
 * @see User
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "name",
   name = "name",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "rights",
   name = "rights",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "relRights",
   base = "rights",
   relationType = RelationType.beforeSave,
   pclass = Rights.class,
   setter = "stringBeanSetter",
   getter = "stringBeanGetter"
  ),
  @PropertyDescription(
   alias = "colRolesMedia",
   pclass = BeanList.class,
   iclass = RolesMedia.class,
   iclassPrefix = "baked",
   setter = "",
   getter = "simpleDataStoreGetter",
   dataStoreName = "core.bakedBeans.JDBCDataStore",
   base = "roleId",
   category = "setup",
   dontBakeAliasSetter = true
  ),
  @PropertyDescription(
   alias = "flags",
   name = "flags",
   pclass = Integer.class,
   dontBakeAliasGetter = true,
   dontBakeAliasSetter = true
  ),
  @PropertyDescription(
   alias = "default",
   base = "flags",
   pclass = Boolean.class,
   getter = "bitMaskGetter",
   setter =  "bitMaskSetter",
   bitMask = Role.BITMASK_DEFAULT
  ),
 },
 bakeBean = true
)
@PersistentBeanDescription(
 source = "roles",
 keyName = "id",
 keyClass = Integer.class
)



public class Role extends JDBCBean {

  // Flags.

  public static final int BITMASK_DEFAULT = 1;

}