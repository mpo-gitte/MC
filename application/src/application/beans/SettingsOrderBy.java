package application.beans;

import core.bakedBeans.PropertyDescription;
import core.bakedBeans.StringBean;
import core.bakedBeans.TypedBeanDescription;


/**
 * This class stores a sort order.
 *
 * @version 2022-12-18
 * @author lp
 *
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "property",
   name = "prop",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "direction",
   name = "dir",
   pclass = Integer.class
  ),
  @PropertyDescription(
   alias = "enumDirection",
   base = "direction",
   pclass = SettingsOrderBy.direction.class,
   setter = "enumSetter",
   getter = "enumGetter"
  ),
 },
 bakeBean = true
)

public class SettingsOrderBy extends StringBean {

 // DON'T CHANGE ORDER! DON'T INSERT ITEMS!
 public enum direction {
  none,
  descending,
  ascending,
 }



 public String getOrderForOrderByClause() {

  SettingsOrderBy.direction order = (SettingsOrderBy.direction)get("enumDirection");

  switch(order) {
   case ascending:
    return "+";
   case descending:
    return "-";

  }

  return "";  // none

 }

}
