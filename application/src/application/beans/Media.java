package application.beans;

import core.bakedBeans.JDBCBean;
import core.bakedBeans.PersistentBeanDescription;
import core.bakedBeans.PropertyDescription;
import core.bakedBeans.TypedBeanDescription;

import java.time.LocalDateTime;



/**
 * An application bean which holds media data.
 *
 * @version 2021-12-07
 * @author pilgrim.lutz@imail.de
 */

@TypedBeanDescription(
 properties = {
  @PropertyDescription(
   alias = "id",
   name = "id",
   pclass = String.class
  ),
  @PropertyDescription(
   alias = "created",
   name = "created",
   pclass = LocalDateTime.class
  ),
  @PropertyDescription(
   alias = "description",
   name = "description",
   pclass = String.class
  )
 },
 bakeBean = true
)
@PersistentBeanDescription(
  source = "media",
  keyName = "id",
  keyClass = Integer.class
 )



public class Media extends JDBCBean {

}