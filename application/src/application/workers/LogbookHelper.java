package application.workers;

import application.beans.Record;
import application.beans.*;
import core.bakedBeans.BeanFactory;
import core.bakedBeans.TypedBean;
import core.exceptions.MCException;
import core.util.*;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;



/**
 * Helper methods for Logbook access.
 *
 * @version 2025-01-06
 * @author lp
 */

//@Configurator.Context(LogbookWorkerImpl.class)
public class LogbookHelper implements ObjectManager.IObject {

 private static Logger logger;  // Log in the name of the worker.

 @WorkerHelper()
 private LoginHelper loginHelper;



 /**
  * Write a logbook entry.
  *
  * @param rec For this record.
  * @param tit For this title.
  * @param mus For this musician.
  * @param omus For this other musician.
  * @param verb What to log.
  */

 void writeLogbook(Record rec, Title tit, Musician mus, Musician omus, LogbookEntry.verbs verb) {

  writeLogbook(rec, tit, mus, omus, verb, loginHelper.getLogin().getUserId());

 }



 /**
  * Write a logbook entry.
  *
  * @param rec For this record.
  * @param tit For this title.
  * @param mus For this musician.
  * @param omus For this other musician.
  * @param verb What to log.
  * @param uid The user id.
  */

 void writeLogbook(Record rec, Title tit, Musician mus, Musician omus, LogbookEntry.verbs verb, Integer uid) {

  LogbookEntry lbe = LogbookWorkerImpl.createLogbookEntry(verb, uid);
  String data = null;

  if (rec != null  &&  !rec.isEmpty()) {
   lbe.set("recordId", rec.getKeyValue());
   if (verb == LogbookEntry.verbs.write) {
    TypedBean ov = rec.oldValues();
    if (ov != null  &&  !ov.isEmpty())
     data = BeanFactory.dumpBeanToString(ov);
   }
  }

  if (tit != null  &&  !tit.isEmpty()) {
   lbe.set("titleId", tit.getKeyValue());
   if (verb == LogbookEntry.verbs.write) {
    TypedBean ov = tit.oldValues();
    if (ov != null  &&  !ov.isEmpty())
     data = BeanFactory.dumpBeanToString(ov);
   }
  }

  if (mus != null  &&  !mus.isEmpty()) {
   lbe.set("musicianId", mus.getKeyValue());

   if (verb == LogbookEntry.verbs.write)
    data = BeanFactory.dumpBeanToString(mus.oldValues());
   else if (verb == LogbookEntry.verbs.delete)
    data = BeanFactory.dumpBeanToString(mus);

  }

  if (omus != null  &&  !omus.isEmpty())
   lbe.set("omusicianId", omus.getKeyValue());

  if (data != null)
   lbe.set("data", data);

  lbe.saveReset();

 }



 /**
  * Corrects the Logbook.
  *
  * <p>If a main bean is deleted the references from the logbook to the main bean will become invalid. This method deletes the references
  * and sets the "sginificant" string from the main bean in the logbook.</p>
  *
  * @param logable The main bean.
  */

 void correctLogbook(ILogable logable) {

  if (logable != null  &&  !logable.isEmpty()) {
//   DBConnection con = DB.get().getConnection();
   DBConnection con = DBConnectionStore.getConnection();
//   con.beginTransaction();

   try {
    for (String cs: logable.getLogBookCorrectionStatements()) {
     PreparedStatement stmt = con.prepareStatement(cs);
     stmt.setObject(1, logable.significant());
     stmt.setObject(2, logable.getKeyValue());
     stmt.execute();
//     stmt.close();
    }

//    con.commit();
//    DB.get().releaseConnection(con);
    DBConnectionStore.releaseConnection();
//    logger.info("correctLogbook(): musician " + logable.getKeyValue() + " " + logable.significant());
   }
   catch (Exception e) {
//    con.rollback();
    DBConnectionStore.rollback();
    throw new MCException(e);
   }

  }

 }

}
