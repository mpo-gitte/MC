package application.workers;



import baked.application.beans.Setting;
import core.bakedBeans.DataStoreManager;
import core.bakedBeans.JDBCDataStore;
import core.base.Utilities;
import core.util.Property;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.time.format.DateTimeFormatter;

import static core.bakedBeans.Operator.AND;
import static core.bakedBeans.Operator.EQUAL;



/**
 * Worker for getting information about MC.
 *
 * @version 2025-02-25
 * @author lp
 */

public class ProgramInformationWorkerImpl implements ProgramInformationWorker {

 private static Logger logger = null;

 @Property("ProgramInformation.version")
 private static String version = "X.XX";
 @Property("ProgramInformation.copyright")
 private static String copyright = "Copyright by Lutz Pilgrim, 2008 - eternity";
 @Property("ProgramInformation.stage")
 private static String stage = "local";
 @Property("ProgramInformation.tag")
 private static String tag = "First try.";
 @Property("ProgramInformation.buildDate")
 private static String buildDate = "";
 @Property("ProgramInformation.deploymentDate")
 private static String deploymentDate = "";

 private static String eTagPrefix;
 LocalDateTime startTime;

 String imprint = null;




 /**
  * Displays a start message for MC.
  */

 @Override
 public void onLoad() {

  logger.info("---------------------------------");
  logger.info(" MC V.{} is rising...", getVersion());
  logger.info(" Stage: {}", stage);
  logger.info("---------------------------------");

  startTime = LocalDateTime.now();

  eTagPrefix = startTime.toString().replaceAll("[-: ]", "") + "|";

 }



 /**
  * Returns MC's version number as a String.
  * @return The version number
  */

 @Override
 public String getVersion() {

  return version;

 }



 /**
  * Returns MC's copyright notice.
  * @return The copyright notice
  */

 @Override
 public String getCopyright() {

  return copyright;

 }



 /**
  * Returns the current stage.
  * @return The stage
  */

 @Override
 public String getStage() {

  return stage;

 }



 /**
  * Returns the current tag.
  * @return The tag
  */

 @Override
 public String getTag() {

  return tag;

 }



 /**
  * Returns the imprint setting.
  * @return The imprint
  */

 @Override
 public String getImprint() {

  // Looking for the imprint

  if (imprint == null) {
   Setting setting = DataStoreManager.get().dataStore(JDBCDataStore.class, "settings")
    .resetable()
    .where(
     AND(
      EQUAL("userId", 0),
      EQUAL("key", "IMPRINT")
     )
    )
    .make(Setting.class)
    .readBean();

   if (!Utilities.isEmpty(setting))
    imprint = setting.getValue();
   }

  return imprint;

 }



 /**
  * Returns a formatted string usable as an ETag.
  *
  * <p>For stage "master" the ETag is prefixed with the version number. For all other
  * stages it is prefixed with the start time.</p>
  *
  * <p>This makes all browser caches invalid for each restart.</p>
  *
  * @param tags All the data which should be included.
  *
  * @return The ETag.
  */

 public String prepareEtag(Object ... tags) {

  StringBuilder sb = new StringBuilder();
  sb.append(eTagPrefix);

  Arrays.stream(tags).forEach(sb::append);

  return sb.toString();

 }



 /**
  * Returns the build date set by gradle in a property.
  *
  * @return The build date
  */

 @Override
 public String getBuildDate() {

  return buildDate;

 }



 /**
  * Returns the build date set by gradle in a property.
  *
  * @return The build date
  */

 @Override
 public String getDeploymentDate() {

  return deploymentDate;

 }



 /**
  * Returns the build date set by gradle in a property.
  *
  * @return The build date
  */

 @Override
 public String getStartDate() {

  DateTimeFormatter formatter =
   DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  return startTime.format(formatter);

 }

}
