package application.workers;

import application.aspects.annotations.NeedsRight;
import baked.application.beans.Blacksheep;
import baked.application.beans.User;
import core.base.WorkerManager;

import java.util.List;



public interface Shepherd extends WorkerManager.IWorker {

 @NeedsRight("seeBlacksheep")
 public List<Blacksheep> getBlacksheep(User user);

 @NeedsRight("seeBlacksheep")
 public void deleteBlacksheep(List<Blacksheep> bss);

}
