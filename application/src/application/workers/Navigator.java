package application.workers;

import core.base.WorkerManager;

import java.util.ArrayList;



public interface Navigator extends WorkerManager.IWorker {

 NavigatorImpl.NavigatorPosition position(String friendlyName,
                                          Boolean dontMentionParams, Boolean overwrite,
                                          ArrayList<String> paramBlackList,
                                          Boolean skip);

 NavigatorImpl.NavigatorPosition currentPosition();

 ArrayList<NavigatorImpl.NavigatorPosition> history();

 void addParameter(String param, String val);

 void deleteParameter(String param);

 int getStackDepth();

}
