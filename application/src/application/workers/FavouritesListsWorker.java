package application.workers;

import application.aspects.annotations.NeedsRight;
import baked.application.beans.*;
import baked.application.beans.Record;
import core.bakedBeans.DataStore;
import core.base.WorkerManager;

import java.util.List;



public interface FavouritesListsWorker extends WorkerManager.IWorker {

 DataStore<FavouritesList> getFavouritesLists(User user);

 DataStore<FavouritesList> getFavouritesListsForMenu(User user);

 void delete(List<FavouritesList> fledel);

 void createNew(User user, String name);

 FavouritesList add(User user, FavouritesList fl, Record rec);

 void deleteFavourites(User user, List<Favourites> fledel);

 void saveFavourites(User user, FavouritesList fl, List<Favourites> fles);

 FavouritesList getNewRecordsFavouritesList(User user);

 @NeedsRight("sortFavouritesLists")
 void orderBy(User user, String property);

 @NeedsRight("sortFavouritesLists")
 public SettingsOrderBy getOrderBy(User user);

 @NeedsRight("sortFavouritesLists")
 public void orderDrag(FavouritesList fle, FavouritesList flet);

 @NeedsRight("sortFavouritesLists")
 public void orderByFavouritesList(FavouritesList fl, String property);

 @NeedsRight("sortFavouritesLists")
 public SettingsOrderBy getOrderByFavouritesList(FavouritesList fl);

 @NeedsRight("sortFavouritesLists")
 public void orderDragFavourites(FavouritesList fl, Record rec, Record rect);

}
