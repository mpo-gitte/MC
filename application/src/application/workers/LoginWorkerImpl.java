package application.workers;

import application.beans.Rights;
import application.exceptions.LoginException;
import baked.application.beans.*;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;
import core.bakedBeans.JDBCDataStore;
import core.bakedBeans.Utilities;
import core.base.Request;
import core.base.WorkerManager;
import core.util.Property;
import core.util.RequestConfig;
import core.util.SecretService;
import core.util.WorkerHelper;
import org.apache.logging.log4j.Logger;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static core.base.WorkerManager.Metric.MetricTypes.GAUGE;


/**
 * Worker for all the authentication stuff.
 *
 * @version 2025-02-20
 * @author lp
 *
 */

public class LoginWorkerImpl implements WorkerManager.IWorker {

 private static final String MC_TICKET = "MC_TICKET";
 private static final String MC_USERNAME = "MC_USERNAME";
 private static final String MC_PASSWORD = "MC_PASSWORD";
 private static final String MC_GUEST_PASSWORD = "GuestGuest1";

 @Property("LoginWorker.maxLoginFailures")
 private static int maxLoginFailures = 5;
 @Property("LoginWorker.Watcher.schedule")
 private static String watcherSchedule = "0 0/5 * * * ?";
 @Property("LoginWorker.LoginWatcher.loginAge")
 private static int loginAge = 600000;
 @Property("LoginWorker.TicketWatcher.ticketAge")
 private static int ticketAge = 1209600000;
 @Property("LoginWorker.Backdoor.userId")
 private static int backdoorUserId = -1;
 @Property("LoginWorker.Ticket.FactorForeign")
 private static int ticketFactorForeign = 10;
 @WorkerHelper()
 private LoginHelper loginHelper;
 @WorkerHelper()
 private ShepherdsDog wuff;

 private static Logger logger;
 private static DataStoreManager dataStoreManager;



 /**
  * Does a login for the given user. Throws a LoginException if user could not log in.
  *
  * @param userPasswordAllowed Login with username and password is allowed.
  * @param guestAllowed Login with "guest" / "guest" is allowed if username
  *                     and password aren't in the request.
  */

 public void login(@RequestConfig() Request request, Boolean userPasswordAllowed, Boolean guestAllowed, Boolean useTicket) {

  Login login = loginHelper.getLogin();

  if (login.getUserId() != null)  // Someone is already logged in.
   writeLogbook(login, LogbookEntry.verbs.logoff, login.getUserId(), null);

  boolean isAutoLogin = true;
  login.setTicketId(null);

  User user = null;

  if (Utilities.isTrue(useTicket)) {

   // Try ticket in cookie.

   String sticket = request.getCookie(MC_TICKET);

   // Try ticket in authorization header.

   if (Utilities.isEmpty(sticket))
    sticket = request.getAuthorization(MC_TICKET);

   // Try ticket in parameter.

   if (Utilities.isEmpty(sticket))
    sticket = request.getParameter(MC_TICKET);

   // Try to get a user for the ticket.

   if (!Utilities.isEmpty(sticket)) {
    Ticket tic = getTicket(sticket);
    user = getUserForTicket(login, request, tic);
    if (!Utilities.isEmpty(user))  // Valid user store ticket.
     login.setTicketId(tic.getId());
   }
  }

  // Still no user? Try username / password finally.

  if (userPasswordAllowed  &&  Utilities.isEmpty(user)) {

   String username = request.getParameter(MC_USERNAME);
   String password = request.getParameter(MC_PASSWORD);

   if ((username == null  || username.isEmpty())  &&  guestAllowed) {
    username = Login.MC_GUEST_USERNAME;
    password = MC_GUEST_PASSWORD;
   }

   if (logger.isDebugEnabled())
    logger.debug("login() for username: \"" + username + "\".");

   user = UsersHelper.getUserForLoginname(username);

   if (!user.isEmpty()) {
    // Valid user.

    if (request.isLocal()  &&  backdoorUserId == user.getId()) {
     logger.warn("login() opening backdoor for username: \"" + username + "\".");
     wuff.resetLoginException(user.getId());
     writeLogbook(
      login, LogbookEntry.verbs.backdoorOpened, null, "Backdoor opened for user \"" + username + "\"!"
     );
    }
    else {
     if (user.getLocked())
      throw new LoginException(user.getId(), "LoginLocked");  // Account locked.

     wuff.checkUser(user.getId(), maxLoginFailures);

     if (!checkPassword(password, user))
      throw new LoginException(user.getId(), "LoginIllUserOrPassword");  // Username ok. But password not.
    }
   }

   isAutoLogin = false;
  }


  // Still no user? --- Abort!

  if (Utilities.isEmpty(user)) {
   wuff.checkSession(request.getSessionId(), maxLoginFailures);
   throw new LoginException("LoginIllUserOrPassword");
  }

  // Create a new session to prevent against session fixation.

  String ossid = request.getSessionId();

  request.makeNewSession();

  // Remove old login.

  removeLogin(ossid);

  // Set up a new login for the new session and save it.

  login = setUpLogin(login, user, isAutoLogin, request);

  // Ok!

  wuff.resetLoginException(user.getId());  // Reset login failures.

  logger.info("User \"" + user.getLoginname() + "\" logged in.");

  writeLogbook(login, isAutoLogin ? LogbookEntry.verbs.autologin : LogbookEntry.verbs.login, null, null);

 }



 private void removeLogin(String sid) {

  Login login = new Login();
  login.load(sid);
  login.delete();

 }



 /**
  * Log off a user.
  */

 public void logoff() {

  Login login = loginHelper.getLogin();

  if (logger.isDebugEnabled())
   logger.debug("logoff() for username: \"" + login.getRefUser().getLoginname() + "\".");

  removeLogin(login.getSessionId());

  writeLogbook(login, LogbookEntry.verbs.logoff, login.getUserId(), null);

 }



 private Client getClient(Request req) {

  Client cl = new Client();
  cl.setClientstring(req.getBrowserName());
  cl.save();

  return cl;

 }



 private User getUserForTicket(Login login, Request req, Ticket ticket) {

  if (!Utilities.isEmpty(ticket)) {

   logger.debug(() -> "getUserForTicket() for ticket: \"" + ticket + "\".");

   validateTicket(login, req, ticket);

   if (!Utilities.isEmpty(ticket))
    return ticket.getRefUser();
  }

  return null;

 }



 private Ticket getTicket(String ticket) {

  Ticket tic = new Ticket();
  try {
   tic.setFromString("cryptKeyId", ticket);
  }
  catch (Exception e) {
   throw new LoginException("InvalidTicket");
  }
  return tic;
 }



 private boolean checkPassword(String password, User user) {

  // Get the password

  Password pw = loginHelper.getCurrentPassword(user);

  // Password expired?

  loginHelper.isPasswordExpired(pw);

  // Check password.

  SecretService.IAgent bond = SecretService.get().getAgent(pw.getSecretPurpose());

  if (bond.check(UsersHelper.saltPassword(password, pw.getSalt()), pw.getPassword())) {

   pw.setExpirationCount(pw.getExpirationCount() - 1);

   pw.save();

   return true;  // Password ok.
  }

  return false;
 }



 private Login setUpLogin(Login login, User user, boolean isAutoLogin, Request req) {

  Login newlogin = new Login();

  if (!login.isEmpty())
   newlogin.setFrom(login);  // Clone current login.

  newlogin.setUserId(user.getId());

  newlogin.setEffectiveRights(user.getRefRole().getRights());

  newlogin.setAutoLogin(isAutoLogin ? Boolean.TRUE : Boolean.FALSE);

  newlogin.setClientId(getClient(req).getId());

  LocalDateTime now = LocalDateTime.now();
  newlogin.setLastseen(now);
  newlogin.setCreated(now);

  String hostAddress = null;
  try {
   hostAddress = InetAddress.getLocalHost().getHostAddress();
  }
  catch (UnknownHostException e) {
   // Maybe we can't get the name of the local host. Does not really bother us.
   logger.error("setUpLogin(): Can't get local host address!");
  }

  newlogin.setHost(hostAddress);
  newlogin.setAddress(req.getIpAddress());

  newlogin.setSessionId(null);  // Force new session.

  newlogin.save();
  dataStoreManager.signal(DataStore.DataStoreSignal.write, newlogin, "logins");

  return newlogin;

 }



 private void validateTicket(Login login, Request req, Ticket ticket) {

  if (ticket.isEmpty()) {
   writeLogbook(login, LogbookEntry.verbs.cancelAutoLogin, login.getUserId(), "Invalid Ticket.");
   throw new LoginException("InvalidTicket");
  }

  if (ticket.isCancelled()) {
   writeLogbook(login, LogbookEntry.verbs.cancelAutoLogin, login.getUserId(), "Ticket is cancelled.");
   throw new LoginException("CancelledTicket");
  }

  Ticket.Type t = ticket.getEnumType();

  if (t == Ticket.Type.season) {
   LocalDateTime ts = ticket.getUntil();
   if (!ts.isBefore(LocalDateTime.now())) {
    writeLogbook(login, LogbookEntry.verbs.cancelAutoLogin, login.getUserId(), "Ticket has expired.");
    throw new LoginException("ExpiredTicket");
   }
  }
  else if (t == Ticket.Type.multi) {
   int factor = ticket.getFactor();
   if (factor <= 0) {
    writeLogbook(login, LogbookEntry.verbs.cancelAutoLogin, login.getUserId(), "Ticket has expired");
    throw new LoginException("ExpiredTicket");
   }
   else {
    -- factor;
    ticket.setFactor(factor);
   }
  }

  // Ticket is valid.

  ticket.setLastaccess(LocalDateTime.now());
  ticket.save();

 }



 private void writeLogbook(Login login, LogbookEntry.verbs verb, Integer suid, String info) {

  LogbookEntry lbe = new LogbookEntry();

  if (suid != null)
   lbe.setUserId(suid);
  lbe.setOuserId(login.getUserId());
  lbe.setEnumVerb(verb);
  lbe.setCreated(LocalDateTime.now());

  lbe.setSessionid(login.getSessionId());
  lbe.setInfo(info);

  Client client = login.getRefClient();
  if (!Utilities.isEmpty(client))
   lbe.setRefClient(client);

  lbe.saveReset();

 }



 /**
  * Activates auto login.
  *
  * @return String with the ticket. This is the encrypted primary key.
  */

 public String activateAutoLogin(@RequestConfig Request request) {

  Login login = loginHelper.getLogin();

  Ticket ticket = new Ticket();
  String lm;

  ticket.setUserId(login.getUserId());
  ticket.setCancelled(Boolean.FALSE);
  if (request.isLocal()) {
   ticket.setEnumType(Ticket.Type.standard);
   lm = Ticket.Type.standard.name();
  }
  else {
   ticket.setEnumType(Ticket.Type.multi);
   ticket.setFactor(ticketFactorForeign);
   lm = Ticket.Type.multi.name() + " (" + ticketFactorForeign + ")";
  }
  ticket.setCreated(LocalDateTime.now());
  ticket.save();

  writeLogbook(login, LogbookEntry.verbs.activateAutoLogin, login.getUserId(), lm);

  String sd = ticket.getCryptKeyId();

  login.setTicketId(ticket.getId());  // Store ticket in login.
  login.save();

  return sd;
 }



 /**
  * Returns a list of all users.
  *
  * @return The list of users.
  */

 public List<User> getAllUsers() {

  DataStore<User> ds = dataStoreManager.makeDataStore("login.allUsers", new Object[]{});

  return ds.read();

 }



 /**
  * Returns a list of all users.
  *
  * @return The list of users.
  */

 public List<Login> getAllLogins() {

  return
   loginHelper.getAllLogins();

 }



 /**
  * Return the current login.
  *
  * @return The current login.
  */

 public Login getLogin() {

  return loginHelper.getLogin();

 }



 /**
  * Returns whether a user is logged in.
  *
  * @return true if a user is logged in. False otherwise.
  */

 public Boolean isLoggedIn() {

  Login login = getLogin();

  if (Utilities.isEmpty(login))
   return Boolean.FALSE;

  return !Utilities.isEmpty(login.getUserId());

 }



 /**
  * Delete a list of logins.
  *
  * @param logins The list.
  */

 public void deleteLogins(List<Login> logins) {

  for (Login login : logins)
   if (login != null) {

    if (logger.isDebugEnabled())
     logger.debug("deleteLogins(): " + login.getSessionId());

    login.delete();

    dataStoreManager.signal(DataStore.DataStoreSignal.delete, login, "logins");
   }

 }



 /**
  * Metric for number of current users.
  *
  * @return the number of current users.
  */

 @WorkerManager.Metric(type = GAUGE, name = "number_current_users")
 public Number getMetricUserCounter() {

  return
   dataStoreManager.dataStore(JDBCDataStore.class, "logins")
    .resetable()
    .make(Login.class)
    .count();

 }



 /**
  * Get rights for the given session id.
  *
  * @param sessionId The session id
  *
  * @return A Rights bean or null if session id is not existing.
  */

 public Rights rightsForSessionId(String sessionId) {

  logger.debug(() -> "rightsForSession(): sessionId=" + sessionId);

  DataStore<Role> roles = dataStoreManager.makeDataStore(
   "loginWorker.roleForSessionId",
   new Object[] {
    Request.stripJvmRouteFromSessionId(sessionId)
   }
  );

  Role role = roles.readBean();

  if (logger.isDebugEnabled())
   logger.debug("rightsForSession(): rights=" + role.getRelRights());

  return role.isEmpty() ? null : role.getRelRights();

 }



 /**
  * TimerJob for removing "old" logins.
  */

 @WorkerManager.TimerJob(name = "LoginWorker.LoginWatcher", schedule = "LoginWorker.LoginWatcher.schedule")
 public void CheckingLogins() {

  logger.debug("LoginWatcher: Checking Logins.");

  try {
   DataStore<Login>logins = dataStoreManager.dataStore(JDBCDataStore.class, "logins")
    .resetable()
    .loadWith("sessionId", "lastseen")
    .make(Login.class);

   LocalDateTime now = LocalDateTime.now();
   boolean did = false;

   for (Login login : logins.read())
    if (Utilities.diffTimestamps(login.getLastseen(), now) > loginAge) {
     if (logger.isDebugEnabled())
      logger.debug("LoginWatcher: Removing Login: " + login.getSessionId());

     login.delete();
     did = true;
    }

   if (did)
    dataStoreManager.signal(DataStore.DataStoreSignal.write, "logins");
  }
  catch (Exception e) {
   logger.error("LoginWatcher: Can't get logins!");
  }

 }



 /**
  * TimerJob for removing "old" tickets.
  */

 @WorkerManager.TimerJob(name = "LoginWorker.TicketWatcher", schedule = "LoginWorker.TicketWatcher.schedule")
 public void CheckingTickets() {

  logger.debug("TicketWatcher: Checking Tickets.");

  LocalDateTime now = LocalDateTime.now();
  AtomicBoolean did = new AtomicBoolean(false);

  try {
   dataStoreManager.dataStore(JDBCDataStore.class, "tickets")
    .resetable()
    .make(Ticket.class)
    .forEach(t -> {
     if (t.getCancelled()  ||
         t.getLastaccess() == null && Utilities.diffTimestamps(t.getCreated(), now) > ticketAge  ||
         Utilities.diffTimestamps(t.getLastaccess(), now) > ticketAge) {
      if (logger.isDebugEnabled())
       logger.debug("TicketWatcher: Removing Ticket: " + t.getId());

      t.delete();
      did.set(true);
     }

    });

   if (did.get())
    dataStoreManager.signal(DataStore.DataStoreSignal.write, "tickets");
  }
  catch (Exception e) {
   logger.error("TicketWatcher: Can't get tickets!");
  }

 }

}
