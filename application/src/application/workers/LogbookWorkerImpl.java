package application.workers;



import baked.application.beans.LogbookEntry;
import baked.application.beans.User;
import core.bakedBeans.*;
import core.bakedBeans.DataStore.DataStoreSignal;
import core.bakedBeans.Utilities;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.List;

import static core.bakedBeans.Operator.*;



/**
 * This worker provides methods for accessing the logbook.
 *
 * @version 2024-04-17
 * @author lp
 *
 */

public class LogbookWorkerImpl implements LogbookWorker {

 private static Logger logger;
 private static DataStoreManager dataStoreManager;



 /**
  *
  * Returns a List of logbook entries (aka the logbook) for a given user.
  *
  * @param user The user.
  * @param verb The verb.
  *
  * @return A DataStore with logbook entries for the given user.
  * A logbook entry will be included in the list either the given user
  * is user or object user in the entry.
  *
  * @see LogbookEntry
  *
  */

 @Override
 public DataStore<? extends PersistentBean> getLogbook(final User user, final String verb) {

  Operator op = null;
  Operator opu = null;
  Operator opv = null;

  if (!Utilities.isEmpty(verb)) {
   LogbookEntry.verbs v
    = LogbookEntry.verbs.valueOf(verb);

   opv = EQUAL("enumVerb", v);
  }

  if (!Utilities.isEmpty(user)) {
   opu =
    OR(
     EQUAL("userId", user.getId()),
     EQUAL("ouserId", user.getId())
    );
  }

  if (opv != null  &&  opu != null)
   op = AND(opv, opu);
  else if (opv != null)
   op = opv;
  else if (opu != null)
   op = opu;

  logger.debug(() -> "getLogbook() for user: " + user.getId());

  return
   dataStoreManager.dataStore(JDBCDataStore.class, "logbook")
    .resetable()
    .where(op)
    .orderBy("-created")
    .make(LogbookEntry.class);

 }



 /**
  *
  * Deletes the given logbook entries.
  *
  * <p>Signals DataStores with category "logbook".</p>
  *
  * @param ledel A list with logbook entries to delete.
  * @see DataStore
  *
  */

 @Override
 public void delete(List<LogbookEntry> ledel) {

  for (LogbookEntry le : ledel)
   if (le != null)
    le.delete();

  dataStoreManager.signal(DataStoreSignal.delete, "logbook");

 }



 /**
  *
  * Deletes the given logbook entries and all older ones.Signals DataStores with category "logbook".
  *
  * @param ledel A list with logbook entries to delete.
  * @see DataStore
  */

 @Override
 public void deleteMore(LogbookEntry ledel) {

  if (ledel != null)
   ledel.deleteThisAndOlder();

  dataStoreManager.signal(DataStoreSignal.delete, "logbook");

 }



 /**
  * Creates a LogbookEntry with creation date and user.
  *
  * @param verb Verb for the LogbookEntry
  *
  * @return A new LogbookEntry
  */

 public static LogbookEntry createLogbookEntry (LogbookEntry.verbs verb, Integer uid) {

  LogbookEntry lbe = new LogbookEntry();

  lbe.setEnumVerb(verb);
  lbe.setCreated(LocalDateTime.now());

  lbe.setUserId(uid);

  return lbe;

 }

}
