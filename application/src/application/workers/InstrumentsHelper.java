package application.workers;

import baked.application.beans.Instrument;
import baked.application.beans.StatisticBean;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;



/**
 * Helpers for Instruments.
 *
 * @version 2023-09-05
 * @author pilgrim.lutz@imail.de
 */

class InstrumentsHelper {



 /**
  * Counts the usage of the given instrument in jobs.
  *
  * @param instr The instrument
  * @return The usage count in a StatisticBean
  */

 static StatisticBean countInstrumentsOnJobs(Instrument instr) {

  DataStore<StatisticBean> ds = DataStoreManager.get().makeDataStore(
   "instruments.countInstrumentsOnJobs",
   new Object[] {instr.getId()}
  );

  return ds.readBean();

 }






 /**
  * Counts the usage of the given instrument as the main instrument for a musician.
  *
  * @param instr The instrument
  * @return The usage count in a StatisticBean
  */

 static StatisticBean countInstrumentsOnMusicians(Instrument instr) {

  DataStore<StatisticBean> ds = DataStoreManager.get().makeDataStore(
   "instruments.countInstrumentsOnMusicians",
   new Object[] {instr.getId()}
  );

  return ds.readBean();

 }

}
