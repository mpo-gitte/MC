package application.workers;



import baked.application.beans.Record;
import baked.application.beans.*;
import application.beans.ResetDataStore;
import application.configs.ConfigCurrentUser;
import core.bakedBeans.*;
import core.base.WorkerManager;
import core.util.MergedPropertyResourceBundle;
import core.util.Utilities;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static core.bakedBeans.Operator.*;



/**
 * Class with methods for all statistics stuff.
 *
 * @version 2023-09-05
 * @author pilgrim.lutz@imail.de
 *
 */

public class StatisticsWorkerImpl implements WorkerManager.IWorker {

 private static Logger logger;
 private static DataStoreManager dataStoreManager;

 private final String ADDITIONSYEAR_KEY_RECORDS = "additions_records.orderBy";
 private final String ADDITIONSYEAR_KEY_TITLES = "additions_titles.orderBy";
 private final String ADDITIONSYEAR_KEY_MUSICIANS = "additions_musicans.orderBy";



 /**
  * Returns a list of StatisticBeans for each media filled with the number of items for this media.
  *
  * @return The list
  */

 public List<StatisticBean> getMediaCounts() {

  DataStore<StatisticBean> ds = dataStoreManager.makeDataStore(
   "application.beans.ResetDataStore", "statisticsCount",
   "select count(1) number, media.id med$id " +
   "from records, media " +
   "where media.id=records.medium_id " +
   "group by media.id " +
   "order by media.id",
   StatisticBean.class
  );

  List<StatisticBean> all = ds.read();

  synchronized(ds) {

   List<StatisticBean> withTitles = dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore", "statisticsCount",
    "select count(1) number, m.id med$id " +
    "from records r, media m " +
    "where r.id in (select record_id from titles) " +  // exist doesn't work in Adabas sometimes. And it seems to be much faster.
    "and m.id=r.medium_id " +
    "group by m.id",
    StatisticBean.class
   ).read();

   for (StatisticBean b : all) {
    b.setNumberWithTitles(0L);
    for (StatisticBean b1 : withTitles)
     if (b.getSubMedia().get("id").equals((b1.getSubMedia().get("id")))) {
      b.setNumberWithTitles(b1.getNumber());
     }
   }
  }

  return all;

 }



 /**
  * Returns a list of StatisticBeans for each media filled with the number
  * of items for this media.
  *
  * @return The list
  */

 public List<StatisticBean> getTitleCounts() {


  DataStore<StatisticBean> ds = dataStoreManager.makeDataStore(
   "application.beans.ResetDataStore", "statisticsCount",
   "select media.id med$id, count(1) number " +
   "from titles, records, media " +
   "where records.id = titles.record_id and records.medium_id=media.id " +
   "group by media.id " +
   "order by media.id",
   StatisticBean.class
  );

  List<StatisticBean> all =  ds.read();

  synchronized(ds) {

   List<StatisticBean> withTitles = dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore", "statisticsCount",
    "select m.id med$id, count(1) number " +
    "from titles pt, records r, media m " +
    "where r.id=pt.record_id " +
    "and m.id=r.medium_id " +
    "and pt.id in (select title_id from jobs) " +
    "group by m.id " +
    "order by m.id",
    StatisticBean.class
   ).read();

   for (StatisticBean b : all) {
    b.set("numberWithMusicians", 0L);
    for (StatisticBean b1 : withTitles)
     if ((b.getSubMedia().getId().equals((b1.getSubMedia()).getId())))
      b.set("numberWithMusicians", b1.get("number"));
   }
  }

  return all;

 }



 /**
  * Returns a list of StatisticBeans for each instrument filled with the number
  * of usages of that instrument.
  *
  * @return The list
  */

 public List<StatisticBean> getInstrumentsCounts() {

  return dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore", "statisticsCount",
    "select i.id instr$id, i.description instr$description, count(1) number " +
    "from jobs j, instruments i " +
    "where j.instrument_id=i.id " +
    "group by i.id, i.description " +
    "order by number desc ",
    StatisticBean.class
   ).read();

 }



 /**
  * Returns a list of StatisticBeans for each instrument filled with the number
  * of musicians who play that instrument.
  *
  * @return The list
  */

 public List<StatisticBean> getMusiciansCounts() {

  return dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore", "statisticsCount",
    "select i.id instr$id, i.description instr$description, count(1) number " +
    "from musicians m, instruments i " +
    "where m.instrument_id=i.id " +
    "group by i.id, i.description " +
    "order by number desc ",
    StatisticBean.class
   ).read();

 }



 /**
  * Searches for the given top issuer.
  *
  * @param searchfor The top issuer to search for.
  *
  * @return The list of his records.
  */

 public List<Record> searchTopIssuer(String searchfor) {

  return
   searchTopIssuerDS(searchfor).read();

 }



 /**
  * Searches for the given top issuer.
  *
  * @param searchfor The top issuer to search for.
  *
  * @return The DataStore of his records.
  */

 public DataStore<Record> searchTopIssuerDS(String searchfor) {

  logger.debug(() -> "searching for records of top issuer: \"" + searchfor + "\"");

  String[] params = {
   searchfor,
   searchfor + " &%",
   "%& " + searchfor + " &%",
   "%& " + searchfor
  };

  return
   dataStoreManager.makeDataStore("statistics.SearchTopIssuer", params);

 }



 /**
  * Returns a list of StatisticBeans filled with musicians which are not attached to any title.
  *
  * @return The list
  */

 public List<StatisticBean> getUnemployedMusicians() {

  return  dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore", "statisticsCount",
    "select m.id sm$id, m.name sm$name, m.instrument_id sm$instrument_id, 0::bigint number " +  // StatisticBean.number is long!
    "from musicians m " +
    "where m.id not in (select musician_id from jobs) " +
    "order by m.name ",
    StatisticBean.class
   ).read();

 }



 /**
  * Returns a list of StatisticBeans filled with musicians and the number of their jobs.
  *
  * @return The list
  */

 public List<StatisticBean> getTopMusicians() {

  return  dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore", "statisticsCount",
    "select m.id sm$id, m.name sm$name, m.instrument_id sm$instrument_id, count(1) number  " +
    "from jobs, musicians m " +
    "where m.id=jobs.musician_id " +
    "group by m.id, m.name, m.instrument_id " +
    "order by number desc, m.name ",
    StatisticBean.class
   ).read();

 }



 /**
  * Returns a list of AdditionYearRecords beans filled with years
  * and the number of records added in that year.
  *
  * @return The list
  */

 public DataStore<AdditionYearRecords> getAdditionRecords(@ConfigCurrentUser User user) {

  Setting setting = SortHelper.getSortSetting(
   user,
  "ayear",
   SettingsOrderBy.direction.descending,
   ADDITIONSYEAR_KEY_RECORDS
  );

  SettingsOrderBy order = setting.getRelOrderBy();

  return
   DataStoreManager.get().dataStore(JDBCDataStore.class, "statisticsCount")
    .resetable()
    .loadWith("ayear", "number")
    .groupBy("ayear")
    .orderBy(SortHelper.getSortOrderForOrderBy(order))
    .make(AdditionYearRecords.class);

 }



 /**
  * Returns a DataStore of AdditionYearTitles bean filled with years
  * and the number of titles added in that year.
  *
  * @return The DataStore.
  */

 public DataStore<AdditionYearTitles> getAdditionTitles(@ConfigCurrentUser User user) {

  Setting setting = SortHelper.getSortSetting(
   user,
  "ayear",
   SettingsOrderBy.direction.descending,
   ADDITIONSYEAR_KEY_TITLES
  );

  SettingsOrderBy order = setting.getRelOrderBy();

  return
   DataStoreManager.get().dataStore(JDBCDataStore.class, "statisticsCount")
    .resetable()
    .loadWith("ayear", "number")
    .groupBy("ayear")
    .orderBy(SortHelper.getSortOrderForOrderBy(order))
    .make(AdditionYearTitles.class);

 }



 /**
  * Returns a DataStore of AdditionYearMusicians bean filled with years
  * and the number of musicians added in that year.
  *
  * @return The DataStore.
  */

 public DataStore<AdditionYearMusicians> getAdditionMusicians(@ConfigCurrentUser User user) {

  Setting setting = SortHelper.getSortSetting(
   user,
   "ayear",
   SettingsOrderBy.direction.descending,
   ADDITIONSYEAR_KEY_MUSICIANS
  );

  SettingsOrderBy order = setting.getRelOrderBy();

  return
   DataStoreManager.get().dataStore(JDBCDataStore.class, "musicians")
    .resetable()
    .loadWith("ayear", "number")
    .groupBy("ayear")
    .orderBy("-ayear")
    .orderBy(SortHelper.getSortOrderForOrderBy(order))
    .make(AdditionYearMusicians.class);

 }



 /**
  * Returns all records added to the database in the specified year.
  *
  * @param year The year.
  *
  * @return A DataStore filled with Record beans.
  */

 public DataStore<Record> getRecordsForYear(String year) {

  return dataStoreManager.makeDataStore(
   "application.beans.ResetDataStore", "statisticsCount",
   "select * from records " +
    "where cast(extract(year from created) as integer)=?" +
    " order by " + Utilities.getParam(MergedPropertyResourceBundle.getMergedBundle("SearchEngine"), "OrderRecords", null),
   new Object[] {Integer.valueOf(year)},
   Record.class
  );

 }



 /**
  * Returns all titles added to the database in the specified year.
  *
  * @param year The year.
  *
  * @return A DataStore filled with Titles beans.
  */

 public DataStore<Title> getTitlesForYear(String year) {

  return dataStoreManager.makeDataStore(
   "application.beans.ResetDataStore", "statisticsCount",
   "select titles.id, titles.interpreter, titles.title, titles.duration, records.interpreter sr$interpreter, records.title sr$title  from titles, records" +
   " where titles.record_id=records.id and cast(extract(year from titles.created) as integer)=?" +
   " order by " + Utilities.getParam(MergedPropertyResourceBundle.getMergedBundle("SearchEngine"), "OrderTitles", null),
   new Object[] {Integer.valueOf(year)},
   Title.class
  );

 }



 /**
  * Returns all musicians added to the database in the specified year.
  *
  * @param year The year.
  *
  * @return A DataStore filled with Musician beans.
  */

 public DataStore<Musician> getMusiciansForYear(String year) {

  return dataStoreManager.makeDataStore(
   "application.beans.ResetDataStore", "statisticsCount",
   "select id, name, instrument_id from musicians " +
    "where cast(extract(year from created) as integer)=?" +
    " order by name",
   new Object[] {Integer.valueOf(year)},
   Musician.class
  );

 }



 public TitleStatisticBean getTitlesStatistics() {

  return
   dataStoreManager.dataStore(ListDataStore.class, "statisticsCount")
    .resetable()
    .sourceList(this::getResultTitles)
    .make(TitleStatisticBean.class)
    .readBean();

 }



 private BeanList<TitleStatisticBean> getResultTitles(int o, int c)  {

  AtomicLong totalTitlesCount = new AtomicLong(0);
  AtomicLong knownTotalDuration = new AtomicLong(0);
  AtomicLong unknownTitlesCount = new AtomicLong(0);
  AtomicLong knownTitleCount = new AtomicLong(0);

  TitleStatisticBean statisticBean = null;

  dataStoreManager.dataStore(ResetDataStore.class, "statisticsCount")
   .loadWith("id", "duration")
   .make(Title.class)
   .forEach(tit -> {
    totalTitlesCount.getAndIncrement();

    String duration = getDuration(tit);
    int dur = 0;
    if (!Utilities.isEmpty(duration))
     try {
      dur = Utilities.getDurationFromString(duration);
     } catch (NumberFormatException e) {
       // Just leave dur untouched!
     }

    if (dur == 0)
     unknownTitlesCount.getAndIncrement();
    else {
     knownTitleCount.getAndIncrement();
     knownTotalDuration.getAndAdd(dur);
    }
   });

  statisticBean = BeanFactory.make(TitleStatisticBean.class);

  statisticBean.setTotalTitleCount(totalTitlesCount.longValue());
  statisticBean.setKnownTitleCount(knownTitleCount.longValue());
  statisticBean.setUnknownTitleCount(unknownTitlesCount.longValue());
  statisticBean.setKnownTotalDuration(knownTotalDuration.longValue());

  long estimatedAverage = Utilities.divide(knownTotalDuration.intValue(), totalTitlesCount.intValue()).intValue();
  long estimatedAdditional = estimatedAverage * unknownTitlesCount.intValue();
  statisticBean.setEstimatedAverage(estimatedAverage);
  statisticBean.setEstimatedUnknown(estimatedAdditional);
  statisticBean.setEstimatedTotal(knownTotalDuration.longValue() + estimatedAdditional);

  BeanList<TitleStatisticBean> res = new BeanList<>(TitleStatisticBean.class);
  res.add(statisticBean);

  return res;

 }



 private String getDuration(Title tit) {

  try {
   return tit.getDuration();
  } catch (Exception e) {
   return null;
  }

 }



 /**
  * Gets statistics about musicians.
  *
  * @return A bean with statistic information.
  */

 public MusicianStatisticBean getMusiciansStatistics() {

  return
   dataStoreManager.dataStore(ListDataStore.class, "statisticsCount")
    .resetable()
    .sourceList(this::getResultMusicians)
    .make(MusicianStatisticBean.class)
    .readBean();

 }



 private synchronized BeanList<MusicianStatisticBean> getResultMusicians(int o, int c) {

  AtomicLong musiciansWithAge = new AtomicLong(0);
  AtomicLong musiciansWithDeath = new AtomicLong(0);
  AtomicLong totalAge = new AtomicLong(0);
  AtomicLong minAge = new AtomicLong(Integer.MAX_VALUE);
  AtomicLong maxAge = new AtomicLong(0);
  AtomicLong maxAgeLiving = new AtomicLong(0);
  List<Musician> youngest = new ArrayList<>();
  AtomicInteger youngestIndex = new AtomicInteger(0);
  List<Musician> eldest = new ArrayList<>();
  AtomicInteger eldestIndex = new AtomicInteger(0);
  List<Musician> eldestLiving = new ArrayList<>();
  AtomicInteger eldestLivingIndex = new AtomicInteger(0);

  MusicianStatisticBean statisticBean = null;

  dataStoreManager.dataStore(ResetDataStore.class, "statisticsCount")
   .loadWith("id", "name", "born", "died", "instrumentId")
   .where(
    GIVEN("born")
   )
   .orderBy("name")
   .make(Musician.class)
   .forEach(mus -> {
    int age = mus.getAge();

    // Total for average.
    totalAge.addAndGet(age);

    if (age != 0) {
     musiciansWithAge.incrementAndGet();  // Count for average.

     int i = minAge.intValue();

     if (age < i) {
      // Younger.
      minAge.set(age);
      youngestIndex.getAndSet(youngest.size());
      youngest.add(mus);
     }
     else if (age == i)
      youngest.add(mus);  // As old as the youngest.

     i = maxAge.intValue();

     if (age > i) {
      // Elder.
      maxAge.set(age);
      eldestIndex.getAndSet(eldest.size());
      eldest.add(mus);
     }
     else if (age == i)
      eldest.add(mus);  // As old as the eldest.

     if (Utilities.isEmpty(mus.getDied())) {
      // Musician still living? Then maximum calculation for the eldest living musician.
      i = maxAgeLiving.intValue();
      if (age > i) {
       maxAgeLiving.set(age);
       eldestLivingIndex.getAndSet(eldestLiving.size());
       eldestLiving.add(mus);
      } else if (age == i)
       eldestLiving.add(mus);
     }
    }

    if (!Utilities.isEmpty(mus.getDied()))
     musiciansWithDeath.incrementAndGet();

   }
  );

  // Create StatisticBean and set result values.

  statisticBean = BeanFactory.make(MusicianStatisticBean.class);

  statisticBean.setMusiciansAverageAge(totalAge.intValue() / musiciansWithAge.intValue());

  statisticBean.setMusiciansMinAge(minAge.intValue());

  statisticBean.setMusiciansWithAge(musiciansWithAge.longValue());
  statisticBean.setMusiciansWithDeath(musiciansWithDeath.longValue());

  BeanList<Musician> musicians = new BeanList<>(Musician.class);
  for (int id = youngestIndex.intValue(); id < youngest.size(); id ++)
   musicians.add(youngest.get(id));
  statisticBean.setMusiciansYoungestMusicians(musicians);

  musicians = new BeanList<>(Musician.class);
  for (int id = eldestIndex.intValue(); id < eldest.size(); id ++)
   musicians.add(eldest.get(id));
  statisticBean.setMusiciansEldestMusicians(musicians);

  musicians = new BeanList<>(Musician.class);
  for (int id = eldestLivingIndex.intValue(); id < eldestLiving.size(); id ++)
   musicians.add(eldestLiving.get(id));
  statisticBean.setMusiciansEldestLivingMusicians(musicians);

  BeanList<MusicianStatisticBean> res = new BeanList<>(MusicianStatisticBean.class);
  res.add(statisticBean);

  return res;

 }



 /**
  * Changes the sort order for the years.
  *
  * @param property the property to sort.
  */

 public void orderByAdditionsRecords(@ConfigCurrentUser User user, String property) {

  if (logger.isDebugEnabled())
   logger.debug(("orderByAdditionsRecords(): sorting list for user: " + user.getId()));

  SortHelper.switchOrder(
   user,
   ADDITIONSYEAR_KEY_RECORDS,
  "ayear",
   application.beans.SettingsOrderBy.direction.none, property,
  "statisticsCount",
  0
  );

 }



 /**
  * Gets the current order settings for addition records.
  *
  * @param user For this user.
  *
  * @return The settings.
  */

 public SettingsOrderBy getOrderByAdditionsRecords(@ConfigCurrentUser User user) {

  Setting setting =
   SortHelper.getSortSetting(
    user,
   "ayear",
    SettingsOrderBy.direction.none,
    ADDITIONSYEAR_KEY_RECORDS
   );

  return setting.getRelOrderBy();

 }



 /**
  * Changes the sort order for the years.
  *
  * @param property the property to sort.
  */

 public void orderByAdditionsTitles(@ConfigCurrentUser User user, String property) {

  if (logger.isDebugEnabled())
   logger.debug(("orderByAdditionsTitles(): sorting list for user: " + user.getId()));

  SortHelper.switchOrder(
   user,
   ADDITIONSYEAR_KEY_TITLES,
   "ayear",
   application.beans.SettingsOrderBy.direction.none, property,
   "statisticsCount",
   0
  );

 }



 /**
  * Gets the current order settings for addition records.
  *
  * @param user For this user.
  *
  * @return The settings.
  */

 public SettingsOrderBy getOrderByAdditionsTitles(@ConfigCurrentUser User user) {

  Setting setting =
   SortHelper.getSortSetting(
    user,
   "ayear",
    SettingsOrderBy.direction.none,
    ADDITIONSYEAR_KEY_TITLES
   );

  return setting.getRelOrderBy();

 }



 /**
  * Changes the sort order for the years.
  *
  * @param property the property to sort.
  */

 public void orderByAdditionsMusicians(@ConfigCurrentUser User user, String property) {

  if (logger.isDebugEnabled())
   logger.debug(("orderByAdditionsMusicians(): sorting list for user: " + user.getId()));

  SortHelper.switchOrder(
   user,
   ADDITIONSYEAR_KEY_MUSICIANS,
   "ayear",
   application.beans.SettingsOrderBy.direction.none, property,
   "statisticsCount",
   0
  );

 }



 /**
  * Gets the current order settings for addition records.
  *
  * @param user For this user.
  *
  * @return The settings.
  */

 public SettingsOrderBy getOrderByAdditionsMusicians(@ConfigCurrentUser User user) {

  Setting setting =
   SortHelper.getSortSetting(
    user,
    "ayear",
    SettingsOrderBy.direction.none,
    ADDITIONSYEAR_KEY_MUSICIANS
   );

  return setting.getRelOrderBy();

 }

}
