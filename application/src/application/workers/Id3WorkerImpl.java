package application.workers;

import core.exceptions.MCException;
import application.beans.LogbookEntry;
import baked.application.beans.Record;
import baked.application.beans.Title;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;
import core.util.WorkerHelper;
import org.apache.logging.log4j.Logger;

import java.util.List;



/**
 * A worker for retrieving ID3 Tags for MP3 generation.
 *
 * @version 2023-09-05
 * @author pilgrim.lutz@imail.de
 */

public class Id3WorkerImpl implements Id3Worker {

 private static Logger logger;
 private static DataStoreManager dataStoreManager;

 @WorkerHelper()
 private LogbookHelper logbookHelper;



 /**
  * Get the ID3 Tags as CSV.
  *
  * @param interpreter The interpreter to search for.
  * @param title The title of the record to search for.
  * @return CSV formatted Tags for all titles of the found record.
  */

 public Record getId3Tags(String interpreter, String title) {

  logger.debug(() -> "getting ID3 tags for: " + interpreter + "/" + title);

  DataStore<Record> ds =  dataStoreManager.makeDataStore(
   "Id3Tags",
   new String[] {interpreter + "%", title + "%"}
  );

  List<Record> all =  ds.read();

  if (all.size() == 0)
   throw new MCException("Id3NotFound");
  if (all.size() > 1)
   throw new MCException("Id3Ambigous");

  Record rec = all.get(0);

  DataStore<Title> tits = rec.getColTitles();

  if (tits.count() == 0)
   throw new MCException("Id3NoTitles");

  logbookHelper.writeLogbook(rec, null, null, null, LogbookEntry.verbs.getId3Tags);

  return rec;

 }

}
