package application.workers;

import core.base.WorkerManager;


public interface TextWorker extends WorkerManager.IWorker {

 String get(String key);

 String getParsed(String text);

}
