package application.workers;

import application.aspects.annotations.NeedsRight;
import application.aspects.annotations.SecurityControl;
import application.beans.Rights;
import baked.application.beans.Login;
import baked.application.beans.User;
import core.aspects.annotations.CatchException;
import core.aspects.annotations.OnlyLocal;
import core.base.WorkerManager;

import java.util.List;



public interface LoginWorker extends WorkerManager.IWorker {

 @SecurityControl
 @CatchException
 void login(Boolean userPasswordAllowed, Boolean guestAllowed, Boolean useTicket);

 String activateAutoLogin();

 public List<User> getAllUsers();

 @NeedsRight("seeCurrentLogins")
 List<Login> getAllLogins();

 Login getLogin();

 @NeedsRight("deleteCurrentLogins")
 public void deleteLogins(List<Login> logins);

 @OnlyLocal()
 public Rights rightsForSessionId(String sessionId);

 public void logoff();

}
