package application.workers;



import core.base.Request;
import core.exceptions.MCException;
import core.util.Property;
import core.util.SecretService;
import core.util.Utilities;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.*;



/**
 * The NavigatorImpl stores all URLs a user visited in MC.
 *
 * <p>The information is used for generating navigation links in the UI.</p>
 *
 * @version 2023-09-05
 * @author pilgrim.lutz@imail.de
 *
 */

public class NavigatorImpl implements Navigator {

 private static Logger logger;

 @Property("Navigator.beanKeys")
 private static Map<String, String> beanKeys = null;

 private static final String SESSION_PARAMETER_NAVSTACK = "navstack";



 /**
  * This is the main method of the NavigatorImpl.
  *
  * <p>The NavigatorImpl manages a list of "positions". A position is made up from a so called friendly name
  * (for displaying to the user), the URL and the parameter map from the request.</p>
  *
  * <p>This method takes such a position and tries to find it in the list (beginning on the end). If it is found
  * the list will be shortened to this entry. Otherwise the new position will be appended to the
  * list.</p>
  *
  * <p>The method returns the position before the given position. This position will be used for
  * rendering a back button.</p>
  *
  * @param friendlyName A friendly name for displaying to the user.
  * @param dontMentionParams If this parameter is set to true the method will not mention parameters
  * during searching in the list.
  * @param overwrite If true  replace the found position with the current position.
  * @param paramBlackList A list of parameters which will be ignored.
  *
  * @return The "back"-position.
  */

 @Override
 public NavigatorPosition position(String friendlyName,
                                   Boolean dontMentionParams, Boolean overwrite,
                                   ArrayList<String> paramBlackList,
                                   Boolean skip){

  Request req = Request.get();

  Map<String, String[]> params = req.getParameterMap();
  String link = "/" + req.getAppPath() + req.getTemplatePath();

  if (logger.isDebugEnabled())
   logger.debug("position() for session: \"" + req.getSessionId() + "\" \"" + friendlyName + "\" \"" + req.getUri() + "\" stack depth: " + getStackDepth());

  ArrayList<NavStackElement> stack = getStack();

  // URI parameter "mm" resets stack.

  if (req.getUriParameter("mm", "0").equals("1"))
   stack.clear();

  if (paramBlackList == null)
   paramBlackList = new ArrayList<>();

  int id = -1;

  if (stack.size() > 0)
   if (logger.isDebugEnabled())
    logger.debug("position() checking for: \"" + friendlyName + "|" + link + "|" + NavStackElement.getQueryString(params));

  for (id = stack.size() - 1; id >= 0; id --) {
   NavStackElement ne = stack.get(id);
   if (logger.isDebugEnabled())
    logger.debug("position() checking    : \"" + ne.getFriendlyName() + "|" + ne.getLink() + "|" + ne.getQueryString());

   if (ne.linksEqual(link)  &&  (dontMentionParams | ne.paramsEqual(params, paramBlackList)))
    break;
  }


  if (id < 0) {
   // not found
   if (!skip)
    stack.add(new NavStackElement(friendlyName, link, params, paramBlackList));
  }
  else {
   if (overwrite)
    stack.set(id, new NavStackElement(friendlyName, link, params, paramBlackList));

   // Just to keep the amount of data for serialization smaller.
   for (int id1 = stack.size() - 1; id1 > id; id1 --)
    stack.remove(id1);

   stack.trimToSize();
  }

  // Make sure session replication will be triggered!
  Request.get().getSession().setAttribute(SESSION_PARAMETER_NAVSTACK, stack);

  if (stack.size() > 1)
   return makeNavigatorPosition(req, stack.get(stack.size() - 2));

  return null;

 }



 private NavigatorPosition makeNavigatorPosition(Request req, NavStackElement ne) {

  String ln = ne.getLink();
  String qs = ne.getQueryString();

  return new NavigatorPosition(
   ne.getFriendlyName(),
   req.getServerUrl(ln) + (ln.endsWith(".vm") ? ";nb=1" : "")
   + ((qs.length() > 0) ? "?" + qs : "")
  );

 }



 /**
  * Return the current position.
  *
  * @return The current position.
  */

 @Override
 public NavigatorPosition currentPosition() {

  ArrayList<NavStackElement> stack = getStack();

  if (stack.size() > 0) {
   NavStackElement ne = stack.get(stack.size() - 1);
   String sd = ne.getQueryString();
   return new NavigatorPosition(
    ne.getFriendlyName(),
    ne.getLink()
    + ((sd.length() > 0) ? "?" + sd : "")
   );
  }

  return null;

 }



 /**
  * Returns the history of links the user selected to reach the current position.
  * <p>This is the complete stack of positions.</p>
  *
  * @return The history list
  */

 @Override
 public ArrayList<NavigatorPosition> history() {

  ArrayList<NavStackElement> stack = getStack();

  if (stack.size() > 1) {
   ArrayList<NavigatorPosition> ret = new ArrayList<>();

   for (int id = stack.size() - 2; id >= 0; id --)  // Suppress the top most entry.
    ret.add(makeNavigatorPosition(Request.get(), stack.get(id)));

   return ret;
  }

  return null;

 }



 /**
  * This class is a return value for some methods of the NavigatorImpl.
  *
  * <p>It contains the friendly name and a complete link.</p>
  *
  */

 public static class NavigatorPosition {

  private final String friendlyName;
  private final String link;



  NavigatorPosition(String friendlyName, String link) {

   this.friendlyName = friendlyName;
   this.link = link;

  }



  /**
   * Returns the friendly name of this NavigatorPosition.
   * @return The name.
   */

  public String getFriendlyName() {

   return friendlyName;

  }



  /**
   * Returns the link of this NavigatorPosition.
   * @return The link.
   */

  public String getLink() {

   return link;

  }

 }



 private static class NavStackElement implements Serializable {

  private final String friendlyName;
  private final String link;
  private final Map<String, String[]> params;



  NavStackElement(String friendlyName, String link, Map<String, String[]> params, ArrayList<String> paramBlackList) {

   this.friendlyName = friendlyName;
   this.link = link;

   this.params = new HashMap<>();

   if (params != null) {
    Set<String> ps = params.keySet();
    Iterator<String> it = ps.iterator();

    while (it.hasNext()) {
     String key = it.next();
     if (!paramBlackList.contains(key))  // Parameter not in blacklist.
      this.params.put(key, params.get(key));
    }
   }

  }



  String getLink()  {

   return link;

  }



  String getFriendlyName() {

   return friendlyName;

  }



  String getQueryString() {

   return getQueryString(params);

  }



  static String getQueryString(Map<String, String[]> params) {

   Iterator<String> it = params.keySet().iterator();
   StringBuilder sb = new StringBuilder();

   while (it.hasNext()) {
    if (sb.length() > 0)
     sb.append("&");
    String key = it.next();
    String[] ps = params.get(key);
    try {
     if (ps.length > 0)
      sb.append(Utilities.encodeURIComponent(key)).append("=");
     for (String p : ps)
      sb.append(Utilities.encodeURIComponent(p));
    }
    catch (UnsupportedEncodingException e) {
     throw new MCException(e);
    }
   }

   return sb.toString();

  }



  boolean paramsEqual(Map<String, String[]> theOtherMap, ArrayList<String> paramBlackList) {

   Set<String> ps = params.keySet();
   Set<String> tos = theOtherMap.keySet();

   if (ps.size() != tos.size()) return false;  // Sets differ in size -> Maps aren't equal.

   Iterator<String> it = ps.iterator();

   while (it.hasNext()) {
    String key = it.next();

    if (!paramBlackList.contains(key)) {  // Parameter not in blacklist.
     String[] toe = theOtherMap.get(key);
     if (toe == null) return false;  // Entry not found in other map -> Maps aren't equal.

     for (int id = 0; id < toe.length; id ++)
      if (compareParam(key, toe, id)) return false;  // Entry values differ -> Maps aren't equal.
    }
   }

   return true;  // Maps must be equal.

  }



  private boolean compareParam(String key, String[] toe, int id) {

   String pval = (params.get(key))[id];

   if (beanKeys.containsKey(key)) {  // Parameter is a crypt key!
    String purpose = "bean." + beanKeys.get(key);
    Map<String, String> sd = Utilities.getMapFromString(SecretService.get().getAgent(purpose).decrypt(pval));
    Map<String, String> sdt = Utilities.getMapFromString(SecretService.get().getAgent(purpose).decrypt(toe[id]));

    return !sd.get("k").equals(sdt.get("k"));  // Property "k" is the id of the requested bean.
   }

   return !toe[id].equals(pval);

  }



  boolean linksEqual(String theOtherLink) {

   return link.equals(theOtherLink);

  }

 }



 /**
  * Adds a parameter to the last element on the navigation stack.
  *
  * <p>Called from templates (via AJAX) to manipulate the navigation.</p>
  *
  * @param param The parameter to add.
  * @param val Its value.
  */

 @Override
 public void addParameter(String param, String val) {

  logger.debug(() -> "addParameter(): \"" + param + "\" \"" + val + "\"");

  ArrayList<NavStackElement> stack = getStack();

  NavStackElement ne = stack.get(stack.size() - 1);

  ne.params.put(param, new String[] {val});

 }



 /**
  * Deletes a parameter from the last element of the navigation stack.
  *
  * <p>Called from templates (via AJAX) to manipulate the navigation.</p>
  *
  * @param param The parameter to delete.
  */

 @Override
 public void deleteParameter(String param) {

  logger.debug(() -> "deleteParameter(): \"" + param + "\"");

  ArrayList<NavStackElement> stack = getStack();

  NavStackElement ne = stack.get(stack.size() - 1);

  ne.params.remove(param);

 }



 /**
  * Returns the current depth of the navigation step.
  *
  * @return The depth
  */

 @Override
 public int getStackDepth() {

  ArrayList<NavStackElement> stack = getStack();

  return stack.size();

 }



 private ArrayList<NavStackElement>getStack() {

  @SuppressWarnings("unchecked")
  ArrayList<NavStackElement> navstack =
   (ArrayList<NavStackElement>)Request.get().getSession().getAttribute(SESSION_PARAMETER_NAVSTACK);

  if (navstack == null)
   navstack = new ArrayList<>();

  return navstack;

 }

}
