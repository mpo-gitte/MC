package application.workers;

import core.base.WorkerManager;



public interface ProgramInformationWorker extends WorkerManager.IWorker {

 String getVersion();

 String getCopyright();

 String getStage();

 String getTag();

 String getImprint();

 String prepareEtag(Object ... tags);

 String getBuildDate();

 String getDeploymentDate();

 String getStartDate();

}
