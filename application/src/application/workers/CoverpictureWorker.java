package application.workers;

import application.aspects.annotations.NeedsRight;
import baked.application.beans.Coverpicture;
import baked.application.beans.Record;
import core.bakedBeans.BeanList;
import core.base.WorkerManager;

import java.util.List;



public interface CoverpictureWorker extends WorkerManager.IWorker {

 @NeedsRight("editCoverpictures")
 public List<Coverpicture> selectDescriptions(String description);

 @NeedsRight("editCoverpictures")
 public List<Coverpicture> selectOrigins(String origin);

 @NeedsRight("editCoverpictures")
 public Coverpicture getTemplateCoverpicture(Record rec);

 @NeedsRight("editCoverpictures")
 public Coverpicture saveCoverpicture(Record rec, Coverpicture copic);

 @NeedsRight("editCoverpictures")
 public void deleteCoverpictures(BeanList<Coverpicture> copics);

}
