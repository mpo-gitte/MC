package application.workers;


import application.aspects.annotations.NeedsRight;
import baked.application.beans.Role;
import baked.application.beans.User;
import core.aspects.annotations.CatchException;
import core.aspects.annotations.Transaction;
import core.bakedBeans.DataStore;
import core.base.WorkerManager;

import java.util.List;

public interface UsersWorker extends WorkerManager.IWorker {

 @NeedsRight("changePassword")
 @CatchException
 void setPassword(String cpw, String npw, String npwr);

 @NeedsRight("userManagement")
 DataStore<User> getAllUsers();

 @NeedsRight("userManagement")
 User saveUser(User user);

 @NeedsRight("userManagement")
 boolean checkUniqueLoginname(String loginname);

 @NeedsRight("userManagement")
 List<Role> getRoles();

 @NeedsRight("userManagement")
 User getTemplateUser();

 @NeedsRight("userManagement")
 @Transaction
 String addUser(User user);

 @NeedsRight("userManagement")
 User lockUnlockUser(User user);

 @NeedsRight("userManagement")
 void deleteUsers(List<User> users);

 @NeedsRight("userManagement")
 @Transaction()
 String newPassword(User user);

}
