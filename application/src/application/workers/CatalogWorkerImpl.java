package application.workers;



import application.configs.ConfigCurrentUser;
import baked.application.beans.Record;
import baked.application.beans.*;
import core.bakedBeans.Utilities;
import core.bakedBeans.*;
import core.base.WorkerManager;
import core.exceptions.MCException;
import core.util.*;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static core.bakedBeans.Operator.*;



/**
 * Methods for managing the catalog.
 *
 * @version 2025-01-06
 * @author lp
 *
*/

public class CatalogWorkerImpl implements WorkerManager.IWorker {

 private static Logger logger;
 private static DataStoreManager dataStoreManager;
 @Property("CatalogWorker.Watcher.schedule")
 private static String watcherSchedule = "0 0 21 * * ?";
 @Property("CatalogWorker.Process.max")
 private static int processMax = 100;
 @Property("CatalogWorker.Signal.after")
 private static int signalAfter = 5;
 @Property("CatalogWorker.TagCloud.nrDrops")
 private static int nrTagCloudDrops = 25;
 @Property("CatalogWorker.TagCloud.blackList")
 private static List<String> blacklist = null;

 private final String SORTKEY_SEEALSO = "seeAlso.orderBy";

 BeanMap<CatalogMapping> map = null;
 Long dsstamp = null;

 @Property("CatalogWorker.Denylist")
 private static List<String> denyList;
 @Property("CatalogWorker.PrefixStripList")
 private static List<String> prefixStripList;

 private static final HashMap<String, String[]> ordersSeeAlso =
  new HashMap<>();
 static {
  ordersSeeAlso.put("+interpreter", new String[] {"releaseyear", "title", "interpreter"});
//  ordersSeeAlso.put("+interpreter", new String[] {"+interpreter"});
  ordersSeeAlso.put("+title", new String[] {"interpreter", "title"});
//  ordersSeeAlso.put("+title", new String[] {"title"});
  ordersSeeAlso.put("+releaseyear", new String[] {"interpreter", "title", "releaseyear"});
//  ordersSeeAlso.put("+releaseyear", new String[] {"releaseyear"});
  ordersSeeAlso.put("-interpreter", new String[] {"releaseyear", "title", "-interpreter"});
//  ordersSeeAlso.put("-interpreter", new String[] {"-interpreter"});
  ordersSeeAlso.put("-title", new String[] {"interpreter", "-title"});
//  ordersSeeAlso.put("-title", new String[] {"-title"});
  ordersSeeAlso.put("-releaseyear", new String[] {"interpreter", "title", "-releaseyear"});
//  ordersSeeAlso.put("-releaseyear", new String[] {"-releaseyear"});
 }



 /**
  * When a worker gets loaded the WorkerManager will call this method.
  *
  * <p>The default implementation is empty.</p>
  *
  * @see WorkerManager
  */

 @Override
 public void onLoad() {

  // TimerJob for updating the catalog.

  Timer.get()
   .run("CatalogWorker.MakeCatalog", watcherSchedule, true, this::makeCatalog);

 }



 private void makeCatalog() {
  logger.info("MakeCatalog: Looking for catalog requests.");

  AtomicInteger max = new AtomicInteger();
  max.set(0);
  AtomicInteger sig = new AtomicInteger();
  sig.set(0);

  prepareMap();


  dataStoreManager.dataStore(JDBCDataStore.class, "catalogRequests")
   .make(CatalogRequest.class)
   .forEach(catreq -> {
     if (logger.isDebugEnabled())
      logger.debug("MakeCatalog: Working on request: " + catreq.getId());

     Record rec = catreq.getSubRecord();

     String recInterpreter = rec.getInterpreter();

     if (logger.isDebugEnabled())
      logger.debug("MakeCatalog: Catalogue: rec.id=" + rec.getId() + " (\"" + recInterpreter + "\")");

     // Delete old catalog records. This removes the current record from all CatalogItems.
     CatalogHelper.removeFromCatalog(rec);

     // Map
     List<String> names = getInterpreters(recInterpreter);

     // Let us have a look at the titles:
     makeCatalogForTitles(rec, recInterpreter, names);

     names.forEach(i -> {
      logger.debug(() -> "MakeCatalog: Processing: \"" + i + "\"");

      if (denyList.contains(i.toUpperCase()))
       // Filtered.
       logger.debug(() -> "MakeCatalog: Filtered: \"" + i + "\"");
      else {
       CatalogItem ci = getCatalogItem(i);

       CatalogRecord cr = BeanFactory.make(CatalogRecord.class);
       cr.setCreated(LocalDateTime.now());
       cr.setRecordId(rec.getId());

       ci.getRelCatalogRecords().add(cr);
       ci.save();
      }
     });

     // Delete the catalog request.
     catreq.delete();

     // Tell the world about it.
     int id = sig.getAndIncrement();
     if (id >= signalAfter - 1) {
      dataStoreManager.signal(DataStore.DataStoreSignal.delete, "catalog", "catalogRequests");
      sig.set(0);
     }
    },
    catreq -> {
     int id = max.getAndIncrement();
     return id >= processMax - 1;
    },
    0, Integer.MAX_VALUE);

  // Clean empty CatalogItems.
  cleanup();

  // Tell the world we're ready.
  int id = sig.get();
  if (id > 0)
   dataStoreManager.signal(DataStore.DataStoreSignal.delete, "catalog");

  dataStoreManager.signal(DataStore.DataStoreSignal.delete, "totalCatalogRequests", "catalogRequests");

 }



 private List<String> getInterpreters(String interpreter) {

  // Map.
  String sd = map(interpreter);

  logger.debug(() -> "MakeCatalog: Map to: \"" + sd + "\"");

  // Split.
  List<String> names = Utilities.splitIssuers(sd);

  // Strip prefixes like "THE"...
  for (int id = 0; id < names.size(); id ++) {
   String s = names.get(id);
   for (String p : prefixStripList)
    if (s.toUpperCase().startsWith(p + " ")) {
     names.set(id, s.substring(p.length()).trim());
     break;
    }
  };

  return names;

 }



 private void makeCatalogForTitles(Record rec, String recinterpreter, List<String> names) {

  try {
   rec.getColTitles().forEach(tit -> {

    try {
     String titinterpreter = tit.getInterpreter();

     if (!Utilities.isEmpty(titinterpreter)  &&  !titinterpreter.equals(recinterpreter)) {
      // Interpreter on title isn't empty and is different to the interpreter on the record.
      List<String> titnames = getInterpreters(titinterpreter);
      names.addAll(
       titnames.stream()
        .filter(n -> !names.contains(n))
        .collect(Collectors.toList()));
     }
    }
    catch (Exception ex) { /* Does not bother us. Just no interpreters from titles. */ }
   });
  }
  catch (Exception ex) { /* Just no titles. Bummer! */ }

 }



 private void cleanup() {

  DataStore<CatalogItem> ds = dataStoreManager.makeDataStore(
   "catalogWorker.deleteCatalogItems",
   null
  );

  ds.forEach(cti -> {
   if (logger.isDebugEnabled())
    logger.debug("CatalogWorker.cleanup(): Deleting catalog item: " + cti.getId());

   // Delete the CatalogItem.
   cti.delete();
  });

 }



 private void prepareMap() {

  // Loading the map.

  DataStore<CatalogMapping> ds = dataStoreManager.dataStore(JDBCDataStore.class, "catalogMappings")
   .resetable()
   .make(CatalogMapping.class);

  if (!ds.getStamp().equals(dsstamp)) {
   dsstamp = ds.getStamp();

   map = new BeanMap<>(CatalogMapping.class, "fromString");

   map.put(ds.read());
  }

 }


 private CatalogItem getCatalogItem(String itemtext) {

  CatalogItem catitem = null;

  catitem = BeanFactory.make(CatalogItem.class);

  catitem.setText(itemtext);

  @SuppressWarnings("unchecked")
  Optional<CatalogItem> ci = (Optional<CatalogItem>)catitem.find("text").stream().findFirst();

  if (ci.isPresent())
   return ci.get();

  catitem.setCreated(LocalDateTime.now());

  return catitem;

 }



 private String map(String interpreter) {

  CatalogMapping catm = map.get(interpreter);

  if (catm == null)
   return interpreter;

  String toString = catm.getToString();

  if (!Utilities.isEmpty(toString))
   return toString;

  return "";

 }



 /**
  * Gets all catalog items.
  *
  * @param filter A filter (not implemented yet).

  * @return A DataStore containing CatalogItems.
  */

 public DataStore<CatalogItem> getCatalogItems(String filter) {

  logger.debug(() -> "getCatalogItems(): filter: " + filter);

  return
   getCatalogItemsFiltered(filter);

 }



 /**
  * Gets the number of pending CatalogRequests.
  *
  * @return The number.
  */

 public Long getTotalCatalogRequests() {

  logger.debug(() -> "getTotalCatalogRequests(): start");

  Long size = dataStoreManager.dataStore(JDBCDataStore.class, "totalCatalogRequests")
   .resetable()
   .loadWith("id")
   .make(CatalogRequest.class)
   .count();

  logger.debug(() -> "getTotalCatalogRequests(): size=" + size);

  return
   size;

 }



 /**
  * Gets the current number of CatalogRequests.
  *
  * @return The number.
  */

 public Long getCurrentCatalogRequests() {

  logger.debug(() -> "getCurrentCatalogRequests(): start");

  Long size = dataStoreManager.dataStore(JDBCDataStore.class, "catalogRequests")
   .resetable()
   .loadWith("id")
   .make(CatalogRequest.class)
   .count();

  logger.debug(() -> "getCurrentCatalogRequests(): size=" + size);

  return
   size;

 }



 /**
  * Rebuilds the catalog.
  *
  * <p>Creates a CatalogRequest for each Record.</p>
  *
  * <p>The "real" rebuild will be made by the TimerJob above.</p>
  */

 public void rebuildCatalog() {

  logger.debug(() -> "rebuildCatalog(): ");

  DBConnection con = DB.get().getConnection();


  // Delete pending requests.

  PreparedStatement stmt = con.prepareStatement(
   "delete from catalogrequests"
  );

  try {
   stmt.execute();
  }
  catch (SQLException e) {
   throw new MCException(e);
  }


  // Create a CatalogRequest for each Record.

  stmt = con.prepareStatement(
   "insert into catalogrequests (id, created, record_id) " +
   "select nextval('catalogrequests_id'), records.created, records.id from records"
  );

  try {
   stmt.execute();
  }
  catch (SQLException e) {
   throw new MCException(e);
  }

  DB.get().releaseConnection(con);


  // Notice.

  dataStoreManager.signal(DataStore.DataStoreSignal.write, "catalogRequests", "totalCatalogRequests");

 }



 /**
  * Returns all mappings.
  *
  * @return A DataStore with the current mappings.
  */

 public DataStore<CatalogMapping> getMappings() {

  return
   dataStoreManager.dataStore(JDBCDataStore.class, "catalogMappings")
    .resetable()
    .orderBy("fromString", "toString")
    .make(CatalogMapping.class);

 }



 /**
  * Saves a CatalogMapping bean.
  *
  * @param catmap The CatalogMapping bean.
  *
  * @return The saved bean.
  */

 public CatalogMapping saveCatalogMapping(CatalogMapping catmap) {

  catmap.save();

  dataStoreManager.signal(DataStore.DataStoreSignal.write, "catalogMappings");

  return catmap;

 }



 /**
  * Deletes the given catalog mappings.
  *
  * @param catmappings The catalog mappings to delete.
  */

 public void deleteCatalogMappings(BeanList<CatalogMapping> catmappings) {

  DBConnectionStore.getConnection();

  for (CatalogMapping catmap : catmappings)
   if (!Utilities.isEmpty(catmap))
    catmap.delete();

  dataStoreManager.signal(DataStore.DataStoreSignal.delete, "catalogMappings");

  DBConnectionStore.releaseConnection();

 }



 /**
  * Returns a DataStore of CatalogItems with the top issuers.
  *
  * @return The DataStore
  */

 public DataStore<CatalogItem> getTopIssuersDS() {

  logger.debug("getTopIssuersDS(): getting top issuers.");

  return getTopIssuers();

 }



 private DataStore<CatalogItem> getTopIssuers() {

  return
   dataStoreManager.dataStore(DataStore.class, "catalog")
    .resetable()
    .sortBy(
     new BeanComparator<CatalogItem>("") {

      @Override
      public int compare(CatalogItem o1, CatalogItem o2) {
       @SuppressWarnings("unchecked")
       Comparable<Object> v1 = (Comparable)o1.getRelCatalogRecords().size();
       @SuppressWarnings("unchecked")
       Comparable<Object> v2 = (Comparable)o2.getRelCatalogRecords().size();

       return
        v2.compareTo(v1);
      }
     }
    )
    .sourceStream(() -> {
      return
       getCatalogItemsFiltered("").read().stream();
    })
    .make(CatalogItem.class);

 }



 /**
  * Returns a DataStore of CatalogItems with the tag cloud.
  *
  * @return The DataStore
  */

 public DataStore<TagCloudDrop> getTagCloudDropsDS() {

  logger.debug("getTopIssuersDS(): getting top issuers.");

  return
   dataStoreManager.dataStore(DataStore.class, "catalog")
    .resetable()
    .sortBy(new BeanComparator<CatalogItem>("label"))
    .onReady(bl -> {
     @SuppressWarnings("unchecked")
     BeanList<TagCloudDrop> bl1 = (BeanList<TagCloudDrop>) bl;
     scale(bl1);
    })
    .sourceStream(this::getTagCloudDropStream)
    .make(TagCloudDrop.class);

 }



 private void scale(BeanList<TagCloudDrop> bl) {

  @SuppressWarnings("unchecked")
  BeanList<TagCloudDrop> drops = bl;
  int maxNumber = 0;
  int minNumber = Integer.MAX_VALUE;

 for (TagCloudDrop dr : drops) {
  int number = 0;
   number = dr.getNumber();
   // number = number * number;  // Wasn't really nice...
   number = (int)Math.log1p(number);  // ... looks much better!
   minNumber = Math.min(minNumber, number);
   maxNumber = Math.max(maxNumber, number);
   dr.setWeight((double) number);
 }

 maxNumber -= minNumber;

 for (TagCloudDrop dr : drops)
  dr.setWeight(round((dr.getWeight() - minNumber) / maxNumber));

 }



 private Stream<TagCloudDrop> getTagCloudDropStream() {

  AtomicInteger nr = new AtomicInteger(0);
  AtomicInteger lastsize = new AtomicInteger(0);

  return getTopIssuersDS().read()
   .stream()
   .filter(this::filterBlacklist)
   .takeWhile((b) -> {
    int size = b.getRelCatalogRecords().size();
    boolean res = nr.getAndIncrement() < nrTagCloudDrops || size == lastsize.intValue();
    lastsize.set(size);

    return
     res;
   })
   .map(this::getTagCloudDrop);

 }



 private boolean filterBlacklist(CatalogItem b) {

  try {
   return !blacklist.contains(b.getText());
  }
  catch (Exception e) {
   return true;
  }

 }



 private TagCloudDrop getTagCloudDrop(CatalogItem b) {

  TagCloudDrop dr = BeanFactory.make(TagCloudDrop.class);

  dr.setLabel(b.getText());
  dr.setNumber(b.getRelCatalogRecords().size());
  dr.setCatalogItemId(b.getId());

  return dr;

 }



 private static DataStore<CatalogItem> getCatalogItemsFiltered(String filter) {

  return
   DataStoreManager.get().dataStore(JDBCDataStore.class, "catalog")
    .resetable()
    .loadWith("id", "text")
    .where(
     LIKE(
      UPPER("text"),
      (core.util.Utilities.isEmpty(filter) ? "" : filter.toUpperCase()) + "%"
     )
    )
    .orderBy("text")
    .make(CatalogItem.class);

 }



 private static Double round(double d) {

  double v = d * 100.;

  v = (double)(int)v;

  return v / 100.;

 }



 /**
  * Gets a list of records which are related to the given record.
  *
  * @param user The current user
  * @param rec The record
  *
  * @return A DataStore with records.
  */

 public DataStore<Record> getSeeAlso(@ConfigCurrentUser User user, Record rec) {

  String recInterpreter = rec.getInterpreter();

  // Build a list of names of interpreters.
  List<String> names = getInterpreters(recInterpreter);
  makeCatalogForTitles(rec, recInterpreter, names);

  // Search for related records via catalog.
  DataStore<Record> cat = dataStoreManager.dataStore(DataStore.class, "catalog")
   .resetable()
   .sourceStream(
    () ->
     names.stream().map(name ->
      getStream(name).map(
       crec -> {
        return
         crec.getSubRecord();
       }
      )
     ).flatMap(Function.identity()).distinct()
   )
   .sortBy(
    getSort(user, rec)
   )
   .taggedWith(rec)
   .make(Record.class);

  return
   cat;

 }



 private String[] getSort(User user, Record rec) {

  try {
   Setting setting =
    SortHelper.getSortSetting(user, "interpreter", SettingsOrderBy.direction.none, SORTKEY_SEEALSO, rec.getId());

   application.beans.SettingsOrderBy order =
    (application.beans.SettingsOrderBy) setting.get("relOrderBy");

   String[] properties =
    ordersSeeAlso.get(order.getOrderForOrderByClause() + order.get("property"));

   if (properties != null)
    return properties;

  }
  catch (Exception e) {
   // Default.
  }

  return
   ordersSeeAlso.get("+interpreter");

 }



 private static Stream<CatalogRecord> getStream(String name) {

  try {
   CatalogItem catitem = BeanFactory.make(CatalogItem.class);

   catitem.setText(name);

   @SuppressWarnings("unchecked")
   Optional<CatalogItem> ci =
    (Optional<CatalogItem>) (catitem.find("text").stream().findFirst());

   if (ci.isPresent())
    return ci.get().getRelCatalogRecords().stream();
  }
  catch (Exception e) {
   // Return empty stream if errors occur.
  }

  return Stream.empty();

 }



 /**
  * Gets the current order settings for SeeAlso
  *
  * @param user For this user.
  *
  * @return The settings.
  */

 public SettingsOrderBy getOrderBySeeAlso(@ConfigCurrentUser User user, Record rec) {

  Setting setting =
   SortHelper.getSortSetting(user, "+interpreter", SettingsOrderBy.direction.none, SORTKEY_SEEALSO, rec.getId());

  return
   setting.getRelOrderBy();

 }



 /**
  * Changes the sort order for SeeAlso.
  *
  * @param user The user owning the favourites lists.
  * @param property the property to sort.
  */

 public void orderBySeeAlso(@ConfigCurrentUser User user, String property, Record rec) {

  if (logger.isDebugEnabled())
   logger.debug(("orderBySeeAlso(): sorting lists for user: " + user.getId()));

  SortHelper.switchOrder(
   user,
   SORTKEY_SEEALSO,
   property,
   SettingsOrderBy.direction.ascending,
   property,
  "catalog",
   rec.getId()
  );

 }

}
