package application.workers;

import application.aspects.annotations.NeedsRight;
import core.aspects.annotations.OnlyLocal;
import core.bakedBeans.JDBCBean;
import core.base.WorkerManager;
import core.util.DB;

import java.util.List;

public interface DatabaseWorker extends WorkerManager.IWorker {

 DB getDB();

 @OnlyLocal()
 @NeedsRight("dumpData")
 List<? extends JDBCBean> getTableData(String beanName, List<String> columns);

}
