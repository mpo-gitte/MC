package application.workers;

import core.bakedBeans.BeanFactory;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;
import core.bakedBeans.JDBCBean;
import core.util.*;
import org.apache.logging.log4j.Logger;

import java.util.List;



/**
 * Worker provides access to the database for information and maintenance.
 *
 * @version 2023-11-30
 * @author pilgrim.lutz@imail.de
 */

public class DatabaseWorkerImpl implements DatabaseWorker {

 private static Logger logger;

 @Property("DatabaseWorker.LiquibasePath")
 private static String liquibasePath;
 @Property("DatabaseWorker.DropTables")
 private static boolean droptables;
 @Property("DatabaseWorker.TablesToDrop")
 private static List<String> tablesToDrop;
 @Property("DatabaseWorker.SequencesToDrop")
 private static List<String> sequencesToDrop;
 @Property("DatabaseWorker.TimesMaxDbConnections")
 private static int timesMaxDbConnections;

 private static DataStoreManager dataStoreManager;
 private static DB db;
 private static MovingIntAverage maxCons;



 /**
  * Initialises or modifies the database with LiquiBase.
  */

 @Override
 public void onLoad() {

  try {
   if (droptables) {
    // Dropping database content.

    logger.info("onLoad(): Dropping tables and sequences!");

    db.dropObjects(false, tablesToDrop);
    db.dropObjects(true, sequencesToDrop);
   }

   if (!core.util.Utilities.isEmpty(liquibasePath))
    LiquibaseSupport.init(db, liquibasePath);
   else
    logger.info("onLoad(): Liquibase support disabled!");
  }
  catch (Exception e) {
   logger.error("Can't work with Liquibase!\n\n" + e.getMessage());
  }

  maxCons =
   new MovingIntAverage(timesMaxDbConnections, 0);

 }



 /**
  * Returns the health state of the worker.
  *
  * <p>Called by the worker manager if requested.</p>
  *
  * @return ill: bad, healthy: good!
  */

 @Override
 public Health healthCheck() {

  maxCons.add(db.getNrUsedConnections());

  if (maxCons.getAverage() == db.getMaxConnections()) {
   logger.error(
    "healthCheck(): Reaching " + timesMaxDbConnections +
    " times the maximum of DB connections (" + db.getMaxConnections() + ")!"
   );
   return Health.ill;  // Got none!
  }

  //logger.info("healthCheck(): Ok!");

  return Health.healthy;  // Ok. DB is up.

 }



 /**
  * Returns the Database.
  *
  * @return The Database.
  */

 @Override
 public DB getDB() {

  return db;

 }



 /**
  * Gets the data from the given beans underlying table.
  *
  * @param beanName The name of the bean.
  * @param columns The columns. Currently not used. All columns will be queried.
  *
  * @return A List of bean.
  */

 @Override
 public List<? extends JDBCBean> getTableData(String beanName, List<String> columns) {

  logger.debug(() -> "getTableData() for bean \"" + beanName + "\"");

  JDBCBean b = BeanFactory.make(beanName);

  dataStoreManager.signal(DataStore.DataStoreSignal.write, "dump");  // Trying to save a little memory.

  DataStore<? extends JDBCBean> ds = dataStoreManager.makeDataStore(
   "application.beans.ResetDataStore", "dump",
//   "select " + sb.toString() + " from " + b.getSource() + " order by id",
   "select * from " + b.getSource() + " order by id",
   b.getClass()
  );

  return ds.read();

 }

}
