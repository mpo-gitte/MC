package application.workers;

import application.aspects.annotations.NeedsRight;
import baked.application.beans.*;
import baked.application.beans.Record;
import core.bakedBeans.DataStore;
import core.bakedBeans.PersistentBean;
import core.base.WorkerManager;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;



public interface SearchEngine extends WorkerManager.IWorker {

 @NeedsRight("search")
 ArrayList<List<? extends PersistentBean>> search(SearchPattern sp);

 LocalDateTime getNewLimit();

 Boolean isNew(Record rec);

 @NeedsRight("savesearchpatterns")
 SearchPattern saveSearchPattern(SearchPattern sp);

 @NeedsRight("savesearchpatterns")
 List<SearchPattern> getSearchPatterns();

 @NeedsRight("namesearchpatterns")
 void rename(SearchPattern sp, String newName);

 @NeedsRight("savesearchpatterns")
 void delete(List<SearchPattern> sps);

 DataStore<Musician> getMusicianSeeAlso(Record rec);

 @NeedsRight("indexSearch")
 DataStore<SearchIndexResult> searchIndex(SearchPattern sp);

 @NeedsRight("indexSearch")
 List<SearchIndex> getSearchstrings(String str);

}
