package application.workers;

import application.aspects.annotations.NeedsRight;
import application.aspects.annotations.SecurityControl;
import baked.application.beans.Record;
import baked.application.beans.ReleaseYear;
import baked.application.beans.SettingsOrderBy;
import baked.application.beans.User;
import core.aspects.annotations.Transaction;
import core.bakedBeans.DataStore;
import core.base.WorkerManager;

import java.util.List;



public interface RecordsWorker extends WorkerManager.IWorker {

 @NeedsRight("addrecords")
 void addRecord(Record rec, String defaultNameNewFavouritesList);

 @SecurityControl
 @NeedsRight("editrecords")
 void save(Record rec);

 @NeedsRight("addrecords")
 List<Record> selectInterpreters(String interpreter);

 @NeedsRight("addrecords")
 List<Record> selectTitles(String title);

 Record getTemplateRecord();

 @NeedsRight("addrecords")
 @Transaction()
 void delete(Record rec);

 @NeedsRight("sort")
 public SettingsOrderBy getOrderBy(Record rec, User user);

 @NeedsRight("sort")
 SettingsOrderBy orderBy(Record rec, User user, String property);

 DataStore<ReleaseYear> releaseYears();

 @NeedsRight("sortReleaseYears")
 void orderByReleaseYears(String property);

 @NeedsRight("sortReleaseYears")
 SettingsOrderBy getOrderByReleaseYears();

 DataStore<Record> releaseYearRecords(String year);

}
