package application.workers;

import baked.application.beans.Record;
import baked.application.beans.*;
import core.bakedBeans.*;
import core.util.Configurator;
import core.util.ObjectManager;
import core.util.Property;
import core.util.Utilities;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.BiFunction;

import static core.bakedBeans.Operator.AND;
import static core.bakedBeans.Operator.EQUAL;



/**
 *  Helper for the SearchEngine.
 *
 * @version 2024-08-09
 * @author lp
 */

@Configurator.Context(SearchEngineImpl.class)
public class SearchEngineHelper implements ObjectManager.IObject {

 @Property("SearchEngine.IndexPropsMusicians")
 private static List<String> indexPropsMusicians;
 @Property("SearchEngine.IndexPropsRecords")
 private static List<String> indexPropsRecords;
 @Property("SearchEngine.IndexPropsTitles")
 private static List<String> indexPropsTitles;
 @Property("SearchEngine.IndexPropsMusicianalias")
 private static List<String> indexPropsMusicianalias;
 @Property("SearchEngine.IndexPropsInstruments")
 private static List<String> indexPropsInstruments;
 @Property("SearchEngine.IndexPropsExcludes")
 private static List<String> IndexPropsExcludes;
 @Property("SearchEngine.IndexMinLength")
 private static int indexMinLength;

 private static Logger logger;

 private static String[] names_meier = {
  "meier",
  "maier",
  "meyer",
  "mayer",
  "meir",
  "mair",
  "meyr",
  "mayr",
  "myer"
 };



 void createIndex(Musician mus) {

  createIndex(mus, indexPropsMusicians, "musicianId");

 }



 void createIndex(Record rec) {

  createIndex(rec, indexPropsRecords, "recordId");

 }



 void createIndex(Title tit) {

  createIndex(tit, indexPropsTitles, "titleId");

 }



 void createIndex(Musicianalias musicianalias) {

  createIndex(musicianalias, indexPropsMusicianalias, "musicianaliasId");

 }



 void createIndex(Instrument instrument) {

  createIndex(instrument, indexPropsInstruments, "instrumentId");

 }



 void createIndex(PersistentBean b, final List<String> props, String idProp) {

  props.forEach(prop -> {
   String propertyValue = (String)b.get(prop);
   if (!Utilities.isEmpty(propertyValue)) {
    Utilities.splitSearchString(propertyValue).stream()
     .distinct()
     .filter(st -> st.length() >= indexMinLength)
     .filter(st -> !IndexPropsExcludes.contains(st))
     .map(splval -> new SearchIndexToken(splval, null, propertyValue))
     .flatMap(sit ->
      modify(sit,
       SearchEngineHelper::handleScotts,
       SearchEngineHelper::handleMeiers,
       SearchEngineHelper::convertScandinavianUmlauts,
       SearchEngineHelper::flattenUmlauts,
       SearchEngineHelper::convertInnerAccent,
       SearchEngineHelper::removeAccents
      ).stream()
     )
     .map(sit -> createSearchIndex(b, idProp, prop, sit))
     .forEach(PersistentBean::save);
   }
  });

  DataStoreManager.get()
   .signal(DataStore.DataStoreSignal.write, SearchEngineImpl.DATASTORE_CATEGORY_INDEXSEARCH);

 }



 private static Void flattenUmlauts(List<SearchIndexToken> all, SearchIndexToken actual) {

  String sd =
   actual.value.replaceAll("ä", "ae")
    .replaceAll("ö", "oe")
    .replaceAll("ü", "ue")
    .replaceAll("ß", "ss");

  if (!actual.value.equals(sd))
   all.add(
    new SearchIndexToken(
     actual,
     sd,
     "flUml"
    )
   );

  return null;

 }



 private static Void convertScandinavianUmlauts(List<SearchIndexToken> all, SearchIndexToken actual) {

  String sd =
   actual.value.replaceAll("ø", "ö")
    .replaceAll("æ", "ä")
    .replaceAll("œ", "ü");

  if (!actual.value.equals(sd)) {
   SearchIndexToken t = new SearchIndexToken(actual, sd, "cvScUml");

   all.add(t);

   flattenUmlauts(all, t);
  }

  return null;

 }



 private static Void convertInnerAccent(List<SearchIndexToken> all, SearchIndexToken actual) {

  String[] parts = actual.value.split("'");

  if (parts.length > 1) {
   SearchIndexToken t = new SearchIndexToken(actual, parts[0] + parts[1], "cInAc");
   all.add(t);

   t = new SearchIndexToken(actual, parts[1], "cIA");
   all.add(t);

  }

  return null;

 }



 private static Void removeAccents(List<SearchIndexToken> all, SearchIndexToken actual) {

  String sd = Utilities.removeAccents(actual.value);

  if (!actual.value.equals(sd))
   all.add(
    new SearchIndexToken(
     actual,
     sd,
     "remAc"
    )
   );

  return null;

 }



 private static Void handleScotts(List<SearchIndexToken> all, SearchIndexToken actual) {

  String sd = actual.value;

  if (sd.startsWith("mc"))
   all.add(
    new SearchIndexToken(
     actual,
     "mac" + sd.substring(2),
     "hSc"
    )
   );
  else if (sd.startsWith("mac"))
   all.add(
    new SearchIndexToken(
     actual,
     "mc" + sd.substring(3),
     "hSc"
    )
   );

  return null;

 }



 private static Void handleMeiers(List<SearchIndexToken> all, SearchIndexToken actual) {

  String sd = actual.value;

  Arrays.stream(names_meier)
   .filter(sd::contains)
   .forEach(nm -> {
    int id = sd.indexOf(nm);
    String before = sd.substring(0, id);
    String after = sd.substring(id + nm.length());

    Arrays.stream(names_meier)
     .filter(nm1 -> !nm1.equals(nm))
     .forEach(nm1 -> {
      all.add(
       new SearchIndexToken(
        actual,
        before + nm1 + after,
        "hMe"
       )
      );
     });
    }
   );

  return null;

 }



 @SafeVarargs
 private List<SearchIndexToken> modify(SearchIndexToken searchIndexToken,
  BiFunction<List<SearchIndexToken>, SearchIndexToken, Void> ... modifiers
 ) {

  List<SearchIndexToken> res = new ArrayList<>();

  res.add(searchIndexToken);  // The original.

  for (BiFunction<List<SearchIndexToken>, SearchIndexToken, Void> mod : modifiers)
   mod.apply(res, searchIndexToken);

  return res;

 }



 private static class SearchIndexToken {

  String value;
  String method;
  int position;
  int length;
  String propertyValue;

  SearchIndexToken(String value, String method, String propertyValue) {

   this.value = value;
   this.method = method;

   if (!Utilities.isEmpty(propertyValue)) {
    this.propertyValue = propertyValue;
    this.position = propertyValue.toLowerCase(Locale.ROOT).indexOf(value);
    this.length = value.length();
   }

  }

  SearchIndexToken(SearchIndexToken token, String value, String method) {

   this(
    value,
    method,
    null
   );

   this.position = token.position;
   this.length = token.length;

  }

 }



 private SearchIndex createSearchIndex(
  PersistentBean b, String idProp, String prop, SearchIndexToken token
 ) {

  SearchIndex si = BeanFactory.make(SearchIndex.class);

  si.setCreated(LocalDateTime.now());
  si.setBeanName(b.getClass().getSimpleName());
  si.setPropertyAlias(prop);
  si.setValue(token.value);
  si.set(idProp, b.getKeyValue());

  si.setMethod(token.method);
  si.setPosition(token.position);
  si.setLength(token.length);

  return si;

 }



 void reCreateIndex(final Musician mus) {

  logger.debug(() -> "reCreateIndex() for musician: " + mus.significant());

  deleteIndex(mus);
  createIndex(mus);

 }



 void reCreateIndex(final Record rec) {

  logger.debug(() -> "reCreateIndex() for record: " + rec.significant());

  deleteIndex(rec);
  createIndex(rec);

 }



 void reCreateIndex(final Title tit) {

  logger.debug(() -> "reCreateIndex() for title: " + tit.significant());

  deleteIndex(tit);
  createIndex(tit);

 }



 void reCreateIndex(final Musicianalias musicianalias) {

  logger.debug(() -> "reCreateIndex() for musicianalias: " + musicianalias.getId());

  deleteIndex(musicianalias);
  createIndex(musicianalias);

 }



 void reCreateIndex(final Instrument instrument) {

  logger.debug(() -> "reCreateIndex() for instrument: " + instrument.getId());

  deleteIndex(instrument);
  createIndex(instrument);

 }



 void deleteIndex(final Musician mus) {

  deleteIndex(mus, "musicianId");

 }



 void deleteIndex(final Record rec) {

  deleteIndex(rec, "recordId");

 }



 void deleteIndex(final Title tit) {

  deleteIndex(tit, "titleId");

 }



 void deleteIndex(final Musicianalias musicianalias) {

  deleteIndex(musicianalias, "musicianaliasId");

 }



 void deleteIndex(final Instrument instrument) {

  deleteIndex(instrument, "instrumentId");

 }



 private void deleteIndex(PersistentBean b, final String referenceProperty) {

  DataStoreManager.get().dataStore(JDBCDataStore.class, SearchEngineImpl.DATASTORE_CATEGORY_INDEXSEARCH)
   .resetable()
   .oneTransaction()
   .where(
    AND(
     EQUAL("beanName", b.getClass().getSimpleName()),
     EQUAL(referenceProperty, b.getKeyValue())
    )
   )
   .make(SearchIndex.class)
   .forEach(si -> {
    logger.debug(() -> "deleteIndex(): " + si.getId());
    si.delete();
   });

  DataStoreManager.get()
   .signal(DataStore.DataStoreSignal.delete, b, SearchEngineImpl.DATASTORE_CATEGORY_INDEXSEARCH);

 }

}
