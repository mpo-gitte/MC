package application.workers;



import baked.application.beans.User;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;

/**
 * Helper methods for UsersWorkerImpl & Co.
 *
 * @version 2023-09-05
 * @author pilgrim.lutz@imail.de
 */

class UsersHelper {



 /**
  * Adds the salt to the password.
  *
  * @param password The password
  * @param salt The salt
  *
  * @return The salted password. Currently just salt + password
  */

 static String saltPassword(String password, String salt) {

  return salt + password;

 }



 /**
  * Fetches a user with his login name.
  *
  * @param loginname The loginname
  *
  * @return The user
  */

 static User getUserForLoginname(String loginname) {

  DataStore<User> ds = DataStoreManager.get().makeDataStore("users.fromLoginname", new Object[] {loginname});

  return ds.readBean();

 }

}
