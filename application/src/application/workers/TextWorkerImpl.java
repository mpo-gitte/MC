package application.workers;

import core.base.Request;
import core.base.WorkerManager;
import core.util.LocaleConfig;
import core.util.MergedPropertyResourceBundle;
import core.util.Property;
import org.apache.logging.log4j.Logger;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;



/**
 * Worker for getting information about MC.
 *
 * @version 2023-05-09
 * @author pilgrim.lutz@imail.de
 */

public class TextWorkerImpl implements WorkerManager.IWorker {

 private static Logger logger;
 private HashMap<Locale, ResourceBundle> resourceBundles;

 @Property("TextWorker.prefix")
 private String filenamePrefix;
 @Property("TextWorker.defaultLocale")
 private Locale defaultLocale;



 /**
  * When a worker gets loaded the WorkerManager will call this method.
  *
  * <p>The default implementation is empty.</p>
  *
  * @see WorkerManager
  */

 @Override
 public void onLoad() {

  resourceBundles = new HashMap<>();

 }



 /**
  * gets a string from the underlying property resource bundle.
  *
  * @param locale The locale.
  * @param text The key of the text.
  * @return The text string.
  *
  */

 public String get(@LocaleConfig Locale locale, String text) {

  try {
   return getBundle(locale).getString(text);
  }
  catch (Exception e) {
   logger.error("get(): Text not found: \"" + text + "\"!");
  }

  return "MISSING: " + text;

 }



 /**
  * Gets a string from the underlying property resource bundle and passes it to Velocity for rendering.
  *
  * @param locale The locale.
  * @param text The key of the text.
  *
  * @return The text string.
  *
  */

 public String getParsed(@LocaleConfig Locale locale, String text) {

  StringWriter strw = new StringWriter();

  Request req = Request.get();

  req.getVelocityEngine().evaluate(req.getVelocityContext(), strw, "TextWorker.getParsed", get(locale, text));

  return strw.toString();

 }



 private synchronized ResourceBundle getBundle(Locale locale) {

  if (!resourceBundles.containsKey(locale)) {
   ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle(filenamePrefix, locale);
   resourceBundles.put(locale, rb);
  }

  if ((locale != defaultLocale)  &&  !resourceBundles.containsKey(locale))
   return getBundle(defaultLocale);

  return resourceBundles.get(locale);

 }

}
