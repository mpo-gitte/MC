package application.workers;

import application.beans.Rights;
import application.exceptions.LoginException;
import baked.application.beans.Login;
import baked.application.beans.Password;
import baked.application.beans.User;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;
import core.bakedBeans.JDBCDataStore;
import core.base.Request;
import core.util.Configurator;
import core.util.ObjectManager;

import java.time.LocalDateTime;
import java.util.List;

import static core.bakedBeans.Operator.EQUAL;


/**
 * Helper methods for working with logins.
 *
 * @version 2025-02-19
 * @author lp
 */

@Configurator.Context(LoginWorkerImpl.class)
public class LoginHelper implements ObjectManager.IObject {

 private static DataStoreManager dataStoreManager;


 /**
  * Returns the current password for the given user.
  *
  * @param user The user.
  *
  * @return Its current password
  */

 Password getCurrentPassword(User user) {

  return
   DataStoreManager.get().dataStore(JDBCDataStore.class, "users")
    .resetable()
    .where(
     EQUAL("userId", user.getId())
    )
    .orderBy("-created")
    .make(Password.class)
    .readBean();

 }



 /**
  * Check if password is expired.
  *
  * @param pw The password to check.
  *
  * @throws LoginException if password is expired.
  */

 void isPasswordExpired(Password pw) {

  if (pw.getExpirationCount() <= 0  ||  pw.getExpires().isBefore(LocalDateTime.now()))
   throw new LoginException("ExpiredPassword");

 }



 /**
  * Gets the current login from the request.
  *
  * @return The Login
  */

 protected Login getLogin() {

  return (Login)Request.get().getSessionBeanViaSessionId(Login.class);

 }



 /**
  * Ask if a user has the given right.
  *
  * <p>Rights are stored in a Rights bean in the user Role bean.</p>

  * @param what The name of the right.
  *
  * @return Tru if the user has the right. False otherwise.
  *
  * @see application.beans.Role
  * @see Rights
  */

 public boolean may(String what) {

  return getLogin().may(what);

 }



 /**
  * Returns the current user.
  *
  * @return The User.
  */

 public User getUser() {

  return
   getLogin().getRefUser();

 }


 /**
  * Deletes all logins for the given user.
  *
  * @param user The user.
  */

 void deleteLoginsForUser(User user) {

  getAllLoginsForUser(user).forEach(Login::delete);

  dataStoreManager.signal(DataStore.DataStoreSignal.delete);

 }



 /**
  * Gets all current logins.
  *
  * @return The logins.
  *
  */

 List<Login> getAllLogins() {

  return
   DataStoreManager.get().dataStore(JDBCDataStore.class, "logins")
    .resetable()
    .make(Login.class)
    .read();

 }



 /**
  * Gets all current logins for a specific user.
  *
  * @param user The user.
  *
  * @return The logins.
  *
  */

 List<Login> getAllLoginsForUser(User user) {

  return
   DataStoreManager.get().dataStore(JDBCDataStore.class, "logins")
    .resetable()
    .where(
     EQUAL("userId", user.getId())
    )
    .make(Login.class)
    .read();

 }

}
