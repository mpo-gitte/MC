package application.workers;


import baked.application.beans.Setting;
import baked.application.beans.SettingsOrderBy;
import baked.application.beans.User;
import core.bakedBeans.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;

import static core.bakedBeans.Operator.AND;
import static core.bakedBeans.Operator.EQUAL;



/**
 * Class with some utility methods for sorting.
 *
 * @version 2023-08-23
 * @author lp
 */

public class SortHelper {

 private static final Logger logger = LogManager.getLogger(SortHelper.class);



 /**
  * Gets the setting for sorting.
  *
  * @param user             For this user.
  * @param defaultProperty  If not set.
  * @param defaultDirection
  * @param key              The key for the setting.
  * @return The setting.
  */

 public static Setting getSortSetting(User user, String defaultProperty, SettingsOrderBy.direction defaultDirection, String key) {

  return getSortSetting(user, defaultProperty, defaultDirection, key, 0);

 }



  /**
   * Gets the setting for sorting.
   *
   * @param user             For this user.
   * @param defaultProperty  If not set.
   * @param defaultDirection Default direction order.
   * @param key              The key for the setting.
   * @param itemId           For this item.
   * @return The setting.
   */

  public static Setting getSortSetting(User user, String defaultProperty, SettingsOrderBy.direction defaultDirection, String key, int itemId) {

  // Fetch setting for sorting.
  Setting setting = DataStoreManager.get().dataStore(JDBCDataStore.class, "settings")
   .resetable()
   .where(
    AND(
     EQUAL("userId", user.getId()),
     EQUAL("key", key),
     EQUAL("itemId", itemId)
    )
   )
   .make(Setting.class)
   .readBean();

  if (core.bakedBeans.Utilities.isEmpty(setting)) {
   // No setting for sorting. Create a default.
   setting.setKey(key);
   setting.setItemId(itemId);
   SettingsOrderBy orderBy = BeanFactory.make(SettingsOrderBy.class);
   orderBy.setProperty(defaultProperty);
   orderBy.setEnumDirection(defaultDirection);
   setting.setRelOrderBy(orderBy);
   setting.setUserId(user.getId());
   setting.setCreated(LocalDateTime.now());
  }

  return setting;

 }



 /**
  * Switches sort order.
  *
  * @param setting The setting.
  * @param property Property name to check.

  * @return New sort order.
  */

 public static SettingsOrderBy switchOrder(Setting setting, String property) {

  SettingsOrderBy order = setting.getRelOrderBy();

  if (!property.equals(order.getProperty()))
   order.setEnumDirection(SettingsOrderBy.direction.ascending);
  else {
   switch (order.getEnumDirection()) {

    case none:
     order.setEnumDirection(SettingsOrderBy.direction.ascending);
     break;

    case ascending:
     order.setEnumDirection(SettingsOrderBy.direction.descending);
     break;

    case descending:
     order.setEnumDirection(SettingsOrderBy.direction.ascending);
    break;

   }
  }

  return order;

 }



 /**
  * Delete a sort setting.
  *
  * @param key For this key.
  * @param itemId And this item id.
  */

 public static void deleteSetting(String key, Object itemId) {

  DataStoreManager.get().dataStore(JDBCDataStore.class, "settings")
   .resetable()
   .oneTransaction()
   .where(
    AND(
     EQUAL("key", key),
     EQUAL("itemId", itemId)
    )
   )
   .make(Setting.class)
   .forEach(PersistentBean::delete);
 }



 /**
  * Gets the sort order for order by clause
  *
  * @param order settings.
  *
  * @return order.
  */

 static String getSortOrderForOrderBy(SettingsOrderBy order) {

  return
   order.getOrderForOrderByClause() + order.getProperty();

 }



 /**
  * Changes sort order.
  *
  * @param user             For user.
  * @param key              With settings key.
  * @param defaultProperty  With default.
  * @param defaultDirection With default direction.
  * @param property         For bean property.
  * @param category         Signal category.
  * @param recordId         The additional id of an object.
  */

 static void switchOrder(User user, String key,
                         String defaultProperty,
                         application.beans.SettingsOrderBy.direction defaultDirection,
                         String property, String category,
                         int recordId) {

  // Switch the sort order.
  Setting setting =
   getSortSetting(
    user,
    defaultProperty,
    defaultDirection,
    key,
    recordId
   );

  switchOrder(setting, property);
  setting.getRelOrderBy().setProperty(property);

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, category);

  // Save current sort order.
  setting.save();
  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, user, "settings");

 }

}
