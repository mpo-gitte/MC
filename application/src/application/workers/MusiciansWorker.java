package application.workers;

import application.aspects.annotations.NeedsRight;
import baked.application.beans.*;
import application.beans.Record;
import application.beans.RestrictionSource;
import application.beans.Title;
import core.aspects.annotations.CatchException;
import core.bakedBeans.BeanList;
import core.bakedBeans.DataStore;
import core.base.WorkerManager;

import java.util.List;



public interface MusiciansWorker extends WorkerManager.IWorker {

 @NeedsRight("editmusicians")
 @CatchException
 void save(Musician mus);

 @NeedsRight("editmusicians")
 void delete(Musician mus);

 List<Instrument> getInstruments(Musician mus, RestrictionSource res);

 DataStore<Instrument> getInstrumentsDS(Musician mus, RestrictionSource res);

 DataStore<Musician> getCoMusiciansDS(Musician mus, RestrictionSource res);

 List<Record> getRecordsWithCoMusician(Musician mus, Musician comus, RestrictionSource res);

 List<Title> getTitlesWithCoMusician(Musician mus, Musician comus, RestrictionSource res);

 List<Title> getTitlesWithInstrument(Musician mus, Instrument instr, RestrictionSource res);

 List<Record> getRecords(Musician mus, Instrument instr, RestrictionSource res);

 DataStore<Record> getRecordsDS(Musician mus, Instrument instr, RestrictionSource res);

 List<Musician> getBirthdayCalendar();

 @NeedsRight("seeBirthdaysCurrent")
 BeanList<BirthdayCalendarEvent> getBirthdayCalendarFromNow();

 @NeedsRight("editmusicians")
 void saveMusicianalias(Musician mus, Musicianalias musicianalias);

 @NeedsRight("editmusicians")
 void deleteMusicianalias(BeanList<Musicianalias> musicianaliases);

}
