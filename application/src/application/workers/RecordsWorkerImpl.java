package application.workers;



import application.beans.LogbookEntry;
import application.configs.ConfigCurrentUser;
import baked.application.beans.Record;
import baked.application.beans.*;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;
import core.bakedBeans.JDBCDataStore;
import core.base.WorkerManager;
import core.util.WorkerHelper;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import static core.bakedBeans.Operator.*;



/**
 * Methods for Records.
 *
 * @version 2025-01-06
 * @author lp
 */

public class RecordsWorkerImpl implements WorkerManager.IWorker {

 private static Logger logger;
 private static DataStoreManager dataStoreManager;

 @WorkerHelper()
 private LoginHelper loginHelper;
 @WorkerHelper()
 private LogbookHelper logbookHelper;
 @WorkerHelper()
 private SearchEngineHelper searchEngineHelper;

 private static final HashMap<String, String[]> orders = new HashMap<>();

 static {
  orders.put("releaseyear", new String[] {"+releaseyear"});
  orders.put("+releaseyear", new String[] {"+releaseyear"});
  orders.put("+number", new String[] {"+number", "+releaseyear"});
  orders.put("-releaseyear", new String[] {"-releaseyear"});
  orders.put("-number", new String[] {"-number", "+releaseyear"});
 }

 private final String RELEASEYEAR_KEY = "releaseyears.orderBy";


 /**
  * Adds a  record and writes logentry and notifies DataStores.
  *
  * @param rec The record
  */

 public void addRecord(Record rec, String defaultNameNewFavouritesList) {

  rec.setId(null);  // Force to save as a new Record!
  rec.setCreated(LocalDateTime.now());

  rec.save();

  CatalogHelper.createCatalogRequest(rec);
  searchEngineHelper.reCreateIndex(rec);

  FavouritesListsHelper.addRecordToNewFavouritesList(
   loginHelper.getLogin().getRefUser(),
   rec,
   defaultNameNewFavouritesList
  );

  writeLogbook(rec, LogbookEntry.verbs.write);

  dataStoreManager.signal(
   DataStore.DataStoreSignal.write,
   rec,
   "records", "statisticsCount", "seeAlso"
  );

 }



 /**
  * Saves a record and writes logentry and notifies DataStores.
  * @param rec The record
  */

 public void save(Record rec) {

  rec.save();

  CatalogHelper.createCatalogRequest(rec);
  searchEngineHelper.reCreateIndex(rec);

  writeLogbook(rec, LogbookEntry.verbs.write);

  dataStoreManager.signal(DataStore.DataStoreSignal.write, rec, "records", "favourites");

 }



 private void writeLogbook(Record rec, LogbookEntry.verbs verb) {

  logbookHelper.writeLogbook(rec, null, null, null, verb);

 }



 /**
  * Gets a list of Records where interpreter is the given interpreter.
  *
  * @param interpreter A name of an interpreter. Case-insensitive. Used as a prefix (A '%' will be added).
  *
  * @return A list of Records.
  *
  * @see application.beans.Title
  */

 public List<Record> selectInterpreters(String interpreter) {

  logger.debug(() -> "selectInterpreters(): interpreter= \"" + interpreter + "\"");

  DataStore<Record> ds =
   dataStoreManager.makeDataStore(
    "recordsWorker.selectInterpreters",
    new Object[] {
     interpreter + "%"
    }
   );

  return ds.read();

 }



 /**
  * Gets a list of Records where title is the given title.
  *
  * @param title A titles title. Case insensitive. Used as a prefix (A '%' will be added).
  *
  * @return A list of Records.
  *
  * @see application.beans.Title
  */

 public List<Record> selectTitles(String title) {

  logger.debug(() -> "selectTitles(() -> ): title= \"" + title + "\"");

  DataStore<Record> ds =
   dataStoreManager.makeDataStore(
    "recordsWorker.selectTitles",
    new Object[] {
     title + "%"
    }
   );

  return ds.read();

 }



 /**
  * Generates a Record-Bean containing the data of the latest Record-Bean.
  *
  * @return A record
  */

 public Record getTemplateRecord() {

  DataStore<Record> ds =
   dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore",
    "records",
    "select * from records where id = (select max(id) from records)",
    new Object[] {
    },
    Record.class);
  List<Record> recs = ds.read();

  Record rec;
  if (recs.size() == 0) {
   rec = new Record();
   rec.setMediumId("CD");
   rec.setRecording("AAD");
  }
  else {
   rec = recs.get(0);
   rec.setId(null);
  }

  return rec;
 }



 /**
  * Deletes the given record.
  *
  * <p>Removes it from all favourites list also.</p>
  *
  * @param rec The record to delete.
  */

 public void delete(Record rec) {

  // Remove the record from all favourites lists. Is not within a transaction with delete!

  FavouritesListsHelper.removeRecordFromAllFavouritesLists(rec);

  // Log action.
  writeLogbook(rec, LogbookEntry.verbs.delete);

  // Remove record from catalog.
  CatalogHelper.removeFromCatalog(rec);

  // Correct references in Logbook.
  logbookHelper.correctLogbook(rec);

  // Delete index.
  searchEngineHelper.deleteIndex(rec);

  // Delete settings
  SortHelper.deleteSetting(Record.SETTINGS_SORTKEY, rec.getKeyValue());

  // Delete the record.
  rec.delete();

  // Tell the DataStores about deletion.
  dataStoreManager.signal(
   DataStore.DataStoreSignal.delete,
   "records", "favouritesLists", "favourites", "setting"
  );

 }



 /**
  * Gets the current order settings for the given Title.
  *
  * @param rec The Title with the Job-list.
  * @param user The user with the setting.
  *
  * @return The settings.
  */

 public SettingsOrderBy getOrderBy(Record rec, User user) {

  Setting setting =
   SortHelper.getSortSetting(
    user,
   "releaseyear",
    SettingsOrderBy.direction.none,
    Record.SETTINGS_SORTKEY,
    rec.getId());

  return setting.getRelOrderBy();

 }



 /**
  * Set sort settings.
  *
  * @param rec For Record.
  * @param user And user.
  * @param property And property.
  *
  * @return The new sort settings.
  */

 public SettingsOrderBy orderBy(Record rec, User user, String property) {

  if (logger.isDebugEnabled())
   logger.debug(("orderBy(): sorting list for user: " + user.getId()));

  // Switch the sort order.
  Setting setting =
   SortHelper.getSortSetting(user, "interpreter", SettingsOrderBy.direction.none, Record.SETTINGS_SORTKEY, rec.getId());
  SortHelper.switchOrder(setting, property);
  setting.getRelOrderBy().setProperty(property);

  // The "real" sorting is done in the getter.

  // Save current sort order.
  setting.save();
  dataStoreManager.signal(DataStore.DataStoreSignal.write, user, "settings");

  return setting.getRelOrderBy();

 }



 /**
  * Returns all release years and the number of records released in this year
  *
  * @return The list of release years
  */

 public DataStore<ReleaseYear> releaseYears(@ConfigCurrentUser User user) {

  Setting setting = SortHelper.getSortSetting(
   user,
  "releaseyear",
   SettingsOrderBy.direction.ascending,
   RELEASEYEAR_KEY
  );
  SettingsOrderBy order = setting.getRelOrderBy();

  return
   DataStoreManager.get().dataStore(JDBCDataStore.class, "records")
    .resetable()
    .groupBy("releaseyear")
    .having(
      AND(
       GIVEN("releaseyear"),
       NOTEQUAL("releaseyear", "")
      )
    )
    .orderBy(orders.get(order.getOrderForOrderByClause() + order.getProperty()))
    .make(ReleaseYear.class);

 }



 /**
  * Changes the sort order for the release years.
  *
  * @param property the property to sort.
  */

 public void orderByReleaseYears(@ConfigCurrentUser User user, String property) {

  if (logger.isDebugEnabled())
   logger.debug(("orderByReleaseYears(): sorting list for user: " + user.getId()));

  SortHelper.switchOrder(
   user,
   RELEASEYEAR_KEY,
   property,
   SettingsOrderBy.direction.ascending,
   property,
   "records",
   0
  );

 }



 /**
  * Gets the current order settings.
  *
  * @param user For this user.
  *
  * @return The settings.
  */

 public SettingsOrderBy getOrderByReleaseYears(@ConfigCurrentUser User user) {

  Setting setting =
   SortHelper.getSortSetting(
    user,
   "releaseyear",
    SettingsOrderBy.direction.none,
    RELEASEYEAR_KEY
    );

  return
   setting.getRelOrderBy();

 }


 public DataStore<Record> releaseYearRecords(String year) {

  return
   DataStoreManager.get().dataStore(JDBCDataStore.class, "records")
    .resetable()
    .where(
      EQUAL("releaseyear", year)
    )
    .orderBy("interpreter", "title")
    .make(Record.class);

 }

}
