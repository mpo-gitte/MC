package application.workers;


import baked.application.beans.Record;
import baked.application.beans.*;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStore.DataStoreSignal;
import core.bakedBeans.DataStoreManager;
import core.bakedBeans.JDBCDataStore;
import core.bakedBeans.Utilities;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static core.bakedBeans.Operator.*;



/**
 * This worker provides methods for managing favourites lists.
 *
 * @version 2024-05-10
 * @author lp
 *
 */

public class FavouritesListsWorkerImpl implements FavouritesListsWorker {

 private static Logger logger;
 private static DataStoreManager dataStoreManager;
 private static final String FAVOURITESLISTS_KEY = "favouritesLists.orderBy";



 /**
  *
  * Returns a List of favourites lists for a given user.
  *
  * @param user The user.
  *
  * @return A DataStore of favourites lists for the given user.
  *
  */

 @Override
 public DataStore<FavouritesList> getFavouritesLists(User user) {

  if (logger.isDebugEnabled())
   logger.debug(("getFavouritesLists(): getting lists for user: " + user.getId()));

  return
   dataStoreManager.dataStore(JDBCDataStore.class, "favouritesLists")
    .resetable(FavouritesListsHelper::concerned)
    .taggedWith(user)
    .where(core.bakedBeans.Utilities.isEmpty(user) ? null : EQUAL("userId", user.getId()))
    .orderBy("orderNr")
    .make(FavouritesList.class);

 }



 /**
  *
  * Returns a List of favourites lists for favourites menu for a given user.
  *
  * @param user The user.
  *
  * @return A list of favourites lists for the given user.
  *
  */

 @Override
 public DataStore<FavouritesList> getFavouritesListsForMenu(User user) {

  if (logger.isDebugEnabled())
   logger.debug(("getFavouritesListsForMenu(): getting lists for user: " + user.getId()));

  return
   dataStoreManager.dataStore(JDBCDataStore.class, "favouritesLists")
    .resetable(FavouritesListsHelper::concerned)
    .taggedWith(user)
    .where(
      AND(
       EQUAL("userId", user.getId()),
       ISTRUE("userAccessWrite")
      )
    )
    .orderBy("name")
    .make(FavouritesList.class);

 }



 /**
  *
  * Deletes the given favourites lists.
  * <p>Signal DataStores of category "favourites".</p>
  *
  * @param fledel A list with favourites lists to delete.
  *
  */

 @Override
 public void delete(List<FavouritesList> fledel) {

  for (FavouritesList fle : fledel)
   if (fle != null  && (fle.getColFavourites()).count() == 0) {
    fle.delete();

    SortHelper.deleteSetting(FavouritesListsHelper.FAVOURITESLIST_KEY, fle.getKeyValue());

   }

  dataStoreManager.signal(DataStoreSignal.delete, "favouritesLists");

 }



 /**
  *
  * Creates a new favourites list for the given user with the given name.
  * <p>Signal DataStores of category "favourites".</p>
  *
  * @param user The user.
  * @param name The Name.
  *
  */

 @Override
 public void createNew(User user, String name) {

  FavouritesListsHelper.createNewFavouritesList(user, name);

  resort(user);

  dataStoreManager.signal(DataStoreSignal.write, "favouritesLists");

 }



 private void resort(User user) {

  Setting setting = SortHelper.getSortSetting(
   user,
  "name",
   SettingsOrderBy.direction.none,
   FAVOURITESLISTS_KEY
  );
  SettingsOrderBy order = setting.getRelOrderBy();
  if (order.getEnumDirection() != SettingsOrderBy.direction.none)
   sort(user, order);

 }



 /**
  * Adds a Record to the specified favourites list.
  *
  * <p>If the FavouritesList doesn't exist it will be created.</p>
  *
  * <p>Signals DataStores of category "favourites" and "favouritesLists".</p>
  *
  * @param fl The FavouritesList
  * @param rec The Record
  *
  * @return The (eventually created) FavouritesList.
  */

 @Override
 public FavouritesList add(User user, FavouritesList fl, Record rec) {

  Integer flid = fl.getId();
  if (flid == null)
   fl = FavouritesListsHelper.createNewFavouritesList(user, fl.getName());

  FavouritesListsHelper.putRecordToFavouritesList(rec, fl);

  dataStoreManager.signal(DataStoreSignal.write, fl, "favourites");
  dataStoreManager.signal(DataStoreSignal.write, user, "favouritesLists");

  return fl;

 }



 /**
  *
  * Deletes the given favourites
  * <p>Signals DataStores of category "favourites".</p>
  *
  * @param user The owner of the favourites.
  * @param fledel A list with favourites to delete.
  *
  */

 @Override
 public void deleteFavourites(User user, List<Favourites> fledel) {

  for (Favourites fle : fledel)
   if (fle != null)
    fle.delete();

  dataStoreManager.signal(DataStoreSignal.delete, "favourites");
  dataStoreManager.signal(DataStoreSignal.write, user, "favouritesLists");

 }



 /**
  *
  * Saves the given favourites
  * <p>Signals DataStores of category "favourites".</p>
  *
  * @param fles A list with favourites to save.
  *
  */

 @Override
 public void saveFavourites(User user, FavouritesList fl, List<Favourites> fles) {


  // Save the favouritesList itself...

  if (fl.isDirty()) {
   fl.save();

   dataStoreManager.signal(DataStoreSignal.write, user, "favouritesLists");
  }


  // ... and the entries separately.

  if (!Utilities.isEmptyList(fles))
   for (Favourites fle : fles)
    if (fle != null)
     fle.save();

  // "re-sort".
  resort(user);

  // Tell it!

  dataStoreManager.signal(DataStoreSignal.write, "favourites");

 }



 /**
  * Returns the favourites list for the newly created records.
  *
  * @param user The user.
  * @return the favourites list for the given user with the new records.
  */

 @Override
 public FavouritesList getNewRecordsFavouritesList(User user) {

  FavouritesList fl = FavouritesListsHelper.getNewRecordsFavouritesList(user);

  if (Utilities.isEmpty(fl))
   return null;

  return fl;

 }



 /**
  * Changes the sort order.
  *
  * @param user The user owning the favourites lists.
  * @param property the property to sort.
  */

 public void orderBy(User user, String property) {

  if (logger.isDebugEnabled())
   logger.debug(("orderBy(): sorting lists for user: " + user.getId()));

  // Switch the sort order.
  Setting setting = SortHelper.getSortSetting(
   user,
  "name",
   SettingsOrderBy.direction.none,
   FAVOURITESLISTS_KEY
  );
  SettingsOrderBy order = SortHelper.switchOrder(setting, "name");

  sort(user, order);

  dataStoreManager.signal(DataStoreSignal.write, user, "favouritesLists");

  // Save current sort order.
  setting.save();
  dataStoreManager.signal(DataStoreSignal.write, user, "settings");

 }



 /**
  * Changes the sort order for the given FavouritesList.
  *
  * @param property the property to sort.
  */

 @Override
 public void orderByFavouritesList(FavouritesList fl, String property) {

  User user = fl.getSubUser();

  if (logger.isDebugEnabled())
   logger.debug(("orderByFavouritesList(): sorting list for user: " + user.getId()));

  // Switch the sort order.
  Setting setting =
   SortHelper.getSortSetting(user, "interpreter", SettingsOrderBy.direction.none, FavouritesListsHelper.FAVOURITESLIST_KEY, fl.getId());
  SettingsOrderBy order = SortHelper.switchOrder(setting, property);
  setting.getRelOrderBy().setProperty(property);

  FavouritesListsHelper.sortFavourites(fl, order);

  dataStoreManager.signal(DataStore.DataStoreSignal.write, fl, "favourites");

  // Save current sort order.
  setting.save();
  dataStoreManager.signal(DataStore.DataStoreSignal.write, user, "settings");

 }



 private void sort(User user, SettingsOrderBy order) {

  // Sort.

  AtomicInteger orderNr = new AtomicInteger(0);

  dataStoreManager.dataStore(JDBCDataStore.class, "favouritesLists")
   .resetable()
   .oneTransaction()
   .where(EQUAL("userId", user.getId()))
   .orderBy(order.getOrderForOrderByClause() + order.getProperty())
   .make(FavouritesList.class)
   .forEach(fle -> {
    try {
     fle.setOrderNr(orderNr.incrementAndGet());
     fle.save();
    } catch (Exception e) {
     logger.error("orderBy(); Cannot set order!");
    }
   });

 }



 /**
  * Gets the current order settings.
  *
  * @param user For this user.
  *
  * @return The settings.
  */

 public SettingsOrderBy getOrderBy(User user) {

  Setting setting =
   SortHelper.getSortSetting(
    user,
   "name",
    SettingsOrderBy.direction.none,
    FAVOURITESLISTS_KEY
   );

  return
   setting.getRelOrderBy();

 }



 @Override
 public void orderDrag(FavouritesList fle, FavouritesList flet) {

  if (logger.isDebugEnabled())
   logger.debug("orderDrag(): inserting " + fle.getId() + " before " + flet.getId());

  User user = fle.getSubUser();

  AtomicInteger insertOrderNumber = new AtomicInteger(-1);

  dataStoreManager.dataStore(JDBCDataStore.class, "favouritesLists")
   .resetable()
   .oneTransaction()
   .where(EQUAL("userId", user.getId()))
   .orderBy("orderNr")
   .make(FavouritesList.class)
   .forEach(fl -> {
     if (insertOrderNumber.get() == -1) {
      if (fl.getId().equals(flet.getId())) {
       // Found insertion position.
       insertOrderNumber.set(fl.getOrderNr());
      }
     }
     if (insertOrderNumber.get() >= 0) {
      // Increment orderNr for all following FavouritesLists.
      int id = fl.getOrderNr() + 1;
      fl.setOrderNr(id);
      fl.save();
     }
    });

  if (insertOrderNumber.get() >= 0) {
   // Insert the dropped FavouritesList on insertion position.
   fle.setOrderNr(insertOrderNumber.get());
   fle.save();

   // Set order direction to "none" to prevent display.
   Setting setting =
    SortHelper.getSortSetting(
     user,
    "name",
     SettingsOrderBy.direction.none,
     FAVOURITESLISTS_KEY
    );
   SettingsOrderBy orderBy = setting.getRelOrderBy();
   orderBy.setEnumDirection(application.beans.SettingsOrderBy.direction.none);
   setting.save();

   dataStoreManager.signal(DataStoreSignal.write, user, "favouritesLists", "settings");
  }

 }



 /**
  * Gets the current order settings for the given FavouritesList.
  *
  * @param fl The FavouritesList.
  *
  * @return The settings.
  */

 @Override
 public SettingsOrderBy getOrderByFavouritesList(FavouritesList fl) {

  User user = fl.getSubUser();

  Setting setting =
   SortHelper.getSortSetting(user, "interpreter", SettingsOrderBy.direction.none, FavouritesListsHelper.FAVOURITESLIST_KEY, fl.getId());

  return setting.getRelOrderBy();

 }



 @Override
 public void orderDragFavourites(FavouritesList fl, Record rec, Record rect) {

  if (logger.isDebugEnabled())
   logger.debug("orderDragFavourites(): inserting " + rec.getId() + " before " + rect.getId());

  User user = fl.getSubUser();

  AtomicInteger insertOrderNumber = new AtomicInteger(-1);
  AtomicInteger insertBefore = new AtomicInteger(0);

  dataStoreManager.dataStore(JDBCDataStore.class, "favourites")
   .resetable()
   .oneTransaction()
   .where(EQUAL("favouriteslistId", fl.getId()))
   .orderBy("orderNr")
   .make(Favourites.class)
   .forEach(f -> {
    if (insertOrderNumber.get() == -1) {
     if (f.getRecordsId().equals(rect.getId())) {
      // Found insertion position.
      insertOrderNumber.set(f.getOrderNr());
      insertBefore.set((Integer)f.getKeyValue());
     }
    }
    if (insertOrderNumber.get() >= 0) {
     // Increment orderNr for all following FavouritesLists.
     int id = f.getOrderNr() + 1;
     f.setOrderNr(id);
     f.save();
    }
   });

  if (insertOrderNumber.get() >= 0) {
   // Insert the dropped Favourites on insertion position.
   Favourites fi = DataStoreManager.get().dataStore(JDBCDataStore.class, "favourites")
    .resetable()
    .where(
     AND(
      EQUAL("recordsId", rec.getId()),
      EQUAL("favouriteslistId", fl.getId())
      )
    )
    .make(Favourites.class)
    .readBean();

   fi.setOrderNr(insertOrderNumber.get());
   fi.save();

   // Set order direction to "none" to prevent display.
   Setting setting =
    SortHelper.getSortSetting(user, "name", SettingsOrderBy.direction.none, FavouritesListsHelper.FAVOURITESLIST_KEY, fl.getId());
   SettingsOrderBy orderBy = setting.getRelOrderBy();
   orderBy.setEnumDirection(application.beans.SettingsOrderBy.direction.none);
   setting.save();

   dataStoreManager.signal(DataStoreSignal.write, fl, "favourites");
   dataStoreManager.signal(DataStoreSignal.write, user, "settings");
  }

 }

}
