package application.workers;

import baked.application.beans.CatalogRecord;
import baked.application.beans.CatalogRequest;
import baked.application.beans.Record;
import baked.application.beans.Title;
import core.bakedBeans.*;
import core.util.Utilities;

import java.time.LocalDateTime;
import java.util.List;

import static core.bakedBeans.Operator.EQUAL;


/**
 * Helper methods for the catalog.
 *
 * @version 2025-01-06
 * @author lp
 */

class CatalogHelper {


 /**
  * Creates a catalog request.
  *
  * @param rec Catalog request will be created for this record if interpreter changes.
  */

 protected static void createCatalogRequest(Record rec) {

  Object oldInterpeter = rec.oldValues().get("interpreter");

  if (!rec.getInterpreter().equals(oldInterpeter))
   createCatalogRequestForce(rec);

 }



 /**
  * Creates a catalog request.
  *
  * @param rec Catalog request will be created for this record.
  * @param tit Catalog request will be created for the record if interpreter of title changes.
  */

 protected static void createCatalogRequest(Record rec, Title tit) {

  Object oldInterpeter = tit.oldValues().get("interpreter");

  if (!tit.getInterpreter().equals(oldInterpeter))
   createCatalogRequestForce(rec);

 }



 /**
  * Creates a catalog request for the given record without any condition.
  *
  * @param rec The record.
  */

 public static void createCatalogRequestForce(Record rec) {

  CatalogRequest catreq = BeanFactory.make(CatalogRequest.class);

  catreq.setRecordId(rec.getId());

  // Try to find a request for the record to keep the requests unique.
  @SuppressWarnings("unchecked")
  List<CatalogRequest> res = (List<CatalogRequest>) catreq.find("recordId");

  if (Utilities.isEmptyList(res))
   catreq.setId(null);
  else
   catreq.setId(res.get(0).getId());

  // Always refresh date.
  catreq.setCreated(LocalDateTime.now());

  catreq.save();

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, "catalogRequests");

 }



 /**
  * Remove a record from the catalog.
  *
  * @param rec The record
  */

 static void removeFromCatalog(Record rec) {

  DataStoreManager.get().dataStore(JDBCDataStore.class, "catalogRequests")
   .resetable()
   .oneTransaction()
   .where(EQUAL("recordId", rec.getId()))
   .make(CatalogRecord.class)
   .forEach(PersistentBean::delete);

 }

}
