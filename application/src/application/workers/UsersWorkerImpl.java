package application.workers;

import application.configs.ConfigCurrentUser;
import application.exceptions.PasswordException;
import baked.application.beans.*;
import core.bakedBeans.*;
import core.base.WorkerManager;
import core.util.DBConnectionStore;
import core.util.Property;
import core.util.SecretService;
import core.util.WorkerHelper;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;

import static core.bakedBeans.Operator.*;


/**
 * This worker contains methods for user management.
 *
 * @version 2025-02-08
 * @author lp
 *
 */

public class UsersWorkerImpl implements WorkerManager.IWorker {

 private static Logger logger;
 @Property("UsersWorker")
 private static ResourceBundle rb;
 @Property("UsersWorker.PasswordHistory.length")
 private static int historyLength;
 @Property("UsersWorker.GeneratedPasswordLength")
 private static int generatedPasswordLength;
 @Property("UsersWorker.DefaultExpirationDate")
 private static String defaultExpirationDate;

 private static DataStoreManager dataStoreManager;

 @WorkerHelper()
 private LoginHelper loginHelper;
 @WorkerHelper()
 private LogbookHelper logbookHelper;



 /**
  * Sets the password for the given user.
  *
  * @param user The user.
  * @param cpw Current password
  * @param npw New password
  * @param npwr Repeated new password
  */

 public void setPassword(@ConfigCurrentUser User user, String cpw, String npw, String npwr) {

  logger.debug(() -> "setPassword(): \"" + user.getLoginname() + "\"");

  // Get users current password.
  Password pw = loginHelper.getCurrentPassword(user);

  // Password expired?
  loginHelper.isPasswordExpired(pw);

  // Check password.
  SecretService.IAgent bond = SecretService.get().getAgent(pw.getSecretPurpose());

  if (!bond.check(UsersHelper.saltPassword(cpw, pw.getSalt()), pw.getPassword()))
   throw new PasswordException("PasswordWrong");

  // New password repeated correctly?
  if (!npw.equals(npwr))
   throw new PasswordException("PasswordsDontMatch");

  checkPassword(user, npw);

  // New password.
  createPassword(npw, user)
   .save();

  dataStoreManager.signal(DataStore.DataStoreSignal.write, user, "users");

  // Shorten history.
  shortenHistory(user);

  // Write logbook.
  writeLogbook(user, application.beans.LogbookEntry.verbs.setPassword);

 }



 private void checkPassword(User user, String password) {

  checkPasswordHistory(user, password);
  checkPasswordRules(password);

 }



 private void checkPasswordRules(String password) {

  Enumeration<String> props = rb.getKeys();

  while (props.hasMoreElements()) {

   String prop = props.nextElement();

   if (prop.startsWith(("Rule."))) {
    String rule = rb.getString(prop);
    if (!password.matches(rule)) {
     logger.error(() -> "Password does not match: \"" + prop + "\"");
     throw new PasswordException("PasswordRules");
    }
   }

  }

 }



 private void checkPasswordHistory(User user, String password) {

  DataStoreManager.get().dataStore(JDBCDataStore.class, "users")
   .resetable()
   .where(
    EQUAL("userId", user.getId())
   )
   .make(Password.class)
   .forEach(pw -> {
     SecretService.IAgent bond = SecretService.get().getAgent(pw.getSecretPurpose());
     if (bond.check(UsersHelper.saltPassword(password, pw.getSalt()), pw.getPassword()))
      throw new PasswordException("PasswordFormerlyUsed");
   });

 }



 private Password createPassword(String password, User u) {

  final String purpose = rb.getString("DefaultSecretPurpose");

  String salt = RandomStringUtils.secure().nextAlphanumeric(Integer.parseInt(rb.getString("DefaultSaltLength")));

  SecretService.IAgent ag = SecretService.get().getAgent(purpose);

  return
   BeanFactory.make(Password.class)
    .setCreated(LocalDateTime.now())
    .setUserId(u.getId())
    .setPassword(ag.encrypt(UsersHelper.saltPassword(password, salt)))
    .setSecretPurpose(purpose)
    .setSalt(salt)
    .setExpirationCount(Integer.MAX_VALUE)
    .setExpires(
      LocalDateTime.parse(
       defaultExpirationDate,
       DateTimeFormatter.ISO_DATE_TIME
      )
     );

 }



 private void writeLogbook(User user, LogbookEntry.verbs verb) {

  (new LogbookEntry())
   .setUserId(loginHelper.getLogin().getUserId())
   .setOuserId(user.getId())
   .setEnumVerb(verb)
   .setCreated(LocalDateTime.now())
   .saveReset();

 }



 private void shortenHistory(User user) {

  List<Password> passwords = DataStoreManager.get().dataStore(JDBCDataStore.class, "users")
   .resetable()
   .where(
    EQUAL("userId", user.getId())
   )
   .orderBy("-created")
   .make(Password.class)
   .read();

  if (passwords.size() >= historyLength) {  // Shorten password history
   if (logger.isDebugEnabled())
    logger.debug("shorten password history for user: " + user.getId());

   for (int id = historyLength; id < passwords.size(); id ++)
    passwords.get(id).delete();

   dataStoreManager.signal(DataStore.DataStoreSignal.delete, user, "users");
  }
 }



 /**
  * Gets all users.
  *
  * @return A DataStore with all users.
  */

 public DataStore<User> getAllUsers() {

  return
   DataStoreManager.get().dataStore(JDBCDataStore.class, "users")
    .resetable()
    .orderBy("loginname")
    .make(User.class);

 }



 /**
  * Saves a user.
  *
  * @param user The user.
  * @return The saved user.
  */

 public User saveUser(User user) {

  user.save();

  dataStoreManager.signal(DataStore.DataStoreSignal.write, user, "users");

  return user;

 }



 /**
  * Checks if a login name is already used.
  *
  * @param loginname the login name
  *
  * @return true if not user. False otherwise.
  */

 public boolean checkUniqueLoginname(String loginname) {

  logger.debug(() -> "checkUniqueLoginname(): " + loginname);

  Long nr = DataStoreManager.get().dataStore(JDBCDataStore.class, "users")
   .resetable()
   .loadWith("id")
   .where(
    EQUAL("loginname", loginname)
   )
   .make(User.class)
   .count();

  logger.debug(() -> "checkUniqueLoginname(): " + loginname + " " + (nr == 0));

  return
   nr == 0;

 }



 /**
  * Get all possible roles which can assigned to a user.
  *
  * @return A list of roles.
  */

 public List<Role> getRoles() {

  return
   DataStoreManager.get().dataStore(JDBCDataStore.class, "roles")
    .resetable()
    .loadWith("id", "name")
    .orderBy("name")
    .make(Role.class)
    .read();

 }



 /**
  * Returns an user.
  *
  * @return A nearly empty user.
  */

 public User getTemplateUser() {

  Role defRole =
   DataStoreManager.get().dataStore(JDBCDataStore.class, "roles")
    .resetable()
    .loadWith("id")
    .where(
     ISTRUE("default")
    )
    .make(Role.class)
    .readBean();

  return
   BeanFactory.make(User.class)
    .setRoleId(defRole.getId());

 }


 /**
  * Adds the given user to MC.
  *
  * @param user The user.
  *
  * @return The users' password.
  */


 public String addUser(User user) {

  // Set some default values..
  user
   .setCreated(LocalDateTime.now())
   .setLocked(Boolean.FALSE);

  user.save();

  String password = generatePassword(user);

  dataStoreManager.signal(DataStore.DataStoreSignal.write, user, "users");

  // Write logbook.
  writeLogbook(user, application.beans.LogbookEntry.verbs.userCreated);

  return
   password;

 }



 private String generatePassword(User user) {

  // Generate a password.
  String password = RandomStringUtils.secure().nextAlphanumeric(generatedPasswordLength);
  Password pw = createPassword(password, user);
  pw.save();

  return password;  // Unencrypted Password!

 }


 /**
  * Lock or unlock a user.
  *
  * @param user The user.
  *
  * @return The user.
  */

 public User lockUnlockUser(User user) {

  if (user.getLocked()) {
   user.setLocked(Boolean.FALSE);
   writeLogbook(user, application.beans.LogbookEntry.verbs.userUnlocked);
  }
  else
   lockUser(user);

  user.save();

  dataStoreManager.signal(DataStore.DataStoreSignal.write, user, "users");

  return
    user;

 }



 private void lockUser(User user) {

  user.setLocked(Boolean.TRUE);

  loginHelper.deleteLoginsForUser(user);

  writeLogbook(user, application.beans.LogbookEntry.verbs.userLocked);

 }



 public void deleteUsers(List<User> users) {

  // Lock user first. Doing it in an isolated transaction to ensure users are locked even removal fails.

  users.forEach(user -> {
   if (user != null) {
    if (!user.getLocked()) {
     DBConnectionStore.getConnection();
     lockUser(user);
     user.save();
     DBConnectionStore.releaseConnection();
    }
   }
  });

  users.forEach(user -> {
   if (user != null) {
    DBConnectionStore.getConnection();

    // Write logbook.
    writeLogbook(user, application.beans.LogbookEntry.verbs.delete);

    // Clean users tracks in system.
    cleanUp(user, "temporaryresults", TitleReference.class);
    cleanUp(user, "searchpatterns", SearchPattern.class);
    cleanUp(user, "blacksheep", Blacksheep.class);
    cleanUp(user, "settings", Ticket.class);
    cleanUp(user, "settings", Setting.class);
    cleanUp(user, "users", Password.class);

    // Delete all entries in logbook which don't have relations to master data. They aren't of interest.
    DataStoreManager.get().dataStore(JDBCDataStore.class, "logbook")
     .resetable()
     .where(
      AND(
       EQUAL("userId", user.getId()),
       NOTGIVEN("recordId"),
       NOTGIVEN("titleId"),
       NOTGIVEN("musicianId"),
       NOTGIVEN("omusicianId")
      )
     )
     .make(LogbookEntry.class)
     .forEach(PersistentBean::delete);

    // Correct remainng log entries.
    logbookHelper.correctLogbook(user);

    // Delete favourites.
    deleteFavourites(user);

    // Finally, delete user itself.
    user.delete();

    // Commit transaction.
    DBConnectionStore.releaseConnection();

    // Tell the world...
    dataStoreManager.signal(
     DataStore.DataStoreSignal.delete,
     "users", "settings", "logbook", "favourites", "favouritesLists"
    );

   }
  });

 }



 private static void deleteFavourites(User user) {

  dataStoreManager.dataStore(JDBCDataStore.class, "favouritesLists")
   .where(
    EQUAL("userId", user.getId())  // For all lists owned by the given user.
   )
   .make(FavouritesList.class)
   .forEach(fl -> {
     // Delete items on the list.
     dataStoreManager.dataStore(JDBCDataStore.class, "favourites")
      .where(
       EQUAL("favouriteslistId", fl.getId())
      )
      .make(Favourites.class)
      .forEach(PersistentBean::delete);

     // Delete the list.
     fl.delete();
    });

 }



 private static void cleanUp(User user, String category, Class<? extends PersistentBean> beanClass) {

  DataStoreManager.get().dataStore(JDBCDataStore.class, category)
   .resetable()
   .where(
    EQUAL("userId", user.getId())
   )
   .make(beanClass)
   .forEach(PersistentBean::delete);

 }


 /**
  * Creates and saves a new password for the given user.
  *
  * <p>Also deletes password history for the user.</p>
  *
  * @param user The user.
  *
  * @return The new password
  */

 public String newPassword(User user) {

  // Delete password history.
  cleanUp(user, "users", Password.class);

  // New password.
  String password = generatePassword(user);

  // Write a logbook entry.
  writeLogbook(user, application.beans.LogbookEntry.verbs.setPassword);

  // Tell...
  dataStoreManager.signal(
   DataStore.DataStoreSignal.write,
   user,
   "users"
  );

  return
   password;

 }

}
