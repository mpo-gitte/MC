package application.workers;



import application.aspects.annotations.NeedsRight;
import baked.application.beans.*;
import baked.application.beans.Record;
import core.bakedBeans.BeanList;
import core.bakedBeans.DataStore;
import core.base.WorkerManager;



public interface CatalogWorker extends WorkerManager.IWorker {

 @NeedsRight("seeCatalog")
 DataStore<CatalogItem> getCatalogItems(String filter);

 @NeedsRight("rebuildCatalog")
 Long getTotalCatalogRequests();

 @NeedsRight("rebuildCatalog")
 Long getCurrentCatalogRequests();

 @NeedsRight("rebuildCatalog")
 void rebuildCatalog();

 @NeedsRight("mappingCatalog")
 DataStore<CatalogMapping> getMappings();

 @NeedsRight("mappingCatalog")
 CatalogMapping saveCatalogMapping(CatalogMapping catmap);

 @NeedsRight("mappingCatalog")
 void deleteCatalogMappings(BeanList<CatalogMapping> catmappings);

 @NeedsRight("seestatisticstopissuers")
 DataStore<CatalogItem> getTopIssuersDS();

 @NeedsRight("seeTagCloud")
 DataStore<CatalogItem> getTagCloudDropsDS();

 DataStore<Record> getSeeAlso(Record rec);

 @NeedsRight("sort")
 public SettingsOrderBy getOrderBySeeAlso(Record rec);

 @NeedsRight("sort")
 void orderBySeeAlso(String property, Record rec);

}
