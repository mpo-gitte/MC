package application.workers;



import core.base.WorkerManager;
import baked.application.beans.Record;



public interface Id3Worker extends WorkerManager.IWorker {

 public Record getId3Tags(String interpreter, String title);

}
