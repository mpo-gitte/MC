package application.workers;

import application.aspects.annotations.NeedsRight;
import baked.application.beans.LogbookEntry;
import baked.application.beans.User;
import core.bakedBeans.DataStore;
import core.bakedBeans.PersistentBean;
import core.base.WorkerManager;

import java.util.List;



public interface LogbookWorker extends WorkerManager.IWorker {

 /**
  *
  * Returns all verbs for logbook entries.
  *
  * @return All verbs.
  *
  */

 static LogbookEntry.verbs[] getAllVerbs() {

  return LogbookEntry.allVerbs();

 }

 @NeedsRight("seelogbook")
 DataStore<? extends PersistentBean> getLogbook(User user, String verb);

 @NeedsRight("deletelogbookentries")
 void delete(List<LogbookEntry> ledel);

 @NeedsRight("deletelogbookentries")
 void deleteMore(LogbookEntry ledel);

}
