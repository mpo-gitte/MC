package application.workers;

import baked.application.beans.Blacksheep;
import baked.application.beans.User;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;
import core.bakedBeans.JDBCDataStore;
import core.bakedBeans.Utilities;
import core.base.WorkerManager;
import core.util.DB;
import core.util.DBConnection;
import core.util.Property;
import core.util.Timer;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.util.List;

import static core.base.WorkerManager.Metric.MetricTypes.GAUGE;


/**
 * A worker for looking for the black sheep.
 *
 * @version 2024-07-08
 * @author lp
 */

public class ShepherdImpl implements Shepherd {

 private static Logger logger;
 private static DataStoreManager dataStoreManager;
 private static DB db;

 @Property("Shepherd.Watcher.schedule")
 private String watcherSchedule = "0 0 0/1 * * ?";



 /**
  * This method only starts a watcher.
  */

 @Override
 public void onLoad() {

  Timer.get().run("Shepherd.BlacksheepWatcher", watcherSchedule, true, () -> {

   logger.debug("BlacksheepWatcher: Checking Blacksheep.");

   try {
    DBConnection con = db.getConnection();

    // Delete old black sheep.

    PreparedStatement stmt = con.prepareStatement("delete from blacksheep where user_id is null and lastseen <= ?");
    stmt.setObject(1, LocalDateTime.now().minusDays(1));

    stmt.execute();

    db.releaseConnection(con);

    dataStoreManager.signal(DataStore.DataStoreSignal.delete, "blacksheep");

    logger.info("BlacksheepWatcher: Removed anonymous black sheep.");

   }
   catch (Exception ie) {
    logger.error("BlacksheepWatcher: Can't remove anonymous black sheep.");
   }

  });

 }



 /**
  * Get blacksheep.
  *
  * @param user if given return blacksheep for this user only.
  * @return a list of blacksheep or nothing if there were none.
  */

 public List<Blacksheep> getBlacksheep(User user) {

  DataStore<Blacksheep> herd;

  if (Utilities.isEmpty(user)) {

   if (logger.isDebugEnabled())
    logger.debug("getBlacksheep()");

   herd = dataStoreManager.makeDataStore(
    "blacksheep.getBlacksheepWithUser",
    new Object[]{
    }
   );
  }
  else {
   if (logger.isDebugEnabled())
    logger.debug("getBlacksheep() for user:" + user.getId());

   herd = dataStoreManager.makeDataStore(
    "blacksheep.fetchForUser",
    new Object[]{
     user.getId()
    }
   );

  }

  return herd.read();

 }



 /**
  * Delete a list of blacksheep.
  *
  * @param bss The list.
  */

 public void deleteBlacksheep(List<Blacksheep> bss) {

  for (Blacksheep bs : bss)
   if (bs != null) {

    if (logger.isDebugEnabled())
     logger.debug("deleteBlacksheep(): " + bs.getId());

    bs.delete();

    dataStoreManager.signal(DataStore.DataStoreSignal.delete, bs, "blacksheep");
   }

 }
 @WorkerManager.Metric(type = GAUGE, name = "number_current_blacksheep")
 public Number getMetricBlacksheepCounter() {

  return
   dataStoreManager.dataStore(JDBCDataStore.class, "blacksheep")
    .resetable()
    .make(Blacksheep.class)
    .count();

 }

}
