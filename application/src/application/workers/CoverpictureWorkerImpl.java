package application.workers;



import baked.application.beans.Coverpicture;
import baked.application.beans.Record;
import core.bakedBeans.BeanList;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;
import core.bakedBeans.JDBCDataStore;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static core.bakedBeans.Operator.EQUAL;



/**
 * Methods for cover pictures.
 *
 * @version 2023-12-01
 *
 * @author pilgrim.lutz@imail.de
 */

public class CoverpictureWorkerImpl implements CoverpictureWorker  {

 private static Logger logger;
 private static DataStoreManager dataStoreManager;



 /**
  * Gets a list of Coverpictures where description is the given description.
  *
  * @param description A name of an interpret. Case insensitive. Used as a prefix (A '%' will be added).
  *
  * @return A list of Coverpictures.
  *
  * @see application.beans.Coverpicture
  */

 @Override
 public List<Coverpicture> selectDescriptions(String description) {

  logger.debug(() -> "selectDescriptions(): description = \"" + description + "\"");

  DataStore<Coverpicture> ds =
   dataStoreManager.makeDataStore(
    "coverpictureWorker.selectDescriptions",
    new Object[] {
     description + "%"
    }
   );

  return ds.read();

 }



 /**
  * Gets a list of Coverpictures where description is the given description.
  *
  * @param origin A name of an interpret. Case insensitive. Used as a prefix (A '%' will be added).
  *
  * @return A list of Coverpictures.
  *
  * @see application.beans.Coverpicture
  */

 @Override
 public List<Coverpicture> selectOrigins(String origin) {

  logger.debug(() -> "selectOrigins(): origin = \"" + origin + "\"");

  DataStore<Coverpicture> ds =
   dataStoreManager.makeDataStore(
    "coverpictureWorker.selectOrigins",
    new Object[] {
     origin + "%"
    }
   );

  return ds.read();

 }



 /**
  * Generates a Coverpicture-Bean containing the data of the latest
  * Coverpicture-Bean for the given record.
  *
  * @param rec The record
  *
  * @return A Coverpicture
  */

 @Override
 public Coverpicture getTemplateCoverpicture(Record rec) {

  DataStore<Coverpicture> ds =
   DataStoreManager.get().dataStore(JDBCDataStore.class, "coverpictures")
   .resetable()
   .where(
     EQUAL("recordId", rec.getId())
   )
   .orderBy("description")
   .make(Coverpicture.class);

  List<Coverpicture> copics = ds.read();

  Coverpicture copic = new Coverpicture();

  if (copics.size() == 0)
   copic.setNr(1);
  else {
   Coverpicture copic1 = copics.get(0);
   Integer nr = copic1.getNr();
   copic.setNr(++ nr);
   copic.setDescription(copic1.getDescription());
   copic.setOrigin(copic1.getOrigin());
   copic.setFilename(copic1.getFilename());
  }

  return copic;

 }



 /**
  * Adds a coverpicture to a record.
  *
  * @param rec The record.
  * @param copic The coverpicture.
  */

 @Override
 public Coverpicture saveCoverpicture(Record rec, Coverpicture copic) {

  copic.setRecordId(rec.getId());

  copic.save();

  dataStoreManager.signal(DataStore.DataStoreSignal.write, rec, "coverpictures", "records", "favourites");

  return copic;

 }



 /**
  * Deletes the given cover pictures.
  *
  * @param copics The cover pictures to delete.
  */

 @Override
 public void deleteCoverpictures(BeanList<Coverpicture> copics) {

  for (Coverpicture copic : copics)
   if (copic != null)
    copic.delete();

  dataStoreManager.signal(DataStore.DataStoreSignal.delete, "coverpictures");

 }

}
