package application.workers;


import application.beans.SearchPatternDataStore;
import application.exceptions.SearchException;
import baked.application.beans.Record;
import baked.application.beans.*;
import core.bakedBeans.*;
import core.bakedBeans.DataStore.DataStoreSignal;
import core.base.WorkerManager;
import core.util.Property;
import core.util.Utilities;
import core.util.WorkerHelper;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static application.beans.SearchPattern.position.exact;
import static core.bakedBeans.Operator.*;
import static core.base.WorkerManager.Metric.MetricTypes.GAUGE;


/**
 * This class implements a search engine for MC.
 *
 * @version 2024-10-08
 * @author lp
 *
 */

public class SearchEngineImpl implements SearchEngine {

 private static Logger logger;
 private static DataStoreManager dataStoreManager;

 @Property("SearchEngine.SearchColumnsRecords")
 private static List<String> searchColumnsRecords = null;
 @Property("SearchEngine.SelectColumnsRecords")
 private static List<String> selectColumnsRecords = null;
 @Property("SearchEngine.SearchColumnsTitles")
 private static List<String> searchColumnsTitles = null;
 @Property("SearchEngine.SelectColumnsTitles")
 private static List<String> selectColumnsTitles = null;
 @Property("SearchEngine.SearchColumnsMusicians")
 private static List<String> searchColumnsMusicians = null;
 @Property("SearchEngine.SelectColumnsMusicians")
 private static List<String> selectColumnsMusicians = null;
 @Property("SearchEngine.OrderRecords")
 private static String orderStringRecords = null;
 @Property("SearchEngine.OrderTitles")
 private static String orderStringTitles = null;
 @Property("SearchEngine.OrderMusicians")
 private static String orderStringMusicians = null;
 @Property("SearchEngine.NoChildrenRecords")
 private static String noChildrenRecords = null;
 @Property("SearchEngine.NoChildrenTitles")
 private static String noChildrenTitles = null;
 @Property("SearchEngine.NoChildrenMusicians")
 private static String noChildrenMusicians = null;
 @Property("SearchEngine.HistoryLength")
 private static int historyLength = 10;
 @WorkerHelper()
 private LoginHelper loginHelper;
 @WorkerHelper()
 private SearchEngineHelper searchEngineHelper;

 static final String DATASTORE_CATEGORY_INDEXSEARCH = "indexSearch";



 /**
  * This method searches for records, titles and musicians.
  *
  * @param sp A SearchPatternBean with all information for searching for items.
  * @return An ArrayList with three ArrayLists containing the result.
  *
  * <p>The first ArrayList contains the records. The second contains the titles. The third contains the musicians.<p>
  */

 public ArrayList<List<? extends PersistentBean>> search(SearchPattern sp) {

  boolean onlyNew = Utilities.isTrue(sp.getRelFlags().getOnlyNew());
  boolean noChildren =
   Utilities.isTrue(sp.getRelFlags().getNoChildren()) && Utilities.isTrue(loginHelper.may("searchForNoChildren"));

  ArrayList<String> searchStrings = new ArrayList<>();

  String sd = sp.getSearchstring();

  if (!Utilities.isEmpty(sd)) {
   StringTokenizer sn = new StringTokenizer(sd, " ;,.%?*\t\"'", false);

   while (sn.hasMoreTokens())
    searchStrings.add(sn.nextToken());

   if (!Utilities.isTrue(loginHelper.may("searchempty")))
    if (Utilities.isEmptyList(searchStrings))
     return null;
  }

  SearchPattern.position spos = sp.getEnumPosition();
  String mediaList = buildMediaList(sp.getRelMedia());

  ArrayList<List<? extends PersistentBean>> resl = new ArrayList<>();  // That's the result list for the template-


  // Search for records.

  if (Utilities.isTrue(sp.getRelAreas().getRecords())) {
   SearchException.when((Void) -> Utilities.isEmpty(mediaList), "No media given!");

   searchRecords(onlyNew, noChildren, searchStrings, spos, mediaList, resl);
  }
  else
   resl.add(new ArrayList<>());


  // Search for titles.

  if (loginHelper.may("readtitles")  &&  Utilities.isTrue(sp.getRelAreas().getTitles())) {
   SearchException.when((Void) -> Utilities.isEmpty(mediaList), "No media given!");

   searchTitles(onlyNew, noChildren, searchStrings, spos, mediaList, resl);
  }
  else
   resl.add(new ArrayList<>());


  // Search for musicians.

  if (loginHelper.may("readmusicians")  &&  Utilities.isTrue(sp.getRelAreas().getMusicians())) {
   searchMusicians(onlyNew, noChildren, searchStrings, spos, resl);
  }
  else
   resl.add(new ArrayList<>());

  return resl;

 }



 private void searchMusicians(boolean onlyNew, boolean noChildren, ArrayList<String> searchStrings, SearchPattern.position spos, ArrayList<List<? extends PersistentBean>> resl) {

  String sd;
  StringBuilder sb = new StringBuilder();

  sb
   .append(buildSelect(selectColumnsMusicians, true))
   .append(" from musicians");

  Object[] params =
   new Object[
    searchStrings.size() * (searchColumnsMusicians.size() + 1) + (onlyNew? 1: 0)
   ];

  sd = buildSearch(
   searchStrings, searchColumnsMusicians,
   spos, params,
   "musicianalias.name"
  );

  String sep =
   " full outer join musicianalias on musicians.id=musicianalias.musician_id where ";

  if (sd.length() > 0) {
   sb.append(sep).append(sd);
   sep = " and ";
  }

  if (onlyNew) {
   sb.append(sep).append(" musicians.created >= ?");
   params[params.length - 1] = getNewLimit();
   sep = " and ";
  }

  if (noChildren)
   sb.append(sep).append(noChildrenMusicians);

  sb.append(" order by ").append(orderStringMusicians);

  logger.debug(() -> "searchMusicians(): " + sb);

  DataStore<Musician> ds =
   dataStoreManager.makeDataStore(
   "application.beans.ResetDataStore", "musicians",
    sb.toString(), params, Musician.class
   );

  resl.add(ds.read());

 }



 private void searchTitles(boolean onlyNew, boolean noChildren, ArrayList<String> searchStrings, application.beans.SearchPattern.position  spos, String mediaList, ArrayList<List<? extends PersistentBean>> resl) {

  String sd;
  boolean appendEnd;
  StringBuilder sb = new StringBuilder();

  sb.append(buildSelect(selectColumnsTitles, false));

  sb.append(" from titles, records where ");

  Object[] params = new Object[searchStrings.size() * searchColumnsTitles.size() + (onlyNew? 1: 0)];

  sd = buildSearch(searchStrings, searchColumnsTitles, spos, params, null);
  sb.append(sd);
  appendEnd = sd.length() > 0;

  if (onlyNew) {
   if (appendEnd) sb.append(" and");
   sb.append(" titles.created >= ?");
   params[params.length - 1] = getNewLimit();
   appendEnd = true;
  }

  if (noChildren) {
   if (appendEnd) sb.append(" and ");
   sb.append(noChildrenTitles);
   appendEnd = true;
  }

  if (appendEnd) sb.append(" and");
  sb.append(" records.id=titles.record_id and ");
  sb.append(mediaList);

  sb.append(" order by ").append(orderStringTitles);

  logger.debug(() -> "searchTitles()" + sb);

  DataStore<Title> ds =
   dataStoreManager.makeDataStore("application.beans.ResetDataStore", "titles", sb.toString(), params, Title.class);

  resl.add(ds.read());

 }



 private void searchRecords(boolean onlyNew, boolean noChildren, ArrayList<String> searchStrings, SearchPattern.position spos, String mediaList, ArrayList<List<? extends PersistentBean>> resl) {

  String sd;
  boolean appendAnd;
  StringBuilder sb = new StringBuilder();

  sb.append(buildSelect(selectColumnsRecords, false)).append(" from records where ");  // append "where" here because media are mandatory.

  Object[] params = new Object[searchStrings.size() * searchColumnsRecords.size() + (onlyNew? 1: 0)];  // eventually one more parameter for new date.

  sd = buildSearch(searchStrings, searchColumnsRecords, spos, params, null);
  sb.append(sd);
  appendAnd = sd.length() > 0;

  if (onlyNew) {
   if (appendAnd) sb.append(" and");
   sb.append(" records.created >= ?");
   params[params.length - 1] = getNewLimit();
   appendAnd = true;
  }

  if (noChildren) {
   if (appendAnd) sb.append(" and ");
   sb.append(noChildrenRecords);
   appendAnd = true;
  }

  if (appendAnd) sb.append(" and ");
  sb.append(mediaList);

  sb.append(" order by ").append(orderStringRecords);

  logger.debug(() -> "searchRecords(): " + sb);

  DataStore<Record> ds =
   dataStoreManager.makeDataStore("application.beans.ResetDataStore", "records", sb.toString(), params, Record.class);

  resl.add(ds.read());

 }



 /**
  * Save a search pattern for the current user.
  *
  * @param sp The search pattern to save.
  * @return The saved search pattern.
  */

 public SearchPattern saveSearchPattern(SearchPattern sp) {

  Login whoCalls = loginHelper.getLogin();

  if (application.beans.Login.get().may("savesearchpatterns")) {
   if (sp.has("name")) {
    // Handle search patterns with a name.
    sp.setUserId(whoCalls.getRefUser().getId());

    @SuppressWarnings("unchecked")
    List<SearchPattern> res =
     (List<SearchPattern>)sp.find(new String[] {"userId", "name", "searchstring", "position", "media", "areas", "flags"});

    if (Utilities.isEmptyList(res)) {
     // Modified named search patterns are treated as new unnamed search patterns.
     sp.setName(null);
     sp = handleUnnamedSearchPattern(sp);
    }
    else {
     // Set new access time.
     logger.debug("saveSearchPattern(): modifying access time.");

     SearchPattern b = res.get(0);
     b.setLastaccess(LocalDateTime.now());

     b.save();

     dataStoreManager.signal(DataStoreSignal.write, b, "searchpatterns");

     sp = b;
    }
   }
   else
    sp = handleUnnamedSearchPattern(sp);

  }

  return sp;

 }



 private SearchPattern handleUnnamedSearchPattern(SearchPattern sp) {

  sp.setUserId(loginHelper.getLogin().getUserId());

  LocalDateTime now = LocalDateTime.now();

  @SuppressWarnings("unchecked")
  List<SearchPattern> res =
   (List<SearchPattern>)sp.find(new String[] {"userId", "searchstring", "position", "media", "areas", "flags"});

  if (Utilities.isEmptyList(res)) {
   // New search pattern.
   logger.debug("handleUnnamedSearchPattern(): saving new search pattern.");

   // Limit to the maximum number of search patterns.
   @SuppressWarnings("unchecked")
   List<SearchPattern> res1 =
    (List<SearchPattern>)sp.find(new String[] {"userId"}, " AND name IS NULL", "lastaccess DESC");

   if (res1.size() >= historyLength) {
    for (int id = res1.size() - 1; id >= (historyLength - 1); id--)
     res1.get(id).delete();
   }

   sp.setCreated(now);
   sp.setLastaccess(now);
   sp.saveNew();

   dataStoreManager.signal(DataStoreSignal.write, sp, "searchpatterns");

   return sp;
  }
  else {
   // Already in data base. Set only new access date.
   logger.debug("handleUnnamedSearchPattern(): setting new access date for existing search pattern.");

   SearchPattern b = res.get(0);
   b.set("lastaccess", now);
   b.save();

   dataStoreManager.signal(DataStoreSignal.write, b, "searchpatterns");

   return b;
  }

 }



 private String buildMediaList(PropertyList<Media> meds) {

  String ret = null;

  if (meds != null) {
   StringBuilder ms = new StringBuilder();
   String msep = "";
   for (Media med : meds) {
    if (med != null) {
     ms.append(msep).append("'").append(med.getId()).append("'");
     msep = ",";
    }
   }
   ret = "records.medium_id in (" + ms.toString() + ")";
  }

  return ret;

 }



 private String buildSearch(List<String> searchStrings, List<String> cols,
                            SearchPattern.position pos, Object[] searchParams, String extraCol) {

  if (searchStrings.size() > 0) {
   StringBuilder sb = new StringBuilder();
   String sd;
   String sep = "(";
   int ix = 0;

   for (String sc : cols) {
    sb.append(sep);
    sd = "lower(" + sc + ") like ";
    String sep1 = "";
    for (String sstr: searchStrings) {
     sb.append(sep1).append(sd).append("lower(?)");
     switch(pos) {
      case prefix:
       searchParams[ix ++] = sstr + "%";
      break;
      case within:
       searchParams[ix ++] = "%" + sstr + "%" ;
      break;
      case postfix:
       searchParams[ix ++] = "%" + sstr;
      break;
      case exact:
       searchParams[ix ++] = sstr;
      break;
     }
     sep1 = " and ";
    }
    sep = " or ";
   }

   if (extraCol != null) {
    sep = " or ";
    for (String sstr: searchStrings) {
     sb.append(sep).append("lower(").append(extraCol).append(") like ").append("lower(?)");
     switch(pos) {
      case prefix:
       searchParams[ix ++] = sstr + "%";
       break;
      case within:
       searchParams[ix ++] = "%" + sstr + "%" ;
       break;
      case postfix:
       searchParams[ix ++] = "%" + sstr;
       break;
      case exact:
       searchParams[ix ++] = sstr;
       break;
     }
     sep = " and ";
    }
   }

   if (sb.length() > 0) {
    sb.append(")");
    return sb.toString();
   }
  }

  return "";

 }



 private String buildSelect(List<String> cols, boolean dist) {

  StringBuilder sb = new StringBuilder();

  sb.append("select");

  if (dist)
   sb.append(" distinct");

  String sep = " ";
  for (String c : cols) {
   sb.append(sep).append(c);
   sep =",";
  }

  return sb.toString();

 }



 /**
  * Returns the date which defines a record as "new".
  *
  * @return The date.
  *
  * @see SearchEngineImpl#isNew(Record)
  */

 public LocalDateTime getNewLimit() {

  Record rec = BeanFactory.make(Record.class);

  return rec.getNewLimit();

 }



 /**
  * Checks whether a record is a "new" record.
  * <p>A new record has to put to the database after the date returned by the method getNewLimit().<p>
  *
  * @param rec The record
  *
  * @return true if the record is a new record. False otherwise.
  *
  */

 public Boolean isNew(Record rec) {

  return rec.isNew();

 }



 /**
  * Returns a list of stored SearchPatterns for the given user.
  *
  * @return The list of SearchPatterns.
  */

 public List<SearchPattern> getSearchPatterns() {

  return
   dataStoreManager.dataStore(SearchPatternDataStore.class, "searchpatterns")
    .resetable()
    .where(
     AND(
      EQUAL("userId", loginHelper.getLogin().getUserId()),
      NOTLIKE("flags", "%ois=true%")
     )
    )
    .orderBy("name", "-lastaccess")
    .make(SearchPattern.class)
    .read();

 }



 /**
  * Returns a list of stored SearchPatterns for the given user
  * and a given prefix.
  *
  * @return The list of SearchPatterns.
  */

 public List<SearchIndex> getSearchstrings(String str) {

  return
  dataStoreManager.dataStore(SearchPatternDataStore.class, "searchpatterns")
   .resetable()
   .distinct()
   .loadWith("value")
   .where(
    LIKE("value", str.toLowerCase() + "%")
   )
   .orderBy("value")
   .make(SearchIndex.class)
   .read();

 }



 /**
  * Renames a given SearchPattern.
  *
  * @param sp The SearchPattern.
  * @param newName The new name.
  */

 public void rename(SearchPattern sp, String newName) {

  sp.setName(newName);
  sp.save();
  dataStoreManager.signal(DataStoreSignal.write, sp, "searchpatterns");

 }



 /**
  * Deletes the given SearchPatterns.
  *
  * @param sps The SearchPatterns.
  */

 public void delete(List<SearchPattern> sps) {

  for (SearchPattern sp : sps)
   if (!sp.isEmpty())
    sp.delete();

  dataStoreManager.signal(DataStoreSignal.delete, "searchpatterns");

 }



 /**
  * Gets a list of musicians for a see also list.
  *
  * @param rec The record
  *
  * @return The list of musicians.
  */

 public DataStore<Musician> getMusicianSeeAlso(Record rec) {

  return
   dataStoreManager.dataStore(ListDataStore.class, "seeAlso")
    .resetable(SearchEngineImpl::concerned)
    .sourceList((o, c) -> getAllMusicians(rec))
    .taggedWith(rec)
    .sortBy("name")
    .make(Musician.class);

 }



 private static Boolean concerned(DataStoreManager.IDataStoreMaker dsm, Bean b) {

  Record rec = (Record)b;

  if (rec ==  null)
   return Boolean.TRUE;  // signal() supplied no user. Better to "feel" concerned.

  try {
   return
    ((Record)(dsm.getTaggedWith()[0])).getId().equals(rec.getId());  // We or another record.
  }
  catch (Exception e) {
   return Boolean.TRUE;  // In doubt: We're concerned.
  }

 }



 private static BeanList<Musician> getAllMusicians(Record rec) {

  rec.load(rec.getId());  // Read bean to get latest values.

  List<String> names = Utilities.splitIssuers(rec.getInterpreter());

  rec.getColTitles().forEach(tit -> {
   try {
    Utilities.splitIssuers(tit.getInterpreter()).forEach(i -> {
     if (!names.contains(i))
      names.add(i);
    });
   }
   catch (Exception e) {
    // Bummer: No interpreters from titles!
   }
  });

  BeanList<Musician> ret =
   dataStoreManager.dataStore(JDBCDataStore.class, "seeAlso")
    .resetable()
    .where(
     OR(
      (Object[]) names.stream()
       .map(n -> Operator.LIKE("name", n + "%")).toArray(Operator[]::new)
     )
    )
    .make(Musician.class)
    .read();

  dataStoreManager.dataStore(JDBCDataStore.class, "seeAlso")
   .resetable()
   .where(
    OR(
     (Object[]) names.stream()
      .map(n -> Operator.LIKE("name", n + "%")).toArray(Operator[]::new)
    )
   )
   .make(Musicianalias.class)
   .forEach(ma -> {
     Musician m = BeanFactory.make(Musician.class);
     m.load(ma.getMusicianId());
     if (!ret.contains(m))
      ret.add(m);
     }
    );

  return ret;

 }



 private void reIndexAll() {

  Setting.setSetting("INDEXREADY", "FALSE");

  logger.info("ReIndex: Musicians...");
  dataStoreManager.dataStore(JDBCDataStore.class, "musicians")
   .resetable()
   .make(Musician.class)
   .forEach(searchEngineHelper::reCreateIndex);
  logger.info("ReIndex: Musicians ready!");

  logger.info("ReIndex: Records...");
  dataStoreManager.dataStore(JDBCDataStore.class, "records")
   .resetable()
   .make(Record.class)
   .forEach(searchEngineHelper::reCreateIndex);
  logger.info("ReIndex: Records ready!");

  logger.info("ReIndex: Titles...");
  dataStoreManager.dataStore(JDBCDataStore.class, "titles")
   .resetable()
   .make(Title.class)
   .forEach(searchEngineHelper::reCreateIndex);
  logger.info("ReIndex: Titles ready!");

  logger.info("ReIndex: Musicianalias...");
  dataStoreManager.dataStore(JDBCDataStore.class, "musicianalias")
   .resetable()
   .make(Musicianalias.class)
   .forEach(searchEngineHelper::reCreateIndex);
  logger.info("ReIndex: Musicianalias ready!");

  logger.info("ReIndex: Instruments...");
  dataStoreManager.dataStore(JDBCDataStore.class, "instruments")
   .resetable()
   .make(Instrument.class)
   .forEach(searchEngineHelper::reCreateIndex);
  logger.info("ReIndex: Instruments ready!");

  Setting.setSetting("DOREINDEX", "FALSE");
  Setting.setSetting("INDEXREADY", "TRUE");

 }



 /**
  * Searches items via Search Index.
  *
  * @param sp The SearchPattern. Only "searchString" ist currently used.
  *
  * @return A DataStore with SearchIndexes.
  */

 @Override
 public DataStore<SearchIndexResult> searchIndex(SearchPattern sp) {

  return DataStoreManager.get().dataStore(DataStore.class, DATASTORE_CATEGORY_INDEXSEARCH)
   .resetable()
   .taggedWith(makeBeanTag(sp))
   .sourceStream(() -> {
      List<String> searchStrings = Utilities.splitSearchString(sp.getSearchstring());
      Operator[] searchOperators;
      List<String> mSearchStrings = searchStrings;

      if (sp.getEnumPosition() == exact) {
       searchOperators =
        searchStrings.stream()
         .map(n -> EQUAL("value", n)).toArray(Operator[]::new);
      }
      else {
       searchOperators =
        searchStrings.stream()
         .map(n -> LIKE("value", modLike(n, sp.getEnumPosition()))).toArray(Operator[]::new);
       mSearchStrings =
        searchStrings.stream().map(s -> modLikeRegex(s, sp.getEnumPosition())).collect(Collectors.toList());
      }

     if (searchOperators.length == 0)
      searchOperators = null;

     BeanList<SearchIndexResult> records =
      Utilities.isTrue(sp.getRelAreas().getRecords())  &&  searchOperators != null ?
      getSearchIndexResultDataStore(sp, "recordId", mSearchStrings, searchOperators).read() :
      new BeanList<SearchIndexResult>(SearchIndexResult.class);

     return
       Stream.of(
        records.stream(),
         Utilities.isTrue(sp.getRelAreas().getTitles())  &&  searchOperators != null ?
         getSearchIndexResultDataStore(sp, "titleId", mSearchStrings, searchOperators).read().stream()
          .filter(t ->
           records.parallelStream().noneMatch(r -> r.getSubRecord().getId().equals(t.getSubTitle().getRecordId()))
          ) :
         null,
        Utilities.isTrue(sp.getRelAreas().getMusicians())  &&  searchOperators != null ?
        getSearchIndexResultDataStore(sp, "musicianId", mSearchStrings, searchOperators).read().stream() :
        null
       )
       .flatMap(sir -> sir);
     }
    )
   .make(SearchIndexResult.class);

 }



 private static int makeBeanTag(SearchPattern sp) {

  return
   sp.getSettedProperties().hashCode() + sp.getRelAreas().hashCode();

 }



 private static String modLike(String str, application.beans.SearchPattern.position pos) {

  switch(pos) {
   case prefix:
    return str + "%";
   case within:
    return "%" + str + "%";
   case postfix:
    return "%" + str;
  }

  return str;

 }



 private static String modLikeRegex(String str, application.beans.SearchPattern.position pos) {

  switch(pos) {
   case prefix:
    return str + ".*";
   case within:
    return ".*" + str + ".*";
   case postfix:
    return ".*" + str;
  }

  return str;

 }



 private static DataStore<SearchIndexResult> getSearchIndexResultDataStore(SearchPattern sp, String poi, List<String> searchStrings, Operator[] searchOperators) {

  return
   DataStoreManager.get().dataStore(DataStore.class, DATASTORE_CATEGORY_INDEXSEARCH)
    .resetable()
    .taggedWith(makeBeanTag(sp) + poi)
    .sourceStream(() -> {
      return
       DataStoreManager.get().dataStore(JDBCDataStore.class, DATASTORE_CATEGORY_INDEXSEARCH)
        .resetable()
        .loadWith("number", "beanName", "allValues", poi)
        .where(
         AND(
          GIVEN(poi),
          OR(
           (Object[])searchOperators
          )
         )
        )
        .groupBy("beanName", poi)
        .make(SearchIndexResult.class)
        .read()
        .stream()
        .filter(sir -> sir.getNumber() >= searchOperators.length)
        .filter(sir -> {
          List<String> vals = Arrays.asList(sir.getAllValues().split(";"));
          for (String sd : searchStrings)
           if (!Utilities.containsMatch(vals, sd))
            return false;

          return true;
         })
        .peek(sir -> {
          sir.setAndOther(
           DataStoreManager.get().dataStore(JDBCDataStore.class, DATASTORE_CATEGORY_INDEXSEARCH)
            .resetable()
            .loadWith("id", "propertyAlias", "position", "length", poi)
            .where(
             AND(
              EQUAL(poi, sir.get(sir.getRefIdAlias())),
              OR(
               (Object[])searchOperators
              )
             )
            )
            .make(SearchIndexResult.class)
            .read()
          );
         }
        );
     }
    )
    .make(SearchIndexResult.class);

 }



 /**
  * One time TimerJob for reindexing.
  */

 @WorkerManager.TimerJob(name = "SearchEngine.ReIndex", schedule = "!!")
 public void reIndexTImerJob() {

  if ("TRUE".equals(Setting.getSetting("DOREINDEX")))
   reIndexAll();

 }



 /**
  * Metric for the number of indices.
  *
   * @return The number of indices.
  */

 @WorkerManager.Metric(type = GAUGE, name = "number_indices")
 public Number getMetricIndicesCounter() {

  return
   dataStoreManager.dataStore(JDBCDataStore.class, SearchEngineImpl.DATASTORE_CATEGORY_INDEXSEARCH)
    .resetable()
    .make(SearchIndex.class)
    .count();

 }

}
