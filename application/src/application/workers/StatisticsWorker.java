package application.workers;

import application.aspects.annotations.NeedsRight;
import application.beans.AdditionYearRecords;
import application.beans.Record;
import baked.application.beans.*;
import core.bakedBeans.DataStore;
import core.base.WorkerManager;

import java.util.List;



public interface StatisticsWorker extends WorkerManager.IWorker {

 List<StatisticBean> getMediaCounts();

 List<StatisticBean> getTitleCounts();

 List<StatisticBean> getInstrumentsCounts();

 List<StatisticBean> getMusiciansCounts();

 List<Record> searchTopIssuer(String searchfor);

 DataStore<Record> searchTopIssuerDS(String searchfor);

 List<StatisticBean> getUnemployedMusicians();

 List<StatisticBean> getTopMusicians();

 DataStore<AdditionYearRecords> getAdditionRecords();

 DataStore<StatisticBean> getAdditionTitles();

 DataStore<StatisticBean> getAdditionMusicians();

 DataStore<Record> getRecordsForYear(String year);

 DataStore<Title> getTitlesForYear(String year);

 DataStore<Musician> getMusiciansForYear(String year);

 TitleStatisticBean getTitlesStatistics();

 MusicianStatisticBean getMusiciansStatistics();

 @NeedsRight("sort")
 SettingsOrderBy getOrderByAdditionsRecords();

 @NeedsRight("sort")
 void orderByAdditionsRecords(String property);

 @NeedsRight("sort")
 SettingsOrderBy getOrderByAdditionsTitles();

 @NeedsRight("sort")
 void orderByAdditionsTitles(String property);

 @NeedsRight("sort")
 SettingsOrderBy getOrderByAdditionsMusicians();

 @NeedsRight("sort")
 void orderByAdditionsMusicians(String property);

}
