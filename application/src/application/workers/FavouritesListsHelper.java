package application.workers;


import baked.application.beans.Record;
import baked.application.beans.*;
import core.bakedBeans.Bean;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;
import core.bakedBeans.JDBCDataStore;
import core.exceptions.MCException;
import core.util.DBConnection;
import core.util.DBConnectionStore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

import static core.bakedBeans.Operator.AND;
import static core.bakedBeans.Operator.EQUAL;


/**
 * Helper methods for working with favourites lists.
 *
 * @version 2025-01-06
 * @author lp
 */

public class FavouritesListsHelper {

 private static final Logger logger = LogManager.getLogger(FavouritesListsWorkerImpl.class);  // Log in the name of the worker.

 private static final HashMap<String, String[]> orders = new HashMap<>();
 static {
  orders.put("+interpreter", new String[] {"+application.beans.Record:interpreter", "application.beans.Record:releaseyear"});
  orders.put("+title", new String[] {"+application.beans.Record:title", "application.beans.Record:interpreter", "application.beans.Record:releaseyear"});
  orders.put("+releaseyear", new String[] {"+application.beans.Record:releaseyear", "application.beans.Record:interpreter", "application.beans.Record:title"});
  orders.put("+info", new String[] {"info", "+application.beans.Record:interpreter", "application.beans.Record:releaseyear"});
  orders.put("-interpreter", new String[] {"-application.beans.Record:interpreter", "application.beans.Record:releaseyear"});
  orders.put("-title", new String[] {"-application.beans.Record:title", "application.beans.Record:interpreter", "application.beans.Record:releaseyear"});
  orders.put("-releaseyear", new String[] {"-application.beans.Record:releaseyear", "application.beans.Record:interpreter", "application.beans.Record:title"});
  orders.put("-info", new String[] {"-info", "application.beans.Record:interpreter", "application.beans.Record:releaseyear"});
 }

 static final String FAVOURITESLIST_KEY = "favouritesList.orderBy";



 /**
  * Removes a record from all favourites lists.
  *
  * @param rec The record to remove.
  */

 static void removeRecordFromAllFavouritesLists(Record rec) {

//  DBConnection con = DB.get().getConnection();
  DBConnection con = DBConnectionStore.getConnection();

  PreparedStatement stmt = con.prepareStatement("delete from favourites where record_id=?");
  try {
   stmt.setObject(1, rec.getId());

   stmt.execute();

   DBConnectionStore.releaseConnection();
  }
  catch (SQLException e) {
   DBConnectionStore.rollback();
   throw new MCException(e);
  }

//  DB.get().releaseConnection(con);

  DataStoreManager.get().signal(
   DataStore.DataStoreSignal.delete,
   "favourites", "favouritesLists"
  );

  logger.info("Removed record from all favourites lists.");

 }



 /**
  * Adds a record to the special favourites list for new records.
  *
  * @param user The user
  * @param rec The record
  * @param newFavoritesListName Name for the new records favourites list.
  */

 static void addRecordToNewFavouritesList(User user, Record rec, String newFavoritesListName) {

  FavouritesList fl = getNewRecordsFavouritesList(user);

  if (core.bakedBeans.Utilities.isEmpty(fl))
   fl = createNewFavouritesList(user, newFavoritesListName, FavouritesList.purposes.systemNewRecords);

  putRecordToFavouritesList(rec, fl);

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, "favourites");

 }



 /**
  * Fetches the user's New-FavouritesList.
  *
  * @param user The user.
  * @return User's list.
  */

 public static FavouritesList getNewRecordsFavouritesList(User user) {

  return DataStoreManager.get().dataStore(JDBCDataStore.class, "favouritesLists")
   .resetable(FavouritesListsHelper::concerned)
   .taggedWith(user)
   .where(
    AND(
     EQUAL("userId", user.getId()),
     EQUAL("enumPurpose", FavouritesList.purposes.systemNewRecords)
    )
   )
   .make(FavouritesList.class)
   .readBean();

 }



 /**
  * Creates a new favourite list.
  *
  * @param user The owner of the new list.
  * @param name The name for the new list.
  *
  * @return the newly created (and saved) list.
  */

 static FavouritesList createNewFavouritesList(User user, String name) {

  return createNewFavouritesList(user, name, FavouritesList.purposes.user);

 }



 /**
  * Creates a new favourite list.
  *
  * <p>The new favourites list has read and write access for the user.</p>
  *
  * @param user The owner of the new list.
  * @param name The name for the new list.
  * @param purpose The purpose for the new list.
  *
  * @return the newly created (and saved) list.
  */

 static FavouritesList createNewFavouritesList(User user, String name, FavouritesList.purposes purpose) {

  FavouritesList fl = new FavouritesList();

  fl
   .setUserId(user.getId())
   .setCreated(LocalDateTime.now())
   .setName(name)
   .setOrderNr(0)
   .setEnumPurpose(purpose)
   .setUserAccessRead(Boolean.TRUE)
   .setUserAccessWrite(Boolean.TRUE);

  fl.save();

  fl = DataStoreManager.get().dataStore(JDBCDataStore.class, "favouritesLists")
   .resetable()
   .where(EQUAL("id", fl.getId()))
   .make(FavouritesList.class)
   .readBean();

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, user, "favouritesLists");

  return fl;

 }



 /**
  * Puts a record on the given favourites list.
  *
  * @param rec The record
  * @param fl The favourites list.
  */

 static void putRecordToFavouritesList(Record rec, FavouritesList fl) {

  Favourites fav = new Favourites();

  fav
   .setCreated(LocalDateTime.now())
   .setFavouriteslistId(fl.getId())
   .setRecordsId((Integer)rec.getKeyValueInternal())
   .setOrderNr(0);

  fav.save();

  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, "favourites");

  resort(fl);

 }



 private static void resort(FavouritesList fl) {

  Setting setting =
   SortHelper.getSortSetting(fl.getSubUser(), "interpreter", SettingsOrderBy.direction.none, FAVOURITESLIST_KEY, fl.getId());

  SettingsOrderBy order = setting.getRelOrderBy();
  if (order.getEnumDirection() != SettingsOrderBy.direction.none)
   sortFavourites(fl, order);

 }



 /**
  * Sorts the given FavouritesList.
  *
  * @param fl the FavouritesList.
  * @param order The sort order.
  */

 static void sortFavourites(FavouritesList fl, SettingsOrderBy order) {

   // Sort.

  AtomicInteger orderNr = new AtomicInteger(0);

  DataStoreManager.get().dataStore(JDBCDataStore.class, "favourites")
   .resetable()
   .oneTransaction()
   .where(EQUAL("favouriteslistId", fl.getId()))
   .orderBy(orders.get(order.getOrderForOrderByClause() + order.getProperty()))
   .make(Favourites.class)
   .forEach(f -> {
    try {
     f.setOrderNr(orderNr.incrementAndGet());
     f.save();
    } catch (Exception e) {
     logger.error("sortFavourites(); Cannot set order!");
    }
   });

 }



 static Boolean concerned(DataStoreManager.IDataStoreMaker dsm, Bean b) {

  User user = (User)b;

  if (user ==  null)
   return Boolean.TRUE;  // signal() supplied no user. Better to "feel" concerned.

  try {
   return
    ((User)(dsm.getTaggedWith()[0])).getId().equals(user.getId());  // We or another user.
  }
  catch (Exception e) {
   return Boolean.TRUE;  // In doubt: We're concerned.
  }

 }

}
