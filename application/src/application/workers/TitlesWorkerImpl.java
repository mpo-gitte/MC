package application.workers;

import baked.application.beans.*;
import baked.application.beans.Record;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;
import core.util.WorkerHelper;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.List;

import static application.beans.Title.SETTINGS_SORTKEY;


/**
 * Methods for Titles.
 *
 * @version 2025-01-06
 * @author lp
 */

public class TitlesWorkerImpl implements TitlesWorker {

 private static Logger logger;
 private static DataStoreManager dataStoreManager;

 @WorkerHelper()
 private LogbookHelper logbookHelper;
 @WorkerHelper()
 private SearchEngineHelper searchEngineHelper;



 /**
  * Gets a list with temporary results.
  *
  * @return The list
  */

 @Override
 public List<TitleReference> getTemporaryResults() {

  DataStore<TitleReference> ds =
   dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore",
    "temporaryResults",
    "select titlereferences.title_id, titles.record_id st$record_id, titles.id st$id, titles.interpreter st$interpreter, titles.title st$title, titles.duration st$duration, records.interpreter st$sr$interpreter, records.title st$sr$title from titlereferences, titles, records where titlereferences.purpose=? and titlereferences.title_id=titles.id and titles.record_id=records.id order by titles.interpreter",
    new Object[] {
     TitleReference.purposes.tempResult.ordinal()
    },
    TitleReference.class);

  return ds.read();

 }



 /**
  * Deletes temporary results.
  */

 @Override
 public void deleteTemporaryResults() {

  for (TitleReference b : getTemporaryResults())
   b.delete();

  dataStoreManager.signal(DataStore.DataStoreSignal.delete, "temporaryResults");

 }



 /**
  * Saves a title for a record and writes logentry and notifies DataStores.
  *
  * @param rec The record
  * @param tit The title
  */

 @Override
 public void save(Record rec, Title tit) {

  tit.save();

  CatalogHelper.createCatalogRequest(rec, tit);

  searchEngineHelper.createIndex(tit);

  writeLogbook(rec, tit, LogbookEntry.verbs.write);

  dataStoreManager.signal(
   DataStore.DataStoreSignal.write,
   rec,
   "titles", "statisticsCount", "seeAlso"
  );

 }



 private void writeLogbook(Record rec, Title tit, LogbookEntry.verbs verb) {

  logbookHelper.writeLogbook(rec, tit, null, null, verb);

 }



 /**
  * Gets a list of Titles where interpret is the given interpret.
  *
  * @param interpreter A name of an interpret. Case-insensitive. Used as a prefix (A '%' will be added).
  *
  * @return A list of Titles.
  *
  * @see application.beans.Title
  */

 @Override
 public List<Title> selectInterpreters(String interpreter) {

  logger.debug(() -> "selectInterpreters(): interpreter = \"" + interpreter + "\"");

  DataStore<Title> ds =
   dataStoreManager.makeDataStore(
    "titlesWorker.selectInterpreters",
    new Object[] {
     interpreter + "%"
    }
   );

  return ds.read();

 }



 /**
  * Gets a list of Titles where title is the given title.
  *
  * @param title A title's title. Case-insensitive. Used as a prefix (A '%' will be added).
  *
  * @return A list of Titles.
  *
  * @see application.beans.Title
  */

 @Override
 public List<Title> selectTitles(String title) {

  logger.debug(() -> "selectTitles(): title = \"" + title + "\"");

  DataStore<Title> ds =
   dataStoreManager.makeDataStore(
    "titlesWorker.selectTitles",
    new Object[] {
     title + "%"
    }
   );

  return ds.read();

 }



 /**
  * Gets a list of Titles where composer is the given composer.
  *
  * @param composer A title's composer. Case-insensitive. Used as a prefix (A '%' will be added).
  *
  * @return A list of Titles.
  *
  * @see application.beans.Title
  */

 @Override
 public List<Title> selectComposers(String composer) {

  logger.debug(() -> "selectComposers(): composer = \"" + composer + "\"");

  DataStore<Title> ds =
   dataStoreManager.makeDataStore(
    "titlesWorker.selectComposers",
    new Object[] {
     composer + "%"
    }
   );

  return ds.read();

 }


 /**
  * Gets a list of Titles where producer is the given producer.
  *
  * @param producer A title's producer. Case-insensitive. Used as a prefix (A '%' will be added).
  *
  * @return A list of Titles.
  *
  * @see application.beans.Title
  */

 @Override
 public List<Title> selectProducers(String producer) {

  logger.debug(() -> "selectProducers(): producer = \"" + producer + "\"");

  DataStore<Title> ds =
   dataStoreManager.makeDataStore(
    "titlesWorker.selectProducers",
    new Object[] {
     producer + "%"
    }
   );

  return ds.read();

 }



 /**
  * Gets a list of Titles where location is the given location.
  *
  * @param location A title's location. Case-insensitive. Used as a prefix (A '%' will be added).
  *
  * @return A list of Titles.
  *
  * @see application.beans.Title
  */

 @Override
 public List<Title> selectLocations(String location) {

  logger.debug(() -> "selectLocations(): location = \"" + location + "\"");

  DataStore<Title> ds =
   dataStoreManager.makeDataStore(
    "titlesWorker.selectLocations",
    new Object[] {
     location + "%"
    }
   );

  return ds.read();

 }



 /**
  * Generates a Title-Bean containing the data of the latest Title-Bean for the given record.
  * The track number will be set to the maximum track number for the given side + 1.
  *
  * @param rec The record
  *
  * @return A title
  */

 @Override
 public Title getTemplateTitle(Record rec) {

  DataStore<Title> ds =
   dataStoreManager.makeDataStore(
    "application.beans.RecordsCollectionDataStore",
    "titles",
    "select * from titles where record_id=? order by id desc",
    new Object[] {
     rec.getId()
    },
    Title.class);
  List<Title> tits = ds.read();

  Title tit;
  if (tits.size() == 0) {
   tit = new Title();
   tit.setSide(1);
   tit.setTrack(1);
   tit.setInterpreter(rec.getInterpreter());
  }
  else {
   tit = tits.get(0);
   tit.setId(null);  // Kill id.

   DataStore<Title> ds1 =
    dataStoreManager.makeDataStore(
     "application.beans.RecordsCollectionDataStore",
     "titles",
     "select track from titles where record_id=? and side=? order by track desc",
     new Object[] {
      rec.getId(),
      tit.getSide()
     },
     Title.class);
   List<Title> tits1 = ds1.read();
   if (tits1.size() > 0) {
    Title tit1 = tits1.get(0);
    int id = tit1.getTrack();
    tit.setTrack(++ id);
   }
  }
  return tit;
 }



 /**
  * Add a title to the given record by calling save()
  *
  * @param rec The record
  * @param tit The title
  *
  * @see TitlesWorkerImpl#save(baked.application.beans.Record,baked.application.beans.Title)
  */

 @Override
 public void addTitle(Record rec, Title tit) {

  tit.setRecordId(rec.getId());
  tit.setCreated(LocalDateTime.now());

  save(rec, tit);

 }



 /**
  *
  * Deletes the given titles.
  *
  * <p>Signals DataStores of category "titles".</p>
  *
  * <p>Writes a logbook entry for concerned titles.</p>
  *
  * @param tits A list with titles to delete.
  */

 @Override
 public void deleteTitles(List<Title> tits) {

  for (Title tit : tits)
   if (tit != null) {

    Record rec = tit.getSubRecord();

    logbookHelper.writeLogbook(rec, tit, null, null, LogbookEntry.verbs.delete);  // Yes it's silly first we write a log entry for a non existing bean...
    logbookHelper.correctLogbook(tit);  // then we correct it.

    SortHelper.deleteSetting(SETTINGS_SORTKEY, tit.getKeyValue());

    searchEngineHelper.deleteIndex(tit);

    tit.delete();

    dataStoreManager.signal(DataStore.DataStoreSignal.delete, rec, "titles");

    CatalogHelper.createCatalogRequestForce(rec);

   }

 }



 /**
  * Gets the current order settings for the given Title.
  *
  * @param tit The Title with the Job-list.
  * @param user The user with the setting.
  *
  * @return The settings.
  */

 @Override
 public SettingsOrderBy getOrderBy(Title tit, User user) {

  Setting setting =
   SortHelper.getSortSetting(user, "instrument", SettingsOrderBy.direction.none, SETTINGS_SORTKEY, tit.getId());

  return setting.getRelOrderBy();

 }



 /**
  * Set sort settings.
  *
  * @param tit For title.
  * @param user And user.
  * @param property And property.
  *
  * @return The new sort settings.
  */

 @Override
 public SettingsOrderBy orderBy(Title tit, User user, String property) {

  if (logger.isDebugEnabled())
   logger.debug(("orderBy(): sorting list for user: " + user.getId()));

  // Switch the sort order.
  Setting setting =
   SortHelper.getSortSetting(user, "interpreter", SettingsOrderBy.direction.none, SETTINGS_SORTKEY, tit.getId());
  SortHelper.switchOrder(setting, property);
  setting.getRelOrderBy().setProperty(property);

  // The "real" sorting is done in the getter.

  // Save current sort order.
  setting.save();
  dataStoreManager.signal(DataStore.DataStoreSignal.write, user, "settings");

  return setting.getRelOrderBy();

 }

}
