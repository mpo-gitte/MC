package application.workers;

import baked.application.beans.*;
import core.bakedBeans.BeanFactory;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;
import core.bakedBeans.Utilities;
import core.util.WorkerHelper;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;



/**
 * Class with methods for Jobs.
 *
 * @version 2024-08-09
 * @author lp
 *
 */

public class InstrumentsWorkerImpl implements InstrumentsWorker {

 private static Logger logger;
 private static DataStoreManager dataStoreManager;

 @WorkerHelper()
 private LogbookHelper logbookHelper;
 @WorkerHelper()
 private SearchEngineHelper searchEngineHelper;



 /**
  * Returns a list of all defined instruments.
  *
  * @return The list with all defined instruments
  */

 public List<Instrument> getInstruments() {

  DataStore<Instrument> ds = dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore", "instruments",
    "select * from instruments order by id",
    Instrument.class
   );

  return ds.read();

 }



 /**
  * Adds an instrument and a musician to a title.
  *
  * @param tits The titles
  * @param instr The instrument
  * @param mus The musician
  */

 public void addInstrument(List<Title> tits, Instrument instr, Musician mus) {

  mus.getConnection();  // Only for beginning a transaction.

  for (Title tit : tits) {
   if (logger.isDebugEnabled())
    logger.debug("addInstrument(): Adding musician " + mus.getId() + " (" + mus.getName() + ") to title " + tit.getId() + " (" + tit.getTitle() + ")");

   if (tit != null) {

    List<Job> jobs = tit.getColJobs().collect();

    Job newJob = BeanFactory.make(Job.class);

    Musician newMus = newJob.getSubMusician();

    if (mus.isDirty()) {
     // New musician.
     newMus.setName(mus.getName());
     newMus.setInstrumentId(getInstrument(instr));
     newMus.setCreated(LocalDateTime.now());
    }
    else {
     // Known musician.
     newMus.loadInternal(mus.getKeyValue());
    }

    newJob.setInstrumentId(instr.getId());
    newJob.setMusicianId(newMus.getId());
    newJob.setCreated(LocalDateTime.now());

    if (!jobs.contains(newJob)) {
     jobs.add(newJob);
     tit.save();  // Save the bunch of beans.

     if (mus.isDirty()) {
      // Have to tell a new musician is created.
      dataStoreManager.signal(DataStore.DataStoreSignal.write, newMus, "musicians");
      // Update search index.
      searchEngineHelper.reCreateIndex(newMus);
      dataStoreManager.signal(DataStore.DataStoreSignal.write, "indexSearch");
      // Have to write log entry for new musician here because it gets its id during saving the title.
      logbookHelper.writeLogbook(null, null, newMus, null, LogbookEntry.verbs.createMusician);
     }

     dataStoreManager.signal(DataStore.DataStoreSignal.write, tit, "jobs");
     dataStoreManager.signal(DataStore.DataStoreSignal.write, "comusicians", "instrumentscount", "statisticsCount", "musicians");

     logbookHelper.writeLogbook(null, tit, newMus, null, LogbookEntry.verbs.attachMusician);

     if (logger.isDebugEnabled())
      logger.debug("addInstrument(): Added " + newMus.getId() + " " + instr.getId() + " " + tit.getId());

     mus = newMus;  // For the next title. If it was formerly a new musician it is now a known musician.
    }
   }
  }

  mus.releaseConnection();  // Only for ending the transaction.

 }



 private String getInstrument(Instrument instr) {

  String minstrId = instr.getMainInstrumentId();

  return (minstrId == null) ? instr.getId() : minstrId;

 }



 /**
  * Adds a list of jobs to some titles.
  *
  * @param tits The titles
  * @param newJobs The jobs
  */

 public void addInstruments(List<Title> tits, List<Job> newJobs) {

  Title titCon = null;

  for (Title tit : tits) {
   if (tit != null) {
    if (titCon == null) {
     titCon = tit;
     titCon.getConnection();  // Only for beginning a transaction.
    }

    List<Job> jobs = tit.getColJobs().collect();

    for (Job newJob : newJobs) {
     if (newJob != null && !Utilities.stringIsEmpty(newJob.getInstrumentId())) {
      Musician mus = newJob.getSubMusician();
      Job addJob = BeanFactory.make(Job.class);
      addJob.setMusicianId(mus.getId());  // The Job didn't get this set because the template sets subMusician.m_id due to enctyption.
      addJob.setInstrumentId(newJob.getInstrumentId());
      if (!jobs.contains(addJob)) {
       addJob.setTitleId(null);
       jobs.add(addJob);

       logbookHelper.writeLogbook(null, tit, mus, null, LogbookEntry.verbs.attachMusician);

       if (logger.isDebugEnabled())
        logger.debug("addInstruments(): adding " + tit.getId() + " " + addJob.getInstrumentId() + " " + mus.getName());
      }
     }
    }
    tit.save();
    dataStoreManager.signal(DataStore.DataStoreSignal.write, tit, "jobs");
    dataStoreManager.signal(DataStore.DataStoreSignal.write, "comusicians", "instrumentscount", "instruments", "statisticsCount", "musicians");
   }
  }

  if (titCon != null)
   titCon.releaseConnection();  // Only for ending the transaction.

 }



 /**
  *
  * Removes the given jobs.
  *
  * <p>Signals DataStores of category "musician".</p>
  *
  * <p>Writes a logbook entry for concerned title and musician.</p>
  *
  * @param jobs A list with jobs to remove.
  *
  */

 @Override
 public void removeJob(List<Job> jobs) {

  Title tit = null;

  for (Job job : jobs)
   if (job != null) {
    job.delete();
    tit = job.getSubTitle();  // Should always be the same for each iteration.
    logbookHelper.writeLogbook(null, tit, job.getSubMusician(), null, LogbookEntry.verbs.detachMusician);
   }

  if (tit != null) {
   dataStoreManager.signal(DataStore.DataStoreSignal.delete, tit, "jobs");
   dataStoreManager.signal(DataStore.DataStoreSignal.delete, "comusicians", "statisticsCount");
   dataStoreManager.signal(DataStore.DataStoreSignal.write, "instrumentscount", "musicians");
  }
 }



 /**
  * Creates a list of musicians for suggestions.
  *
  * @param str Musicians name. Will be treated as the beginning of the name.
  * @param instr Optional instrument. May be null.
  * @return The list of musicians.
  */

 public List<Musician> selectMusicians(String str, Instrument instr) {

  if (logger.isDebugEnabled())
   if (instr == null)
    logger.debug(() -> "selectMusicians(): str = \"" + str + "\"");
   else
    if (logger.isDebugEnabled())
     logger.debug("selectMusicians(): str = \"" + str + "\", instr = \"" + instr.getId() + "\"");

  DataStore<Musician> ds;

   if (instr == null)
    ds =
     dataStoreManager.makeDataStore(
      "instrumentsWorker.selectMusicians",
      new Object[] {
       str + "%"
      }
     );
   else
    ds =
     dataStoreManager.makeDataStore(
      "instrumentsWorker.selectMusiciansAndInstrument",
      new Object[] {
       str + "%",
       instr.getId()
      }
     );

  return ds.read();

 }



 /**
  * Returns a list of jobs for the given titles. The jobs are unique.
  *
  * @param tits The titles.
  * @return Some jobs.
  */

 public List<Job> getUniqueJobsForTitles(List<Title> tits) {

  if (Utilities.isEmpty(tits))
   return null;

  StringBuilder sel = new StringBuilder();
  String sep = "";
  Integer[] args = new Integer[tits.size()];
  int id = 0;

  sel.append("select distinct " +
   "jobs.instrument_id, musicians.id sm$id, musicians.name sm$name, musicians.instrument_id sm$instrument_id " +
   "from jobs, musicians " +
   "where jobs.musician_id = musicians.id and jobs.title_id in (");

  for (Title t: tits) {
   sel.append(sep).append("?");
   args[id ++] = t.getId();
   sep = ",";
  }
  sel.append(") order by jobs.instrument_id, musicians.name");

  DataStore<Job> ds =
   dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore",
    "jobs",
    sel.toString(),
    args,
    Job.class);

  return ds.read();

 }



 /**
  * Exchanges a musician.
  *
  * <p>The musician "omus" playing his instrument "oinstr" will be added to all given titles where the musician "mus" plays "pinstr".</p>
  *
  * <p>"pinstr" may be empty. In this case all titles where the musician "mus" plays any instrument will be selected."</p>
  *
  * <p>"omus" may be null. Then "mus" remains on the title. But instrument is changed to "oinstr".</p>
  *
  * <p>"oinstr" may be null. Then "omus" is attached to the title playing the same instrument "mus" did.</p>
  *
  * @param user The user who does the exchange. Used for title reference.
  * @param tits A list of titles to change the musician for.
  * @param mus The musician to exchange (old musician).
  * @param pinstr His instrument. NULL means "all instruments".
  * @param omus Other (new)  musician.
  * @param oinstr His  instrument.
  */

 public void exchange(User user, List<Title> tits, Musician mus, String pinstr, Musician omus, String oinstr) {

  if (logger.isDebugEnabled())
   logger.debug("exchange(): musician " + mus.getId() + " playing " + pinstr + " with " + omus.getId() + " playing " + oinstr);

  boolean told = false;

  mus.getConnection();  // Only for beginning a transaction.

  for (Title tit : tits) {
   if (logger.isDebugEnabled())
    logger.debug("exchange(): on title " + tit.getId());

   boolean tellIt;

   if (!Utilities.isEmpty(tit)) {
    List<Job> jobs = tit.getColJobs().read();  // The jobs of the title.
    List<Job> origJobs = new ArrayList<>();

    tellIt = false;

    for (Job j : jobs)  // Store current jobs.
     origJobs.add(newJob(j));

    for (Job j : jobs) {
     if (instrEqual(j, mus, pinstr)) {  // The old musician plays the optionally given instrument?
      Job newJob = newJob(j);
      if (!Utilities.isEmpty(omus))
       newJob.setSubMusician(omus);  // Other musician given. Set.
      if (!Utilities.isEmpty(pinstr) && !Utilities.isEmpty(oinstr))
       newJob.setInstrumentId(oinstr);  // Other instrument given. Set.
      if (!origJobs.contains(newJob)) {  //  Ignore exchange if musician/instrument is already attached to this title.
       newJob.setId(j.getId());  // Overwrite the old job.
       newJob.save();
       tellIt = true;

       logbookHelper.writeLogbook(/*(Record)tit.get("subRecord")*/ null, tit, mus, omus, LogbookEntry.verbs.exchangeMusician);
      }
      else
       logger.error("exchange(): musician " + mus.getId() + " playing " + pinstr + " with " + omus.getId() + " playing " + oinstr + "not done because new Job already existing!");
     }
    }

    if (tellIt) {
     dataStoreManager.signal(DataStore.DataStoreSignal.write, tit, "jobs");

     makeTitleReference(user, tit);

     told = true;
    }
   }
  }

  if (told) {
   dataStoreManager.signal(DataStore.DataStoreSignal.write, "temporaryResults");
   dataStoreManager.signal(DataStore.DataStoreSignal.write, mus, "musicians", "comusicians", "records");
  }

  mus.releaseConnection();  // Only for ending the transaction.

 }



 private void makeTitleReference(User user, Title tit) {

  if (logger.isDebugEnabled())
   logger.debug("makeTitleReference(): user " + user.getId() + " title " + tit.getId());

  TitleReference tref = new TitleReference();

  tref.setSubTitle(tit);
  tref.setEnumPurpose(TitleReference.purposes.tempResult);
  tref.setUserId(user.getId());

  tref.saveNew();  // ToDo: Can't use save() because key comes from a subBean.

 }



 private Job newJob(Job otherJob) {

  Job retJob = BeanFactory.make(Job.class);

  retJob.setCreated(LocalDateTime.now());


  // Copy the essentials.

  retJob.setMusicianId(otherJob.getMusicianId());
  retJob.setTitleId(otherJob.getTitleId());
  retJob.setInstrumentId(otherJob.getInstrumentId());

  return retJob;

 }



 private boolean instrEqual(Job job, Musician mus, String pinstr) {

  if (job.getMusicianId().equals(mus.getId())) {
   if (!Utilities.isEmpty(pinstr))
    return job.getInstrumentId().equals(pinstr);

   return true;
  }

  return false;
 }



 /**
  * This method counts the usage of the given instrument within jobs.
  *
  * @param instr The instrument
  * @return The usage
  */

 public int countInstrumentsOnJobs(Instrument instr) {

  if (logger.isDebugEnabled())
   logger.debug("countInstrumentsOnJobs(): " + instr.getId());

  StatisticBean stat = InstrumentsHelper.countInstrumentsOnJobs(instr);

  Long number = stat.getNumber();

  if (number == null)
   return 0;

  return number.intValue();

 }



 /**
  * This method counts the usage of the given instrument as musicians main instrument.
  *
  * @param instr The instrument
  * @return The usage
  */

 public int countInstrumentsOnMusicians(Instrument instr) {

  if (logger.isDebugEnabled())
   logger.debug("countInstrumentsOnMusicians(): " + instr.getId());

  StatisticBean stat = InstrumentsHelper.countInstrumentsOnMusicians(instr);

  Long number = stat.getNumber();

  if (number == null)
   return 0;

  return number.intValue();

 }



 /**
  * Checks whether an instrument can be deleted.
  *
  * <p>Deletion is possible if the instrument isn't used in any job
  * or as a main instrument for any musician.</p>
  *
  * @param instr The instrument.
  * @return true if instrument is unused. False otherwise.
  */

 public boolean enableDelete(Instrument instr) {

  if (logger.isDebugEnabled())
   logger.debug("enableDelete(): " + instr.getId());

  StatisticBean stat = InstrumentsHelper.countInstrumentsOnJobs(instr);

  int id = stat.getNumber().intValue();

  stat = InstrumentsHelper.countInstrumentsOnMusicians(instr);

  id += stat.getNumber().intValue();

  return id == 0;

 }



 /**
  * Saves the given instrument.
  *
  * <p>Signals all DataStores of category "instruments".</p>
  *
  * @param instr The instrument
  * @param asNew If true save a new instrument.
  */

 public void save(Instrument instr, boolean asNew) {

  if (logger.isDebugEnabled())
   logger.debug("save(): " + instr.getId());

  if (Utilities.isEmpty(instr.getMainInstrumentId()))
   instr.setMainInstrumentId(null);  // Due to #listSelectExt() :-(

  // Save.
  if (asNew)
   instr.saveNew();
  else
   instr.save();

  // Tell it.
  dataStoreManager.signal(DataStore.DataStoreSignal.write, instr, "instruments");

 }



 /**
  * Checks if the given string is a unique key for an instrument.
  *
  * @param instrid Id for the instrument.
  *
  * @return true if given instrument is not used. False otherwise.
  */

 public boolean checkUniqueInstrument(String instrid) {

  logger.debug(() -> "checkUniqueInstrument(): " + instrid);

  DataStore<StatisticBean> ds = dataStoreManager.makeDataStore(
   "instruments.checkUniqueInstrument",
   new Object[] {instrid}
  );

  int nr = ds.readBean().getNumber().intValue();

  logger.debug(() -> "checkUniqueInstrument(): " + instrid + " " + (nr == 0));

  return nr == 0;

 }



 /**
  * Deletes a list of instruments.
  *
  * @param instrs A list of Instrument beans.
  */

 public void deleteInstrument(List<Instrument> instrs) {

  for (Instrument instr : instrs)
   if (instr != null) {

    if (logger.isDebugEnabled())
     logger.debug("deleteInstrument(): " + instr.getId());

    instr.delete();

    dataStoreManager.signal(DataStore.DataStoreSignal.delete, instr, "instruments");
   }

 }



 /**
  * Gets a list of PersistentBeans with musicians playing thw given instrument.
  *
  * @param instr Instrument.
  *
  * @return The musisicans. May be null.
  */

 public List<Musician> musiciansPlayingInstrument(Instrument instr) {

  if (logger.isDebugEnabled())
   logger.debug("musiciansPlayingInstrument(): " + instr.getId());

  DataStore<Musician> ds = dataStoreManager.makeDataStore(
   "instruments.musiciansPlayingInstrument",
   new Object[] {instr.getId()});

  return ds.read();

 }

}
