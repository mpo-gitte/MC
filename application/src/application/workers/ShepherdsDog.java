package application.workers;



import application.exceptions.AccountLockedException;
import application.exceptions.SessionLockedException;
import baked.application.beans.Blacksheep;
import baked.application.beans.Client;
import baked.application.beans.Login;
import core.bakedBeans.DataStore;
import core.bakedBeans.DataStoreManager;
import core.bakedBeans.JDBCDataStore;
import core.bakedBeans.Utilities;
import core.base.Request;
import core.exceptions.MCException;
import core.exceptions.SecurityException;
import core.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

import static core.bakedBeans.Operator.*;



/**
 * Helper methods for black sheep.
 *
 * @version 2024-10-01
 * @author lp
 */

public class ShepherdsDog implements Request.IRequestValidator, ObjectManager.IObject {

 private static final Logger logger = LogManager.getLogger(ShepherdsDog.class);

 private static final ResourceBundle rbl =
  MergedPropertyResourceBundle.getMergedBundle("LoginWorker");

 private static final long maxLoginFailures =
  Utilities.getParam(rbl, "maxLoginFailures", 3);

 private static final ResourceBundle rb = MergedPropertyResourceBundle.getMergedBundle("Shepherd");
 private static final long maxSecurityExceptions =
  Utilities.getParam(rb, "maxSecurityExceptions", 3);

 @WorkerHelper()
 private LoginHelper loginHelper;



 /**
  * Gets the current login from the request.
  *
  * @return The Login
  */

 public Blacksheep fetchBlackSheep(SecurityException ex, Integer userId) {

  Request req = Request.get();
  DataStore<Blacksheep> herd;

  if (!Utilities.isEmpty(userId))
   // Try to find a LoginException for user.
   herd = DataStoreManager.get().makeDataStore(
    "blacksheep.fetchForUser",
    new Object[] {
     userId,
     ex.getClass().getName()
    }
   );
  else
   herd = DataStoreManager.get().makeDataStore(
    "blacksheep.fetchForSessionId",
    new Object[] {
     req.getSessionId(),
     ex.getClass().getName()
    }
   );

  return
   getBlacksheep(ex, userId, req, herd);

 }



 /**
  * Deletes all Blacksheep with reason "LoginException" for the given user.
  *
  * @param userId The user id.
  */

 void resetLoginException(Integer userId) {

  DBConnection con = DB.get().getConnection();

  PreparedStatement stmt = con.prepareStatement("delete from blacksheep where user_id=?");

  try {
   stmt.setObject(1, userId);

   stmt.execute();
  }
  catch (SQLException e) {
   throw new MCException(e);
  }

  DB.get().releaseConnection(con);

  DataStoreManager.get().signal(DataStore.DataStoreSignal.delete, "blacksheep");

 }



 /**
  * Checks if user is Blacksheep with reason "LoginException".
  *
  * @param userId The user id.
  * @param count The maximum count for the reason.
  */

 void checkUser(Integer userId, int count) {

  DataStore<Blacksheep> herd = DataStoreManager.get().makeDataStore(
   "blacksheep.fetchForUser",
   new Object[] {
    userId,
    "application.exceptions.LoginException"
   }
  );

  Blacksheep bs = herd.readBean();

  if (!bs.isEmpty()  &&  bs.getCounter() >= count)  // Too much login failures.
   throw new AccountLockedException("AccountLocked");

 }



 /**
  * Checks if session is black sheep with reason "LoginException".
  *
  * @param sessionId The session id to check
  * @param count The maximum count for the reason.
  */

 void checkSession(String sessionId, int count) {

  DataStore<Blacksheep> herd = DataStoreManager.get().makeDataStore(
   "blacksheep.fetchForSessionId",
   new Object[] {
    sessionId,
    "application.exceptions.LoginException"
   }
  );

  Blacksheep bs = herd.readBean();

  if (!bs.isEmpty()  &&  bs.getCounter() >= count)  // Too much login failures.
   throw new SessionLockedException("SessionLocked");

 }



 /**
  * Increments the failure counter of the given Blacksheep.
  *
  * @param bs The Blacksheep.
  */

 public void incCounter(Blacksheep bs) {

  int id = bs.getCounter();
  id ++;
  bs.setCounter(id);

  bs.setLastseen(LocalDateTime.now());

  bs.save();
  DataStoreManager.get().signal(DataStore.DataStoreSignal.write, "blacksheep");

 }



 private Blacksheep getBlacksheep(SecurityException ex, Integer userId, Request req, DataStore<Blacksheep> herd) {

  Blacksheep res = herd.readBean();

  if (res.isEmpty()) {
   res = new Blacksheep();
   LocalDateTime val = LocalDateTime.now();
   res.setCreated(val);
   res.setLastseen(val);
   res.setAddress(req.getIpAddress());
   res.setPort(req.getPort());
   res.setSessionId(req.getSessionId());
   res.setCounter(0);
   res.setReason(ex.getClass().getName());
   res.setUserId(userId);

   Login login = loginHelper.getLogin();
   Client client = login.getRefClient();
   if (!Utilities.isEmpty(client))
    res.setRefClient(client);
  }

  return res;

 }



 private static boolean checkIpAddress(String ipAddress) {

  // Check if ip address lead to LoginException in the past.

  AtomicInteger mfl = new AtomicInteger(0);

  DataStoreManager.get().dataStore(JDBCDataStore.class, "blacksheep")
   .resetable()
   .loadWith("reason", "address", "counter")
   .where(
     AND(
      EQUAL("reason", "application.exceptions.LoginException"),
      EQUAL("address", ipAddress),
      GE("counter", maxLoginFailures)
     )
   )
   .make(Blacksheep.class)
   .forEach(bs -> {
    if (bs.getCounter() >= maxLoginFailures)
     mfl.incrementAndGet();
   });

   if (mfl.get() > maxLoginFailures)
    return true;  // Sorry, too much LoginException.

  // Check for other SecurityException.

  mfl.set(0);

  DataStoreManager.get().dataStore(JDBCDataStore.class, "blacksheep")
   .resetable()
   .loadWith("reason", "address", "counter")
   .where(
     AND(
     EQUAL( "address", ipAddress),
     EQUAL(
      "reason",
      "core.exceptions.SecurityException",
      "core.exceptions.BeanAccessException"
     )
    )
   )
   .make(Blacksheep.class)
   .forEach(bs -> mfl.addAndGet(bs.getCounter()));

  if (mfl.get() > maxSecurityExceptions)
   return true;  // Sorry, too much other SecurityException.

  return false;  // Request is ok.

 }



 @Override
 public boolean isValid(Request req) {

  try {
   if (checkIpAddress(req.getIpAddress())) {
    logger.warn("isValid(): Requests ip address is a black sheep!");
    return false;  // ip address is a black sheep.
   }
  } catch (Exception e) {
   logger.error("isValid(): Exception during check of request:\n\n" + e);
  }

  return true;

 }

}
