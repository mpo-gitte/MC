package application.workers;

import core.exceptions.MCException;
import application.beans.RestrictionSource;
import baked.application.beans.*;
import baked.application.beans.Record;
import baked.application.beans.Musicianalias;
import core.bakedBeans.*;
import core.base.WorkerManager;
import core.util.*;
import core.util.Utilities;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

import static core.bakedBeans.Operator.*;


/**
 * Methods for managing musicians.
 *
 * @version 2024-11-14
 * @author lp
 *
*/

public class MusiciansWorkerImpl implements WorkerManager.IWorker {

 private static Logger logger;
 private static DataStoreManager dataStoreManager;
 @Property("MusicianWorker.Watcher.schedule")
 private static String watcherSchedule = "0 0 21 * * ?";
 @Property("MuscianWorker.Watcher.age")
 private static int age = 600000;

 @WorkerHelper()
 private LogbookHelper logbookHelper;
 @WorkerHelper()
 private SearchEngineHelper searchEngineHelper;



 /**
  * When a worker gets loaded the WorkerManager will call this method.
  *
  * <p>The default implementation is empty.</p>
  *
  * @see WorkerManager
  */

 @Override
 public void onLoad() {


  // TimerJob for deleting "unemployed" musicians.

  Timer.get().run("MusicianWorker.UnemployedWatcher", watcherSchedule, true, () -> {

   logger.debug("UnemployedWatcher: Removing unemployed musicians.");

   try {
    DataStore<Musician> ds = dataStoreManager.makeDataStore(
     "musicianWorker.deleteMusicians",
     new Object[]{
      Utilities.todaysBeginning()
     }
    );

    ds.forEach(mus -> {
     try {
      if (logger.isDebugEnabled())
       logger.debug("UnemployedWatcher: Deleting musician: " + mus.getId());

      // Put some information into the logbook.
      logbookHelper.writeLogbook(null, null, mus, null, LogbookEntry.verbs.deleteSystem, null);

      removeMusician(mus);

      // Tell the world about it.
      dataStoreManager.signal(DataStore.DataStoreSignal.delete, "musicians", "jobs", "instrumentscount", "statisticsCount");

     } catch (Exception e) {
      logger.error("UnemployedWatcher: Can't handle musician!");
     }
    });
   } catch (Exception e) {
    logger.error("UnemployedWatcher: Can't select musicians!");
   }

  });

 }



 /**
  * Saves a musician.
  *
  * @param mus The musician.
  *
  * @throws MCException If musician is already existing.
  *
  */

 public void save(Musician mus) {

  if (mus.isUniqueOther()) {

   searchEngineHelper.reCreateIndex(mus);

   mus.save();

   dataStoreManager
    .signal(DataStore.DataStoreSignal.write, mus, "musicians", "comusicians", "statisticsCount")
    .signal(DataStore.DataStoreSignal.write, "jobs", "instrumentscount");

   writeLogbook(mus, LogbookEntry.verbs.write);
  }
  else
   throw new MCException("MusicianExisting");

 }



 /**
  * Deletes a musician.
  *
  * @param mus The musician.
  */

 public void delete(Musician mus) {

  if (logger.isDebugEnabled())
   logger.debug("delete(): deleting musician: " + mus.getId());

  writeLogbook(mus, LogbookEntry.verbs.delete);

  removeMusician(mus);

  dataStoreManager.signal(DataStore.DataStoreSignal.delete, mus, "musicians");
  dataStoreManager.signal(DataStore.DataStoreSignal.delete, "jobs", "instrumentscount", "statisticsCount");

 }



 private void removeMusician(Musician mus) {

  logbookHelper.correctLogbook(mus);

  mus.getColMusicianAliases().forEach(musicianalias -> {
   searchEngineHelper.deleteIndex(musicianalias);
   musicianalias.delete();
  });

  searchEngineHelper.deleteIndex(mus);

  mus.delete();

 }



 /**
  * This method returns a list of Instrument beans for instruments played by this musician.
  *
  * @param mus The musician.
  * @param res A RestrictionSource for restricting access.
  *
  * @return A list of instruments played by this musician.
  */

 public List<Instrument> getInstruments(Musician mus, RestrictionSource res) {

  return
   getInstrumentsDS(mus, res).read();

 }



 /**
  * This method returns a DataStore of Instrument beans for instruments played by this musician.
  *
  * @param mus The musician.
  * @param res A RestrictionSource for restricting access.
  *
  * @return A list of instruments played by this musician.
  */

 public DataStore<Instrument> getInstrumentsDS(Musician mus, RestrictionSource res) {

  if (logger.isDebugEnabled())
   logger.debug("getInstrumentsDS() for musician " + mus.getId());

  return
   dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore",
    "musicians",
    "select distinct instruments.id, instruments.description " +
     "from instruments, jobs where jobs.musician_id=? " +
     "and jobs.title_id in (select id from titles where record_id in (select id from records where " +
     makeResClause("records.medium_id", mus, res, false) + ")) " +
     "and instruments.id = jobs.instrument_id order by instruments.id",
    new Object[] {mus.getId()},
    Instrument.class
   );

 }



 private String makeResClause(String what, Musician mus, RestrictionSource res, boolean flag) {

  List<? extends PersistentBean> resBeans = res.getMediaRestriction(mus, RestrictionSource.Purpose.SelectingRecords);

  if (resBeans.size() == 0) return "";  // Return an empty string if no restrictions.

  StringBuilder sb = new StringBuilder();

  String sep = (flag ? " and " : " ") + what + " in ('";
  for (PersistentBean resBean : resBeans) {
   sb.append(sep).append(((Media)resBean.get("subMedia")).get("id")).append("'");
   sep = ",'";
  }
  sb.append(")");

  return sb.toString();

 }



 /**
  * This method returns a DataStore filled with CoMusicians for the given musician.
  *
  * @param mus The musician
  * @param res A RestrictionSource for restricting access.
  *
  * @return An DataStore with Musician beans.
  */

 public DataStore<Musician> getCoMusiciansDS(Musician mus, RestrictionSource res) {

  Integer he = mus.getId();

  logger.debug(() -> "getCoMusiciansDS(): for musician " + he);

  return
   dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore",
    "comusicians",
    "select musicians.id, musicians.name, musicians.instrument_id from musicians " +
     "where musicians.id != ? and musicians.id in " +
     "(select musician_id from jobs where title_id in (select id from titles " +
     "where id in (select title_id from jobs where musician_id=?) " +
     "and record_id in (select id from records where " +
     makeResClause("records.medium_id", mus, res, false) + "))) " +
     "order by musicians.name, musicians.instrument_id",
    new Object[] {he, he},
    Musician.class
   );

 }



 /**
  * Returns  a list of records on which this musicians plays together with the given musician.
  *
  * @param mus The musiscian
  * @param comus A Musician.
  * @param res A RestrictionSource.
  *
  * @return A list of records.
  *
  * @see Record
  */

 public List<Record> getRecordsWithCoMusician(Musician mus, Musician comus, RestrictionSource res) {

  return
   dataStoreManager.makeDataStore(
   "application.beans.ResetDataStore",
   "comusicians",
   "select id, created, interpreter, title, releaseyear, number, medium_id, label, ordernumber " +
    " from records " +
    "where id in (select record_id from titles where id in " +
    "(select title_id from jobs where musician_id=? and title_id in " +
    "(select title_id from jobs where musician_id=?)))" + makeResClause("records.medium_id", mus, res, true),
    new Object[] {comus.getId(), mus.getId()}, Record.class
   ).read();

 }



 /**
  * Returns  a list of titles on which this musicians plays together with the given musician.
  *
  * @param mus The musiscian
  * @param comus A Musician.
  * @param res A RestrictionSource.
  *
  * @return A list of titles.
  *
  * @see Title
  */

 public List<Title> getTitlesWithCoMusician(Musician mus, Musician comus, RestrictionSource res) {

  return
   dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore",
    "comusicians",
    "select titles.id, titles.interpreter, titles.title, duration, " +
     "record_id sr$id, records.interpreter sr$interpreter, records.title sr$title " +
     "from titles, records " +
     "where titles.id in (select title_id from jobs where musician_id=? and title_id in " +
     "(select title_id from jobs where musician_id=?)) and records.id in (select id from records where" +
     makeResClause("records.medium_id", mus, res, false) +
     ") and records.id=titles.record_id order by titles.title",
    new Object[] {comus.getId(), mus.getId()}, Title.class
   ).read();

 }



 /**
  * Returns a list with titles on which the given musician plays the given instrument.
  *
  * @param mus The musician.
  * @param instr The instrument.
  *              If empty all title on which this musician appears will be returned
  * @param res A RestrictionSource.
  *
  * @return A list of titles.
  *
  * @see Title
  */

 public List<Title> getTitlesWithInstrument(Musician mus, Instrument instr, RestrictionSource res) {

  if (logger.isDebugEnabled())
   logger.debug("getTitlesWithInstrument(): for musician " + mus.getId() + " playing " + instr.getId());

  StringBuilder sel = new StringBuilder();

  sel.append("select ");

  String iid = instr.getId();

  if (Utilities.isEmpty(iid))
   sel.append("distinct ");

  sel.append(
   "titles.id, titles.interpreter, titles.title, titles.duration, records.id sr$id, records.interpreter sr$interpreter, records.title sr$title").append(
   " from jobs, titles, records where jobs.musician_id=?"
  );

  if (!core.util.Utilities.isEmpty(iid))
   sel.append(" and jobs.instrument_id=?");
  else
   sel.append(" and jobs.instrument_id is not null");  // ToDo: Check why this is necessary for Adabas.

  sel.append(
   " and titles.id=jobs.title_id and records.id=titles.record_id order by titles.interpreter"
  );

  Object[] pars;
  if (Utilities.isEmpty(iid))
   pars = new Object[] {mus.getKeyValue()};
  else
   pars = new Object[] {mus.getKeyValue(), iid};

  return dataStoreManager.makeDataStore(
   "application.beans.ResetDataStore",
   "jobs",
   sel.toString(),
   pars,
   Title.class
  ).read();

 }



 /**
  * This method returns a list of Record beans with records on which the given musician appears.
  *
  * @param mus The musician
  * @param instr Instrument. May be null for all instruments.
  * @param res A RestrictionSource.
  * @return A list of records.
  */

 public List<Record> getRecords(Musician mus, Instrument instr, RestrictionSource res) {

  return
   getRecordsDS(mus, instr, res).read();

 }



 /**
  * This method returns a DataStore with records on which the given musician appears.
  *
  * @param mus The musician
  * @param instr Instrument. May be null for all instruments.
  * @param res A RestrictionSource.
  * @return A list of records.
  */

 public DataStore<Record> getRecordsDS(Musician mus, Instrument instr, RestrictionSource res) {

  boolean noInstr = Utilities.isEmpty(instr);

  if (logger.isDebugEnabled())
   logger.debug("getRecordsDS(): for musician " + mus.getId() + (noInstr ? "" : " playing " + instr.getId()));

  return
   dataStoreManager.makeDataStore(
    "application.beans.ResetDataStore",
    "records",
    "select distinct * from records " +
     "where id in " +
     "(select record_id from titles where id in " +
     "(select title_id from jobs where musician_id=? " + (noInstr ? "))" : "and instrument_id=?))") +
     makeResClause("records.medium_id", mus, res, true) +
     " order by records.interpreter, records.releaseyear",
    noInstr ? new Object[] {mus.getId()} : new Object[] {mus.getId(), instr.getId()},
    Record.class
   );

 }



 /**
  * Returns a list of musicians ordered by birthdays month, birthdays date and name.
  *
  * @return The order list of musicians.
  */

 public List<Musician> getBirthdayCalendar() {

  logger.debug("getBirthdayCalendar()");

  DataStore<Musician> ds = dataStoreManager.makeDataStore(
   "musiciansWorker.getBirthdayCalendar",
   new Object[] {
    application.beans.Musician.BITMASK_APPEARS_IN_BIRTHDAY_CALENDAR
   }
  );

  return ds.read();

 }



 /**
  * Returns a list of musicians ordered by birthdays month, birthdays date and name.
  *
  * @return The ordered list of musicians.
  */

 public BeanList<BirthdayCalendarEvent> getBirthdayCalendarFromNow(@LocaleConfig() Locale locale) {

  logger.debug(() -> "getBirthdayCalendarFromNow(): using locale: " + locale);

  return
   dataStoreManager.dataStore(ListDataStore.class, "musicians")
    .resetable()
    .sourceList((o, c) -> createBirthdayCalendar(Utilities.beginOfThisWeek(locale)))
    .taggedWith(locale)
    .make(BirthdayCalendarEvent.class)
    .read();

 }



 private BeanList<BirthdayCalendarEvent> createBirthdayCalendar(LocalDate thisWeek) {

  logger.debug(
   () -> "getBirthdayCalendarFromNow(): Generating new birthday calendar for beginning of this week: \"" + thisWeek.toString() + "\""
  );

  BeanList<BirthdayCalendarEvent> res = new BeanList<>(BirthdayCalendarEvent.class);
  int thisYear = LocalDateTime.now().getYear();

   // Build list of BirthdayCalendarEvent

  dataStoreManager.dataStore(JDBCDataStore.class, "musicians")
   .loadWith("id", "name", "born", "died", "instrumentId")
   .resetable()
   .where(
    AND(
     GIVEN("born"),
     ISTRUE("appearsInBirthdayCalendarMainMenu")
    )
   )
   .orderBy("name")
   .make(Musician.class)
   .forEach(mus -> {
     BirthdayCalendarEvent bce =  BeanFactory.make(BirthdayCalendarEvent.class);
     bce.setSubMusician(mus);
     LocalDate val = mus.getBorn().withYear(thisYear);
     bce.setEventDate(val);  // Trim to current year.
     res.add(bce);
    }
   );

   // Sort list.

   res.sort(
    new BeanComparator<application.beans.BirthdayCalendarEvent>(
     "eventDate",
     BeanComparator.Direction.ascending
    )
   );

  boolean thisWeekFlag = true;

  // Find first birthday after current week.

  for (BirthdayCalendarEvent bce : res) {
   LocalDate eventDate = bce.getEventDate();
   if ((eventDate.isEqual(thisWeek) || eventDate.isAfter(thisWeek))  &&  thisWeekFlag) {
    // Match!
    thisWeekFlag = false;
    bce.setThisWeek(true);
   }
  }

  return res;

 }



 private void writeLogbook(Musician mus, LogbookEntry.verbs verb) {

  logbookHelper.writeLogbook(null, null, mus, null, verb);

 }



 /**
  * Saves a musicianalias for a musician.
  *
  * @param mus The musician.
  *
  * @param musicianalias Its alias.
  */

 public void saveMusicianalias(Musician mus, Musicianalias musicianalias) {

  musicianalias.setMusicianId(mus.getId());
  musicianalias.setCreated(LocalDateTime.now());

  musicianalias.save();

  searchEngineHelper.reCreateIndex(musicianalias);

  dataStoreManager.signal(
   DataStore.DataStoreSignal.write,
   mus,
   "musicians"
  );

 }



 /**
  * Deletes a list of musician aliases.
  *
  * @param musicianaliases The list.
  */

 public void deleteMusicianalias(BeanList<Musicianalias> musicianaliases) {

  DBConnectionStore.getConnection();

  for (Musicianalias musa : musicianaliases)
   if (!core.bakedBeans.Utilities.isEmpty(musa)) {
    searchEngineHelper.deleteIndex(musa);
    musa.delete();
   }

  dataStoreManager.signal(
   DataStore.DataStoreSignal.delete,
   "musicians", "musicianalias"
  );

  DBConnectionStore.releaseConnection();

 }

}
