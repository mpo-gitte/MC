package application.workers;

import application.aspects.annotations.NeedsRight;
import baked.application.beans.*;
import core.base.WorkerManager;

import java.util.List;



public interface InstrumentsWorker extends WorkerManager.IWorker {

 public List<Instrument> getInstruments();

 public void addInstrument(List<Title> tits, Instrument instr, Musician mus);

 public void addInstruments(List<Title> tits, List<Job> newJobs);

 @NeedsRight("addinstruments")
 public void removeJob(List<Job> jobs);

 @NeedsRight("editInstruments")
 public List<Musician> selectMusicians(String str, Instrument instr);

 public List<Job> getUniqueJobsForTitles(List<Title> tits);

 public void exchange(User user, List<Title> tits, Musician mus, String pinstr, Musician omus, String oinstr);

 public int countInstrumentsOnJobs(Instrument instr);

 public int countInstrumentsOnMusicians(Instrument instr);

 public boolean enableDelete(Instrument instr);

 @NeedsRight("editInstruments")
 public void save(Instrument instr, boolean asNew);

 public boolean checkUniqueInstrument(String instrid);

 @NeedsRight("editInstruments")
 public void deleteInstrument(List<Instrument> instrs);

 public List<Musician> musiciansPlayingInstrument(Instrument instr);

}
