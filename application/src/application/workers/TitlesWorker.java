package application.workers;

import application.aspects.annotations.NeedsRight;
import baked.application.beans.*;
import baked.application.beans.Record;
import core.aspects.annotations.Transaction;
import core.base.WorkerManager;

import java.util.List;



public interface TitlesWorker extends WorkerManager.IWorker {

 public List<TitleReference> getTemporaryResults();

 public void deleteTemporaryResults();

 public void save(Record rec, Title tit);

 @NeedsRight("addtitles")
 public List<Title> selectInterpreters(String interpreter);

 @NeedsRight("addtitles")
 public List<Title> selectTitles(String title);

 @NeedsRight("addtitles")
 public List<Title> selectComposers(String composer);

 @NeedsRight("addtitles")
 public List<Title> selectProducers(String producer);

 @NeedsRight("addtitles")
 public List<Title> selectLocations(String location);

 public Title getTemplateTitle(Record rec);

 @NeedsRight("addtitles")
 public void addTitle(Record rec, Title tit);

 @NeedsRight("addtitles")
 @Transaction()
 public void deleteTitles(List<Title> tits);

 @NeedsRight("sort")
 public SettingsOrderBy getOrderBy(Title tit, User user);

 @NeedsRight("sort")
 public SettingsOrderBy orderBy(Title tit, User user, String property);

}
