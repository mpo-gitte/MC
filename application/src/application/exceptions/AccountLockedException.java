package application.exceptions;

import core.exceptions.SecurityException;


/**
 * This exception is thrown when a user account must be locked.
 *
 * @version 2019-02-07
 * @author pilgrim.lutz@imail..de
 */

public class AccountLockedException extends SecurityException {



 public AccountLockedException(String message) {

  super(message);

 }

}
