package application.exceptions;

import core.exceptions.MCException;

import java.util.function.Predicate;



/**
 * Exception for search faults.
 *
 * @version 2023-09-11
 * @author pilgrim.lutz@imail.de
 *
 */

public class SearchException extends MCException {



 /**
  * Standard constructor.
  *
  * @param mess error message
  */

 public SearchException(String mess) {

  super(mess);

 }



 /**
  * Factory method for creating a new MCException.
  *
  * @param condition Exception is only thrown if condition returns true.
  * @param args Arguments for the constructor of the exception.
  */

 public static void when(Predicate<Void> condition, Object ... args) {

  MCException.when(SearchException.class, condition, args);

 }

}
