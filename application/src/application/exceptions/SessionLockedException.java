package application.exceptions;

import core.exceptions.SecurityException;


/**
 * This exception is thrown when a session must be locked.
 *
 * @version 2019-02-07
 * @author pilgrim.lutz@imail..de
 */

public class SessionLockedException extends SecurityException {



 public SessionLockedException(String message) {

  super(message);

 }

}
