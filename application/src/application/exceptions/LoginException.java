package application.exceptions;

import core.exceptions.SecurityException;

/**
 * Exception for indicating problems during user login.
 * 
 * @version 2019-02-04
 * @author pilgrim.lutz@imai.lde
 *
 */

public class LoginException extends SecurityException {

 private Integer userId;



 /**
  * Creates a new default exception.
  *
  * @param mess A message for the exception. It's a code for access to an appropriate text.
  */

 public LoginException(String mess) {

  super(mess);

 }



 /**
  * Creates a new default exception.
  *
  * @param userId The id of the user who raised this exception.
  * @param mess A message for the exception. It's a code for access to an appropriate text.
  */

 public LoginException(Integer userId, String mess) {

  super(mess);

  this.userId = userId;

 }



 /**
  * Gets the user id.
  *
  * @return users id.
  */

 public Integer getUserId() {

  return userId;

 }

}
