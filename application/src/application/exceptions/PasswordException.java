package application.exceptions;

import core.exceptions.SecurityException;


/**
 * This exception is thrown when problems with passwords occur.
 *
 * @version 2018-11-26
 * @author pilgrim.lutz@imail..de
 */

public class PasswordException extends SecurityException {



 public PasswordException(String message) {

  super(message);

 }

}
