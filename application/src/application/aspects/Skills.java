package application.aspects;

import application.workers.FavouritesListsHelper;
import application.workers.LoginHelper;
import baked.application.beans.FavouritesList;
import baked.application.beans.Setting;
import core.bakedBeans.Utilities;
import core.util.Aspectator;
import core.util.WorkerHelper;

import static core.util.Aspectator.*;


/**
 * Aspect implements skills.
 *
 * @version 2024-08-09
 * @author lp
 */

public class Skills implements Aspectator.Aspect{

 @WorkerHelper()
 private LoginHelper loginHelper;




 /**
  * Skill for seeing New-FavouritesList.
  *
  * @param jp JoinPoint currently not used.
  *
  * @return TRUE if user may see and list exists and has entries from today.
  * False otherwise.
  */

 @Around(
  matchedBy = MethodSelect.METHOD_HAS_ANNOTATION,
  value = ".*SkillNewFavouritesList.*"
 )
 public Object skillNewFavouritesList(JoinPoint jp) {

  if (!loginHelper.may("seefavouriteslists"))
   return Boolean.FALSE;  // User has no favouriteslists.

  FavouritesList newRecordsFavouritesList =
   FavouritesListsHelper.getNewRecordsFavouritesList(loginHelper.getUser());
  if (newRecordsFavouritesList.isEmpty())
   return
    Boolean.FALSE;  // User has no new list.

  return
   newRecordsFavouritesList
    .getColFavourites()
    .read()
    .stream()
    .filter(f -> Utilities.diffTimestamps(f.getCreated(), Utilities.todaysBeginning()) < 0)
    .count() > 0;  // Got any items from today?

 }



 /**
  * Skill for seeing Search index.
  *
  * @param jp JoinPoint currently not used.
  *
  * @return TRUE if user may see and list exists and has entries from today.
  * False otherwise.
  */

 @Around(
  matchedBy = MethodSelect.METHOD_HAS_ANNOTATION,
  value = ".*SkillIndexSearch.*"
 )
 public Object skillndexSearch(JoinPoint jp) {

  if (!loginHelper.may("indexSearch"))
   return Boolean.FALSE;  // User doesn't have index search.

  if ("TRUE".equals(Setting.getSetting("INDEXREADY")))
   return true;

  return false;

 }

}
