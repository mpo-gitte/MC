package application.aspects.annotations;

import java.lang.annotation.*;


/**
 * Triggers Aspect
 *
 * @version 2018-08-06
 * @author pilgrim.lutz@imail.de
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface NeedsRight {

 String value();

}
