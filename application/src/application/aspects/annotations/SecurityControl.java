package application.aspects.annotations;

import java.lang.annotation.*;


/**
 * Triggers Aspect SecurityControl
 *
 * @see SecurityControl
 *
 * @version 2019-02-03
 * @author pilgrim.lutz@imail.de
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface SecurityControl {

}
