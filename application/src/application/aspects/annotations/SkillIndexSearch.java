package application.aspects.annotations;

import application.aspects.Skills;
import core.util.Aspectator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Annotation to trigger the skill IndexSearch.
 *
 * @version 2024-06-09
 * @author lp
 *
 * @see Skills#skillNewFavouritesList(Aspectator.JoinPoint)
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface SkillIndexSearch {
}
