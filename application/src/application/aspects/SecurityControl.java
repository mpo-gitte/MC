package application.aspects;


import application.exceptions.LoginException;
import application.workers.ShepherdsDog;
import core.exceptions.SecurityException;
import core.util.Aspectator;
import core.util.Aspectator.Around;
import core.util.Aspectator.JoinPoint;
import core.util.Aspectator.MethodSelect;
import core.util.Utilities;
import core.util.WorkerHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



/**
 * Aspects for security control.
 *
 * @version 2013-09-13
 * @author lp
 */

public class SecurityControl implements Aspectator.Aspect {

 private static final Logger logger = LogManager.getLogger(SecurityControl.class);

 @WorkerHelper()
 private ShepherdsDog wuff;



 /**
  * Watch for SecurityExceptions thrown by methods.
  *
  * <p>Triggered by Annotation CatchException.</p>
  *
  * @param jointPoint The join point
  */

 @Around(
  matchedBy = MethodSelect.METHOD_HAS_ANNOTATION,
  value = ".*SecurityControl.*",
  order = 100
 )
 public Object control(JoinPoint jointPoint) throws Exception {

  logger.debug(
   () -> "control(): controlling: \"" + jointPoint.getMethodSignature() + "\""
  );

  try {
   return jointPoint.proceed();
  }
  catch (Exception e) {
   Throwable ex = Utilities.getRealProblem(e);

   if (Utilities.isClassDerivedFrom(ex.getClass(), SecurityException.class)) {
    // Handling all kind of SecurityException.
    SecurityException exl = (SecurityException)ex;

    Integer uid = 0;
    if (exl.getClass() == LoginException.class) {
     // Special case: SecurityException is a LoginException.
     LoginException exls = (LoginException)exl;
     uid = exls.getUserId();
     if (logger.isDebugEnabled())
      logger.debug("control(): got LoginException for uid " + uid + "!");
    }

    wuff.incCounter(wuff.fetchBlackSheep(exl, uid));

   }

   logger.error("control(): Pass through exception: " + e);

   throw e;

  }

 }



 /**
  * Watch for SecurityExceptions thrown by the BeanWorker.
  *
  * Triggered by method name.
  *
  * @param jointPoint The join point
  *
  * @see core.workers.BeanWorker
  */

 @Around(
  matchedBy = MethodSelect.METHOD_HAS_NAME,
  value = ".*BeanWorkerImpl\\.getParameterAsBean.*",
  order = 100
 )
 public Object controlBeanWorker(JoinPoint jointPoint) throws Exception {

  logger.debug(() -> "controlBeanWorker(): controlling: \"" + jointPoint.getMethodSignature() + "\"");

  try {
   return jointPoint.proceed();
  }
  catch (Exception e) {
   Throwable ex = Utilities.getRealProblem(e);

   if (Utilities.isClassDerivedFrom(ex.getClass(), core.exceptions.SecurityException.class)) {
    core.exceptions.SecurityException exl = (core.exceptions.SecurityException)ex;

    logger.debug("controlBeanWorker(): got a kind of SecurityException!");

    try {
     wuff.incCounter(wuff.fetchBlackSheep(exl, null));  // ToDo: Can't we get a user here?
    }
    catch (Exception e1) {
     logger.error("controlBeanWorker(): Can't fetch black sheep! Because: ", e1);
    }

   }
   logger.error("controlBeanWorker(): Pass through exception: " + e);
   throw e;
  }

 }

}
