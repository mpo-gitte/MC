package application.aspects;



import application.beans.Login;
import application.beans.User;
import application.aspects.annotations.NeedsRight;
import core.exceptions.SecurityException;
import core.util.Aspectator;
import core.util.Aspectator.Aspect;
import core.util.Aspectator.JoinPoint;
import core.util.Aspectator.MethodSelect;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;



/**
 * Aspect for checking necessary rights for worker methods.
 *
 * @version 2024-01-01
 * @author lp
 */

public class CheckRightsAspect implements Aspect {

 private static Logger logger = LogManager.getLogger(CheckRightsAspect.class.getName());



 /**
  * Aspect checks if the logged-in user has the given right to call the worker method.
  *
  * <p>The method is marked with the NeedsRight annotation which bears the
  * necessary right as its value.</p>
  *
  * @param joinPoint The join point (with the annotated method in it)
  * @see NeedsRight
  */

 @Aspectator.Before(matchedBy = MethodSelect.METHOD_HAS_ANNOTATION, value = ".*NeedsRight.*")
 public void checkRight(JoinPoint joinPoint) {

  Method m = joinPoint.getMethodInInterface();  // Normally the interface "bears" the annotations...
  NeedsRight annotation = m.getAnnotation(NeedsRight.class);
  String right;

  if (annotation != null)
   right = annotation.value();
  else {
   m = joinPoint.getMethod();  // ... but sometimes the annotations sticks to the method.
   annotation = m.getAnnotation(NeedsRight.class);
   if (annotation == null)
    logger.error("checkRight(): Can't find annotation!");  // Should not occur.

   right = annotation.value();

  }

  logger.debug(() -> "checking value \"" + right +
   "\" for method: \"" + joinPoint.getMethodSignature() + "\"");

  try {
   if (Login.get().may(right))
    return;  // Ok!
  }
  catch (Exception e) {
   throw new SecurityException(e);
  }

  String errmess =
   "Insufficient rights for method: \"" +
   joinPoint.getMethodSignature() + "\" " +
   "for session \"" +
   Login.get().getKeyValue() + "\" " +
   "for user \"" +
   ((User) Login.get().get("refUser")).get("loginname") + " (" +
   ((User) Login.get().get("refUser")).get("id") + ") ";

  logger.info(errmess);

  throw
   new SecurityException(errmess);

 }

}
