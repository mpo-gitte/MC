--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

ALTER TABLE coverpictures ALTER COLUMN created SET DEFAULT now();
