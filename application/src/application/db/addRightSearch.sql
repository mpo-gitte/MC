--liquibase formatted sql
--changeset author:lp

update roles set rights=rights||'search=true;' where id=3;
update roles set rights=rights||'search=true;' where id=2;
update roles set rights=rights||'search=true;' where id=1;
update roles set rights=rights||'search=false;' where id=0;
