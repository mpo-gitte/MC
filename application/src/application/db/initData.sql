--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

insert into media (id, created, description)
 values ( 'CD', null,  'Compact Disc');
insert into media (id, created, description)
 values ( 'CDR', null,  'Compact Disc write once');
insert into media (id, created, description)
 values ( 'LP', null,  'Langspielplatte');

insert into records (id, created, interpreter, title, label, ordernumber, recording, medium_id, number, releaseyear, comment)
 values (7, '1997-11-15 15:41:20.787109',  'Brown, James',  'The CD of JB',  'Polydor',  '825 714-2',  'AAD',  'CD', 1,  '1985',  'Datum der Zusammenstellung.');
insert into records (id, created, interpreter, title, label, ordernumber, recording, medium_id, number, releaseyear, comment)
 values (208, '1997-11-15 15:41:27.347161',  'Beatles',  'Greatest',  'Parlophone EMI',  'OMHS 3001',  '',  'LP', 1,  '????',  '');
insert into records (id, created, interpreter, title, label, ordernumber, recording, medium_id, number, releaseyear, comment)
 values (249, '1997-11-15 15:41:28.695272',  'Clapton, Eric',  'Backless',  'RSO (DGR)',  '2394 213',  '',  'LP', 1,  '1978',  '');
insert into records (id, created, interpreter, title, label, ordernumber, recording, medium_id, number, releaseyear, comment)
 values (1439, '2016-12-02 22:28:59.139301',  'Cohen, Leonard',  'You want it darker',  'Sony',  '88985365072',  '??D',  'CD', 1,  '2016',  '');
insert into records (id, created, interpreter, title, label, ordernumber, recording, medium_id, number, releaseyear, comment)
 values (1307, '2013-05-25 18:23:36.988949',  'Burdon, Eric',  'Soul of a Man',  'SPV',  'SPV 78412 CD',  '??D',  'CD', 1,  '2006',  '');
insert into records (id, created, interpreter, title, label, ordernumber, recording, medium_id, number, releaseyear, comment)
 values (9, '1997-11-15 15:41:20.875613',  'Burdon, Eric',  'Starportrait',  'Polydor',  '825 231-2',  'AAD',  'CD', 2,  '????',  'Produktionen verschiedener Jahre  da Sampler.');
insert into records (id, created, interpreter, title, label, ordernumber, recording, medium_id, number, releaseyear, comment)
 values (237, '1997-11-15 15:41:28.310636',  'Burdon, Eric & War',  'The Black Mans Burdon',  'MGM',  'SE-4710-2',  'AAA',  'LP', 2,  '1971',  'US-Pressung');
insert into records (id, created, interpreter, title, label, ordernumber, recording, medium_id, number, releaseyear, comment)
 values (1308, '2013-05-25 18:24:40.291209',  'Burdon, Eric',  'Til your River runs dry',  'ABKCO Music & Records',  '8906-2',  '??D',  'CD', 1,  '2013',  '');

insert into roles (id, created, name, rights)
 values (0, null,  'guest',  'readrecords=true;readtitles=true;seebrowserinfo=true;seeTagCloud=true');
insert into roles (id, created, name, rights)
 values (1, null,  'user',  'readrecords=true;readtitles=true;readinstruments=true;readmusicians=true;getdiscography=true;seebrowserinfo=true;savesearchpatterns=true;namesearchpatterns=true;searchempty=true;autologin=true');
insert into roles (id, created, name, rights)
 values (2, null,  'userplus',  'readrecords=true;editrecords=true;addrecords=true;readtitles=true;edittitles=true;addtitles=true;readinstruments=true;readmusicians=true;getdiscography=true;seebrowserinfo=true;seedbinfo=true;seememinfo=true;seedatastoreparams=true;deletedatastores=true;savesearchpatterns=true;namesearchpatterns=true;searchempty=true;autologin=true;seelogbook=true;seelogbookothers=true;deletelogbookentries=true;seefavouriteslists=true;seefavouriteslistsothers=true;seestatisticscount=true;seestatisticstopissuers=true;seestatisticstopmusicians=true;seestatisticsunemployedmusicians=true;seestatisticsaddition=true;addinstruments=true;editmusicians=true;seenewmark=true;seestatisticsunemployedmusicians=true;complexsearch=true;seenewmark=true;searchForNoChildren=true;generateId3Tags=true;edIstr=true;dDa=true;');

insert into users (id, created, loginname, password, firstname, lastname, email, role_id)
 values (0, null,  'guest',  'guest',  'anonymous',  'guest', null, 0);
insert into users (id, created, loginname, password, firstname, lastname, email, role_id)
 values (1, null,  'mcmaster',  'mcmaster1',  'MC',  'Master', null, 2);

insert into favouriteslists (id, created, user_id, name, purpose)
 values (1, '2009-12-06 19:31:44.764', 1,  'Brand new', 0);

insert into favourites (id, created, favouriteslist_id, record_id, info)
 values (1, '2009-12-13 21:32:11.952', 1, 1308, 'Ok');

insert into titles (id, created, record_id, interpreter, title, duration, composer, recordingdate, location, producer, side, track)
 values (57, '2008-04-14 20:40:42.464273', 7,  'Brown, James',  'Doing it to Death',  '5:23',  'Brown, James',  '29.01.1973',  'Soundcraft Studios, North Augusta, S.C.',  'Brown, James', 1, 1);
insert into titles (id, created, record_id, interpreter, title, duration, composer, recordingdate, location, producer, side, track)
 values (58, '2008-04-14 20:41:44.335512', 7,  'Brown, James',  'Super bad',  '2:59',  'Brown, James',  '30.06.1970',  'Starday-King Studios, Nashville, Tennessee',  'Brown, James', 1, 2);
insert into titles (id, created, record_id, interpreter, title, duration, composer, recordingdate, location, producer, side, track)
 values (59, '2008-04-14 20:42:37.316969', 7,  'Brown, James',  'Soul Power',  '3:01',  'Brown, James',  '26.01.1971',  'Rodel Studios, Washington D.C.',  'Brown, James', 1, 3);

insert into titles (id, created, record_id, interpreter, title, duration, composer, recordingdate, location, producer, side, track)
 values (15583, '2016-02-26 22:17:31.739', 1439,  'Cohen, Leonard',  'You want it darker',  '',  'Cohen, Leonard & Leonard, Patrick',  '',  '',  'Cohen, Adam', 1, 1);
insert into titles (id, created, record_id, interpreter, title, duration, composer, recordingdate, location, producer, side, track)
 values (15584, '2016-02-26 22:17:31.74', 1439,  'Cohen, Leonard',  'Treaty',  '',  'Cohen, Leonard',  '',  '',  'Cohen, Adam', 1, 2);
insert into titles (id, created, record_id, interpreter, title, duration, composer, recordingdate, location, producer, side, track)
 values (15585, '2016-02-26 22:17:31.741', 1439,  'Cohen, Leonard',  'On the Level',  '',  'Cohen, Leonard & Robinson, Shannon',  '',  '',  'Cohen, Adam', 1, 3);
insert into titles (id, created, record_id, interpreter, title, duration, composer, recordingdate, location, producer, side, track)
 values (15586, '2016-02-26 22:17:31.742', 1439,  'Cohen, Leonard',  'Leaving the Table',  '',  'Cohen, Leonard',  '',  '',  'Cohen, Adam', 1, 4);
insert into titles (id, created, record_id, interpreter, title, duration, composer, recordingdate, location, producer, side, track)
 values (15587, '2016-02-26 22:17:31.743', 1439,  'Cohen, Leonard',  'If I didn\0027t have your Love',  '',  'Cohen, Leonard & Leonard, Patrick',  '',  '',  'Cohen, Adam', 1, 5);
insert into titles (id, created, record_id, interpreter, title, duration, composer, recordingdate, location, producer, side, track)
 values (15588, '2016-02-26 22:17:31.744', 1439,  'Cohen, Leonard',  'Travelling light',  '',  'Cohen, Leonard & Leonard, Patrick & Cohen, Adam',  '',  '',  'Cohen, Adam', 1, 6);
insert into titles (id, created, record_id, interpreter, title, duration, composer, recordingdate, location, producer, side, track)
 values (15589, '2016-02-26 22:17:31.745', 1439,  'Cohen, Leonard',  'It seemed the better Way',  '',  'Cohen, Leonard & Leonard, Patrick',  '',  '',  'Cohen, Adam', 1, 7);
insert into titles (id, created, record_id, interpreter, title, duration, composer, recordingdate, location, producer, side, track)
 values (15590, '2016-02-26 22:17:31.746', 1439,  'Cohen, Leonard',  'Steer your Way',  '',  'Cohen, Leonard',  '',  '',  'Cohen, Adam', 1, 8);
insert into titles (id, created, record_id, interpreter, title, duration, composer, recordingdate, location, producer, side, track)
 values (15591, '2016-02-26 22:17:31.747', 1439,  'Cohen, Leonard',  'String Reprise / Treaty',  '',  'Cohen, Leonard',  '',  '',  'Cohen, Adam', 1, 9);

insert into instruments (id, created, description)
 values ( 'acc', null,  'accordion');
insert into instruments (id, created, description)
 values ( 'acl', null,  'alto clarinet');
insert into instruments (id, created, description)
 values ( 'afl', null,  'alto flute');
insert into instruments (id, created, description)
 values ( 'arr', null,  'arranger');
insert into instruments (id, created, description)
 values ( 'as', null,  'alto saxophone');
insert into instruments (id, created, description)
 values ( 'b', null,  'bass');
insert into instruments (id, created, description)
 values ( 'bacl', null,  'bass clarinet');
insert into instruments (id, created, description)
 values ( 'bagp', null,  'bagpipe');
insert into instruments (id, created, description)
 values ( 'bassoon', null,  'bassoon');
insert into instruments (id, created, description)
 values ( 'batr', null,  'bass trombone');
insert into instruments (id, created, description)
 values ( 'bfl', null,  'bass flute');
insert into instruments (id, created, description)
 values ( 'bj', null,  'banjo');
insert into instruments (id, created, description)
 values ( 'bnd', null,  'bandoneon');
insert into instruments (id, created, description)
 values ( 'boz', null,  'bozouki');
insert into instruments (id, created, description)
 values ( 'bs', null,  'baritone saxophone');
insert into instruments (id, created, description)
 values ( 'bvoc', null,  'backing vocal');
insert into instruments (id, created, description)
 values ( 'cbassoon', null,  'contra bassoon');
insert into instruments (id, created, description)
 values ( 'ce', null,  'cello');
insert into instruments (id, created, description)
 values ( 'cel', null,  'celeste');
insert into instruments (id, created, description)
 values ( 'cl', null,  'clarinet');
insert into instruments (id, created, description)
 values ( 'co', null,  'cornet');
insert into instruments (id, created, description)
 values ( 'comp', null,  'composer');
insert into instruments (id, created, description)
 values ( 'cond', null,  'conductor');
insert into instruments (id, created, description)
 values ( 'dr', null,  'drums');
insert into instruments (id, created, description)
 values ( 'dul', null,  'dulcimer');
insert into instruments (id, created, description)
 values ( 'enh', null,  'english horn');
insert into instruments (id, created, description)
 values ( 'fl', null,  'flute');
insert into instruments (id, created, description)
 values ( 'flh', null,  'flugelhorn');
insert into instruments (id, created, description)
 values ( 'frh', null,  'french horn');
insert into instruments (id, created, description)
 values ( 'g', null,  'guitar');
insert into instruments (id, created, description)
 values ( 'harm', null,  'harmonica');
insert into instruments (id, created, description)
 values ( 'harmo', null,  'harmonium');
insert into instruments (id, created, description)
 values ( 'harp', null,  'harp');
insert into instruments (id, created, description)
 values ( 'horn', null,  'horn');
insert into instruments (id, created, description)
 values ( 'keyb', null,  'keyboards');
insert into instruments (id, created, description)
 values ( 'ld', null,  'leader');
insert into instruments (id, created, description)
 values ( 'man', null,  'mandolin');
insert into instruments (id, created, description)
 values ( 'oboe', null,  'oboe');
insert into instruments (id, created, description)
 values ( 'org', null,  'organ');
insert into instruments (id, created, description)
 values ( 'p', null,  'piano');
insert into instruments (id, created, description)
 values ( 'pcl', null,  'piccolo clarinet');
insert into instruments (id, created, description)
 values ( 'perc', null,  'percussion');
insert into instruments (id, created, description)
 values ( 'prog', null,  'programming');
insert into instruments (id, created, description)
 values ( 'sax', null,  'saxophone');
insert into instruments (id, created, description)
 values ( 'sit', null,  'sitar');
insert into instruments (id, created, description)
 values ( 'ss', null,  'soprano saxophone');
insert into instruments (id, created, description)
 values ( 'synth', null,  'synthesizer');
insert into instruments (id, created, description)
 values ( 'tp', null,  'trumpet');
insert into instruments (id, created, description)
 values ( 'tr', null,  'trombone');
insert into instruments (id, created, description)
 values ( 'ts', null,  'tenor saxophone');
insert into instruments (id, created, description)
 values ( 'tu', null,  'tuba');
insert into instruments (id, created, description)
 values ( 'uk', null,  'Ukulele');
insert into instruments (id, created, description)
 values ( 'v', null,  'violin');
insert into instruments (id, created, description)
 values ( 'vib', null,  'vibraphone');
insert into instruments (id, created, description)
 values ( 'vio', null,  'viola');
insert into instruments (id, created, description)
 values ( 'voc', null,  'vocal');

insert into musicians (id, created, name, comment, born, died, instrument_id)
 values (49, '2000-01-05 09:10:08.209514',  'Wesley, Fred',  '',  '',  '',  'tr');
insert into musicians (id, created, name, comment, born, died, instrument_id)
 values (338, '2000-03-29 18:58:25.87434',  'Parker, Maceo',  '',  '',  '',  'sax');
insert into musicians (id, created, name, comment, born, died, instrument_id)
 values (1534, '2000-12-03 18:40:29.316434',  'Larsen, Neil',  '',  '',  '',  'keyb');

insert into jobs (id, created, title_id, musician_id, instrument_id)
 values (2535, '2008-04-14 21:02:18.693028', 57, 49,  'tr');
insert into jobs (id, created, title_id, musician_id, instrument_id)
 values (10042, '2008-04-14 21:03:06.555791', 57, 338,  'as');
insert into jobs (id, created, title_id, musician_id, instrument_id)
 values (2536, '2008-04-14 21:08:34.322755', 59, 49,  'tr');

insert into jobs (id, created, title_id, musician_id, instrument_id)
 values (32961, '2018-01-02 21:18:09.042', 15583, 1534,  'org');
insert into jobs (id, created, title_id, musician_id, instrument_id)
 values (32962, '2018-01-02 21:18:09.042', 15584, 1534,  'org');
insert into jobs (id, created, title_id, musician_id, instrument_id)
 values (32963, '2018-01-02 21:18:09.042', 15585, 1534,  'org');
insert into jobs (id, created, title_id, musician_id, instrument_id)
 values (32964, '2018-01-02 21:18:09.042', 15586, 1534,  'org');
insert into jobs (id, created, title_id, musician_id, instrument_id)
 values (32965, '2018-01-02 21:18:09.042', 15587, 1534,  'org');
insert into jobs (id, created, title_id, musician_id, instrument_id)
 values (32966, '2018-01-02 21:18:09.042', 15588, 1534,  'org');
insert into jobs (id, created, title_id, musician_id, instrument_id)
 values (32967, '2018-01-02 21:18:09.042', 15589, 1534,  'org');
insert into jobs (id, created, title_id, musician_id, instrument_id)
 values (32968, '2018-01-02 21:18:09.042', 15590, 1534,  'org');
insert into jobs (id, created, title_id, musician_id, instrument_id)
 values (32969, '2018-01-02 21:18:09.042', 15591, 1534,  'org');

insert into rolesmedia (id, medium_id, role_id)
 values (1,  'CD', 0);
insert into rolesmedia (id, medium_id, role_id)
 values (2,  'LP', 1);
insert into rolesmedia (id, medium_id, role_id)
 values (3,  'CD', 1);
insert into rolesmedia (id, medium_id, role_id)
 values (4,  'LP', 2);
insert into rolesmedia (id, medium_id, role_id)
 values (5,  'CD', 2);
insert into rolesmedia (id, medium_id, role_id)
 values (6,  'LP', 0);
