--liquibase formatted sql
--changeset author:lp

update roles set rights=new.newrights from (
 select replace(rights, 'seedbinfo=true;', '') newrights from roles where id=2
) as new
where id=2
