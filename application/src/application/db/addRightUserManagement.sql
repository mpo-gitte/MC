--liquibase formatted sql
--changeset author:lp

update roles set rights=rights||'umnt=false;' where id=3;
update roles set rights=rights||'umnt=true;' where id=2;
update roles set rights=rights||'umnt=false;' where id=1;
update roles set rights=rights||'umnt=false;' where id=0;
