--liquibase formatted sql
--changeset author:lp

create table musicianalias (
 id integer,
 created timestamp,
 musician_id integer,
 name varchar(128),
 reason varchar(128),
 primary key (id),
 foreign key (musician_id) references musicians (id)
);

CREATE INDEX musicianalias_name ON musicianalias (name);

create sequence musicianalias_id start with 1;
