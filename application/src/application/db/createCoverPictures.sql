--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

create table coverpictures (
 id integer,
 created timestamp,
 record_id integer not null,
 description varchar(200),
 origin varchar(200),
 filename varchar(200),
 nr integer,
 primary key (id),
 foreign key (record_id) references records (id)
);

create sequence coverpictures_id start with 1;

insert into coverpictures (id, record_id, description, origin, nr)
 values (nextval('coverpictures_id'),  249, 'Frontside', 'built in demo data', 1);
insert into coverpictures (id, record_id, description, origin, nr)
 values (nextval('coverpictures_id'),  1307, 'Frontside', 'built in demo data', 1);
insert into coverpictures (id, record_id, description, origin, nr)
 values (nextval('coverpictures_id'),  1308, 'Frontside', 'built in demo data', 1);
insert into coverpictures (id, record_id, description, origin, nr)
 values (nextval('coverpictures_id'),  1439, 'Frontside', 'built in demo data', 1);
