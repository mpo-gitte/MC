--liquibase formatted sql
--changeset author:lp

create table settings (
 id integer,
 created timestamp,
 user_id integer not null,
 key varchar(64),
 value varchar(256),
 primary key (id),
 foreign key (user_id) references users (id)
);

create sequence settings_id start with 1;
