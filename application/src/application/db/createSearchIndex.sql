--liquibase formatted sql
--changeset author:lp

create table searchindex (
 id integer,
 created timestamp,
 beanname varchar(64),
 propertyalias varchar(64),
 value varchar(64),
 record_id integer,
 title_id integer,
 musician_id integer,
 musicianalias_id integer,
 instrument_id varchar(8),
 method varchar(8),
 position integer,
 length integer,

 primary key (id),

 foreign key (record_id) references records (id),
 foreign key (title_id) references titles (id),
 foreign key (musician_id) references musicians (id),
 foreign key (musicianalias_id) references musicianalias (id),
 foreign key (instrument_id) references instruments (id)

);

create index searchindex_value ON searchindex (value);
create index searchindex_beanname ON searchindex (beanname);

create sequence searchindex_id start with 1;

-- Force reindex.

insert into settings (id, created, user_id, key, value)
 values (nextval('settings_id'),  '2024-06-09 10:00:00.000', 0, 'DOREINDEX', 'TRUE');
insert into settings (id, created, user_id, key, value)
 values (nextval('settings_id'),  '2024-06-09 10:00:00.000', 0, 'INDEXREADY', 'FALSE');
