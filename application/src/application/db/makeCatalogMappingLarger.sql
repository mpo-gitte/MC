--liquibase formatted sql
--changeset author:lp



alter table catalogmappings alter column fromString type varchar(200);
alter table catalogmappings alter column toString type varchar(200);
