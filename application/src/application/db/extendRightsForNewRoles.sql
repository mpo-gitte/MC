--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

update roles set rights=rights||';readmusicians=true;' where id=1;
update roles set rights=rights||'readmusicians=true;complexsearch=true;' where id=3;
