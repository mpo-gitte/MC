--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

update roles set rights=rights||'searchForNoChildren=true;' where id=3;
