--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

update roles set name='reader' where id=1;
update roles set name='master' where id=2;
