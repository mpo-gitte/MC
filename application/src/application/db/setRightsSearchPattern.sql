--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

update roles set rights=rights||'ssc=true;' where id=1;
update roles set rights=rights||'ssc=true;' where id=2;
update roles set rights=rights||'ssc=true;' where id=3;
