--liquibase formatted sql
--changeset author:lp

alter table favourites add column ordernr integer;

update favouriteslists set ordernr=0;

CREATE INDEX favourites_ordernr ON favourites (ordernr);
