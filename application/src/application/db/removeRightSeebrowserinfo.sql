--liquibase formatted sql
--changeset author:lp

update roles set rights=new.newrights from (
 select replace(rights, 'seebrowserinfo=true;', '') newrights from roles where id=0
) as new
where id=0
