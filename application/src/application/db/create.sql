--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

create table media (
 id varchar(4),
 created timestamp,
 description varchar(200),
 primary key (id)
);
create table records (
 id integer,
 created timestamp,
 interpreter varchar(200),
 title varchar(200),
 label varchar(200),
 ordernumber varchar(200),
 recording varchar(4),
 medium_id varchar(4),
 number integer,
 releaseyear varchar(5),
 comment varchar(200),
 primary key (id),
 foreign key (medium_id) references media (id)
);
create table roles (
 id integer,
 created timestamp,
 name varchar(64),
 rights varchar(2048),
 primary key(id)
);
create table users (
 id integer,
 created timestamp,
 loginname varchar(64),
 password varchar(64),
 firstname varchar(64),
 lastname varchar(64),
 email varchar(64),
 role_id integer,
 primary key (id),
 foreign key (role_id) references roles (id)
);
create table favouriteslists (
 id integer,
 created timestamp,
 user_id integer not null,
 name varchar(64),
 purpose integer,
 primary key (id),
 foreign key (user_id) references users (id)
);
create table favourites (
 id integer,
 created timestamp,
 favouriteslist_id integer not null,
 record_id integer not null,
 info varchar(200),
 primary key (id),
 foreign key (favouriteslist_id) references favouriteslists (id),
 foreign key (record_id) references records (id)
);
create table titles (
 id integer,
 created timestamp,
 record_id integer not null,
 interpreter varchar(200),
 title varchar(200),
 duration varchar(32),
 composer varchar(200),
 recordingdate varchar(64),
 location varchar(200),
 producer varchar(200),
 side integer,
 track integer,
 primary key (id),
 foreign key (record_id) references records (id)
);
create table instruments (
 id varchar(8),
 created timestamp,
 description varchar(200),
 primary key (id)
);
create table musicians (
 id integer,
 created timestamp,
 name varchar(200),
 comment varchar(200),
 born varchar(32),
 died varchar(32),
 instrument_id varchar(8),
 primary key (id),
 foreign key (instrument_id) references instruments (id)
);
create table jobs (
 id integer,
 created timestamp,
 title_id integer not null,
 musician_id integer not null,
 instrument_id varchar(8) not null,
 primary key (id),
 foreign key (title_id) references titles (id),
 foreign key (musician_id) references musicians (id),
 foreign key (instrument_id) references instruments (id),
 unique (title_id, musician_id, instrument_id)
);
create table clients (
 id integer,
 created timestamp,
 clientstring varchar(200),
 compatibilitylevel integer,
 primary key (id)
);
create table logbook (
 id integer,
 created timestamp,
 user_id integer,
 client_id integer,
 verb integer,
 record_id integer,
 title_id integer,
 musician_id integer,
 ouser_id integer,
 omusician_id integer,
 clientip varchar(32),
 sessionid varchar(64),
 referer varchar(200),
 info varchar(200),
 data varchar(1024),
 titlesig varchar(200),
 musiciansig varchar(200),
 omusiciansig varchar(200),
 ousersig varchar(200),
 recordsig varchar(200),
 primary key (id),
 foreign key (user_id) references users (id),
 foreign key (client_id) references clients (id),
 foreign key (title_id) references titles (id),
 foreign key (musician_id) references musicians (id),
 foreign key (ouser_id) references users (id),
 foreign key (omusician_id) references musicians (id),
 foreign key (record_id) references records (id)
);
create table tickets (
 id integer,
 created timestamp,
 user_id integer not null,
 lastaccess timestamp,
 type integer,
 cancelled boolean,
 until timestamp,
 factor integer,
 primary key (id),
 foreign key (user_id) references users (id)
);
create table searchpatterns (
 id integer,
 created timestamp,
 user_id integer not null,
 lastaccess timestamp,
 name varchar(64),
 searchstring varchar(512),
 position integer,
 media varchar(128),
 areas varchar(128),
 flags varchar(128),
 primary key (id),
 foreign key (user_id) references users (id)
);
create table titlereferences (
 user_id integer not null,
 title_id integer not null,
 purpose integer,
 foreign key (user_id) references users (id),
 foreign key (title_id) references titles (id)
);
create table rolesmedia (
 id integer,
 medium_id varchar(4) not null,
 role_id integer not null,
 primary key (id),
 foreign key (medium_id) references media (id),
 foreign key (role_id) references roles (id)
);

