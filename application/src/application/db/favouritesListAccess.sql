--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

alter table favouriteslists add column access integer;

-- Set read/write to owner
update favouriteslists set access=3;
