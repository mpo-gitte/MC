--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

insert into roles (id, created, name, rights)
 values (3, '2020-08-04 20:00:00.000', 'writer', 'readrecords=true;readtitles=true;seebrowserinfo=true;seeTagCloud=true;readinstruments=true;getdiscography=true;sBC=true;editrecords=true;addrecords=true;edittitles=true;addtitles=true;getdiscography=true;savesearchpatterns=true;namesearchpatterns=true;searchempty=true;autologin=true;seefavouriteslists=true;seestatisticscount=true;seestatisticstopissuers=true;seestatisticstopmusicians=true;seestatisticsaddition=true;addinstruments=true;editmusicians=true;');

update roles set rights='readrecords=true;readtitles=true;seebrowserinfo=true;seeTagCloud=true;readinstruments=true;getdiscography=true;sBC=true;getdiscography=true;savesearchpatterns=true;namesearchpatterns=true;searchempty=true;autologin=true;seefavouriteslists=true;seestatisticscount=true;seestatisticstopissuers=true;seestatisticstopmusicians=true;seestatisticsaddition=true' where id=1