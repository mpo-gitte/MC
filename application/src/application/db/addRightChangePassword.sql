--liquibase formatted sql
--changeset author:lp

update roles set rights=rights||'cpw=true;' where id=3;
update roles set rights=rights||'cpw=true;' where id=2;
update roles set rights=rights||'cpw=true;' where id=1;
update roles set rights=rights||'cpw=false;' where id=0;
