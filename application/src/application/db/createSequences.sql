--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

do $$
declare maxid int;
begin

select max(id)+1 from records into maxid;
execute 'create sequence records_id start with '|| maxid;

select max(id)+1 from roles into maxid;
execute 'create sequence roles_id start with '|| maxid;

select max(id)+1 from users into maxid;
execute 'create sequence users_id start with '|| maxid;

select max(id)+1 from favouriteslists into maxid;
execute 'create sequence favouriteslists_id start with '|| maxid;

select max(id)+1 from favourites into maxid;
execute 'create sequence favourites_id start with '|| maxid;

select max(id)+1 from titles into maxid;
execute 'create sequence titles_id start with '|| maxid;

select max(id)+1 from musicians into maxid;
execute 'create sequence musicians_id start with '|| maxid;

select max(id)+1 from jobs into maxid;
execute 'create sequence jobs_id start with '|| maxid;

--select max(id)+1 from clients into maxid;
--execute 'create sequence clients_id start with '|| maxid;
execute 'create sequence clients_id start with 1';

--select max(id)+1 from logbook into maxid;
--execute 'create sequence logbook_id start with '|| maxid;
execute 'create sequence logbook_id start with 1';

--select max(id)+1 from tickets into maxid;
--execute 'create sequence tickets_id start with '|| maxid;
execute 'create sequence tickets_id start with 1';

--select max(id)+1 from searchpatterns into maxid;
--execute 'create sequence searchpatterns_id start with '|| maxid;
execute 'create sequence searchpatterns_id start with 1';

select max(id)+1 from rolesmedia into maxid;
execute 'create sequence rolesmedia_id start with '|| maxid;

end;
$$ language plpgsql
