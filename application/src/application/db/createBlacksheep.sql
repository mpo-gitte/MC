--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

create table blacksheep (
 id integer,
 created timestamp,
 user_id integer,
 reason varchar(128),
 address varchar(64),
 port integer,
 session_id varchar(64),
 client_id integer,
 lastseen timestamp,
 counter integer,
 primary key (id),
 foreign key (user_id) references users (id),
 foreign key (client_id) references clients (id)
);

create sequence blacksheep_id start with 1;



--select * from blacksheep;
--select * from blacksheep;

--delete from blacksheep;

--select * from blacksheep where address='0:0:0:0:0:0:0:1'
--select address, count(1) overall from blacksheep group by address, counter having counter >= 3