--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

alter table instruments add column main_instrument_id varchar(8) references instruments(id);

update instruments set main_instrument_id='voc' where id='bvoc';
update instruments set main_instrument_id='sax' where id='ss';
update instruments set main_instrument_id='sax' where id='as';
update instruments set main_instrument_id='sax' where id='ts';
update instruments set main_instrument_id='sax' where id='bs';
update instruments set main_instrument_id='tp' where id='flh';
