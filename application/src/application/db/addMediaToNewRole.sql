--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

insert into rolesmedia (id, medium_id, role_id)
 values (nextval('rolesmedia_id'),  'CD', 3);
insert into rolesmedia (id, medium_id, role_id)
 values (nextval('rolesmedia_id'),  'LP', 3);
