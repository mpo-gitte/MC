--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

update roles set rights=rights||'sCP=true;' where id=1;
update roles set rights=rights||'sCP=true;' where id=2;
update roles set rights=rights||'sCP=true;' where id=3;

update roles set rights=rights||'eCP=true;' where id=2;
update roles set rights=rights||'eCP=true;' where id=3;

update roles set rights=rights||'sCPu=true;' where id=2;
update roles set rights=rights||'sCPu=true;' where id=3;
