--liquibase formatted sql
--changeset author:lp

CREATE INDEX favouriteslists_user_id ON favouriteslists (user_id);
CREATE INDEX favouriteslists_ordernr ON favouriteslists (ordernr);
