--liquibase formatted sql
--changeset author:lp

update roles set rights=new.newrights from (
 select replace(rights, 'seememinfo=true;', '') newrights from roles where id=2
) as new
where id=2;

update roles set rights=new.newrights from (
 select replace(rights, 'deletedatastores=true;', '') newrights from roles where id=2
) as new
where id=2;

update roles set rights=new.newrights from (
 select replace(rights, 'seedatastores=true;', '') newrights from roles where id=2
) as new
where id=2;

update roles set rights=new.newrights from (
 select replace(rights, 'seedatastoreparams=true;', '') newrights from roles where id=2
) as new
where id=2;
