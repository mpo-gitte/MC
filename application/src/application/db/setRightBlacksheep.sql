--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

update roles set rights=rights||'sBs=true;' where id=2;

--select * from roles;

--select blacksheep.*, users.loginname subuser$loginname, users.firstname subuser$firstname, users.lastname subuser$lastname from blacksheep left join users on users.id=blacksheep.user_id