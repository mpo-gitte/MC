--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

insert into users (id, created, loginname, firstname, lastname, email, role_id)
 values (2, '2020-08-04 20:00:00.000', 'mcreader', 'MC', 'reader', '', 1);
insert into users (id, created, loginname, firstname, lastname, email, role_id)
 values (3, '2020-08-04 20:00:00.000', 'mcwriter', 'MC', 'writer', '', 3);

-- Mcreader1
insert into passwords (id, created, user_id, password, salt, secretpurpose, expires, expcount)
 values (nextval('passwords_id'), '2020-08-04 20:00:00.000', 2, '$2b$04$QVGzYF.0axbtTDbfTlPUM.zZcY2G6/rDbeLCeqZCMtHX0GWdWTFje', 'UOclxYbxiiSMyzUg', 'password.bcrypt', '2050-12-31 23:59:59.000', 2147483646);
-- Mcwriter1
insert into passwords (id, created, user_id, password, salt, secretpurpose, expires, expcount)
 values (nextval('passwords_id'), '2020-08-04 20:00:00.000', 3, '$2b$04$ZUa1YiTza03tPkv0USu2R.zQBG3brPRC2lT3I.iLM8LJh7YN3j1jC', 'dc1VPOWawzyElyQu', 'password.bcrypt', '2050-12-31 23:59:59.000', 2147483646);
