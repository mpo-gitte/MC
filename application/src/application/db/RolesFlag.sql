--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

alter table roles add column flags integer;

update roles set flags=0;
update roles set flags=1 where id = 1;
