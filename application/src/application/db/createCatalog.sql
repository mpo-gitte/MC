--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

create table catalogrequests (
 id integer,
 created timestamp,
 record_id integer not null,
 primary key (id),
 foreign key (record_id) references records (id)
);

create sequence catalogrequests_id start with 1;

create table catalogitems (
 id integer,
 created timestamp,
 text varchar(64),
 primary key (id)
);

create sequence catalogitems_id start with 1;

create table catalogrecords (
 id integer,
 created timestamp,
 record_id integer not null,
 catalogitem_id integer not null,
 primary key (id),
 foreign key (record_id) references records (id),
 foreign key (catalogitem_id) references catalogitems (id)
);

create sequence catalogrecords_id start with 1;

create table catalogmappings (
 id integer,
 created timestamp,
 fromString varchar(64),
 toString varchar(64),
 primary key (id)
);

create sequence catalogmappings_id start with 1;
