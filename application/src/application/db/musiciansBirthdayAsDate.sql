--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

alter table musicians drop column born;

alter table musicians add column born date;
alter table musicians add column bornin varchar(64);

alter table musicians drop column died;

alter table musicians add column died date;
alter table musicians add column diedin varchar(64);
