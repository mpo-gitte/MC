--liquibase formatted sql
--changeset author:lp

update roles set rights=rights||'idxs=true;' where id=3;
update roles set rights=rights||'idxs=true;' where id=2;
update roles set rights=rights||'idxs=true;' where id=1;
update roles set rights=rights||'idxs=true;' where id=0;
