--liquibase formatted sql
--changeset author:lp

alter table users add column flags integer;

update users set flags=0;
