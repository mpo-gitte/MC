--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

create table logins (
 session_id varchar(64),
 created timestamp,
 user_id integer not null,
 host varchar(64),
 effective_rights varchar(2048),
 autologin boolean,
 ticket_id integer,
 client_id integer,
 lastseen timestamp,
 primary key (session_id),
 foreign key (user_id) references users (id),
 foreign key (client_id) references clients (id),
 foreign key (ticket_id) references tickets(id)
);
