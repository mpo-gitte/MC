--liquibase formatted sql
--changeset author:pilgrim.lutz@imail.de

create table passwords (
 id integer,
 created timestamp,
 user_id integer not null,
 password varchar(64),
 salt varchar(64),
 secretpurpose varchar(64),
 expires timestamp,
 expcount integer,
 primary key (id),
 foreign key (user_id) references users (id)
);

-- User guest: password: GuestGuest1
insert into passwords (id, created, user_id, password, salt, secretpurpose, expires, expcount)
 values (1, '2018-12-01 20:06:15.525', 0, '$2b$04$QDa2OyyvQUvBaS2ubVntcewurTXnDdhTjjeDiZw073Og7KxB.DIYS', 'KYiiVYG2xb4MW3Ce', 'password.bcrypt', '2050-12-31 23:59:59', 2147483647);

-- User mcmaster: password = Mcmaster1
insert into passwords (id, created, user_id, password, salt, secretpurpose, expires, expcount)
 values (2, '2018-12-01 19:16:50.871', 1, '$2b$04$ayCzcSzHYkjFPECyTDjgQuLvyx5kLzkDqrnKSZc6n57OjXkmkqA7C', 'X0ul0wkT1Ud7GRmb', 'password.bcrypt', '2050-12-31 23:59:59', 2147483647);

create sequence passwords_id start with 3;

update roles set rights=rights||'sfP=true;' where id=2;

alter table users drop column password;
