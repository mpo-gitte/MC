package application.configs;

import baked.application.beans.Login;
import core.util.Configurator;



/**
 * Config for all login related values.
 *
 * @version 2023-09-05
 * @author lp
 */

public class ConfigLogin implements Configurator.Config {



 /**
  * Get the current user.
  *
  * @param Configurator.ConfigurableField The field.
  * @return The current user.
  *
  * @see core.util.RequestConfig
  */

 @Configurator.ConfigValue(matchedBy = Configurator.FieldSelect.FIELD_HAS_ANNOTATION, value= ".*ConfigCurrentUser.*")
 public Object getConfigCurrentUser(Configurator.ConfigurableField field) {

  return Login.get().get("refUser");

 }
}
