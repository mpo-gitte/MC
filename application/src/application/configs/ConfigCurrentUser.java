package application.configs;

import java.lang.annotation.*;


/**
 * Marks a Method parameter to be filled with the Locale
 *
 * @version 2023-04-23
 * @author lp
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface ConfigCurrentUser {

}
