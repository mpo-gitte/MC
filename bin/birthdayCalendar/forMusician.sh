#!/bin/sh

# ShellScript for getting a birthday calendar.
#
# version: 2021-01-09
# author: pilgrim.lutz@imail.de

# Sets url
. ../settings

export url=$MCHost/MC-$MCStage/services/birthdayCalendar/forMusicians.vm

arg1=""
arg2=""

if [ "$1" = "true" ]; then
 arg1="--data-urlencode bcdeco=true"
fi
if [ "$2" = "true" ]; then
 arg1="--data-urlencode discogr=true"
fi

curl -s -X GET -H 'Accept-Language: de' -G "$url" $arg1 $arg2
