#!/bin/sh

# ShellScript for getting the ID3 tags for titles of a record.
#
# version: 2021-12-08
# author: pilgrim.lutz@imail.de

# Sets url
. ../settings

export url=$MCHost/MC-$MCStage/services/id3/id3.vm

curl -s -X GET --header "authorization: MC_TICKET $ticket" --data-urlencode "interpreter=$1" --data-urlencode "title=$2" -G "$url" -v
