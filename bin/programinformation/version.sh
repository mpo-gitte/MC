#!/bin/sh

# ShellScript for getting the version of a runnting MC.
#
# version: 2020-08-06
# author: pilgrim.lutz@imail.de

# Sets url
. ../settings

export url=$MCHost/MC-$MCStage/services/programinformation/version.vm

curl -s -X GET -G "$url"
