#!/bin/sh

# ShellScript for checking health of loaded workers.
#
# version: 2022-05-20
# author: pilgrim.lutz@imail.de

# Sets url
. ../settings

export url=$MCHost/MC-$MCStage/services/programinformation/health.vm

curl -s -X GET -w "%{http_code}" -G "$url"
echo ""
