#!/bin/bash
#
# This is the way to start MC.
#
# Example to show the minimum required options for MC.
#
# @version 2025-02-12
# @author lp

export PUC_DB_Dropable=true
export PUC_DB_Password=EFyaMcAus8
export PUC_DB_URL=jdbc:postgresql://localhost/mc_local
export PUC_DB_User=mc_local
export PUC_Request_ProxyIpAddressPrefixes=192.168.1.74
export PUC_Request_ValidUrls=ewvp426pc9lzrvmy.myfritz.net

/usr/bin/java --add-opens=java.base/java.io=ALL-UNNAMED -jar build/libs/MC-local.war