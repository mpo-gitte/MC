#!/bin/sh

# ShellScript for getting the rights of the user authenticated by the given session id.
#
# $1: session id
#
# version: 2021-01-04
# author: pilgrim.lutz@imail.de

# Sets url and ticket
. ../settings

export url=$MCHost/MC-$MCStage/services/rights/forSessionId.vm

curl -s -X GET -w "%{http_code}" --data-urlencode "sessionid=$1" -G "$url"
