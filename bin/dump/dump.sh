#!/bin/sh

# ShellScript for dumping database content.
#
# version: 2020-08-06
# author: pilgrim.lutz@imail.de

# Sets url and ticket
. ../settings

export url=$MCHost/MC-$MCStage/services/dump/data.vm

curl -s -X GET -w "%{http_code}" -H "authorization: MC_TICKET $ticket" -G "$url"
