#!/bin/bash
#
# Build script for MC.
#
# @version 2025-02-12
# @author lp
#
# $1: Stage: new, fix, master
# $2: Branch: master, new-1-33-0

# If you are on a X86 set environment variable HOSTTYPE to "amd64"!

dockreg=joe:49153
gitrep=ssh://git@joe.fritz.box:222/lutz/mc.git

now=`date +%Y%m%d%H%M%s`
workdir=`mktemp -d /tmp/builder.${now}.XXXXXXXXX`

gtasks="dependencyCheckAnalyze build core:testWithDatabase makeDockerImage"

errormode=1
if [[ $2 =~ ^(master*.) ]]
then
 errormode=0
fi

echo ---------------------------------------------
echo doing $gtasks
echo for stage $1
echo on platform ${HOSTTYPE}
echo ---------------------------------------------

git clone ${gitrep} $workdir
cd $workdir
git checkout $2
export DOCKER_REGISTRY=${dockreg}
lastcm=`git --no-pager log --format=format:%s -1`
echo "tagging with ${lastcm}"

sh gradlew -Pmcstage="$1" -Pmctag="${lastcm}" -Pmcerrormode=${errormode} -Pplatform=${HOSTTYPE} $gtasks
